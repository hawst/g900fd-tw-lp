.class Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$4;
.super Ljava/lang/Object;
.source "UvSummaryFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;)V
    .locals 0

    .prologue
    .line 2630
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$4;->this$1:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 2660
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 2654
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    const/16 v1, 0x64

    .line 2637
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-le v0, v1, :cond_1

    .line 2639
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$4;->this$1:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 2647
    :cond_0
    :goto_0
    return-void

    .line 2641
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const-string v0, "000"

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2644
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25$4;->this$1:Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment$25;->this$0:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mSpfNumberPicker:Landroid/widget/NumberPicker;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setValue(I)V

    goto :goto_0
.end method
