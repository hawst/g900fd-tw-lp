.class public Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "FoodPickActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnAddCustomFoodListener;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnFoodInfoSelectedListener;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnListItemLongClickListener;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;
.implements Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunnerAccessor;
.implements Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor;


# static fields
.field private static final ADD_CUSTOM_FOOD_CODE:I = 0x4d2

.field private static final DONE_BUTTON_INDEX:I = 0x1

.field private static final EDIT_DELETE_DIALOG_TAG:Ljava/lang/String; = "EDIT_DELETE_DIALOG_TAG"

.field private static final NO_BACKGROUND_RESOURCE:I = 0x0

.field private static final OK_CANCEL_DELETE_DIALOG_TAG:Ljava/lang/String; = "OK_CANCEL_DELETE_DIALOG_TAG"

.field private static final sDeleteOptionIndex:I = 0x1


# instance fields
.field private mOnCancelClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

.field private mOnDoneClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

.field private mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

.field private mSelectedPanelHolder:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

.field private mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field protected mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mOnCancelClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mOnDoneClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->showOkCancelDeleteMealDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSelectedPanelHolder:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "MEAL_DATA_HOLDER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    if-nez v0, :cond_0

    .line 103
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'s inheritance must start with extra: key"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MEAL_DATA_HOLDER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 108
    return-void
.end method

.method private showOkCancelDeleteMealDialog()V
    .locals 4

    .prologue
    const v3, 0x7f090035

    .line 320
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09094c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "OK_CANCEL_DELETE_DIALOG_TAG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 325
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 116
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 118
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)V

    .line 130
    .local v2, "onCancelClickListener":Landroid/view/View$OnClickListener;
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)V

    .line 143
    .local v3, "onDoneClickListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    const v5, 0x7f09003f

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 144
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f090048

    invoke-direct {v0, v6, v4, v2, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 145
    .local v0, "cancelButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f090044

    invoke-direct {v1, v6, v4, v3, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 146
    .local v1, "doneButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v0, v5, v6

    aput-object v1, v5, v7

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 149
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x1

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 354
    .local v1, "currentView":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/16 v6, 0x3d

    if-ne v4, v6, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_0

    .line 356
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v4

    const v6, 0x7f08030e

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    move-object v2, v4

    check-cast v2, Landroid/widget/LinearLayout;

    .line 358
    .local v2, "parentView":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v4

    const v6, 0x7f080452

    if-ne v4, v6, :cond_0

    .line 360
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 361
    .local v3, "targetView":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 363
    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    .line 364
    const-string v4, "audio"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 365
    .local v0, "amm":Landroid/media/AudioManager;
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->playSoundEffect(I)V

    move v4, v5

    .line 373
    .end local v0    # "amm":Landroid/media/AudioManager;
    .end local v2    # "parentView":Landroid/widget/LinearLayout;
    .end local v3    # "targetView":Landroid/view/View;
    :goto_0
    return v4

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v4

    goto :goto_0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    .line 196
    .local v0, "contentInitializationListener":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    if-eqz v0, :cond_0

    .line 199
    .end local v0    # "contentInitializationListener":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    :goto_0
    return-object v0

    .restart local v0    # "contentInitializationListener":Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 264
    const-string v0, "EDIT_DELETE_DIALOG_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)V

    .line 281
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTaskRunner()Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    return-object v0
.end method

.method public isDeleteMode()Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 286
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 287
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 288
    packed-switch p1, :pswitch_data_0

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 290
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->toggleActivityState(Z)V

    goto :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x4d2
        :pswitch_0
    .end packed-switch
.end method

.method public onAddCustomFoodClick()Z
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addCustomFoodSavedToMyFood()V

    .line 208
    const/4 v0, 0x1

    return v0
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    .line 233
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->hideSearchResults()Z

    move-result v6

    if-nez v6, :cond_1

    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 235
    .local v1, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v3

    .line 237
    .local v3, "fragmentList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    const/4 v5, 0x0

    .line 238
    .local v5, "isProceedByChildFragment":Z
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 239
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v4, v6, -0x1

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_0

    .line 240
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/Fragment;

    .line 241
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v2, :cond_2

    instance-of v6, v2, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isHidden()Z

    move-result v6

    if-nez v6, :cond_2

    move-object v0, v2

    .line 242
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 243
    .local v0, "baseFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->backPressed()Z

    move-result v5

    .line 244
    if-eqz v5, :cond_2

    .line 250
    .end local v0    # "baseFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .end local v2    # "fragment":Landroid/support/v4/app/Fragment;
    .end local v4    # "i":I
    :cond_0
    if-nez v5, :cond_1

    .line 251
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    .line 254
    .end local v1    # "fm":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "fragmentList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    .end local v5    # "isProceedByChildFragment":Z
    :cond_1
    return-void

    .line 239
    .restart local v1    # "fm":Landroid/support/v4/app/FragmentManager;
    .restart local v2    # "fragment":Landroid/support/v4/app/Fragment;
    .restart local v3    # "fragmentList":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    .restart local v4    # "i":I
    .restart local v5    # "isProceedByChildFragment":Z
    :cond_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->init()V

    .line 91
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const v0, 0x7f030100

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->setContentView(I)V

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f08044b

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 97
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->shutDown()V

    .line 170
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 171
    return-void
.end method

.method public onFoodInfoSelected(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z
    .locals 1
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addFoodItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 214
    const/4 v0, 0x1

    return v0
.end method

.method public onListItemLongClick(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z
    .locals 1
    .param p1, "selectedPanelHolder"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSelectedPanelHolder:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .line 226
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->showEditDeleteMealDialog(Ljava/lang/String;)V

    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 157
    .local v0, "currentView":Landroid/view/View;
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->showSoftInputKeypad(Landroid/content/Context;Landroid/view/View;)V

    .line 158
    return-void
.end method

.method public onSelectedPanelHolderSelected(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z
    .locals 1
    .param p1, "selectedPanelHolder"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mSearchFragment:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->addFoodItem(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)V

    .line 220
    const/4 v0, 0x1

    return v0
.end method

.method public setDoneButtonEnabled(Z)V
    .locals 2
    .param p1, "isEnabled"    # Z

    .prologue
    const/4 v1, 0x1

    .line 185
    if-eqz p1, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->enableActionBarButton(I)V

    .line 190
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    goto :goto_0
.end method

.method public setOnCancelClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;)V
    .locals 0
    .param p1, "onCancelClickListener"    # Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mOnCancelClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

    .line 176
    return-void
.end method

.method public setOnDoneClickListener(Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;)V
    .locals 0
    .param p1, "onDoneClickListener"    # Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->mOnDoneClickListener:Lcom/sec/android/app/shealth/food/foodpick/search/ActionBarDoneCancelAccessor$OnActionBarClick;

    .line 181
    return-void
.end method

.method public showEditDeleteMealDialog(Ljava/lang/String;)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 299
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const/4 v4, -0x1

    const/16 v5, 0x9

    invoke-direct {v1, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 303
    .local v1, "dialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity$4;-><init>(Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;)V

    .line 310
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 311
    const/4 v4, 0x2

    new-array v0, v4, [Z

    fill-array-data v0, :array_0

    .line 312
    .local v0, "checked":[Z
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 313
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v3

    .line 314
    .local v3, "listChooseDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->setTitle(Ljava/lang/String;)V

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "EDIT_DELETE_DIALOG_TAG"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 317
    return-void

    .line 311
    :array_0
    .array-data 1
        0x1t
        0x0t
    .end array-data
.end method
