.class public Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;
.super Ljava/lang/Object;
.source "SimplePopupController.java"


# static fields
.field private static final POPUP_SHOW_TIME_IN_MILLIS:I = 0x7d0


# instance fields
.field delayedRunnable:Ljava/lang/Runnable;

.field handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->handler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public onShow()V
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->delayedRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->delayedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 21
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->delayedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 23
    :cond_0
    return-void
.end method

.method public released(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->delayedRunnable:Ljava/lang/Runnable;

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;->handler:Landroid/os/Handler;

    const-wide/16 v1, 0x7d0

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 30
    return-void
.end method
