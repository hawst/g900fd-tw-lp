.class public Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerContentView;
.super Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;
.source "ThermoHygrometerContentView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;-><init>(Landroid/content/Context;)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0069

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerContentView;->setBalanceLine(II)V

    .line 37
    return-void
.end method


# virtual methods
.method protected getBalanceLeftIconId()I
    .locals 1

    .prologue
    .line 51
    const v0, 0x7f0206fd

    return v0
.end method

.method protected getBalanceRightIconId()I
    .locals 1

    .prologue
    .line 83
    const v0, 0x7f0206f9

    return v0
.end method

.method protected getLeftBarTitleText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090130

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNoDataLeftIconId()I
    .locals 1

    .prologue
    .line 61
    const v0, 0x7f0206fd

    return v0
.end method

.method protected getNoDataRightIconId()I
    .locals 1

    .prologue
    .line 93
    const v0, 0x7f0206f9

    return v0
.end method

.method protected getRightBarTitleText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/ThermoHygrometerContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0910a2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getUnbalanceLeftIconId()I
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f0206fd

    return v0
.end method

.method protected getUnbalanceRightIconId()I
    .locals 1

    .prologue
    .line 88
    const v0, 0x7f0206f9

    return v0
.end method

.method protected getUnitLeftIconId()I
    .locals 2

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v1, "F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    const v0, 0x7f0900ce

    .line 71
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0900cd

    goto :goto_0
.end method

.method protected getUnitRightIconId()I
    .locals 1

    .prologue
    .line 78
    const v0, 0x7f0910a9

    return v0
.end method
