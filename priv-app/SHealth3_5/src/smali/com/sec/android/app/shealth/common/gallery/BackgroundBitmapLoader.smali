.class public Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;
.super Landroid/os/AsyncTask;
.source "BackgroundBitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field private static final IN_SAMPLE_SIZE:I = 0x2


# instance fields
.field private mScalableImage:Lcom/sec/android/app/shealth/common/gallery/ScalableView;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/shealth/common/gallery/ScalableView;)V
    .locals 0
    .param p1, "scalableImage"    # Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;->mScalableImage:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    .line 33
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 55
    :cond_0
    :goto_0
    return-object v3

    .line 40
    :cond_1
    const/4 v4, 0x0

    aget-object v2, p1, v4

    .line 42
    .local v2, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 44
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 49
    const/4 v4, 0x2

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/common/gallery/GalleryUtils;->getBitmapWithReusingByLast(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 50
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 55
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapWithCorrectingOrientationToNormal(Landroid/graphics/Bitmap;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;->doInBackground([Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;->mScalableImage:Lcom/sec/android/app/shealth/common/gallery/ScalableView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/gallery/ScalableView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 64
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/BackgroundBitmapLoader;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
