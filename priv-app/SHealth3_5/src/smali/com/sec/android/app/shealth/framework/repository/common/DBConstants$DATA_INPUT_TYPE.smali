.class public Lcom/sec/android/app/shealth/framework/repository/common/DBConstants$DATA_INPUT_TYPE;
.super Ljava/lang/Object;
.source "DBConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DATA_INPUT_TYPE"
.end annotation


# static fields
.field public static final ANT_INPUT:I = 0x3

.field public static final BLUETOOTH_INPUT:I = 0x1

.field public static final B_WATCH_INPUT:I = 0x9

.field public static final DEFAULT_INPUT_TYPE:I = -0x1

.field public static final INPUT_TYPE_MANUAL:I = -0x2

.field public static final NETPULSE_INPUT:I = 0x7

.field public static final REALTIME_INDOOR_INPUT:I = 0x4

.field public static final REALTIME_OUTDOOR_INPUT:I = 0x5

.field public static final USB_INPUT:I = 0x2

.field public static final WALKFORLIFE_INPUT:I = 0x8

.field public static final WATCH_INPUT:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
