.class Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;
.super Ljava/lang/Object;
.source "HomeWallpaperChooser.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x0

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->setGalleryBackgroundFocus(Landroid/view/View;)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # getter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgroundView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$100(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Landroid/view/View;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # getter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$000(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # setter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->currentWallpaper:I
    invoke-static {v1, p3}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$202(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;I)I

    .line 97
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser$1;->this$0:Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;

    # getter for: Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->backgrounds:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;->access$000(Lcom/sec/android/app/shealth/home/widget/HomeWallpaperChooser;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 98
    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 99
    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 100
    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 97
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_1
    return-void
.end method
