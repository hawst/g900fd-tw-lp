.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;
.super Ljava/lang/Object;
.source "WalkingMateStatisticServerTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserInfo"
.end annotation


# instance fields
.field public final accountId:Ljava/lang/String;

.field public final age:Ljava/lang/String;

.field public final gender:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final ranking:I

.field private final totalDistance:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "JSONString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 635
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 636
    .local v0, "object":Lorg/json/JSONObject;
    const-string/jumbo v1, "ranking"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->ranking:I

    .line 637
    const-string v1, "accountId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->accountId:Ljava/lang/String;

    .line 638
    const-string/jumbo v1, "name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->name:Ljava/lang/String;

    .line 639
    const-string v1, "age"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->age:Ljava/lang/String;

    .line 640
    const-string v1, "gender"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->gender:Ljava/lang/String;

    .line 641
    const-string/jumbo v1, "totalDistance"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->totalDistance:Ljava/lang/String;

    .line 642
    return-void
.end method


# virtual methods
.method public getAge()I
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->age:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getTotalDistance()J
    .locals 2

    .prologue
    .line 649
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->totalDistance:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method
