.class final enum Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
.super Ljava/lang/Enum;
.source "CoachRestoreHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Step"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

.field public static final enum FAILED_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

.field public static final enum NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

.field public static final enum NOTICE_FOR_NO_DATA_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

.field public static final enum RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

.field public static final enum START_BACKUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

.field public static final enum START_RESTORE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

.field public static final enum SUCCESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 59
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    const-string v1, "START_RESTORE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->START_RESTORE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 60
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    const-string v1, "START_BACKUP"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->START_BACKUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 62
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    const-string v1, "RESTORATION_PROGRESS_POPUP"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 63
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    const-string v1, "NOTICE_FOR_NO_DATA_POPUP"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NOTICE_FOR_NO_DATA_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    const-string v1, "FAILED_POPUP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->FAILED_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    const-string v1, "SUCCESS_POPUP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->SUCCESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 56
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->START_RESTORE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->START_BACKUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NOTICE_FOR_NO_DATA_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->FAILED_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->SUCCESS_POPUP:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    return-object v0
.end method
