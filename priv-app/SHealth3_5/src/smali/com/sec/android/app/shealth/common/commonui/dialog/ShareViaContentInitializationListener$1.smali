.class Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$1;
.super Ljava/lang/Object;
.source "ShareViaContentInitializationListener.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 89
    .local v2, "launchable":Landroid/content/pm/ResolveInfo;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->access$100(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 90
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 91
    .local v1, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string/jumbo v5, "share_via_popup"

    invoke-virtual {v1, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/DialogFragment;

    .line 92
    .local v4, "shareViaDialogFragment":Landroid/support/v4/app/DialogFragment;
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->getIntentFromShareExtras(Landroid/content/pm/ResolveInfo;Landroid/support/v4/app/DialogFragment;)Landroid/content/Intent;
    invoke-static {v5, v2, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->access$200(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;Landroid/content/pm/ResolveInfo;Landroid/support/v4/app/DialogFragment;)Landroid/content/Intent;

    move-result-object v3

    .line 93
    .local v3, "shareIntent":Landroid/content/Intent;
    if-eqz v4, :cond_0

    .line 94
    invoke-virtual {v4}, Landroid/support/v4/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 95
    :cond_0
    if-eqz v3, :cond_1

    .line 96
    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v1    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "shareIntent":Landroid/content/Intent;
    .end local v4    # "shareViaDialogFragment":Landroid/support/v4/app/DialogFragment;
    :cond_1
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v5

    goto :goto_0
.end method
