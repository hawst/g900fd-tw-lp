.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;
.super Landroid/content/BroadcastReceiver;
.source "WalkingMateSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0

    .prologue
    .line 1507
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1510
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1511
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.intent.action.TIME_TICK"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mCurrentEndofDate:J
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$3100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    .line 1514
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 1515
    .local v1, "inTime":J
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;)V

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$1;->run()V

    .line 1528
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TIME_TICK duration = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1541
    .end local v1    # "inTime":J
    :cond_1
    :goto_0
    return-void

    .line 1529
    :cond_2
    const-string v3, "com.sec.android.app.shealth.command.resettotalstep"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "com.sec.android.app.shealth.command.deletetotalstep"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1532
    :cond_3
    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9;)V

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$9$2;->run()V

    goto :goto_0
.end method
