.class Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;
.super Ljava/lang/Object;
.source "MyRankingsFragment.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->sendRequestForNewUserToken()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 303
    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$700()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sendRequestForUserToken - accessToken : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$700()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sendRequestForUserToken - userId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$700()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "sendRequestForUserToken - clientId : 1y90e30264"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_0

    .line 320
    :goto_0
    return-void

    .line 307
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 309
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "access_token"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    const-string v2, "email_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 311
    const-string v2, "client_id"

    const-string v3, "1y90e30264"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 314
    .local v0, "deviceId":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$700()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sendRequestForUserToken - deviceId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const-string v2, "device_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 317
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/loaders/WalkingMateRankingHelper;->setSamsungAccountInfoIntent(Landroid/content/Intent;)V

    .line 318
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->cancelUpdateTask()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;)V

    .line 319
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->requestUpdateData(Landroid/content/Intent;Z)V
    invoke-static {v2, v1, v3}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$900(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Landroid/content/Intent;Z)V

    goto :goto_0
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 324
    # getter for: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$700()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NewUserToken - errMsg: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->dismissLoadingDialog()V

    .line 332
    return-void
.end method

.method public setNetworkFailure()V
    .locals 3

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0907ff

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->loadOfflineData(Ljava/lang/String;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/MyRankingsFragment;Ljava/lang/String;Z)V

    .line 337
    return-void
.end method
