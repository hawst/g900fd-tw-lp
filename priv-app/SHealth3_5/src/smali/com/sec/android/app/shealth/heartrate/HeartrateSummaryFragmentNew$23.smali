.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;
.super Ljava/lang/Object;
.source "HeartrateSummaryFragmentNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->stopSensorAndUpdateUI(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

.field final synthetic val$heartRate:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V
    .locals 0

    .prologue
    .line 2123
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iput p2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;->val$heartRate:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 2126
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$5600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;->val$heartRate:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2127
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;->val$heartRate:I

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setHRMStateBar(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$5700(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    .line 2129
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$23;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$1500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 2130
    return-void
.end method
