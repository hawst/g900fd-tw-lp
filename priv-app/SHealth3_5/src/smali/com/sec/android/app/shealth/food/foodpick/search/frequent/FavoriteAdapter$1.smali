.class Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;
.super Ljava/lang/Object;
.source "FavoriteAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->getViewFromListItem(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

.field final synthetic val$listItem:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V
    .locals 0

    .prologue
    .line 56
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter.1;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "favoriteWrapper"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter.1;"
    const v6, 0x7f0803d1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 60
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 61
    .local v2, "isChecked":Z
    if-nez v2, :cond_0

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    iget-object v3, v3, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isFavoriteItemsLimitExceed(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 65
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    iget-object v3, v3, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->context:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    iget-object v6, v6, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->context:Landroid/content/Context;

    const v7, 0x7f090926

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v8, 0x1e

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-virtual {v6, v7, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 80
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    if-nez v2, :cond_1

    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->performClickOnFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V

    .line 74
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->mOnFavoriteClickListeners:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 75
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->mOnFavoriteClickListeners:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;

    .line 76
    .local v0, "favoriteClickListener":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener<TE;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;

    invoke-interface {v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;->onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V

    goto :goto_2

    .end local v0    # "favoriteClickListener":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener<TE;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    move v4, v5

    .line 71
    goto :goto_1

    .line 79
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
