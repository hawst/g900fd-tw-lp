.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;
.super Ljava/lang/Object;
.source "StressSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStart(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

.field final synthetic val$isFailed:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Z)V
    .locals 0

    .prologue
    .line 827
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->val$isFailed:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 831
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const v2, 0x43b38000    # 359.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 832
    .local v0, "circleRotateAnimation":Landroid/view/animation/RotateAnimation;
    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 833
    invoke-virtual {v0, v9}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 834
    invoke-virtual {v0, v9}, Landroid/view/animation/RotateAnimation;->setRepeatMode(I)V

    .line 835
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 837
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v7, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 838
    .local v7, "fadeIn":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x14d

    invoke-virtual {v7, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 840
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    new-instance v2, Landroid/view/animation/AnimationSet;

    invoke-direct {v2, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2202(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;

    .line 841
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/animation/AnimationSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 842
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/animation/AnimationSet;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 844
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->val$isFailed:Z

    if-eqz v1, :cond_0

    .line 845
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 846
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 851
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 852
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mMeasuringAniLinear:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/view/animation/AnimationSet;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 853
    return-void

    .line 848
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 849
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$6;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondMeasuringProgressView1:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$2400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
