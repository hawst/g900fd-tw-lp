.class public Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
.super Ljava/lang/Object;
.source "SpO2DatabaseHelper.java"


# static fields
.field private static instance:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;


# instance fields
.field private latestEntry:Landroid/net/Uri;

.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->latestEntry:Landroid/net/Uri;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method

.method private getChildQuery(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 107
    const-string v0, "create_time>? AND create_time<?"

    .line 108
    .local v0, "clause":Ljava/lang/String;
    const-string v6, "create_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 109
    .local v4, "time":J
    new-array v2, v10, [Ljava/lang/String;

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getMonthToStringByMillisecond(ZJ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v8

    invoke-static {v8, v4, v5}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getMonthToStringByMillisecond(ZJ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v9

    .line 110
    .local v2, "selectionArgs":[Ljava/lang/String;
    sget v6, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;->SELECTED_FILTER_INDEX:I

    if-nez v6, :cond_0

    .line 111
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "0==0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    :cond_0
    const-string v6, "AND"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v3, v6, v10

    .line 115
    .local v3, "selectionClause":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "select A.*, B.AvgSpO2Month, B.AvgPulseMonth from pulse_oximeter AS A LEFT OUTER JOIN (SELECT C.[_id], C.[create_time], AVG(C.[value]) as AvgSpO2Month, AVG(C.[heart_rate]) as AvgPulseMonth from pulse_oximeter as C group by strftime(\"%d-%m-%Y\", (C.[create_time]/1000),\'unixepoch\', \'localtime\')) as B on A.[_id] = B.[_id] where A.create_time >= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v8

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and A."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "create_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " <= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v9

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " order by A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "create_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]/1000 DESC, B.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "AvgSpO2Month"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] DESC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "query":Ljava/lang/String;
    return-object v1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const-class v1, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->instance:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->instance:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .line 33
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->instance:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getShareData(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const v6, 0x7f090d14

    .line 293
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 294
    .local v2, "shareData":Ljava/lang/StringBuffer;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 295
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 296
    const-string v3, "create_time"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 297
    .local v0, "sampleTime":J
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    const v4, 0x7f090240

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string/jumbo v4, "value"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f090c9e

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f090f98

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "heart_rate"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f0900d2

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    const v5, 0x7f090135

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 300
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    .line 302
    .end local v0    # "sampleTime":J
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getSpO2Data(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 54
    new-instance v0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;-><init>()V

    .line 55
    .local v0, "spo2Data":Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 56
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setId(J)V

    .line 58
    const-string/jumbo v1, "user_device__id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setUserDeviceId(Ljava/lang/String;)V

    .line 59
    const-string/jumbo v1, "value"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setSpo2(F)V

    .line 60
    const-string v1, "heart_rate"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setHeartrate(F)V

    .line 61
    const-string/jumbo v1, "start_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setStartTime(J)V

    .line 62
    const-string v1, "create_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setCreateTime(J)V

    .line 63
    const-string v1, "comment"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setComment(Ljava/lang/String;)V

    .line 65
    :cond_0
    return-object v0
.end method

.method private getSpO2Datas(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/spo2/data/SpO2Data;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 69
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v1, "spo2Datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/spo2/data/SpO2Data;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 71
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 72
    new-instance v0, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;-><init>()V

    .line 73
    .local v0, "spo2Data":Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    const-string v2, "_id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setId(J)V

    .line 74
    const-string/jumbo v2, "user_device__id"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setUserDeviceId(Ljava/lang/String;)V

    .line 75
    const-string/jumbo v2, "start_time"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setStartTime(J)V

    .line 76
    const-string/jumbo v2, "value"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setSpo2(F)V

    .line 77
    const-string v2, "heart_rate"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setHeartrate(F)V

    .line 78
    const-string v2, "create_time"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setCreateTime(J)V

    .line 79
    const-string v2, "comment"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setComment(Ljava/lang/String;)V

    .line 81
    const-string v2, "AvgSpO2"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_0

    .line 82
    const-string v2, "AvgSpO2"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setAvgSpO2(D)V

    .line 84
    :cond_0
    const-string v2, "AvgPulse"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v4, :cond_1

    .line 85
    const-string v2, "AvgPulse"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setAvgHeartrate(D)V

    .line 88
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 89
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 92
    .end local v0    # "spo2Data":Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    :cond_2
    return-object v1
.end method


# virtual methods
.method public deleteAllData()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 144
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v2, "warning_checked_spo2"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 145
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public deleteDailyDataByTime(J)Z
    .locals 6
    .param p1, "timeMillis"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 165
    const-string v0, "create_time>? AND create_time<?"

    .line 166
    .local v0, "selection":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    .line 167
    .local v1, "selectionArgs":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v0, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public deleteLatestData(J)I
    .locals 4
    .param p1, "timeMills"    # J

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->latestEntry:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->latestEntry:Landroid/net/Uri;

    const-string v2, "0==0"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 158
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteRowById(Ljava/lang/String;)Z
    .locals 6
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 150
    const-string v0, "_id=?"

    .line 151
    .local v0, "selection":Ljava/lang/String;
    new-array v1, v2, [Ljava/lang/String;

    aput-object p1, v1, v3

    .line 152
    .local v1, "selectionArgs":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v0, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_0

    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public executeQuery(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "constraint"    # Ljava/lang/String;

    .prologue
    .line 96
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, " AVG (value) AS AvgSpO2"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, " AVG (heart_rate) AS AvgPulse"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "create_time"

    aput-object v1, v2, v0

    .line 97
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") GROUP BY (strftime(\"%m-%Y\",(["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "pulse_oximeter"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "].["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "create_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]/1000),\'unixepoch\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 98
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, "start_time DESC"

    .line 99
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getCalendarDatas(JJ)Ljava/util/ArrayList;
    .locals 10
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/spo2/data/SpO2Data;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 353
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const-string v0, "create_time"

    aput-object v0, v2, v5

    .line 354
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "create_time>? AND create_time<?"

    .line 355
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 356
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 359
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 360
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 361
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 362
    .local v7, "spO2Datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/spo2/data/SpO2Data;>;"
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 363
    new-instance v8, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    invoke-direct {v8}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;-><init>()V

    .line 364
    .local v8, "spo2Data":Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setId(J)V

    .line 365
    const-string v0, "create_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->setStartTime(J)V

    .line 367
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 368
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 372
    .end local v7    # "spO2Datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/spo2/data/SpO2Data;>;"
    .end local v8    # "spo2Data":Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 373
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 372
    .restart local v7    # "spO2Datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/spo2/data/SpO2Data;>;"
    :cond_1
    if-eqz v6, :cond_2

    .line 373
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v7
.end method

.method public getChildrenCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getChildQuery(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 243
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "*"

    aput-object v0, v2, v1

    .line 244
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "_id=?"

    .line 245
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v5, [Ljava/lang/String;

    aput-object p1, v4, v1

    .line 248
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 250
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 251
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 252
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getSpO2Data(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 254
    if-eqz v6, :cond_0

    .line 255
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 254
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 255
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public getDataCount()I
    .locals 7

    .prologue
    .line 340
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 341
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 343
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 344
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 346
    if-eqz v6, :cond_0

    .line 347
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    .line 346
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 347
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public getLastTwoDataByTime(J)Ljava/util/ArrayList;
    .locals 8
    .param p1, "timeMills"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/spo2/data/SpO2Data;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 380
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "*"

    aput-object v0, v2, v7

    .line 381
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "create_time<?"

    .line 382
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 383
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v5, "create_time DESC LIMIT 2"

    .line 384
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 387
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 389
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getSpO2Datas(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 391
    if-eqz v6, :cond_0

    .line 392
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 391
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 392
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public getNextDateOfData(J)Ljava/util/Date;
    .locals 13
    .param p1, "selectedTime"    # J

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 211
    new-array v2, v12, [Ljava/lang/String;

    const-string v0, "create_time"

    aput-object v0, v2, v11

    .line 212
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "create_time>? AND start_time<?"

    .line 213
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v11

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v12

    .line 214
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v5, "create_time ASC LIMIT 1"

    .line 216
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 218
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 219
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 221
    .local v9, "todayTime":J
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 223
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 224
    .local v7, "searchTime":J
    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v11

    cmp-long v0, v0, v11

    if-gez v0, :cond_1

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v11

    cmp-long v0, v0, v11

    if-lez v0, :cond_1

    .line 225
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v9, v10}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    if-eqz v6, :cond_0

    .line 236
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "searchTime":J
    :cond_0
    :goto_0
    return-object v0

    .line 227
    .restart local v7    # "searchTime":J
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v7, v8}, Ljava/util/Date;-><init>(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    if-eqz v6, :cond_0

    .line 236
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 229
    .end local v7    # "searchTime":J
    :cond_2
    :try_start_2
    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v11

    cmp-long v0, v0, v11

    if-gez v0, :cond_3

    .line 230
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v9, v10}, Ljava/util/Date;-><init>(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 235
    if-eqz v6, :cond_0

    .line 236
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 232
    :cond_3
    const/4 v0, 0x0

    .line 235
    if-eqz v6, :cond_0

    .line 236
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 235
    .end local v9    # "todayTime":J
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 236
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getPreviousDateOfData(J)Ljava/util/Date;
    .locals 13
    .param p1, "selectedTime"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v11, 0x0

    .line 179
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "create_time"

    aput-object v0, v2, v11

    .line 180
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "create_time<?"

    .line 181
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v11

    .line 182
    .local v4, "selectionArgs":[Ljava/lang/String;
    const-string v5, "create_time DESC LIMIT 1"

    .line 184
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 186
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 187
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 189
    .local v9, "todayTime":J
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 191
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 192
    .local v7, "searchTime":J
    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v11

    cmp-long v0, v0, v11

    if-gez v0, :cond_1

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v11

    cmp-long v0, v0, v11

    if-lez v0, :cond_1

    .line 193
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v9, v10}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    if-eqz v6, :cond_0

    .line 204
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v7    # "searchTime":J
    :cond_0
    :goto_0
    return-object v0

    .line 195
    .restart local v7    # "searchTime":J
    :cond_1
    :try_start_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v7, v8}, Ljava/util/Date;-><init>(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    if-eqz v6, :cond_0

    .line 204
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 197
    .end local v7    # "searchTime":J
    :cond_2
    :try_start_2
    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v11

    cmp-long v0, v0, v11

    if-gez v0, :cond_3

    .line 198
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v9, v10}, Ljava/util/Date;-><init>(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 203
    if-eqz v6, :cond_0

    .line 204
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 200
    :cond_3
    const/4 v0, 0x0

    .line 203
    if-eqz v6, :cond_0

    .line 204
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 203
    .end local v9    # "todayTime":J
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 204
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public getShareData(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 261
    .local p1, "checkedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_4

    .line 262
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    .line 263
    .local v8, "selection":Ljava/lang/StringBuffer;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 264
    .local v9, "size":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v9, :cond_1

    .line 265
    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    add-int/lit8 v0, v9, -0x1

    if-ne v7, v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 264
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 265
    :cond_0
    const-string v0, ","

    goto :goto_1

    .line 268
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT * FROM pulse_oximeter WHERE _id IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "create_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 277
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 279
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 280
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getShareData(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 282
    if-eqz v6, :cond_2

    .line 283
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 287
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "i":I
    .end local v8    # "selection":Ljava/lang/StringBuffer;
    .end local v9    # "size":I
    :cond_2
    :goto_2
    return-object v0

    .line 282
    .restart local v3    # "query":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "i":I
    .restart local v8    # "selection":Ljava/lang/StringBuffer;
    .restart local v9    # "size":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 283
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 287
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "i":I
    .end local v8    # "selection":Ljava/lang/StringBuffer;
    .end local v9    # "size":I
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    const v2, 0x7f090240

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    const v2, 0x7f090d14

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    const v2, 0x7f090f98

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public getSpO2GraphDatasByType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/spo2/data/SpO2Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_1

    const-string v6, "STRFTIME(\"%d-%m-%Y\", C.[create_time]/1000, \'unixepoch\', \'localtime\')"

    .line 307
    .local v6, "condition1":Ljava/lang/String;
    :goto_0
    const-string v7, "0==0"

    .line 308
    .local v7, "condition2":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT A.*, B.AvgSpO2, B.AvgPulse FROM (SELECT C.[_id], MAX(C.[create_time]) AS MaxCreateTime, AVG(C.[value]) AS AvgSpO2, AVG(C.[heart_rate]) AS AvgPulse FROM pulse_oximeter AS C GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS B LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "pulse_oximeter"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " A ON B.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MaxCreateTime"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] = A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "create_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] ORDER BY A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "create_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 328
    .local v3, "query":Ljava/lang/String;
    const/4 v8, 0x0

    .line 330
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 331
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getSpO2Datas(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 333
    if-eqz v8, :cond_0

    .line 334
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 306
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "condition1":Ljava/lang/String;
    .end local v7    # "condition2":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    const-string v6, "STRFTIME(\"%m-%Y\", C.[create_time]/1000, \'unixepoch\', \'localtime\')"

    goto/16 :goto_0

    :cond_2
    const-string v6, "(C.[create_time]/(1000 * 60 *5))"

    goto/16 :goto_0

    .line 333
    .restart local v3    # "query":Ljava/lang/String;
    .restart local v6    # "condition1":Ljava/lang/String;
    .restart local v7    # "condition2":Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 334
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public insertData(FF)V
    .locals 5
    .param p1, "spo2"    # F
    .param p2, "bpm"    # F

    .prologue
    .line 41
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 42
    .local v1, "values":Landroid/content/ContentValues;
    const-string/jumbo v2, "value"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 43
    const-string v2, "heart_rate"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 44
    const-string/jumbo v2, "start_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 45
    const-string v2, "end_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 47
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->latestEntry:Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    return-void

    .line 48
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->printStackTrace()V

    goto :goto_0
.end method

.method public updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 171
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 172
    .local v2, "values":Landroid/content/ContentValues;
    const-string v5, "comment"

    invoke-virtual {v2, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v0, "_id=?"

    .line 174
    .local v0, "selection":Ljava/lang/String;
    new-array v1, v3, [Ljava/lang/String;

    aput-object p1, v1, v4

    .line 175
    .local v1, "selectionArgs":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$PulseOximeter;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v2, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method
