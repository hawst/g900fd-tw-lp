.class Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;
.super Ljava/lang/Object;
.source "SelectGoalMissionActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllTouch:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    if-eqz p2, :cond_1

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->setSelectAll()V

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateActionBar(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;I)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllTouch:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$102(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;Z)Z

    .line 123
    :cond_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->setUnSelectAll()V

    goto :goto_0
.end method
