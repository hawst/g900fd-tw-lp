.class public Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HelpSleepActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f091043

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 29
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const v0, 0x7f03020f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->setContentView(I)V

    .line 19
    return-void
.end method

.method protected onResume()V
    .locals 12

    .prologue
    const v11, 0x7f08093b

    const/16 v10, 0x8

    const/4 v9, 0x4

    const/4 v8, 0x0

    .line 34
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 35
    const v6, 0x7f0808f9

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 36
    .local v1, "hideWithRTL1":Landroid/view/View;
    const v6, 0x7f0808fd

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 37
    .local v2, "hideWithRTL2":Landroid/view/View;
    const v6, 0x7f0808fb

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 38
    .local v4, "showWithRTL1":Landroid/view/View;
    const v6, 0x7f0808ff

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 40
    .local v5, "showWithRTL2":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 41
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 43
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 44
    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    .line 45
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 46
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 47
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 48
    .local v3, "lllp":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v6, 0x5

    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 49
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/help/activity/HelpSleepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    .end local v3    # "lllp":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 54
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 55
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 56
    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
