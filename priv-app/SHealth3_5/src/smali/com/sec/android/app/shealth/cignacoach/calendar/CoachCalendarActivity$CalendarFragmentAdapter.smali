.class Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "CoachCalendarActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CalendarFragmentAdapter"
.end annotation


# instance fields
.field private final mStartPeriodList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final periodType:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;Landroid/support/v4/app/FragmentManager;Ljava/util/List;I)V
    .locals 0
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p4, "periodType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentManager;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 664
    .local p3, "mStartPeriodList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    .line 665
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 666
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->mStartPeriodList:Ljava/util/List;

    .line 667
    iput p4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->periodType:I

    .line 668
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mKeyboardLeftRight:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->access$900(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    const/4 v0, -0x2

    .line 683
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 13
    .param p1, "index"    # I

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->mStartPeriodList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->periodType:I

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mSelectedTime:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mDataType:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->access$400(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupStartTime:J
    invoke-static {v7}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)J

    move-result-wide v7

    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupEndTime:J
    invoke-static {v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)J

    move-result-wide v9

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mBackupInputButtonType:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->access$700(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity$CalendarFragmentAdapter;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->mTimeColumnName:Ljava/lang/String;
    invoke-static {v12}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->access$800(Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {v0 .. v12}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getInstance(Landroid/content/Context;JIJLjava/lang/String;JJLjava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    move-result-object v0

    return-object v0
.end method
