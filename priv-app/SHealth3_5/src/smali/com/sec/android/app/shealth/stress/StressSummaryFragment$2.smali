.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "StressSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initSCover()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 4
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 317
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iput v3, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->percent:I

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStressDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getDataCount()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/stress/state/StressStateFinished;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    if-eqz v0, :cond_3

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mStateBar:Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/stress/custom/StressStateSummaryBar;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$500(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mThirdStartDiscard:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$600(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    const/16 v1, -0x3e9

    const/16 v2, -0x7d1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateNextState(II)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 337
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->startDialogAnimation()V

    .line 359
    :cond_2
    :goto_1
    return-void

    .line 330
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    instance-of v0, v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureFailed;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    if-eqz v0, :cond_4

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressState;->reStartStates()V

    goto :goto_0

    .line 332
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$700(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->showErrorDialog(I)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mIsOnConfigChanged:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$702(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Z)Z

    goto :goto_0

    .line 342
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    if-nez v0, :cond_2

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mErrorDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$800(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->initDialogAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$900(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    .line 344
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->clearDataCollection()V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDeviceConnector:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->stopCountDownTimer()V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    if-eqz v0, :cond_7

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mState:Lcom/sec/android/app/shealth/stress/state/StressState;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/state/StressState;->terminateState()V

    .line 350
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->firstAnimationClear()V

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mScaleAnimator:Landroid/animation/ValueAnimator;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1102(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->measuringCircleAnimationStop(Z)V
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;Z)V

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mFirstStateLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1300(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIconBigLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$200(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mDataCollectedProgressPercent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method
