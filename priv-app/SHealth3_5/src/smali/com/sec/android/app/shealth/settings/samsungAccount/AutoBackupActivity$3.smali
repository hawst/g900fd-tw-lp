.class Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;
.super Ljava/lang/Object;
.source "AutoBackupActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mMigrationState:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$900(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mMigrationState:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$900(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1000(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAppID:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "MI01"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mDataMigrationHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1200(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->startLocalMigration()V

    .line 291
    :goto_0
    return-void

    .line 287
    :cond_0
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->doNormalInitialization(Landroid/content/Context;)Z

    .line 288
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setContentProviderState(Z)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->restoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1300(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->startRestoreProcess()V

    goto :goto_0
.end method
