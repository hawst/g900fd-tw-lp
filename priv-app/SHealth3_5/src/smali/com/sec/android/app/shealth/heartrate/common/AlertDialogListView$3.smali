.class Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;
.super Ljava/lang/Object;
.source "AlertDialogListView.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->showChooserDialog(ZLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 2
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 136
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$100(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    # getter for: Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mTagChooserDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->access$100(Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mDialogItemClickListener:Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/heartrate/common/DialogListItemClickListener;->onOkButtonClicked()V

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mActivity:Landroid/support/v4/app/FragmentActivity;

    instance-of v1, v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    if-eqz v1, :cond_1

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView$3;->this$0:Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;

    iget-object v0, v1, Lcom/sec/android/app/shealth/heartrate/common/AlertDialogListView;->mActivity:Landroid/support/v4/app/FragmentActivity;

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .line 142
    .local v0, "mHeartRateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->isDialogshown:Z

    .line 144
    .end local v0    # "mHeartRateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    :cond_1
    return-void
.end method
