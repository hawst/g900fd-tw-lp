.class public final Lcom/sec/android/app/shealth/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final ab_spinner_item_normal_text:I = 0x7f0700d7

.field public static final ab_spinner_item_selected_text:I = 0x7f0700d6

.field public static final accesory_device_status:I = 0x7f0700cf

.field public static final accesory_device_text:I = 0x7f070048

.field public static final accessory_connection_view_state_text_color:I = 0x7f070051

.field public static final aciton_bar_select_spn_shadow:I = 0x7f0700c9

.field public static final aciton_bar_select_spn_txt:I = 0x7f0700c8

.field public static final action_bar_text_color:I = 0x7f070098

.field public static final action_bar_text_shadow_color:I = 0x7f070047

.field public static final actionbar_spinner_item_selector:I = 0x7f070297

.field public static final actionbar_summary_div:I = 0x7f0700ce

.field public static final alert_dialog_title_dark_color:I = 0x7f070067

.field public static final alert_dialog_title_light_color:I = 0x7f070068

.field public static final alpha_a6ffffff:I = 0x7f0700fd

.field public static final alpha_bfffffff:I = 0x7f0700fc

.field public static final alpha_black:I = 0x7f0700fa

.field public static final alpha_black_ff:I = 0x7f0700fb

.field public static final alpha_ccffffff:I = 0x7f0700fe

.field public static final alpha_white:I = 0x7f0700f9

.field public static final app_widget_title_text_out_glow:I = 0x7f070128

.field public static final bg_chart_after_meal_line_color:I = 0x7f070023

.field public static final bg_chart_background_color:I = 0x7f070026

.field public static final bg_chart_bottom_x_axis_color:I = 0x7f070025

.field public static final bg_chart_fasting_line_color:I = 0x7f070022

.field public static final bg_chart_grid_color:I = 0x7f070027

.field public static final bg_chart_inform_area_pressure_color:I = 0x7f07002e

.field public static final bg_chart_inform_area_pulse_color:I = 0x7f07002f

.field public static final bg_chart_text_color:I = 0x7f070028

.field public static final bg_chart_top_x_axis_color:I = 0x7f070024

.field public static final bg_drawer_default_profile_picture:I = 0x7f0700d5

.field public static final black:I = 0x7f0700e3

.field public static final black_15:I = 0x7f0700f0

.field public static final black_20:I = 0x7f0700f8

.field public static final black_30:I = 0x7f0700f7

.field public static final black_40:I = 0x7f0700f6

.field public static final black_50:I = 0x7f0700f5

.field public static final black_80:I = 0x7f0700f4

.field public static final black_color:I = 0x7f070042

.field public static final black_with_shadow:I = 0x7f070099

.field public static final blue:I = 0x7f070101

.field public static final blue_lock:I = 0x7f0701c7

.field public static final bottom_button_font_color_selector:I = 0x7f070298

.field public static final bp_chart_value_marking_color:I = 0x7f07002b

.field public static final bp_graph_line_color:I = 0x7f07002c

.field public static final bp_graph_series_color_in_range:I = 0x7f070029

.field public static final bp_graph_series_fill_color_in_range:I = 0x7f07002a

.field public static final bp_graph_x_axis_color:I = 0x7f07002d

.field public static final bt_item_device:I = 0x7f0700d0

.field public static final calendar_bg_color:I = 0x7f0700a1

.field public static final calendar_black:I = 0x7f0700b9

.field public static final calendar_btn_this:I = 0x7f0700d1

.field public static final calendar_button_disabled:I = 0x7f0700ad

.field public static final calendar_button_enabled:I = 0x7f0700ac

.field public static final calendar_button_pressed:I = 0x7f0700af

.field public static final calendar_button_text_sunday:I = 0x7f0700a6

.field public static final calendar_day_color:I = 0x7f0700bd

.field public static final calendar_day_selected:I = 0x7f0700be

.field public static final calendar_day_text_selector:I = 0x7f070299

.field public static final calendar_day_today:I = 0x7f0700bf

.field public static final calendar_days_of_week_container_background_color:I = 0x7f07009a

.field public static final calendar_grey:I = 0x7f0700bc

.field public static final calendar_hdivider_color:I = 0x7f0700b6

.field public static final calendar_month_text_color:I = 0x7f0700b7

.field public static final calendar_pager_container_background_color:I = 0x7f07009c

.field public static final calendar_period_switches_container_background_color:I = 0x7f07009b

.field public static final calendar_selected_day:I = 0x7f0700ae

.field public static final calendar_text_day:I = 0x7f0700a4

.field public static final calendar_text_day_dim:I = 0x7f0700a8

.field public static final calendar_text_selected:I = 0x7f0700a3

.field public static final calendar_text_selected_day:I = 0x7f0700a7

.field public static final calendar_text_sunday:I = 0x7f0700a5

.field public static final calendar_text_sunday_dim:I = 0x7f0700a9

.field public static final calendar_text_today:I = 0x7f0700a2

.field public static final calendar_today_monthdate_color:I = 0x7f0700b8

.field public static final calendar_today_pressed:I = 0x7f0700ba

.field public static final calendar_today_text_selector:I = 0x7f07029a

.field public static final calendar_top_ampm:I = 0x7f0700bb

.field public static final calendar_underline_abnormal:I = 0x7f0700ab

.field public static final calendar_underline_normal:I = 0x7f0700aa

.field public static final cancel_ok_button_text_color:I = 0x7f070041

.field public static final cancel_ok_button_text_selector:I = 0x7f07029b

.field public static final cancel_ok_green_text_color:I = 0x7f070064

.field public static final cigna_about_text_color:I = 0x7f070140

.field public static final cigna_assessment_next_text_color_selector:I = 0x7f07029c

.field public static final cigna_assessment_spinner_select_color:I = 0x7f070132

.field public static final cigna_bg_color:I = 0x7f070129

.field public static final cigna_bg_color_white:I = 0x7f07012b

.field public static final cigna_bg_color_yellow:I = 0x7f07012a

.field public static final cigna_bottom_bar_divider:I = 0x7f070137

.field public static final cigna_category_header_color:I = 0x7f07013a

.field public static final cigna_complete_header_marvel_txt_color:I = 0x7f070143

.field public static final cigna_current_mission_detail_day_text_color:I = 0x7f070136

.field public static final cigna_custom_viewpager_indicator_number_line_color:I = 0x7f07013c

.field public static final cigna_details_green_text_color:I = 0x7f07012c

.field public static final cigna_edit_text_opcity_selector:I = 0x7f07029d

.field public static final cigna_header_divider_bg_color:I = 0x7f07013d

.field public static final cigna_help_bg_color:I = 0x7f070141

.field public static final cigna_help_text_green_color:I = 0x7f070142

.field public static final cigna_intro_bg_color_white:I = 0x7f0700e2

.field public static final cigna_library_base_list_item_txt_subInfomation_textColor:I = 0x7f070145

.field public static final cigna_library_home_article_tip_txt_title_background:I = 0x7f070147

.field public static final cigna_library_home_article_tip_txt_title_textColor:I = 0x7f070146

.field public static final cigna_lifestyle_color_red:I = 0x7f07012f

.field public static final cigna_lifestyle_color_yellow:I = 0x7f070130

.field public static final cigna_lifestyle_multi_current_score_text_color:I = 0x7f070135

.field public static final cigna_lifestyle_multi_score_text_color:I = 0x7f070134

.field public static final cigna_lifestyle_scoreview_txt_color:I = 0x7f070133

.field public static final cigna_list_divider:I = 0x7f070138

.field public static final cigna_mission_complete_header_txt_exclamations_text_color:I = 0x7f070144

.field public static final cigna_no_goal_text:I = 0x7f070139

.field public static final cigna_score_txt_color_lifestyle:I = 0x7f07013f

.field public static final cigna_suggest_detail_goal_view_bg_color:I = 0x7f070131

.field public static final cigna_text_color_black:I = 0x7f0700e6

.field public static final cigna_text_color_blue:I = 0x7f07012d

.field public static final cigna_text_color_green:I = 0x7f07012e

.field public static final cigna_tw_background_light:I = 0x7f07013b

.field public static final cigna_two_info_header_bg_color:I = 0x7f07013e

.field public static final color_325a17:I = 0x7f07010d

.field public static final color_39424a:I = 0x7f070262

.field public static final color_3f9204:I = 0x7f07010b

.field public static final color_4c4c4a:I = 0x7f0700c0

.field public static final color_52b20e:I = 0x7f07010c

.field public static final color_545454:I = 0x7f07010a

.field public static final color_6b726b:I = 0x7f070109

.field public static final color_6f8a0d:I = 0x7f07010e

.field public static final color_8a8a8a:I = 0x7f070108

.field public static final color_e2ebce:I = 0x7f070120

.field public static final color_e57171:I = 0x7f070110

.field public static final color_green:I = 0x7f0700ec

.field public static final color_green_prog:I = 0x7f0700ed

.field public static final common_action_bar_splitter:I = 0x7f070009

.field public static final common_btn_color:I = 0x7f07029e

.field public static final common_signin_btn_dark_text_default:I = 0x7f070000

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f070002

.field public static final common_signin_btn_dark_text_focused:I = 0x7f070003

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f070001

.field public static final common_signin_btn_default_background:I = 0x7f070008

.field public static final common_signin_btn_light_text_default:I = 0x7f070004

.field public static final common_signin_btn_light_text_disabled:I = 0x7f070006

.field public static final common_signin_btn_light_text_focused:I = 0x7f070007

.field public static final common_signin_btn_light_text_pressed:I = 0x7f070005

.field public static final common_signin_btn_text_dark:I = 0x7f07029f

.field public static final common_signin_btn_text_light:I = 0x7f0702a0

.field public static final common_spn_shadow:I = 0x7f0700c5

.field public static final common_spn_txt:I = 0x7f0702a1

.field public static final common_wgt_txt_color:I = 0x7f0702a2

.field public static final compatible_accessories_bgcolor:I = 0x7f0700ee

.field public static final compatible_accessories_text_bgcolor:I = 0x7f0700ef

.field public static final compatible_device_text:I = 0x7f0700d2

.field public static final connectivity_scanning_header_text:I = 0x7f0700d3

.field public static final cursor_color:I = 0x7f07007e

.field public static final cyan:I = 0x7f070043

.field public static final dark_gray:I = 0x7f070106

.field public static final dark_gray1:I = 0x7f070107

.field public static final dark_green:I = 0x7f070083

.field public static final date_selector_bg_color:I = 0x7f0700cd

.field public static final day_color:I = 0x7f0700b0

.field public static final day_progress_background:I = 0x7f070221

.field public static final day_progress_motionless:I = 0x7f070222

.field public static final default_background_color:I = 0x7f07004f

.field public static final default_text_color:I = 0x7f07004e

.field public static final default_window_background_color:I = 0x7f070080

.field public static final dialog_item_content_text:I = 0x7f070040

.field public static final dialog_item_text:I = 0x7f07004c

.field public static final dialog_secondary_text:I = 0x7f07004d

.field public static final dialog_title:I = 0x7f07004b

.field public static final dialog_title_background:I = 0x7f0701c9

.field public static final dialog_top_text_color:I = 0x7f070065

.field public static final dosage_status_bar_missed:I = 0x7f070121

.field public static final dosage_status_bar_not_eaten:I = 0x7f070122

.field public static final drawer_item_selector:I = 0x7f0702a3

.field public static final drawer_menu_header_text:I = 0x7f0700d4

.field public static final drawer_menu_header_text1:I = 0x7f070096

.field public static final drawer_menu_selected_text_color:I = 0x7f070090

.field public static final edit_text_cursor_color:I = 0x7f0701d2

.field public static final edit_text_hint_color:I = 0x7f0701d3

.field public static final ex_spinner_selector_color:I = 0x7f07016b

.field public static final expandable_title_text_color:I = 0x7f070039

.field public static final focus_color:I = 0x7f0700e8

.field public static final food_barcode_search_black_transparent_color:I = 0x7f070202

.field public static final food_breakfast_diagram_color:I = 0x7f07020c

.field public static final food_category_fragment_caption_text_color:I = 0x7f0701e9

.field public static final food_category_title_bottom_color:I = 0x7f0701de

.field public static final food_category_title_color_selector:I = 0x7f0702a4

.field public static final food_chart_bottom_x_axis_color:I = 0x7f07001a

.field public static final food_chart_breakfast_item_color:I = 0x7f07001c

.field public static final food_chart_dinner_item_color:I = 0x7f07001e

.field public static final food_chart_line_color:I = 0x7f070020

.field public static final food_chart_lunch_item_color:I = 0x7f07001d

.field public static final food_chart_other_item_color:I = 0x7f07001f

.field public static final food_chart_text_color:I = 0x7f070021

.field public static final food_chart_value_marking_color:I = 0x7f07001b

.field public static final food_chart_x_axis_color:I = 0x7f070019

.field public static final food_checked_button_default_text_color:I = 0x7f0701d5

.field public static final food_color_1e1e1e:I = 0x7f0701d8

.field public static final food_color_2a400e:I = 0x7f070214

.field public static final food_details_head_green_text_color:I = 0x7f0701f8

.field public static final food_details_head_text_color:I = 0x7f0701f9

.field public static final food_dinner_diagram_color:I = 0x7f07020e

.field public static final food_goal_line:I = 0x7f070211

.field public static final food_graph_information_area_amount_view_text_color:I = 0x7f070210

.field public static final food_graph_information_date_text_color:I = 0x7f070212

.field public static final food_graph_information_sum_text_color:I = 0x7f070213

.field public static final food_health_activity_back_ground_color:I = 0x7f0701d4

.field public static final food_health_care_input_background:I = 0x7f0701d7

.field public static final food_list_category_title_selected_text_color:I = 0x7f0701eb

.field public static final food_list_category_title_selected_text_shadow_color:I = 0x7f0701ec

.field public static final food_list_category_title_text_color:I = 0x7f0701ea

.field public static final food_list_item_calories_field_color:I = 0x7f0701ee

.field public static final food_list_item_category_caption_color:I = 0x7f0701ef

.field public static final food_list_item_highlight_color:I = 0x7f0701d1

.field public static final food_list_title_color:I = 0x7f0701e8

.field public static final food_list_title_shadow_color:I = 0x7f0701ed

.field public static final food_lunch_diagram_color:I = 0x7f07020d

.field public static final food_meal_header_text_color:I = 0x7f0701d6

.field public static final food_meal_input_add_button_background_color:I = 0x7f0701db

.field public static final food_meal_input_total_calories_background_color:I = 0x7f0701da

.field public static final food_meal_input_total_calories_text_color:I = 0x7f0701d9

.field public static final food_meal_item_sub_text_color_selector:I = 0x7f0702a5

.field public static final food_meal_item_text_color_selector:I = 0x7f0702a6

.field public static final food_meal_plan_date_btn_color:I = 0x7f07020a

.field public static final food_meal_plan_period_date_color:I = 0x7f07020b

.field public static final food_nutrition_list_background_color:I = 0x7f0701fc

.field public static final food_nutrition_split_line_grey:I = 0x7f0701fa

.field public static final food_nutrition_split_line_light_grey:I = 0x7f0701fb

.field public static final food_other_diagram_color:I = 0x7f07020f

.field public static final food_pick_bg_color:I = 0x7f0701dc

.field public static final food_pick_calories_color:I = 0x7f0701e4

.field public static final food_pick_category_line_bg_color:I = 0x7f0701dd

.field public static final food_pick_no_food_text_color:I = 0x7f0701e3

.field public static final food_pick_no_items_color:I = 0x7f0701e5

.field public static final food_pick_search_edit_text_hint_text_color_selector:I = 0x7f0702a7

.field public static final food_pick_search_hint_text_color:I = 0x7f0701e1

.field public static final food_pick_search_hint_text_color_dim:I = 0x7f0701e2

.field public static final food_pick_search_text_color:I = 0x7f0701e0

.field public static final food_pick_sub_tab_text_color:I = 0x7f0701df

.field public static final food_portion_size_input_screen_unit_color:I = 0x7f070209

.field public static final food_portion_size_slider_handler_up_view_color:I = 0x7f070207

.field public static final food_portion_size_slider_portion_number_view_color:I = 0x7f070208

.field public static final food_portion_size_slider_view_1_gradient_color:I = 0x7f070203

.field public static final food_portion_size_slider_view_2_gradient_color:I = 0x7f070204

.field public static final food_portion_size_slider_view_3_gradient_color:I = 0x7f070205

.field public static final food_portion_size_slider_view_4_gradient_color:I = 0x7f070206

.field public static final food_selected_panel_divider_color:I = 0x7f0701e6

.field public static final food_selected_panel_item_text_color:I = 0x7f0701e7

.field public static final food_selector_divider_color:I = 0x7f0701fd

.field public static final food_selector_text:I = 0x7f0701fe

.field public static final food_set_goal_input_module_title_unit_text_color:I = 0x7f0701ff

.field public static final food_sub_tab_text_color_selector:I = 0x7f0702a8

.field public static final food_summary_bottom_button_text_selector:I = 0x7f0702a9

.field public static final food_text_general:I = 0x7f0701f0

.field public static final food_title_text_of_component_color:I = 0x7f0701d0

.field public static final food_tracker_bottom_button_text_default_color:I = 0x7f0701f6

.field public static final food_tracker_bottom_button_text_selected_color:I = 0x7f0701f7

.field public static final food_tracker_bottom_container_background_color:I = 0x7f0701f2

.field public static final food_tracker_calorie_text_abnormal:I = 0x7f0701f5

.field public static final food_tracker_calorie_text_normal:I = 0x7f0701f4

.field public static final food_tracker_progress_color:I = 0x7f0701f3

.field public static final food_tracker_top_container_background_color:I = 0x7f0701f1

.field public static final food_voice_search_dialog_title_text_color:I = 0x7f070200

.field public static final food_voice_search_language_text_color:I = 0x7f070201

.field public static final goal_line_text_color:I = 0x7f07021c

.field public static final gold:I = 0x7f070104

.field public static final graph_anime_handler_text:I = 0x7f0700d9

.field public static final graph_bg_color:I = 0x7f0700d8

.field public static final graph_black:I = 0x7f07008c

.field public static final graph_bubble_text:I = 0x7f0700da

.field public static final graph_bubble_value_text:I = 0x7f0700db

.field public static final graph_chart_goal_line_color:I = 0x7f070037

.field public static final graph_chart_goal_text_color:I = 0x7f070038

.field public static final graph_chart_line_darkgreen_color:I = 0x7f070031

.field public static final graph_chart_line_out_normal_range_color:I = 0x7f070032

.field public static final graph_chart_line_yellowgreen_color:I = 0x7f070030

.field public static final graph_chart_normal_range_color:I = 0x7f070033

.field public static final graph_chart_separator_color:I = 0x7f070035

.field public static final graph_chart_text_color:I = 0x7f070036

.field public static final graph_chart_x_axis_color:I = 0x7f070034

.field public static final graph_color:I = 0x7f070089

.field public static final graph_dark_grey:I = 0x7f070093

.field public static final graph_grey:I = 0x7f070088

.field public static final graph_information_area_subtitle_text_color:I = 0x7f070017

.field public static final graph_information_date_text_color:I = 0x7f070018

.field public static final graph_information_nodata:I = 0x7f0700c7

.field public static final graph_legend_background_color:I = 0x7f070092

.field public static final graph_medium_grey:I = 0x7f070087

.field public static final graph_no_data_text_color:I = 0x7f0700dc

.field public static final graph_sleepmonitor_bar_good_color:I = 0x7f07021a

.field public static final graph_sleepmonitor_bar_normal_color:I = 0x7f070219

.field public static final graph_transparent:I = 0x7f07008a

.field public static final graph_weight_line_color:I = 0x7f07008b

.field public static final graph_yellow:I = 0x7f070081

.field public static final gray:I = 0x7f070105

.field public static final grey_text:I = 0x7f07009e

.field public static final health_care_input_background:I = 0x7f07006d

.field public static final health_care_summary_bg_dark:I = 0x7f07006c

.field public static final help_moreinfo_text_color:I = 0x7f0701cf

.field public static final help_moreinfo_title_color:I = 0x7f0701ce

.field public static final help_normal_text_color:I = 0x7f0701cd

.field public static final highlight_color:I = 0x7f070217

.field public static final holo_orange_light:I = 0x7f0700c4

.field public static final home_shortcut_pressed:I = 0x7f0700e1

.field public static final horizontal_axes_color:I = 0x7f07008d

.field public static final hrm_activity_background:I = 0x7f070194

.field public static final hrm_common_green_text:I = 0x7f07017d

.field public static final hrm_dialog_fail_background:I = 0x7f070184

.field public static final hrm_dialog_guide_background:I = 0x7f070183

.field public static final hrm_graph_average_value:I = 0x7f070198

.field public static final hrm_graph_line_color:I = 0x7f070185

.field public static final hrm_graph_text:I = 0x7f070187

.field public static final hrm_green_pulse:I = 0x7f070188

.field public static final hrm_green_pulse_1:I = 0x7f07018a

.field public static final hrm_green_pulse_2:I = 0x7f07018b

.field public static final hrm_green_pulse_3:I = 0x7f07018c

.field public static final hrm_green_pulse_4:I = 0x7f07018d

.field public static final hrm_green_pulse_5:I = 0x7f07018e

.field public static final hrm_list_body_sub_green_text:I = 0x7f070181

.field public static final hrm_list_index_left_green_text:I = 0x7f07017f

.field public static final hrm_list_index_right_green_text:I = 0x7f070180

.field public static final hrm_log_detail_accessory:I = 0x7f070186

.field public static final hrm_log_group_green_text:I = 0x7f07017e

.field public static final hrm_orange_pulse:I = 0x7f070189

.field public static final hrm_orange_pulse_1:I = 0x7f07018f

.field public static final hrm_orange_pulse_2:I = 0x7f070190

.field public static final hrm_orange_pulse_3:I = 0x7f070191

.field public static final hrm_orange_pulse_4:I = 0x7f070192

.field public static final hrm_orange_pulse_5:I = 0x7f070193

.field public static final hrm_summary_gradient_first:I = 0x7f070195

.field public static final hrm_summary_gradient_second:I = 0x7f070196

.field public static final hrm_summary_gradient_third:I = 0x7f070197

.field public static final hrm_summary_measure_fail:I = 0x7f070182

.field public static final information_area_color:I = 0x7f07021b

.field public static final information_bg_color:I = 0x7f0700e9

.field public static final input_activity_date_btn_txt:I = 0x7f0700cb

.field public static final input_activity_date_divider:I = 0x7f0700cc

.field public static final input_field_normal_bottom_line:I = 0x7f070077

.field public static final input_field_ubnormal_bottom_line:I = 0x7f070078

.field public static final input_memo_header_text_color:I = 0x7f0700ca

.field public static final input_view_normal_color:I = 0x7f07006e

.field public static final input_view_systolic_input_color:I = 0x7f07006f

.field public static final inputmodule_abnormal_color:I = 0x7f070075

.field public static final inputmodule_normal_color:I = 0x7f070074

.field public static final item_color_1:I = 0x7f070111

.field public static final item_color_2:I = 0x7f070112

.field public static final legend_mark_text_color:I = 0x7f07003e

.field public static final light_green:I = 0x7f070103

.field public static final list_header:I = 0x7f0700e7

.field public static final list_popup_item_default_text_color:I = 0x7f07007a

.field public static final list_popup_item_selected_text_color:I = 0x7f070079

.field public static final list_text_color_selector:I = 0x7f0702aa

.field public static final log_list_group_header_background:I = 0x7f07000a

.field public static final log_list_item_background_color:I = 0x7f07003f

.field public static final log_list_row_color:I = 0x7f07000c

.field public static final log_list_row_right_text_color:I = 0x7f07003d

.field public static final log_list_small_text:I = 0x7f070215

.field public static final log_list_team_header_background:I = 0x7f07000b

.field public static final log_list_team_header_background_color:I = 0x7f07003c

.field public static final log_list_team_header_color_left:I = 0x7f07003a

.field public static final log_list_team_header_color_right:I = 0x7f07003b

.field public static final log_list_title_text:I = 0x7f070216

.field public static final log_no_data_message_text_color:I = 0x7f0700de

.field public static final log_no_data_text_color:I = 0x7f0700dd

.field public static final log_select_all:I = 0x7f070095

.field public static final menu_background:I = 0x7f070046

.field public static final menu_background_white:I = 0x7f07004a

.field public static final menu_selected:I = 0x7f070045

.field public static final month_progress_background:I = 0x7f070223

.field public static final month_progress_motionless:I = 0x7f070224

.field public static final no_connected_accessories_text:I = 0x7f070097

.field public static final no_food_nfo_color:I = 0x7f070086

.field public static final pedometer_background:I = 0x7f070124

.field public static final pedometer_status_bar_powerwalking:I = 0x7f070125

.field public static final pedometer_status_bar_running:I = 0x7f070126

.field public static final pedometer_status_bar_up_down:I = 0x7f070127

.field public static final pedometer_status_bar_walking:I = 0x7f070123

.field public static final picker_highlight:I = 0x7f07009f

.field public static final picker_text:I = 0x7f0700a0

.field public static final popup_btn_bg:I = 0x7f0700b3

.field public static final popup_title:I = 0x7f070049

.field public static final pro_exercise_pro_skip_text_color_selector:I = 0x7f0702ab

.field public static final pro_exercise_pro_time_goal_text_color_selector:I = 0x7f0702ac

.field public static final pro_exercise_pro_two_button_font_color_selector:I = 0x7f0702ad

.field public static final pro_photo_gallery_image_background_color_selector:I = 0x7f0702ae

.field public static final pro_realtime_list_font_color_selector:I = 0x7f0702af

.field public static final pro_realtime_two_button_font_color_selector:I = 0x7f0702b0

.field public static final pro_set_goal_edit_text_color:I = 0x7f0702b1

.field public static final profile_activity_level_color_normal:I = 0x7f070119

.field public static final profile_activity_level_color_select:I = 0x7f07011a

.field public static final profile_card_step_normal:I = 0x7f07011d

.field public static final profile_dim_text_color:I = 0x7f07011c

.field public static final profile_image_bg:I = 0x7f07011e

.field public static final profile_nor_text_color:I = 0x7f07011b

.field public static final progress_text_color:I = 0x7f07007f

.field public static final red:I = 0x7f07010f

.field public static final regular_background:I = 0x7f07008e

.field public static final regular_text_color:I = 0x7f07008f

.field public static final rubble_black:I = 0x7f070102

.field public static final s_health_setup_btn:I = 0x7f070082

.field public static final saturday_color:I = 0x7f0700b2

.field public static final scale_begin_color:I = 0x7f07021d

.field public static final scale_center_color:I = 0x7f07021e

.field public static final scale_end_color:I = 0x7f07021f

.field public static final scan_button_bottom_font_color_selector:I = 0x7f0702b2

.field public static final sleep_graph_motionless_color:I = 0x7f070225

.field public static final sleep_graph_movement_color:I = 0x7f070226

.field public static final spinner_item_normal_text:I = 0x7f07007c

.field public static final spinner_item_selected_text:I = 0x7f07007d

.field public static final spinner_selected_text:I = 0x7f0701c6

.field public static final spinner_text_color_selector:I = 0x7f0702b3

.field public static final split_line:I = 0x7f070091

.field public static final split_line_log_list:I = 0x7f0700b4

.field public static final spo2_activity_background:I = 0x7f070233

.field public static final spo2_color_first:I = 0x7f070248

.field public static final spo2_color_second:I = 0x7f070249

.field public static final spo2_color_third:I = 0x7f07024a

.field public static final spo2_common_bottom_btn:I = 0x7f070236

.field public static final spo2_dialog_guide_background:I = 0x7f070235

.field public static final spo2_gradient_fifth:I = 0x7f070246

.field public static final spo2_gradient_first:I = 0x7f070242

.field public static final spo2_gradient_fourth:I = 0x7f070245

.field public static final spo2_gradient_last:I = 0x7f070247

.field public static final spo2_gradient_second:I = 0x7f070243

.field public static final spo2_gradient_third:I = 0x7f070244

.field public static final spo2_graph_bpm_line_color:I = 0x7f07023b

.field public static final spo2_graph_header_color:I = 0x7f070239

.field public static final spo2_graph_spo2_line_color:I = 0x7f07023a

.field public static final spo2_graph_text:I = 0x7f070238

.field public static final spo2_green_pulse_1:I = 0x7f070227

.field public static final spo2_green_pulse_2:I = 0x7f070228

.field public static final spo2_green_pulse_3:I = 0x7f070229

.field public static final spo2_green_pulse_4:I = 0x7f07022a

.field public static final spo2_green_pulse_5:I = 0x7f07022b

.field public static final spo2_green_pulse_6:I = 0x7f07022c

.field public static final spo2_list_body_sub_green_text:I = 0x7f07023c

.field public static final spo2_list_index_left_green_text:I = 0x7f07023f

.field public static final spo2_list_index_right_green_text:I = 0x7f070240

.field public static final spo2_log_detail_accessory:I = 0x7f07023d

.field public static final spo2_log_group_green_text:I = 0x7f07023e

.field public static final spo2_orange_pulse_1:I = 0x7f07022d

.field public static final spo2_orange_pulse_2:I = 0x7f07022e

.field public static final spo2_orange_pulse_3:I = 0x7f07022f

.field public static final spo2_orange_pulse_4:I = 0x7f070230

.field public static final spo2_orange_pulse_5:I = 0x7f070231

.field public static final spo2_orange_pulse_6:I = 0x7f070232

.field public static final spo2_summary_button_text_dim:I = 0x7f070234

.field public static final spo2_summary_completed_text:I = 0x7f070241

.field public static final spo2_summary_third_bmp_unit_color:I = 0x7f070237

.field public static final stm_activity_background:I = 0x7f07024b

.field public static final stm_common_green_text:I = 0x7f07024c

.field public static final stm_dialog_fail_background:I = 0x7f070255

.field public static final stm_dialog_guide_background:I = 0x7f070254

.field public static final stm_gradient_fifth:I = 0x7f07025b

.field public static final stm_gradient_first:I = 0x7f070257

.field public static final stm_gradient_fourth:I = 0x7f07025a

.field public static final stm_gradient_last:I = 0x7f07025c

.field public static final stm_gradient_second:I = 0x7f070258

.field public static final stm_gradient_third:I = 0x7f070259

.field public static final stm_list_body_sub_green_text:I = 0x7f070250

.field public static final stm_list_index_left_green_text:I = 0x7f07024e

.field public static final stm_list_index_right_green_text:I = 0x7f07024f

.field public static final stm_log_detail_accessory:I = 0x7f070256

.field public static final stm_log_group_green_text:I = 0x7f07024d

.field public static final stm_state_average:I = 0x7f07025f

.field public static final stm_state_high:I = 0x7f070260

.field public static final stm_state_low:I = 0x7f07025e

.field public static final stm_state_very_high:I = 0x7f070261

.field public static final stm_state_very_low:I = 0x7f07025d

.field public static final stm_summary_button_text_dim:I = 0x7f070251

.field public static final stm_summary_completed_text:I = 0x7f070253

.field public static final stm_summary_measure_fail:I = 0x7f070252

.field public static final sub_tab_text_color_selector:I = 0x7f0702b4

.field public static final summary_animated_counter_bad_color:I = 0x7f070071

.field public static final summary_animated_counter_default_color:I = 0x7f070070

.field public static final summary_animated_counter_good_color:I = 0x7f070072

.field public static final summary_animated_counter_shadow_color:I = 0x7f070073

.field public static final summary_title_color:I = 0x7f070220

.field public static final summary_view_abnormal_color:I = 0x7f070069

.field public static final summary_view_background_color:I = 0x7f070055

.field public static final summary_view_balance_interval_color:I = 0x7f07006b

.field public static final summary_view_bottom_background_color:I = 0x7f070056

.field public static final summary_view_button_text_color:I = 0x7f070053

.field public static final summary_view_button_text_color_disabled:I = 0x7f070054

.field public static final summary_view_button_text_shadow_color:I = 0x7f070052

.field public static final summary_view_content_bar_state_and_info_text_color:I = 0x7f07005b

.field public static final summary_view_content_bar_title_color:I = 0x7f07005c

.field public static final summary_view_content_bar_value_text_balance_color:I = 0x7f070058

.field public static final summary_view_content_bar_value_text_no_data_color:I = 0x7f07005a

.field public static final summary_view_content_bar_value_text_unbalance_color:I = 0x7f070059

.field public static final summary_view_content_divider_color:I = 0x7f07005d

.field public static final summary_view_header_balance_state_text_color:I = 0x7f070050

.field public static final summary_view_health_care_background_color:I = 0x7f070057

.field public static final summary_view_normal_color:I = 0x7f07006a

.field public static final summary_view_progress_bar_aftermeal_column_unbalance_color:I = 0x7f070061

.field public static final summary_view_progress_bar_column_balance_color:I = 0x7f07005e

.field public static final summary_view_progress_bar_fasting_column_unbalance_color:I = 0x7f070060

.field public static final summary_view_progress_bar_label_color:I = 0x7f070063

.field public static final summary_view_progress_bar_slider_balance_color:I = 0x7f07005f

.field public static final summary_view_progress_bar_title_color:I = 0x7f070062

.field public static final sunday_color:I = 0x7f0700b1

.field public static final sunday_color_again:I = 0x7f0700b5

.field public static final text_care:I = 0x7f070094

.field public static final text_date_bar:I = 0x7f070084

.field public static final text_general:I = 0x7f070218

.field public static final text_regular:I = 0x7f07007b

.field public static final text_regular_50:I = 0x7f07009d

.field public static final text_view_item_color:I = 0x7f070066

.field public static final text_white:I = 0x7f070085

.field public static final tgh_graph_header_color:I = 0x7f070269

.field public static final tgh_graph_humidity_line_color:I = 0x7f07026b

.field public static final tgh_graph_line_color:I = 0x7f07026a

.field public static final tgh_graph_series_fill_color_in_range:I = 0x7f07026d

.field public static final tgh_graph_text:I = 0x7f070268

.field public static final tgh_state_average:I = 0x7f070265

.field public static final tgh_state_high:I = 0x7f070266

.field public static final tgh_state_low:I = 0x7f070264

.field public static final tgh_state_very_high:I = 0x7f070267

.field public static final tgh_state_very_low:I = 0x7f070263

.field public static final tgh_summary_button_text_dim:I = 0x7f07026c

.field public static final timeline_bg_color:I = 0x7f070113

.field public static final timeline_bg_green:I = 0x7f070114

.field public static final timeline_compare_bg:I = 0x7f070116

.field public static final timeline_compare_value:I = 0x7f070117

.field public static final timeline_map_text_color:I = 0x7f070118

.field public static final timeline_text_green:I = 0x7f070115

.field public static final transparent:I = 0x7f0700e0

.field public static final transprent_lock:I = 0x7f0701c8

.field public static final txt_contents_0_light:I = 0x7f0700c3

.field public static final txt_contents_1_light:I = 0x7f0700c1

.field public static final txt_contents_6_light:I = 0x7f0700c2

.field public static final txt_contents_disabled:I = 0x7f0700c6

.field public static final unit_settings_unit_color:I = 0x7f0700df

.field public static final url_color:I = 0x7f0701cc

.field public static final uv_spf_ready_text_color:I = 0x7f0701ca

.field public static final uv_summary_gradient_fifth:I = 0x7f070272

.field public static final uv_summary_gradient_first:I = 0x7f07026e

.field public static final uv_summary_gradient_fourth:I = 0x7f070271

.field public static final uv_summary_gradient_second:I = 0x7f07026f

.field public static final uv_summary_gradient_third:I = 0x7f070270

.field public static final vertical_progress_mark_color:I = 0x7f070076

.field public static final walk_heahlty_step_color:I = 0x7f070159

.field public static final walk_list_divider:I = 0x7f07028c

.field public static final walk_sitting_color:I = 0x7f07015a

.field public static final water_chart_point_text_color:I = 0x7f070011

.field public static final water_chart_popup_color:I = 0x7f070013

.field public static final water_chart_popup_stroke_color:I = 0x7f070014

.field public static final water_chart_text_color:I = 0x7f070010

.field public static final water_graph_line_color:I = 0x7f07000f

.field public static final water_graph_series_color_in_range:I = 0x7f07000e

.field public static final water_graph_tooltip_color:I = 0x7f070015

.field public static final water_graph_x_axis_color:I = 0x7f070012

.field public static final water_handler_line_color:I = 0x7f070016

.field public static final wearable_no_data_summary_text_color:I = 0x7f0701cb

.field public static final weight_graph_line_color:I = 0x7f07000d

.field public static final welcome_background_color:I = 0x7f07011f

.field public static final welcome_page_text_selector:I = 0x7f0702b5

.field public static final welcome_text_selector:I = 0x7f0702b6

.field public static final wgt_bottom_navigation_button_text_color_selector:I = 0x7f0702b7

.field public static final wgt_graph_information_area_weight_color:I = 0x7f0701bf

.field public static final wgt_input_screen_info_text_color:I = 0x7f0701b5

.field public static final wgt_input_screen_unit_color:I = 0x7f0701b3

.field public static final wgt_input_screen_weight_text_color:I = 0x7f0701b2

.field public static final wgt_list_text_color_selector:I = 0x7f0702b8

.field public static final wgt_set_goal_input_module_title_unit_text_color:I = 0x7f0701c2

.field public static final wgt_set_goal_progressbar_values_color:I = 0x7f0701c1

.field public static final wgt_set_goal_target_date_btn_txt:I = 0x7f0701c3

.field public static final wgt_set_goal_target_date_color:I = 0x7f0701c5

.field public static final wgt_set_goal_target_date_divider:I = 0x7f0701c4

.field public static final wgt_set_goal_view_add_goal_top_label_divider_color:I = 0x7f0701c0

.field public static final wgt_summary_view_indicator_view_bold_dial_color:I = 0x7f0701ba

.field public static final wgt_summary_view_indicator_view_dial_color:I = 0x7f0701b9

.field public static final wgt_summary_view_indicator_view_goal_line_color:I = 0x7f0701bb

.field public static final wgt_summary_view_indicator_view_normal_range_color:I = 0x7f0701bd

.field public static final wgt_summary_view_indicator_view_out_of_normal_range_color:I = 0x7f0701be

.field public static final wgt_summary_view_indicator_view_text_color:I = 0x7f0701bc

.field public static final wgt_summary_view_main_view_color:I = 0x7f0701b4

.field public static final wgt_summary_view_range_text_color:I = 0x7f0701b8

.field public static final wgt_summary_view_text_color_black:I = 0x7f0701b7

.field public static final wgt_summary_view_top_view_color:I = 0x7f0701b6

.field public static final white:I = 0x7f070044

.field public static final white_20:I = 0x7f0700f1

.field public static final white_40:I = 0x7f0700f2

.field public static final white_50:I = 0x7f0700f3

.field public static final white_dialog:I = 0x7f0700ff

.field public static final white_smoke:I = 0x7f070100

.field public static final workout_action_bar:I = 0x7f07019e

.field public static final workout_activity_background:I = 0x7f0701b1

.field public static final workout_alphabet_horizontal_line:I = 0x7f070152

.field public static final workout_alphabet_letter_green:I = 0x7f070155

.field public static final workout_app_widget_title_text_out_glow:I = 0x7f070277

.field public static final workout_black:I = 0x7f0700e4

.field public static final workout_black_00:I = 0x7f0700e5

.field public static final workout_black_alpha_40:I = 0x7f07016d

.field public static final workout_blue:I = 0x7f070282

.field public static final workout_bodyfat_line_color:I = 0x7f070294

.field public static final workout_bodyfat_ui_color:I = 0x7f070166

.field public static final workout_bpm_ui_color:I = 0x7f070178

.field public static final workout_color_000000:I = 0x7f070179

.field public static final workout_color_3f9204:I = 0x7f070296

.field public static final workout_color_4c4c4c:I = 0x7f07015f

.field public static final workout_color_696969:I = 0x7f070160

.field public static final workout_color_ffffff:I = 0x7f070292

.field public static final workout_date_color:I = 0x7f0700eb

.field public static final workout_details_green_text_color:I = 0x7f070149

.field public static final workout_everyone_ranking_dialog_icon_border_color:I = 0x7f07014e

.field public static final workout_everyone_ranking_dialog_item_separator_color:I = 0x7f07015c

.field public static final workout_everyone_ranking_dialog_item_text_size:I = 0x7f07014f

.field public static final workout_everyone_ranking_header_bottom_background:I = 0x7f070281

.field public static final workout_everyone_ranking_header_top_background:I = 0x7f07027f

.field public static final workout_everyone_ranking_list_item_delimiter_color:I = 0x7f07015b

.field public static final workout_everyone_ranking_list_item_icon_border_color:I = 0x7f07014d

.field public static final workout_exercise_pick_search_bg:I = 0x7f07016a

.field public static final workout_exercise_pro_datas_bg_color:I = 0x7f07016c

.field public static final workout_expandable_list:I = 0x7f070288

.field public static final workout_goal_line_text_color:I = 0x7f070162

.field public static final workout_graph_black:I = 0x7f07028f

.field public static final workout_graph_chart_goal_line_color:I = 0x7f0701a5

.field public static final workout_graph_climbing_bar_color:I = 0x7f0701a4

.field public static final workout_graph_elevation_color:I = 0x7f070168

.field public static final workout_graph_exercise_bar_color:I = 0x7f070158

.field public static final workout_graph_exercisemate_line_color:I = 0x7f070163

.field public static final workout_graph_grey:I = 0x7f07028d

.field public static final workout_graph_healthy_steps_pie_char_color:I = 0x7f0701a7

.field public static final workout_graph_healthy_steps_text_color:I = 0x7f0701a9

.field public static final workout_graph_heart_rate_bar:I = 0x7f070170

.field public static final workout_graph_list_view_background_color:I = 0x7f07027d

.field public static final workout_graph_medium_grey:I = 0x7f070291

.field public static final workout_graph_normal_steps_pie_chart_color:I = 0x7f0701a6

.field public static final workout_graph_running_bar_color:I = 0x7f0701a3

.field public static final workout_graph_speed_bar:I = 0x7f07016f

.field public static final workout_graph_star_climbing_color:I = 0x7f070273

.field public static final workout_graph_total_steps_text_color:I = 0x7f0701a8

.field public static final workout_graph_transparent:I = 0x7f07028e

.field public static final workout_graph_view_memo_title_text_color:I = 0x7f070161

.field public static final workout_graph_walking_bar_color:I = 0x7f0701a2

.field public static final workout_graph_walking_color:I = 0x7f070275

.field public static final workout_graph_walkingmate_line_color:I = 0x7f0701a1

.field public static final workout_graph_weight_line_color:I = 0x7f070171

.field public static final workout_gray:I = 0x7f070283

.field public static final workout_gray2:I = 0x7f070284

.field public static final workout_header_dark_green_background:I = 0x7f07014c

.field public static final workout_highlight_color:I = 0x7f07027a

.field public static final workout_horizontal_axes_color:I = 0x7f070290

.field public static final workout_image_bg_cyan:I = 0x7f07017a

.field public static final workout_information_area_color:I = 0x7f070167

.field public static final workout_initial_buttons_text_color:I = 0x7f070151

.field public static final workout_inputmodule_abnormal_color:I = 0x7f070279

.field public static final workout_inputmodule_normal_color:I = 0x7f070278

.field public static final workout_list_item_bg_selector:I = 0x7f0701ae

.field public static final workout_list_item_you_bg_selector:I = 0x7f0701af

.field public static final workout_list_popup_item_default_text_color:I = 0x7f07027c

.field public static final workout_list_popup_item_selected_text_color:I = 0x7f07027b

.field public static final workout_list_text_color:I = 0x7f070148

.field public static final workout_list_title_color:I = 0x7f070153

.field public static final workout_list_title_shadow_color:I = 0x7f070154

.field public static final workout_log_list_bg_main:I = 0x7f0701b0

.field public static final workout_log_list_small_text:I = 0x7f07019b

.field public static final workout_log_list_title_text:I = 0x7f070150

.field public static final workout_my_ranking_item_header_background:I = 0x7f070280

.field public static final workout_my_ranking_progress_bar_result_high_color:I = 0x7f07019f

.field public static final workout_my_ranking_progress_bar_result_low_color:I = 0x7f0701a0

.field public static final workout_notification_color:I = 0x7f07028b

.field public static final workout_photo_gallery_image_background_sel:I = 0x7f070176

.field public static final workout_ranking_activity_background:I = 0x7f07019c

.field public static final workout_ranking_item_highlight_color:I = 0x7f07019d

.field public static final workout_red:I = 0x7f070285

.field public static final workout_regular_background:I = 0x7f0701ab

.field public static final workout_s_health_main_bg_color:I = 0x7f07015e

.field public static final workout_selected:I = 0x7f07014a

.field public static final workout_skeletal_muscle_line_color:I = 0x7f070293

.field public static final workout_skeletal_muscle_ui_color:I = 0x7f070165

.field public static final workout_slider_bubble_text_color:I = 0x7f070157

.field public static final workout_speed_info_ui_color:I = 0x7f070172

.field public static final workout_split_line:I = 0x7f0701ac

.field public static final workout_status_during_workout_background:I = 0x7f07016e

.field public static final workout_summary_bg_color:I = 0x7f070199

.field public static final workout_summary_date_blue:I = 0x7f070276

.field public static final workout_supplement_text_color:I = 0x7f070287

.field public static final workout_tabs_divider_color:I = 0x7f07027e

.field public static final workout_te_goal_guide_text_bg:I = 0x7f070177

.field public static final workout_text:I = 0x7f07015d

.field public static final workout_text_care:I = 0x7f07017b

.field public static final workout_text_care_alpha_40:I = 0x7f07017c

.field public static final workout_text_general:I = 0x7f0701ad

.field public static final workout_time_goal_text_disable:I = 0x7f070174

.field public static final workout_time_goal_text_enable:I = 0x7f070173

.field public static final workout_time_goal_text_selected:I = 0x7f070175

.field public static final workout_title_color:I = 0x7f0700ea

.field public static final workout_title_text_of_component_color:I = 0x7f070169

.field public static final workout_transparent:I = 0x7f0701aa

.field public static final workout_walk_graph_running_color:I = 0x7f070274

.field public static final workout_walking_statistics_background:I = 0x7f07014b

.field public static final workout_weigth_info_ui_color:I = 0x7f070164

.field public static final workout_white:I = 0x7f07019a

.field public static final workout_white_40:I = 0x7f070286

.field public static final workout_widget_initial_dim:I = 0x7f070295

.field public static final workout_win_lose_bg_l:I = 0x7f070289

.field public static final workout_win_lose_bg_p:I = 0x7f07028a

.field public static final workout_wrc:I = 0x7f070156


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
