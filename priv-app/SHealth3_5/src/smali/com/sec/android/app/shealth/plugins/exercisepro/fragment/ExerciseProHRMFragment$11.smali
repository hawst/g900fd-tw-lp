.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;
.super Ljava/lang/Object;
.source "ExerciseProHRMFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->scaleAnimationStarter(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

.field final synthetic val$isGreen:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)V
    .locals 0

    .prologue
    .line 662
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->val$isGreen:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->val$isGreen:Z

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pulseAnimationShow(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)V

    .line 679
    invoke-virtual {p1}, Landroid/view/animation/Animation;->cancel()V

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 681
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 674
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 665
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/ImageView;

    move-result-object v1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$11;->val$isGreen:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020526

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 670
    return-void

    .line 668
    :cond_0
    const v0, 0x7f020529

    goto :goto_0
.end method
