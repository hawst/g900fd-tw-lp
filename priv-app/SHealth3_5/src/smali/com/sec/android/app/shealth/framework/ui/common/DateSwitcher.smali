.class public Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$3;,
        Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimationListenerForLazy;,
        Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimeListener;,
        Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;
    }
.end annotation


# static fields
.field private static final ALPHA_FACTOR:F = 0.0f

.field private static final DURATION:J = 0x190L

.field private static final IN_ALPHA_DURATION:J = 0xc8L

.field private static final MOVE_FACTOR:F = 0.25f

.field private static final OUT_ALPHA_DURATION:J = 0x1f4L


# instance fields
.field private animationMode:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;

.field private animeListener:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimeListener;

.field private curNextAnimation:Landroid/view/animation/AnimationSet;

.field private curPrevAnimation:Landroid/view/animation/AnimationSet;

.field private nextCurAnimation:Landroid/view/animation/AnimationSet;

.field private prevCurAnimation:Landroid/view/animation/AnimationSet;

.field private text:Ljava/lang/CharSequence;

.field private textColor:I

.field private textSize:F

.field private textSwitcher:Landroid/widget/TextSwitcher;

.field private textUnit:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->text:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textColor:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textUnit:I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->date_bar_font_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSize:F

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->text:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textColor:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textUnit:I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->date_bar_font_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSize:F

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->init()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;)Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimeListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->animeListener:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimeListener;

    return-object v0
.end method

.method private init()V
    .locals 2

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->initTextSwitcher()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v0, p0}, Landroid/widget/TextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->initAnimations()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->list_selector:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setBackground(Landroid/graphics/drawable/Drawable;)V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;->NULL:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->setAnimationMode(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;)V

    return-void
.end method

.method private initAnimations()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->initPrevCurAnimation()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->initNextCurAnimation()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->initCurNextAnimation()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->initCurPrevAnimation()V

    return-void
.end method

.method private initCurNextAnimation()V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {v9, v0, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v9, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v9, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v4, 0x3e800000    # 0.25f

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->curNextAnimation:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->curNextAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->curNextAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private initCurPrevAnimation()V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {v9, v0, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v9, v3, v4}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v9, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v4, -0x41800000    # -0.25f

    move v3, v1

    move v5, v1

    move v6, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->curPrevAnimation:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->curPrevAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->curPrevAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private initNextCurAnimation()V
    .locals 10

    const/4 v1, 0x1

    const/4 v4, 0x0

    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {v9, v4, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v2, 0x3e800000    # 0.25f

    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->nextCurAnimation:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->nextCurAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->nextCurAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->nextCurAnimation:Landroid/view/animation/AnimationSet;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method

.method private initPrevCurAnimation()V
    .locals 10

    const/4 v1, 0x1

    const/4 v4, 0x0

    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {v9, v4, v0}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v2, -0x41800000    # -0.25f

    move v3, v1

    move v5, v1

    move v6, v4

    move v7, v1

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->prevCurAnimation:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->prevCurAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->prevCurAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->prevCurAnimation:Landroid/view/animation/AnimationSet;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method

.method private initTextSwitcher()V
    .locals 4

    new-instance v0, Landroid/widget/TextSwitcher;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextSwitcher;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$dimen;->date_bar_margin:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v1, v0}, Landroid/widget/TextSwitcher;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->text:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public makeView()Landroid/view/View;
    .locals 4

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->date_selector_date_view:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textUnit:I

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSize:F

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ko"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "droid-sans-fallback"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_0
    return-object v0
.end method

.method public setAnimationMode(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->animationMode:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->animationMode:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$3;->$SwitchMap$com$sec$android$app$shealth$framework$ui$common$DateSwitcher$Mode:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->animationMode:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->nextCurAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->curPrevAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->prevCurAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->curNextAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v0, v2}, Landroid/widget/TextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v0, v2}, Landroid/widget/TextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setAnimeListener(Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->animeListener:Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher$AnimeListener;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textSwitcher:Landroid/widget/TextSwitcher;

    invoke-virtual {v0, p1}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->text:Ljava/lang/CharSequence;

    return-void
.end method

.method public setTextColor(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/DateSwitcher;->textColor:I

    return-void
.end method
