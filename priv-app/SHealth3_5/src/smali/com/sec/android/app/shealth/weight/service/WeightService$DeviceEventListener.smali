.class Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;
.super Ljava/lang/Object;
.source "WeightService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/service/WeightService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeviceEventListener"
.end annotation


# instance fields
.field private mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field weightService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/weight/service/WeightService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/sec/android/app/shealth/weight/service/WeightService;)V
    .locals 1
    .param p1, "shealthSensorDevice"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .param p2, "wgtService"    # Lcom/sec/android/app/shealth/weight/service/WeightService;

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 291
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    .line 292
    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    .line 296
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*****onJoined() name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$500(Lcom/sec/android/app/shealth/weight/service/WeightService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    new-instance v2, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/weight/service/WeightService;

    invoke-direct {v2, v3, v1}, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceDataListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/sec/android/app/shealth/weight/service/WeightService;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/sec/android/app/shealth/weight/service/WeightService;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    invoke-static {v0, v2, v1}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$600(Lcom/sec/android/app/shealth/weight/service/WeightService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 299
    return-void
.end method

.method public onLeft(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 303
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*****onLeft(), error code :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$500(Lcom/sec/android/app/shealth/weight/service/WeightService;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$500(Lcom/sec/android/app/shealth/weight/service/WeightService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mToJoinDevices:Ljava/util/Map;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$500(Lcom/sec/android/app/shealth/weight/service/WeightService;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    :cond_0
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 3
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    .line 343
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*****onResponseReceived(), name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    return-void
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 315
    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*****onStateChanged(), state :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " name : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->mShealthSensorDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    packed-switch p1, :pswitch_data_0

    .line 338
    :goto_0
    return-void

    .line 322
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;->updateStateView(I)V

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->cleanDevices()V

    goto :goto_0

    .line 329
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # getter for: Lcom/sec/android/app/shealth/weight/service/WeightService;->mWeightDataReceivedListener:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$200(Lcom/sec/android/app/shealth/weight/service/WeightService;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightDataReceivedListener;->updateStateView(I)V

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/service/WeightService$DeviceEventListener;->weightService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/service/WeightService;

    # invokes: Lcom/sec/android/app/shealth/weight/service/WeightService;->startWeightService()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/service/WeightService;->access$300(Lcom/sec/android/app/shealth/weight/service/WeightService;)V

    goto :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0x7d0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
