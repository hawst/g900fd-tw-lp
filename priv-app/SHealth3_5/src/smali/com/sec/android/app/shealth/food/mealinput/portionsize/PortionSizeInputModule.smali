.class public Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
.super Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
.source "PortionSizeInputModule.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    return-void
.end method


# virtual methods
.method public getMinInputValue()F
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->getMinInputValue()F

    move-result v0

    return v0
.end method

.method protected getRootLayoutId()I
    .locals 1

    .prologue
    .line 44
    const v0, 0x7f03010e

    return v0
.end method
