.class Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$6;
.super Ljava/lang/Object;
.source "SleepMonitorActivity_Base.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->showResetDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$6;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 358
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$6;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->access$500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->resetDataConfirm(Z)V

    .line 360
    const v1, 0x7f09094a

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :goto_0
    return-void

    .line 361
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
