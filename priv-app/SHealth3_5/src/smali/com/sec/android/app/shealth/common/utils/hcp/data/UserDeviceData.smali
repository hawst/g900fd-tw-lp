.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.source "UserDeviceData.java"


# instance fields
.field private mConnectivityType:I

.field private mCustomName:Ljava/lang/String;

.field private mDeviceId:Ljava/lang/String;

.field private mDeviceType:I

.field private mManufacture:Ljava/lang/String;

.field private mModel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceType"    # I

    .prologue
    const/4 v1, -0x1

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 21
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceType:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mCustomName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mModel:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mManufacture:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mConnectivityType:I

    .line 85
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceId:Ljava/lang/String;

    .line 86
    iput p2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceType:I

    .line 87
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceType"    # I
    .param p3, "customName"    # Ljava/lang/String;
    .param p4, "model"    # Ljava/lang/String;
    .param p5, "manufacture"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 21
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceType:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mCustomName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mModel:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mManufacture:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mConnectivityType:I

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceId:Ljava/lang/String;

    .line 71
    iput p2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceType:I

    .line 72
    invoke-virtual {p0, p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->setModel(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0, p5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->setManufacture(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->setCustomName(Ljava/lang/String;)V

    .line 75
    iput-object p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mManufacture:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJII)V
    .locals 9
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceType"    # I
    .param p3, "customName"    # Ljava/lang/String;
    .param p4, "model"    # Ljava/lang/String;
    .param p5, "manufacture"    # Ljava/lang/String;
    .param p6, "connectivityType"    # I
    .param p7, "id"    # J
    .param p9, "createTime"    # J
    .param p11, "updateTime"    # J
    .param p13, "timeZone"    # I
    .param p14, "daylightSaving"    # I

    .prologue
    .line 48
    move-object v0, p0

    move-wide/from16 v1, p7

    move-wide/from16 v3, p9

    move-wide/from16 v5, p11

    move/from16 v7, p13

    move/from16 v8, p14

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJII)V

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceType:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mCustomName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mModel:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mManufacture:Ljava/lang/String;

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mConnectivityType:I

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceId:Ljava/lang/String;

    .line 51
    iput p2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceType:I

    .line 52
    invoke-virtual {p0, p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->setModel(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0, p5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->setManufacture(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->setCustomName(Ljava/lang/String;)V

    .line 55
    iput p6, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mConnectivityType:I

    .line 56
    return-void
.end method


# virtual methods
.method public getConnectivityType()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mConnectivityType:I

    return v0
.end method

.method public getCustomName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mCustomName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mDeviceType:I

    return v0
.end method

.method public getManufacture()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mManufacture:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method public setConnectivityType(I)V
    .locals 0
    .param p1, "connectivityType"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mConnectivityType:I

    .line 167
    return-void
.end method

.method public setCustomName(Ljava/lang/String;)V
    .locals 2
    .param p1, "customName"    # Ljava/lang/String;

    .prologue
    .line 115
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xff

    if-le v0, v1, :cond_0

    .line 116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "customName should be <= 250"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mCustomName:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setManufacture(Ljava/lang/String;)V
    .locals 2
    .param p1, "manufacture"    # Ljava/lang/String;

    .prologue
    .line 149
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xff

    if-le v0, v1, :cond_0

    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "manufacture should be <= 250"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mManufacture:Ljava/lang/String;

    .line 153
    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 2
    .param p1, "model"    # Ljava/lang/String;

    .prologue
    .line 132
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xff

    if-le v0, v1, :cond_0

    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "model should be <= 250"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->mModel:Ljava/lang/String;

    .line 136
    return-void
.end method
