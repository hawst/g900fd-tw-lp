.class Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;
.super Ljava/lang/Object;
.source "SHealthApplication.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->onChange(ZLandroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;)V
    .locals 0

    .prologue
    .line 721
    iput-object p1, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/16 v8, 0x2728

    const/16 v7, 0x2726

    const/16 v6, 0x2723

    const/4 v5, 0x1

    const/16 v4, 0x272e

    .line 724
    const-string v2, "-----SIC-----"

    const-string v3, "WearableStatusObserver.onChange"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v0

    .line 727
    .local v0, "connectedDeviceType":I
    if-eq v0, v8, :cond_0

    const/16 v2, 0x2724

    if-eq v0, v2, :cond_0

    if-eq v0, v7, :cond_0

    if-eq v0, v4, :cond_0

    const/16 v2, 0x2730

    if-eq v0, v2, :cond_0

    if-eq v0, v6, :cond_0

    const/16 v2, 0x2727

    if-ne v0, v2, :cond_1

    .line 735
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v3, v3, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 736
    .local v1, "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->updateAppDataAfterGearConnecton(Ljava/util/List;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$500(Lcom/sec/android/app/shealth/SHealthApplication;Ljava/util/List;)V

    .line 737
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 740
    .end local v1    # "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    :cond_1
    invoke-static {v4}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->wasGearConnected(I)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->wasGearConnected(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 742
    :cond_2
    const-string v2, "-----SIC-----"

    const-string v3, "Wearable was connected earlier, hence no changes to do"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    :cond_3
    :goto_0
    return-void

    .line 745
    :cond_4
    if-eq v0, v8, :cond_3

    .line 749
    if-eq v0, v7, :cond_5

    if-ne v0, v6, :cond_7

    .line 752
    :cond_5
    invoke-static {v0, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->setFirstGearConnectionAsSuccess(IZ)V

    .line 753
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090022

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v3, v3, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 754
    .restart local v1    # "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-eq v2, v3, :cond_6

    .line 755
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->updateAppDataAfterGearConnecton(Ljava/util/List;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$500(Lcom/sec/android/app/shealth/SHealthApplication;Ljava/util/List;)V

    .line 756
    :cond_6
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 760
    .end local v1    # "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    :cond_7
    if-ne v0, v4, :cond_3

    .line 762
    invoke-static {v0, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$APPDATA_PREFRENENCE;->setFirstGearConnectionAsSuccess(IZ)V

    .line 763
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090022

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v3, v3, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 764
    .restart local v1    # "pluginlist":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;>;"
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-eq v2, v3, :cond_8

    .line 765
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->updateAppDataAfterGearConnecton(Ljava/util/List;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$500(Lcom/sec/android/app/shealth/SHealthApplication;Ljava/util/List;)V

    .line 766
    :cond_8
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 767
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/SHealthApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v3, v3, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # getter for: Lcom/sec/android/app/shealth/SHealthApplication;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/shealth/SHealthApplication;->access$400(Lcom/sec/android/app/shealth/SHealthApplication;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getPluginRegistryDataFromName(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 768
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->UV:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$UV;

    if-eq v2, v3, :cond_9

    .line 769
    iget-object v2, p0, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver$1;->this$1:Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;

    iget-object v2, v2, Lcom/sec/android/app/shealth/SHealthApplication$WearableConnectionObserver;->this$0:Lcom/sec/android/app/shealth/SHealthApplication;

    # invokes: Lcom/sec/android/app/shealth/SHealthApplication;->updateAppDataAfterGearConnecton(Ljava/util/List;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/SHealthApplication;->access$500(Lcom/sec/android/app/shealth/SHealthApplication;Ljava/util/List;)V

    .line 770
    :cond_9
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto/16 :goto_0
.end method
