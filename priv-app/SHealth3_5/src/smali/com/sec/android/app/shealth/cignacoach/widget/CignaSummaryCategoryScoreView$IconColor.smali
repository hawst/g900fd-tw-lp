.class final enum Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;
.super Ljava/lang/Enum;
.source "CignaSummaryCategoryScoreView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "IconColor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

.field public static final enum GREEN:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

.field public static final enum RED:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

.field public static final enum WHITE:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

.field public static final enum YELLOW:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    const-string v1, "RED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->RED:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    const-string v1, "YELLOW"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->YELLOW:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    const-string v1, "GREEN"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->GREEN:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    const-string v1, "WHITE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->WHITE:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->RED:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->YELLOW:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->GREEN:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->WHITE:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    return-object v0
.end method
