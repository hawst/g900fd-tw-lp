.class Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

.field final synthetic val$logActivity:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->val$logActivity:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0xc25e1e21154d513L

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x49

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x79

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v5, v0, v11

    if-eqz v5, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I
    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$1802(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)I

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, 0x2b30

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, 0x2b00

    aput v2, v0, v3

    move v2, v3

    :goto_2
    array-length v5, v0

    if-lt v2, v5, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    :goto_3
    array-length v2, v0

    if-lt v3, v2, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_6
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_8
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_9

    xor-long/2addr v0, v9

    :cond_9
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    if-lez v0, :cond_12

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mFilterRange:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$2200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Ljava/util/ArrayList;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mFilterRange:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$2200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Ljava/util/ArrayList;

    move-result-object v4

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_c

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, 0x531e

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, 0x532e

    aput v2, v0, v3

    move v2, v3

    :goto_4
    array-length v5, v0

    if-lt v2, v5, :cond_a

    array-length v0, v1

    new-array v0, v0, [C

    :goto_5
    array-length v2, v0

    if-lt v3, v2, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_a
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_b
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_c
    aget-wide v0, v2, v3

    cmp-long v5, v0, v11

    if-eqz v5, :cond_d

    xor-long/2addr v0, v9

    :cond_d
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mFilterRange:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$2200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Ljava/util/ArrayList;

    move-result-object v5

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_10

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x7599

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x75a9

    aput v2, v0, v3

    move v2, v3

    :goto_6
    array-length v5, v0

    if-lt v2, v5, :cond_e

    array-length v0, v1

    new-array v0, v0, [C

    :goto_7
    array-length v2, v0

    if-lt v3, v2, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_e
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_f
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_10
    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_11

    xor-long/2addr v0, v9

    :cond_11
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    :goto_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mViewbyTask:Landroid/os/AsyncTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$2300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Landroid/os/AsyncTask;

    move-result-object v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mViewbyTask:Landroid/os/AsyncTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$2300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Landroid/os/AsyncTask;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_17

    :goto_9
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    new-instance v5, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;

    iget-object v6, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->val$logActivity:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_15

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x33

    aput v0, v1, v3

    new-array v0, v8, [I

    const/4 v2, -0x3

    aput v2, v0, v3

    move v2, v3

    :goto_a
    array-length v5, v0

    if-lt v2, v5, :cond_13

    array-length v0, v1

    new-array v0, v0, [C

    :goto_b
    array-length v2, v0

    if-lt v3, v2, :cond_14

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v0

    :cond_12
    :goto_c
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->log:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_8

    :cond_13
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_14
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_15
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_16

    xor-long/2addr v0, v9

    :cond_16
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-direct {v5, v6, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V

    new-array v0, v3, [Ljava/lang/Void;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mViewbyTask:Landroid/os/AsyncTask;
    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->access$2302(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    :cond_17
    return-void

    :catch_1
    move-exception v0

    goto :goto_c

    :catch_2
    move-exception v0

    goto :goto_9
.end method
