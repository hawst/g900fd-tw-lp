.class public Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants$SWearableConstants;
.super Ljava/lang/Object;
.source "HomeLatestDataConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SWearableConstants"
.end annotation


# static fields
.field public static final ACTIVITY_TRACKER_DEVICE_CONTENT_URI:Landroid/net/Uri;

.field public static final ACTIVITY_TRACKER_DEVICE_URL:Ljava/lang/String; = "content://com.samsung.android.app.atracker.shealthsync.SHealthProvider"

.field public static final ACTIVITY_TRACKER_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_SBAND_SYNC"

.field public static final DEVICE_NAME_ACTIVITY_TRACKER:Ljava/lang/String; = "SamsungEI-AN900A"

.field public static final DEVICE_NAME_GEAR:Ljava/lang/String; = "GALAXY Gear"

.field public static final DEVICE_NAME_GEAR2:Ljava/lang/String; = "Gear2"

.field public static final DEVICE_NAME_GEAR3:Ljava/lang/String; = "GearS"

.field public static final DEVICE_NAME_TIZEN_GEAR:Ljava/lang/String; = "Gear"

.field public static final DEVICE_NAME_WINGTIP:Ljava/lang/String; = "GearFit"

.field public static final GEAR2_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_GEAR2_SYNC"

.field public static final GEAR3_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_GEAR_SYNC"

.field public static final GEAR_SYNC:Ljava/lang/String; = "android.intent.action.PEDOMETER_SETTING_SYNC"

.field public static final WINGTIP_SYNC:Ljava/lang/String; = "com.samsung.android.shealth.ACTION_WINGTIP_SYNC"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "content://com.samsung.android.app.atracker.shealthsync.SHealthProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataConstants$SWearableConstants;->ACTIVITY_TRACKER_DEVICE_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
