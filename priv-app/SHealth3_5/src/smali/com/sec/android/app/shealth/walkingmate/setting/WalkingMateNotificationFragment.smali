.class public Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "WalkingMateNotificationFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContainer:Landroid/view/ViewGroup;

.field private mInactiveConfigurationValue:Landroid/widget/TextView;

.field private mInactiveEnabled:Landroid/widget/CheckBox;

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "WalkingMateNotificationFragment"

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mContainer:Landroid/view/ViewGroup;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mInactiveEnabled:Landroid/widget/CheckBox;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mInactiveConfigurationValue:Landroid/widget/TextView;

    return-void
.end method

.method private static enableDisableViewGroup(Landroid/view/ViewGroup;Z)V
    .locals 4
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;
    .param p1, "enabled"    # Z

    .prologue
    .line 153
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 154
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 155
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 156
    .local v2, "view":Landroid/view/View;
    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 157
    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 158
    check-cast v2, Landroid/view/ViewGroup;

    .end local v2    # "view":Landroid/view/View;
    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->enableDisableViewGroup(Landroid/view/ViewGroup;Z)V

    .line 154
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 161
    :cond_1
    return-void
.end method

.method private getInactiveTimeConfiguration()Ljava/lang/String;
    .locals 11

    .prologue
    const v5, 0x7f090d71

    const/4 v10, 0x2

    const/16 v7, 0x1e

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 76
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingPeriod()I

    move-result v2

    .line 78
    .local v2, "timeOut":I
    const-string v0, ""

    .line 79
    .local v0, "configuration":Ljava/lang/String;
    if-ne v2, v7, :cond_1

    .line 80
    const v4, 0x7f090f6d

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 89
    :cond_0
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeAlways()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 92
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0907c7

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    :goto_1
    return-object v0

    .line 81
    :cond_1
    const/16 v4, 0x3c

    if-ne v2, v4, :cond_2

    .line 82
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 83
    :cond_2
    const/16 v4, 0x5a

    if-ne v2, v4, :cond_3

    .line 84
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090f6d

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 85
    :cond_3
    const/16 v4, 0x78

    if-ne v2, v4, :cond_0

    .line 86
    const v4, 0x7f090d72

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get24Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    .local v1, "fromTime":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get24Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 98
    .local v3, "toTime":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 99
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get12Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->get12Hrformat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 103
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%s - %s"

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v1, v6, v8

    aput-object v3, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method


# virtual methods
.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v7, 0x7f080bf4

    const v4, 0x7f080bef

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 37
    if-nez p1, :cond_1

    .line 38
    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->TAG:Ljava/lang/String;

    const-string v4, "There is no view instance."

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 71
    :pswitch_1
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-nez v4, :cond_3

    move v4, v5

    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 43
    :pswitch_2
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 44
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 47
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 48
    .local v1, "checkBoxView2":Landroid/widget/CheckBox;
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-ne v3, v5, :cond_2

    .line 49
    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 53
    :goto_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInactiveTimeTracking()Z

    move-result v3

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eq v3, v4, :cond_0

    .line 54
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setInactiveTimeTracking(Z)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 56
    const-string v3, "INACTIVE"

    const-string v4, "ON/OFF"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 51
    :cond_2
    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2

    .line 60
    .end local v1    # "checkBoxView2":Landroid/widget/CheckBox;
    :pswitch_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 61
    .local v0, "checkBoxView":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    .line 62
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInactiveTimeTracking()Z

    move-result v3

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eq v3, v4, :cond_0

    .line 63
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setInactiveTimeTracking(Z)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 65
    const-string v3, "INACTIVE"

    const-string v4, "ON/OFF"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .end local v0    # "checkBoxView":Landroid/widget/CheckBox;
    :cond_3
    move v4, v6

    .line 71
    goto :goto_1

    .line 41
    :pswitch_data_0
    .packed-switch 0x7f080bed
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f080bef

    .line 114
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mContainer:Landroid/view/ViewGroup;

    .line 115
    const v0, 0x7f0302a9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    const v1, 0x7f080bf1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    const v1, 0x7f080bf4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mInactiveEnabled:Landroid/widget/CheckBox;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mInactiveEnabled:Landroid/widget/CheckBox;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInactiveTimeTracking()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mInactiveEnabled:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    const v1, 0x7f080bf2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mInactiveConfigurationValue:Landroid/widget/TextView;

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->updateInactiveTimeConfiguration()V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    const v1, 0x7f080bf3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    const v1, 0x7f080bed

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getNotificationValue(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mView:Landroid/view/View;

    return-object v0
.end method

.method public setEnable(Z)V
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    const v5, 0x106000c

    const v4, 0x3ecccccd    # 0.4f

    .line 139
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mContainer:Landroid/view/ViewGroup;

    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->enableDisableViewGroup(Landroid/view/ViewGroup;Z)V

    .line 141
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v2

    const/16 v3, 0x2719

    if-eq v2, v3, :cond_0

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mContainer:Landroid/view/ViewGroup;

    const v3, 0x7f080bee

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 143
    .local v0, "achievementText":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 144
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mContainer:Landroid/view/ViewGroup;

    const v3, 0x7f080bf2

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 147
    .local v1, "inactiveConfText":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 148
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setAlpha(F)V

    .line 150
    .end local v0    # "achievementText":Landroid/widget/TextView;
    .end local v1    # "inactiveConfText":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public updateInactiveTimeConfiguration()V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->mInactiveConfigurationValue:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateNotificationFragment;->getInactiveTimeConfiguration()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    return-void
.end method
