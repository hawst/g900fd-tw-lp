.class public Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;
.super Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.source "VerticalGradationView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method


# virtual methods
.method protected drawGradationLine(Landroid/graphics/Canvas;Landroid/graphics/Paint;III)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "position"    # I
    .param p4, "start"    # I
    .param p5, "length"    # I

    .prologue
    .line 75
    int-to-float v1, p4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getLength()I

    move-result v0

    sub-int/2addr v0, p3

    int-to-float v2, v0

    add-int v0, p4, p5

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getLength()I

    move-result v0

    sub-int/2addr v0, p3

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 76
    return-void
.end method

.method protected drawGradationText(Landroid/graphics/Canvas;Landroid/graphics/Paint;Ljava/lang/String;II)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "position"    # I
    .param p5, "heightOffset"    # I

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v3, p5

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p5

    int-to-float v2, v3

    .line 63
    .local v2, "textX":F
    const-string v0, "  "

    .line 64
    .local v0, "addedSpace":Ljava/lang/String;
    invoke-virtual {v0, p3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "modText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getLength()I

    move-result v3

    sub-int/2addr v3, p4

    int-to-float v3, v3

    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getTextHalfHeight(Landroid/graphics/Paint;)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual {p1, v1, v2, v3, p2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 66
    return-void
.end method

.method protected drawRange(Landroid/graphics/Canvas;IILandroid/graphics/Paint;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "from"    # I
    .param p3, "to"    # I
    .param p4, "rangePaint"    # Landroid/graphics/Paint;

    .prologue
    .line 70
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getLength()I

    move-result v0

    sub-int/2addr v0, p3

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getBottomLineHeight()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getLength()I

    move-result v0

    sub-int/2addr v0, p2

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 71
    return-void
.end method

.method protected getBottomLineHeight()I
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_vertical_bottomline_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected getDistanceBetweenGradations()I
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$dimen;->gradationview_vertical_between_gradationbars_distance:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method protected getLength()I
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/controller/VerticalGradationView;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method protected getOrientationTouchCoord(Landroid/view/MotionEvent;)F
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 80
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    neg-float v0, v0

    return v0
.end method
