.class Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->simplePopupController:Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;
    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;

    move-result-object v4

    invoke-direct {v1, v2, p1, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;-><init>(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;Lcom/sec/android/app/shealth/common/commonui/dialog/SimplePopupController;)V

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->toast:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$402(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;)Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->toast:Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ToastHint;->show()V

    const/4 v0, 0x1

    return v0
.end method
