.class public Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;
.super Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;
.source "SleepMonitorInformationArea.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea$1;
    }
.end annotation


# instance fields
.field private HID:Ljava/text/SimpleDateFormat;

.field private HID_LAST_DAY:Ljava/text/SimpleDateFormat;

.field private final context:Landroid/content/Context;

.field private mDayPercent:Landroid/widget/TextView;

.field private mDayPercentClose:Landroid/widget/TextView;

.field private mDaySleepArea:Landroid/view/View;

.field private mInformationAreaparent:Landroid/widget/LinearLayout;

.field private mMotHourText:Landroid/widget/TextView;

.field private mMotHourTextLabel:Landroid/widget/TextView;

.field private mMotMinText:Landroid/widget/TextView;

.field private mMotMinTextLabel:Landroid/widget/TextView;

.field private mMotSleepTitleText:Landroid/widget/TextView;

.field private mPercent:Landroid/widget/TextView;

.field private mRightPadding:Landroid/view/View;

.field private mTimeText:Landroid/widget/TextView;

.field private mTotalHourLabel:Landroid/widget/TextView;

.field private mTotalHourText:Landroid/widget/TextView;

.field private mTotalMinLabel:Landroid/widget/TextView;

.field private mTotalMinText:Landroid/widget/TextView;

.field private mTotalSleepTitleText:Landroid/widget/TextView;

.field private motionlessSleepArea:Landroid/view/View;

.field private totalSleepArea:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;-><init>(Landroid/content/Context;)V

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->context:Landroid/content/Context;

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->initLayout()V

    .line 54
    return-void
.end method

.method private adjustTextSize(II)V
    .locals 4
    .param p1, "totalSleepMin"    # I
    .param p2, "motionlessSleepMin"    # I

    .prologue
    const/16 v0, 0x63

    const/high16 v3, 0x42040000    # 33.0f

    const/high16 v2, 0x42000000    # 32.0f

    const/4 v1, 0x1

    .line 252
    if-gt p1, v0, :cond_0

    if-le p2, v0, :cond_1

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalHourText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalMinText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotHourText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotMinText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mPercent:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mDayPercentClose:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 267
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalHourText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalMinText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotHourText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotMinText:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mPercent:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mDayPercentClose:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method public static convertDpToPx(Landroid/content/res/Resources;F)F
    .locals 2
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "dp"    # F

    .prologue
    .line 115
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 116
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    const/4 v1, 0x1

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    return v1
.end method

.method public static getDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "realX"    # D
    .param p2, "periodH"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v1, :cond_0

    .line 104
    invoke-static {p3}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatMonthYear(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    .line 111
    .local v0, "mDateFormat":Ljava/text/DateFormat;
    :goto_0
    new-instance v1, Ljava/util/Date;

    double-to-long v2, p0

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .end local v0    # "mDateFormat":Ljava/text/DateFormat;
    :goto_1
    return-object v1

    .line 105
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v1, :cond_1

    .line 106
    invoke-static {p3}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    .restart local v0    # "mDateFormat":Ljava/text/DateFormat;
    goto :goto_0

    .line 108
    .end local v0    # "mDateFormat":Ljava/text/DateFormat;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    double-to-long v4, p0

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    double-to-long v3, p0

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {p3, v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f080357

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mInformationAreaparent:Landroid/widget/LinearLayout;

    .line 58
    const v0, 0x7f080943

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTimeText:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f08094d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->totalSleepArea:Landroid/view/View;

    .line 60
    const v0, 0x7f080950

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalHourLabel:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f080952

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalMinLabel:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f08094f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalHourText:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f08094e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalSleepTitleText:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f080951

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalMinText:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f08094c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mRightPadding:Landroid/view/View;

    .line 66
    const v0, 0x7f080944

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->motionlessSleepArea:Landroid/view/View;

    .line 67
    const v0, 0x7f080945

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotSleepTitleText:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f080947

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotHourTextLabel:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f080949

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotMinTextLabel:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f080946

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotHourText:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f080948

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotMinText:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f08094a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mPercent:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f080955

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mDayPercentClose:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f080953

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mDaySleepArea:Landroid/view/View;

    .line 75
    const v0, 0x7f080954

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mDayPercent:Landroid/widget/TextView;

    .line 76
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mInformationAreaparent:Landroid/widget/LinearLayout;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 272
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030213

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mInformationAreaparent:Landroid/widget/LinearLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 98
    return-void
.end method

.method public setData(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;JJJJFLandroid/content/Context;Ljava/lang/Long;Z)V
    .locals 24
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .param p2, "timeStamp"    # J
    .param p4, "sleepStartTime"    # J
    .param p6, "sleepRiseTime"    # J
    .param p8, "sleepTime"    # J
    .param p10, "efficiency"    # F
    .param p11, "context"    # Landroid/content/Context;
    .param p12, "motoinLess"    # Ljava/lang/Long;
    .param p13, "falseOverlap"    # Z

    .prologue
    .line 120
    new-instance v19, Ljava/util/Date;

    invoke-direct/range {v19 .. v19}, Ljava/util/Date;-><init>()V

    .line 121
    .local v19, "date":Ljava/util/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 123
    .local v18, "cal":Ljava/util/Calendar;
    sget-object v4, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea$1;->$SwitchMap$com$sec$android$app$shealth$framework$ui$graph$PeriodH:[I

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 220
    :goto_0
    return-void

    .line 127
    :pswitch_0
    const-wide/16 v4, 0x0

    cmp-long v4, p4, v4

    if-eqz v4, :cond_0

    .line 128
    move-object/from16 v0, v19

    move-wide/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 138
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    move-object/from16 v0, p11

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 140
    .local v21, "hourText":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ~ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 143
    const-wide/16 v4, 0x0

    cmp-long v4, p6, v4

    if-eqz v4, :cond_1

    .line 144
    move-object/from16 v0, v19

    move-wide/from16 v1, p6

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 156
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    move-object/from16 v0, p11

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 157
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTimeText:Landroid/widget/TextView;

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mRightPadding:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 160
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 161
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mDayPercent:Landroid/widget/TextView;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move/from16 v0, p10

    float-to-int v11, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mDaySleepArea:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 166
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalSleepTitleText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090d8f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotSleepTitleText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090d58

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 131
    .end local v21    # "hourText":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 132
    invoke-virtual/range {v18 .. v19}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 133
    const/16 v4, 0xc

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v23

    .line 134
    .local v23, "minutes":I
    div-int/lit8 v4, v23, 0x14

    mul-int/lit8 v23, v4, 0x14

    .line 135
    const/16 v4, 0xc

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, Ljava/util/Calendar;->set(II)V

    .line 136
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v19

    goto/16 :goto_1

    .line 147
    .end local v23    # "minutes":I
    .restart local v21    # "hourText":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {v18 .. v19}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 148
    const/16 v4, 0xc

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v23

    .line 149
    .restart local v23    # "minutes":I
    div-int/lit8 v4, v23, 0x14

    mul-int/lit8 v23, v4, 0x14

    .line 150
    if-eqz p13, :cond_2

    .line 151
    const/16 v4, 0xc

    add-int/lit8 v5, v23, 0x28

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 154
    :goto_4
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v19

    goto/16 :goto_2

    .line 153
    :cond_2
    const/16 v4, 0xc

    add-int/lit8 v5, v23, 0x14

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->set(II)V

    goto :goto_4

    .line 163
    .end local v23    # "minutes":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mDayPercent:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move/from16 v0, p10

    float-to-int v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 171
    .end local v21    # "hourText":Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, v19

    move-wide/from16 v1, p4

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 172
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    move-object/from16 v0, p11

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 173
    .local v20, "dayText":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ~ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 174
    move-object/from16 v0, v19

    move-wide/from16 v1, p6

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 175
    move-wide/from16 v0, p6

    move-wide/from16 v2, p4

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isSameYear(JJ)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 176
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    move-object/from16 v0, p11

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 180
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTimeText:Landroid/widget/TextView;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    const-wide/32 v4, 0xea60

    div-long v4, p8, v4

    long-to-int v4, v4

    int-to-long v0, v4

    move-wide/from16 p8, v0

    .line 183
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalHourText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalMinText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalHourLabel:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalMinLabel:Landroid/widget/TextView;

    move-object/from16 v4, p0

    move-wide/from16 v9, p8

    invoke-virtual/range {v4 .. v10}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->setSleepViews(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;J)V

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->totalSleepArea:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mRightPadding:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v22

    check-cast v22, Landroid/widget/LinearLayout$LayoutParams;

    .line 187
    .local v22, "lParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x41700000    # 15.0f

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->convertDpToPx(Landroid/content/res/Resources;F)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    move-object/from16 v0, v22

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 188
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mRightPadding:Landroid/view/View;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotHourText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotMinText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotHourTextLabel:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotMinTextLabel:Landroid/widget/TextView;

    invoke-virtual/range {p12 .. p12}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v10}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->setSleepViews(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;J)V

    .line 191
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mPercent:Landroid/widget/TextView;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "(%d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move/from16 v0, p10

    float-to-int v11, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->motionlessSleepArea:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 196
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalSleepTitleText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090d8f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotSleepTitleText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090d58

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 178
    .end local v22    # "lParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID_LAST_DAY:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    move-object/from16 v0, p11

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_5

    .line 194
    .restart local v22    # "lParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mPercent:Landroid/widget/TextView;

    const-string v5, "(%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move/from16 v0, p10

    float-to-int v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 201
    .end local v20    # "dayText":Ljava/lang/String;
    .end local v22    # "lParams":Landroid/widget/LinearLayout$LayoutParams;
    :pswitch_2
    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Ljava/util/Date;->setTime(J)V

    .line 202
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTimeText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID:Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    const-wide/32 v4, 0xea60

    div-long v4, p8, v4

    long-to-int v4, v4

    int-to-long v9, v4

    .line 205
    .local v9, "sleepMin":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalHourText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalMinText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalHourLabel:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalMinLabel:Landroid/widget/TextView;

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v10}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->setSleepViews(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;J)V

    .line 206
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->totalSleepArea:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 207
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotHourText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotMinText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotHourTextLabel:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotMinTextLabel:Landroid/widget/TextView;

    invoke-virtual/range {p12 .. p12}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-object/from16 v11, p0

    invoke-virtual/range {v11 .. v17}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->setSleepViews(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;J)V

    .line 208
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 209
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mPercent:Landroid/widget/TextView;

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "(%d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move/from16 v0, p10

    float-to-int v11, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->motionlessSleepArea:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 216
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mTotalSleepTitleText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090d90

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mMotSleepTitleText:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090d91

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 211
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->mPercent:Landroid/widget/TextView;

    const-string v5, "(%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move/from16 v0, p10

    float-to-int v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 79
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatMonthYear(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID:Ljava/text/SimpleDateFormat;

    .line 87
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID_LAST_DAY:Ljava/text/SimpleDateFormat;

    .line 88
    return-void

    .line 81
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatDateMonth(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID:Ljava/text/SimpleDateFormat;

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->HID:Ljava/text/SimpleDateFormat;

    goto :goto_0
.end method

.method public setSleepViews(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;J)V
    .locals 8
    .param p1, "hourText"    # Landroid/widget/TextView;
    .param p2, "minuteText"    # Landroid/widget/TextView;
    .param p3, "hourLabel"    # Landroid/widget/TextView;
    .param p4, "minuteLabel"    # Landroid/widget/TextView;
    .param p5, "sleepMin"    # J

    .prologue
    const/16 v3, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 223
    long-to-int v2, p5

    div-int/lit8 v0, v2, 0x3c

    .line 224
    .local v0, "hour":I
    long-to-int v2, p5

    rem-int/lit8 v1, v2, 0x3c

    .line 225
    .local v1, "minute":I
    if-nez v0, :cond_2

    .line 226
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 228
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%1$d"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    :goto_0
    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    .line 247
    const v2, 0x7f0901dd

    invoke-virtual {p4, v2}, Landroid/widget/TextView;->setText(I)V

    .line 249
    :cond_0
    return-void

    .line 231
    :cond_1
    const-string v2, "%1$d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 234
    :cond_2
    invoke-virtual {p1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 235
    invoke-virtual {p3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    if-ne v0, v6, :cond_3

    .line 237
    const v2, 0x7f0901db

    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 239
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 240
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%d"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%1$d"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 243
    :cond_4
    const-string v2, "%d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%1$d"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public update(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 277
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    return-void
.end method
