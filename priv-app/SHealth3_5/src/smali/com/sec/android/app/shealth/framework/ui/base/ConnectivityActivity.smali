.class public Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final HEADER_TEXT_KEY:Ljava/lang/String; = "HeaderText"


# instance fields
.field private args:Landroid/os/Bundle;

.field private isNoSupportScan:Z

.field private mConnectivityType:I

.field private mDataType:I

.field private mDeviceText:Ljava/lang/String;

.field private mDeviceTypeResId:I

.field private mDeviceTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstTab:Landroid/widget/LinearLayout;

.field private mHandler:Landroid/os/Handler;

.field private mHeaderText:Ljava/lang/String;

.field private mHeaderTextResIds:[I

.field private mInfoContentId:I

.field private mInfoContentIds:[I

.field private mInfoText:Ljava/lang/String;

.field private mInfoTextResId:I

.field private mNoDeviceText:Ljava/lang/String;

.field private mNoDeviceTextResId:I

.field private mPairedText:Ljava/lang/String;

.field private mPairedTextResId:I

.field private mSecondTab:Landroid/widget/LinearLayout;

.field private mSubTabsContent:[Ljava/lang/String;

.field private mSubTabsContentResIds:[I

.field private mSubTabsGrp:Landroid/widget/RadioGroup;

.field private mTypeFaceSelected:Landroid/graphics/Typeface;

.field private mTypeFaceUnSelected:Landroid/graphics/Typeface;

.field private mfirstSubTab:Landroid/widget/RadioButton;

.field private msecondSubTab:Landroid/widget/RadioButton;

.field scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mNoDeviceTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypeResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mPairedTextResId:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoTextResId:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->isNoSupportScan:Z

    return-void
.end method

.method private SwitchTabs(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->first_sub_tab_rd_btn:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->second_sub_tab_rd_btn:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mFirstTab:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->selected:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSecondTab:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mTypeFaceSelected:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mTypeFaceUnSelected:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setSelected(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setSelected(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    const-string v1, "#ffe57f"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    const-string v1, "#B3fafafa"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTextColor(I)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->first_sub_tab_rd_btn:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v1, 0x2718

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSecondTab:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->selected:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mFirstTab:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mTypeFaceSelected:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mTypeFaceUnSelected:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setSelected(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setSelected(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    const-string v1, "#ffe57f"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    const-string v1, "#B3fafafa"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTextColor(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v1, 0x271d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v1, 0x2723

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v1, 0x2728

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v1, 0x2724

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v1, 0x2726

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v1, 0x272e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_2
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)[I
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHeaderTextResIds:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    return-object v0
.end method

.method private dismissPopUp()V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "Unpair"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "connect"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "disconnect"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 4

    const/4 v3, -0x1

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->device_connectivity:I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "DataType"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->setTitle(I)V

    return-void

    :pswitch_1
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->weighing_scale:I

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->blood_pressure_monitor:I

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->blood_glucose_meter:I

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->pedometer_acc:I

    goto :goto_0

    :pswitch_5
    const-string v0, "action_bar_title_ids"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "action_bar_title_ids"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :cond_0
    const-string v0, "ConnectivityType"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ConnectivityType"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mConnectivityType:I

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mConnectivityType:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->hrm_heartrate_accessories:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->exercise_acc:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->exercise_acc:I

    goto :goto_0

    :pswitch_6
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->sleep_sleep_accessories:I

    goto :goto_0

    :pswitch_7
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->stress_accessories:I

    goto :goto_0

    :pswitch_8
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->my_accessories:I

    goto :goto_0

    :pswitch_9
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->uv_accessories:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->onBackPressed()V

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->SwitchTabs(Landroid/view/View;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->connectivity_activity:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->setContentView(I)V

    const-string/jumbo v0, "sans-serif"

    invoke-static {v0, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mTypeFaceSelected:Landroid/graphics/Typeface;

    const-string/jumbo v0, "sec-roboto-light"

    invoke-static {v0, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mTypeFaceUnSelected:Landroid/graphics/Typeface;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DataType"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "DataType"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    :cond_0
    const-string v1, "ConnectivityType"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "ConnectivityType"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mConnectivityType:I

    :cond_1
    const-string v1, "header_text_string_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v1, "header_text_string_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHeaderTextResIds:[I

    :cond_2
    :goto_0
    const-string v1, "device_type_text_res_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, "device_type_text_res_id"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypeResId:I

    :cond_3
    :goto_1
    const-string/jumbo v1, "paired_text_res_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string/jumbo v1, "paired_text_res_id"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mPairedTextResId:I

    :cond_4
    :goto_2
    const-string/jumbo v1, "no_device_text_res_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string/jumbo v1, "no_device_text_res_id"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mNoDeviceTextResId:I

    :cond_5
    :goto_3
    const-string/jumbo v1, "scanning_type_info_text_res_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string/jumbo v1, "scanning_type_info_text_res_id"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoTextResId:I

    :cond_6
    :goto_4
    const-string v1, "info_content_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    const-string v1, "info_content_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoContentIds:[I

    :cond_7
    :goto_5
    const-string/jumbo v1, "sub_tabs_content_res_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    const-string/jumbo v1, "sub_tabs_content_res_ids"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    :cond_8
    :goto_6
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->subtabs_rd_grp:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsGrp:Landroid/widget/RadioGroup;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->first_sub_tab_rd_btn:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->second_sub_tab_rd_btn:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->first_sub_tab:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mFirstTab:Landroid/widget/LinearLayout;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->second_sub_tab:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSecondTab:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    array-length v0, v0

    if-le v0, v6, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mFirstTab:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->selected:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSecondTab:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_tab:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsGrp:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v5}, Landroid/widget/RadioGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    aget v2, v2, v5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mfirstSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, v6}, Landroid/widget/RadioButton;->setSelected(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->msecondSubTab:Landroid/widget/RadioButton;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    aget v2, v2, v6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    if-nez v0, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v1, 0x2718

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "DeviceTypes"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_a
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    if-eq v0, v4, :cond_1b

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    if-eqz p1, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "Rename"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "Rename"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "Rename"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->dismissPopUp()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "DataType"

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "ConnectivityType"

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mConnectivityType:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypeResId:I

    if-eq v0, v4, :cond_14

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "device_type_text_res_id"

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypeResId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_7
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mPairedTextResId:I

    if-eq v0, v4, :cond_15

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string/jumbo v1, "paired_text_res_id"

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mPairedTextResId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_8
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mNoDeviceTextResId:I

    if-eq v0, v4, :cond_16

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string/jumbo v1, "no_device_text_res_id"

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mNoDeviceTextResId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_9
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoTextResId:I

    if-eq v0, v4, :cond_17

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string/jumbo v1, "scanning_type_info_text_res_id"

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoTextResId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_a
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoContentIds:[I

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoContentIds:[I

    array-length v0, v0

    if-le v0, v6, :cond_18

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "info_content_ids"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoContentIds:[I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    :goto_b
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string/jumbo v1, "sub_tabs_content_res_ids"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContentResIds:[I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    :goto_c
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1a

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1a

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_1a

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    if-ne v0, v7, :cond_c

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mConnectivityType:I

    if-eq v0, v7, :cond_1a

    :cond_c
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I

    const/16 v1, 0x14

    if-eq v0, v1, :cond_1a

    :goto_d
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "is_no_support_scan"

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->isNoSupportScan:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->setArguments(Landroid/os/Bundle;)V

    :goto_e
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->scancontainer:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void

    :cond_d
    const-string v1, "HeaderText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "HeaderText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHeaderText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_e
    const-string v1, "DeviceTypeText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "DeviceTypeText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceText:Ljava/lang/String;

    goto/16 :goto_1

    :cond_f
    const-string v1, "PairedText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "PairedText"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mPairedText:Ljava/lang/String;

    goto/16 :goto_2

    :cond_10
    const-string v1, "NoDeviceString"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "NoDeviceString"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mNoDeviceText:Ljava/lang/String;

    goto/16 :goto_3

    :cond_11
    const-string v1, "info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoText:Ljava/lang/String;

    goto/16 :goto_4

    :cond_12
    const-string v1, "info_content"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "info_content"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoContentId:I

    goto/16 :goto_5

    :cond_13
    const-string/jumbo v1, "sub_tabs_content"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string/jumbo v1, "sub_tabs_content"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContent:[Ljava/lang/String;

    goto/16 :goto_6

    :cond_14
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "DeviceTypeText"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    :cond_15
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "PairedText"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mPairedText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_16
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "NoDeviceString"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mNoDeviceText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    :cond_17
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "info"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_18
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string v1, "info_content"

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mInfoContentId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_b

    :cond_19
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->args:Landroid/os/Bundle;

    const-string/jumbo v1, "sub_tabs_content"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mSubTabsContent:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_c

    :cond_1a
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->isNoSupportScan:Z

    goto/16 :goto_d

    :cond_1b
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    goto/16 :goto_e
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onPause()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->dismissPopUp()V

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p1}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceIDRename:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "Rename"

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceIDRename:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
