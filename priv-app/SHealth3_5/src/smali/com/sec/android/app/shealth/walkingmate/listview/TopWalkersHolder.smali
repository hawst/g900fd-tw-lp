.class public Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
.super Ljava/util/Observable;
.source "TopWalkersHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT:Ljava/lang/String; = "___DEFAULT___"

.field private static Ex_UrlImg:Ljava/lang/String;


# instance fields
.field private age:I

.field private calorie:I

.field private distance:J

.field private gender:Ljava/lang/String;

.field private iconSrc:Landroid/graphics/drawable/Drawable;

.field private iconUrl:Ljava/lang/String;

.field private iconback:Landroid/graphics/drawable/Drawable;

.field private name:Ljava/lang/String;

.field private ranking:I

.field private step:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->Ex_UrlImg:Ljava/lang/String;

    .line 225
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/drawable/Drawable;JIII)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "gender"    # Ljava/lang/String;
    .param p3, "age"    # I
    .param p4, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p5, "distance"    # J
    .param p7, "position"    # I
    .param p8, "step"    # I
    .param p9, "calorie"    # I

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 112
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    .line 113
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->gender:Ljava/lang/String;

    .line 114
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->age:I

    .line 115
    iput-object p4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    .line 116
    iput-wide p5, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->distance:J

    .line 117
    iput p7, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    .line 118
    iput p8, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->step:I

    .line 119
    iput p9, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->calorie:I

    .line 120
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JIII)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "gender"    # Ljava/lang/String;
    .param p3, "age"    # I
    .param p4, "iconUrl"    # Ljava/lang/String;
    .param p5, "distance"    # J
    .param p7, "position"    # I
    .param p8, "step"    # I
    .param p9, "calorie"    # I

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 125
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    .line 126
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->gender:Ljava/lang/String;

    .line 127
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->age:I

    .line 128
    iput-object p4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 129
    iput-wide p5, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->distance:J

    .line 130
    iput p7, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    .line 131
    iput p8, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->step:I

    .line 132
    iput p9, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->calorie:I

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    .line 134
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Landroid/graphics/drawable/Drawable;JIII)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "gender"    # Ljava/lang/String;
    .param p3, "age"    # I
    .param p4, "iconUrl"    # Ljava/lang/String;
    .param p5, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p6, "distance"    # J
    .param p8, "position"    # I
    .param p9, "step"    # I
    .param p10, "calorie"    # I

    .prologue
    .line 136
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 137
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    .line 138
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->gender:Ljava/lang/String;

    .line 139
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->age:I

    .line 140
    iput-object p4, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 141
    iput-object p5, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    .line 142
    iput-wide p6, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->distance:J

    .line 143
    iput p8, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    .line 144
    iput p9, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->step:I

    .line 145
    iput p10, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->calorie:I

    .line 146
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "json"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 46
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 47
    const-string/jumbo v0, "ranking"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    .line 48
    const-string/jumbo v0, "ranking"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ranking1 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    const-string/jumbo v0, "name"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-string/jumbo v0, "name"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    .line 52
    :cond_0
    const-string/jumbo v0, "step"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    const-string/jumbo v0, "step"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->step:I

    .line 55
    :cond_1
    const-string v0, "distance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    const-string v0, "distance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->distance:J

    .line 58
    :cond_2
    const-string v0, "calorie"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 59
    const-string v0, "calorie"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->calorie:I

    .line 61
    :cond_3
    const-string v0, "age"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 62
    const-string v0, "age"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->age:I

    .line 64
    :cond_4
    const-string v0, "gender"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 65
    const-string v0, "gender"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->gender:Ljava/lang/String;

    .line 67
    :cond_5
    const-string v0, "imgURL"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 68
    const-string v0, "imgURL"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    const-string v1, "\\"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 71
    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    .line 76
    :goto_0
    return-void

    .line 73
    :cond_6
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->Ex_UrlImg:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 74
    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public constructor <init>(Lorg/json/JSONObject;Z)V
    .locals 4
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "fromMy"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 78
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 79
    const-string/jumbo v0, "ranking"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    .line 81
    const-string/jumbo v0, "ranking"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ranking2 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const-string/jumbo v0, "name"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string/jumbo v0, "name"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    .line 85
    :cond_0
    const-string/jumbo v0, "step"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    const-string/jumbo v0, "step"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->step:I

    .line 88
    :cond_1
    const-string v0, "distance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    const-string v0, "distance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->distance:J

    .line 91
    :cond_2
    const-string v0, "calorie"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    const-string v0, "calorie"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->calorie:I

    .line 94
    :cond_3
    const-string v0, "age"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95
    const-string v0, "age"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->age:I

    .line 97
    :cond_4
    const-string v0, "gender"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 98
    const-string v0, "gender"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->gender:Ljava/lang/String;

    .line 100
    :cond_5
    const-string v0, "imgURL"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 101
    const-string v0, "imgURL"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    const-string v1, "\\"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 104
    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    .line 109
    :goto_0
    return-void

    .line 106
    :cond_6
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->Ex_UrlImg:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    .line 107
    iput-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return v0
.end method

.method public getAge()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->age:I

    return v0
.end method

.method public getCalorie()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->calorie:I

    return v0
.end method

.method public getDistance()J
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->distance:J

    return-wide v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->gender:Ljava/lang/String;

    return-object v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getIconback()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconback:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getImageBitmap()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getRanking()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    return v0
.end method

.method public getStep()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->step:I

    return v0
.end method

.method public getTrophy()Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;->getTrophy(I)Lcom/sec/android/app/shealth/walkingmate/dialog/Trophy;

    move-result-object v0

    return-object v0
.end method

.method public setIconSrc(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "iconSrc"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconSrc:Landroid/graphics/drawable/Drawable;

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setChanged()V

    .line 195
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->notifyObservers(Ljava/lang/Object;)V

    .line 196
    return-void
.end method

.method public setIconback(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "iconback"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconback:Landroid/graphics/drawable/Drawable;

    .line 206
    return-void
.end method

.method public setMyName(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    .line 190
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "content"    # I

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "___DEFAULT___"

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->gender:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "___DEFAULT___"

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 217
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->age:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "___DEFAULT___"

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 219
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->distance:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 220
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->ranking:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 221
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->step:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 222
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->calorie:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 223
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->name:Ljava/lang/String;

    goto :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->gender:Ljava/lang/String;

    goto :goto_1

    .line 218
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->iconUrl:Ljava/lang/String;

    goto :goto_2
.end method
