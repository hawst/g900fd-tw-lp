.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$2;
.super Ljava/lang/Object;
.source "ExerciseProDetailsMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setUpMapIfNeeded(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapClick(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2
    .param p1, "arg0"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 366
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapClickListener:Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 367
    new-instance v0, Landroid/view/View;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 368
    .local v0, "v":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 369
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapClickListener:Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;->onMapClick(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 371
    .end local v0    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method
