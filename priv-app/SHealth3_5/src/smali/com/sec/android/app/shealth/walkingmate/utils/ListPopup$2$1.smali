.class Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2$1;
.super Ljava/lang/Object;
.source "ListPopup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;

.field final synthetic val$selectedChild:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;

    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2$1;->val$selectedChild:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 347
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2$1;->val$selectedChild:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-result v0

    .line 348
    .local v0, "focusPassed":Z
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1600()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "focus was "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v1, ""

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, "passed"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1600()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "window = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;

    iget-object v3, v3, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1500(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;

    iget-object v1, v1, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1500(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Landroid/widget/ListView;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->refreshFocusables(Landroid/view/View;)V

    .line 351
    return-void

    .line 348
    :cond_0
    const-string/jumbo v1, "not"

    goto :goto_0
.end method
