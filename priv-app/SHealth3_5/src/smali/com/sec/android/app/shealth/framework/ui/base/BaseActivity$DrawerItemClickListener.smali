.class Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrawerItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v2, v0, [J

    const/4 v0, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v2, v0

    const/4 v0, 0x0

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v3, v2, v1

    long-to-int v1, v3

    if-gtz v1, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v3, 0x0

    int-to-long v0, p3

    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    ushr-long v4, v0, v4

    aget-wide v0, v2, v3

    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-eqz v6, :cond_1

    const-wide v6, -0x30b86fe7f9051141L    # -8.326396347532425E73

    xor-long/2addr v0, v6

    :cond_1
    const/16 v6, 0x20

    ushr-long/2addr v0, v6

    const/16 v6, 0x20

    shl-long/2addr v0, v6

    xor-long/2addr v0, v4

    const-wide v4, -0x30b86fe7f9051141L    # -8.326396347532425E73

    xor-long/2addr v0, v4

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$402(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x5a89

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, -0x5ab9

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v4, v0

    if-lt v2, v4, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_5

    const-wide v4, -0x30b86fe7f9051141L    # -8.326396347532425E73

    xor-long/2addr v0, v4

    :cond_5
    const/16 v4, 0x20

    shl-long/2addr v0, v4

    const/16 v4, 0x20

    shr-long/2addr v0, v4

    long-to-int v0, v0

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_8

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, 0x6845

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v4, 0x6875

    aput v4, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v4, v0

    if-lt v2, v4, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v4, v0

    if-lt v2, v4, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    aget v4, v1, v2

    int-to-char v4, v4

    aput-char v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    aget-wide v0, v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_9

    const-wide v4, -0x30b86fe7f9051141L    # -8.326396347532425E73

    xor-long/2addr v0, v4

    :cond_9
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v0, v0

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->selectItem(I)V
    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1400(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->resetSelectedPosition()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->access$1500(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V

    return-void
.end method
