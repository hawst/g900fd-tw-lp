.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->initPortionSizePopup(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private alreadySelected:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public resultOk(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V
    .locals 9
    .param p1, "portionSizeHolder"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .prologue
    const/4 v8, 0x0

    .line 282
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanelHolderForPortionSize:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$600(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    move-result-object v4

    if-nez v4, :cond_0

    .line 283
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getQuantity()F

    move-result v7

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getUnit()I

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;FI)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->putItem(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z

    .line 318
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshDoneButtonState()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$900(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    .line 319
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->refreshFragmentFocusables()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    .line 321
    return-void

    .line 288
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->getSelectedItems()Ljava/util/ArrayList;

    move-result-object v3

    .line 289
    .local v3, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 291
    .local v1, "mMealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 293
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->alreadySelected:Z

    .line 302
    .end local v1    # "mMealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->alreadySelected:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFromSelectPanel:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanelHolderForPortionSize:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$600(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getMealItemData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    move-result-object v2

    .line 305
    .local v2, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getUnit()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setUnit(I)V

    .line 306
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getQuantity()F

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setAmount(F)V

    .line 307
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanelHolderForPortionSize:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
    invoke-static {v5}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$600(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->putItem(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z

    .line 308
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->alreadySelected:Z

    .line 309
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->isFromSelectPanel:Z
    invoke-static {v4, v8}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$802(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;Z)Z

    goto :goto_0

    .line 298
    .end local v2    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .restart local v1    # "mMealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->alreadySelected:Z

    goto :goto_1

    .line 312
    .end local v1    # "mMealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mSelectedPanel:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$000(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$4;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mFoodInfoDataForPortionSize:Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-static {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getQuantity()F

    move-result v7

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getUnit()I

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;FI)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanel;->putItem(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z

    goto/16 :goto_0
.end method
