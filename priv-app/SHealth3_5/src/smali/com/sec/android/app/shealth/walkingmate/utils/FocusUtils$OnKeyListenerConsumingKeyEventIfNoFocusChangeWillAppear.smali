.class public Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils$OnKeyListenerConsumingKeyEventIfNoFocusChangeWillAppear;
.super Ljava/lang/Object;
.source "FocusUtils.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OnKeyListenerConsumingKeyEventIfNoFocusChangeWillAppear"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 328
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 331
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/walkingmate/utils/FocusUtils;->focusWillChange(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method
