.class Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;
.super Ljava/lang/Object;
.source "RestoreManager.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/backup/server/RestoreManager;->start(Lcom/sec/android/app/shealth/backup/server/IRestoreListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 143
    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "start(): onReceived()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # setter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mUserToken:Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$102(Lcom/sec/android/app/shealth/backup/server/RestoreManager;Ljava/lang/String;)Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$300(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mAccountID:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$202(Lcom/sec/android/app/shealth/backup/server/RestoreManager;Ljava/lang/String;)Ljava/lang/String;

    .line 147
    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mUserToken"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mUserToken:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$100(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isRestoreStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$400(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "mUserToken ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mUserToken:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$100(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  mAccountID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mAccountID:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$200(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mDeviceID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mDeviceID:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$500(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$600(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 153
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x6b

    iput v1, v0, Landroid/os/Message;->what:I

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$600(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 156
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 160
    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "start(): setFailureMessage()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isRestoreStarted:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$400(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # invokes: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStopped()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$700(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$800(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onAccountException(Ljava/lang/String;)V

    .line 166
    :cond_0
    return-void
.end method

.method public setNetworkFailure()V
    .locals 2

    .prologue
    .line 170
    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "start(): setNetworkFailure()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->isRestoreStarted:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$400(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # invokes: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->restoreStopped()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$700(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/backup/server/RestoreManager$1;->this$0:Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    # getter for: Lcom/sec/android/app/shealth/backup/server/RestoreManager;->mRestoreListener:Lcom/sec/android/app/shealth/backup/server/IRestoreListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->access$800(Lcom/sec/android/app/shealth/backup/server/RestoreManager;)Lcom/sec/android/app/shealth/backup/server/IRestoreListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/backup/server/IRestoreListener;->onAccountException(Ljava/lang/String;)V

    .line 176
    :cond_0
    return-void
.end method
