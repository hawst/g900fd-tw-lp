.class public Lcom/sec/android/app/shealth/framework/ui/receiver/GenericPluginAppBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field private static final ACTIONS:Ljava/lang/String; = "action"

.field private static final APP_ACCESSORY_SUPPORT_KEY:Ljava/lang/String; = "health_accessory_support"

.field private static final APP_CATEGORY_KEY:Ljava/lang/String; = "health_app_category"

.field private static final APP_DELETABLE_DB_KEY:Ljava/lang/String; = "health_db_deletable"

.field private static final APP_DELETE_DB_INFO_KEY:Ljava/lang/String; = "health_delete_db_info"

.field public static final APP_DOWNLOADED:I = 0x1

.field private static final APP_SPP_STATUS:Ljava/lang/String; = "health_push_enable"

.field public static final APP_TYPE:Ljava/lang/String; = "app_type"

.field private static final CATEGORY_OTHER:Ljava/lang/String; = "Others"

.field private static final DEFAULT_PLUGIN_ICON_NAME:Ljava/lang/String; = "ic_launcher"

.field private static final SHEALTH_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.shealth"

.field private static final TAG:Ljava/lang/String; = "GenericPluginAppBroadcastReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.shealth.request.package.information"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    new-instance v15, Landroid/content/Intent;

    const-string v2, "com.sec.shealth.send.package.information"

    invoke-direct {v15, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :cond_1
    const/16 v16, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v14, 0x0

    const/4 v3, 0x0

    const/16 v17, 0x0

    :try_start_0
    move/from16 v0, v17

    invoke-virtual {v13, v2, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v14

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x80

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v13

    iget-object v13, v13, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v13, :cond_2

    const-string v17, "app_type"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v17, "action"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v6

    :try_start_2
    const-string v17, "app_icon"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v3

    :try_start_3
    const-string v17, "health_app_category"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v12

    :try_start_4
    const-string v17, "health_db_deletable"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_4 .. :try_end_4} :catch_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_5

    move-result-object v11

    :try_start_5
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const-string v18, "health_delete_db_info"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_5
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_6

    move-result-object v10

    :goto_1
    :try_start_6
    const-string v17, "health_accessory_support"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_6
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_6 .. :try_end_6} :catch_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_7

    move-result-object v9

    :try_start_7
    const-string v17, "health_measure_type"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    const-string v17, "health_push_enable"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_7 .. :try_end_7} :catch_f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_7 .. :try_end_7} :catch_8

    move-result-object v8

    :cond_2
    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move/from16 v19, v7

    move-wide v6, v4

    move-object v4, v3

    move-object v3, v12

    move-object v12, v10

    move/from16 v10, v19

    :goto_2
    if-eqz v14, :cond_4

    const-string v5, "app_names"

    invoke-virtual {v15, v5, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_3
    if-eqz v4, :cond_5

    const-string v5, "icon_names"

    invoke-virtual {v15, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_4
    if-eqz v2, :cond_3

    if-eqz v14, :cond_3

    if-lez v10, :cond_3

    const/4 v5, 0x4

    if-ge v10, v5, :cond_3

    if-eqz v9, :cond_3

    if-nez v4, :cond_6

    :cond_3
    const-string v2, "GenericPluginAppBroadcastReceiver"

    const-string v3, "Improper data from downloadable app "

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v17

    :try_start_8
    const-string v17, "GenericPluginAppBroadcastReceiver"

    const-string v18, "Plugin doesn\'t have resource specified"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_1

    :catch_1
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move-wide/from16 v20, v4

    move-object v5, v14

    move-object v4, v3

    move-object/from16 v3, v19

    move-object v14, v12

    move-object v12, v10

    move v10, v7

    move-wide/from16 v6, v20

    :goto_5
    invoke-virtual {v3}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    move-object v3, v14

    move-object v14, v5

    goto :goto_2

    :catch_2
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v10

    move-object v10, v9

    move v9, v7

    move-object v7, v6

    move-wide/from16 v20, v4

    move-wide/from16 v5, v20

    move-object v4, v3

    move-object/from16 v3, v19

    :goto_6
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v3, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v10

    move v10, v9

    move-object v9, v7

    move-wide/from16 v19, v5

    move-wide/from16 v6, v19

    goto :goto_2

    :cond_4
    const-string v5, "app_names"

    move-object/from16 v0, v16

    invoke-virtual {v15, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    :cond_5
    const-string v5, "icon_names"

    const-string v16, "ic_launcher"

    move-object/from16 v0, v16

    invoke-virtual {v15, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_4

    :cond_6
    const-string/jumbo v4, "package_name"

    invoke-virtual {v15, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "application_name"

    invoke-virtual {v15, v2, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-nez v3, :cond_8

    const-string v2, "Others"

    :goto_7
    const-string v3, "application_category"

    invoke-virtual {v15, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "app_deletable_db"

    invoke-virtual {v15, v2, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "app_delete_db_info"

    invoke-virtual {v15, v2, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "support_accessory"

    invoke-virtual {v15, v2, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "health_measure_type"

    invoke-virtual {v15, v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v2, "spp_enabled"

    invoke-virtual {v15, v2, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "app_type"

    invoke-virtual {v15, v2, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "actions"

    invoke-virtual {v15, v2, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    const-string v2, "GenericPluginAppBroadcastReceiver"

    const-string v3, "Action not defined so not supposed to receiver it "

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_3
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v10

    move-object v10, v9

    move v9, v7

    move-object v7, v6

    move-wide/from16 v20, v4

    move-wide/from16 v5, v20

    move-object v4, v3

    move-object/from16 v3, v19

    goto :goto_6

    :catch_4
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v10

    move-object v10, v9

    move v9, v7

    move-object v7, v6

    move-wide/from16 v20, v4

    move-wide/from16 v5, v20

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_6

    :catch_5
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v10

    move-object v10, v9

    move v9, v7

    move-object v7, v6

    move-wide/from16 v20, v4

    move-wide/from16 v5, v20

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_6

    :catch_6
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v10

    move-object v10, v9

    move v9, v7

    move-object v7, v6

    move-wide/from16 v20, v4

    move-wide/from16 v5, v20

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_6

    :catch_7
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v10

    move-object v10, v9

    move v9, v7

    move-object v7, v6

    move-wide/from16 v20, v4

    move-wide/from16 v5, v20

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_6

    :catch_8
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v12

    move-object v12, v11

    move-object v11, v10

    move-object v10, v9

    move v9, v7

    move-object v7, v6

    move-wide/from16 v20, v4

    move-wide/from16 v5, v20

    move-object v4, v3

    move-object/from16 v3, v19

    goto/16 :goto_6

    :catch_9
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move-wide/from16 v20, v4

    move-object v5, v14

    move-object v4, v3

    move-object/from16 v3, v19

    move-object v14, v12

    move-object v12, v10

    move v10, v7

    move-wide/from16 v6, v20

    goto/16 :goto_5

    :catch_a
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move-wide/from16 v20, v4

    move-object v5, v14

    move-object v4, v3

    move-object/from16 v3, v19

    move-object v14, v12

    move-object v12, v10

    move v10, v7

    move-wide/from16 v6, v20

    goto/16 :goto_5

    :catch_b
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move-wide/from16 v20, v4

    move-object v5, v14

    move-object v4, v3

    move-object/from16 v3, v19

    move-object v14, v12

    move-object v12, v10

    move v10, v7

    move-wide/from16 v6, v20

    goto/16 :goto_5

    :catch_c
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move-wide/from16 v20, v4

    move-object v5, v14

    move-object v4, v3

    move-object/from16 v3, v19

    move-object v14, v12

    move-object v12, v10

    move v10, v7

    move-wide/from16 v6, v20

    goto/16 :goto_5

    :catch_d
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move-wide/from16 v20, v4

    move-object v5, v14

    move-object v4, v3

    move-object/from16 v3, v19

    move-object v14, v12

    move-object v12, v10

    move v10, v7

    move-wide/from16 v6, v20

    goto/16 :goto_5

    :catch_e
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move-wide/from16 v20, v4

    move-object v5, v14

    move-object v4, v3

    move-object/from16 v3, v19

    move-object v14, v12

    move-object v12, v10

    move v10, v7

    move-wide/from16 v6, v20

    goto/16 :goto_5

    :catch_f
    move-exception v13

    move-object/from16 v19, v13

    move-object v13, v11

    move-object v11, v9

    move-object v9, v6

    move-wide/from16 v20, v4

    move-object v5, v14

    move-object v4, v3

    move-object/from16 v3, v19

    move-object v14, v12

    move-object v12, v10

    move v10, v7

    move-wide/from16 v6, v20

    goto/16 :goto_5

    :cond_8
    move-object v2, v3

    goto/16 :goto_7
.end method
