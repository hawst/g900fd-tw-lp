.class Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;
.super Ljava/lang/Object;
.source "WalkingTimePicker.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->initDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 5
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    .line 99
    const v1, 0x7f080a8c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    .line 101
    .local v0, "timePicker":Landroid/widget/TimePicker;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mFromDate:Ljava/util/Date;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->access$000(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;)Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 129
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$2;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;Landroid/widget/TimePicker;)V

    const/4 v2, 0x0

    const-wide/16 v3, 0x64

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1$2;->sendEmptyMessageDelayed(IJ)Z

    .line 139
    return-void
.end method
