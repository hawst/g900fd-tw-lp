.class Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;
.super Ljava/lang/Object;
.source "GeneralMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QuickInputDialogController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V
    .locals 0

    .prologue
    .line 671
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;

    .prologue
    .line 671
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 1
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 677
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne v0, p1, :cond_0

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateQuickInput()V

    .line 684
    :cond_0
    return-void
.end method
