.class Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;
.super Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;
.source "MealImagesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MealImageScaleCopyTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "inputImagePath"    # Ljava/lang/String;
    .param p3, "outputImagePath"    # Ljava/lang/String;

    .prologue
    .line 501
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    .line 502
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->notifyActivityAboutLoading()V
    invoke-static {p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V

    .line 504
    # operator++ for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I
    invoke-static {p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$1208(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)I

    .line 505
    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isSuccessful"    # Ljava/lang/Boolean;

    .prologue
    .line 509
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 510
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # operator-- for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$1210(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)I

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mLoadingImagesCount:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$1200(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)I

    move-result v0

    if-nez v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->setMealImage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$1300(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;Ljava/lang/String;)V

    .line 515
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->refreshLastTakenPhoto(Landroid/content/Context;)V

    .line 517
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 499
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$MealImageScaleCopyTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
