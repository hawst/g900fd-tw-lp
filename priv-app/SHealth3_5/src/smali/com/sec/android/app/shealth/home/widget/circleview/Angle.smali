.class final Lcom/sec/android/app/shealth/home/widget/circleview/Angle;
.super Ljava/lang/Object;
.source "Angle.java"


# static fields
.field static final ZERO:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;


# instance fields
.field public final degreeValue:F

.field public final radianValue:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    invoke-direct {v0, v1, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;-><init>(FF)V

    sput-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->ZERO:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    return-void
.end method

.method private constructor <init>(FF)V
    .locals 0
    .param p1, "degreeValue"    # F
    .param p2, "radianValue"    # F

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->degreeValue:F

    .line 27
    iput p2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->radianValue:F

    .line 28
    return-void
.end method

.method static fromDegrees(F)Lcom/sec/android/app/shealth/home/widget/circleview/Angle;
    .locals 5
    .param p0, "degreeValue"    # F

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    float-to-double v1, p0

    const-wide v3, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v1, v3

    const-wide v3, 0x4066800000000000L    # 180.0

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;-><init>(FF)V

    return-object v0
.end method

.method static fromRadians(F)Lcom/sec/android/app/shealth/home/widget/circleview/Angle;
    .locals 5
    .param p0, "radianValue"    # F

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v1, p0

    float-to-double v1, v1

    const-wide v3, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;-><init>(FF)V

    return-object v0
.end method
