.class public Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;
.super Ljava/lang/Object;
.source "CalendarFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Ljava/util/GregorianCalendar;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 21
    .local v0, "gc":Ljava/util/GregorianCalendar;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isWeekStartsFromSunday()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setFirstDayOfWeek(I)V

    .line 27
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 28
    return-object v0

    .line 21
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public static getInstance(III)Ljava/util/GregorianCalendar;
    .locals 2
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I

    .prologue
    .line 57
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p0, p1, p2}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 58
    .local v0, "gc":Ljava/util/GregorianCalendar;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isWeekStartsFromSunday()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setFirstDayOfWeek(I)V

    .line 64
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 65
    return-object v0

    .line 58
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public static getInstance(J)Ljava/util/GregorianCalendar;
    .locals 2
    .param p0, "time"    # J

    .prologue
    .line 75
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 76
    .local v0, "gc":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 77
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isWeekStartsFromSunday()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setFirstDayOfWeek(I)V

    .line 83
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 84
    return-object v0

    .line 77
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public static getInstance(Ljava/util/TimeZone;)Ljava/util/GregorianCalendar;
    .locals 2
    .param p0, "tz"    # Ljava/util/TimeZone;

    .prologue
    .line 40
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p0}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 41
    .local v0, "gc":Ljava/util/GregorianCalendar;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->isWeekStartsFromSunday()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setFirstDayOfWeek(I)V

    .line 42
    return-object v0

    .line 41
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method
