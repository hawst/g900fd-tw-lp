.class Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;
.super Ljava/lang/Object;
.source "AutoBackupActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v5, 0x1

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->setEnableListView(Z)V
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Z)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getAutoBackupInterval(Landroid/content/Context;)J

    move-result-wide v1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSyncInterval:J
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$102(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;J)J

    .line 213
    if-nez p2, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mPeriodicSyncManager:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->cancelPeriodicSync(Landroid/content/Context;)Z

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupSwitchStatus(Landroid/content/Context;Z)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->interval:[J
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$400()[J

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Landroid/content/Context;[J)V

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$302(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIntervalListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->notifyDataSetChanged()V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->refreshWithoutContent()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V

    .line 220
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->scheduleReminderNotification(Landroid/content/Context;ILcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    .line 234
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mPeriodicSyncManager:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSyncInterval:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->setPeroidicSync(Landroid/content/Context;J)V

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupSwitchStatus(Landroid/content/Context;Z)V

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->interval:[J
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$400()[J

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Landroid/content/Context;[J)V

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$302(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIntervalListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->notifyDataSetChanged()V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->refreshContent()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$800(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)V

    .line 232
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->cancelReminderNotification(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)V

    goto :goto_0
.end method
