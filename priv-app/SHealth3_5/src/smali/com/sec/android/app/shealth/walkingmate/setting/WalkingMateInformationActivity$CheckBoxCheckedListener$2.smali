.class Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;
.super Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;
.source "WalkingMateInformationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->initializeDropDownAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;Landroid/view/View;IZ)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;
    .param p3, "x1"    # I
    .param p4, "x2"    # Z

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;->this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;-><init>(Landroid/view/View;IZ)V

    return-void
.end method


# virtual methods
.method public afterChanges()V
    .locals 5

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;->this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->access$300(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)Landroid/widget/ScrollView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 227
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;->this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->access$300(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;->this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mTitle:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->access$400(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/FocusUtils;->getRectDescribingViewFully(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;->this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a015f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->scrollTo(II)V

    goto :goto_0
.end method

.method public finished()V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;->this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;->this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->mContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->access$100(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener$2;->this$1:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity$CheckBoxCheckedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->refreshFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;->access$500(Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInformationActivity;)V

    .line 236
    return-void
.end method
