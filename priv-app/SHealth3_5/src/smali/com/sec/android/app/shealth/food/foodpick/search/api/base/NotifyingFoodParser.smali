.class public abstract Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;
.super Ljava/lang/Object;
.source "NotifyingFoodParser.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodParser;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodParser",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private mListener:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method


# virtual methods
.method protected final notifyFoodParsingComplete(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 1
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 41
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;->mListener:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;->mListener:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;->onFoodParsingComplete(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    .line 44
    :cond_0
    return-void
.end method

.method public setOnFoodParsingCompleteListener(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;

    .prologue
    .line 32
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser<TE;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser;->mListener:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/NotifyingFoodParser$OnFoodParsingCompleteListener;

    .line 33
    return-void
.end method
