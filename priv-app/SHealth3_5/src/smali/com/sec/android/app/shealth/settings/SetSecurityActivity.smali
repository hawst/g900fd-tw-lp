.class public Lcom/sec/android/app/shealth/settings/SetSecurityActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "SetSecurityActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/SetSecurityActivity$4;
    }
.end annotation


# static fields
.field protected static final ADD_SHEALTH_ACCOUNT:Ljava/lang/String; = "com.sec.android.service.health.cp.serversync.syncadapter.authenticator.SHEALTH_ACCOUNT"

.field protected static final REQUEST_CODE:I = 0x3e7

.field protected static final REQUEST_CODE_CHANGE_PWD:I = 0x65

.field protected static final REQUEST_CODE_POPUP_LOCKSCREEN:I = 0x3e6

.field private static final REQUEST_SIGNIN_LOCKSCREEN:Ljava/lang/String; = "request_signin_lockscreen"

.field private static final REQUEST_SIGNIN_PINCODE:Ljava/lang/String; = "request_signin_pincode"

.field private static final SAMSUNG_ACCOUNT_DISABLE:Ljava/lang/String; = "samsung_account_disable"


# instance fields
.field private isLockScreenLayoutVisible:Z

.field private mChangePinLayout:Landroid/widget/LinearLayout;

.field private mLockScreenCB:Landroid/widget/CheckBox;

.field private mPinCodeCB:Landroid/widget/CheckBox;

.field private mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->isLockScreenLayoutVisible:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/SetSecurityActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/SetSecurityActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V

    return-void
.end method

.method private createSignInPopup(Ljava/lang/String;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 261
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    const v3, 0x7f09086e

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    .line 262
    .local v0, "signInBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f09088c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 263
    const v1, 0x7f09004c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 264
    const v1, 0x7f090873

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 265
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method private initLayout()V
    .locals 4

    .prologue
    const v3, 0x7f0808db

    const/4 v2, 0x1

    .line 75
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mChangePinLayout:Landroid/widget/LinearLayout;

    .line 76
    const v0, 0x7f0808da

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v1, "security_pin_enabled"

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 87
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V

    .line 92
    :goto_0
    const v0, 0x7f0808d9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    return-void

    .line 89
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V

    goto :goto_0
.end method

.method private launchPinCodeActivity(ZZ)V
    .locals 3
    .param p1, "isSigned"    # Z
    .param p2, "isChangePassword"    # Z

    .prologue
    .line 250
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 251
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_0

    .line 252
    const-string v1, "changePassword"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 256
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->startActivity(Landroid/content/Intent;)V

    .line 257
    return-void

    .line 254
    :cond_0
    const-string/jumbo v2, "state"

    if-nez p1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private setChangePinEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mChangePinLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mChangePinLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 173
    const v0, 0x7f0808dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 182
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 166
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907bb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 168
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 271
    const-string/jumbo v0, "request_signin_pincode"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    new-instance v0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/SetSecurityActivity;)V

    .line 343
    :goto_0
    return-object v0

    .line 297
    :cond_0
    const-string/jumbo v0, "request_signin_lockscreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    new-instance v0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/SetSecurityActivity;)V

    goto :goto_0

    .line 321
    :cond_1
    const-string/jumbo v0, "samsung_account_disable"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 322
    new-instance v0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/SetSecurityActivity;)V

    goto :goto_0

    .line 343
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const v7, 0x7f090870

    const v6, 0x7f0900e1

    const/4 v5, -0x1

    const/4 v3, 0x0

    .line 99
    const/16 v0, 0xd

    .line 101
    .local v0, "SAMSUNG_ACCOUNT_DISABLED":I
    const/16 v4, 0x65

    if-ne p1, v4, :cond_2

    .line 103
    if-eq v5, p2, :cond_1

    .line 105
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 106
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-virtual {v4, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 107
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v3, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 135
    :cond_1
    :goto_0
    return-void

    .line 110
    :cond_2
    const/16 v4, 0x3e7

    if-ne p1, v4, :cond_5

    .line 111
    if-eq p2, v5, :cond_3

    if-nez p2, :cond_4

    .line 112
    :cond_3
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "changePassword"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 114
    const-string/jumbo v4, "state"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 115
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 116
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_4
    if-ne p2, v0, :cond_1

    .line 117
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 118
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 119
    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 120
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string/jumbo v5, "samsung_account_disable"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    .end local v1    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_5
    const/16 v4, 0x3e6

    if-ne p1, v4, :cond_1

    .line 124
    if-eq p2, v5, :cond_1

    .line 128
    if-ne p2, v0, :cond_1

    .line 129
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 130
    .restart local v1    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 131
    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 132
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string/jumbo v5, "samsung_account_disable"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "compoundButton"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v1

    const v2, 0x7f0808da

    if-ne v1, v2, :cond_0

    .line 231
    if-eqz p2, :cond_1

    .line 232
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 233
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V

    .line 234
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->startActivity(Landroid/content/Intent;)V

    .line 235
    invoke-virtual {p1, v3}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 246
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V

    .line 238
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 239
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string/jumbo v1, "state"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 240
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V

    .line 241
    const/16 v1, 0x65

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 242
    invoke-virtual {p1, v3}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v5, 0x7f0808db

    const v4, 0x7f0808d9

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 186
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    .line 187
    .local v0, "isSignedSamsungAccount":Z
    if-eqz v0, :cond_3

    .line 188
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 190
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->launchPinCodeActivity(ZZ)V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-nez v4, :cond_2

    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 201
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v4, :cond_4

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 205
    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->launchPinCodeActivity(ZZ)V

    goto :goto_0

    .line 207
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v3, v5, :cond_5

    .line 209
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->launchPinCodeActivity(ZZ)V

    goto :goto_0

    .line 216
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 217
    const-string/jumbo v1, "request_signin_pincode"

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->createSignInPopup(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    const v0, 0x7f0301fb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setContentView(I)V

    .line 67
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    .line 68
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 69
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->isLockScreenLayoutVisible:Z

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->initLayout()V

    .line 72
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 139
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const-string/jumbo v1, "security_pin_enabled"

    invoke-virtual {v0, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 145
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V

    .line 155
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 162
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->mPinCodeCB:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 151
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/settings/SetSecurityActivity;->setChangePinEnabled(Z)V

    goto :goto_0
.end method
