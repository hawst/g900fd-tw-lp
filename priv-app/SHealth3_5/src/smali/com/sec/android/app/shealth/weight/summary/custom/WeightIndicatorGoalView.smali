.class Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;
.super Landroid/view/View;
.source "WeightIndicatorGoalView.java"


# static fields
.field private static final sStartArcAngle:I = -0x5a


# instance fields
.field private mArcAngle:F

.field private mGoalArcPaint:Landroid/graphics/Paint;

.field private mRadius:I

.field private mViewRect:Landroid/graphics/Rect;

.field private mViewRectf:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->initialize()V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->initialize()V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->initialize()V

    .line 58
    return-void
.end method

.method private initialize()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->initializeGoalLinePaint()V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->resetView()V

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mViewRect:Landroid/graphics/Rect;

    .line 64
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mViewRectf:Landroid/graphics/RectF;

    .line 65
    return-void
.end method

.method private initializeGoalLinePaint()V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mGoalArcPaint:Landroid/graphics/Paint;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mGoalArcPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07010c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 79
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mViewRectf:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mViewRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mViewRectf:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mArcAngle:F

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mGoalArcPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 95
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 84
    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mRadius:I

    mul-int/lit8 v0, v1, 0x2

    .line 85
    .local v0, "size":I
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->setMeasuredDimension(II)V

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mViewRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 87
    return-void
.end method

.method public resetView()V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mGoalArcPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0053

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->calculateAlpha(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 72
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mArcAngle:F

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->invalidate()V

    .line 74
    return-void
.end method

.method setRadius(I)V
    .locals 0
    .param p1, "radius"    # I

    .prologue
    .line 103
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->mRadius:I

    .line 104
    return-void
.end method
