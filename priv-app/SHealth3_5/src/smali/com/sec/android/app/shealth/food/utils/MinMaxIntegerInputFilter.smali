.class public abstract Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;
.super Ljava/lang/Object;
.source "MinMaxIntegerInputFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# static fields
.field public static final CASE_FLOAT:I = 0x1

.field public static final CASE_INT:I = 0x2

.field public static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final sDefaultNumberSeparator:Ljava/lang/String; = "."


# instance fields
.field private caseFilter:I

.field private mMaxValue:F

.field private mMinValue:F

.field private mSystemNumberSeparator:Ljava/lang/String;


# direct methods
.method public constructor <init>(FFI)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F
    .param p3, "switchCase"    # I

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v0

    invoke-virtual {v0}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mSystemNumberSeparator:Ljava/lang/String;

    .line 50
    iput p1, p0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mMinValue:F

    .line 51
    iput p2, p0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mMaxValue:F

    .line 53
    iput p3, p0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->caseFilter:I

    .line 54
    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 16
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "sourceStart"    # I
    .param p3, "sourceEnd"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "destStart"    # I
    .param p6, "destEnd"    # I

    .prologue
    .line 67
    :try_start_0
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move/from16 v0, p5

    invoke-virtual {v14, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    move/from16 v0, p6

    invoke-virtual {v14, v0, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 69
    .local v7, "newValue":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    move/from16 v0, p5

    invoke-virtual {v7, v14, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v14

    move/from16 v0, p5

    invoke-virtual {v7, v0, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 72
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v14, "."

    invoke-virtual {v7, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 75
    const-string v13, ""

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 77
    const-string v13, ""

    .line 188
    .end local v7    # "newValue":Ljava/lang/String;
    :goto_0
    return-object v13

    .line 80
    .restart local v7    # "newValue":Ljava/lang/String;
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, "."

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 82
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->caseFilter:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_1

    .line 84
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->showInvalidInputToast()V

    .line 85
    const/4 v13, 0x0

    goto :goto_0

    .line 87
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mSystemNumberSeparator:Ljava/lang/String;

    goto :goto_0

    .line 90
    :cond_2
    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    .line 92
    .local v6, "input":F
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, "."

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 94
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->caseFilter:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_3

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->showInvalidInputToast()V

    .line 97
    const/4 v13, 0x0

    goto :goto_0

    .line 99
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mSystemNumberSeparator:Ljava/lang/String;

    goto :goto_0

    .line 106
    :cond_4
    move-object v8, v7

    .line 108
    .local v8, "str":Ljava/lang/String;
    const-string v13, "\\."

    invoke-virtual {v8, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, "array":[Ljava/lang/String;
    array-length v13, v1

    const/4 v14, 0x2

    if-ne v13, v14, :cond_9

    .line 112
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v15, "."

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 114
    .local v9, "str2":Ljava/lang/String;
    const-string v13, "\\."

    invoke-virtual {v9, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "array2":[Ljava/lang/String;
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mSystemNumberSeparator:Ljava/lang/String;

    const-string v15, "."

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 118
    .local v10, "str3":Ljava/lang/String;
    const-string v13, "\\."

    invoke-virtual {v10, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 120
    .local v3, "array3":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->caseFilter:I

    packed-switch v13, :pswitch_data_0

    .line 155
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 124
    :pswitch_0
    const/4 v13, 0x0

    aget-object v13, v2, v13

    goto/16 :goto_0

    .line 129
    :pswitch_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mMaxValue:F

    cmpl-float v13, v6, v13

    if-lez v13, :cond_5

    .line 131
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->showAlertToast()V

    .line 132
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 135
    :cond_5
    const-string v13, ""

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 137
    invoke-static {v9}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    .line 138
    .local v11, "strFloat":Ljava/lang/Float;
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToTenth(F)F

    move-result v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v13

    const-string v14, "."

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mSystemNumberSeparator:Ljava/lang/String;

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_0

    .line 141
    .end local v11    # "strFloat":Ljava/lang/Float;
    :cond_6
    array-length v13, v3

    const/4 v14, 0x1

    if-ne v13, v14, :cond_7

    .line 143
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 144
    .local v12, "strInt":Ljava/lang/Integer;
    invoke-virtual {v12}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_0

    .line 147
    .end local v12    # "strInt":Ljava/lang/Integer;
    :cond_7
    const/4 v13, 0x1

    aget-object v13, v1, v13

    invoke-virtual {v13}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x2

    if-ge v13, v14, :cond_8

    .line 149
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 152
    :cond_8
    const-string v13, ""

    goto/16 :goto_0

    .line 162
    .end local v2    # "array2":[Ljava/lang/String;
    .end local v3    # "array3":[Ljava/lang/String;
    .end local v9    # "str2":Ljava/lang/String;
    .end local v10    # "str3":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mMaxValue:F

    cmpg-float v13, v6, v13

    if-gtz v13, :cond_a

    .line 164
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 167
    :cond_a
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->mMaxValue:F

    cmpl-float v13, v6, v13

    if-lez v13, :cond_b

    .line 169
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->showAlertToast()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 170
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 173
    .end local v1    # "array":[Ljava/lang/String;
    .end local v6    # "input":F
    .end local v7    # "newValue":Ljava/lang/String;
    .end local v8    # "str":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 175
    .local v5, "exception":Ljava/lang/NumberFormatException;
    const-class v13, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 187
    .end local v5    # "exception":Ljava/lang/NumberFormatException;
    :cond_b
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;->showInvalidInputToast()V

    .line 188
    const-string v13, ""

    goto/16 :goto_0

    .line 178
    :catch_1
    move-exception v4

    .line 182
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    const-class v13, Lcom/sec/android/app/shealth/food/utils/MinMaxIntegerInputFilter;

    invoke-static {v13}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public abstract showAlertToast()V
.end method

.method public abstract showInvalidInputToast()V
.end method
