.class final Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;
.super Ljava/util/HashMap;
.source "HeartrateTagUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getTag(I)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/Integer;",
        "Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 9

    .prologue
    const/16 v8, 0x526d

    const/16 v5, 0x5208

    const/16 v4, 0x4e20

    const v7, 0x7f090f08

    const v6, 0x7f02053d

    .line 26
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 27
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const v3, 0x7f090c0d

    invoke-direct {v1, v2, v4, v3, v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    invoke-direct {v1, v2, v5, v7, v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    const/16 v0, 0x5335

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x5335

    const v4, 0x7f090f0b

    const v5, 0x7f020538

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const v3, 0x7f090f0d

    const v4, 0x7f020539

    invoke-direct {v1, v2, v8, v3, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    const/16 v0, 0x526f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x526f

    const v4, 0x7f090f0e

    const v5, 0x7f020541

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const/16 v0, 0x52d3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x52d3

    const v4, 0x7f090f24

    const v5, 0x7f020530

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const/16 v0, 0x52d6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x52d6

    const v4, 0x7f090f27

    const v5, 0x7f020536

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const/16 v0, 0x52d5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x52d5

    const v4, 0x7f090f26

    const v5, 0x7f020533

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const/16 v0, 0x52d2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x52d2

    const v4, 0x7f090f23

    const v5, 0x7f02053c

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const/16 v0, 0x52d1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x52d1

    const v4, 0x7f090f22

    const v5, 0x7f020535

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const/16 v0, 0x52d4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x52d4

    const v4, 0x7f090f25

    const v5, 0x7f02053a

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const/16 v0, 0x5336

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x5336

    const v4, 0x7f090f2d

    const v5, 0x7f0204d5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const/16 v0, 0x5337

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x5337

    const v4, 0x7f090f2e

    const v5, 0x7f020537

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    const/16 v0, 0x5338

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x5338

    const v4, 0x7f090f2f

    const v5, 0x7f020531

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const/16 v0, 0x5339

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x5339

    const v4, 0x7f090f30

    const v5, 0x7f020532

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const/16 v0, 0x533a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x533a

    const v4, 0x7f090f31

    const v5, 0x7f02053b

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const/16 v0, 0x533b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x533b

    invoke-direct {v1, v2, v3, v7, v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const/16 v0, 0x533c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;

    sget-object v2, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;->TAG_TYPE_PREDEFINED:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;

    const/16 v3, 0x533c

    invoke-direct {v1, v2, v3, v7, v6}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$Tag;-><init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTag$TagType;III)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method
