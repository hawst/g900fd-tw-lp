.class public Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;
.super Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;
.source "WeightLogActivity.java"


# static fields
.field private static final SHARING_DATA_SEPARATOR:Ljava/lang/String; = ", "

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContentObserver:Landroid/database/ContentObserver;

.field private mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->ALL:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;-><init>(Lcom/sec/android/app/shealth/logutils/log/LogFilter;)V

    .line 55
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    .line 83
    return-void
.end method


# virtual methods
.method protected createConcreteDelegate()Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;-><init>(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;)V

    return-object v0
.end method

.method public getNoDataImageResource()I
    .locals 1

    .prologue
    .line 87
    const v0, 0x7f0205b7

    return v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 13

    .prologue
    .line 119
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    .local v4, "stringBuilder":Ljava/lang/StringBuilder;
    new-instance v7, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    .line 122
    .local v7, "unitHelper":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 123
    .local v2, "dateFormat":Ljava/text/DateFormat;
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v6

    .line 125
    .local v6, "timeFormat":Ljava/text/DateFormat;
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v9

    .line 127
    .local v9, "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 128
    .local v5, "tag":Ljava/lang/String;
    const-string v10, "_"

    invoke-virtual {v5, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    aget-object v10, v10, v11

    invoke-static {v10}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 129
    .local v0, "dataId":J
    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-interface {v10, v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 131
    .local v8, "weightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    const-string v10, "("

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    const-string v10, " "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const-string v10, ", "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v10

    invoke-static {v10, v9}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 137
    invoke-virtual {v9, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string v10, ")"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    const-string v10, "\n"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v10, "\n"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 144
    .end local v0    # "dataId":J
    .end local v5    # "tag":Ljava/lang/String;
    .end local v8    # "weightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 146
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    const-string v11, ", "

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v11

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 147
    const-string v10, "\n"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "com.sec.android.app.shealth.weight"

    const-string v12, "WT02"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.weight"

    const-string v2, "WT04"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onDestroy()V

    .line 79
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->unregisterContentObserver()V

    .line 74
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onPause()V

    .line 75
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 93
    .local v0, "state":Z
    const v2, 0x7f080c8b

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09110b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 94
    sget-object v2, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v3, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->PRELOAD:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v3, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->DOWNLOAD_MR:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 95
    :cond_0
    const v2, 0x7f080c8c

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 96
    .local v1, "viewByMenuItem":Landroid/view/MenuItem;
    if-eqz v1, :cond_1

    .line 97
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 100
    .end local v1    # "viewByMenuItem":Landroid/view/MenuItem;
    :cond_1
    return v0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 67
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->onResume()V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->registerContentObserver()V

    .line 69
    return-void
.end method

.method protected registerContentObserver()V
    .locals 4

    .prologue
    .line 160
    new-instance v0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity$1;-><init>(Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mContentObserver:Landroid/database/ContentObserver;

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->updateProfile()V

    .line 179
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->registerContentObserver()V

    .line 180
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 5
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 110
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 111
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->getIdFromTag(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 112
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    const-string v2, "INPUT_ACTIVITY_MODE_KEY"

    const-string v3, "Edit"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const-string v2, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->startActivity(Landroid/content/Intent;)V

    .line 115
    return-void
.end method

.method protected unregisterContentObserver()V
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0}, Landroid/database/ContentObserver;->releaseContentObserver()Landroid/database/IContentObserver;

    .line 219
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/log/LogListVersatileActivity;->unregisterContentObserver()V

    .line 220
    return-void
.end method

.method protected updateProfile()V
    .locals 6

    .prologue
    .line 186
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 188
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v2

    .line 190
    .local v2, "weightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-eqz v2, :cond_0

    .line 196
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 197
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->refreshAdapter()V

    .line 211
    return-void

    .line 200
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
