.class public Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
.super Ljava/lang/Object;
.source "HeartrateData.java"


# instance fields
.field private HRM_AGE_12_TO_15:I

.field private HRM_AGE_16_TO_19:I

.field private HRM_AGE_20_TO_39:I

.field private HRM_AGE_2_TO_3:I

.field private HRM_AGE_40_TO_59:I

.field private HRM_AGE_4_TO_5:I

.field private HRM_AGE_60_TO_79:I

.field private HRM_AGE_6_TO_8:I

.field private HRM_AGE_80_AND_ABOVE:I

.field private HRM_AGE_9_TO_11:I

.field private HRM_AGE_ONE:I

.field private HRM_AGE_UNDER_ONE:I

.field private average:F

.field private comment:Ljava/lang/String;

.field private deviceName:Ljava/lang/String;

.field private id:J

.field private mContext:Landroid/content/Context;

.field private maxData:F

.field private meanHeartRate:F

.field private minData:F

.field private pulse_data:[[[I

.field private range_data:[[[I

.field private sampleTime:J

.field private startTime:J

.field private tag:Ljava/lang/String;

.field private tagIndex:Ljava/lang/String;

.field private userDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0xb

    const/4 v4, 0x2

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->userDeviceId:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->comment:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->deviceName:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->tag:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->tagIndex:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->maxData:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->minData:F

    .line 69
    new-array v0, v4, [[[I

    const/16 v1, 0xc

    new-array v1, v1, [[I

    new-array v2, v5, [I

    fill-array-data v2, :array_0

    aput-object v2, v1, v6

    new-array v2, v5, [I

    fill-array-data v2, :array_1

    aput-object v2, v1, v7

    new-array v2, v5, [I

    fill-array-data v2, :array_2

    aput-object v2, v1, v4

    new-array v2, v5, [I

    fill-array-data v2, :array_3

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-array v3, v5, [I

    fill-array-data v3, :array_4

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-array v3, v5, [I

    fill-array-data v3, :array_5

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v5, [I

    fill-array-data v3, :array_6

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v5, [I

    fill-array-data v3, :array_7

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v5, [I

    fill-array-data v3, :array_8

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v5, [I

    fill-array-data v3, :array_9

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v5, [I

    fill-array-data v3, :array_a

    aput-object v3, v1, v2

    new-array v2, v5, [I

    fill-array-data v2, :array_b

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/16 v1, 0xc

    new-array v1, v1, [[I

    new-array v2, v5, [I

    fill-array-data v2, :array_c

    aput-object v2, v1, v6

    new-array v2, v5, [I

    fill-array-data v2, :array_d

    aput-object v2, v1, v7

    new-array v2, v5, [I

    fill-array-data v2, :array_e

    aput-object v2, v1, v4

    new-array v2, v5, [I

    fill-array-data v2, :array_f

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-array v3, v5, [I

    fill-array-data v3, :array_10

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-array v3, v5, [I

    fill-array-data v3, :array_11

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v5, [I

    fill-array-data v3, :array_12

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v5, [I

    fill-array-data v3, :array_13

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v5, [I

    fill-array-data v3, :array_14

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v5, [I

    fill-array-data v3, :array_15

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v5, [I

    fill-array-data v3, :array_16

    aput-object v3, v1, v2

    new-array v2, v5, [I

    fill-array-data v2, :array_17

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    .line 100
    new-array v0, v4, [[[I

    const/16 v1, 0xc

    new-array v1, v1, [[I

    new-array v2, v4, [I

    fill-array-data v2, :array_18

    aput-object v2, v1, v6

    new-array v2, v4, [I

    fill-array-data v2, :array_19

    aput-object v2, v1, v7

    new-array v2, v4, [I

    fill-array-data v2, :array_1a

    aput-object v2, v1, v4

    new-array v2, v4, [I

    fill-array-data v2, :array_1b

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-array v3, v4, [I

    fill-array-data v3, :array_1c

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-array v3, v4, [I

    fill-array-data v3, :array_1d

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v4, [I

    fill-array-data v3, :array_1e

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v4, [I

    fill-array-data v3, :array_1f

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v4, [I

    fill-array-data v3, :array_20

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v4, [I

    fill-array-data v3, :array_21

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v4, [I

    fill-array-data v3, :array_22

    aput-object v3, v1, v2

    new-array v2, v4, [I

    fill-array-data v2, :array_23

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/16 v1, 0xc

    new-array v1, v1, [[I

    new-array v2, v4, [I

    fill-array-data v2, :array_24

    aput-object v2, v1, v6

    new-array v2, v4, [I

    fill-array-data v2, :array_25

    aput-object v2, v1, v7

    new-array v2, v4, [I

    fill-array-data v2, :array_26

    aput-object v2, v1, v4

    new-array v2, v4, [I

    fill-array-data v2, :array_27

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-array v3, v4, [I

    fill-array-data v3, :array_28

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-array v3, v4, [I

    fill-array-data v3, :array_29

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v4, [I

    fill-array-data v3, :array_2a

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v4, [I

    fill-array-data v3, :array_2b

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v4, [I

    fill-array-data v3, :array_2c

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v4, [I

    fill-array-data v3, :array_2d

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v4, [I

    fill-array-data v3, :array_2e

    aput-object v3, v1, v2

    new-array v2, v4, [I

    fill-array-data v2, :array_2f

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    .line 131
    iput v6, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_UNDER_ONE:I

    .line 132
    iput v7, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_ONE:I

    .line 133
    iput v4, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_2_TO_3:I

    .line 134
    iput v8, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_4_TO_5:I

    .line 135
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_6_TO_8:I

    .line 136
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_9_TO_11:I

    .line 137
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_12_TO_15:I

    .line 138
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_16_TO_19:I

    .line 139
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_20_TO_39:I

    .line 140
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_40_TO_59:I

    .line 141
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_60_TO_79:I

    .line 142
    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_80_AND_ABOVE:I

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->mContext:Landroid/content/Context;

    .line 145
    return-void

    .line 69
    nop

    :array_0
    .array-data 4
        0x54
        0x62
        0x66
        0x6b
        0x73
        0x7d
        0x89
        0x94
        0x9b
        0xa0
        0xab
    .end array-data

    :array_1
    .array-data 4
        -0x1
        0x5b
        0x5f
        0x64
        0x6b
        0x72
        0x7a
        0x83
        0x89
        0x92
        0x9c
    .end array-data

    :array_2
    .array-data 4
        0x4a
        0x52
        0x55
        0x59
        0x60
        0x68
        0x70
        0x77
        0x7c
        0x83
        0x8b
    .end array-data

    :array_3
    .array-data 4
        0x45
        0x47
        0x4a
        0x4b
        0x54
        0x5c
        0x64
        0x6c
        0x70
        0x74
        0x78
    .end array-data

    :array_4
    .array-data 4
        0x3b
        0x3f
        0x42
        0x46
        0x4c
        0x53
        0x5c
        0x64
        0x69
        0x6d
        0x72
    .end array-data

    :array_5
    .array-data 4
        0x38
        0x3b
        0x3d
        0x42
        0x46
        0x4e
        0x56
        0x5e
        0x61
        0x66
        0x6e
    .end array-data

    :array_6
    .array-data 4
        0x34
        0x36
        0x39
        0x3c
        0x42
        0x4a
        0x53
        0x5b
        0x61
        0x66
        0x6c
    .end array-data

    :array_7
    .array-data 4
        0x2e
        0x32
        0x34
        0x38
        0x3d
        0x45
        0x4e
        0x57
        0x5c
        0x5f
        0x68
    .end array-data

    :array_8
    .array-data 4
        0x2f
        0x32
        0x34
        0x37
        0x3d
        0x45
        0x4c
        0x54
        0x59
        0x5f
        0x65
    .end array-data

    :array_9
    .array-data 4
        0x2e
        0x31
        0x34
        0x37
        0x3d
        0x44
        0x4d
        0x55
        0x5a
        0x5f
        0x68
    .end array-data

    :array_a
    .array-data 4
        0x2d
        0x30
        0x32
        0x36
        0x3c
        0x43
        0x4b
        0x54
        0x5b
        0x62
        0x66
    .end array-data

    :array_b
    .array-data 4
        -0x1
        0x30
        0x33
        0x36
        0x3d
        0x44
        0x4e
        0x56
        0x5e
        0x61
        -0x1
    .end array-data

    :array_c
    .array-data 4
        0x60
        0x63
        0x68
        0x6c
        0x76
        0x7f
        0x89
        0x96
        0x9c
        0xa3
        0xae
    .end array-data

    :array_d
    .array-data 4
        0x52
        0x5c
        0x5f
        0x65
        0x6e
        0x75
        0x7d
        0x87
        0x8b
        0x8f
        0x9e
    .end array-data

    :array_e
    .array-data 4
        0x4e
        0x53
        0x58
        0x5b
        0x62
        0x6b
        0x72
        0x78
        0x7d
        0x82
        0x89
    .end array-data

    :array_f
    .array-data 4
        0x46
        0x49
        0x4c
        0x51
        0x57
        0x5f
        0x68
        0x6e
        0x75
        0x7a
        0x84
    .end array-data

    :array_10
    .array-data 4
        0x3d
        0x42
        0x45
        0x49
        0x4f
        0x57
        0x5e
        0x65
        0x6a
        0x6d
        0x75
    .end array-data

    :array_11
    .array-data 4
        0x3a
        0x3f
        0x42
        0x45
        0x4c
        0x53
        0x5b
        0x62
        0x67
        0x6b
        0x71
    .end array-data

    :array_12
    .array-data 4
        0x36
        0x39
        0x3c
        0x3f
        0x46
        0x4f
        0x57
        0x5e
        0x63
        0x67
        0x6e
    .end array-data

    :array_13
    .array-data 4
        0x32
        0x36
        0x3a
        0x3e
        0x45
        0x4d
        0x55
        0x5e
        0x63
        0x67
        0x6c
    .end array-data

    :array_14
    .array-data 4
        0x34
        0x37
        0x39
        0x3c
        0x42
        0x4a
        0x52
        0x59
        0x5f
        0x63
        0x68
    .end array-data

    :array_15
    .array-data 4
        0x33
        0x35
        0x38
        0x3b
        0x40
        0x47
        0x4f
        0x56
        0x5c
        0x61
        0x65
    .end array-data

    :array_16
    .array-data 4
        0x34
        0x36
        0x38
        0x3b
        0x40
        0x46
        0x4e
        0x56
        0x5c
        0x60
        0x66
    .end array-data

    :array_17
    .array-data 4
        -0x1
        0x35
        0x38
        0x3b
        0x40
        0x47
        0x4d
        0x55
        0x5d
        0x62
        0x64
    .end array-data

    .line 100
    :array_18
    .array-data 4
        0x54
        0xab
    .end array-data

    :array_19
    .array-data 4
        0x5b
        0x9c
    .end array-data

    :array_1a
    .array-data 4
        0x4a
        0x8b
    .end array-data

    :array_1b
    .array-data 4
        0x45
        0x78
    .end array-data

    :array_1c
    .array-data 4
        0x3b
        0x72
    .end array-data

    :array_1d
    .array-data 4
        0x38
        0x6e
    .end array-data

    :array_1e
    .array-data 4
        0x34
        0x6c
    .end array-data

    :array_1f
    .array-data 4
        0x2e
        0x68
    .end array-data

    :array_20
    .array-data 4
        0x2f
        0x65
    .end array-data

    :array_21
    .array-data 4
        0x2e
        0x68
    .end array-data

    :array_22
    .array-data 4
        0x2d
        0x66
    .end array-data

    :array_23
    .array-data 4
        0x30
        0x61
    .end array-data

    :array_24
    .array-data 4
        0x60
        0xae
    .end array-data

    :array_25
    .array-data 4
        0x52
        0x9e
    .end array-data

    :array_26
    .array-data 4
        0x4e
        0x89
    .end array-data

    :array_27
    .array-data 4
        0x46
        0x84
    .end array-data

    :array_28
    .array-data 4
        0x3d
        0x75
    .end array-data

    :array_29
    .array-data 4
        0x3a
        0x71
    .end array-data

    :array_2a
    .array-data 4
        0x36
        0x6e
    .end array-data

    :array_2b
    .array-data 4
        0x32
        0x6c
    .end array-data

    :array_2c
    .array-data 4
        0x34
        0x68
    .end array-data

    :array_2d
    .array-data 4
        0x33
        0x65
    .end array-data

    :array_2e
    .array-data 4
        0x34
        0x66
    .end array-data

    :array_2f
    .array-data 4
        0x35
        0x64
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0xb

    const/4 v4, 0x2

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->userDeviceId:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->comment:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->deviceName:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->tag:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->tagIndex:Ljava/lang/String;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->maxData:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->minData:F

    .line 69
    new-array v0, v4, [[[I

    const/16 v1, 0xc

    new-array v1, v1, [[I

    new-array v2, v5, [I

    fill-array-data v2, :array_0

    aput-object v2, v1, v6

    new-array v2, v5, [I

    fill-array-data v2, :array_1

    aput-object v2, v1, v7

    new-array v2, v5, [I

    fill-array-data v2, :array_2

    aput-object v2, v1, v4

    new-array v2, v5, [I

    fill-array-data v2, :array_3

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-array v3, v5, [I

    fill-array-data v3, :array_4

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-array v3, v5, [I

    fill-array-data v3, :array_5

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v5, [I

    fill-array-data v3, :array_6

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v5, [I

    fill-array-data v3, :array_7

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v5, [I

    fill-array-data v3, :array_8

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v5, [I

    fill-array-data v3, :array_9

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v5, [I

    fill-array-data v3, :array_a

    aput-object v3, v1, v2

    new-array v2, v5, [I

    fill-array-data v2, :array_b

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/16 v1, 0xc

    new-array v1, v1, [[I

    new-array v2, v5, [I

    fill-array-data v2, :array_c

    aput-object v2, v1, v6

    new-array v2, v5, [I

    fill-array-data v2, :array_d

    aput-object v2, v1, v7

    new-array v2, v5, [I

    fill-array-data v2, :array_e

    aput-object v2, v1, v4

    new-array v2, v5, [I

    fill-array-data v2, :array_f

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-array v3, v5, [I

    fill-array-data v3, :array_10

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-array v3, v5, [I

    fill-array-data v3, :array_11

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v5, [I

    fill-array-data v3, :array_12

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v5, [I

    fill-array-data v3, :array_13

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v5, [I

    fill-array-data v3, :array_14

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v5, [I

    fill-array-data v3, :array_15

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v5, [I

    fill-array-data v3, :array_16

    aput-object v3, v1, v2

    new-array v2, v5, [I

    fill-array-data v2, :array_17

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    .line 100
    new-array v0, v4, [[[I

    const/16 v1, 0xc

    new-array v1, v1, [[I

    new-array v2, v4, [I

    fill-array-data v2, :array_18

    aput-object v2, v1, v6

    new-array v2, v4, [I

    fill-array-data v2, :array_19

    aput-object v2, v1, v7

    new-array v2, v4, [I

    fill-array-data v2, :array_1a

    aput-object v2, v1, v4

    new-array v2, v4, [I

    fill-array-data v2, :array_1b

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-array v3, v4, [I

    fill-array-data v3, :array_1c

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-array v3, v4, [I

    fill-array-data v3, :array_1d

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v4, [I

    fill-array-data v3, :array_1e

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v4, [I

    fill-array-data v3, :array_1f

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v4, [I

    fill-array-data v3, :array_20

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v4, [I

    fill-array-data v3, :array_21

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v4, [I

    fill-array-data v3, :array_22

    aput-object v3, v1, v2

    new-array v2, v4, [I

    fill-array-data v2, :array_23

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    const/16 v1, 0xc

    new-array v1, v1, [[I

    new-array v2, v4, [I

    fill-array-data v2, :array_24

    aput-object v2, v1, v6

    new-array v2, v4, [I

    fill-array-data v2, :array_25

    aput-object v2, v1, v7

    new-array v2, v4, [I

    fill-array-data v2, :array_26

    aput-object v2, v1, v4

    new-array v2, v4, [I

    fill-array-data v2, :array_27

    aput-object v2, v1, v8

    const/4 v2, 0x4

    new-array v3, v4, [I

    fill-array-data v3, :array_28

    aput-object v3, v1, v2

    const/4 v2, 0x5

    new-array v3, v4, [I

    fill-array-data v3, :array_29

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v4, [I

    fill-array-data v3, :array_2a

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v4, [I

    fill-array-data v3, :array_2b

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v4, [I

    fill-array-data v3, :array_2c

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v4, [I

    fill-array-data v3, :array_2d

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v4, [I

    fill-array-data v3, :array_2e

    aput-object v3, v1, v2

    new-array v2, v4, [I

    fill-array-data v2, :array_2f

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    .line 131
    iput v6, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_UNDER_ONE:I

    .line 132
    iput v7, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_ONE:I

    .line 133
    iput v4, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_2_TO_3:I

    .line 134
    iput v8, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_4_TO_5:I

    .line 135
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_6_TO_8:I

    .line 136
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_9_TO_11:I

    .line 137
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_12_TO_15:I

    .line 138
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_16_TO_19:I

    .line 139
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_20_TO_39:I

    .line 140
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_40_TO_59:I

    .line 141
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_60_TO_79:I

    .line 142
    iput v5, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_80_AND_ABOVE:I

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->mContext:Landroid/content/Context;

    .line 148
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->mContext:Landroid/content/Context;

    .line 149
    return-void

    .line 69
    nop

    :array_0
    .array-data 4
        0x54
        0x62
        0x66
        0x6b
        0x73
        0x7d
        0x89
        0x94
        0x9b
        0xa0
        0xab
    .end array-data

    :array_1
    .array-data 4
        -0x1
        0x5b
        0x5f
        0x64
        0x6b
        0x72
        0x7a
        0x83
        0x89
        0x92
        0x9c
    .end array-data

    :array_2
    .array-data 4
        0x4a
        0x52
        0x55
        0x59
        0x60
        0x68
        0x70
        0x77
        0x7c
        0x83
        0x8b
    .end array-data

    :array_3
    .array-data 4
        0x45
        0x47
        0x4a
        0x4b
        0x54
        0x5c
        0x64
        0x6c
        0x70
        0x74
        0x78
    .end array-data

    :array_4
    .array-data 4
        0x3b
        0x3f
        0x42
        0x46
        0x4c
        0x53
        0x5c
        0x64
        0x69
        0x6d
        0x72
    .end array-data

    :array_5
    .array-data 4
        0x38
        0x3b
        0x3d
        0x42
        0x46
        0x4e
        0x56
        0x5e
        0x61
        0x66
        0x6e
    .end array-data

    :array_6
    .array-data 4
        0x34
        0x36
        0x39
        0x3c
        0x42
        0x4a
        0x53
        0x5b
        0x61
        0x66
        0x6c
    .end array-data

    :array_7
    .array-data 4
        0x2e
        0x32
        0x34
        0x38
        0x3d
        0x45
        0x4e
        0x57
        0x5c
        0x5f
        0x68
    .end array-data

    :array_8
    .array-data 4
        0x2f
        0x32
        0x34
        0x37
        0x3d
        0x45
        0x4c
        0x54
        0x59
        0x5f
        0x65
    .end array-data

    :array_9
    .array-data 4
        0x2e
        0x31
        0x34
        0x37
        0x3d
        0x44
        0x4d
        0x55
        0x5a
        0x5f
        0x68
    .end array-data

    :array_a
    .array-data 4
        0x2d
        0x30
        0x32
        0x36
        0x3c
        0x43
        0x4b
        0x54
        0x5b
        0x62
        0x66
    .end array-data

    :array_b
    .array-data 4
        -0x1
        0x30
        0x33
        0x36
        0x3d
        0x44
        0x4e
        0x56
        0x5e
        0x61
        -0x1
    .end array-data

    :array_c
    .array-data 4
        0x60
        0x63
        0x68
        0x6c
        0x76
        0x7f
        0x89
        0x96
        0x9c
        0xa3
        0xae
    .end array-data

    :array_d
    .array-data 4
        0x52
        0x5c
        0x5f
        0x65
        0x6e
        0x75
        0x7d
        0x87
        0x8b
        0x8f
        0x9e
    .end array-data

    :array_e
    .array-data 4
        0x4e
        0x53
        0x58
        0x5b
        0x62
        0x6b
        0x72
        0x78
        0x7d
        0x82
        0x89
    .end array-data

    :array_f
    .array-data 4
        0x46
        0x49
        0x4c
        0x51
        0x57
        0x5f
        0x68
        0x6e
        0x75
        0x7a
        0x84
    .end array-data

    :array_10
    .array-data 4
        0x3d
        0x42
        0x45
        0x49
        0x4f
        0x57
        0x5e
        0x65
        0x6a
        0x6d
        0x75
    .end array-data

    :array_11
    .array-data 4
        0x3a
        0x3f
        0x42
        0x45
        0x4c
        0x53
        0x5b
        0x62
        0x67
        0x6b
        0x71
    .end array-data

    :array_12
    .array-data 4
        0x36
        0x39
        0x3c
        0x3f
        0x46
        0x4f
        0x57
        0x5e
        0x63
        0x67
        0x6e
    .end array-data

    :array_13
    .array-data 4
        0x32
        0x36
        0x3a
        0x3e
        0x45
        0x4d
        0x55
        0x5e
        0x63
        0x67
        0x6c
    .end array-data

    :array_14
    .array-data 4
        0x34
        0x37
        0x39
        0x3c
        0x42
        0x4a
        0x52
        0x59
        0x5f
        0x63
        0x68
    .end array-data

    :array_15
    .array-data 4
        0x33
        0x35
        0x38
        0x3b
        0x40
        0x47
        0x4f
        0x56
        0x5c
        0x61
        0x65
    .end array-data

    :array_16
    .array-data 4
        0x34
        0x36
        0x38
        0x3b
        0x40
        0x46
        0x4e
        0x56
        0x5c
        0x60
        0x66
    .end array-data

    :array_17
    .array-data 4
        -0x1
        0x35
        0x38
        0x3b
        0x40
        0x47
        0x4d
        0x55
        0x5d
        0x62
        0x64
    .end array-data

    .line 100
    :array_18
    .array-data 4
        0x54
        0xab
    .end array-data

    :array_19
    .array-data 4
        0x5b
        0x9c
    .end array-data

    :array_1a
    .array-data 4
        0x4a
        0x8b
    .end array-data

    :array_1b
    .array-data 4
        0x45
        0x78
    .end array-data

    :array_1c
    .array-data 4
        0x3b
        0x72
    .end array-data

    :array_1d
    .array-data 4
        0x38
        0x6e
    .end array-data

    :array_1e
    .array-data 4
        0x34
        0x6c
    .end array-data

    :array_1f
    .array-data 4
        0x2e
        0x68
    .end array-data

    :array_20
    .array-data 4
        0x2f
        0x65
    .end array-data

    :array_21
    .array-data 4
        0x2e
        0x68
    .end array-data

    :array_22
    .array-data 4
        0x2d
        0x66
    .end array-data

    :array_23
    .array-data 4
        0x30
        0x61
    .end array-data

    :array_24
    .array-data 4
        0x60
        0xae
    .end array-data

    :array_25
    .array-data 4
        0x52
        0x9e
    .end array-data

    :array_26
    .array-data 4
        0x4e
        0x89
    .end array-data

    :array_27
    .array-data 4
        0x46
        0x84
    .end array-data

    :array_28
    .array-data 4
        0x3d
        0x75
    .end array-data

    :array_29
    .array-data 4
        0x3a
        0x71
    .end array-data

    :array_2a
    .array-data 4
        0x36
        0x6e
    .end array-data

    :array_2b
    .array-data 4
        0x32
        0x6c
    .end array-data

    :array_2c
    .array-data 4
        0x34
        0x68
    .end array-data

    :array_2d
    .array-data 4
        0x33
        0x65
    .end array-data

    :array_2e
    .array-data 4
        0x34
        0x66
    .end array-data

    :array_2f
    .array-data 4
        0x35
        0x64
    .end array-data
.end method

.method private isSamsungAccount()Z
    .locals 13

    .prologue
    .line 473
    const/4 v7, 0x0

    .line 474
    .local v7, "isSamAccount":Z
    const-string/jumbo v0, "samstresstest@gmail.com"

    .line 475
    .local v0, "TEST_EMAIL_ADDRESS":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->mContext:Landroid/content/Context;

    invoke-static {v10}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 476
    .local v2, "accountMgr":Landroid/accounts/AccountManager;
    if-eqz v2, :cond_1

    .line 477
    invoke-virtual {v2}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    .line 478
    .local v3, "accounts":[Landroid/accounts/Account;
    sget-object v5, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    .line 480
    .local v5, "emailPattern":Ljava/util/regex/Pattern;
    move-object v4, v3

    .local v4, "arr$":[Landroid/accounts/Account;
    array-length v8, v4

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v8, :cond_1

    aget-object v1, v4, v6

    .line 481
    .local v1, "account":Landroid/accounts/Account;
    iget-object v10, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 482
    iget-object v9, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 483
    .local v9, "possibleEmail":Ljava/lang/String;
    const-string v10, "kmanik"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "possibleEmail = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 485
    const/4 v7, 0x1

    .line 480
    .end local v9    # "possibleEmail":Ljava/lang/String;
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 489
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v3    # "accounts":[Landroid/accounts/Account;
    .end local v4    # "arr$":[Landroid/accounts/Account;
    .end local v5    # "emailPattern":Ljava/util/regex/Pattern;
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    :cond_1
    return v7
.end method


# virtual methods
.method public getAccessoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getAverage()F
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->average:F

    return v0
.end method

.method public getAvgBPMRange(II)[I
    .locals 6
    .param p1, "gender"    # I
    .param p2, "userAge"    # I

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 249
    new-array v1, v2, [I

    .line 250
    .local v1, "avgRange":[I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserAge()I

    move-result v0

    .line 252
    .local v0, "CurrentUserAge":I
    if-gtz p2, :cond_0

    .line 253
    move p2, v0

    .line 255
    :cond_0
    if-ge p2, v4, :cond_2

    .line 256
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_UNDER_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 257
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_UNDER_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    .line 293
    :cond_1
    :goto_0
    return-object v1

    .line 258
    :cond_2
    if-ne p2, v4, :cond_3

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 260
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto :goto_0

    .line 261
    :cond_3
    if-lt p2, v2, :cond_4

    const/4 v2, 0x3

    if-gt p2, v2, :cond_4

    .line 262
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_2_TO_3:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_2_TO_3:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto :goto_0

    .line 264
    :cond_4
    const/4 v2, 0x4

    if-lt p2, v2, :cond_5

    const/4 v2, 0x5

    if-gt p2, v2, :cond_5

    .line 265
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_4_TO_5:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 266
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_4_TO_5:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto :goto_0

    .line 267
    :cond_5
    const/4 v2, 0x6

    if-lt p2, v2, :cond_6

    const/16 v2, 0x8

    if-gt p2, v2, :cond_6

    .line 268
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_6_TO_8:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_6_TO_8:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto :goto_0

    .line 270
    :cond_6
    const/16 v2, 0x9

    if-lt p2, v2, :cond_7

    const/16 v2, 0xb

    if-gt p2, v2, :cond_7

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_9_TO_11:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 272
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_9_TO_11:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto/16 :goto_0

    .line 273
    :cond_7
    const/16 v2, 0xc

    if-lt p2, v2, :cond_8

    const/16 v2, 0xf

    if-gt p2, v2, :cond_8

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_12_TO_15:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 275
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_12_TO_15:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto/16 :goto_0

    .line 276
    :cond_8
    const/16 v2, 0x10

    if-lt p2, v2, :cond_9

    const/16 v2, 0x13

    if-gt p2, v2, :cond_9

    .line 277
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_16_TO_19:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 278
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_16_TO_19:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto/16 :goto_0

    .line 279
    :cond_9
    const/16 v2, 0x14

    if-lt p2, v2, :cond_a

    const/16 v2, 0x27

    if-gt p2, v2, :cond_a

    .line 280
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_20_TO_39:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 281
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_20_TO_39:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto/16 :goto_0

    .line 282
    :cond_a
    const/16 v2, 0x28

    if-lt p2, v2, :cond_b

    const/16 v2, 0x3b

    if-gt p2, v2, :cond_b

    .line 283
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_40_TO_59:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_40_TO_59:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto/16 :goto_0

    .line 285
    :cond_b
    const/16 v2, 0x3c

    if-lt p2, v2, :cond_c

    const/16 v2, 0x4f

    if-gt p2, v2, :cond_c

    .line 286
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_60_TO_79:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_60_TO_79:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto/16 :goto_0

    .line 288
    :cond_c
    const/16 v2, 0x50

    if-lt p2, v2, :cond_1

    .line 289
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_80_AND_ABOVE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v5

    .line 290
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->range_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_80_AND_ABOVE:I

    aget-object v2, v2, v3

    aget v2, v2, v4

    aput v2, v1, v4

    goto/16 :goto_0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getHRMSateBarColor(III[I[I)Ljava/lang/String;
    .locals 4
    .param p1, "bpm"    # I
    .param p2, "userAge"    # I
    .param p3, "gender"    # I
    .param p4, "resting_percentile"    # [I
    .param p5, "avg_range"    # [I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 396
    const-string v0, "#000000"

    .line 397
    .local v0, "color":Ljava/lang/String;
    aget v1, p5, v2

    if-lt p1, v1, :cond_0

    aget v1, p5, v3

    if-le p1, v1, :cond_2

    .line 398
    :cond_0
    const-string v0, "#a3e9ad"

    .line 404
    :cond_1
    :goto_0
    return-object v0

    .line 399
    :cond_2
    aget v1, p5, v2

    if-lt p1, v1, :cond_3

    aget v1, p4, v2

    if-lt p1, v1, :cond_4

    :cond_3
    aget v1, p5, v3

    if-le p1, v1, :cond_4

    aget v1, p4, v3

    if-le p1, v1, :cond_5

    .line 400
    :cond_4
    const-string v0, "#77dab1"

    goto :goto_0

    .line 401
    :cond_5
    aget v1, p4, v2

    if-lt p1, v1, :cond_1

    aget v1, p4, v3

    if-gt p1, v1, :cond_1

    .line 402
    const-string v0, "#42c0ad"

    goto :goto_0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->id:J

    return-wide v0
.end method

.method public getMaleOrFemale()I
    .locals 2

    .prologue
    .line 409
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserGender()I

    move-result v0

    .line 411
    .local v0, "gender":I
    const v1, 0x2e636

    if-ne v0, v1, :cond_0

    .line 412
    const/4 v1, 0x1

    .line 414
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaxData()F
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->maxData:F

    return v0
.end method

.method public getMeanHeartRate()F
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->meanHeartRate:F

    return v0
.end method

.method public getMinData()F
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->minData:F

    return v0
.end method

.method public getRestingPulsePercentile(II)[I
    .locals 8
    .param p1, "gender"    # I
    .param p2, "userAge"    # I

    .prologue
    const/4 v2, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v4, 0x1

    .line 346
    new-array v1, v2, [I

    .line 347
    .local v1, "percent":[I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserAge()I

    move-result v0

    .line 349
    .local v0, "CurrentUserAge":I
    if-gtz p2, :cond_0

    .line 350
    move p2, v0

    .line 352
    :cond_0
    if-ge p2, v4, :cond_2

    .line 353
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_UNDER_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 354
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_UNDER_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    .line 390
    :cond_1
    :goto_0
    return-object v1

    .line 355
    :cond_2
    if-ne p2, v4, :cond_3

    .line 356
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 357
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto :goto_0

    .line 358
    :cond_3
    if-lt p2, v2, :cond_4

    const/4 v2, 0x3

    if-gt p2, v2, :cond_4

    .line 359
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_2_TO_3:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 360
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_2_TO_3:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto :goto_0

    .line 361
    :cond_4
    if-lt p2, v5, :cond_5

    const/4 v2, 0x5

    if-gt p2, v2, :cond_5

    .line 362
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_4_TO_5:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 363
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_4_TO_5:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto :goto_0

    .line 364
    :cond_5
    if-lt p2, v6, :cond_6

    const/16 v2, 0x8

    if-gt p2, v2, :cond_6

    .line 365
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_6_TO_8:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 366
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_6_TO_8:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto :goto_0

    .line 367
    :cond_6
    const/16 v2, 0x9

    if-lt p2, v2, :cond_7

    const/16 v2, 0xb

    if-gt p2, v2, :cond_7

    .line 368
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_9_TO_11:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 369
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_9_TO_11:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto/16 :goto_0

    .line 370
    :cond_7
    const/16 v2, 0xc

    if-lt p2, v2, :cond_8

    const/16 v2, 0xf

    if-gt p2, v2, :cond_8

    .line 371
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_12_TO_15:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 372
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_12_TO_15:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto/16 :goto_0

    .line 373
    :cond_8
    const/16 v2, 0x10

    if-lt p2, v2, :cond_9

    const/16 v2, 0x13

    if-gt p2, v2, :cond_9

    .line 374
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_16_TO_19:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 375
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_16_TO_19:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto/16 :goto_0

    .line 376
    :cond_9
    const/16 v2, 0x14

    if-lt p2, v2, :cond_a

    const/16 v2, 0x27

    if-gt p2, v2, :cond_a

    .line 377
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_20_TO_39:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 378
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_20_TO_39:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto/16 :goto_0

    .line 379
    :cond_a
    const/16 v2, 0x28

    if-lt p2, v2, :cond_b

    const/16 v2, 0x3b

    if-gt p2, v2, :cond_b

    .line 380
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_40_TO_59:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 381
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_40_TO_59:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto/16 :goto_0

    .line 382
    :cond_b
    const/16 v2, 0x3c

    if-lt p2, v2, :cond_c

    const/16 v2, 0x4f

    if-gt p2, v2, :cond_c

    .line 383
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_60_TO_79:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 384
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_60_TO_79:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto/16 :goto_0

    .line 385
    :cond_c
    const/16 v2, 0x50

    if-lt p2, v2, :cond_1

    .line 386
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_80_AND_ABOVE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v7

    .line 387
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_80_AND_ABOVE:I

    aget-object v2, v2, v3

    aget v2, v2, v6

    aput v2, v1, v4

    goto/16 :goto_0
.end method

.method public getRestingPulsePercentile_5thAnd95th(II)[I
    .locals 9
    .param p1, "gender"    # I
    .param p2, "userAge"    # I

    .prologue
    const/4 v8, 0x4

    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 297
    new-array v1, v5, [I

    .line 298
    .local v1, "percent":[I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getUserAge()I

    move-result v0

    .line 300
    .local v0, "CurrentUserAge":I
    if-gtz p2, :cond_0

    .line 301
    move p2, v0

    .line 303
    :cond_0
    if-ge p2, v4, :cond_2

    .line 304
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_UNDER_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 305
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_UNDER_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    .line 341
    :cond_1
    :goto_0
    return-object v1

    .line 306
    :cond_2
    if-ne p2, v4, :cond_3

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 308
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_ONE:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto :goto_0

    .line 309
    :cond_3
    if-lt p2, v5, :cond_4

    const/4 v2, 0x3

    if-gt p2, v2, :cond_4

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_2_TO_3:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_2_TO_3:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto :goto_0

    .line 312
    :cond_4
    if-lt p2, v8, :cond_5

    const/4 v2, 0x5

    if-gt p2, v2, :cond_5

    .line 313
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_4_TO_5:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 314
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_4_TO_5:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto :goto_0

    .line 315
    :cond_5
    const/4 v2, 0x6

    if-lt p2, v2, :cond_6

    if-gt p2, v7, :cond_6

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_6_TO_8:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_6_TO_8:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto :goto_0

    .line 318
    :cond_6
    const/16 v2, 0x9

    if-lt p2, v2, :cond_7

    const/16 v2, 0xb

    if-gt p2, v2, :cond_7

    .line 319
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_9_TO_11:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_9_TO_11:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto/16 :goto_0

    .line 321
    :cond_7
    const/16 v2, 0xc

    if-lt p2, v2, :cond_8

    const/16 v2, 0xf

    if-gt p2, v2, :cond_8

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_12_TO_15:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_12_TO_15:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto/16 :goto_0

    .line 324
    :cond_8
    const/16 v2, 0x10

    if-lt p2, v2, :cond_9

    const/16 v2, 0x13

    if-gt p2, v2, :cond_9

    .line 325
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_16_TO_19:I

    aget-object v2, v2, v3

    aget v2, v2, v8

    aput v2, v1, v6

    .line 326
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_16_TO_19:I

    aget-object v2, v2, v3

    const/4 v3, 0x6

    aget v2, v2, v3

    aput v2, v1, v4

    goto/16 :goto_0

    .line 327
    :cond_9
    const/16 v2, 0x14

    if-lt p2, v2, :cond_a

    const/16 v2, 0x27

    if-gt p2, v2, :cond_a

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_20_TO_39:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 329
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_20_TO_39:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto/16 :goto_0

    .line 330
    :cond_a
    const/16 v2, 0x28

    if-lt p2, v2, :cond_b

    const/16 v2, 0x3b

    if-gt p2, v2, :cond_b

    .line 331
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_40_TO_59:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 332
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_40_TO_59:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto/16 :goto_0

    .line 333
    :cond_b
    const/16 v2, 0x3c

    if-lt p2, v2, :cond_c

    const/16 v2, 0x4f

    if-gt p2, v2, :cond_c

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_60_TO_79:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 335
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_60_TO_79:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto/16 :goto_0

    .line 336
    :cond_c
    const/16 v2, 0x50

    if-lt p2, v2, :cond_1

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_80_AND_ABOVE:I

    aget-object v2, v2, v3

    aget v2, v2, v5

    aput v2, v1, v6

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->pulse_data:[[[I

    aget-object v2, v2, p1

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->HRM_AGE_80_AND_ABOVE:I

    aget-object v2, v2, v3

    aget v2, v2, v7

    aput v2, v1, v4

    goto/16 :goto_0
.end method

.method public getSampleTime()J
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->sampleTime:J

    return-wide v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 191
    iget-wide v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->startTime:J

    return-wide v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public getTagIndex()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->tagIndex:Ljava/lang/String;

    return-object v0
.end method

.method public getUserAge()I
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    .line 424
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 426
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 427
    .local v1, "birth":Ljava/util/Calendar;
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    .line 429
    .local v4, "today":Ljava/util/Calendar;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 431
    .local v0, "bDate":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 432
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 434
    const/4 v2, 0x0

    .line 435
    .local v2, "factor":I
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 436
    const/4 v2, -0x1

    .line 439
    :cond_0
    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v5, v2

    return v5
.end method

.method public getUserAge(J)I
    .locals 9
    .param p1, "sampleTime"    # J

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    .line 443
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 445
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 446
    .local v1, "birth":Ljava/util/Calendar;
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    .line 448
    .local v4, "today":Ljava/util/Calendar;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 450
    .local v0, "bDate":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 451
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 453
    const/4 v2, 0x0

    .line 454
    .local v2, "factor":I
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 455
    const/4 v2, -0x1

    .line 458
    :cond_0
    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v5, v2

    return v5
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->userDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getUserGender()I
    .locals 3

    .prologue
    .line 419
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 420
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v0

    .line 421
    .local v0, "gender":I
    return v0
.end method

.method public isHRMNonMedicalCountryDA()Z
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x0

    .line 466
    .local v0, "isNonMediDA":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->isSamsungAccount()Z

    move-result v0

    .line 467
    return v0
.end method

.method public setAccessoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->deviceName:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public setAverage(F)V
    .locals 0
    .param p1, "average"    # F

    .prologue
    .line 229
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->average:F

    .line 230
    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->comment:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 164
    iput-wide p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->id:J

    .line 165
    return-void
.end method

.method public setMaxData(F)V
    .locals 0
    .param p1, "maxData"    # F

    .prologue
    .line 55
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->maxData:F

    .line 56
    return-void
.end method

.method public setMeanHeartRate(F)V
    .locals 0
    .param p1, "meanHeartRate"    # F

    .prologue
    .line 187
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->meanHeartRate:F

    .line 188
    return-void
.end method

.method public setMinData(F)V
    .locals 0
    .param p1, "minData"    # F

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->minData:F

    .line 64
    return-void
.end method

.method public setSampleTime(J)V
    .locals 0
    .param p1, "sampleTime"    # J

    .prologue
    .line 208
    iput-wide p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->sampleTime:J

    .line 209
    return-void
.end method

.method public setStartTime(J)V
    .locals 0
    .param p1, "startTime"    # J

    .prologue
    .line 195
    iput-wide p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->startTime:J

    .line 196
    return-void
.end method

.method public setTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->tag:Ljava/lang/String;

    .line 246
    return-void
.end method

.method public setTagIndex(Ljava/lang/String;)V
    .locals 0
    .param p1, "tagIndex"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->tagIndex:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->userDeviceId:Ljava/lang/String;

    .line 180
    return-void
.end method
