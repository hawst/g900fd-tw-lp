.class Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$2;
.super Ljava/lang/Object;
.source "WeightLogDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->showDeletePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$400(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    invoke-static {v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->access$000(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getId()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->deleteDataById(J)Z

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->setResult(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->finish()V

    .line 252
    return-void
.end method
