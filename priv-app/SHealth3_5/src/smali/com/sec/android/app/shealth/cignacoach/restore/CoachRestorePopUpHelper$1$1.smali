.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;
.super Ljava/lang/Object;
.source "CoachRestorePopUpHelper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const v2, 0x7f0802b5

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->val$isLocalDataPresent:Z

    if-eqz v1, :cond_1

    .line 144
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f091110

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 148
    :goto_0
    const v1, 0x7f0802bb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f09078c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;

    iget-object v2, v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    const v1, 0x7f0802ba

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mDoNotShowAgain:Landroid/widget/CheckBox;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$102(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 151
    const v1, 0x7f0802b9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->val$isLocalDataPresent:Z

    if-eqz v1, :cond_0

    .line 163
    const v1, 0x7f0802b6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 164
    .local v0, "brChkBoxLL":Landroid/widget/LinearLayout;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 166
    const v1, 0x7f0802b8

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090d44

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;

    iget-object v2, v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    const v1, 0x7f0802b7

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mBackupRestore:Landroid/widget/CheckBox;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$202(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 170
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    .end local v0    # "brChkBoxLL":Landroid/widget/LinearLayout;
    :cond_0
    return-void

    .line 146
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f09110f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
