.class public Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LaunchWidgetReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final WIDGET_CLICK_TIME_DELAY:J = 0xbb8L

.field public static prevWidgetClickTime:J


# instance fields
.field private shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 63
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "onReceive called"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_0

    if-nez p1, :cond_1

    .line 65
    :cond_0
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "onReceive called but Intent/Action/Context is null"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/MemoryStatusUtils;->isNotEnoughMemory()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 69
    const v12, 0x7f09078b

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 77
    :cond_2
    new-instance v12, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .line 78
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual {v12}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isBackUpRestoreInProgress()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 79
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    const-string v13, "disable launch via widget: Backup or Restore is ongoing"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const v12, 0x7f0907f5

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 85
    :cond_3
    sget-boolean v12, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsProgressCompleted:Z

    if-eqz v12, :cond_4

    .line 86
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v13, "mIsProgressCompleted is true, go to RestoreActivity"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    new-instance v8, Landroid/content/Intent;

    const-class v12, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    .local v8, "launcherintent":Landroid/content/Intent;
    const v12, 0x10018000

    invoke-virtual {v8, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 89
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 93
    .end local v8    # "launcherintent":Landroid/content/Intent;
    :cond_4
    const-string/jumbo v12, "widgetActivityAction"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 95
    .local v6, "isCoachWidgetIntent":Ljava/lang/String;
    if-eqz v6, :cond_5

    const-string v12, "com.sec.shealth.action.COACH"

    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 97
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    const-string v13, "isCoachWidgetIntent yes => dismissCignaWidgetNotification"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string/jumbo v12, "widgetType"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 99
    .local v11, "widgetType":Ljava/lang/String;
    if-eqz v11, :cond_5

    .line 100
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "isCoachWidgetIntent dismissCignaWidgetNotification, widgetType: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v10, Landroid/content/Intent;

    const-class v12, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 102
    .local v10, "service":Landroid/content/Intent;
    const-string v12, "ACTION_NAME"

    const-string v13, "com.cigna.mobile.coach.COACH_WIDGET_INTENT_DISMISS"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    const-string/jumbo v12, "widgetType"

    invoke-virtual {v10, v12, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 110
    .end local v10    # "service":Landroid/content/Intent;
    .end local v11    # "widgetType":Ljava/lang/String;
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 111
    .local v2, "currentTime":J
    sget-wide v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->prevWidgetClickTime:J

    const-wide/16 v14, 0xbb8

    add-long/2addr v12, v14

    cmp-long v12, v12, v2

    if-gez v12, :cond_6

    const/4 v7, 0x1

    .line 112
    .local v7, "isWidgetLaunchAllowed":Z
    :goto_1
    if-eqz v7, :cond_b

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v12

    const-string v13, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 114
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    const-string v13, "LaunchWidgetReceiver INSIDE"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    sput-wide v2, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->prevWidgetClickTime:J

    .line 118
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v12

    if-nez v12, :cond_7

    .line 120
    new-instance v4, Landroid/content/Intent;

    const-string v12, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v4, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 121
    .local v4, "i":Landroid/content/Intent;
    const-string/jumbo v12, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const/high16 v12, 0x34000000

    invoke-virtual {v4, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 123
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 111
    .end local v4    # "i":Landroid/content/Intent;
    .end local v7    # "isWidgetLaunchAllowed":Z
    :cond_6
    const/4 v7, 0x0

    goto :goto_1

    .line 126
    .restart local v7    # "isWidgetLaunchAllowed":Z
    :cond_7
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v5

    .line 127
    .local v5, "initializationNeeded":Z
    const/4 v9, 0x0

    .line 128
    .local v9, "migrationState":I
    if-nez v5, :cond_8

    .line 129
    invoke-static/range {p1 .. p1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v9

    .line 130
    if-eqz v9, :cond_8

    .line 131
    const/4 v5, 0x1

    .line 135
    :cond_8
    if-eqz v5, :cond_a

    .line 137
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 138
    .restart local v8    # "launcherintent":Landroid/content/Intent;
    const-string v12, "com.sec.android.app.shealth"

    invoke-virtual {v8, v12}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v12, "launchWidget"

    const/4 v13, 0x1

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 140
    if-eqz v6, :cond_9

    const-string v12, "com.sec.shealth.action.COACH"

    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 141
    const-string v12, "COACH_WIDGET"

    const/4 v13, 0x1

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 142
    :cond_9
    const-class v12, Lcom/sec/android/app/shealth/SplashScreenActivity;

    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v12}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 143
    const v12, 0x10018000

    invoke-virtual {v8, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 144
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 148
    .end local v8    # "launcherintent":Landroid/content/Intent;
    :cond_a
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    .line 149
    new-instance v8, Landroid/content/Intent;

    const-string v12, "com.sec.shealth.HomeActivity"

    invoke-direct {v8, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 150
    .restart local v8    # "launcherintent":Landroid/content/Intent;
    const-string v12, "com.sec.android.app.shealth"

    invoke-virtual {v8, v12}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    const-string v12, "com.sec.shealth.HomeActivity"

    invoke-virtual {v8, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string v12, "launchWidget"

    const/4 v13, 0x1

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 153
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 154
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-virtual {v8, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 155
    const/high16 v12, 0x10010000

    invoke-virtual {v8, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 156
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "BroadCastReceived  "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v8}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 161
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v5    # "initializationNeeded":Z
    .end local v8    # "launcherintent":Landroid/content/Intent;
    .end local v9    # "migrationState":I
    :cond_b
    sget-object v12, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "isWidgetLaunchAllowed("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "), intent.getAction("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
