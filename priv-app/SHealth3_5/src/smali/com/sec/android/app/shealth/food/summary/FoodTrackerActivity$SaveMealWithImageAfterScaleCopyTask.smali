.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;
.super Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;
.source "FoodTrackerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveMealWithImageAfterScaleCopyTask"
.end annotation


# instance fields
.field private final mMealTypeId:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p2, "inputImagePath"    # Ljava/lang/String;
    .param p3, "outputImagePath"    # Ljava/lang/String;
    .param p4, "mealTypeId"    # I

    .prologue
    .line 622
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    .line 623
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/food/utils/ImageScaleCopyAsyncTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iput p4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->mMealTypeId:I

    .line 625
    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 12
    .param p1, "isSuccessful"    # Ljava/lang/Boolean;

    .prologue
    const/16 v5, 0xc

    const/16 v4, 0xb

    .line 629
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 630
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->mMealTypeId:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getMealIdByMealTypeId(J)J

    move-result-wide v9

    .line 632
    .local v9, "mealId":J
    const-wide/16 v1, 0x0

    cmp-long v1, v9, v1

    if-gez v1, :cond_0

    .line 633
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 634
    .local v8, "calendarForDefineTimeForMeal":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 635
    .local v7, "calendarForCurrentHourAndMinute":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v7, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 637
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {v8, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 638
    invoke-virtual {v7, v5}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v8, v5, v1}, Ljava/util/Calendar;->set(II)V

    .line 639
    invoke-virtual {v7, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v8, v4, v1}, Ljava/util/Calendar;->set(II)V

    .line 640
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    const-string v1, ""

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->mMealTypeId:I

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-string v6, ""

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>(Ljava/lang/String;FIJLjava/lang/String;)V

    .line 642
    .local v0, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    new-instance v1, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 643
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v9

    .line 645
    .end local v0    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v7    # "calendarForCurrentHourAndMinute":Ljava/util/Calendar;
    .end local v8    # "calendarForDefineTimeForMeal":Ljava/util/Calendar;
    :cond_0
    new-instance v11, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->getOutputImagePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v11, v9, v10, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;-><init>(JLjava/lang/String;)V

    .line 646
    .local v11, "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    new-instance v1, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-static {v11, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/DataBaseUpdater;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;Lcom/sec/android/app/shealth/common/commondao/CommonDao;)V

    .line 647
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-static {v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->refreshLastTakenPhoto(Landroid/content/Context;)V

    .line 649
    .end local v9    # "mealId":J
    .end local v11    # "mealImageData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 619
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SaveMealWithImageAfterScaleCopyTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
