.class public interface abstract Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$SyncEventListener;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SyncEventListener"
.end annotation


# virtual methods
.method public abstract onSyncDeviceJoined()V
.end method

.method public abstract onSyncDeviceLeft()V
.end method

.method public abstract onSyncStopped(ILcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
.end method
