.class Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$3;
.super Ljava/lang/Object;
.source "GalleryActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->initGallery(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$3;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 117
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 106
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$3;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    # getter for: Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mGallery:Lcom/sec/android/app/shealth/common/gallery/GalleryView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->access$100(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;)Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->selectItem(I)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity$3;->this$0:Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    # setter for: Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->mLastPosition:I
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;->access$202(Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;I)I

    .line 112
    return-void
.end method
