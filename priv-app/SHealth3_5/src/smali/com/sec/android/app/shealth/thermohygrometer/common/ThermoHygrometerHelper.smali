.class public Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;
.super Ljava/lang/Object;
.source "ThermoHygrometerHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static getCreateTime()J
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->getCreateTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getHumidity()F
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->getHumidity()F

    move-result v0

    return v0
.end method

.method public static getLastData()Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;-><init>()V

    .line 43
    .local v0, "data":Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;->getTemperature()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->setTemperature(F)V

    .line 44
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;->getHumidity()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->setHumidity(F)V

    .line 45
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;->getCreateTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->setCreateTime(J)V

    .line 46
    return-object v0
.end method

.method public static getTemperature()F
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->getTemperature()F

    move-result v0

    return v0
.end method

.method public static setCreateTime(J)V
    .locals 0
    .param p0, "createTime"    # J

    .prologue
    .line 34
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->setCreateTime(J)V

    .line 35
    return-void
.end method

.method public static setData(Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;)V
    .locals 2
    .param p0, "data"    # Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->getTemperature()F

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;->setTemperature(F)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->getHumidity()F

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;->setHumidity(F)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/data/ThermoHygrometerData;->getCreateTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/common/ThermoHygrometerHelper;->setCreateTime(J)V

    .line 53
    return-void
.end method

.method public static setHumidity(F)V
    .locals 0
    .param p0, "humidity"    # F

    .prologue
    .line 26
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->setHumidity(F)V

    .line 27
    return-void
.end method

.method public static setTemperature(F)V
    .locals 0
    .param p0, "temperature"    # F

    .prologue
    .line 18
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$THERMOHYGROMETER_STATISTICS;->setTemperature(F)V

    .line 19
    return-void
.end method
