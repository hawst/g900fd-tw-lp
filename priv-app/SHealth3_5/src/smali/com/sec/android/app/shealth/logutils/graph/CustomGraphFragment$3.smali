.class Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;
.super Ljava/lang/Object;
.source "CustomGraphFragment.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getHandlerListener()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)V
    .locals 0

    .prologue
    .line 535
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private swellHandler()V
    .locals 4

    .prologue
    .line 567
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->access$100(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 568
    .local v0, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    sget v3, Lcom/sec/android/app/shealth/logutils/R$drawable;->s_health_graph_handler:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->swellHandler(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Landroid/graphics/Bitmap;)V

    .line 570
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 571
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->access$100(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 572
    return-void
.end method


# virtual methods
.method public OnGraphData(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 551
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->access$100(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerItemTextVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->swellHandler()V

    .line 554
    :cond_0
    if-eqz p1, :cond_1

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mHandlerUpdateDataManager:Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    invoke-static {v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->access$200(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V

    .line 561
    :cond_1
    return-void
.end method

.method public OnReleaseTimeOut()V
    .locals 2

    .prologue
    .line 544
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->access$100(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v0

    .line 545
    .local v0, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-static {v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->shrinkHandler(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 546
    iget-object v1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    # getter for: Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->access$100(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 547
    return-void
.end method

.method public OnVisible()V
    .locals 0

    .prologue
    .line 539
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$3;->swellHandler()V

    .line 540
    return-void
.end method
