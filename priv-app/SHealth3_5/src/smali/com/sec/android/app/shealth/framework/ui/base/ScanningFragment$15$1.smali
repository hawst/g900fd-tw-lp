.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

.field final synthetic val$deviceID:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->val$deviceID:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 52

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectToastReq:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->val$item:Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;->deviceName:Ljava/lang/String;

    sput-object v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHrmUIName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->checkIsHrmSwitchReq:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->connectedHRM:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5200(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->leaveDeviceWithOutRefresh(Ljava/lang/String;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5400(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Ljava/lang/String;Z)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v2, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->deviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->val$deviceID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->joinWithDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$4000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)V

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1$1;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v3, v3, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5502(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setCancelable(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->IsJoining:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2302(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->progressDlg:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$5500(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->val$device:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_7

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, -0x2ecd

    aput v50, v2, v49

    const/16 v49, -0x5d

    aput v49, v2, v48

    const/16 v48, -0x8bc

    aput v48, v2, v47

    const/16 v47, -0x6e

    aput v47, v2, v46

    const/16 v46, -0x31

    aput v46, v2, v45

    const/16 v45, -0x55

    aput v45, v2, v44

    const/16 v44, -0x1b9b

    aput v44, v2, v43

    const/16 v43, -0x6a

    aput v43, v2, v42

    const/16 v42, 0x4b1e

    aput v42, v2, v41

    const/16 v41, 0x33

    aput v41, v2, v40

    const/16 v40, -0x389b

    aput v40, v2, v39

    const/16 v39, -0x17

    aput v39, v2, v38

    const/16 v38, -0x27

    aput v38, v2, v37

    const/16 v37, -0x40ac

    aput v37, v2, v36

    const/16 v36, -0x2a

    aput v36, v2, v35

    const/16 v35, -0x2e

    aput v35, v2, v34

    const/16 v34, 0x146e

    aput v34, v2, v33

    const/16 v33, 0x3878

    aput v33, v2, v32

    const/16 v32, 0x6d48

    aput v32, v2, v31

    const/16 v31, 0x7343

    aput v31, v2, v30

    const/16 v30, -0xce5

    aput v30, v2, v29

    const/16 v29, -0x79

    aput v29, v2, v28

    const/16 v28, -0x11

    aput v28, v2, v27

    const/16 v27, -0x74e4

    aput v27, v2, v26

    const/16 v26, -0x12

    aput v26, v2, v25

    const/16 v25, -0x5fef

    aput v25, v2, v24

    const/16 v24, -0x2d

    aput v24, v2, v23

    const/16 v23, 0x5724

    aput v23, v2, v22

    const/16 v22, -0x41d9

    aput v22, v2, v21

    const/16 v21, -0x32

    aput v21, v2, v20

    const/16 v20, -0x56a4

    aput v20, v2, v19

    const/16 v19, -0x79

    aput v19, v2, v18

    const/16 v18, -0x7a

    aput v18, v2, v17

    const/16 v17, -0xa

    aput v17, v2, v16

    const/16 v16, -0x1d

    aput v16, v2, v15

    const/16 v15, -0x5c9a

    aput v15, v2, v14

    const/16 v14, -0x39

    aput v14, v2, v13

    const/4 v13, -0x4

    aput v13, v2, v12

    const/16 v12, -0x59

    aput v12, v2, v11

    const/16 v11, -0x72

    aput v11, v2, v10

    const/16 v10, -0x17

    aput v10, v2, v9

    const/16 v9, -0x34

    aput v9, v2, v8

    const/16 v8, 0x534

    aput v8, v2, v7

    const/16 v7, 0x5b2b

    aput v7, v2, v6

    const/16 v6, -0x14ca

    aput v6, v2, v5

    const/16 v5, -0x7c

    aput v5, v2, v3

    const/16 v3, -0x6f6

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, -0x2ea4

    aput v51, v1, v50

    const/16 v50, -0x2f

    aput v50, v1, v49

    const/16 v49, -0x8cc

    aput v49, v1, v48

    const/16 v48, -0x9

    aput v48, v1, v47

    const/16 v47, -0x44

    aput v47, v1, v46

    const/16 v46, -0x3e

    aput v46, v1, v45

    const/16 v45, -0x1bfa

    aput v45, v1, v44

    const/16 v44, -0x1c

    aput v44, v1, v43

    const/16 v43, 0x4b7b

    aput v43, v1, v42

    const/16 v42, 0x4b

    aput v42, v1, v41

    const/16 v41, -0x3900

    aput v41, v1, v40

    const/16 v40, -0x39

    aput v40, v1, v39

    const/16 v39, -0x56

    aput v39, v1, v38

    const/16 v38, -0x40c6

    aput v38, v1, v37

    const/16 v37, -0x41

    aput v37, v1, v36

    const/16 v36, -0x4b

    aput v36, v1, v35

    const/16 v35, 0x141b

    aput v35, v1, v34

    const/16 v34, 0x3814

    aput v34, v1, v33

    const/16 v33, 0x6d38

    aput v33, v1, v32

    const/16 v32, 0x736d

    aput v32, v1, v31

    const/16 v31, -0xc8d

    aput v31, v1, v30

    const/16 v30, -0xd

    aput v30, v1, v29

    const/16 v29, -0x7d

    aput v29, v1, v28

    const/16 v28, -0x7483

    aput v28, v1, v27

    const/16 v27, -0x75

    aput v27, v1, v26

    const/16 v26, -0x5f87

    aput v26, v1, v25

    const/16 v25, -0x60

    aput v25, v1, v24

    const/16 v24, 0x570a

    aput v24, v1, v23

    const/16 v23, -0x41a9

    aput v23, v1, v22

    const/16 v22, -0x42

    aput v22, v1, v21

    const/16 v21, -0x56c3

    aput v21, v1, v20

    const/16 v20, -0x57

    aput v20, v1, v19

    const/16 v19, -0x1e

    aput v19, v1, v18

    const/16 v18, -0x61

    aput v18, v1, v17

    const/16 v17, -0x74

    aput v17, v1, v16

    const/16 v16, -0x5cec

    aput v16, v1, v15

    const/16 v15, -0x5d

    aput v15, v1, v14

    const/16 v14, -0x6e

    aput v14, v1, v13

    const/16 v13, -0x3a

    aput v13, v1, v12

    const/16 v12, -0x60

    aput v12, v1, v11

    const/16 v11, -0x76

    aput v11, v1, v10

    const/16 v10, -0x57

    aput v10, v1, v9

    const/16 v9, 0x547

    aput v9, v1, v8

    const/16 v8, 0x5b05

    aput v8, v1, v7

    const/16 v7, -0x14a5

    aput v7, v1, v6

    const/16 v6, -0x15

    aput v6, v1, v5

    const/16 v5, -0x697

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x56

    aput v9, v2, v8

    const/4 v8, -0x7

    aput v8, v2, v7

    const/16 v7, 0x1421

    aput v7, v2, v6

    const/16 v6, 0x4046

    aput v6, v2, v3

    const/16 v3, 0x6708

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/16 v10, -0x65

    aput v10, v1, v9

    const/16 v9, -0x37

    aput v9, v1, v8

    const/16 v8, 0x1472

    aput v8, v1, v7

    const/16 v7, 0x4014

    aput v7, v1, v6

    const/16 v6, 0x6740

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v1}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_5
    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isReScanNeeded:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->val$deviceID:Ljava/lang/String;

    iput-object v2, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->JoinDeviceID:Ljava/lang/Object;

    goto/16 :goto_0

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_5
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_6
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$15;->val$device:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v1, 0x2f

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, 0x331c

    aput v50, v2, v49

    const/16 v49, 0x5041

    aput v49, v2, v48

    const/16 v48, -0xbe0

    aput v48, v2, v47

    const/16 v47, -0x6f

    aput v47, v2, v46

    const/16 v46, -0x5e

    aput v46, v2, v45

    const/16 v45, -0x23

    aput v45, v2, v44

    const/16 v44, -0x5a

    aput v44, v2, v43

    const/16 v43, -0x4

    aput v43, v2, v42

    const/16 v42, 0x5a47

    aput v42, v2, v41

    const/16 v41, -0x1de

    aput v41, v2, v40

    const/16 v40, -0x65

    aput v40, v2, v39

    const/16 v39, -0x3bb2

    aput v39, v2, v38

    const/16 v38, -0x49

    aput v38, v2, v37

    const/16 v37, -0x1eff

    aput v37, v2, v36

    const/16 v36, -0x78

    aput v36, v2, v35

    const/16 v35, 0x713

    aput v35, v2, v34

    const/16 v34, 0x5e72

    aput v34, v2, v33

    const/16 v33, 0x832

    aput v33, v2, v32

    const/16 v32, 0x3478

    aput v32, v2, v31

    const/16 v31, -0x4ae6

    aput v31, v2, v30

    const/16 v30, -0x23

    aput v30, v2, v29

    const/16 v29, 0x1462

    aput v29, v2, v28

    const/16 v28, -0x1788

    aput v28, v2, v27

    const/16 v27, -0x77

    aput v27, v2, v26

    const/16 v26, -0x61ff

    aput v26, v2, v25

    const/16 v25, -0xa

    aput v25, v2, v24

    const/16 v24, -0x53

    aput v24, v2, v23

    const/16 v23, -0x5

    aput v23, v2, v22

    const/16 v22, -0x1

    aput v22, v2, v21

    const/16 v21, 0x3660

    aput v21, v2, v20

    const/16 v20, -0x7a9

    aput v20, v2, v19

    const/16 v19, -0x2a

    aput v19, v2, v18

    const/16 v18, -0x23

    aput v18, v2, v17

    const/16 v17, 0x824

    aput v17, v2, v16

    const/16 v16, -0x5099

    aput v16, v2, v15

    const/16 v15, -0x23

    aput v15, v2, v14

    const/16 v14, -0x40

    aput v14, v2, v13

    const/4 v13, -0x4

    aput v13, v2, v12

    const/16 v12, 0x1806

    aput v12, v2, v11

    const/16 v11, -0x43ca

    aput v11, v2, v10

    const/16 v10, -0x21

    aput v10, v2, v9

    const/16 v9, 0x6611

    aput v9, v2, v8

    const/16 v8, -0x7feb

    aput v8, v2, v7

    const/16 v7, -0x52

    aput v7, v2, v6

    const/16 v6, -0x15

    aput v6, v2, v5

    const/16 v5, -0x5a8

    aput v5, v2, v3

    const/16 v3, -0x67

    aput v3, v2, v1

    const/16 v1, 0x2f

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, 0x3373

    aput v51, v1, v50

    const/16 v50, 0x5033

    aput v50, v1, v49

    const/16 v49, -0xbb0

    aput v49, v1, v48

    const/16 v48, -0xc

    aput v48, v1, v47

    const/16 v47, -0x2f

    aput v47, v1, v46

    const/16 v46, -0x4c

    aput v46, v1, v45

    const/16 v45, -0x3b

    aput v45, v1, v44

    const/16 v44, -0x72

    aput v44, v1, v43

    const/16 v43, 0x5a22

    aput v43, v1, v42

    const/16 v42, -0x1a6

    aput v42, v1, v41

    const/16 v41, -0x2

    aput v41, v1, v40

    const/16 v40, -0x3ba0

    aput v40, v1, v39

    const/16 v39, -0x3c

    aput v39, v1, v38

    const/16 v38, -0x1e91

    aput v38, v1, v37

    const/16 v37, -0x1f

    aput v37, v1, v36

    const/16 v36, 0x774

    aput v36, v1, v35

    const/16 v35, 0x5e07

    aput v35, v1, v34

    const/16 v34, 0x85e

    aput v34, v1, v33

    const/16 v33, 0x3408

    aput v33, v1, v32

    const/16 v32, -0x4acc

    aput v32, v1, v31

    const/16 v31, -0x4b

    aput v31, v1, v30

    const/16 v30, 0x1416

    aput v30, v1, v29

    const/16 v29, -0x17ec

    aput v29, v1, v28

    const/16 v28, -0x18

    aput v28, v1, v27

    const/16 v27, -0x619c

    aput v27, v1, v26

    const/16 v26, -0x62

    aput v26, v1, v25

    const/16 v25, -0x22

    aput v25, v1, v24

    const/16 v24, -0x2b

    aput v24, v1, v23

    const/16 v23, -0x71

    aput v23, v1, v22

    const/16 v22, 0x3610

    aput v22, v1, v21

    const/16 v21, -0x7ca

    aput v21, v1, v20

    const/16 v20, -0x8

    aput v20, v1, v19

    const/16 v19, -0x47

    aput v19, v1, v18

    const/16 v18, 0x84d

    aput v18, v1, v17

    const/16 v17, -0x50f8

    aput v17, v1, v16

    const/16 v16, -0x51

    aput v16, v1, v15

    const/16 v15, -0x5c

    aput v15, v1, v14

    const/16 v14, -0x6e

    aput v14, v1, v13

    const/16 v13, 0x1867

    aput v13, v1, v12

    const/16 v12, -0x43e8

    aput v12, v1, v11

    const/16 v11, -0x44

    aput v11, v1, v10

    const/16 v10, 0x6674

    aput v10, v1, v9

    const/16 v9, -0x7f9a

    aput v9, v1, v8

    const/16 v8, -0x80

    aput v8, v1, v7

    const/16 v7, -0x7a

    aput v7, v1, v6

    const/16 v6, -0x5c9

    aput v6, v1, v5

    const/4 v5, -0x6

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x5

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/16 v9, -0x78c9

    aput v9, v2, v8

    const/16 v8, -0x49

    aput v8, v2, v7

    const/16 v7, -0x49c4

    aput v7, v2, v6

    const/16 v6, -0x1c

    aput v6, v2, v3

    const/16 v3, 0x4438

    aput v3, v2, v1

    const/4 v1, 0x5

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/16 v10, -0x78fa

    aput v10, v1, v9

    const/16 v9, -0x79

    aput v9, v1, v8

    const/16 v8, -0x4983

    aput v8, v1, v7

    const/16 v7, -0x4a

    aput v7, v1, v6

    const/16 v6, 0x4470

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v6, v1

    if-lt v3, v6, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v6, v1

    if-lt v3, v6, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v1}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_a
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_b
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9
.end method
