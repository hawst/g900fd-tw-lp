.class final Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData$1;
.super Ljava/lang/Object;
.source "RealtimeData.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;
    .locals 12
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 135
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v7

    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v8

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v9

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v11

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;-><init>(JJIFFFJI)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 141
    new-array v0, p1, [Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData$1;->newArray(I)[Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    move-result-object v0

    return-object v0
.end method
