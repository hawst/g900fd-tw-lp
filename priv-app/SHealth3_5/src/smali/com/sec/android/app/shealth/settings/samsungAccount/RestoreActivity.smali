.class public Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "RestoreActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$12;,
        Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;
    }
.end annotation


# static fields
.field private static final LOW_STORAGE_FAILED_POPUP:Ljava/lang/String; = "LOW_STORAGE_FAILED_POPUP"

.field private static final RESTORE_FAILED_NETWORK:Ljava/lang/String; = "restore_failed_network"

.field private static final RESTORE_FAILED_NODATA:Ljava/lang/String; = "restore_failed_nodata"

.field private static final RESTORE_PROGRESS_POPUP:Ljava/lang/String; = "progress_popup"

.field private static final SERVER_MAINTENANCE_FAILED_POPUP:Ljava/lang/String; = "SERVER_MAINTENANCE_FAILED_POPUP"

.field public static mIsProgressCompleted:Z


# instance fields
.field private RESTART_POPUP:Ljava/lang/String;

.field private SERVER_ERROR_MESSAGE:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private isDeviceStorageLow:Z

.field private isOngoing:Z

.field private isServerUnderMaintenance:Z

.field private mCBBackup:Landroid/widget/CheckBox;

.field private mCignaBackupRestoreEnable:Z

.field private mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mIsNetworkErrorOccured:Z

.field private mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mRestoreProgress:Landroid/widget/TextView;

.field private mRestoreProgressBar:Landroid/widget/ProgressBar;

.field private mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mTextPercentage:Landroid/widget/TextView;

.field private netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field private shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsProgressCompleted:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 51
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    .line 54
    const-string v0, "completedRestore"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->RESTART_POPUP:Ljava/lang/String;

    .line 74
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z

    .line 77
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isDeviceStorageLow:Z

    .line 78
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isServerUnderMaintenance:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCignaBackupRestoreEnable:Z

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCBBackup:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCignaBackupRestoreEnable:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->cignaBackupRestore(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgress:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgress:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->RESTART_POPUP:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->restartApp()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->startBackup()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Landroid/widget/ProgressBar;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isDeviceStorageLow:Z

    return p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isServerUnderMaintenance:Z

    return p1
.end method

.method private cignaBackupRestore(Ljava/lang/String;)Z
    .locals 5
    .param p1, "isBackUpRestore"    # Ljava/lang/String;

    .prologue
    .line 282
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cignaBackupRestore, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;-><init>(Landroid/content/Context;)V

    .line 284
    .local v0, "helper":Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    const/4 v1, 0x0

    .line 285
    .local v1, "result":Z
    const-string v2, "BACKUP"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 286
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->backup(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v1

    .line 416
    :goto_0
    if-nez v1, :cond_0

    .line 417
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    const-string v3, "FAILED <in restoreActivity> "

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_0
    return v1

    .line 345
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$6;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->restore(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v1

    goto :goto_0
.end method

.method private restartApp()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "restart SHealth app()"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KeyManager restart #1 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setSucceedReStoreFlag()V

    .line 257
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isDefaultPassword()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 258
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->storeTemporaryPass()V

    .line 262
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 263
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "key"

    const-string v3, "Is_Restore_Completed"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string/jumbo v2, "value"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v4, "CONFIG_OPTION_PUT"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->finishAffinity()V

    .line 270
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 271
    .local v1, "intent_splash":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 272
    const/high16 v2, 0x20000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 273
    const-string/jumbo v2, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    const-class v2, Lcom/sec/android/app/shealth/SplashScreenActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 276
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->startActivity(Landroid/content/Intent;)V

    .line 278
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 279
    return-void
.end method

.method private showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
    .locals 6
    .param p1, "failStatus"    # Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    .prologue
    const v5, 0x7f0907f3

    const v3, 0x7f090047

    const v2, 0x7f090139

    const/4 v4, 0x0

    .line 773
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->START:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    if-ne p1, v1, :cond_2

    .line 775
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 778
    :cond_1
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v1, p0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 779
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 780
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 781
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 782
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v2, 0x7f030017

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$9;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 798
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 799
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$10;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)V

    .line 812
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "progress_popup"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 814
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    if-ne p1, v1, :cond_3

    .line 816
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 817
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 818
    const v1, 0x7f0907f7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 819
    const v1, 0x7f09004b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 820
    const v1, 0x7f09004c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 821
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 822
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "restore_failed_network"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 824
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_3
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->NODATA:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    if-ne p1, v1, :cond_4

    .line 826
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 827
    .restart local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 828
    const v1, 0x7f0907f6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 829
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 830
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "restore_failed_nodata"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 831
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_4
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    if-ne p1, v1, :cond_5

    .line 832
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 833
    .restart local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 834
    const v1, 0x7f0907f2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 835
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 836
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 837
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "LOW_STORAGE_FAILED_POPUP"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 838
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isDeviceStorageLow:Z

    goto/16 :goto_0

    .line 839
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_5
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    if-ne p1, v1, :cond_6

    .line 840
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 841
    .restart local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 842
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 843
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 844
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 845
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "SERVER_MAINTENANCE_FAILED_POPUP"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 846
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isServerUnderMaintenance:Z

    goto/16 :goto_0

    .line 847
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_6
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->RESTART:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    if-ne p1, v1, :cond_0

    .line 849
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 850
    .restart local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900d8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 851
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090d28

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 852
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 853
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setCancelable(Z)V

    .line 854
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->RESTART_POPUP:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private startBackup()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 425
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    const/4 v2, 0x1

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$7;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->startBackup(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :goto_0
    return-void

    .line 562
    :catch_0
    move-exception v0

    .line 564
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 565
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z

    .line 566
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    .line 567
    const v1, 0x7f0808b1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 568
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z

    goto :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090139

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 161
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 864
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$11;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Ljava/lang/String;)V

    return-object v0
.end method

.method public initLayout()V
    .locals 5

    .prologue
    const v4, 0x7f0808b1

    .line 165
    const v2, 0x7f0808b3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCBBackup:Landroid/widget/CheckBox;

    .line 167
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isBackupBeforeRestoreChecked(Landroid/content/Context;)Z

    move-result v0

    .line 168
    .local v0, "isBackupBeforeRestoringValue":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCBBackup:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 170
    const v2, 0x7f0808b2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    const v2, 0x7f0808b4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 180
    .local v1, "textView":Landroid/widget/TextView;
    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$2;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Landroid/widget/TextView;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCBBackup:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 205
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 230
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z

    .line 232
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 102
    const v2, 0x7f0301f6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->setContentView(I)V

    .line 104
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v2, :cond_0

    .line 105
    const v2, 0x7f090d26

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    .local v1, "tmpString":Ljava/lang/String;
    const-string v2, "Wi-Fi"

    const-string v3, "WLAN"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 107
    const v2, 0x7f0808af

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    .end local v1    # "tmpString":Ljava/lang/String;
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .line 111
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->initLayout()V

    .line 115
    if-eqz p1, :cond_1

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "progress_popup"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 118
    .local v0, "restoreProgressDialog":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1

    .line 120
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 121
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 126
    .end local v0    # "restoreProgressDialog":Landroid/support/v4/app/DialogFragment;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isCoachBREnabled(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCignaBackupRestoreEnable:Z

    .line 127
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 238
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 243
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCBBackup:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCBBackup:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setBackupBeforeRestoreValue(Landroid/content/Context;Z)V

    .line 249
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 859
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 860
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 132
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResume()"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-boolean v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsProgressCompleted:Z

    if-eqz v1, :cond_2

    .line 137
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->RESTART:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    .line 146
    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 148
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 149
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 150
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->startActivity(Landroid/content/Intent;)V

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->finish()V

    .line 153
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    return-void

    .line 138
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isDeviceStorageLow:Z

    if-eqz v1, :cond_3

    .line 139
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    goto :goto_0

    .line 140
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isServerUnderMaintenance:Z

    if-eqz v1, :cond_4

    .line 141
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    goto :goto_0

    .line 142
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z

    if-eqz v1, :cond_0

    .line 143
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    goto :goto_0
.end method

.method public startRestore()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 576
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    const/16 v2, 0xb

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->startRestore(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 747
    :goto_0
    return-void

    .line 735
    :catch_0
    move-exception v0

    .line 737
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "3.0 Restore Exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 740
    const v1, 0x7f0808b1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setClickable(Z)V

    .line 741
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z

    .line 742
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z

    .line 743
    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    goto :goto_0
.end method

.method public stopBackupOrRestore(Z)V
    .locals 4
    .param p1, "isBackup"    # Z

    .prologue
    .line 751
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "stopBackupOrRestore called for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p1, :cond_1

    const-string v1, "Backup"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " work"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 754
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 755
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 757
    :cond_0
    const v1, 0x7f0808b1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setClickable(Z)V

    .line 758
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z

    .line 760
    if-eqz p1, :cond_2

    .line 761
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->stopBackup()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 769
    :goto_1
    return-void

    .line 751
    :cond_1
    const-string v1, "Restore"

    goto :goto_0

    .line 763
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->shcm:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->stopRestore()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 765
    :catch_0
    move-exception v0

    .line 766
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
