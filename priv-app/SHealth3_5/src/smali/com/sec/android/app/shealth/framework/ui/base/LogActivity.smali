.class public abstract Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$10;,
        Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ViewByTask;,
        Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;,
        Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteDialogButtonController;,
        Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$UpdateLogTagTask;,
        Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$SpinnerCustomAdapter;
    }
.end annotation


# static fields
.field private static final COLLAPSED_GROUP:Ljava/lang/String; = "collapsed_group"

.field private static final DELETE_POPUP:Ljava/lang/String; = "log_delete_popup"

.field private static final MAX_ITEM_SHARE_LIMIT:I = 0x32

.field public static final POPUP_FILTER:Ljava/lang/String; = "Filter"

.field private static final SINGLE_ITEM:I = 0x1


# instance fields
.field private isCabButtonAdded:Z

.field private isDeleteMode:Z

.field private isItemClicked:Z

.field private isSelectMode:Z

.field private mActionModeListener:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;

.field private mContext:Landroid/content/Context;

.field protected mCursor:Landroid/database/Cursor;

.field private mDeleteTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

.field protected mExpandableListView:Landroid/widget/ExpandableListView;

.field private mFilterRange:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsItemsSelected:Z

.field private mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

.field private mLogActivity:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

.field private mNoDataLayout:Landroid/widget/LinearLayout;

.field private mNoDataMessage:Landroid/widget/TextView;

.field private mSelectDataAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectSpinner:Landroid/widget/Spinner;

.field private mSelectedFilterType:I

.field private mShareItemLimit:I

.field private mUpdateTagTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mViewbyTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isCabButtonAdded:Z

    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mShareItemLimit:I

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mDialogControllerMap:Ljava/util/Map;

    return-void
.end method

.method private ShareLogs()V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getSelectedLogDataForSharing()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mIsShareViaRunning:Z

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaDialog(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->removeDeleteMode()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->deleteDone()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Landroid/os/AsyncTask;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLogActivity:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showLoadingPopup(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->loadAllData(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->performNotifyDataChange()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->hideLoadingPopup()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mIsItemsSelected:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)I
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->removeDeleteMode()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setFilterType(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->ShareLogs()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mFilterRange:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Landroid/os/AsyncTask;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mViewbyTask:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mViewbyTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->updateSelectedCount(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleSelectAllCheckBox()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleShareViewButton()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->enableActionBarButtons(Z)V

    return-void
.end method

.method private addSelectSpinner()V
    .locals 6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->action_bar_spinner:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$6;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_2

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$SpinnerCustomAdapter;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$layout;->spinner_header_view:I

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$id;->text:I

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$SpinnerCustomAdapter;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$layout;->spinner_child_view:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method private deleteDone()V
    .locals 2

    const-string v0, "log_delete_popup"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showDeletePopup(Ljava/lang/String;Z)V

    return-void
.end method

.method private enableActionBarButtons(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    return-void
.end method

.method private hideLoadingPopup()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    :cond_0
    return-void
.end method

.method private loadAllData(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V
    .locals 8

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p1, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupCount()I

    move-result v3

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v1, "dd/MM/yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getChild(II)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getColumnNameForCreateTime()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getColumnNameForID()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setLogSelected(Ljava/lang/String;Z)V

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-nez v5, :cond_2

    :cond_3
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private performNotifyDataChange()V
    .locals 2

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isCheckAll()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setCheckAll(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getTotalChildCount()I

    move-result v1

    if-ne v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setCheckAll(Z)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->updateSelectedCount(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private removeDeleteMode()V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->applyFilter(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setDeleteMode(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setMenuDeleteMode(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setCheckAll(Z)V

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isCabButtonAdded:Z

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->removeSelectSpinner()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCancelDoneVisibility(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->clearCheckedItems()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshAdapter()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->invalidateOptionsMenu()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshFocusables()V

    return-void
.end method

.method private removeSelectSpinner()V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeSpinnerView()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    return-void
.end method

.method private setFilterType(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->applyFilter(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private showDeletePopup(Ljava/lang/String;Z)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mIsItemsSelected:Z

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mIsItemsSelected:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v1

    if-le v1, v3, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->deleted_message_over_two_items:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    :goto_0
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->delete:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->delete:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->deleted_message_one_item:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->selected_item_deleted_message:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->selected_item_deleted_message:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    goto :goto_0
.end method

.method private showLoadingPopup(I)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$7;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$7;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLoadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    :cond_0
    return-void
.end method

.method private toggleActionButtonsVisibilityforSelectMode(Z)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionModeListener:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionModeListener:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;->enterActionMode()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionModeListener:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;->exitActionMode()V

    goto :goto_0
.end method

.method private toggleSelectAllCheckBox()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getTotalChildCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setCheckAll(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->updateSelectedCount(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private toggleShareViewButton()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isCabButtonAdded:Z

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mShareItemLimit:I

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v3, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    goto :goto_0
.end method

.method private updateSelectSpinner(I)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->addSelectSpinner()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    :cond_2
    if-ltz p1, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v1, "ar"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->selected_format:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_0
    if-nez p1, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->select_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    :goto_2
    return-void

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->selected_format:I

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getTotalChildCount()I

    move-result v0

    if-ne p1, v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->unselect_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getTotalChildCount()I

    move-result v0

    if-ge p1, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->select_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->unselect_all:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_2
.end method

.method private updateSelectedCount(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->updateSelectSpinner(I)V

    return-void
.end method


# virtual methods
.method protected abstract applyFilter(Ljava/lang/CharSequence;)V
.end method

.method public closeScreen()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onBackPressed()V

    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->log:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    return-void
.end method

.method protected delete()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mDeleteTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mDeleteTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$DeleteAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mDeleteTask:Landroid/os/AsyncTask;

    :cond_1
    return-void
.end method

.method protected abstract getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
.end method

.method protected abstract getColumnNameForMemo()Ljava/lang/String;
.end method

.method protected abstract getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method protected getDeleteList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected abstract getFilterRange()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getFilterType(I)Ljava/lang/String;
.end method

.method protected abstract getLogDataTypeURI()Landroid/net/Uri;
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1

    const-string v0, "Filter"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$9;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getSelectedLogDataForSharing()Ljava/lang/String;
.end method

.method protected abstract isMemoVisible(Landroid/database/Cursor;)Z
.end method

.method protected loadingCompleted()V
    .locals 0

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isMenuDeleteMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->removeDeleteMode()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshFocusables()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->finish()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const v2, 0x8000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v2, 0x20000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    :cond_0
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->log_fragment:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setContentView(I)V

    iput-object p0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mContext:Landroid/content/Context;

    iput-object p0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mLogActivity:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "log_delete_popup"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "Filter"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v2, v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismiss()V

    :cond_2
    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->expandable_listView:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    if-eqz p1, :cond_6

    const-string v0, "collapsed_group"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v5}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setClickedGroupPosition(IZ)V

    goto :goto_0

    :cond_3
    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupCount()I

    move-result v3

    if-ge v0, v3, :cond_7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupCount()I

    move-result v2

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    move v0, v1

    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupCount()I

    move-result v2

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$4;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->addSelectSpinner()V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->no_data_view:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->no_data_message:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mNoDataMessage:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getTotalChildCount()I

    move-result v0

    if-gtz v0, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showNoDataView()V

    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->registerForContextMenu(Landroid/view/View;)V

    return-void

    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    goto :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$menu;->log_activity_actions:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->changeCursor(Landroid/database/Cursor;)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mDeleteTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mDeleteTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mViewbyTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mViewbyTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mUpdateTagTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_4
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->select:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setUpSelectMode()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->refreshFocusables()V

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->view_by:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getFilterRange()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mFilterRange:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showFilterDialog()V

    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->select:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getTotalChildCount()I

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v3, :cond_0

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isDeleteMode()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isMenuDeleteMode()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->performNotifyDataChange()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->isGroupCollapsed(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "collapsed_group"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isItemClicked:Z

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onStop()V

    return-void
.end method

.method protected refreshAdapter()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->changeCursor(Landroid/database/Cursor;)V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->performNotifyDataChange()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getTotalChildCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->showNoDataView()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected registerContentObserver()V
    .locals 0

    return-void
.end method

.method public setActionModeListener(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mActionModeListener:Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$ActionModeChangeListener;

    return-void
.end method

.method protected setMaxItemShareLimit(I)V
    .locals 1

    const/16 v0, 0x32

    if-ltz p1, :cond_0

    if-gt p1, v0, :cond_0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mShareItemLimit:I

    :goto_0
    return-void

    :cond_0
    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mShareItemLimit:I

    goto :goto_0
.end method

.method public setNoLogImageAndText(II)V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->invalidateOptionsMenu()V

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mNoDataMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    return-void
.end method

.method protected setUpMenuDeleteMode()V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isDeleteMode:Z

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->applyFilter(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setMenuDeleteMode(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->notifyDataSetChanged()V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$5;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->done:I

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(Landroid/view/View$OnClickListener;I)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v3, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->updateSelectedCount(I)V

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->addSelectSpinner()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    goto :goto_1
.end method

.method protected setUpSelectMode()V
    .locals 6

    const/4 v5, 0x1

    const/4 v1, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isSelectMode:Z

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->applyFilter(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->setMenuDeleteMode(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->notifyDataSetChanged()V

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getGroupCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity$8;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getSelectedLogCount()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->updateSelectedCount(I)V

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isCabButtonAdded:Z

    if-nez v2, :cond_1

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->isCabButtonAdded:Z

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->tw_action_bar_icon_share:I

    invoke-direct {v2, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->share_via:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->share_via:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->tw_action_bar_icon_delete:I

    invoke-direct {v2, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->delete:I

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$string;->delete:I

    invoke-virtual {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v3, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->addSelectSpinner()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    goto :goto_1
.end method

.method protected showDetailScreen(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected abstract showDetailScreen(Ljava/lang/String;)V
.end method

.method protected showFilterDialog()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mFilterRange:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Z

    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->view_by:I

    const/16 v3, 0xc

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mFilterRange:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    iget v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mSelectedFilterType:I

    const/4 v3, 0x1

    aput-boolean v3, v0, v2

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Filter"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public showNoDataView()V
    .locals 3

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mNoDataMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;->getTotalChildCount()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mNoDataLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0, v2}, Landroid/widget/ExpandableListView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected unregisterContentObserver()V
    .locals 0

    return-void
.end method

.method protected abstract updateMemo(Ljava/lang/String;Ljava/lang/String;)V
.end method
