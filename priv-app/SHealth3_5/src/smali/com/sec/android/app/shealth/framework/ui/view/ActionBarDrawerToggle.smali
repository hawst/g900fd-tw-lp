.class public Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$DrawerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$1;,
        Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;,
        Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImplHC;,
        Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImplBase;,
        Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;,
        Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;,
        Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$DelegateProvider;
    }
.end annotation


# static fields
.field private static final ID_HOME:I = 0x102002c

.field private static final IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

.field private final mCloseDrawerContentDescRes:I

.field private mDrawerImage:Landroid/graphics/drawable/Drawable;

.field private final mDrawerImageResource:I

.field private mDrawerIndicatorEnabled:Z

.field private final mDrawerLayout:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

.field private final mOpenDrawerContentDescRes:I

.field private mSetIndicatorInfo:Ljava/lang/Object;

.field private mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

.field private mThemeImage:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImplHC;

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImplHC;-><init>(Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$1;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImplBase;

    invoke-direct {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImplBase;-><init>(Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$1;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;III)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerIndicatorEnabled:Z

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerLayout:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iput p3, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerImageResource:I

    iput p4, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mOpenDrawerContentDescRes:I

    iput p5, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mCloseDrawerContentDescRes:I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->getThemeUpIndicator()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mThemeImage:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerImage:Landroid/graphics/drawable/Drawable;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerImage:Landroid/graphics/drawable/Drawable;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    const v1, 0x3eaaaaab

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;->setOffsetBy(F)V

    instance-of v0, p1, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$DelegateProvider;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$DelegateProvider;

    invoke-interface {p1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$DelegateProvider;->getDrawerToggleDelegate()Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    goto :goto_0
.end method


# virtual methods
.method getThemeUpIndicator()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;->getThemeUpIndicator()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivity:Landroid/app/Activity;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;->getThemeUpIndicator(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public onDrawerClosed(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;->setOffset(F)V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerIndicatorEnabled:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mCloseDrawerContentDescRes:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->setActionBarDescription(I)V

    :cond_0
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;->setOffset(F)V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerIndicatorEnabled:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mOpenDrawerContentDescRes:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->setActionBarDescription(I)V

    :cond_0
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 4

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f000000    # 0.5f

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;->getOffset()F

    move-result v0

    cmpl-float v1, p2, v2

    if-lez v1, :cond_0

    const/4 v1, 0x0

    sub-float v2, p2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;->setOffset(F)V

    return-void

    :cond_0
    mul-float v1, p2, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method public onDrawerStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const v2, 0x800003

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerIndicatorEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerLayout:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->isDrawerVisible(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerLayout:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(I)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerLayout:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->openDrawer(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method setActionBarDescription(I)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;->setActionBarDescription(I)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSetIndicatorInfo:Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivity:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, p1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;->setActionBarDescription(Ljava/lang/Object;Landroid/app/Activity;I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSetIndicatorInfo:Ljava/lang/Object;

    goto :goto_0
.end method

.method setActionBarUpIndicator(Landroid/graphics/drawable/Drawable;I)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivityImpl:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$Delegate;->setActionBarUpIndicator(Landroid/graphics/drawable/Drawable;I)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSetIndicatorInfo:Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mActivity:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$ActionBarDrawerToggleImpl;->setActionBarUpIndicator(Ljava/lang/Object;Landroid/app/Activity;Landroid/graphics/drawable/Drawable;I)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSetIndicatorInfo:Ljava/lang/Object;

    goto :goto_0
.end method

.method public syncState()V
    .locals 3

    const v2, 0x800003

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerLayout:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->isDrawerOpen(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;->setOffset(F)V

    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerIndicatorEnabled:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mDrawerLayout:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->isDrawerOpen(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mOpenDrawerContentDescRes:I

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->setActionBarUpIndicator(Landroid/graphics/drawable/Drawable;I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mSlider:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle$SlideDrawable;->setOffset(F)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->mCloseDrawerContentDescRes:I

    goto :goto_1
.end method
