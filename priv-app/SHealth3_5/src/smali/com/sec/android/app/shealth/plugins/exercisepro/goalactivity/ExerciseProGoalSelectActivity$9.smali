.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;
.super Ljava/lang/Object;
.source "ExerciseProGoalSelectActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->showInputMaxHRPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 6
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v5, 0x1

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    const v2, 0x7f080807

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1102(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;Landroid/widget/EditText;)Landroid/widget/EditText;

    .line 262
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    const-string v3, "inputType=YearDateTime_edittext"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 263
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x42a00000    # 80.0f

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setWidth(I)V

    .line 264
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 305
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    const/high16 v3, 0x41a80000    # 21.0f

    invoke-virtual {v2, v5, v3}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 306
    new-array v0, v5, [Landroid/text/InputFilter;

    .line 307
    .local v0, "fArray":[Landroid/text/InputFilter;
    const/4 v2, 0x0

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/4 v4, 0x3

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v0, v2

    .line 308
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 309
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrValue:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 310
    const v2, 0x7f08080a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 311
    .local v1, "tv":Landroid/widget/TextView;
    const v2, 0x7f0900d2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 334
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;)V

    const-wide/16 v4, 0x320

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 344
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->mMaxHrEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProGoalSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 345
    return-void
.end method
