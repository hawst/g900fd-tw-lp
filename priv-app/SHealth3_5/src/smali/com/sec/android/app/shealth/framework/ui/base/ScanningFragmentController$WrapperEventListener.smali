.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WrapperEventListener"
.end annotation


# instance fields
.field mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

.field private mScanningFrEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mScanningFrEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x627e087bf64eeae9L    # 2.7671862198799844E166

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->setIsPaired(Z)V

    :goto_0
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x6e

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x5e

    aput v2, v0, v3

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_2
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    if-eq v0, v8, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getShealthSensorDevice()Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getConnectivityType()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_6

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->addPairedDeviceToDB(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)Landroid/net/Uri;

    move-result-object v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mScanningFrEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mScanningFrEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_9

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x2c

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x1c

    aput v2, v0, v3

    move v2, v3

    :goto_4
    array-length v5, v0

    if-lt v2, v5, :cond_7

    array-length v0, v1

    new-array v0, v0, [C

    :goto_5
    array-length v2, v0

    if-lt v3, v2, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->updatePairedStatusInDB(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)I

    goto :goto_3

    :cond_7
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_8
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_9
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_a

    xor-long/2addr v0, v9

    :cond_a
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-interface {v4, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;->onJoined(I)V

    :goto_6
    return-void

    :catch_1
    move-exception v0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_6
.end method

.method public onLeft(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, -0x148007d9384906c1L    # -6.5688703993926625E209

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->setIsPaired(Z)V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mDeviceDetail:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/shealth/framework/ui/utils/ScanningFragmentHelper;->updatePairedStatusInDB(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;)I

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mScanningFrEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x55

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x65

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-interface {v4, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;->onLeft(I)V

    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mScanningFrEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V

    return-void
.end method

.method public onStateChanged(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, 0x1d3dccd26d7cff5fL    # 7.896232796520706E-168

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$WrapperEventListener;->mScanningFrEventListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x75

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x45

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-interface {v4, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;->onStateChanged(I)V

    return-void
.end method
