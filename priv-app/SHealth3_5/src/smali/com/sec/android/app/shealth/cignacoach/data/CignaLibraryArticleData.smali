.class public Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
.super Ljava/lang/Object;
.source "CignaLibraryArticleData.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBackgroundResID:I

.field private mCategory:Ljava/lang/String;

.field private mCategoryId:I

.field private mChecked:Z

.field private mDetail:Ljava/lang/String;

.field private mFavorite:Z

.field private mId:I

.field private mSubCategory:Ljava/lang/String;

.field private mSubCategoryId:I

.field private mTitle:Ljava/lang/String;

.field private mTitleImg:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mChecked:Z

    .line 27
    return-void
.end method

.method public static valueOf(Ljava/lang/Object;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .locals 3
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 129
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;-><init>()V

    .line 131
    .local v1, "cignaLibraryArticleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    instance-of v2, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    if-eqz v2, :cond_0

    move-object v0, p0

    .line 133
    check-cast v0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .line 144
    .local v0, "article":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setId(I)V

    .line 145
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getCategoryId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setCategoryId(I)V

    .line 146
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getSubCategoryId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setSubCategoryId(I)V

    .line 147
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getCategory()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setCategory(Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getDetail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setDetail(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getSubCategory()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setSubCategory(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setTitle(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getTitleImg()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setTitleImg(Ljava/lang/String;)V

    .line 152
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->isFavorite()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setFavorite(Z)V

    .line 155
    .end local v0    # "article":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public getBackground()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mBackgroundResID:I

    return v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryId()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mCategoryId:I

    return v0
.end method

.method public getDetail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mDetail:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mDetail:Ljava/lang/String;

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mDetail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mDetail:Ljava/lang/String;

    goto :goto_0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mId:I

    return v0
.end method

.method public getSubCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mSubCategory:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 63
    const-string v0, ""

    .line 65
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mSubCategory:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSubCategoryId()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mSubCategoryId:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleImg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mTitleImg:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mChecked:Z

    return v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mFavorite:Z

    return v0
.end method

.method public setBackground(I)V
    .locals 1
    .param p1, "imageResource"    # I

    .prologue
    .line 112
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mCategory:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryImageResource;->getLibraryArticleImageResource(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mBackgroundResID:I

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mBackgroundResID:I

    goto :goto_0
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mCategory:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setCategoryId(I)V
    .locals 0
    .param p1, "categoryId"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mCategoryId:I

    .line 43
    return-void
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 124
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mChecked:Z

    .line 125
    return-void
.end method

.method public setDetail(Ljava/lang/String;)V
    .locals 0
    .param p1, "detail"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mDetail:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setFavorite(Z)V
    .locals 0
    .param p1, "favorite"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mFavorite:Z

    .line 105
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mId:I

    .line 35
    return-void
.end method

.method public setSubCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "subCategory"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mSubCategory:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setSubCategoryId(I)V
    .locals 0
    .param p1, "mSubCategoryId"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mSubCategoryId:I

    .line 51
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mTitle:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setTitleImg(Ljava/lang/String;)V
    .locals 0
    .param p1, "titleImg"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->mTitleImg:Ljava/lang/String;

    .line 97
    return-void
.end method
