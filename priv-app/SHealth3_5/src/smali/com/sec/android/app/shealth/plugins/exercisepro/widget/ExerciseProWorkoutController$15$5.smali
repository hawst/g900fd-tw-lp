.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->onUpdateTEPaceGuide(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

.field final synthetic val$paceGuide:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;I)V
    .locals 0

    .prologue
    .line 1120
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;->val$paceGuide:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->checkSupportedHRM()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isHRMConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;->val$paceGuide:I

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateVisualGuideText(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;I)V

    .line 1128
    :goto_0
    return-void

    .line 1126
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15$5;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateVisualGuideLayout(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Z)V

    goto :goto_0
.end method
