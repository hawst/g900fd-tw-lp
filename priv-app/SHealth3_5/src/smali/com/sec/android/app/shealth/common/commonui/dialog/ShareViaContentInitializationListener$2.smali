.class Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$2;
.super Ljava/lang/Object;
.source "ShareViaContentInitializationListener.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 111
    .local v2, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->adapter:Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;
    invoke-static {v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;->access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener$AppAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 112
    .local v3, "launchable":Landroid/content/pm/ResolveInfo;
    iget-object v1, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 113
    .local v1, "activityInfo":Landroid/content/pm/ActivityInfo;
    const-string v5, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string/jumbo v5, "package"

    iget-object v6, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 115
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 116
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 117
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 118
    const/4 v5, 0x1

    return v5
.end method
