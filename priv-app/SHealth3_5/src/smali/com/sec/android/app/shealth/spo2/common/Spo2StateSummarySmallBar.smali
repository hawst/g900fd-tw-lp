.class public Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;
.super Landroid/widget/LinearLayout;
.source "Spo2StateSummarySmallBar.java"


# static fields
.field private static FIRST_MAX_VALUE:D

.field private static FIRST_MIN_VALUE:D

.field private static LENGTH:I

.field private static MAX_VALUE:D

.field private static MIN_VALUE:D

.field private static SECOND_MAX_VALUE:D

.field private static SECOND_MIN_VALUE:D

.field private static SIZE_ICON:I

.field private static THIRD_MAX_VALUE:D

.field private static THIRD_MIN_VALUE:D


# instance fields
.field private isFromGraphInfo:Z

.field private ivPolygon:Landroid/widget/ImageView;

.field private llInfo:Landroid/widget/LinearLayout;

.field private tvAvg:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    const-wide v1, 0x4051800000000000L    # 70.0

    .line 21
    const/16 v0, 0x3c

    sput v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->LENGTH:I

    .line 22
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SIZE_ICON:I

    .line 23
    sput-wide v1, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->MIN_VALUE:D

    .line 24
    sput-wide v3, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->MAX_VALUE:D

    .line 26
    sput-wide v1, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->FIRST_MIN_VALUE:D

    .line 27
    const-wide v0, 0x4056800000000000L    # 90.0

    sput-wide v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->FIRST_MAX_VALUE:D

    .line 29
    sget-wide v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->FIRST_MAX_VALUE:D

    sput-wide v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SECOND_MIN_VALUE:D

    .line 30
    const-wide v0, 0x4057c00000000000L    # 95.0

    sput-wide v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SECOND_MAX_VALUE:D

    .line 32
    sget-wide v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SECOND_MAX_VALUE:D

    sput-wide v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->THIRD_MIN_VALUE:D

    .line 33
    sput-wide v3, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->THIRD_MAX_VALUE:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->isFromGraphInfo:Z

    .line 37
    const v0, 0x7f030228

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    const v0, 0x7f080516

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->llInfo:Landroid/widget/LinearLayout;

    .line 39
    const v0, 0x7f080518

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->ivPolygon:Landroid/widget/ImageView;

    .line 40
    const v0, 0x7f080517

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->tvAvg:Landroid/widget/TextView;

    .line 43
    return-void
.end method

.method private convertDptoPx(I)I
    .locals 5
    .param p1, "dp"    # I

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 107
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 108
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v3, p1

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, v3, v4

    .line 109
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method

.method public static getLeftMargin(D)D
    .locals 12
    .param p0, "dobvalue"    # D

    .prologue
    .line 89
    const-wide/16 v2, 0x0

    .line 90
    .local v2, "leftMargin":D
    invoke-static {p0, p1}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    .line 91
    .local v4, "value":J
    long-to-double v6, v4

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->FIRST_MIN_VALUE:D

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_0

    long-to-double v6, v4

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->FIRST_MAX_VALUE:D

    cmpg-double v6, v6, v8

    if-gez v6, :cond_0

    .line 92
    sget v6, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->LENGTH:I

    int-to-double v6, v6

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->FIRST_MAX_VALUE:D

    sget-wide v10, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->FIRST_MIN_VALUE:D

    sub-double/2addr v8, v10

    div-double v0, v6, v8

    .line 93
    .local v0, "dValue":D
    long-to-double v6, v4

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->FIRST_MIN_VALUE:D

    sub-double/2addr v6, v8

    mul-double/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v6, v6

    int-to-double v2, v6

    .line 102
    :goto_0
    return-wide v2

    .line 94
    .end local v0    # "dValue":D
    :cond_0
    long-to-double v6, v4

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SECOND_MIN_VALUE:D

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_1

    long-to-double v6, v4

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SECOND_MAX_VALUE:D

    cmpg-double v6, v6, v8

    if-gez v6, :cond_1

    .line 95
    sget v6, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->LENGTH:I

    int-to-double v6, v6

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SECOND_MAX_VALUE:D

    sget-wide v10, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SECOND_MIN_VALUE:D

    sub-double/2addr v8, v10

    div-double v0, v6, v8

    .line 96
    .restart local v0    # "dValue":D
    long-to-double v6, v4

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->SECOND_MIN_VALUE:D

    sub-double/2addr v6, v8

    mul-double/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v6, v6

    sget v7, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->LENGTH:I

    add-int/2addr v6, v7

    int-to-double v2, v6

    .line 97
    goto :goto_0

    .line 98
    .end local v0    # "dValue":D
    :cond_1
    sget v6, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->LENGTH:I

    int-to-double v6, v6

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->THIRD_MAX_VALUE:D

    sget-wide v10, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->THIRD_MIN_VALUE:D

    sub-double/2addr v8, v10

    div-double v0, v6, v8

    .line 99
    .restart local v0    # "dValue":D
    long-to-double v6, v4

    sget-wide v8, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->THIRD_MIN_VALUE:D

    sub-double/2addr v6, v8

    mul-double/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v6, v6

    sget v7, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->LENGTH:I

    mul-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    int-to-double v2, v6

    goto :goto_0
.end method


# virtual methods
.method public moveToPolygon(D)V
    .locals 9
    .param p1, "value"    # D

    .prologue
    const-wide/16 v7, 0x5f

    const/4 v3, -0x2

    .line 51
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->getLeftMargin(D)D

    move-result-wide v0

    .line 52
    .local v0, "leftMargin":D
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 53
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    cmp-long v3, v3, v7

    if-nez v3, :cond_1

    .line 54
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    add-int/lit8 v3, v3, 0x2

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->convertDptoPx(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 60
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->isFromGraphInfo:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 61
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    const-wide/16 v5, 0x64

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    .line 62
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    add-int/lit8 v3, v3, -0x6

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->convertDptoPx(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 84
    :cond_0
    :goto_1
    const/4 v3, 0x3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 85
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->llInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    return-void

    .line 57
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    add-int/lit8 v3, v3, -0x2

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->convertDptoPx(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0

    .line 63
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    cmp-long v3, v3, v7

    if-nez v3, :cond_3

    .line 64
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    add-int/lit8 v3, v3, -0x2

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->convertDptoPx(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_1

    .line 65
    :cond_3
    invoke-static {p1, p2}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    const-wide/16 v5, 0x5a

    cmp-long v3, v3, v5

    if-nez v3, :cond_4

    .line 66
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    add-int/lit8 v3, v3, -0x7

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->convertDptoPx(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_1

    .line 68
    :cond_4
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    long-to-int v3, v3

    add-int/lit8 v3, v3, -0x8

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->convertDptoPx(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_1
.end method

.method public setGraphAverageVisibility(Z)V
    .locals 3
    .param p1, "isVisibility"    # Z

    .prologue
    const/4 v0, 0x0

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->tvAvg:Landroid/widget/TextView;

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->tvAvg:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setIsFromGraphInfo(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->isFromGraphInfo:Z

    .line 48
    return-void
.end method
