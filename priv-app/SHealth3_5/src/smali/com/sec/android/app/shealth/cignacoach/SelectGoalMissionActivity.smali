.class public Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "SelectGoalMissionActivity.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;,
        Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$SpinnerCustomAdapter;,
        Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;
    }
.end annotation


# static fields
.field private static final DELETE_DIALOG:Ljava/lang/String; = "DeleteDialog"

.field private static final GOAL_MISSION_DELETE_KEY:Ljava/lang/String; = "GoalMissionDelete"

.field private static TAG:Ljava/lang/String;


# instance fields
.field goals_mission_delete:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;",
            ">;"
        }
    .end annotation
.end field

.field private isDeleteDialogShown:Z

.field mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

.field private mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

.field private mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

.field private mExpandableListView:Landroid/widget/ExpandableListView;

.field private mGoalDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mMissionDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSelectAllCheckBox:Landroid/widget/CheckBox;

.field private mSelectAllTouch:Z

.field private mSelectDataAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectSpinner:Landroid/widget/Spinner;

.field private mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

.field private mTotalCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 61
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->isDeleteDialogShown:Z

    .line 63
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->goals_mission_delete:Ljava/util/HashSet;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;

    .line 74
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllTouch:Z

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    .line 572
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$9;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    .line 768
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllTouch:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->isAllDelete()Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllTouch:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->setAdapter()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->allExpandGroup()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;)Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;
    .param p1, "x1"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateActionBar(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->showDeleteDoneDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTotalCount:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->performDelete()V

    return-void
.end method

.method private addSelectSpinner()V
    .locals 6

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    .line 592
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 625
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    .line 628
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_2

    .line 630
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$SpinnerCustomAdapter;

    const v3, 0x7f03021a

    const v4, 0x7f08080a

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$SpinnerCustomAdapter;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    .line 631
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x7f030219

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 634
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 635
    return-void
.end method

.method private allExpandGroup()V
    .locals 2

    .prologue
    .line 235
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    if-eqz v1, :cond_0

    .line 236
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private getListData()V
    .locals 5

    .prologue
    .line 376
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 377
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 379
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalListData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 381
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTotalCount:I

    .line 382
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 383
    .local v1, "missionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTotalCount:I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTotalCount:I

    goto :goto_0

    .line 385
    .end local v1    # "missionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :cond_0
    return-void
.end method

.method private isAllDelete()Z
    .locals 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTotalCount:I

    if-lt v0, v1, :cond_0

    .line 514
    const/4 v0, 0x1

    .line 516
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performDelete()V
    .locals 9

    .prologue
    .line 486
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupCount()I

    move-result v4

    .line 487
    .local v4, "groupCount":I
    const/4 v3, 0x0

    .local v3, "group":I
    :goto_0
    if-ge v3, v4, :cond_3

    .line 489
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v7, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 490
    .local v2, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 491
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getId()I

    move-result v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->UpdateGoalStatus(ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 494
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v7, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChildrenCount(I)I

    move-result v1

    .line 495
    .local v1, "childCount":I
    const/4 v0, 0x0

    .local v0, "child":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 497
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v7, v3, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 499
    .local v5, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 501
    new-instance v6, Lcom/cigna/coach/apiobjects/MissionOptions;

    invoke-direct {v6}, Lcom/cigna/coach/apiobjects/MissionOptions;-><init>()V

    .line 502
    .local v6, "mo":Lcom/cigna/coach/apiobjects/MissionOptions;
    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v6, v7}, Lcom/cigna/coach/apiobjects/MissionOptions;->setMissionStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    move-object v7, v5

    .line 504
    check-cast v7, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v7

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->getId()I

    move-result v8

    invoke-static {v7, v8, v6}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->UpdateMissionStatus(IILcom/cigna/coach/apiobjects/MissionOptions;)V

    .line 495
    .end local v6    # "mo":Lcom/cigna/coach/apiobjects/MissionOptions;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 487
    .end local v5    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 509
    .end local v0    # "child":I
    .end local v1    # "childCount":I
    .end local v2    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_3
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->isDeleteDialogShown:Z

    .line 510
    return-void
.end method

.method private setAdapter()V
    .locals 4

    .prologue
    .line 388
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->goals_mission_delete:Ljava/util/HashSet;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->setMainType(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$MainType;)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$7;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->setOnCheckedCountListener(Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;)V

    .line 404
    return-void
.end method

.method private showDeleteDoneDialog()V
    .locals 8

    .prologue
    const v7, 0x7f090035

    const/4 v4, 0x1

    .line 463
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->isDeleteDialogShown:Z

    .line 464
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v0

    .line 465
    .local v0, "count":I
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v3, 0x2

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v2

    if-le v2, v4, :cond_0

    const v2, 0x7f09011d

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$8;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    .line 482
    .local v1, "deleteDoneBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 483
    return-void

    .line 465
    .end local v1    # "deleteDoneBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_0
    const v2, 0x7f09011c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private toggleActionButtonsVisibility(Z)V
    .locals 2
    .param p1, "visibility"    # Z

    .prologue
    .line 521
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 523
    return-void
.end method

.method private toggleActionButtonsVisibilityforSelectMode(Z)V
    .locals 2
    .param p1, "visibility"    # Z

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 529
    return-void
.end method

.method private updateActionBar(I)V
    .locals 4
    .param p1, "checkedCount"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 445
    if-gtz p1, :cond_1

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v0, v1, :cond_0

    .line 447
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    .line 459
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateSelectedCount(I)V

    .line 460
    return-void

    .line 449
    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->toggleActionButtonsVisibility(Z)V

    goto :goto_0

    .line 452
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v0, v1, :cond_2

    .line 453
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    goto :goto_0

    .line 455
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->toggleActionButtonsVisibility(Z)V

    goto :goto_0
.end method

.method private updateListSelectInfo()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v8, -0x1

    .line 243
    const/4 v1, -0x1

    .line 244
    .local v1, "expandGroupPosition":I
    const/4 v0, -0x1

    .line 246
    .local v0, "expandChildPosition":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 247
    .local v3, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_0

    .line 248
    const-string v7, "EXTRA_NAME_EXPAND_GROUP_POSITION"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 249
    const-string v7, "EXTRA_NAME_EXPAND_CHILD_POSITION"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 251
    :cond_0
    const-string v7, "EXTRA_NAME_EXPAND_GROUP_POSITION"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 252
    const-string v7, "EXTRA_NAME_EXPAND_CHILD_POSITION"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 255
    if-eq v1, v8, :cond_4

    if-eq v0, v8, :cond_4

    .line 257
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v7, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 258
    .local v4, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    if-eqz v4, :cond_3

    .line 259
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isChecked()Z

    move-result v7

    if-nez v7, :cond_1

    move v5, v6

    :cond_1
    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setChecked(Z)V

    .line 263
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->notifyDataSetChanged()V

    .line 265
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v5, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 266
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v5, v1, v0, v6}, Landroid/widget/ExpandableListView;->setSelectedChild(IIZ)Z

    .line 282
    .end local v4    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_2
    :goto_1
    return-void

    .line 261
    .restart local v4    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_3
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->TAG:Ljava/lang/String;

    const-string v7, "MissionData is null~"

    invoke-static {v5, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 268
    .end local v4    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_4
    if-eq v1, v8, :cond_2

    if-ne v0, v8, :cond_2

    .line 270
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v7, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    .line 271
    .local v2, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    if-eqz v2, :cond_5

    .line 272
    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->setChecked(Z)V

    .line 277
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v7, v1, v6}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->allChildItemCheckOfGroup(IZ)V

    .line 279
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v7, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 280
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v7, v1, v5, v6}, Landroid/widget/ExpandableListView;->setSelectedChild(IIZ)Z

    goto :goto_1

    .line 275
    :cond_5
    sget-object v7, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->TAG:Ljava/lang/String;

    const-string v8, "GoalData is null~"

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private updateSelectSpinner(I)V
    .locals 7
    .param p1, "selectedCounts"    # I

    .prologue
    const v6, 0x7f090073

    const v5, 0x7f090071

    const/4 v4, 0x0

    .line 639
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_1

    .line 641
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->addSelectSpinner()V

    .line 643
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    if-nez v0, :cond_2

    .line 645
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    .line 647
    :cond_2
    if-ltz p1, :cond_6

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    const v1, 0x7f090074

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 652
    if-nez p1, :cond_4

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 676
    :goto_1
    return-void

    .line 657
    :cond_4
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTotalCount:I

    if-ne p1, v0, :cond_5

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 661
    :cond_5
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTotalCount:I

    if-ge p1, v0, :cond_3

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 672
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1
.end method

.method private updateSelectedCount(I)V
    .locals 5
    .param p1, "count"    # I

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090074

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 371
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateSelectSpinner(I)V

    .line 372
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 9

    .prologue
    const v4, 0x7f090033

    const v8, 0x7f0207b0

    const/4 v7, 0x1

    const v6, 0x7f090035

    const/4 v5, 0x0

    .line 286
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 288
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v2, v3, :cond_1

    .line 289
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    .line 313
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v2, 0x7f0207cf

    invoke-direct {v1, v2, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 314
    .local v1, "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 315
    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 321
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setIconResDrawable(Landroid/graphics/drawable/Drawable;)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 322
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 323
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 326
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    .line 354
    .end local v0    # "actionBarButtonListener":Landroid/view/View$OnClickListener;
    .end local v1    # "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v2, v3, :cond_0

    .line 329
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$6;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    .line 349
    .restart local v0    # "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-direct {v1, v8, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 350
    .restart local v1    # "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 351
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 7
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 409
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v3, p3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 411
    .local v0, "groupData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 440
    :goto_0
    return v5

    .line 417
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v3, p3, p4}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 418
    .local v2, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isChecked()Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v4

    :goto_1
    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setChecked(Z)V

    .line 419
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->notifyDataSetChanged()V

    .line 422
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, p4, v4}, Landroid/widget/ExpandableListView;->setItemChecked(IZ)V

    .line 423
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    invoke-direct {v1, p3, p4}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;-><init>(II)V

    .line 425
    .local v1, "individual_mission":Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 427
    const-string v3, "Mission Selected"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mission Selected"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->goals_mission_delete:Ljava/util/HashSet;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 430
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isChecked()Z

    move-result v3

    if-nez v3, :cond_3

    .line 432
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->goals_mission_delete:Ljava/util/HashSet;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 433
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->goals_mission_delete:Ljava/util/HashSet;

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 435
    :cond_2
    const-string v3, "Mission Deselected"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mission Deselected"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateActionBar(I)V

    goto/16 :goto_0

    .end local v1    # "individual_mission":Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;
    :cond_4
    move v3, v5

    .line 418
    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 87
    const-string v2, "EXTRA_NAME_DELETE_TYPE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    .line 90
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    const v2, 0x7f030044

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->setContentView(I)V

    .line 94
    const v2, 0x7f080163

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ExpandableListView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setClickable(Z)V

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 99
    const v2, 0x7f080245

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 125
    const v2, 0x7f080244

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 126
    .local v1, "selectAllLayout":Landroid/widget/LinearLayout;
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListView:Landroid/widget/ExpandableListView;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setOnGroupCollapseListener(Landroid/widget/ExpandableListView$OnGroupCollapseListener;)V

    .line 201
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getListData()V

    .line 202
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->setAdapter()V

    .line 203
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateListSelectInfo()V

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateActionBar(I)V

    .line 205
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->allExpandGroup()V

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v2, :cond_1

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    .line 219
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    .line 221
    return-void

    .line 215
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->addSelectSpinner()V

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 360
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v0, v1, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 366
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mTask:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->cancel(Z)Z

    .line 231
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    .line 730
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 733
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;-><init>()V

    .line 736
    .local v0, "gm":Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;
    const-string v5, "DeleteDialog"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 739
    const-string/jumbo v5, "onRestoreInstanceState"

    const-string v6, "Showing Delete Dialog for Deleting Goals and Missions"

    invoke-static {v5, v6}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    const-string v5, "GoalMissionDelete"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/HashSet;

    .line 743
    .local v4, "restored_goals":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;>;"
    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 745
    .local v2, "iterator1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 747
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "gm":Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;

    .line 748
    .restart local v0    # "gm":Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->getChild()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    .line 750
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->getGroup()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;

    .line 751
    .local v1, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;->setChecked(Z)V

    goto :goto_0

    .line 757
    .end local v1    # "goalData":Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->getGroup()I

    move-result v6

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;->getChild()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 758
    .local v3, "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-virtual {v3, v8}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setChecked(Z)V

    goto :goto_0

    .line 763
    .end local v3    # "missionData":Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->showDeleteDoneDialog()V

    .line 765
    .end local v2    # "iterator1":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;>;"
    .end local v4    # "restored_goals":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$GoalMission;>;"
    :cond_2
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 720
    const-string v0, "DeleteDialog"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->isDeleteDialogShown:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 721
    const-string/jumbo v0, "onSaveInstanceState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Was Dialog Shown Before "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->isDeleteDialogShown:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    const-string v0, "GoalMissionDelete"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->goals_mission_delete:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 725
    return-void
.end method
