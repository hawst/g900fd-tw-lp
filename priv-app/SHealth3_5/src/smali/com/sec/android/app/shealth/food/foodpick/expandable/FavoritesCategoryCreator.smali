.class public Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator;
.super Ljava/lang/Object;
.source "FavoritesCategoryCreator.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private sortFavorites(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "favorites":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 64
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public getFoodListItems(Landroid/content/Context;)Ljava/util/List;
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v5, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 41
    .local v5, "foodInfoDao":Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v4, "favoritesList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFavoritesFoods(Landroid/content/Context;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 43
    .local v6, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    new-instance v14, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;

    invoke-direct {v14, v6, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;)V

    invoke-interface {v4, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    .end local v6    # "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator;->sortFavorites(Ljava/util/List;)V

    .line 46
    new-instance v12, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 47
    .local v12, "mealItemDao":Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;
    new-instance v10, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;-><init>(Landroid/content/Context;)V

    .line 48
    .local v10, "mealDao":Lcom/sec/android/app/shealth/food/fooddao/MealDao;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .local v3, "favoriteMeals":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getFavoriteMeals(Landroid/content/Context;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .end local v7    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 50
    .local v9, "meal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;

    invoke-direct {v2, v9, v10}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;Lcom/sec/android/app/shealth/food/fooddao/MealDao;)V

    .line 51
    .local v2, "expandableItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v14

    invoke-interface {v12, v14, v15}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v13

    .line 52
    .local v13, "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 53
    .local v11, "mealItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    new-instance v15, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-interface {v5, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-direct {v15, v11, v14}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V

    invoke-virtual {v2, v15}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;->addChild(Lcom/sec/android/app/shealth/food/foodpick/expandable/ChildFoodListItem;)V

    goto :goto_2

    .line 55
    .end local v11    # "mealItem":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_1
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 57
    .end local v2    # "expandableItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableFoodListItem;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "meal":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v13    # "mealItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator;->sortFavorites(Ljava/util/List;)V

    .line 58
    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 59
    return-object v4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 73
    return-void
.end method
