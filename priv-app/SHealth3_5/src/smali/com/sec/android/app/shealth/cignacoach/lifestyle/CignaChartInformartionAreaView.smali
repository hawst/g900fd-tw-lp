.class public Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;
.super Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;
.source "CignaChartInformartionAreaView.java"


# instance fields
.field private mArrowIcon:Landroid/widget/ImageView;

.field private mCharteDateTxt:Landroid/widget/TextView;

.field private mCoachMessageTxt:Landroid/widget/TextView;

.field private mComparisonLayout:Landroid/widget/LinearLayout;

.field private mScoreTxt:Landroid/widget/TextView;

.field private mTVCompareScore:Landroid/widget/TextView;

.field private mTVCompareString:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;-><init>(Landroid/content/Context;)V

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->initLayout()V

    .line 33
    return-void
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f0801ed

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mTVCompareScore:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0801e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mCoachMessageTxt:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0801e9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mCharteDateTxt:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0801ea

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mScoreTxt:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0801eb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mComparisonLayout:Landroid/widget/LinearLayout;

    .line 67
    const v0, 0x7f0801ec

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mArrowIcon:Landroid/widget/ImageView;

    .line 68
    const v0, 0x7f0801ee

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mTVCompareString:Landroid/widget/TextView;

    .line 69
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 3

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030067

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 40
    .local v0, "view":Landroid/view/View;
    return-object v0
.end method

.method public isVisibleComparisonLayout()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mComparisonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x1

    .line 115
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public refreshInformationAreaView()V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method public setCoachMessageTxt(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mCoachMessageTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method

.method public setCurrentScoreInfo(JII)V
    .locals 3
    .param p1, "timemills"    # J
    .param p3, "score"    # I
    .param p4, "graphType"    # I

    .prologue
    .line 76
    const/4 v0, 0x5

    if-ne p4, v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mCharteDateTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getExcludeDayDateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mScoreTxt:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    return-void

    .line 78
    :cond_1
    const/4 v0, 0x6

    if-ne p4, v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mCharteDateTxt:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getOnlyYear(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public update(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    return-void
.end method

.method public updateComparisonTxt(Ljava/lang/String;I)V
    .locals 5
    .param p1, "comparisonStr"    # Ljava/lang/String;
    .param p2, "lastDifferenceScore"    # I

    .prologue
    const/16 v4, 0x8

    .line 86
    if-nez p2, :cond_0

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mComparisonLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 109
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mComparisonLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 93
    if-gez p2, :cond_2

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mArrowIcon:Landroid/widget/ImageView;

    const v2, 0x7f0200c9

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mArrowIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mContext:Landroid/content/Context;

    const v3, 0x7f0901aa

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mTVCompareScore:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070110

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 102
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mTVCompareScore:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mTVCompareString:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mComparisonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 97
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    if-lez p2, :cond_1

    .line 98
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mArrowIcon:Landroid/widget/ImageView;

    const v2, 0x7f0200ce

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mArrowIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mContext:Landroid/content/Context;

    const v3, 0x7f0901a9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->mTVCompareScore:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07012d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
