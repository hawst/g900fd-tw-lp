.class Lcom/sec/android/app/shealth/cignacoach/util/AssessmentDataFactory;
.super Ljava/lang/Object;
.source "AssessmentDataFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/util/AssessmentDataFactory$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentDataFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentDataFactory;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method

.method public static createDatas(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .locals 22
    .param p0, "lifeStyleType"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 29
    if-nez p0, :cond_0

    const/4 v3, 0x0

    .line 91
    :goto_0
    return-object v3

    .line 31
    :cond_0
    const/4 v3, 0x0

    .line 32
    .local v3, "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    invoke-static {}, Lcom/cigna/coach/factory/CoachFactory;->getInstance()Lcom/cigna/coach/factory/CoachFactory;

    move-result-object v20

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/cigna/coach/factory/CoachFactory;->getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;

    move-result-object v11

    .line 34
    .local v11, "ils":Lcom/cigna/coach/interfaces/ILifeStyle;
    const/4 v10, -0x1

    .line 35
    .local v10, "iconResID":I
    const/4 v5, 0x0

    .line 36
    .local v5, "categoryTypeRequest":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    :try_start_0
    sget-object v20, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentDataFactory$1;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->ordinal()I

    move-result v21

    aget v20, v20, v21

    packed-switch v20, :pswitch_data_0

    .line 59
    :goto_1
    new-instance v4, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    invoke-direct {v4, v10, v5}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;-><init>(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_1

    .line 61
    .end local v3    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .local v4, "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getUserId()Ljava/lang/String;

    move-result-object v18

    .line 62
    .local v18, "userId":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-interface {v11, v0, v5}, Lcom/cigna/coach/interfaces/ILifeStyle;->getQuestionAnswersByCat(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/CategoryGroupQAs;

    move-result-object v6

    .line 63
    .local v6, "cgqas":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/CategoryGroupQAs;->keys()Ljava/util/Enumeration;

    move-result-object v8

    .line 65
    .local v8, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 66
    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 67
    .local v14, "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {v6, v14}, Lcom/cigna/coach/apiobjects/CategoryGroupQAs;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/List;

    .line 69
    .local v19, "value":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 70
    .local v13, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    :cond_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 71
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;

    .line 72
    .local v16, "qag":Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->getQuestionGroupId()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->setGroupID(I)V

    .line 75
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/cigna/coach/apiobjects/QuestionAnswers;
    :try_end_1
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 38
    .end local v4    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v6    # "cgqas":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .end local v8    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v13    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .end local v14    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v16    # "qag":Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
    .end local v18    # "userId":Ljava/lang/String;
    .end local v19    # "value":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .restart local v3    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    :pswitch_0
    const v10, 0x7f0200fb

    .line 39
    :try_start_2
    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 40
    goto :goto_1

    .line 42
    :pswitch_1
    const v10, 0x7f0200fc

    .line 43
    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 44
    goto :goto_1

    .line 46
    :pswitch_2
    const v10, 0x7f0200fd

    .line 47
    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 48
    goto :goto_1

    .line 50
    :pswitch_3
    const v10, 0x7f0200fe

    .line 51
    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 52
    goto :goto_1

    .line 54
    :pswitch_4
    const v10, 0x7f0200ff

    .line 55
    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    :try_end_2
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 79
    .end local v3    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .restart local v4    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .restart local v6    # "cgqas":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .restart local v8    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v13    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .restart local v14    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .restart local v16    # "qag":Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
    .restart local v18    # "userId":Ljava/lang/String;
    .restart local v19    # "value":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    :cond_2
    :try_start_3
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v15

    .line 80
    .local v15, "lqa":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 81
    .local v12, "iqa":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 82
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/cigna/coach/apiobjects/QuestionAnswers;

    .line 83
    .local v17, "qas":Lcom/cigna/coach/apiobjects/QuestionAnswers;
    new-instance v20, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;

    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->getQuestionGroupId()I

    move-result v21

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;-><init>(ILcom/cigna/coach/apiobjects/QuestionAnswers;)V

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->addQAData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentQAData;)V
    :try_end_3
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    .line 87
    .end local v6    # "cgqas":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .end local v8    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v12    # "iqa":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    .end local v13    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .end local v14    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v15    # "lqa":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    .end local v16    # "qag":Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
    .end local v17    # "qas":Lcom/cigna/coach/apiobjects/QuestionAnswers;
    .end local v18    # "userId":Ljava/lang/String;
    .end local v19    # "value":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    :catch_0
    move-exception v7

    move-object v3, v4

    .line 88
    .end local v4    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .restart local v3    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .local v7, "e":Lcom/cigna/coach/exceptions/CoachException;
    :goto_4
    invoke-virtual {v7}, Lcom/cigna/coach/exceptions/CoachException;->printStackTrace()V

    goto/16 :goto_0

    .end local v3    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v7    # "e":Lcom/cigna/coach/exceptions/CoachException;
    .restart local v4    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .restart local v6    # "cgqas":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .restart local v8    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .restart local v18    # "userId":Ljava/lang/String;
    :cond_3
    move-object v3, v4

    .line 89
    .end local v4    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .restart local v3    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    goto/16 :goto_0

    .line 87
    .end local v6    # "cgqas":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .end local v8    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .end local v18    # "userId":Ljava/lang/String;
    :catch_1
    move-exception v7

    goto :goto_4

    .line 36
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static varargs createDatas([Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "lifeStyleTypes"    # [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    if-nez p0, :cond_1

    const/4 v1, 0x0

    .line 103
    :cond_0
    return-object v1

    .line 97
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v1, "assessmentDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;>;"
    move-object v0, p0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 100
    .local v4, "lifeStyleType":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentDataFactory;->createDatas(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
