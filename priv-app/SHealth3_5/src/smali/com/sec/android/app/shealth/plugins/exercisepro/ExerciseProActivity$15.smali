.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;
.super Ljava/lang/Object;
.source "ExerciseProActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0

    .prologue
    .line 1494
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 6
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 1498
    const v3, 0x7f080040

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1499
    .local v2, "text":Landroid/widget/TextView;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090ac7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090ac8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1501
    .local v1, "location_str":Ljava/lang/String;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1503
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    const v3, 0x7f08031b

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->doNotShowCheckBoxLocation:Landroid/widget/CheckBox;
    invoke-static {v4, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$1402(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 1504
    const v3, 0x7f08031c

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1506
    .local v0, "doNotShowText":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->doNotShowCheckBoxLocation:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Landroid/widget/CheckBox;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1512
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1522
    return-void
.end method
