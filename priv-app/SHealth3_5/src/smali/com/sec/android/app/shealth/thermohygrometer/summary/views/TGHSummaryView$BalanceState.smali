.class public final enum Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
.super Ljava/lang/Enum;
.source "TGHSummaryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BalanceState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum CHECKING_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum CHECKING_TEMPERATURE:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum CHECKING_TEMPERATURE_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum DEFAULT:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum HIGH_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum HIGH_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum HIGH_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum LOW_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum LOW_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum LOW_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum NORMAL_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum NORMAL_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

.field public static final enum NORMAL_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;


# instance fields
.field private final mTextIds:[I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 122
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "NORMAL_TEMPERATURE_NORMAL_HUMIDITY"

    new-array v2, v6, [I

    const v3, 0x7f09109e

    aput v3, v2, v5

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 123
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "NORMAL_TEMPERATURE_LOW_HUMIDITY"

    new-array v2, v6, [I

    const v3, 0x7f0910b4

    aput v3, v2, v5

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 124
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "NORMAL_TEMPERATURE_HIGH_HUMIDITY"

    new-array v2, v6, [I

    const v3, 0x7f0910b3

    aput v3, v2, v5

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 126
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "HIGH_TEMPERATURE_HIGH_HUMIDITY"

    new-array v2, v7, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 127
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "HIGH_TEMPERATURE_LOW_HUMIDITY"

    new-array v2, v7, [I

    fill-array-data v2, :array_1

    invoke-direct {v0, v1, v9, v2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 128
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "HIGH_TEMPERATURE_NORMAL_HUMIDITY"

    const/4 v2, 0x5

    new-array v3, v6, [I

    const v4, 0x7f0910b1

    aput v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 130
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "LOW_TEMPERATURE_LOW_HUMIDITY"

    const/4 v2, 0x6

    new-array v3, v7, [I

    fill-array-data v3, :array_2

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 131
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "LOW_TEMPERATURE_HIGH_HUMIDITY"

    const/4 v2, 0x7

    new-array v3, v7, [I

    fill-array-data v3, :array_3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 132
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "LOW_TEMPERATURE_NORMAL_HUMIDITY"

    const/16 v2, 0x8

    new-array v3, v6, [I

    const v4, 0x7f0910b2

    aput v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 134
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "CHECKING_TEMPERATURE"

    const/16 v2, 0x9

    new-array v3, v6, [I

    const v4, 0x7f0910b5

    aput v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_TEMPERATURE:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 135
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "CHECKING_HUMIDITY"

    const/16 v2, 0xa

    new-array v3, v6, [I

    const v4, 0x7f0910b6

    aput v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 136
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "CHECKING_TEMPERATURE_HUMIDITY"

    const/16 v2, 0xb

    new-array v3, v6, [I

    const v4, 0x7f0910b7

    aput v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_TEMPERATURE_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 138
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    const-string v1, "DEFAULT"

    const/16 v2, 0xc

    new-array v3, v6, [I

    const v4, 0x7f0910b8

    aput v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;-><init>(Ljava/lang/String;I[I)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->DEFAULT:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 120
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->NORMAL_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->HIGH_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_LOW_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_HIGH_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->LOW_TEMPERATURE_NORMAL_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_TEMPERATURE:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_TEMPERATURE_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->DEFAULT:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->$VALUES:[Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    return-void

    .line 126
    :array_0
    .array-data 4
        0x7f0910b1
        0x7f0910b3
    .end array-data

    .line 127
    :array_1
    .array-data 4
        0x7f0910b1
        0x7f0910b4
    .end array-data

    .line 130
    :array_2
    .array-data 4
        0x7f0910b2
        0x7f0910b4
    .end array-data

    .line 131
    :array_3
    .array-data 4
        0x7f0910b2
        0x7f0910b3
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I[I)V
    .locals 0
    .param p3, "textId"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)V"
        }
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    iput-object p3, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->mTextIds:[I

    .line 150
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 120
    const-class v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->$VALUES:[Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    return-object v0
.end method


# virtual methods
.method public getTextIds()[I
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->mTextIds:[I

    return-object v0
.end method
