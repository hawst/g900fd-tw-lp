.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;
.super Ljava/lang/Object;
.source "ExerciseProDetailsMapFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->showHRdialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V
    .locals 0

    .prologue
    .line 1184
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 8
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v6, 0x0

    const/16 v7, 0x8

    .line 1190
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const v2, 0x7f08076e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutMedical:Landroid/widget/LinearLayout;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 1191
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const v2, 0x7f080771

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutNonMedical:Landroid/widget/LinearLayout;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 1193
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v2, v3, :cond_1

    .line 1194
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutMedical:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1195
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutNonMedical:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1196
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const v2, 0x7f080772

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->hrBpmTextNonMedical:Landroid/widget/TextView;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$802(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1197
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->hrBpmTextNonMedical:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->bpm:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1258
    :cond_0
    :goto_0
    return-void

    .line 1201
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutMedical:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1202
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutNonMedical:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1206
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const v2, 0x7f080770

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    .line 1208
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const v2, 0x7f080760

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->hrBpmText:Landroid/widget/TextView;

    .line 1209
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const v2, 0x7f08076b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    .line 1210
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const v2, 0x7f08076c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxBpmText:Landroid/widget/TextView;

    .line 1214
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    const v2, 0x7f08075e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->rangeText:Landroid/widget/TextView;

    .line 1216
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0684

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPolygonWidth:I

    .line 1217
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0685

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPolygonHeight:I

    .line 1221
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->bpm:I

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setHRMStateBar(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;I)V

    .line 1223
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->hrBpmText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->bpm:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1225
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->rangeText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090f68

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minHRzone:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v6, v6, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxHRzone:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1226
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minHRzone:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1227
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxBpmText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxHRzone:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1228
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minHRzone:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxHRzone:I

    if-nez v2, :cond_0

    .line 1233
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->setVisibility(I)V

    .line 1235
    const v2, 0x7f080764

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1236
    .local v1, "hrLowIndex":Landroid/widget/TextView;
    const v2, 0x7f080766

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1237
    .local v0, "hrHighIndex":Landroid/widget/TextView;
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1238
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1239
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1240
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxBpmText:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1241
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->rangeText:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method
