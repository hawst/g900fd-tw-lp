.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$5;
.super Ljava/lang/Object;
.source "ProfileFirstView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mUseRankingCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/CheckBox;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mUseRankingCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 227
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mUseRankingCheck:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->announceForCheckBoxAccessibility(Landroid/view/View;Ljava/lang/String;ZZ)V

    .line 228
    return-void

    :cond_0
    move v0, v1

    .line 226
    goto :goto_0
.end method
