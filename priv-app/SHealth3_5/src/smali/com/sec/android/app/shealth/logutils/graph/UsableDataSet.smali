.class public Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;
.super Lcom/samsung/android/sdk/chart/series/SchartDataSet;
.source "UsableDataSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    return-void
.end method

.method private static getMaxTimeForSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)J
    .locals 4
    .param p0, "series"    # Lcom/samsung/android/sdk/chart/series/SchartSeries;

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSize()I

    move-result v0

    .line 100
    .local v0, "seriesSize":I
    if-nez v0, :cond_0

    .line 101
    const-wide/high16 v1, -0x8000000000000000L

    .line 107
    .end local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :goto_0
    return-wide v1

    .line 104
    .restart local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_0
    instance-of v1, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    if-eqz v1, :cond_1

    .line 105
    check-cast p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .end local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getX(I)J

    move-result-wide v1

    goto :goto_0

    .line 106
    .restart local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_1
    instance-of v1, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    if-eqz v1, :cond_2

    .line 107
    check-cast p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    .end local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->getX(I)J

    move-result-wide v1

    goto :goto_0

    .line 109
    .restart local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Series type is illegal: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static getMinTimeForSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)J
    .locals 4
    .param p0, "series"    # Lcom/samsung/android/sdk/chart/series/SchartSeries;

    .prologue
    .line 84
    instance-of v1, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    if-eqz v1, :cond_0

    .line 85
    check-cast p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .end local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    .line 91
    .local v0, "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 92
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v1

    .line 94
    :goto_1
    return-wide v1

    .line 86
    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    .restart local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_0
    instance-of v1, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    if-eqz v1, :cond_1

    .line 87
    check-cast p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    .end local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    .restart local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    goto :goto_0

    .line 89
    .end local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    .restart local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Series type is illegal: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 94
    .end local p0    # "series":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    .restart local v0    # "dataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    :cond_2
    const-wide v1, 0x7fffffffffffffffL

    goto :goto_1
.end method


# virtual methods
.method public addXYTimeSeriesList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "seriesDataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 119
    .local v1, "timeSeries":Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    goto :goto_0

    .line 121
    .end local v1    # "timeSeries":Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;
    :cond_0
    return-void
.end method

.method public getMaxDataTime()J
    .locals 6

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getSeriesCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 69
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot count maximal data time while no data added to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 72
    :cond_0
    const-wide/high16 v1, -0x8000000000000000L

    .line 73
    .local v1, "maxTime":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getSeriesCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 74
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getMaxTimeForSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_1
    const-wide/high16 v3, -0x8000000000000000L

    cmp-long v3, v1, v3

    if-nez v3, :cond_2

    .line 77
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "All series are not allowed to be empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 79
    :cond_2
    return-wide v1
.end method

.method public getMinDataTime()J
    .locals 6

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getSeriesCount()I

    move-result v3

    if-nez v3, :cond_0

    .line 50
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot count minimal data time while no data added to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 53
    :cond_0
    const-wide v1, 0x7fffffffffffffffL

    .line 54
    .local v1, "minTime":J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getSeriesCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 55
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getMinTimeForSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_1
    const-wide v3, 0x7fffffffffffffffL

    cmp-long v3, v1, v3

    if-nez v3, :cond_2

    .line 58
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "All series are not allowed to be empty"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 60
    :cond_2
    return-wide v1
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 36
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getSeriesCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 37
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/logutils/graph/UsableDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSize()I

    move-result v1

    if-lez v1, :cond_0

    .line 38
    const/4 v1, 0x0

    .line 41
    :goto_1
    return v1

    .line 36
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
