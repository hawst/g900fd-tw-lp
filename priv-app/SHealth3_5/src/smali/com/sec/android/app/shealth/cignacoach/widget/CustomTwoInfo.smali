.class public Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;
.super Landroid/widget/RelativeLayout;
.source "CustomTwoInfo.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$OnInfoClickListener;
    }
.end annotation


# instance fields
.field private mInfoClickListener:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$OnInfoClickListener;

.field private mInfoLeftCountTxt:Landroid/widget/TextView;

.field private mInfoLeftLayout:Landroid/widget/LinearLayout;

.field private mInfoLeftTitleTxt:Landroid/widget/TextView;

.field private mInfoRightCountTxt:Landroid/widget/TextView;

.field private mInfoRightLayout:Landroid/widget/LinearLayout;

.field private mInfoRightTitleTxt:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->init()V

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->initListener()V

    .line 37
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030042

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 42
    const v0, 0x7f08015c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoLeftLayout:Landroid/widget/LinearLayout;

    .line 43
    const v0, 0x7f08015f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoRightLayout:Landroid/widget/LinearLayout;

    .line 44
    const v0, 0x7f08015d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoLeftCountTxt:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f080160

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoRightCountTxt:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f08015e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoLeftTitleTxt:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f080161

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoRightTitleTxt:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoLeftLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoRightLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 60
    return-void
.end method

.method private initListener()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoLeftLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoRightLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoClickListener:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$OnInfoClickListener;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoClickListener:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$OnInfoClickListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$OnInfoClickListener;->onInfoClick(Landroid/view/View;)V

    .line 104
    :cond_0
    return-void
.end method

.method public setInfoClickListener(Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$OnInfoClickListener;)V
    .locals 0
    .param p1, "infoClickListener"    # Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$OnInfoClickListener;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoClickListener:Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo$OnInfoClickListener;

    .line 93
    return-void
.end method

.method public setLeftCountText(Ljava/lang/String;)V
    .locals 1
    .param p1, "count"    # Ljava/lang/String;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoLeftCountTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    return-void
.end method

.method public setLeftCountTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoLeftCountTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    return-void
.end method

.method public setLeftTitleText(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoLeftTitleTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void
.end method

.method public setRightCountText(Ljava/lang/String;)V
    .locals 1
    .param p1, "count"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoRightCountTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method

.method public setRightCountTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoRightCountTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 89
    return-void
.end method

.method public setRightTitleText(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CustomTwoInfo;->mInfoRightTitleTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method
