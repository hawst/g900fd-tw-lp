.class Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;
.super Ljava/lang/Object;
.source "FavoritesListAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyOnCheckedChildChangeListener"
.end annotation


# instance fields
.field private mChildData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;I)V
    .locals 0
    .param p2, "childData"    # Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .param p3, "groupPosition"    # I

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;->mChildData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 219
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;->mChildData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setChecked(Z)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->access$000(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;)Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->mCheckedCountListener:Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->access$000(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;)Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$MyOnCheckedChildChangeListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getCheckedItemCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;->checkedCheckBoxCountChanged(I)V

    .line 229
    :cond_0
    return-void
.end method
