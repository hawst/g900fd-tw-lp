.class public final Lcom/sec/android/app/shealth/walkingmate/constants/ConfigurationConstants;
.super Ljava/lang/Object;
.source "ConfigurationConstants.java"


# static fields
.field public static final CHINA_AMAP_LOCATION_ON:Z

.field public static CIGNA_ENABLED:Z = false

.field public static final CROWN_CONFIG:I = 0x2710

.field public static final MAX_VISIBLE_STEPS:I = 0x1869f

.field public static final PEDOMETER_LOGGING_ACCESSORY:Ljava/lang/String; = "W002"

.field public static final PEDOMETER_LOGGING_ACHIEVE_GOAL:Ljava/lang/String; = "STEP100"

.field public static final PEDOMETER_LOGGING_ACHIEVE_HALF_GOAL:Ljava/lang/String; = "STEP50"

.field public static final PEDOMETER_LOGGING_CHART:Ljava/lang/String; = "W006"

.field public static final PEDOMETER_LOGGING_DEVICE_CHANGED:Ljava/lang/String; = "W013"

.field public static final PEDOMETER_LOGGING_ENTER_WIDGET:Ljava/lang/String; = "W030"

.field public static final PEDOMETER_LOGGING_LOG:Ljava/lang/String; = "W003"

.field public static final PEDOMETER_LOGGING_PAUSE:Ljava/lang/String; = "W008"

.field public static final PEDOMETER_LOGGING_PEDOMETER_ONOFF:Ljava/lang/String; = "W009"

.field public static final PEDOMETER_LOGGING_RANKING:Ljava/lang/String; = "W005"

.field public static final PEDOMETER_LOGGING_SETGOAL:Ljava/lang/String; = "W004"

.field public static final PEDOMETER_LOGGING_SHARE_DETAIL:Ljava/lang/String; = "W021"

.field public static final PEDOMETER_LOGGING_SHARE_RANKING:Ljava/lang/String; = "W022"

.field public static final PEDOMETER_LOGGING_SHARE_SUMMARY_ACTIONBAR:Ljava/lang/String; = "W020"

.field public static final PEDOMETER_LOGGING_SHARE_SUMMARY_OPTION:Ljava/lang/String; = "W010"

.field public static final PEDOMETER_LOGGING_START:Ljava/lang/String; = "W001"

.field public static final PEDOMETER_LOGGING_START_FOR_DAY:Ljava/lang/String; = "STEP0"

.field public static final PEDOMETER_LOGGING_USER_MANUAL:Ljava/lang/String; = "W007"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/constants/ConfigurationConstants;->CIGNA_ENABLED:Z

    .line 27
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isChinaModel()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
