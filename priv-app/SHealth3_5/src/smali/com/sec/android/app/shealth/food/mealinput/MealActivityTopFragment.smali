.class public Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;
.super Landroid/support/v4/app/Fragment;
.source "MealActivityTopFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;,
        Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$TimePickerDialogButtonsController;
    }
.end annotation


# static fields
.field private static final CHOOSE_MEAL_TYPE_POPUP_SHOWN:Ljava/lang/String; = "CHOOSE_MEAL_TYPE_POPUP_SHOWN"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MEAL_TIME_KEY:Ljava/lang/String; = "meal_time"

.field private static final MEAL_TYPE_KEY:Ljava/lang/String; = "meal_type"

.field private static final TYPES_MAP:[I


# instance fields
.field private mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

.field mFakeView:Landroid/widget/RelativeLayout;

.field private mHoverListener:Landroid/view/View$OnHoverListener;

.field private mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

.field private mListPopupShown:Z

.field private mMealType:I

.field private mMealTypeSelector:Landroid/widget/TextView;

.field private mMealTypeSelectorLL:Landroid/widget/LinearLayout;

.field private mSelectedIndexInPopup:I

.field private mStringValuesMealTypes:[Ljava/lang/String;

.field private mTime:J

.field private mTimeDateTouchListener:Landroid/view/View$OnTouchListener;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->LOG_TAG:Ljava/lang/String;

    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->TYPES_MAP:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x186a1
        0x186a2
        0x186a3
        0x186a4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 72
    const v0, 0x186a1

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopupShown:Z

    .line 279
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$4;-><init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mTimeDateTouchListener:Landroid/view/View$OnTouchListener;

    .line 294
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$5;-><init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mHoverListener:Landroid/view/View$OnHoverListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->showMealTypeSelectorPopup()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->resetFocus(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopupShown:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelector:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    return p1
.end method

.method static synthetic access$600()[I
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->TYPES_MAP:[I

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method private calculateSelectedIndexInListPopup(I)I
    .locals 3
    .param p1, "mealType"    # I

    .prologue
    .line 169
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->TYPES_MAP:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 170
    sget-object v1, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->TYPES_MAP:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 171
    return v0

    .line 169
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "wrong mealType, value mealType must be one of ShealthContract.Constants.MealType"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private handleSelectedIndexForListPopup()V
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->calculateSelectedIndexInListPopup(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mSelectedIndexInPopup:I

    .line 166
    return-void
.end method

.method private resetFocus(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 312
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mFakeView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mFakeView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestFocus()Z

    .line 320
    :cond_0
    return-void
.end method

.method private showMealTypeSelectorPopup()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 136
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_0

    instance-of v2, v0, Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 139
    .local v1, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 140
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->resetFocus(Landroid/view/View;)V

    .line 142
    .end local v1    # "inputManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    if-eqz v2, :cond_1

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->getSelectedItemIndex()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mSelectedIndexInPopup:I

    .line 145
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mStringValuesMealTypes:[Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setScrollable(Z)V

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mSelectedIndexInPopup:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setCurrentItem(I)V

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    new-instance v3, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setOnItemClickedListener(Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v3

    rsub-int/lit8 v3, v3, 0x0

    invoke-virtual {v2, v6, v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->show(II)V

    .line 162
    return-void
.end method


# virtual methods
.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 180
    const-string/jumbo v0, "time_picker_dialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$TimePickerDialogButtonsController;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$TimePickerDialogButtonsController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$1;)V

    .line 185
    :goto_0
    return-object v0

    .line 182
    :cond_0
    const-string v0, "date_picker_dialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$1;)V

    goto :goto_0

    .line 185
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDialogNotSupported(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method public getMealType()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    return v0
.end method

.method public getTimeInMillis()J
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 81
    const v5, 0x7f0300e3

    invoke-virtual {p1, v5, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 82
    .local v2, "fragmentView":Landroid/view/View;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mTime:J

    .line 83
    const v5, 0x7f0803e8

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 85
    .local v1, "dateTimePickerView":Landroid/view/View;
    new-instance v5, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-direct {v5, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    .line 87
    const v5, 0x7f0803a4

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 88
    .local v0, "dateButton":Landroid/widget/Button;
    const v5, 0x7f0803a5

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 90
    .local v3, "timeButton":Landroid/widget/Button;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mTimeDateTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 91
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mTimeDateTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 92
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 94
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 96
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mTime:J

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->setMeasureTime(J)V

    .line 97
    if-eqz p3, :cond_0

    .line 98
    const-string/jumbo v5, "meal_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {p3, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mTime:J

    .line 99
    const-string/jumbo v5, "meal_type"

    const v6, 0x186a1

    invoke-virtual {p3, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    .line 100
    const-string v5, "CHOOSE_MEAL_TYPE_POPUP_SHOWN"

    invoke-virtual {p3, v5, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopupShown:Z

    .line 101
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v5, p3}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 103
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->refreshDateTime()V

    .line 104
    const v5, 0x7f0803d9

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mFakeView:Landroid/widget/RelativeLayout;

    .line 105
    const v5, 0x7f0803ea

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelector:Landroid/widget/TextView;

    .line 106
    const v5, 0x7f0803e9

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    .line 107
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelector:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealTypeString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0025

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mStringValuesMealTypes:[Ljava/lang/String;

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->handleSelectedIndexForListPopup()V

    .line 110
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    new-instance v6, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;

    new-instance v7, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V

    invoke-direct {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListenerDecorator;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    .line 118
    .local v4, "viewTreeObserver":Landroid/view/ViewTreeObserver;
    invoke-virtual {v4}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 119
    new-instance v5, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 129
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v5}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 130
    return-object v2
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 190
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 191
    const-string/jumbo v0, "meal_time"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 192
    const-string/jumbo v0, "meal_type"

    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "CHOOSE_MEAL_TYPE_POPUP_SHOWN"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mListPopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->isShowing()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 197
    return-void
.end method

.method public setEnableMealTypeChoice(Z)V
    .locals 1
    .param p1, "enableMealType"    # Z

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelector:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 226
    return-void
.end method

.method public setMealType(I)V
    .locals 4
    .param p1, "mealType"    # I

    .prologue
    .line 212
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelector:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealType:I

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealTypeString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelectorLL:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mMealTypeSelector:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ec

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->handleSelectedIndexForListPopup()V

    .line 216
    return-void
.end method

.method public setTimeInMillis(J)V
    .locals 1
    .param p1, "timeInMillis"    # J

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->setMeasureTime(J)V

    .line 207
    iput-wide p1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mTime:J

    .line 208
    return-void
.end method
