.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;
.source "SearchListByCategoryResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
        ">",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase",
        "<TE;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .param p2, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TE;>;I)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult<TE;>;"
    .local p1, "searchResult":Ljava/util/List;, "Ljava/util/List<TE;>;"
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;-><init>(Ljava/util/List;I)V

    .line 31
    return-void
.end method


# virtual methods
.method public createNextSearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;
    .locals 5
    .param p1, "searchApi"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .prologue
    .line 43
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult<TE;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;->getRequest()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    .line 44
    .local v0, "prevRequest":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;->isAllDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    const/4 v1, 0x0

    .line 47
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getSubCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getPageNumber()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getPerPageNumber()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;II)V

    goto :goto_0
.end method

.method public bridge synthetic createNextSearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .prologue
    .line 23
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult<TE;>;"
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;->createNextSearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    move-result-object v0

    return-object v0
.end method
