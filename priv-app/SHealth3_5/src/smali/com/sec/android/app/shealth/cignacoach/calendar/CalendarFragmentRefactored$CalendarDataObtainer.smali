.class Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;
.super Ljava/lang/Thread;
.source "CalendarFragmentRefactored.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CalendarDataObtainer"
.end annotation


# instance fields
.field dayButtonMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;",
            ">;"
        }
    .end annotation
.end field

.field monthEnd:J

.field monthStart:J

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;JJLjava/util/Map;)V
    .locals 0
    .param p2, "periodStart"    # J
    .param p4, "periodEnd"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 944
    .local p6, "periodStartToDayButtonMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 945
    iput-wide p2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->monthStart:J

    .line 946
    iput-wide p4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->monthEnd:J

    .line 947
    iput-object p6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->dayButtonMap:Ljava/util/Map;

    .line 948
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;JJLjava/util/Map;Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
    .param p2, "x1"    # J
    .param p4, "x2"    # J
    .param p6, "x3"    # Ljava/util/Map;
    .param p7, "x4"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$1;

    .prologue
    .line 936
    invoke-direct/range {p0 .. p6}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;JJLjava/util/Map;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 953
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 958
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->access$200(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 964
    .local v2, "dayStart":Ljava/lang/Long;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->access$200(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;

    .line 966
    .local v0, "calendarButton":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;
    const/4 v1, 0x0

    .line 967
    .local v1, "containsMeasurements":Z
    new-instance v4, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v2, v6}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/lang/Long;Z)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 972
    .end local v0    # "calendarButton":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;
    .end local v1    # "containsMeasurements":Z
    .end local v2    # "dayStart":Ljava/lang/Long;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    iget-wide v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->monthStart:J

    iget-wide v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->monthEnd:J

    invoke-virtual {v4, v6, v7, v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getDaysStatuses(JJ)Ljava/util/TreeMap;

    move-result-object v4

    # setter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mDaysInfo:Ljava/util/TreeMap;
    invoke-static {v5, v4}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->access$302(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/util/TreeMap;)Ljava/util/TreeMap;

    .line 974
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->dayButtonMap:Ljava/util/Map;

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mDaysInfo:Ljava/util/TreeMap;
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->access$300(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;)Ljava/util/TreeMap;

    move-result-object v6

    # invokes: Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->checkIfDataExists(Ljava/util/Map;Ljava/util/TreeMap;)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->access$400(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/util/Map;Ljava/util/TreeMap;)V

    .line 975
    return-void
.end method
