.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->initialize(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

.field final synthetic val$serviceConnection:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$1;->val$serviceConnection:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 40

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, 0x54bed06b84f72a37L    # 1.6849562848863278E100

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, 0x54bed06b84f72a37L    # 1.6849562848863278E100

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$1;->val$serviceConnection:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x69

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x59

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_5

    const-wide v5, 0x54bed06b84f72a37L    # 1.6849562848863278E100

    xor-long/2addr v1, v5

    :cond_5
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    invoke-interface {v4, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;->onServiceConnected(I)V

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1c70

    aput v28, v2, v27

    const/16 v27, -0x3687

    aput v27, v2, v26

    const/16 v26, -0x5b

    aput v26, v2, v25

    const/16 v25, -0xde

    aput v25, v2, v24

    const/16 v24, -0x70

    aput v24, v2, v23

    const/16 v23, -0xe

    aput v23, v2, v22

    const/16 v22, -0x55

    aput v22, v2, v21

    const/16 v21, -0x1dd0

    aput v21, v2, v20

    const/16 v20, -0x73

    aput v20, v2, v19

    const/16 v19, 0x11f

    aput v19, v2, v18

    const/16 v18, 0xb75

    aput v18, v2, v17

    const/16 v17, -0x179b

    aput v17, v2, v16

    const/16 v16, -0x73

    aput v16, v2, v15

    const/16 v15, 0x533b

    aput v15, v2, v14

    const/16 v14, 0x634

    aput v14, v2, v13

    const/16 v13, 0x1867

    aput v13, v2, v12

    const/16 v12, 0x196a

    aput v12, v2, v11

    const/16 v11, 0x575f

    aput v11, v2, v10

    const/16 v10, -0x4bd0

    aput v10, v2, v9

    const/16 v9, -0x26

    aput v9, v2, v8

    const/16 v8, 0x7d47

    aput v8, v2, v7

    const/16 v7, 0x713

    aput v7, v2, v6

    const/16 v6, 0x7169

    aput v6, v2, v5

    const/16 v5, -0x29f0

    aput v5, v2, v4

    const/16 v4, -0x4b

    aput v4, v2, v3

    const/16 v3, -0x13

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1c02

    aput v29, v1, v28

    const/16 v28, -0x36e4

    aput v28, v1, v27

    const/16 v27, -0x37

    aput v27, v1, v26

    const/16 v26, -0xb2

    aput v26, v1, v25

    const/16 v25, -0x1

    aput v25, v1, v24

    const/16 v24, -0x80

    aput v24, v1, v23

    const/16 v23, -0x21

    aput v23, v1, v22

    const/16 v22, -0x1da2

    aput v22, v1, v21

    const/16 v21, -0x1e

    aput v21, v1, v20

    const/16 v20, 0x15c

    aput v20, v1, v19

    const/16 v19, 0xb01

    aput v19, v1, v18

    const/16 v18, -0x17f5

    aput v18, v1, v17

    const/16 v17, -0x18

    aput v17, v1, v16

    const/16 v16, 0x5356

    aput v16, v1, v15

    const/16 v15, 0x653

    aput v15, v1, v14

    const/16 v14, 0x1806

    aput v14, v1, v13

    const/16 v13, 0x1918

    aput v13, v1, v12

    const/16 v12, 0x5719

    aput v12, v1, v11

    const/16 v11, -0x4ba9

    aput v11, v1, v10

    const/16 v10, -0x4c

    aput v10, v1, v9

    const/16 v9, 0x7d2e

    aput v9, v1, v8

    const/16 v8, 0x77d

    aput v8, v1, v7

    const/16 v7, 0x7107

    aput v7, v1, v6

    const/16 v6, -0x298f

    aput v6, v1, v5

    const/16 v5, -0x2a

    aput v5, v1, v4

    const/16 v4, -0x42

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0x3c

    aput v38, v2, v37

    const/16 v37, -0x32

    aput v37, v2, v36

    const/16 v36, 0x5e5a

    aput v36, v2, v35

    const/16 v35, -0x4dc3

    aput v35, v2, v34

    const/16 v34, -0x29

    aput v34, v2, v33

    const/16 v33, -0x5ac

    aput v33, v2, v32

    const/16 v32, -0x6c

    aput v32, v2, v31

    const/16 v31, -0x35

    aput v31, v2, v30

    const/16 v30, 0x5667

    aput v30, v2, v29

    const/16 v29, 0x6176

    aput v29, v2, v28

    const/16 v28, 0x7d04

    aput v28, v2, v27

    const/16 v27, -0x1be2

    aput v27, v2, v26

    const/16 v26, -0x73

    aput v26, v2, v25

    const/16 v25, -0x37

    aput v25, v2, v24

    const/16 v24, -0x3

    aput v24, v2, v23

    const/16 v23, -0x59

    aput v23, v2, v22

    const/16 v22, -0x17e7

    aput v22, v2, v21

    const/16 v21, -0x38

    aput v21, v2, v20

    const/16 v20, 0xf04

    aput v20, v2, v19

    const/16 v19, 0x412f

    aput v19, v2, v18

    const/16 v18, -0x5adc

    aput v18, v2, v17

    const/16 v17, -0x35

    aput v17, v2, v16

    const/16 v16, -0x49

    aput v16, v2, v15

    const/16 v15, -0x1c91

    aput v15, v2, v14

    const/16 v14, -0x3d

    aput v14, v2, v13

    const/16 v13, -0x7c

    aput v13, v2, v12

    const/4 v12, -0x2

    aput v12, v2, v11

    const/16 v11, -0x33a3

    aput v11, v2, v10

    const/16 v10, -0x55

    aput v10, v2, v9

    const/4 v9, -0x2

    aput v9, v2, v8

    const/16 v8, -0x2d91

    aput v8, v2, v7

    const/16 v7, -0x4a

    aput v7, v2, v6

    const/16 v6, -0x1586

    aput v6, v2, v5

    const/16 v5, -0x7d

    aput v5, v2, v3

    const/16 v3, -0x59d5

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x60

    aput v39, v1, v38

    const/16 v38, -0x55

    aput v38, v1, v37

    const/16 v37, 0x5e2e

    aput v37, v1, v36

    const/16 v36, -0x4da2

    aput v36, v1, v35

    const/16 v35, -0x4e

    aput v35, v1, v34

    const/16 v34, -0x5c6

    aput v34, v1, v33

    const/16 v33, -0x6

    aput v33, v1, v32

    const/16 v32, -0x5c

    aput v32, v1, v31

    const/16 v31, 0x5604

    aput v31, v1, v30

    const/16 v30, 0x6156

    aput v30, v1, v29

    const/16 v29, 0x7d61

    aput v29, v1, v28

    const/16 v28, -0x1b83

    aput v28, v1, v27

    const/16 v27, -0x1c

    aput v27, v1, v26

    const/16 v26, -0x41

    aput v26, v1, v25

    const/16 v25, -0x71

    aput v25, v1, v24

    const/16 v24, -0x3e

    aput v24, v1, v23

    const/16 v23, -0x17b6

    aput v23, v1, v22

    const/16 v22, -0x18

    aput v22, v1, v21

    const/16 v21, 0xf29

    aput v21, v1, v20

    const/16 v20, 0x410f

    aput v20, v1, v19

    const/16 v19, -0x5abf

    aput v19, v1, v18

    const/16 v18, -0x5b

    aput v18, v1, v17

    const/16 v17, -0x28

    aput v17, v1, v16

    const/16 v16, -0x1cf5

    aput v16, v1, v15

    const/16 v15, -0x1d

    aput v15, v1, v14

    const/16 v14, -0x9

    aput v14, v1, v13

    const/16 v13, -0x69

    aput v13, v1, v12

    const/16 v12, -0x3383

    aput v12, v1, v11

    const/16 v11, -0x34

    aput v11, v1, v10

    const/16 v10, -0x70

    aput v10, v1, v9

    const/16 v9, -0x2dfa

    aput v9, v1, v8

    const/16 v8, -0x2e

    aput v8, v1, v7

    const/16 v7, -0x15ec

    aput v7, v1, v6

    const/16 v6, -0x16

    aput v6, v1, v5

    const/16 v5, -0x5997

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5
.end method

.method public onServiceDisconnected(I)V
    .locals 30

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    const/4 v1, 0x0

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    aget-wide v4, v3, v2

    long-to-int v2, v4

    if-gtz v2, :cond_0

    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v1, v0

    const/16 v5, 0x20

    shl-long/2addr v1, v5

    const/16 v5, 0x20

    ushr-long v5, v1, v5

    aget-wide v1, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_1

    const-wide v7, -0x87d934aade80dd6L    # -4.752752669039914E267

    xor-long/2addr v1, v7

    :cond_1
    const/16 v7, 0x20

    ushr-long/2addr v1, v7

    const/16 v7, 0x20

    shl-long/2addr v1, v7

    xor-long/2addr v1, v5

    const-wide v5, -0x87d934aade80dd6L    # -4.752752669039914E267

    xor-long/2addr v1, v5

    aput-wide v1, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$1;->val$serviceConnection:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget-wide v1, v3, v1

    long-to-int v1, v1

    if-gtz v1, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x24d2

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x24e2

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    aget-wide v1, v3, v1

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-eqz v3, :cond_5

    const-wide v5, -0x87d934aade80dd6L    # -4.752752669039914E267

    xor-long/2addr v1, v5

    :cond_5
    const/16 v3, 0x20

    shl-long/2addr v1, v3

    const/16 v3, 0x20

    shr-long/2addr v1, v3

    long-to-int v1, v1

    invoke-interface {v4, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;->onServiceDisconnected(I)V

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x5c15

    aput v28, v2, v27

    const/16 v27, -0x20c7

    aput v27, v2, v26

    const/16 v26, -0x4d

    aput v26, v2, v25

    const/16 v25, -0x7882

    aput v25, v2, v24

    const/16 v24, -0x18

    aput v24, v2, v23

    const/16 v23, 0x6c24

    aput v23, v2, v22

    const/16 v22, 0x2e18

    aput v22, v2, v21

    const/16 v21, -0x26c0

    aput v21, v2, v20

    const/16 v20, -0x4a

    aput v20, v2, v19

    const/16 v19, -0x2b

    aput v19, v2, v18

    const/16 v18, 0x2e1c

    aput v18, v2, v17

    const/16 v17, 0x7c40

    aput v17, v2, v16

    const/16 v16, 0x6019

    aput v16, v2, v15

    const/16 v15, -0x40f3

    aput v15, v2, v14

    const/16 v14, -0x28

    aput v14, v2, v13

    const/16 v13, 0x17

    aput v13, v2, v12

    const/16 v12, 0x4b72

    aput v12, v2, v11

    const/16 v11, 0x240d

    aput v11, v2, v10

    const/16 v10, -0x5dbd

    aput v10, v2, v9

    const/16 v9, -0x34

    aput v9, v2, v8

    const/16 v8, -0x59

    aput v8, v2, v7

    const/16 v7, -0x3dfc

    aput v7, v2, v6

    const/16 v6, -0x54

    aput v6, v2, v5

    const/4 v5, -0x2

    aput v5, v2, v4

    const/16 v4, 0x1f66

    aput v4, v2, v3

    const/16 v3, 0x5f4c

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x5c67

    aput v29, v1, v28

    const/16 v28, -0x20a4

    aput v28, v1, v27

    const/16 v27, -0x21

    aput v27, v1, v26

    const/16 v26, -0x78ee

    aput v26, v1, v25

    const/16 v25, -0x79

    aput v25, v1, v24

    const/16 v24, 0x6c56

    aput v24, v1, v23

    const/16 v23, 0x2e6c

    aput v23, v1, v22

    const/16 v22, -0x26d2

    aput v22, v1, v21

    const/16 v21, -0x27

    aput v21, v1, v20

    const/16 v20, -0x6a

    aput v20, v1, v19

    const/16 v19, 0x2e68

    aput v19, v1, v18

    const/16 v18, 0x7c2e

    aput v18, v1, v17

    const/16 v17, 0x607c

    aput v17, v1, v16

    const/16 v16, -0x40a0

    aput v16, v1, v15

    const/16 v15, -0x41

    aput v15, v1, v14

    const/16 v14, 0x76

    aput v14, v1, v13

    const/16 v13, 0x4b00

    aput v13, v1, v12

    const/16 v12, 0x244b

    aput v12, v1, v11

    const/16 v11, -0x5ddc

    aput v11, v1, v10

    const/16 v10, -0x5e

    aput v10, v1, v9

    const/16 v9, -0x32

    aput v9, v1, v8

    const/16 v8, -0x3d96

    aput v8, v1, v7

    const/16 v7, -0x3e

    aput v7, v1, v6

    const/16 v6, -0x61

    aput v6, v1, v5

    const/16 v5, 0x1f05

    aput v5, v1, v4

    const/16 v4, 0x5f1f

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, -0x64

    aput v23, v2, v22

    const/16 v22, 0x6920

    aput v22, v2, v21

    const/16 v21, 0x601d

    aput v21, v2, v20

    const/16 v20, 0x7403

    aput v20, v2, v19

    const/16 v19, 0x2311

    aput v19, v2, v18

    const/16 v18, -0x57b3

    aput v18, v2, v17

    const/16 v17, -0x3a

    aput v17, v2, v16

    const/16 v16, -0x64

    aput v16, v2, v15

    const/16 v15, -0x4b

    aput v15, v2, v14

    const/16 v14, -0xdb9

    aput v14, v2, v13

    const/16 v13, -0x65

    aput v13, v2, v12

    const/16 v12, -0x59

    aput v12, v2, v11

    const/16 v11, -0x72b0

    aput v11, v2, v10

    const/16 v10, -0x18

    aput v10, v2, v9

    const/16 v9, -0x34

    aput v9, v2, v8

    const/16 v8, -0x3d

    aput v8, v2, v7

    const/16 v7, 0x3d47

    aput v7, v2, v6

    const/16 v6, -0x27b1

    aput v6, v2, v5

    const/16 v5, -0x43

    aput v5, v2, v3

    const/16 v3, -0x60

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x8

    aput v24, v1, v23

    const/16 v23, 0x6945

    aput v23, v1, v22

    const/16 v22, 0x6069

    aput v22, v1, v21

    const/16 v21, 0x7460

    aput v21, v1, v20

    const/16 v20, 0x2374

    aput v20, v1, v19

    const/16 v19, -0x57dd

    aput v19, v1, v18

    const/16 v18, -0x58

    aput v18, v1, v17

    const/16 v17, -0xd

    aput v17, v1, v16

    const/16 v16, -0x2a

    aput v16, v1, v15

    const/16 v15, -0xdcc

    aput v15, v1, v14

    const/16 v14, -0xe

    aput v14, v1, v13

    const/16 v13, -0x3d

    aput v13, v1, v12

    const/16 v12, -0x7290

    aput v12, v1, v11

    const/16 v11, -0x73

    aput v11, v1, v10

    const/16 v10, -0x51

    aput v10, v1, v9

    const/16 v9, -0x56

    aput v9, v1, v8

    const/16 v8, 0x3d31

    aput v8, v1, v7

    const/16 v7, -0x27c3

    aput v7, v1, v6

    const/16 v6, -0x28

    aput v6, v1, v5

    const/16 v5, -0xd

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->mHealthSensor:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->access$002(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-void

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5
.end method
