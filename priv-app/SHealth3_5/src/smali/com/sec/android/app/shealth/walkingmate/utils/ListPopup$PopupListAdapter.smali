.class Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ListPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PopupListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;Landroid/content/Context;[Ljava/lang/String;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "items"    # [Ljava/lang/String;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    .line 224
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getLayoutId()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getTextViewId()I

    move-result v1

    invoke-direct {p0, p2, v0, v1, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 225
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    .line 230
    invoke-super/range {p0 .. p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 231
    .local v12, "view":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getTextViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 232
    .local v10, "text":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getBelowTextViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 233
    .local v1, "belowTextView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$400(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 234
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->applyPaddinsToTextView(Landroid/widget/TextView;)V
    invoke-static {v13, v10}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$500(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;Landroid/widget/TextView;)V

    .line 235
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->applyPaddinsToTextView(Landroid/widget/TextView;)V
    invoke-static {v13, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$500(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;Landroid/widget/TextView;)V

    .line 237
    :cond_0
    if-nez p1, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mIsAdditionalTextVisible:Z
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$600(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Z

    move-result v13

    if-nez v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTitleText:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$700(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_9

    .line 238
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getRecetTextViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 239
    .local v11, "titleTextView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextPaddings:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$400(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$TextPaddings;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 240
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->applyPaddinsToTextView(Landroid/widget/TextView;)V
    invoke-static {v13, v11}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$500(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;Landroid/widget/TextView;)V

    .line 242
    :cond_2
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 243
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTitleText:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$700(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_3

    .line 244
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTitleText:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$700(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    .end local v11    # "titleTextView":Landroid/widget/TextView;
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getClickableViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 251
    .local v2, "clickableView":Landroid/view/View;
    if-eqz v2, :cond_4

    .line 252
    if-nez p1, :cond_a

    .line 253
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getTopItemDrawableId()I

    move-result v13

    invoke-virtual {v2, v13}, Landroid/view/View;->setBackgroundResource(I)V

    .line 259
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mListeners:[Landroid/view/View$OnClickListener;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$800(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)[Landroid/view/View$OnClickListener;

    move-result-object v13

    aget-object v13, v13, p1

    invoke-virtual {v2, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 261
    .local v3, "clickableViewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getListPopupItemHeight()I

    move-result v13

    iput v13, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 262
    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 264
    .end local v3    # "clickableViewLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mIsAdditionalTextVisible:Z
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$600(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 265
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAdditionalItemsPosition:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$900(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    move-result-object v13

    sget-object v14, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;->BELOW:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$AdditionalItemsPosition;

    if-ne v13, v14, :cond_c

    .line 266
    const/4 v13, 0x0

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 267
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAdditionalItems:[Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1000(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, p1

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mHasContentDescription:Z
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1100(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 275
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mDescriptions:[Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1200(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, p1

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 277
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mSelectedItemIndex:I
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1300(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)I

    move-result v13

    move/from16 v0, p1

    if-ne v0, v13, :cond_d

    const/4 v4, 0x1

    .line 278
    .local v4, "isCurrentItemSelected":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    iget-object v13, v13, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getSelectedItemColorId()I

    move-result v13

    :goto_4
    invoke-virtual {v14, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 279
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextSelection:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1400(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextSelection:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1400(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "\\s+"

    invoke-virtual {v13, v14}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 280
    invoke-virtual {v10}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 281
    .local v6, "s":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextSelection:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1400(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 282
    .local v8, "selectionStart":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mTextSelection:Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1400(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int v7, v8, v13

    .line 283
    .local v7, "selectionEnd":I
    if-ltz v8, :cond_7

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v13

    if-gt v7, v13, :cond_7

    .line 284
    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 285
    .local v9, "spannable":Landroid/text/SpannableStringBuilder;
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    iget-object v14, v14, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f07027a

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-direct {v13, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v14, 0x21

    invoke-virtual {v9, v13, v8, v7, v14}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 287
    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    .end local v6    # "s":Ljava/lang/String;
    .end local v7    # "selectionEnd":I
    .end local v8    # "selectionStart":I
    .end local v9    # "spannable":Landroid/text/SpannableStringBuilder;
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    iget-object v13, v13, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getSelectedItemColorId()I

    move-result v13

    :goto_5
    invoke-virtual {v14, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setTextColor(I)V

    .line 291
    if-eqz v4, :cond_8

    .line 292
    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    .line 293
    :cond_8
    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/view/View;->setFocusable(Z)V

    .line 294
    return-object v12

    .line 247
    .end local v2    # "clickableView":Landroid/view/View;
    .end local v4    # "isCurrentItemSelected":Z
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getRecetTextViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 248
    const/16 v13, 0x8

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 254
    .restart local v2    # "clickableView":Landroid/view/View;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->getCount()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    move/from16 v0, p1

    if-ne v0, v13, :cond_b

    .line 255
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getBottomItemDrawableId()I

    move-result v13

    invoke-virtual {v2, v13}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 257
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getMiddleItemDrawableId()I

    move-result v13

    invoke-virtual {v2, v13}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 269
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getAdditionalUnitViewId()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 270
    .local v5, "rightText":Landroid/widget/TextView;
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->mAdditionalItems:[Ljava/lang/String;
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->access$1000(Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;)[Ljava/lang/String;

    move-result-object v13

    aget-object v13, v13, p1

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 277
    .end local v5    # "rightText":Landroid/widget/TextView;
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 278
    .restart local v4    # "isCurrentItemSelected":Z
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getDefaultItemColorId()I

    move-result v13

    goto/16 :goto_4

    .line 290
    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup$PopupListAdapter;->this$0:Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/walkingmate/utils/ListPopup;->getGreyItemColorId()I

    move-result v13

    goto/16 :goto_5
.end method
