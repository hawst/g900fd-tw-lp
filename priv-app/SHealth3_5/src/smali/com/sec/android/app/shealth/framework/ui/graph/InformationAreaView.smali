.class public abstract Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;
.super Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;->initInformationAreaView()Landroid/view/View;

    return-void
.end method


# virtual methods
.method public abstract dimInformationAreaView()V
.end method

.method protected abstract initInformationAreaView()Landroid/view/View;
.end method

.method public abstract refreshInformationAreaView()V
.end method

.method public abstract update(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation
.end method
