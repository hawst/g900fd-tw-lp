.class public Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;
.super Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.source "FoodGraphInformationAreaView.java"


# static fields
.field private static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final FOOD_SERIES_ID:I = 0x0

.field private static final SUM_FOOD_SERIES_ID:I = 0x1


# instance fields
.field private mAvgTextViewForMonth:Landroid/widget/TextView;

.field private mDateView:Landroid/widget/TextView;

.field private mFirstContainer:Landroid/widget/RelativeLayout;

.field private mGraphInfo:Landroid/widget/RelativeLayout;

.field private mMaxCaloriesInDayMealTypeView:Landroid/widget/TextView;

.field private mMaxCaloriesInDayView:Landroid/widget/TextView;

.field private mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

.field private mPreviousFoodChartData:Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;

.field private mSecondContainer:Landroid/widget/RelativeLayout;

.field private mSumCalorieSubtitleView:Landroid/widget/TextView;

.field private mSumCalorieUnitView:Landroid/widget/TextView;

.field private mSumCaloriesView:Landroid/widget/TextView;

.field private mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mView:Landroid/view/View;

.field private pieChartColors:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;-><init>(Landroid/content/Context;)V

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07020c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07020d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07020e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07020f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->pieChartColors:[I

    .line 71
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 72
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->initViews(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 73
    return-void
.end method

.method private centerViews(Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;)V
    .locals 3
    .param p1, "foodSchartHandlerData"    # Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPreviousFoodChartData:Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPreviousFoodChartData:Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    :goto_0
    return-void

    .line 107
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPreviousFoodChartData:Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mFirstContainer:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mMaxCaloriesInDayView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mMaxCaloriesInDayMealTypeView:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->centerViewsInContainer(Landroid/widget/RelativeLayout;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSecondContainer:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCaloriesView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieSubtitleView:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->centerViewsInContainer(Landroid/widget/RelativeLayout;Landroid/widget/TextView;Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method private centerViewsInContainer(Landroid/widget/RelativeLayout;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 6
    .param p1, "container"    # Landroid/widget/RelativeLayout;
    .param p2, "amount"    # Landroid/widget/TextView;
    .param p3, "title"    # Landroid/widget/TextView;

    .prologue
    const/4 v5, 0x0

    .line 113
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    aput-object p3, v3, v5

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->removePaddings([Landroid/view/View;)V

    .line 114
    invoke-virtual {p1, v5, v5}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 115
    invoke-virtual {p2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 116
    .local v0, "amountViewWidth":I
    invoke-virtual {p3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    .line 117
    .local v2, "titleViewWidth":I
    sub-int v3, v0, v2

    div-int/lit8 v1, v3, 0x2

    .line 118
    .local v1, "padding":I
    sub-int v3, v0, v2

    if-gez v3, :cond_0

    .line 119
    neg-int v3, v1

    invoke-virtual {p2, v3, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-virtual {p3, v1, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method private initViews(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 4
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const v3, 0x7f0803b9

    .line 76
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_0

    const v0, 0x7f0300cf

    .line 79
    .local v0, "layoutResId":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->layoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v1, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803b5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mDateView:Landroid/widget/TextView;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803b4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mGraphInfo:Landroid/widget/RelativeLayout;

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803b6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCaloriesView:Landroid/widget/TextView;

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803b7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0804c8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->pieChartColors:[I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setColors([I)V

    .line 86
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803be

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieSubtitleView:Landroid/widget/TextView;

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803bc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mMaxCaloriesInDayView:Landroid/widget/TextView;

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803bb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mMaxCaloriesInDayMealTypeView:Landroid/widget/TextView;

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803ba

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mFirstContainer:Landroid/widget/RelativeLayout;

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803bd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSecondContainer:Landroid/widget/RelativeLayout;

    .line 101
    :goto_1
    return-void

    .line 76
    .end local v0    # "layoutResId":I
    :cond_0
    const v0, 0x7f0300ce

    goto/16 :goto_0

    .line 94
    .restart local v0    # "layoutResId":I
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_2

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    const v2, 0x7f0803b8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieUnitView:Landroid/widget/TextView;

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mAvgTextViewForMonth:Landroid/widget/TextView;

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieUnitView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieUnitView:Landroid/widget/TextView;

    goto :goto_1
.end method

.method private varargs removePaddings([Landroid/view/View;)V
    .locals 5
    .param p1, "views"    # [Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 126
    move-object v0, p1

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 127
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 129
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 0

    .prologue
    .line 222
    return-void
.end method

.method public getMonth(I)Ljava/lang/String;
    .locals 1
    .param p1, "month"    # I

    .prologue
    .line 225
    new-instance v0, Ljava/text/DateFormatSymbols;

    invoke-direct {v0}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 0

    .prologue
    .line 211
    return-object p0
.end method

.method public recyclePieChart()V
    .locals 4

    .prologue
    .line 230
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    if-eqz v3, :cond_0

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 233
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v3, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_0

    move-object v1, v2

    .line 234
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 235
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 236
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 239
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method public refreshInformationAreaView()V
    .locals 0

    .prologue
    .line 217
    return-void
.end method

.method public update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 25
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v21

    if-nez v21, :cond_0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesId()I

    move-result v21

    if-eqz v21, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    new-instance v9, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    move-object/from16 v0, v21

    invoke-direct {v9, v0}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;-><init>(Lcom/samsung/android/sdk/chart/view/SchartHandlerData;)V

    .line 141
    .local v9, "foodChartData":Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v7

    .line 142
    .local v7, "dateValue":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v17

    .line 143
    .local v17, "timeValue":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->setSelectedDateInChart(J)V

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v21, v0

    sget-object v22, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_2

    .line 145
    move-object/from16 v0, p2

    move-wide/from16 v1, v17

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;->getLatestMeasureTimeInHour(J)J

    move-result-wide v17

    .line 147
    :cond_2
    new-instance v6, Ljava/util/Date;

    move-wide/from16 v0, v17

    invoke-direct {v6, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 148
    .local v6, "date":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    move-object/from16 v21, v0

    new-instance v22, Ljava/text/DecimalFormat;

    const-string v23, "#"

    invoke-direct/range {v22 .. v23}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v22}, Ljava/text/DateFormat;->setNumberFormat(Ljava/text/NumberFormat;)V

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mDateView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .line 151
    .local v4, "data":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    invoke-virtual {v4}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v20

    .line 152
    .local v20, "vectorData":Ljava/util/Vector;
    invoke-static/range {v20 .. v20}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->setValues(Ljava/util/Vector;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->refreshValuesToColorPairs()V

    .line 155
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->getCaloriesSum()D

    move-result-wide v14

    .line 156
    .local v14, "sum":D
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->isNotLessThanZero(D)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v21, v0

    sget-object v22, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_7

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v21, v0

    sget-object v22, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mAvgTextViewForMonth:Landroid/widget/TextView;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090f20

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .line 161
    .local v5, "dataSum":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    if-eqz v5, :cond_3

    .line 162
    invoke-virtual {v5}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Double;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    .line 165
    .end local v5    # "dataSum":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCaloriesView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;->convertCaloriesToString(D)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieUnitView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const v22, 0x7f0900b9

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(I)V

    .line 168
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->measure(II)V

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCaloriesView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v16

    .line 170
    .local v16, "sumViewWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mDateView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v19

    .line 171
    .local v19, "titleViewWidth":I
    sub-int v21, v19, v16

    div-int/lit8 v8, v21, 0x2

    .line 172
    .local v8, "deltaTitleOfSum":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0a0848

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 174
    .local v11, "marginRight":I
    if-le v8, v11, :cond_4

    .line 175
    move v11, v8

    .line 177
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v21

    move-object/from16 v0, v21

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mPieChart:Lcom/sec/android/app/shealth/logutils/graph/PieChart;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/shealth/logutils/graph/PieChart;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v21

    check-cast v21, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v21

    iput v11, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 181
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v21, v0

    sget-object v22, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 182
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 183
    .local v3, "cal":Ljava/util/Calendar;
    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mGraphInfo:Landroid/widget/RelativeLayout;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v23, 0x2

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->getMonth(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCaloriesView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieUnitView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 186
    .end local v3    # "cal":Ljava/util/Calendar;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mGraphInfo:Landroid/widget/RelativeLayout;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mDateView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCaloriesView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieUnitView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 188
    .end local v8    # "deltaTitleOfSum":I
    .end local v11    # "marginRight":I
    .end local v16    # "sumViewWidth":I
    .end local v19    # "titleViewWidth":I
    :cond_7
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    .line 189
    .restart local v5    # "dataSum":Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
    if-eqz v5, :cond_8

    .line 190
    invoke-virtual {v5}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Double;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    .line 192
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCaloriesView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;->convertCaloriesToString(D)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieSubtitleView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    const v22, 0x7f090939

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(I)V

    .line 194
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->getmMealsCount()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_9

    const/4 v10, 0x1

    .line 195
    .local v10, "isMealsMoreThanOne":Z
    :goto_1
    if-eqz v10, :cond_a

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->getCaloriesSum()D

    move-result-wide v12

    .line 197
    .local v12, "maxCaloriesOrSumCalories":D
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mMaxCaloriesInDayView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;->convertCaloriesToString(D)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mMaxCaloriesInDayMealTypeView:Landroid/widget/TextView;

    move-object/from16 v22, v0

    if-eqz v10, :cond_b

    const-string v21, ""

    :goto_3
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->centerViews(Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;)V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mGraphInfo:Landroid/widget/RelativeLayout;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mDateView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mMaxCaloriesInDayMealTypeView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCaloriesView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mSumCalorieSubtitleView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->mMaxCaloriesInDayView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 194
    .end local v10    # "isMealsMoreThanOne":Z
    .end local v12    # "maxCaloriesOrSumCalories":D
    :cond_9
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 195
    .restart local v10    # "isMealsMoreThanOne":Z
    :cond_a
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->getMaxCaloriesValue()D

    move-result-wide v12

    goto/16 :goto_2

    .line 198
    .restart local v12    # "maxCaloriesOrSumCalories":D
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/graph/FoodSchartHandlerData;->getMealTypeMaxValue()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v23

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    move/from16 v23, v0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_3
.end method
