.class Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;
.super Ljava/lang/Object;
.source "FoodListExpandableAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->initExpandableIndicatorListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Landroid/widget/ExpandableListView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

.field final synthetic val$groupPosition:I

.field final synthetic val$parent:Landroid/widget/ExpandableListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;Landroid/widget/ExpandableListView;I)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;->val$parent:Landroid/widget/ExpandableListView;

    iput p3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;->val$groupPosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "expandableIndicator"    # Landroid/view/View;

    .prologue
    .line 347
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    .line 348
    .local v0, "isExpanded":Z
    if-eqz v0, :cond_0

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;->val$parent:Landroid/widget/ExpandableListView;

    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;->val$groupPosition:I

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 353
    :goto_0
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    .line 354
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->notifyDataSetChanged()V

    .line 355
    return-void

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;->val$parent:Landroid/widget/ExpandableListView;

    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$1;->val$groupPosition:I

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    goto :goto_0

    .line 353
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
