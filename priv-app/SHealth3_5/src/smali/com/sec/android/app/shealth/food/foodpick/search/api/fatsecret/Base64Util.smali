.class final Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;
.super Ljava/lang/Object;
.source "Base64Util.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    }
.end annotation


# static fields
.field private static final ALPHABET:[B

.field private static final DECODABET:[B

.field public static final DONT_BREAK_LINES:I = 0x8

.field public static final ENCODE:I = 0x1

.field private static final EQUALS_SIGN:B = 0x3dt

.field public static final GZIP:I = 0x2

.field private static final MAX_LINE_LENGTH:I = 0x4c

.field private static final NATIVE_ALPHABET:[B

.field private static final NEW_LINE:B = 0xat

.field public static final NO_OPTIONS:I = 0x0

.field private static final PREFERRED_ENCODING:Ljava/lang/String; = "UTF-8"

.field private static final TAG:Ljava/lang/String;

.field private static final WHITE_SPACE_ENC:B = -0x5t


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    const-class v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->TAG:Ljava/lang/String;

    .line 42
    const/16 v2, 0x40

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    sput-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->NATIVE_ALPHABET:[B

    .line 60
    :try_start_0
    const-string v2, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 65
    .local v0, "alphabetArray":[B
    :goto_0
    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    .line 68
    const/16 v2, 0x7f

    new-array v2, v2, [B

    fill-array-data v2, :array_1

    sput-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    return-void

    .line 62
    .end local v0    # "alphabetArray":[B
    :catch_0
    move-exception v1

    .line 63
    .local v1, "use":Ljava/io/UnsupportedEncodingException;
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->NATIVE_ALPHABET:[B

    .restart local v0    # "alphabetArray":[B
    goto :goto_0

    .line 42
    nop

    :array_0
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
        0x67t
        0x68t
        0x69t
        0x6at
        0x6bt
        0x6ct
        0x6dt
        0x6et
        0x6ft
        0x70t
        0x71t
        0x72t
        0x73t
        0x74t
        0x75t
        0x76t
        0x77t
        0x78t
        0x79t
        0x7at
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x2bt
        0x2ft
    .end array-data

    .line 68
    :array_1
    .array-data 1
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x5t
        -0x5t
        -0x9t
        -0x9t
        -0x5t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x5t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        0x3et
        -0x9t
        -0x9t
        -0x9t
        0x3ft
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x3at
        0x3bt
        0x3ct
        0x3dt
        -0x9t
        -0x9t
        -0x9t
        -0x1t
        -0x9t
        -0x9t
        -0x9t
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        0xat
        0xbt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1et
        0x1ft
        0x20t
        0x21t
        0x22t
        0x23t
        0x24t
        0x25t
        0x26t
        0x27t
        0x28t
        0x29t
        0x2at
        0x2bt
        0x2ct
        0x2dt
        0x2et
        0x2ft
        0x30t
        0x31t
        0x32t
        0x33t
        -0x9t
        -0x9t
        -0x9t
        -0x9t
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    return-void
.end method

.method static synthetic access$000([B[BI)[B
    .locals 1
    .param p0, "x0"    # [B
    .param p1, "x1"    # [B
    .param p2, "x2"    # I

    .prologue
    .line 25
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encode3to4([B[BI)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()[B
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    return-object v0
.end method

.method static synthetic access$200([BI[BI)I
    .locals 1
    .param p0, "x0"    # [B
    .param p1, "x1"    # I
    .param p2, "x2"    # [B
    .param p3, "x3"    # I

    .prologue
    .line 25
    invoke-static {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->decode4to3([BI[BI)I

    move-result v0

    return v0
.end method

.method private static decode4to3([BI[BI)I
    .locals 8
    .param p0, "source"    # [B
    .param p1, "srcOffset"    # I
    .param p2, "destination"    # [B
    .param p3, "destOffset"    # I

    .prologue
    const/16 v5, 0x3d

    .line 229
    add-int/lit8 v4, p1, 0x2

    aget-byte v4, p0, v4

    if-ne v4, v5, :cond_0

    .line 230
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    aget-byte v5, p0, p1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x12

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    add-int/lit8 v6, p1, 0x1

    aget-byte v6, p0, v6

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0xc

    or-int v3, v4, v5

    .line 232
    .local v3, "outBuff":I
    ushr-int/lit8 v4, v3, 0x10

    int-to-byte v4, v4

    aput-byte v4, p2, p3

    .line 233
    const/4 v4, 0x1

    .line 261
    .end local v3    # "outBuff":I
    :goto_0
    return v4

    .line 234
    :cond_0
    add-int/lit8 v4, p1, 0x3

    aget-byte v4, p0, v4

    if-ne v4, v5, :cond_1

    .line 235
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    aget-byte v5, p0, p1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x12

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    add-int/lit8 v6, p1, 0x1

    aget-byte v6, p0, v6

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0xc

    or-int/2addr v4, v5

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    add-int/lit8 v6, p1, 0x2

    aget-byte v6, p0, v6

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x6

    or-int v3, v4, v5

    .line 238
    .restart local v3    # "outBuff":I
    ushr-int/lit8 v4, v3, 0x10

    int-to-byte v4, v4

    aput-byte v4, p2, p3

    .line 239
    add-int/lit8 v4, p3, 0x1

    ushr-int/lit8 v5, v3, 0x8

    int-to-byte v5, v5

    aput-byte v5, p2, v4

    .line 240
    const/4 v4, 0x2

    goto :goto_0

    .line 243
    .end local v3    # "outBuff":I
    :cond_1
    :try_start_0
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    aget-byte v5, p0, p1

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x12

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    add-int/lit8 v6, p1, 0x1

    aget-byte v6, p0, v6

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0xc

    or-int/2addr v4, v5

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    add-int/lit8 v6, p1, 0x2

    aget-byte v6, p0, v6

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x6

    or-int/2addr v4, v5

    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    add-int/lit8 v6, p1, 0x3

    aget-byte v6, p0, v6

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    or-int v3, v4, v5

    .line 247
    .restart local v3    # "outBuff":I
    shr-int/lit8 v4, v3, 0x10

    int-to-byte v4, v4

    aput-byte v4, p2, p3

    .line 248
    add-int/lit8 v4, p3, 0x1

    shr-int/lit8 v5, v3, 0x8

    int-to-byte v5, v5

    aput-byte v5, p2, v4

    .line 249
    add-int/lit8 v4, p3, 0x2

    int-to-byte v5, v3

    aput-byte v5, p2, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    const/4 v4, 0x3

    goto :goto_0

    .line 251
    .end local v3    # "outBuff":I
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x4

    .line 253
    .local v2, "numOfShifts":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v4, 0x4

    if-ge v1, v4, :cond_2

    .line 255
    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int v6, p1, v1

    aget-byte v6, p0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->DECODABET:[B

    add-int v7, p1, v1

    aget-byte v7, p0, v7

    aget-byte v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 261
    :cond_2
    const/4 v4, -0x1

    goto/16 :goto_0
.end method

.method private static encode3to4([BII[BI)[B
    .locals 5
    .param p0, "source"    # [B
    .param p1, "srcOffset"    # I
    .param p2, "numSigBytes"    # I
    .param p3, "destination"    # [B
    .param p4, "destOffset"    # I

    .prologue
    const/16 v4, 0x3d

    const/4 v1, 0x0

    .line 197
    if-lez p2, :cond_1

    aget-byte v2, p0, p1

    shl-int/lit8 v2, v2, 0x18

    ushr-int/lit8 v2, v2, 0x8

    move v3, v2

    :goto_0
    const/4 v2, 0x1

    if-le p2, v2, :cond_2

    add-int/lit8 v2, p1, 0x1

    aget-byte v2, p0, v2

    shl-int/lit8 v2, v2, 0x18

    ushr-int/lit8 v2, v2, 0x10

    :goto_1
    or-int/2addr v2, v3

    const/4 v3, 0x2

    if-le p2, v3, :cond_0

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x18

    ushr-int/lit8 v1, v1, 0x18

    :cond_0
    or-int v0, v2, v1

    .line 203
    .local v0, "inBuff":I
    packed-switch p2, :pswitch_data_0

    .line 223
    :goto_2
    return-object p3

    .end local v0    # "inBuff":I
    :cond_1
    move v3, v1

    .line 197
    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    .line 205
    .restart local v0    # "inBuff":I
    :pswitch_0
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    ushr-int/lit8 v2, v0, 0x12

    aget-byte v1, v1, v2

    aput-byte v1, p3, p4

    .line 206
    add-int/lit8 v1, p4, 0x1

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 207
    add-int/lit8 v1, p4, 0x2

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 208
    add-int/lit8 v1, p4, 0x3

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    and-int/lit8 v3, v0, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    goto :goto_2

    .line 211
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    ushr-int/lit8 v2, v0, 0x12

    aget-byte v1, v1, v2

    aput-byte v1, p3, p4

    .line 212
    add-int/lit8 v1, p4, 0x1

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 213
    add-int/lit8 v1, p4, 0x2

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0x6

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 214
    add-int/lit8 v1, p4, 0x3

    aput-byte v4, p3, v1

    goto :goto_2

    .line 217
    :pswitch_2
    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    ushr-int/lit8 v2, v0, 0x12

    aget-byte v1, v1, v2

    aput-byte v1, p3, p4

    .line 218
    add-int/lit8 v1, p4, 0x1

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->ALPHABET:[B

    ushr-int/lit8 v3, v0, 0xc

    and-int/lit8 v3, v3, 0x3f

    aget-byte v2, v2, v3

    aput-byte v2, p3, v1

    .line 219
    add-int/lit8 v1, p4, 0x2

    aput-byte v4, p3, v1

    .line 220
    add-int/lit8 v1, p4, 0x3

    aput-byte v4, p3, v1

    goto :goto_2

    .line 203
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static encode3to4([B[BI)[B
    .locals 1
    .param p0, "b4"    # [B
    .param p1, "threeBytes"    # [B
    .param p2, "numSigBytes"    # I

    .prologue
    const/4 v0, 0x0

    .line 191
    invoke-static {p1, v0, p2, p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encode3to4([BII[BI)[B

    .line 192
    return-object p0
.end method

.method public static encodeBytes([B)Ljava/lang/String;
    .locals 2
    .param p0, "source"    # [B

    .prologue
    const/4 v1, 0x0

    .line 104
    array-length v0, p0

    invoke-static {p0, v1, v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encodeBytes([BIII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static encodeBytes([BIII)Ljava/lang/String;
    .locals 12
    .param p0, "source"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I
    .param p3, "options"    # I

    .prologue
    const/4 v8, 0x0

    .line 119
    and-int/lit8 v2, p3, 0x8

    .line 120
    .local v2, "dontBreakLines":I
    and-int/lit8 v4, p3, 0x2

    .line 121
    .local v4, "gzip":I
    const/4 v9, 0x2

    if-ne v4, v9, :cond_1

    .line 122
    invoke-static {p0, p1, p2, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encodeBytesForGzip([BIII)Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    .line 123
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    if-nez v0, :cond_0

    const/4 v8, 0x0

    .line 138
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :goto_0
    return-object v8

    .line 125
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    :cond_0
    :try_start_0
    new-instance v8, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    const-string v10, "UTF-8"

    invoke-direct {v8, v9, v10}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v7

    .line 127
    .local v7, "uue":Ljava/io/UnsupportedEncodingException;
    new-instance v8, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>([B)V

    goto :goto_0

    .line 130
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v7    # "uue":Ljava/io/UnsupportedEncodingException;
    :cond_1
    if-nez v2, :cond_2

    const/4 v1, 0x1

    .line 131
    .local v1, "breakLines":Z
    :goto_1
    mul-int/lit8 v9, p2, 0x4

    div-int/lit8 v5, v9, 0x3

    .line 132
    .local v5, "len43":I
    rem-int/lit8 v9, p2, 0x3

    if-lez v9, :cond_3

    const/4 v9, 0x4

    :goto_2
    add-int v10, v5, v9

    if-eqz v1, :cond_4

    div-int/lit8 v9, v5, 0x4c

    :goto_3
    add-int/2addr v9, v10

    new-array v6, v9, [B

    .line 134
    .local v6, "outBuff":[B
    invoke-static {p0, p1, p2, v1, v6}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encodeOutBuffForNonGzip([BIIZ[B)I

    move-result v3

    .line 136
    .local v3, "e":I
    :try_start_1
    new-instance v9, Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "UTF-8"

    invoke-direct {v9, v6, v10, v3, v11}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v8, v9

    goto :goto_0

    .end local v1    # "breakLines":Z
    .end local v3    # "e":I
    .end local v5    # "len43":I
    .end local v6    # "outBuff":[B
    :cond_2
    move v1, v8

    .line 130
    goto :goto_1

    .restart local v1    # "breakLines":Z
    .restart local v5    # "len43":I
    :cond_3
    move v9, v8

    .line 132
    goto :goto_2

    :cond_4
    move v9, v8

    goto :goto_3

    .line 137
    .restart local v3    # "e":I
    .restart local v6    # "outBuff":[B
    :catch_1
    move-exception v7

    .line 138
    .restart local v7    # "uue":Ljava/io/UnsupportedEncodingException;
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v6, v8, v3}, Ljava/lang/String;-><init>([BII)V

    move-object v8, v9

    goto :goto_0
.end method

.method private static encodeBytesForGzip([BIII)Ljava/io/ByteArrayOutputStream;
    .locals 12
    .param p0, "source"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I
    .param p3, "dontBreakLines"    # I

    .prologue
    const/4 v8, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 145
    const/4 v2, 0x0

    .line 146
    .local v2, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v5, 0x0

    .line 147
    .local v5, "gzos":Ljava/util/zip/GZIPOutputStream;
    const/4 v0, 0x0

    .line 149
    .local v0, "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .local v3, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;

    or-int/lit8 v7, p3, 0x1

    invoke-direct {v1, v3, v7}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 152
    .end local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .local v1, "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    :try_start_2
    new-instance v6, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v6, v1}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 153
    .end local v5    # "gzos":Ljava/util/zip/GZIPOutputStream;
    .local v6, "gzos":Ljava/util/zip/GZIPOutputStream;
    :try_start_3
    invoke-virtual {v6, p0, p1, p2}, Ljava/util/zip/GZIPOutputStream;->write([BII)V

    .line 154
    invoke-virtual {v6}, Ljava/util/zip/GZIPOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 159
    new-array v7, v8, [Ljava/io/Closeable;

    aput-object v6, v7, v9

    aput-object v1, v7, v10

    aput-object v3, v7, v11

    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    move-object v0, v1

    .end local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    move-object v5, v6

    .end local v6    # "gzos":Ljava/util/zip/GZIPOutputStream;
    .restart local v5    # "gzos":Ljava/util/zip/GZIPOutputStream;
    move-object v2, v3

    .line 161
    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    :goto_0
    return-object v3

    .line 155
    :catch_0
    move-exception v4

    .line 156
    .local v4, "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    sget-object v7, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->TAG:Ljava/lang/String;

    invoke-static {v7, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 157
    const/4 v3, 0x0

    .line 159
    new-array v7, v8, [Ljava/io/Closeable;

    aput-object v5, v7, v9

    aput-object v0, v7, v10

    aput-object v2, v7, v11

    invoke-static {v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    goto :goto_0

    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_2
    new-array v8, v8, [Ljava/io/Closeable;

    aput-object v5, v8, v9

    aput-object v0, v8, v10

    aput-object v2, v8, v11

    invoke-static {v8}, Lcom/sec/android/app/shealth/common/utils/Utils;->closeStreams([Ljava/io/Closeable;)V

    throw v7

    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    goto :goto_2

    .end local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    :catchall_2
    move-exception v7

    move-object v0, v1

    .end local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    move-object v2, v3

    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    goto :goto_2

    .end local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "gzos":Ljava/util/zip/GZIPOutputStream;
    .restart local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "gzos":Ljava/util/zip/GZIPOutputStream;
    :catchall_3
    move-exception v7

    move-object v0, v1

    .end local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    move-object v5, v6

    .end local v6    # "gzos":Ljava/util/zip/GZIPOutputStream;
    .restart local v5    # "gzos":Ljava/util/zip/GZIPOutputStream;
    move-object v2, v3

    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    goto :goto_2

    .line 155
    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    goto :goto_1

    .end local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_2
    move-exception v4

    move-object v0, v1

    .end local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    move-object v2, v3

    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    goto :goto_1

    .end local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .end local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v5    # "gzos":Ljava/util/zip/GZIPOutputStream;
    .restart local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "gzos":Ljava/util/zip/GZIPOutputStream;
    :catch_3
    move-exception v4

    move-object v0, v1

    .end local v1    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    .restart local v0    # "b64os":Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util$OutputStream;
    move-object v5, v6

    .end local v6    # "gzos":Ljava/util/zip/GZIPOutputStream;
    .restart local v5    # "gzos":Ljava/util/zip/GZIPOutputStream;
    move-object v2, v3

    .end local v3    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "baos":Ljava/io/ByteArrayOutputStream;
    goto :goto_1
.end method

.method private static encodeOutBuffForNonGzip([BIIZ[B)I
    .locals 6
    .param p0, "source"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I
    .param p3, "breakLines"    # Z
    .param p4, "outBuff"    # [B

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "d":I
    const/4 v1, 0x0

    .line 171
    .local v1, "e":I
    add-int/lit8 v2, p2, -0x2

    .line 172
    .local v2, "len2":I
    const/4 v3, 0x0

    .line 173
    .local v3, "lineLength":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 174
    add-int v4, v0, p1

    const/4 v5, 0x3

    invoke-static {p0, v4, v5, p4, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encode3to4([BII[BI)[B

    .line 175
    add-int/lit8 v3, v3, 0x4

    .line 176
    if-eqz p3, :cond_0

    const/16 v4, 0x4c

    if-ne v3, v4, :cond_0

    .line 177
    add-int/lit8 v4, v1, 0x4

    const/16 v5, 0xa

    aput-byte v5, p4, v4

    .line 178
    add-int/lit8 v1, v1, 0x1

    .line 179
    const/4 v3, 0x0

    .line 173
    :cond_0
    add-int/lit8 v0, v0, 0x3

    add-int/lit8 v1, v1, 0x4

    goto :goto_0

    .line 182
    :cond_1
    if-ge v0, p2, :cond_2

    .line 183
    add-int v4, v0, p1

    sub-int v5, p2, v0

    invoke-static {p0, v4, v5, p4, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/Base64Util;->encode3to4([BII[BI)[B

    .line 184
    add-int/lit8 v1, v1, 0x4

    .line 186
    :cond_2
    return v1
.end method
