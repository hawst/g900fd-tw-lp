.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;
.super Ljava/lang/Object;
.source "CoachRestoreHelper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

.field final synthetic val$dialogTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->val$dialogTag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 4
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/4 v3, 0x0

    .line 410
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->val$dialogTag:Ljava/lang/String;

    const-string v2, "RESTORE_ERROR_POPUP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 411
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$6;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 472
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;->NONE:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->popupStatus:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$702(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$Step;

    .line 473
    return-void

    .line 414
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1600(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1600(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1600(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1602(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 420
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->netUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1700(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isNetworkConnected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mPopupHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1300(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 437
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mPopupHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1300(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 452
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->val$dialogTag:Ljava/lang/String;

    const-string v2, "RESTORE_SUCCESS_POPUP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$6;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 457
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1800(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1800(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 459
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1800(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 460
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mRestartAppDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1802(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 463
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.cignacoach.RESTORE_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 464
    .local v0, "mCoachResetIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 411
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 454
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method
