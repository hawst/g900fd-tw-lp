.class Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment$1;
.super Ljava/lang/Object;
.source "LifeStyleMultiScoreFragment.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnGraphData(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const/4 v5, 0x0

    .line 505
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->access$000(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 506
    const-string v2, "HandlerListener Handler Data"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HandlerListener "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v0

    .line 508
    .local v0, "dateValue":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    .line 509
    .local v1, "value":Ljava/lang/Double;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->access$000(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v1}, Ljava/lang/Double;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I
    invoke-static {v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->setCurrentScoreInfo(JII)V

    .line 511
    .end local v0    # "dateValue":Ljava/lang/String;
    .end local v1    # "value":Ljava/lang/Double;
    :cond_0
    return-void
.end method

.method public OnReleaseTimeOut()V
    .locals 0

    .prologue
    .line 501
    return-void
.end method

.method public OnVisible()V
    .locals 0

    .prologue
    .line 490
    return-void
.end method
