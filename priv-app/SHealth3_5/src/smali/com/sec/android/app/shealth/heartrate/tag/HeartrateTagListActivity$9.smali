.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;
.super Ljava/lang/Object;
.source "HeartrateTagListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->addSelectSpinner(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V
    .locals 0

    .prologue
    .line 829
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "selectedItemView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x1

    .line 834
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    const v2, 0x7f08054d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 835
    .local v0, "mCheckBox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_1

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090071

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 837
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 850
    :cond_0
    :goto_0
    return-void

    .line 840
    :cond_1
    if-eqz v0, :cond_0

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090073

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 842
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_2

    .line 843
    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 844
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 855
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$9;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mTagsToDelete:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;I)V

    .line 856
    return-void
.end method
