.class Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;
.super Landroid/os/AsyncTask;
.source "SuggestedGoalsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateListAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private coachMsg:Ljava/lang/String;

.field private mNewListData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;)V
    .locals 1

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->coachMsg:Ljava/lang/String;

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->mNewListData:Ljava/util/List;

    return-void
.end method

.method private getCoachMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->getGoalCoachMessage()Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "coachMsg":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 259
    const-string v0, ""

    .line 261
    .end local v0    # "coachMsg":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private getListData()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 247
    const/4 v0, 0x0

    .line 248
    .local v0, "listData":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;>;>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 249
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mGoalCategory:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createGoalListDataByCategory(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v0

    .line 253
    :goto_0
    return-object v0

    .line 251
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createGoalListData(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 191
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Integer;

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->getListData()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->mNewListData:Ljava/util/List;

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->mNewListData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->getCoachMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->coachMsg:Ljava/lang/String;

    .line 207
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 191
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 212
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 215
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 216
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UpdateListAsyncTask list update [START]"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCategoryListView;->setPreventScroll(Z)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->mNewListData:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->clearAllData()V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mListData:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->mNewListData:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mSuggestedListAdapter:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mListData:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedListAdapter;->setItemsList(Ljava/util/List;)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->mNewListData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->coachMsg:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->updateHeaderMessage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;Ljava/lang/String;)V

    .line 235
    :cond_1
    :goto_0
    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UpdateListAsyncTask list update [END]"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 244
    :cond_2
    return-void

    .line 229
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mCommentView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mNoSuggestTextView:Landroid/widget/TextView;

    const v1, 0x7f090331

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity$UpdateListAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;->mNoSuggestDescView:Landroid/widget/TextView;

    const v1, 0x7f090330

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
