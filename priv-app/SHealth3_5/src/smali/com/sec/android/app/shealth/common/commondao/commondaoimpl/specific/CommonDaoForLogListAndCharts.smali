.class public abstract Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;
.source "CommonDaoForLogListAndCharts.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;",
        "Contract::",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
        ">",
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb",
        "<TT;>;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations",
        "<TContract;>;"
    }
.end annotation


# static fields
.field protected static final PERCENT_100:F = 100.0f


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field protected final contractMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TContract;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "timeColumnNameForCheckRules"    # Ljava/lang/String;
    .param p4, "idColumnName"    # Ljava/lang/String;
    .param p5, "keyValueColumnName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<TContract;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts<TT;TContract;>;"
    .local p6, "localContract":Ljava/util/Map;, "Ljava/util/Map<TContract;Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoWithDateTimeImplDb;-><init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-class v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;->LOG_TAG:Ljava/lang/String;

    .line 58
    iput-object p6, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;->contractMap:Ljava/util/Map;

    .line 59
    return-void
.end method


# virtual methods
.method public final getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TContract;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts<TT;TContract;>;"
    .local p1, "item":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;, "TContract;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;->contractMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 73
    .local v0, "columnName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 74
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "is not declarated in DB realisation. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Please check contract map in your *DaoDbImpl.\nKnown contracts: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;->contractMap:Ljava/util/Map;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 77
    :cond_0
    return-object v0
.end method

.method public final getContractColumnName(Ljava/util/Set;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TContract;>;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts<TT;TContract;>;"
    .local p1, "values":Ljava/util/Set;, "Ljava/util/Set<TContract;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 64
    .local v2, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;

    .line 65
    .local v1, "item":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;, "TContract;"
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    .end local v1    # "item":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;, "TContract;"
    :cond_0
    return-object v2
.end method

.method public getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;Ljava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;
    .locals 9
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .param p7, "additionalWhereRule"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .param p8, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            "Ljava/util/Set",
            "<TContract;>;",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts<TT;TContract;>;"
    .local p6, "projection":Ljava/util/Set;, "Ljava/util/Set<TContract;>;"
    invoke-virtual {p0, p6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;->getContractColumnName(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;
    .locals 1
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "periodType"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .param p6, "projection"    # [Ljava/lang/String;
    .param p7, "additionalWhereRule"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .param p8, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;

    .prologue
    .line 94
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts<TT;TContract;>;"
    invoke-virtual/range {p0 .. p8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;->getPeriodCursor(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
