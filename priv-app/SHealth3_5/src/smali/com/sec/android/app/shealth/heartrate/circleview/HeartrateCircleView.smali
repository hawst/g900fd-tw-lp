.class public Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;
.super Landroid/view/View;
.source "HeartrateCircleView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$1;,
        Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;
    }
.end annotation


# instance fields
.field private angleCorrectionValue:I

.field color:[I

.field private endAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

.field private externalRadius:F

.field private gradient:Landroid/graphics/Shader;

.field greenColor:[I

.field private innerRadius:F

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field orangeColor:[I

.field pos:[F

.field private startAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x5

    .line 62
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->ZERO:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->startAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->ZERO:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->endAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    .line 42
    const/16 v0, 0xf5

    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->angleCorrectionValue:I

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPath:Landroid/graphics/Path;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPaint:Landroid/graphics/Paint;

    .line 47
    new-array v0, v4, [I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07018a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07018b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07018c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07018d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07018e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->greenColor:[I

    .line 53
    new-array v0, v4, [I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07018f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070190

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070191

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070192

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070193

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->orangeColor:[I

    .line 59
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->pos:[F

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->initAttributes()V

    .line 64
    return-void

    .line 59
    nop

    :array_0
    .array-data 4
        0x0
        0x3f19999a    # 0.6f
        0x3f333333    # 0.7f
        0x3f59999a    # 0.85f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private convertDptoPx(F)F
    .locals 4
    .param p1, "dp"    # F

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 161
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 162
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    iget v3, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v3

    .line 163
    .local v1, "px":F
    return v1
.end method

.method private drawArc(Landroid/graphics/Path;FLcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;Z)V
    .locals 4
    .param p1, "path"    # Landroid/graphics/Path;
    .param p2, "radius"    # F
    .param p3, "from"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;
    .param p4, "to"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;
    .param p5, "forceMoveTo"    # Z

    .prologue
    .line 100
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getRectForCircle(F)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p3, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->degreeValue:F

    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->angleCorrectionValue:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iget v2, p4, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->degreeValue:F

    iget v3, p3, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->degreeValue:F

    sub-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2, p5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    .line 104
    return-void
.end method

.method private getRectForCircle(F)Landroid/graphics/RectF;
    .locals 4
    .param p1, "radius"    # F

    .prologue
    .line 107
    iget v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->externalRadius:F

    sub-float v0, v2, p1

    .line 108
    .local v0, "marginLT":F
    const/high16 v2, 0x40000000    # 2.0f

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->externalRadius:F

    mul-float/2addr v2, v3

    sub-float v1, v2, v0

    .line 109
    .local v1, "marginRB":F
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v0, v0, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2
.end method

.method private initAttributes()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPath:Landroid/graphics/Path;

    .line 73
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPaint:Landroid/graphics/Paint;

    .line 74
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v8, 0x1

    const-wide v12, 0x4066800000000000L    # 180.0

    const-wide v10, 0x400921fb54442d18L    # Math.PI

    const/high16 v7, 0x40000000    # 2.0f

    .line 78
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->endAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    iget v0, v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->degreeValue:F

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->endAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    iget v3, v3, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->degreeValue:F

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->startAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    iget v4, v4, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;->degreeValue:F

    sub-float/2addr v3, v4

    div-float/2addr v3, v7

    sub-float v9, v0, v3

    .line 84
    .local v9, "angle":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v7

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->externalRadius:F

    float-to-double v3, v3

    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->angleCorrectionValue:I

    int-to-float v5, v5

    sub-float v5, v9, v5

    float-to-double v5, v5

    mul-double/2addr v5, v10

    div-double/2addr v5, v12

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    double-to-float v3, v3

    add-float v1, v0, v3

    .line 85
    .local v1, "x":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v7

    iget v3, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->externalRadius:F

    float-to-double v3, v3

    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->angleCorrectionValue:I

    int-to-float v5, v5

    sub-float v5, v9, v5

    float-to-double v5, v5

    mul-double/2addr v5, v10

    div-double/2addr v5, v12

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    double-to-float v3, v3

    add-float v2, v0, v3

    .line 86
    .local v2, "y":F
    const/4 v0, 0x0

    cmpl-float v0, v9, v0

    if-nez v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 89
    :cond_0
    new-instance v0, Landroid/graphics/RadialGradient;

    const/high16 v3, 0x432d0000    # 173.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->convertDptoPx(F)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->color:[I

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->pos:[F

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->gradient:Landroid/graphics/Shader;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPaint:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->gradient:Landroid/graphics/Shader;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 93
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPath:Landroid/graphics/Path;

    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->innerRadius:F

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->startAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->endAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->drawArc(Landroid/graphics/Path;FLcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;Z)V

    .line 94
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPath:Landroid/graphics/Path;

    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->externalRadius:F

    iget-object v6, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->endAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->startAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    const/4 v8, 0x0

    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->drawArc(Landroid/graphics/Path;FLcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;Z)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPath:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setColor(Z)V
    .locals 1
    .param p1, "isGreen"    # Z

    .prologue
    .line 137
    if-eqz p1, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->greenColor:[I

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->color:[I

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->orangeColor:[I

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->color:[I

    goto :goto_0
.end method

.method public setEndAngle(F)V
    .locals 1
    .param p1, "endAngle"    # F

    .prologue
    .line 117
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;->DEGREES:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->setEndAngle(FLcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;)V

    .line 118
    return-void
.end method

.method public setEndAngle(FLcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;)V
    .locals 1
    .param p1, "endAngle"    # F
    .param p2, "angleUnits"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;

    .prologue
    .line 125
    invoke-virtual {p2, p1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;->getAngle(F)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->endAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    .line 126
    return-void
.end method

.method public setExternalRadius(F)V
    .locals 0
    .param p1, "externalRadius"    # F

    .prologue
    .line 129
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->externalRadius:F

    .line 130
    return-void
.end method

.method public setInnerRadius(F)V
    .locals 0
    .param p1, "innerRadius"    # F

    .prologue
    .line 133
    iput p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->innerRadius:F

    .line 134
    return-void
.end method

.method public setStartAngle(F)V
    .locals 1
    .param p1, "startAngle"    # F

    .prologue
    .line 113
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;->DEGREES:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->setStartAngle(FLcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;)V

    .line 114
    return-void
.end method

.method public setStartAngle(FLcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;)V
    .locals 1
    .param p1, "startAngle"    # F
    .param p2, "angleUnits"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;

    .prologue
    .line 121
    invoke-virtual {p2, p1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView$AngleUnits;->getAngle(F)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->startAngle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateAngle;

    .line 122
    return-void
.end method
