.class final Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;
.super Ljava/util/HashMap;
.source "MealDaoImplDb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string/jumbo v1, "total_kilo_calorie"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string/jumbo v1, "sample_time"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->MAX_SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "MAX(sample_time)"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->_ID:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "_id"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->COMMENT:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "comment"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->TYPE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string/jumbo v1, "type"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->AVG_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "AVG(total_kilo_calorie)"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->SUM_TOTAL_KILO_CALORIE:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    const-string v1, "SUM(total_kilo_calorie)"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    return-void
.end method
