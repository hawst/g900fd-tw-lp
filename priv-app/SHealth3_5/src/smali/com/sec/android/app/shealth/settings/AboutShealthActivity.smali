.class public Lcom/sec/android/app/shealth/settings/AboutShealthActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "AboutShealthActivity.java"


# static fields
.field private static final INTRO_REQUEST_CODE:I = 0x3


# instance fields
.field private backupText:Landroid/widget/TextView;

.field private btn_notice:Landroid/widget/Button;

.field private btn_opensource:Landroid/widget/Button;

.field private isPreloadedService:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->isPreloadedService:Z

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->btn_notice:Landroid/widget/Button;

    return-void
.end method

.method private customizeViewsAccordingToModel()V
    .locals 4

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "language":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isRtlLanguage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->backupText:Landroid/widget/TextView;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->backupText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "\u200f"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0908c4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v1

    if-nez v1, :cond_1

    .line 146
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isJapanModel()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ja"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 149
    :cond_2
    return-void
.end method

.method private isDownloadableModel()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 153
    if-nez p2, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 154
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->setResult(I)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->finish()V

    .line 157
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x8

    const/4 v10, 0x0

    .line 45
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v5, 0x7f030001

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->setContentView(I)V

    .line 48
    const v5, 0x7f08001a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 49
    .local v1, "help":Landroid/widget/Button;
    const v5, 0x7f080018

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 50
    .local v2, "koreanPopupButton":Landroid/widget/Button;
    const v5, 0x7f08001b

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->btn_opensource:Landroid/widget/Button;

    .line 51
    const v5, 0x7f080016

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->backupText:Landroid/widget/TextView;

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ko"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->isDownloadableModel()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->isPreloadedService:Z

    if-eqz v5, :cond_0

    .line 56
    invoke-virtual {v1, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 57
    new-instance v5, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;

    invoke-direct {v5, p0, v1, v2}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/AboutShealthActivity;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ko"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->isPreloadedService:Z

    if-eqz v5, :cond_1

    .line 71
    invoke-virtual {v2, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 72
    new-instance v5, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;

    invoke-direct {v5, p0, v1, v2}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/AboutShealthActivity;Landroid/widget/Button;Landroid/widget/Button;)V

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->btn_opensource:Landroid/widget/Button;

    new-instance v6, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/AboutShealthActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v5, 0x7f080019

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->btn_notice:Landroid/widget/Button;

    .line 95
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->btn_notice:Landroid/widget/Button;

    invoke-virtual {v5, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 114
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->backupText:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->customizeViewsAccordingToModel()V

    .line 119
    const-string v4, ""

    .line 122
    .local v4, "version":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 123
    .local v3, "pInfo":Landroid/content/pm/PackageInfo;
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    .end local v3    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    const v5, 0x7f080017

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f0908c5

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/settings/AboutShealthActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v9, 0x7df

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    return-void

    .line 81
    .end local v4    # "version":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 124
    .restart local v4    # "version":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method
