.class public Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;
.super Ljava/lang/Object;
.source "ErrorCodeConstants.java"


# static fields
.field public static ACCESS_TOKEN_ERROR_CODE:I = 0x0

.field public static final BAD_REQUEST:I = 0xfa2

.field public static final DATABASE_ERROR_CODE:I = 0xfa6

.field public static final ERROR_CODE_NONE:I = 0x0

.field public static final INVALID_ACCESS_TOKEN:I = 0xfa1

.field public static final INVALID_HANDLER_ID:I = 0xfa9

.field public static final INVALID_HEADER_LENGTH:I = 0xfa4

.field public static final INVALID_HEADER_PARAMETER:I = 0xfa3

.field public static final INVALID_PARAMETER:I = 0xfa0

.field public static final INVALID_PARAMETER_FORMAT:I = 0xfa5

.field public static NET_EXCEPTION:I = 0x0

.field public static PARAMETER_ERROR_CODE:I = 0x0

.field public static final RES_CODE_BAD_REQUEST:I = 0x190

.field public static final RES_CODE_INTERNAL_SERVER_ERROR:I = 0x1f4

.field public static final RES_CODE_INVALID_TOKEN:I = 0x193

.field public static final RES_CODE_SUCCESS:I = 0xc8

.field public static final RES_CODE_UNAVAILABLE_TRANSACTION:I = 0x258

.field public static final RES_NULL_ACCESS_TOKEN:I = 0x191

.field public static final UNKNOWN_ERROR:I = 0xfa8

.field public static final USER_TAKEN_ERROR_CODE:I = 0xfa7


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/16 v0, 0x1389

    sput v0, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;->NET_EXCEPTION:I

    .line 20
    const/16 v0, 0x138a

    sput v0, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;->PARAMETER_ERROR_CODE:I

    .line 25
    const/16 v0, 0x138b

    sput v0, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;->ACCESS_TOKEN_ERROR_CODE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    return-void
.end method


# virtual methods
.method public getErrorCode(I)I
    .locals 1
    .param p1, "errorCode"    # I

    .prologue
    .line 107
    packed-switch p1, :pswitch_data_0

    .line 120
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 113
    :pswitch_1
    sget v0, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;->PARAMETER_ERROR_CODE:I

    goto :goto_0

    .line 115
    :pswitch_2
    sget v0, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;->ACCESS_TOKEN_ERROR_CODE:I

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0xfa0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getUserTokenErrorCode(I)I
    .locals 1
    .param p1, "errorCode"    # I

    .prologue
    .line 126
    packed-switch p1, :pswitch_data_0

    .line 137
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 131
    :pswitch_1
    sget v0, Lcom/sec/android/app/shealth/common/utils/constants/ErrorCodeConstants;->ACCESS_TOKEN_ERROR_CODE:I

    goto :goto_0

    .line 126
    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
