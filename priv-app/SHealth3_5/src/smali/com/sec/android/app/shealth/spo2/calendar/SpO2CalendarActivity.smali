.class public Lcom/sec/android/app/shealth/spo2/calendar/SpO2CalendarActivity;
.super Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;
.source "SpO2CalendarActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getDaysStatuses(JJ)Ljava/util/TreeMap;
    .locals 7
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 17
    .local v1, "calendarDayInfoMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;>;"
    invoke-static {p0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v5

    invoke-virtual {v5, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getCalendarDatas(JJ)Ljava/util/ArrayList;

    move-result-object v3

    .line 19
    .local v3, "heartrateDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/spo2/data/SpO2Data;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;

    .line 20
    .local v2, "each":Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 21
    .local v0, "calendarDayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/spo2/data/SpO2Data;->getCreateTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/spo2/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 23
    .end local v0    # "calendarDayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    .end local v2    # "each":Lcom/sec/android/app/shealth/spo2/data/SpO2Data;
    :cond_0
    return-object v1
.end method
