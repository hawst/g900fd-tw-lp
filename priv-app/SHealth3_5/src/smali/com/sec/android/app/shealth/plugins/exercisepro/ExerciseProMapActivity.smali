.class public Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$BTreceiver;
    }
.end annotation


# static fields
.field private static final POLYLINE_DEFAULT_COLOR:I

.field private static POLYLINE_DEFAULT_WIDTH:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final TEMP_IMAGE_PREFIX:Ljava/lang/String; = "temp_"

.field private static final TEMP_IMAGE_TIME_FORMAT:Ljava/lang/String; = "HH_mm_ss_SSS"

.field private static mZoomLevel:F


# instance fields
.field private IMAGE_CAPTURE_DATA:Ljava/lang/String;

.field private IMAGE_CAPTURE_URI:Ljava/lang/String;

.field private final RRECEIVER_CONTSTANT:I

.field actionBarButtonHRMListener:Landroid/view/View$OnClickListener;

.field private actionBarDrawable:Landroid/graphics/drawable/Drawable;

.field animationCallback:Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;

.field private animationToCenterInProgress:Z

.field content:Landroid/widget/FrameLayout;

.field private final dataTextViewIDs:[I

.field private defaultLatLng:Lcom/google/android/gms/maps/model/LatLng;

.field private delay:Z

.field private dispDataTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private firstValue:Ljava/lang/String;

.field private gpsSignalLostBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private gpsSignalLostDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field hrmContainer:Landroid/widget/LinearLayout;

.field hrmLayout:Landroid/widget/LinearLayout;

.field private images:[I

.field private internalViewIdUniquizeHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

.field private isAutoFocuses:Z

.field private isGoalAchieved:Z

.field private isHRMConnected:Z

.field public isInCenter:Z

.field public isManuallyDragged:Z

.field private isOptionMenuEnable:Z

.field private isVisualGuideLayoutVisible:Z

.field private mBTBroadcast:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$BTreceiver;

.field private mContext:Landroid/content/Context;

.field private mDataLayout:Landroid/widget/RelativeLayout;

.field private mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

.field public mFragmentHandler:Landroid/os/Handler;

.field private mHRMValue:I

.field mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mInfoDialog:Landroid/app/Dialog;

.field private mInfoView:Landroid/view/View;

.field private mLastLocPoint:Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

.field private mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

.field private mLastQueryTime:J

.field private mLastTakePhoto:Ljava/lang/String;

.field private mMap:Lcom/google/android/gms/maps/GoogleMap;

.field public mMapCenter:Lcom/google/android/gms/maps/model/LatLng;

.field private mMapHRMList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;"
        }
    .end annotation
.end field

.field private mMapMarkerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/maps/model/Marker;",
            ">;"
        }
    .end annotation
.end field

.field private mMapMode:I

.field private mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

.field mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

.field private mModeBtn:Landroid/widget/ImageButton;

.field public mMoveRange:D

.field public mMoveRange_latitude:D

.field public mMoveRange_longitude:D

.field private mPhotoDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field private mShowAgain:Landroid/widget/CheckBox;

.field private mShowAgainCheckLayout:Landroid/widget/LinearLayout;

.field private mTvClock:Landroid/widget/TextView;

.field private mUiSettings:Lcom/google/android/gms/maps/UiSettings;

.field private mVideoView:Landroid/widget/ImageView;

.field private mView:Landroid/view/View;

.field private mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

.field mapModeSettingBtn:Landroid/widget/ImageButton;

.field private notiTimer:Ljava/util/Timer;

.field private notificationText:Ljava/lang/String;

.field okOncliclListener:Landroid/view/View$OnClickListener;

.field public onFling:Z

.field private original_file:Ljava/io/File;

.field private photo:Landroid/graphics/Bitmap;

.field private popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

.field private secondValue:Ljava/lang/String;

.field private setMapTypeInPlayMap:Z

.field public startLocation:Lcom/google/android/gms/maps/model/LatLng;

.field public startingPolylines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/maps/model/Polyline;",
            ">;"
        }
    .end annotation
.end field

.field private uiThreadHandler:Landroid/os/Handler;

.field private viewModeHandler:Landroid/os/Handler;

.field private viewModeRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 142
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;

    .line 144
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->POLYLINE_DEFAULT_WIDTH:I

    .line 145
    const/16 v0, 0x80

    const/4 v1, 0x0

    const/16 v2, 0x85

    const/16 v3, 0x2c

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->POLYLINE_DEFAULT_COLOR:I

    .line 148
    const/high16 v0, 0x41880000    # 17.0f

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 154
    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMoveRange:D

    .line 155
    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMoveRange_latitude:D

    .line 156
    iput-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMoveRange_longitude:D

    .line 158
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startingPolylines:Ljava/util/List;

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMarkerList:Ljava/util/ArrayList;

    .line 160
    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->actionBarDrawable:Landroid/graphics/drawable/Drawable;

    .line 164
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->uiThreadHandler:Landroid/os/Handler;

    .line 166
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->RRECEIVER_CONTSTANT:I

    .line 169
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dataTextViewIDs:[I

    .line 177
    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->gpsSignalLostBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 180
    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    .line 181
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setMapTypeInPlayMap:Z

    .line 183
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->delay:Z

    .line 184
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isOptionMenuEnable:Z

    .line 197
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mHRMValue:I

    .line 199
    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocPoint:Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 201
    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 203
    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->photo:Landroid/graphics/Bitmap;

    .line 204
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 205
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 208
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isGoalAchieved:Z

    .line 209
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notificationText:Ljava/lang/String;

    .line 210
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->secondValue:Ljava/lang/String;

    .line 211
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->firstValue:Ljava/lang/String;

    .line 215
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->viewModeHandler:Landroid/os/Handler;

    .line 217
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->viewModeRunnable:Ljava/lang/Runnable;

    .line 224
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    .line 226
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationToCenterInProgress:Z

    .line 227
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isManuallyDragged:Z

    .line 228
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->onFling:Z

    .line 231
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationCallback:Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;

    .line 248
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, v2, v3, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->defaultLatLng:Lcom/google/android/gms/maps/model/LatLng;

    .line 254
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mPhotoDatas:Ljava/util/ArrayList;

    .line 256
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->internalViewIdUniquizeHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .line 260
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isAutoFocuses:Z

    .line 261
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isVisualGuideLayoutVisible:Z

    .line 262
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isHRMConnected:Z

    .line 264
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->images:[I

    .line 405
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->actionBarButtonHRMListener:Landroid/view/View$OnClickListener;

    .line 604
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    .line 806
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .line 1900
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$18;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->okOncliclListener:Landroid/view/View$OnClickListener;

    .line 1974
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mFragmentHandler:Landroid/os/Handler;

    return-void

    .line 169
    nop

    :array_0
    .array-data 4
        0x7f0806f7
        0x7f0806f9
    .end array-data

    .line 264
    :array_1
    .array-data 4
        0x7f020173
        0x7f020174
        0x7f0201fa
        0x7f0201fb
        0x7f0201fc
        0x7f0201fd
        0x7f0201fe
        0x7f0201ff
        0x7f0201fe
        0x7f0201ff
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->delay:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 140
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->delay:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationToCenterInProgress:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 140
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationToCenterInProgress:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/google/android/gms/maps/GoogleMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->viewModeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->viewModeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateNotificationBarText()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # I

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateDataText(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # J

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->lapClockDispUpdate(J)V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateDataText()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->gpsSignalLostBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->gpsSignalLostBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->gpsSignalLostDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->playMapFromCacheDB(Z)V

    return-void
.end method

.method static synthetic access$2300()F
    .locals 1

    .prologue
    .line 140
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateOutdoorLayout()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isVisualGuideLayoutVisible:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 140
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isVisualGuideLayoutVisible:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateLockMode(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMarkerList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # J

    .prologue
    .line 140
    iput-wide p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastQueryTime:J

    return-wide p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->uiThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getHeartRateData()V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->internalViewIdUniquizeHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3602(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mTvClock:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$3700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 140
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->saveInfoAndUpdateViewGrid(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->launchConnectivityActivity()V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Landroid/widget/ImageView;[IIZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # [I
    .param p3, "x3"    # I
    .param p4, "x4"    # Z

    .prologue
    .line 140
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animate(Landroid/widget/ImageView;[IIZ)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->doTakePhotoAction()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->showInfomationDialog()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V

    return-void
.end method

.method private addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V
    .locals 2
    .param p1, "_fragment"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 1969
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 1970
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f080091

    invoke-virtual {v0, v1, p1, p2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 1971
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1972
    return-void
.end method

.method private animate(Landroid/widget/ImageView;[IIZ)V
    .locals 12
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "images"    # [I
    .param p3, "imageIndex"    # I
    .param p4, "forever"    # Z

    .prologue
    .line 1925
    const/16 v8, 0x1f4

    .line 1926
    .local v8, "fadeInDuration":I
    const/16 v11, 0x3e8

    .line 1927
    .local v11, "timeBetween":I
    const/16 v10, 0x1f4

    .line 1929
    .local v10, "fadeOutDuration":I
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1930
    aget v0, p2, p3

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1932
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v7, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1933
    .local v7, "fadeIn":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v7, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1934
    int-to-long v0, v8

    invoke-virtual {v7, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1936
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {v9, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1937
    .local v9, "fadeOut":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1938
    const/16 v0, 0x5dc

    int-to-long v0, v0

    invoke-virtual {v9, v0, v1}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1939
    int-to-long v0, v10

    invoke-virtual {v9, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1941
    new-instance v6, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v6, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1942
    .local v6, "animation":Landroid/view/animation/AnimationSet;
    invoke-virtual {v6, v7}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1943
    invoke-virtual {v6, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1944
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/view/animation/AnimationSet;->setRepeatCount(I)V

    .line 1945
    invoke-virtual {p1, v6}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1947
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$19;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p1

    move/from16 v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$19;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;[IILandroid/widget/ImageView;Z)V

    invoke-virtual {v6, v0}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1966
    return-void
.end method

.method private convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 1148
    if-ne p2, v6, :cond_4

    .line 1149
    :try_start_0
    const-string v5, ":"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1150
    .local v1, "splitedDuration":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v3

    .line 1151
    .local v3, "state":I
    array-length v5, v1

    if-ne v5, v7, :cond_2

    .line 1152
    const/16 v5, 0x7d1

    if-ne v3, v5, :cond_1

    .line 1153
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090216

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    .end local v1    # "splitedDuration":[Ljava/lang/String;
    .end local v3    # "state":I
    :cond_0
    :goto_0
    move-object v4, p1

    .line 1194
    .end local p1    # "value":Ljava/lang/String;
    .local v4, "value":Ljava/lang/String;
    :goto_1
    return-object v4

    .line 1158
    .end local v4    # "value":Ljava/lang/String;
    .restart local v1    # "splitedDuration":[Ljava/lang/String;
    .restart local v3    # "state":I
    .restart local p1    # "value":Ljava/lang/String;
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090216

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1162
    :cond_2
    array-length v5, v1

    if-le v5, v7, :cond_0

    .line 1163
    const/16 v5, 0x7d1

    if-ne v3, v5, :cond_3

    .line 1164
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090216

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 1170
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09020b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    aget-object v6, v1, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090216

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 1176
    .end local v1    # "splitedDuration":[Ljava/lang/String;
    .end local v3    # "state":I
    :cond_4
    const/16 v5, 0x13

    if-ne p2, v5, :cond_0

    .line 1177
    const-string v5, "\'"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1178
    const-string v5, "\'"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1179
    .local v2, "splittedStrings":[Ljava/lang/String;
    array-length v5, v2

    if-le v5, v6, :cond_0

    .line 1180
    const/4 v5, 0x1

    aget-object v5, v2, v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1181
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901c0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v2, v6

    const-string v7, "\""

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cd

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    aget-object v7, v2, v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v7, v7, v8

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0

    .line 1185
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901c0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cd

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto/16 :goto_0

    .line 1191
    .end local v2    # "splittedStrings":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .local v0, "nfe":Ljava/lang/NumberFormatException;
    move-object v4, p1

    .line 1192
    .end local p1    # "value":Ljava/lang/String;
    .restart local v4    # "value":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method private createSaveCropFile()Landroid/net/Uri;
    .locals 9

    .prologue
    .line 2186
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2187
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 2188
    .local v3, "time":J
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v7, "HH_mm_ss_SSS"

    invoke-direct {v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 2189
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "temp_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2190
    .local v6, "url":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/SHealth/Exercise/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 2191
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 2192
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 2193
    .local v5, "uri":Landroid/net/Uri;
    return-object v5
.end method

.method private deleteTempFile()V
    .locals 2

    .prologue
    .line 2172
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 2174
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2176
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2178
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2181
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private doTakePhotoAction()V
    .locals 3

    .prologue
    .line 2161
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->deleteTempFile()V

    .line 2162
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2163
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 2164
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2165
    const-string/jumbo v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2166
    const/16 v1, 0x82

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2167
    return-void
.end method

.method private drawMapRoute(Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;)V
    .locals 10
    .param p1, "startLoc"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    .param p2, "endLoc"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .prologue
    .line 1457
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->POLYLINE_DEFAULT_COLOR:I

    .line 1458
    .local v0, "lineColor":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v2, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/android/gms/maps/model/LatLng;

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v6

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v6

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/PolylineOptions;->add([Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->POLYLINE_DEFAULT_WIDTH:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    .line 1462
    return-void
.end method

.method private drawRouteOnMap(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/maps/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1465
    .local p1, "mapPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/gms/maps/model/LatLng;>;"
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->POLYLINE_DEFAULT_COLOR:I

    .line 1466
    .local v0, "lineColor":I
    new-instance v1, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    .line 1467
    .local v1, "options":Lcom/google/android/gms/maps/model/PolylineOptions;
    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/PolylineOptions;->addAll(Ljava/lang/Iterable;)Lcom/google/android/gms/maps/model/PolylineOptions;

    .line 1468
    sget v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->POLYLINE_DEFAULT_WIDTH:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    .line 1469
    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    .line 1471
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    .line 1472
    return-void
.end method

.method private getActionbarBGDrawable()I
    .locals 3

    .prologue
    .line 1791
    const v0, 0x7f020023

    .line 1792
    .local v0, "resId":I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1793
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getCurrentMode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1794
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v1

    const/16 v2, 0x7d0

    if-eq v1, v2, :cond_0

    .line 1795
    const v0, 0x7f020251

    .line 1803
    :cond_0
    :goto_0
    return v0

    .line 1798
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1799
    const v0, 0x7f020251

    goto :goto_0
.end method

.method private getHeartRateData()V
    .locals 13

    .prologue
    const/4 v1, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 2022
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2023
    const/4 v10, 0x0

    .line 2024
    .local v10, "hrmValue":I
    const-wide/16 v7, 0x0

    .line 2025
    .local v7, "heartrateSampleTime":J
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v11

    const-string v0, "heart_rate_per_min"

    aput-object v0, v2, v12

    const-string/jumbo v0, "sample_time"

    aput-object v0, v2, v1

    .line 2029
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "exercise__id =?  AND user_device__id=? "

    .line 2031
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v11

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v12

    .line 2034
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 2035
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2038
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2040
    if-eqz v6, :cond_1

    .line 2041
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 2042
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2043
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2044
    const-string v0, "heart_rate_per_min"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 2045
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 2046
    new-instance v9, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    invoke-direct {v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;-><init>()V

    .line 2047
    .local v9, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    int-to-float v0, v10

    invoke-virtual {v9, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setHeartRateVal(F)V

    .line 2048
    invoke-virtual {v9, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setSampleTime(J)V

    .line 2049
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2050
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2057
    .end local v9    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 2058
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 2057
    :cond_1
    if-eqz v6, :cond_2

    .line 2058
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2061
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 2062
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setLocationtoHRMData()V

    .line 2063
    :cond_3
    return-void
.end method

.method private getMapMode()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x3

    .line 1476
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v4}, Lcom/google/android/gms/maps/GoogleMap;->getMapType()I

    move-result v1

    .line 1478
    .local v1, "mapMode":I
    const-string/jumbo v4, "mapMode"

    invoke-virtual {p0, v4, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1479
    .local v3, "viewMode":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1480
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v4, "mapMode"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1481
    .local v2, "savedViewMode":I
    if-ge v1, v6, :cond_0

    .line 1482
    const/4 v1, 0x1

    .line 1483
    :cond_0
    if-ne v2, v7, :cond_2

    .line 1484
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    .line 1485
    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    if-le v4, v5, :cond_1

    .line 1486
    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    .line 1488
    :cond_1
    const-string/jumbo v4, "mapMode"

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1489
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1497
    :goto_0
    return-void

    .line 1490
    :cond_2
    if-le v2, v5, :cond_3

    .line 1491
    iput v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    goto :goto_0

    .line 1493
    :cond_3
    if-ge v2, v6, :cond_4

    .line 1494
    const/4 v2, 0x1

    .line 1495
    :cond_4
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    goto :goto_0
.end method

.method private getNotificationBarText(I)Ljava/lang/String;
    .locals 6
    .param p1, "dataType"    # I

    .prologue
    .line 953
    const-string v3, ""

    .line 954
    .local v3, "valueStr":Ljava/lang/String;
    const-string v0, ""

    .line 955
    .local v0, "unitStr":Ljava/lang/String;
    const/4 v4, 0x2

    if-ne p1, v4, :cond_1

    .line 956
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 957
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {p1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v1

    .line 959
    .local v1, "val":F
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v5, "km"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 960
    float-to-long v4, v1

    long-to-double v4, v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMilesFromMetersWhileWorkingOut(D)Ljava/lang/String;

    move-result-object v3

    .line 964
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 975
    .end local v1    # "val":F
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 962
    .restart local v1    # "val":F
    :cond_0
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 966
    .end local v1    # "val":F
    :cond_1
    const/4 v4, 0x3

    if-ne p1, v4, :cond_2

    .line 967
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainSecToGoal(J)J

    move-result-wide v1

    .line 969
    .local v1, "val":J
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v3

    .line 970
    goto :goto_1

    .line 971
    .end local v1    # "val":J
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {p1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v1

    .line 972
    .local v1, "val":F
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v3

    .line 973
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private getOverlayEffectorDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1707
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->actionBarDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1708
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->actionBarDrawable:Landroid/graphics/drawable/Drawable;

    .line 1711
    :goto_0
    return-object v0

    .line 1710
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0202cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->actionBarDrawable:Landroid/graphics/drawable/Drawable;

    .line 1711
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->actionBarDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private initInfoVideo()V
    .locals 4

    .prologue
    .line 1919
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f08056b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mVideoView:Landroid/widget/ImageView;

    .line 1920
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mVideoView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->images:[I

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animate(Landroid/widget/ImageView;[IIZ)V

    .line 1921
    return-void
.end method

.method private initLayout(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 436
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const v1, 0x7f0301dd

    invoke-direct {v0, p0, p1, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;-><init>(Landroid/app/Activity;Landroid/os/Bundle;I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setActionBarOverlayMode(Z)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setEnableSwitchButton(Z)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setHealthControllerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setContentView(Landroid/view/View;)V

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0301aa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mView:Landroid/view/View;

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090a36

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mView:Landroid/view/View;

    const v1, 0x7f0806f6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mDataLayout:Landroid/widget/RelativeLayout;

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setMainView(Landroid/view/View;)V

    .line 446
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mView:Landroid/view/View;

    const v1, 0x7f0806fe

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->hrmContainer:Landroid/widget/LinearLayout;

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mView:Landroid/view/View;

    const v1, 0x7f0806ff

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->hrmLayout:Landroid/widget/LinearLayout;

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->hrmLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f080091

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->content:Landroid/widget/FrameLayout;

    .line 450
    const v0, 0x7f0806fb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mModeBtn:Landroid/widget/ImageButton;

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mModeBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 462
    const v0, 0x7f0806fa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mapModeSettingBtn:Landroid/widget/ImageButton;

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mapModeSettingBtn:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 474
    return-void
.end method

.method private internalHrmAvailable()Z
    .locals 8

    .prologue
    .line 1771
    const/4 v2, 0x0

    .line 1772
    .local v2, "internalHrmAvailable":Z
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    .line 1774
    .local v1, "healthService":Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getShealthDeviceFinderC()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1776
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getShealthDeviceFinderC()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v3

    const/4 v4, 0x4

    const/16 v5, 0x2718

    const/4 v6, 0x4

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->isAvailable(IIILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    .line 1787
    :cond_0
    :goto_0
    return v2

    .line 1779
    :catch_0
    move-exception v0

    .line 1780
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1781
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1782
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1783
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 1784
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private lapClockDispUpdate(J)V
    .locals 1
    .param p1, "second"    # J

    .prologue
    .line 827
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$13;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;J)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 833
    return-void
.end method

.method private launchConnectivityActivity()V
    .locals 1

    .prologue
    .line 1695
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_0

    .line 1696
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->stopScanning()V

    .line 1698
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->launchConnectivityActivity(Landroid/content/Context;)V

    .line 1699
    return-void
.end method

.method private playMap(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;I)V
    .locals 6
    .param p1, "newLocation"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    .param p2, "mode"    # I

    .prologue
    .line 1377
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "playMap: lat="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", lng="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1378
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    iget-wide v4, p1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 1379
    .local v0, "currentLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 1401
    :cond_0
    :goto_0
    return-void

    .line 1381
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setMapTypeInPlayMap:Z

    if-eqz v2, :cond_2

    .line 1382
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setMapTypeInPlayMap:Z

    .line 1383
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "playMap, Change the map type only once!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1384
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    invoke-static {v0, v3}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 1385
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->setMapType(I)V

    .line 1388
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    invoke-static {v0, v3}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationCallback:Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    .line 1391
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v1

    .line 1392
    .local v1, "state":I
    const/16 v2, 0x7d1

    if-ne v1, v2, :cond_0

    .line 1393
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->playMapFromCacheDB(Z)V

    goto :goto_0
.end method

.method private playMapFromCacheDB(Z)V
    .locals 14
    .param p1, "shouldAnimateCamera"    # Z

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v10

    .line 1405
    .local v10, "state":I
    const/16 v0, 0x7d0

    if-eq v10, v0, :cond_1

    .line 1408
    const-string v5, "create_time ASC"

    .line 1409
    .local v5, "sortOder":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "create_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastQueryTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1410
    .local v3, "selection":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1412
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$LocationData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1414
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMapPathDB(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 1416
    .local v6, "cacheLocationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    if-eqz v8, :cond_0

    .line 1417
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1419
    :cond_0
    if-nez v6, :cond_3

    .line 1420
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "playMapFromCacheDB, No Data!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1454
    .end local v3    # "selection":Ljava/lang/String;
    .end local v5    # "sortOder":Ljava/lang/String;
    .end local v6    # "cacheLocationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_0
    return-void

    .line 1416
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v5    # "sortOder":Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_2

    .line 1417
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1424
    .restart local v6    # "cacheLocationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_4

    .line 1425
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v9, v0, :cond_4

    .line 1426
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    add-int/lit8 v1, v9, 0x1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->drawMapRoute(Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;)V

    .line 1427
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastQueryTime:J

    .line 1428
    add-int/lit8 v0, v9, 0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocPoint:Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 1425
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1438
    .end local v9    # "i":I
    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1439
    new-instance v11, Landroid/location/Location;

    const-string v0, "TEMPLOC"

    invoke-direct {v11, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1440
    .local v11, "tmpLocation":Landroid/location/Location;
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v11, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 1441
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v0

    invoke-virtual {v11, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 1444
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {v11, v0, v1}, Landroid/location/Location;->setTime(J)V

    .line 1445
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getAccuracy()F

    move-result v0

    invoke-virtual {v11, v0}, Landroid/location/Location;->setAccuracy(F)V

    .line 1446
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRestart:Z

    .line 1447
    new-instance v7, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v11}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v11}, Landroid/location/Location;->getLongitude()D

    move-result-wide v12

    invoke-direct {v7, v0, v1, v12, v13}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 1448
    .local v7, "currentLatLng":Lcom/google/android/gms/maps/model/LatLng;
    if-eqz p1, :cond_1

    .line 1449
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    invoke-static {v7, v1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationCallback:Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    .line 1450
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    goto/16 :goto_0
.end method

.method private saveInfoAndUpdateViewGrid(Ljava/lang/String;J)V
    .locals 5
    .param p1, "picturePath"    # Ljava/lang/String;
    .param p2, "exerciseId"    # J

    .prologue
    .line 1617
    if-eqz p1, :cond_1

    .line 1618
    new-instance v1, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v1, p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>(Ljava/lang/String;)V

    .line 1619
    .local v1, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    if-eqz v2, :cond_2

    .line 1620
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    double-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iget-wide v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    double-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setLocate(FF)V

    .line 1628
    :cond_0
    :goto_0
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->insertTempitem(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Z)J

    .line 1629
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updatePhotoMarkIcon()V

    .line 1631
    .end local v1    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_1
    return-void

    .line 1623
    .restart local v1    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getLocateMetaInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v0

    .line 1624
    .local v0, "location":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    if-eqz v0, :cond_0

    .line 1625
    iget-wide v2, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    double-to-float v2, v2

    iget-wide v3, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    double-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setLocate(FF)V

    goto :goto_0
.end method

.method private setDropDownContentDescription(Landroid/view/View;I)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "type"    # I

    .prologue
    const v7, 0x7f09020b

    .line 1128
    if-eqz p1, :cond_0

    .line 1129
    const v4, 0x7f0806e1

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 1130
    .local v2, "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    const v4, 0x7f0806e4

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1131
    .local v0, "dataValue":Landroid/widget/TextView;
    const v4, 0x7f0806de

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1132
    .local v1, "dropDown":Landroid/view/View;
    const-string v3, " "

    .line 1133
    .local v3, "unitText":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1134
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->convertToProperUnit(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 1135
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901ec

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1144
    .end local v0    # "dataValue":Landroid/widget/TextView;
    .end local v1    # "dropDown":Landroid/view/View;
    .end local v2    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v3    # "unitText":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setUpMap()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1346
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, v6}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 1347
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPrevLocation:Landroid/location/Location;

    if-eqz v2, :cond_1

    .line 1348
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "setUpMap, RealtimeHealthService.mPrevLocation is NOT NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->setMapType(I)V

    .line 1350
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreviousLatLng:Lcom/google/android/gms/maps/model/LatLng;

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    invoke-static {v3, v4}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 1351
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setMapTypeInPlayMap:Z

    .line 1361
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    .line 1362
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    invoke-virtual {v2, v5}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 1363
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    invoke-virtual {v2, v5}, Lcom/google/android/gms/maps/UiSettings;->setZoomControlsEnabled(Z)V

    .line 1364
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    invoke-virtual {v2, v5}, Lcom/google/android/gms/maps/UiSettings;->setCompassEnabled(Z)V

    .line 1365
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    invoke-virtual {v2, v6}, Lcom/google/android/gms/maps/UiSettings;->setAllGesturesEnabled(Z)V

    .line 1367
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v1

    .line 1368
    .local v1, "lat":Lcom/google/android/gms/maps/model/LatLng;
    if-eqz v1, :cond_0

    .line 1369
    new-instance v0, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    .line 1370
    .local v0, "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    .line 1371
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v3

    const/16 v4, 0x168

    const/16 v5, 0x220

    const/4 v6, 0x3

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 1372
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeMapZoomLevel()F

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/maps/CameraUpdateFactory;->zoomTo(F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 1374
    .end local v0    # "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    :cond_0
    return-void

    .line 1353
    .end local v1    # "lat":Lcom/google/android/gms/maps/model/LatLng;
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "setUpMap, RealtimeHealthService.mPrevLocation is NULL!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1354
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->setMapType(I)V

    .line 1355
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->defaultLatLng:Lcom/google/android/gms/maps/model/LatLng;

    sget v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    invoke-static {v3, v4}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngZoom(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 1356
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/gms/maps/UiSettings;->setAllGesturesEnabled(Z)V

    .line 1357
    const/high16 v2, 0x41880000    # 17.0f

    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    .line 1358
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setMapTypeInPlayMap:Z

    goto :goto_0
.end method

.method private setUpMapIfNeeded()V
    .locals 3

    .prologue
    .line 1310
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v1, :cond_1

    .line 1311
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0806f5

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    .line 1312
    .local v0, "supportMapFragment":Lcom/google/android/gms/maps/SupportMapFragment;
    if-eqz v0, :cond_0

    .line 1313
    invoke-virtual {v0}, Lcom/google/android/gms/maps/SupportMapFragment;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 1315
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v1, :cond_1

    .line 1316
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$14;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraChangeListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;)V

    .line 1339
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getMapMode()V

    .line 1340
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setUpMap()V

    .line 1343
    .end local v0    # "supportMapFragment":Lcom/google/android/gms/maps/SupportMapFragment;
    :cond_1
    return-void
.end method

.method private setUpMarkIcon()V
    .locals 2

    .prologue
    .line 774
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    .line 775
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->setOnUpdatePhotoListListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;)V

    .line 776
    return-void
.end method

.method private setVisivibilityBasedOnRtl()V
    .locals 24

    .prologue
    .line 1807
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    if-nez v20, :cond_0

    .line 1858
    :goto_0
    return-void

    .line 1810
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08056c

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 1811
    .local v10, "oneView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08056f

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    .line 1812
    .local v18, "twoView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080572

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 1813
    .local v15, "threeView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080575

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 1814
    .local v8, "fourView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080578

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1815
    .local v6, "fiveView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08057b

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 1817
    .local v13, "sixView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08056e

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 1818
    .local v11, "oneViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080571

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    .line 1819
    .local v19, "twoViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080574

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 1820
    .local v16, "threeViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080577

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 1821
    .local v9, "fourViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08057a

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1822
    .local v7, "fiveViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08057d

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 1823
    .local v14, "sixViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080557

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 1824
    .local v12, "showagaintextview":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080568

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 1826
    .local v17, "titletextview":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    .line 1827
    .local v5, "config":Landroid/content/res/Configuration;
    invoke-virtual {v5}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    .line 1828
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1829
    const/16 v20, 0x8

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1830
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1831
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1832
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1833
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1834
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1835
    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    .line 1836
    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1837
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1838
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1839
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1840
    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0a058e

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v22

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1841
    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0a058e

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v22

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0

    .line 1843
    :cond_1
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1844
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1845
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1846
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1847
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1848
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1849
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1850
    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    .line 1851
    const/16 v20, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1852
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1853
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1854
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1855
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a058e

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1856
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0
.end method

.method private setZoomLevel()V
    .locals 1

    .prologue
    .line 1643
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1644
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    .line 1648
    :goto_0
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeMapZoomLevel(F)V

    .line 1649
    return-void

    .line 1646
    :cond_0
    const/high16 v0, 0x41880000    # 17.0f

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mZoomLevel:F

    goto :goto_0
.end method

.method private showInfomationDialog()V
    .locals 4

    .prologue
    .line 1861
    const-string v2, "layout_inflater"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1862
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030146

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    .line 1863
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setVisivibilityBasedOnRtl()V

    .line 1864
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->initInfoVideo()V

    .line 1865
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    const v3, 0x7f0804ee

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mShowAgainCheckLayout:Landroid/widget/LinearLayout;

    .line 1866
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    const v3, 0x7f0804ef

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mShowAgain:Landroid/widget/CheckBox;

    .line 1867
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mShowAgainCheckLayout:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$16;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$16;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1875
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    const v3, 0x7f08056a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1877
    .local v1, "okButton":Landroid/widget/Button;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->okOncliclListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1881
    new-instance v2, Landroid/app/Dialog;

    const v3, 0x7f0c0081

    invoke-direct {v2, p0, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoDialog:Landroid/app/Dialog;

    .line 1882
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoDialog:Landroid/app/Dialog;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 1883
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoDialog:Landroid/app/Dialog;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$17;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$17;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1897
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mInfoDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 1898
    return-void
.end method

.method private startNotificationBarTimer()V
    .locals 7

    .prologue
    .line 499
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notiTimer:Ljava/util/Timer;

    .line 500
    const/16 v6, 0x3e8

    .line 501
    .local v6, "period":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notiTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    const-wide/16 v2, 0x0

    int-to-long v4, v6

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 523
    return-void
.end method

.method private updateDataText()V
    .locals 6

    .prologue
    .line 1106
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dataTextViewIDs:[I

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 1107
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1108
    .local v3, "type":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dataTextViewIDs:[I

    aget v5, v5, v0

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1109
    .local v4, "view":Landroid/view/View;
    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;

    invoke-direct {v5, p0, v4, v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$DropDownClickListener;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Landroid/view/View;II)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1111
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    invoke-static {v5, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 1112
    .local v1, "text":Ljava/lang/String;
    const v5, 0x7f0806e1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 1113
    .local v2, "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1114
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->invalidate()V

    .line 1115
    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    .line 1116
    const v5, 0x7f0806e4

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mTvClock:Landroid/widget/TextView;

    .line 1120
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateDataText(I)V

    .line 1122
    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setDropDownContentDescription(Landroid/view/View;I)V

    .line 1106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1124
    .end local v1    # "text":Ljava/lang/String;
    .end local v2    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v3    # "type":I
    .end local v4    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private updateDataText(I)V
    .locals 23
    .param p1, "type"    # I

    .prologue
    .line 978
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dataTextViewIDs:[I

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v8, v0, :cond_1d

    .line 979
    const/4 v13, 0x0

    .line 980
    .local v13, "update":Z
    const-string v17, ""

    .local v17, "valueStr":Ljava/lang/String;
    const-string v12, ""

    .line 981
    .local v12, "unitStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_9

    .line 982
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v5

    .line 983
    .local v5, "goalType":I
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v3

    .line 984
    .local v3, "dataType":I
    const/16 v19, 0x7

    move/from16 v0, p1

    move/from16 v1, v19

    if-eq v0, v1, :cond_0

    move/from16 v0, p1

    if-ne v0, v3, :cond_1

    .line 985
    :cond_0
    const/4 v13, 0x1

    .line 986
    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v3, v0, :cond_5

    .line 987
    const-string v17, "--"

    .line 988
    const-string v12, ""

    .line 1084
    .end local v3    # "dataType":I
    .end local v5    # "goalType":I
    :cond_1
    :goto_1
    if-eqz v13, :cond_4

    .line 1085
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mView:Landroid/view/View;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dataTextViewIDs:[I

    move-object/from16 v20, v0

    aget v20, v20, v8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    .line 1087
    .local v18, "view":Landroid/view/View;
    const v19, 0x7f0806e4

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;

    .line 1089
    .local v11, "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    const/16 v19, 0x13

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const/16 v20, 0x13

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    :cond_2
    const/16 v19, 0x3

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const/16 v20, 0x13

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1c

    .line 1092
    :cond_3
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1098
    .local v14, "updateText":Ljava/lang/String;
    :goto_2
    invoke-virtual {v11, v14}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1099
    invoke-virtual {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;->invalidate()V

    .line 1100
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setDropDownContentDescription(Landroid/view/View;I)V

    .line 978
    .end local v11    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v14    # "updateText":Ljava/lang/String;
    .end local v18    # "view":Landroid/view/View;
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 989
    .restart local v3    # "dataType":I
    .restart local v5    # "goalType":I
    :cond_5
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v3, v0, :cond_6

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainSecToGoal(J)J

    move-result-wide v15

    .line 991
    .local v15, "val":J
    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v17

    .line 992
    const-string v12, ""

    .line 993
    goto/16 :goto_1

    .end local v15    # "val":J
    :cond_6
    const/16 v19, 0x2

    move/from16 v0, v19

    if-ne v3, v0, :cond_8

    .line 994
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->loadUnitsFromSharedPreferences()V

    .line 995
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    move/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v15

    .line 997
    .local v15, "val":F
    sget-object v19, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/CommonCache;->infoDistanceUnit:Ljava/lang/String;

    const-string v20, "km"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_7

    .line 998
    float-to-long v0, v15

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    long-to-double v0, v0

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getMilesFromMetersWhileWorkingOut(D)Ljava/lang/String;

    move-result-object v17

    .line 1002
    :goto_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    .line 1004
    goto/16 :goto_1

    .line 1000
    :cond_7
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    goto :goto_3

    .line 1005
    .end local v15    # "val":F
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    move/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getRemainValueToGoal(IF)F

    move-result v15

    .line 1006
    .restart local v15    # "val":F
    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    .line 1007
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-static {v15}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1010
    .end local v3    # "dataType":I
    .end local v5    # "goalType":I
    .end local v15    # "val":F
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const/16 v20, 0xe

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, p1

    move/from16 v1, v19

    if-eq v0, v1, :cond_b

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const/16 v20, 0xe

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_12

    const/16 v19, 0x18

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_12

    .line 1012
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v5

    .line 1013
    .restart local v5    # "goalType":I
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v3

    .line 1014
    .restart local v3    # "dataType":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v6

    .line 1015
    .local v6, "goalValue":I
    const/16 v19, 0xe

    move/from16 v0, p1

    move/from16 v1, v19

    if-eq v0, v1, :cond_c

    const/16 v19, 0x18

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 1016
    :cond_c
    const/4 v13, 0x1

    .line 1017
    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v3, v0, :cond_d

    .line 1018
    const-string v17, "--"

    .line 1019
    const-string v12, ""

    goto/16 :goto_1

    .line 1020
    :cond_d
    const/16 v19, 0x5

    move/from16 v0, v19

    if-ne v5, v0, :cond_f

    .line 1021
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v9

    .line 1022
    .local v9, "level":I
    const/4 v10, 0x0

    .line 1023
    .local v10, "progressInPercentage":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    const/16 v20, 0x18

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    if-eqz v19, :cond_e

    .line 1024
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    const/16 v20, 0x18

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 1026
    :cond_e
    const/16 v19, 0x18

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    .line 1027
    const-string v12, "%"

    .line 1028
    goto/16 :goto_1

    .end local v9    # "level":I
    .end local v10    # "progressInPercentage":F
    :cond_f
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v3, v0, :cond_11

    .line 1029
    const/16 v19, 0x5

    move/from16 v0, v19

    if-ne v5, v0, :cond_10

    .line 1031
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v9

    .line 1032
    .restart local v9    # "level":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v9, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getEffectStringByType(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v17

    .line 1033
    const-string v12, ""

    .line 1034
    goto/16 :goto_1

    .line 1035
    .end local v9    # "level":I
    :cond_10
    mul-int/lit8 v19, v6, 0x3c

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v17

    .line 1036
    const-string v12, ""

    goto/16 :goto_1

    .line 1039
    :cond_11
    int-to-float v0, v6

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    .line 1040
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    int-to-float v0, v6

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1043
    .end local v3    # "dataType":I
    .end local v5    # "goalType":I
    .end local v6    # "goalValue":I
    :cond_12
    const/16 v19, 0x13

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const/16 v20, 0x13

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_14

    :cond_13
    const/16 v19, 0x3

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    const/16 v20, 0x13

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_16

    .line 1045
    :cond_14
    const/4 v13, 0x1

    .line 1046
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    const/16 v20, 0x3

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    const/16 v20, 0x0

    cmpg-float v19, v19, v20

    if-gtz v19, :cond_15

    .line 1047
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f090acd

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1051
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const/16 v21, 0x13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    move/from16 v0, v21

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v19

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1049
    :cond_15
    const/16 v20, 0x13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    const/16 v21, 0x3

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    move/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    goto :goto_4

    .line 1052
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v19

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 1053
    const/4 v13, 0x1

    .line 1054
    const/16 v19, 0x1

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_17

    .line 1055
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v17

    .line 1056
    const-string v12, ""

    goto/16 :goto_1

    .line 1057
    :cond_17
    const/16 v19, 0x17

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_19

    .line 1058
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v5

    .line 1059
    .restart local v5    # "goalType":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalValue()I

    move-result v6

    .line 1060
    .restart local v6    # "goalValue":I
    const/16 v19, 0x5

    move/from16 v0, v19

    if-ne v5, v0, :cond_1

    .line 1061
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeTrainingLevel()I

    move-result v9

    .line 1062
    .restart local v9    # "level":I
    const-string v4, ""

    .line 1063
    .local v4, "goal":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    const/16 v20, 0x17

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    if-eqz v19, :cond_18

    .line 1064
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getEffectGoalStringByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    .line 1066
    :cond_18
    move-object/from16 v17, v4

    .line 1067
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getActivityType()I

    move-result v20

    const/16 v21, 0x7

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getActivityTypeGoalValue(Landroid/content/Context;II)I

    move-result v7

    .line 1069
    .local v7, "goalValueMins":I
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " / "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v20

    const v21, 0x7f0900eb

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_1

    .line 1072
    .end local v4    # "goal":Ljava/lang/String;
    .end local v5    # "goalType":I
    .end local v6    # "goalValue":I
    .end local v7    # "goalValueMins":I
    .end local v9    # "level":I
    :cond_19
    const/16 v19, 0x5

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    const/16 v20, 0x0

    cmpl-float v19, v19, v20

    if-nez v19, :cond_1a

    .line 1073
    const-string v17, "--"

    .line 1080
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    move/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v19

    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 1074
    :cond_1a
    const/16 v19, 0x6

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Float;->floatValue()F

    move-result v19

    const/16 v20, 0x0

    cmpl-float v19, v19, v20

    if-nez v19, :cond_1b

    .line 1075
    const-string v17, "0"

    goto :goto_5

    .line 1078
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-interface/range {v19 .. v20}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Float;

    move/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v17

    goto :goto_5

    .line 1095
    .restart local v11    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .restart local v18    # "view":Landroid/view/View;
    :cond_1c
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "updateText":Ljava/lang/String;
    goto/16 :goto_2

    .line 1103
    .end local v11    # "tvScroll":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ScrollTextView;
    .end local v12    # "unitStr":Ljava/lang/String;
    .end local v13    # "update":Z
    .end local v14    # "updateText":Ljava/lang/String;
    .end local v17    # "valueStr":Ljava/lang/String;
    .end local v18    # "view":Landroid/view/View;
    :cond_1d
    return-void
.end method

.method private updateLockMode(Z)V
    .locals 10
    .param p1, "isLocked"    # Z

    .prologue
    const/4 v9, 0x1

    .line 749
    const/4 v2, 0x0

    .line 751
    .local v2, "visibility":I
    if-eqz p1, :cond_1

    .line 752
    const/4 v2, 0x4

    .line 753
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    .line 755
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    if-eqz v3, :cond_0

    .line 756
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/maps/model/CameraPosition;->builder(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    iget-wide v7, v7, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    .line 757
    .local v1, "pos":Lcom/google/android/gms/maps/model/CameraPosition;
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationToCenterInProgress:Z

    .line 758
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-static {v1}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->animationCallback:Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;Lcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    .line 765
    .end local v1    # "pos":Lcom/google/android/gms/maps/model/CameraPosition;
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mModeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 767
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mapModeSettingBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 768
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dataTextViewIDs:[I

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 769
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dataTextViewIDs:[I

    aget v3, v3, v0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0807d6

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 768
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 761
    .end local v0    # "i":I
    :cond_1
    const/4 v2, 0x0

    .line 762
    iput-boolean v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    .line 763
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setLocationModeIcon(Z)V

    goto :goto_0

    .line 771
    .restart local v0    # "i":I
    :cond_2
    return-void
.end method

.method private updateNotificationBarText()V
    .locals 2

    .prologue
    .line 863
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    .line 864
    .local v0, "goalType":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 866
    :cond_0
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startNotification(I)V

    .line 872
    :goto_0
    return-void

    .line 867
    :cond_1
    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 868
    const/16 v1, 0x17

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startNotification(I)V

    goto :goto_0

    .line 870
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startNotification(I)V

    goto :goto_0
.end method

.method private updateOutdoorLayout()V
    .locals 5

    .prologue
    .line 1658
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a015f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1659
    .local v0, "actionBarSize":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v3, :cond_2

    .line 1660
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v2

    .line 1661
    .local v2, "state":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1663
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090a36

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 1665
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mDataLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1666
    .local v1, "param":Landroid/widget/RelativeLayout$LayoutParams;
    packed-switch v2, :pswitch_data_0

    .line 1680
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1681
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mDataLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1682
    const-wide/16 v3, 0x0

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->lapClockDispUpdate(J)V

    .line 1683
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v3, :cond_1

    .line 1684
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMap;->clear()V

    .line 1689
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateDataText()V

    .line 1691
    .end local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "state":I
    :cond_2
    return-void

    .line 1670
    .restart local v1    # "param":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v2    # "state":I
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getIsVisualGuideShown()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1671
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0867

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v3, v0

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1676
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mDataLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1674
    :cond_3
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_1

    .line 1666
    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updatePhotoMarkIcon()V
    .locals 7

    .prologue
    .line 779
    const/4 v6, 0x0

    .line 781
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 782
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 783
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 784
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getExercisePhotoDataList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mPhotoDatas:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 787
    :cond_0
    if-eqz v6, :cond_1

    .line 788
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 790
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mPhotoDatas:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    .line 804
    :cond_2
    :goto_0
    return-void

    .line 787
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 788
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 793
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 797
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->SetPhotoList(Ljava/util/List;)V

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public addHRMMarkerToMap()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2097
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 2098
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 2100
    .local v1, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    .line 2101
    const v4, 0x7f02031c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p0, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->drawTextToBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2103
    .local v0, "bm":Landroid/graphics/Bitmap;
    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v6

    float-to-double v6, v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 2104
    .local v3, "pos":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v5, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v5}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HRM_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->snippet(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v5

    const/high16 v6, 0x3f000000    # 0.5f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/maps/model/MarkerOptions;->anchor(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/Marker;->setIcon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)V

    .line 2097
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "pos":Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 2107
    .end local v1    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_1
    return-void
.end method

.method protected customizeActionBar()V
    .locals 15

    .prologue
    .line 345
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getOverlayEffectorDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f080304

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 349
    .local v14, "view":Landroid/view/View;
    if-eqz v14, :cond_0

    .line 351
    const v3, 0x7f020251

    invoke-virtual {v14, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 352
    invoke-virtual {v14}, Landroid/view/View;->clearFocus()V

    .line 353
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 355
    :cond_0
    new-instance v8, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$3;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    .line 363
    .local v8, "accessaryButtonListener":Landroid/view/View$OnClickListener;
    new-instance v9, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$4;

    invoke-direct {v9, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    .line 373
    .local v9, "cameraButtonListener":Landroid/view/View$OnClickListener;
    new-instance v10, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$5;

    invoke-direct {v10, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    .line 385
    .local v10, "galleryButtonListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->internalHrmAvailable()Z

    move-result v11

    .line 387
    .local v11, "isInternalHrmAvailable":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v12

    .line 388
    .local v12, "screenHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v13

    .line 389
    .local v13, "screenWidth":I
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v1, 0x7f020382

    const/4 v2, 0x0

    const v3, 0x7f090022

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->actionBarButtonHRMListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionbarBGDrawable()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 391
    .local v0, "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    if-eqz v11, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isHRMConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 392
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 394
    :cond_1
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207bc

    const/4 v3, 0x0

    const v4, 0x7f0907b4

    const v6, 0x7f020251

    move-object v5, v8

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 397
    .local v1, "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f020250

    const/4 v4, 0x0

    const v5, 0x7f0900df

    const v7, 0x7f020251

    move-object v6, v9

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 399
    .local v2, "actionButton3":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v3

    const/16 v4, 0x7d0

    if-eq v3, v4, :cond_2

    .line 400
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 402
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 403
    return-void
.end method

.method public drawTextToBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "resourceId"    # I
    .param p3, "mText"    # Ljava/lang/String;

    .prologue
    .line 2112
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 2113
    .local v6, "resources":Landroid/content/res/Resources;
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    iget v7, v10, Landroid/util/DisplayMetrics;->density:F

    .line 2114
    .local v7, "scale":F
    move/from16 v0, p2

    invoke-static {v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2116
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    .line 2118
    .local v2, "bitmapConfig":Landroid/graphics/Bitmap$Config;
    if-nez v2, :cond_0

    .line 2119
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 2123
    :cond_0
    const/4 v10, 0x1

    invoke-virtual {v1, v2, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2125
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2127
    .local v4, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    const/4 v10, 0x1

    invoke-direct {v5, v10}, Landroid/graphics/Paint;-><init>(I)V

    .line 2129
    .local v5, "paint":Landroid/graphics/Paint;
    const/16 v10, 0x6e

    const/16 v11, 0x6e

    const/16 v12, 0x6e

    invoke-static {v10, v11, v12}, Landroid/graphics/Color;->rgb(III)I

    move-result v10

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 2131
    const/high16 v10, 0x41400000    # 12.0f

    mul-float/2addr v10, v7

    float-to-int v10, v10

    int-to-float v10, v10

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2133
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, -0xbbbbbc

    invoke-virtual {v5, v10, v11, v12, v13}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 2138
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 2139
    .local v3, "bounds":Landroid/graphics/Rect;
    const/4 v10, 0x0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v11

    move-object/from16 v0, p3

    invoke-virtual {v5, v0, v10, v11, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2144
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    div-int/lit8 v8, v10, 0x2

    .line 2145
    .local v8, "x":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v11

    add-int/2addr v10, v11

    div-int/lit8 v9, v10, 0x2

    .line 2147
    .local v9, "y":I
    int-to-float v10, v8

    int-to-float v11, v9

    move-object/from16 v0, p3

    invoke-virtual {v4, v0, v10, v11, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2155
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "bitmapConfig":Landroid/graphics/Bitmap$Config;
    .end local v3    # "bounds":Landroid/graphics/Rect;
    .end local v4    # "canvas":Landroid/graphics/Canvas;
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "resources":Landroid/content/res/Resources;
    .end local v7    # "scale":F
    .end local v8    # "x":I
    .end local v9    # "y":I
    :goto_0
    return-object v1

    .line 2150
    :catch_0
    move-exception v10

    .line 2155
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2016
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2017
    .local v0, "android_id":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "10008_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2018
    .local v1, "deviceId":Ljava/lang/String;
    return-object v1
.end method

.method public getNotificationTextByType(I)Ljava/lang/String;
    .locals 5
    .param p1, "type"    # I

    .prologue
    const/16 v4, 0x18

    .line 920
    const-string v0, ""

    .line 922
    .local v0, "notiText":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 949
    :goto_0
    return-object v0

    .line 928
    :sswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090a6f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getNotificationBarText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 929
    goto :goto_0

    .line 931
    :sswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090a70

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getNotificationBarText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 936
    goto :goto_0

    .line 938
    :sswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090a6e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getNotificationBarText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 939
    goto :goto_0

    .line 942
    :sswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090a2d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 944
    goto/16 :goto_0

    .line 922
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0x4 -> :sswitch_1
        0x5 -> :sswitch_3
        0x17 -> :sswitch_3
    .end sparse-switch
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1501
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$15;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    .line 1523
    .local v0, "onSaveChosenItemListener":Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    return-object v0
.end method

.method public hideActionBar()V
    .locals 1

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->hideActionBar()V

    .line 337
    return-void
.end method

.method public isDeviceConnected()Z
    .locals 12

    .prologue
    const/4 v7, 0x1

    .line 1716
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x12

    if-lt v8, v9, :cond_2

    .line 1717
    const-string v8, "bluetooth"

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothManager;

    .line 1718
    .local v2, "btManager":Landroid/bluetooth/BluetoothManager;
    const/4 v8, 0x7

    invoke-virtual {v2, v8}, Landroid/bluetooth/BluetoothManager;->getConnectedDevices(I)Ljava/util/List;

    move-result-object v3

    .line 1719
    .local v3, "connectedList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v3, :cond_2

    .line 1720
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 1721
    .local v1, "btDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_0

    .line 1722
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1723
    .local v4, "deviceName":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 1724
    const-string v8, " "

    const-string v9, ""

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1725
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Samsung EI-AN900A"

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, " "

    const-string v11, ""

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1744
    .end local v1    # "btDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "btManager":Landroid/bluetooth/BluetoothManager;
    .end local v3    # "connectedList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v4    # "deviceName":Ljava/lang/String;
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    return v7

    .line 1736
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "connected_wearable_id"

    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1737
    .local v0, "btAddress":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "connected_wearable"

    invoke-static {v8, v9}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1738
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_3

    .line 1739
    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    const-string v9, "GEAR2"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    const-string v9, "WINGTIP"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 1744
    :cond_3
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public isHRMConnected()Z
    .locals 1

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_0

    .line 2008
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isHRMConnected()Z

    move-result v0

    .line 2010
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isNinePopup()Z
    .locals 11

    .prologue
    const/16 v10, 0x9

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1748
    const/4 v7, 0x0

    .line 1749
    .local v7, "imageCount":I
    const/4 v6, 0x0

    .line 1752
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1753
    if-eqz v6, :cond_0

    .line 1754
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 1757
    :cond_0
    if-eqz v6, :cond_1

    .line 1758
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1761
    :cond_1
    if-ge v7, v10, :cond_3

    move v0, v8

    .line 1766
    :goto_0
    return v0

    .line 1757
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1758
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1765
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090f73

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v9}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v9

    .line 1766
    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 13
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1529
    const/16 v0, 0x7b

    if-ne p1, v0, :cond_2

    .line 1530
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 1531
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_1

    .line 1532
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateAcessaryText(Z)V

    .line 1590
    :cond_0
    :goto_0
    return-void

    .line 1536
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    if-eqz v0, :cond_0

    .line 1537
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateAcessaryText(Z)V

    goto :goto_0

    .line 1540
    :cond_2
    const/16 v0, 0x7e

    if-ne p1, v0, :cond_3

    .line 1541
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setResult(I)V

    .line 1542
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->finish()V

    goto :goto_0

    .line 1545
    :cond_3
    const/16 v0, 0x7e

    if-ne p1, v0, :cond_4

    .line 1546
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1547
    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setResult(I)V

    .line 1548
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->finish()V

    goto :goto_0

    .line 1550
    :cond_4
    const/16 v0, 0x82

    if-ne p1, v0, :cond_6

    .line 1551
    if-eqz p2, :cond_0

    .line 1553
    const/4 v3, 0x0

    .line 1554
    .local v3, "picturePath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v0, :cond_5

    .line 1557
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getImagePathByUri(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1567
    :goto_1
    if-eqz v3, :cond_0

    .line 1568
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file://"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1569
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->getPathToImage()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getRealtimeExerciseId()J

    move-result-wide v5

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$ExerciseImageScaleCopyTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1560
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastTakePhoto:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1563
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastTakePhoto:Ljava/lang/String;

    .line 1564
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastTakePhoto:Ljava/lang/String;

    goto :goto_1

    .line 1572
    .end local v3    # "picturePath":Ljava/lang/String;
    :cond_6
    const/16 v0, 0x83

    if-ne p1, v0, :cond_0

    .line 1573
    if-eqz p2, :cond_0

    .line 1575
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1576
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 1577
    .local v5, "selectedImage":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v6, v0

    .line 1578
    .local v6, "filePathColumn":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1579
    .local v11, "cursor":Landroid/database/Cursor;
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1580
    const/4 v0, 0x0

    aget-object v0, v6, v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 1581
    .local v10, "columnIndex":I
    invoke-interface {v11, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1582
    .restart local v3    # "picturePath":Ljava/lang/String;
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1583
    if-eqz v3, :cond_0

    .line 1584
    new-instance v12, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v12, v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>(Ljava/lang/String;)V

    .line 1585
    .local v12, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    const/4 v0, 0x0

    invoke-static {p0, v12, v0}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->insertTempitem(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Z)J

    .line 1586
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updatePhotoMarkIcon()V

    goto/16 :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1635
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->stopTimer()V

    .line 1636
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isScreenLockState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1640
    :goto_0
    return-void

    .line 1639
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x5

    .line 273
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 274
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v3

    if-nez v3, :cond_0

    .line 276
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 277
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v3, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 279
    const/high16 v3, 0x4000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 280
    const/high16 v3, 0x20000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 281
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startActivity(Landroid/content/Intent;)V

    .line 283
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 285
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->doBindRealtimeService(Landroid/content/Context;)V

    .line 286
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->initLayout(Landroid/os/Bundle;)V

    .line 287
    iput-object p0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mContext:Landroid/content/Context;

    .line 288
    iput-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastQueryTime:J

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "less_than_2min_tag"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 291
    .local v0, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1

    .line 293
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 296
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->checkSupportedHRM()Z

    move-result v3

    if-nez v3, :cond_2

    .line 297
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->DISP_DATA_LIST:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 298
    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->DISP_DATA_LIST:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 300
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isHRMConnected()Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isHRMConnected:Z

    .line 303
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeDispDataStatus()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    .line 305
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mTvClock:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    .line 306
    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->lapClockDispUpdate(J)V

    .line 308
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setUpMarkIcon()V

    .line 309
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 310
    .local v2, "lFilter":Landroid/content/IntentFilter;
    const-string v3, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 311
    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$BTreceiver;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$BTreceiver;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$1;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mBTBroadcast:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$BTreceiver;

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mBTBroadcast:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$BTreceiver;

    invoke-virtual {p0, v3, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 313
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapHRMList:Ljava/util/ArrayList;

    .line 314
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 572
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getIsLocked()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isOptionMenuEnable:Z

    if-nez v1, :cond_1

    .line 573
    :cond_0
    const/4 v1, 0x0

    .line 583
    :goto_0
    return v1

    .line 575
    :cond_1
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 576
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 577
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f10001e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 578
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealTimeAudioGuideItemIndex()I

    move-result v1

    if-nez v1, :cond_2

    .line 579
    const v1, 0x7f090a77

    invoke-interface {p1, v1}, Landroid/view/Menu;->removeItem(I)V

    .line 583
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 581
    :cond_2
    const v1, 0x7f090a78

    invoke-interface {p1, v1}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 564
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mBTBroadcast:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$BTreceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->onDestroy()V

    .line 567
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->stopTimer()V

    .line 568
    return-void
.end method

.method public onMarkerClick(Lcom/google/android/gms/maps/model/Marker;)Z
    .locals 1
    .param p1, "arg0"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 1703
    const/4 v0, 0x0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 588
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getIsLocked()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isOptionMenuEnable:Z

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 600
    :goto_0
    return v0

    .line 591
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 600
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 593
    :pswitch_0
    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealTimeAudioGuideIndex(I)V

    goto :goto_0

    .line 597
    :pswitch_1
    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setRealTimeAudioGuideIndex(I)V

    goto :goto_0

    .line 591
    :pswitch_data_0
    .packed-switch 0x7f090a77
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 492
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setHealthControllerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;)V

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->onPause()V

    .line 495
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startNotificationBarTimer()V

    .line 496
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    const/16 v1, 0x7d0

    if-ne v0, v1, :cond_0

    .line 534
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->finish()V

    .line 536
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 538
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090a36

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 540
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    if-eqz v0, :cond_2

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->popup:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->dismiss()V

    .line 544
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->stopTimer()V

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v0, :cond_3

    .line 546
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getMapMode()V

    .line 547
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapMode:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setMapType(I)V

    .line 548
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->playMapFromCacheDB(Z)V

    .line 551
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getHealthControllerListener()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    if-nez v0, :cond_4

    .line 552
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setHealthControllerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;)V

    .line 553
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->onResume()V

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isLockMode()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateLockMode(Z)V

    .line 556
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updatePhotoMarkIcon()V

    .line 557
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getHeartRateData()V

    .line 558
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->addHRMMarkerToMap()V

    .line 560
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 485
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onStart()V

    .line 486
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setUpMapIfNeeded()V

    .line 488
    return-void
.end method

.method public refreshFocusables()V
    .locals 0

    .prologue
    .line 340
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->refreshFocusables()V

    .line 341
    return-void
.end method

.method public restoreLocationIfNeeded()V
    .locals 15

    .prologue
    .line 2198
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isManuallyDragged:Z

    if-nez v0, :cond_0

    .line 2214
    :goto_0
    return-void

    .line 2201
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startLocation:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startLocation:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v4}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v6}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getDistanceFromLatLngInMeter(DDDD)D

    move-result-wide v8

    .line 2202
    .local v8, "distance":D
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    float-to-double v13, v0

    .line 2204
    .local v13, "zoom":D
    const-wide/high16 v0, 0x3ffc000000000000L    # 1.75

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    const-wide/high16 v4, 0x4035000000000000L    # 21.0

    sub-double/2addr v4, v13

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double v11, v0, v2

    .line 2206
    .local v11, "thresholdDistance":D
    cmpg-double v0, v8, v11

    if-gez v0, :cond_1

    .line 2207
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/maps/model/CameraPosition;->builder(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startLocation:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v10

    .line 2208
    .local v10, "pos":Lcom/google/android/gms/maps/model/CameraPosition;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-static {v10}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    .line 2213
    .end local v10    # "pos":Lcom/google/android/gms/maps/model/CameraPosition;
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isManuallyDragged:Z

    goto :goto_0

    .line 2210
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setLocationModeIcon(Z)V

    .line 2211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isInCenter:Z

    goto :goto_1
.end method

.method public setHRMStatus(Z)V
    .locals 1
    .param p1, "isHRMConnected"    # Z

    .prologue
    .line 428
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isHRMConnected:Z

    if-eq v0, p1, :cond_0

    .line 429
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isHRMConnected:Z

    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->invalidateOptionsMenu()V

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->customizeActionBar()V

    .line 433
    :cond_0
    return-void
.end method

.method public setLocationModeIcon(Z)V
    .locals 2
    .param p1, "isInCenter"    # Z

    .prologue
    .line 477
    if-nez p1, :cond_0

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mModeBtn:Landroid/widget/ImageButton;

    const v1, 0x7f0203f7

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 481
    :goto_0
    return-void

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mModeBtn:Landroid/widget/ImageButton;

    const v1, 0x7f0203f6

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method public setLocationtoHRMData()V
    .locals 11

    .prologue
    .line 2070
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_3

    .line 2071
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 2072
    .local v7, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select location_data.[latitude],location_data.[longitude] from location_data where location_data.[exercise__id]= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and location_data.[sample_time]<= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getSampleTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " order by location_data.[sample_time] desc limit 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2073
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2076
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2077
    if-eqz v6, :cond_0

    .line 2078
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2079
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2080
    const-string v0, "latitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    .line 2081
    .local v9, "latitude":F
    const-string v0, "longitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v10

    .line 2082
    .local v10, "longitude":F
    invoke-virtual {v7, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setLatitude(F)V

    .line 2083
    invoke-virtual {v7, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setLongitude(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2088
    .end local v9    # "latitude":F
    .end local v10    # "longitude":F
    :cond_0
    if-eqz v6, :cond_1

    .line 2089
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2070
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 2088
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 2089
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2094
    .end local v3    # "selectionClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_3
    return-void
.end method

.method public setNotificationText(I)V
    .locals 6
    .param p1, "type"    # I

    .prologue
    const/4 v5, 0x2

    .line 882
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isGoalAchieved:Z

    if-nez v1, :cond_0

    .line 883
    sparse-switch p1, :sswitch_data_0

    .line 901
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-static {v5, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkValue(ILjava/lang/Float;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-static {v5, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkUnit(ILjava/lang/Float;)I

    move-result v1

    invoke-static {v3, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getDataUnitByType(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->secondValue:Ljava/lang/String;

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getDataSet()Ljava/util/Map;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->checkTimeValue(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->firstValue:Ljava/lang/String;

    .line 908
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->firstValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->secondValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notificationText:Ljava/lang/String;

    .line 917
    :goto_0
    return-void

    .line 885
    :sswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v0

    .line 886
    .local v0, "goalType":I
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getNotificationTextByType(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notificationText:Ljava/lang/String;

    goto :goto_0

    .line 897
    .end local v0    # "goalType":I
    :sswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getNotificationTextByType(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notificationText:Ljava/lang/String;

    goto :goto_0

    .line 912
    :cond_0
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->firstValue:Ljava/lang/String;

    .line 913
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090b95

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->secondValue:Ljava/lang/String;

    .line 914
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->secondValue:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notificationText:Ljava/lang/String;

    goto :goto_0

    .line 883
    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_1
        0x7 -> :sswitch_0
        0x17 -> :sswitch_1
    .end sparse-switch
.end method

.method public showActionBar()V
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->showActionBar()V

    .line 333
    return-void
.end method

.method public startNotification(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 875
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->isGoalAchieved()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isGoalAchieved:Z

    .line 876
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getWorkoutState()I

    move-result v0

    const/16 v1, 0x7d0

    if-eq v0, v1, :cond_0

    .line 877
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->setNotificationText(I)V

    .line 878
    :cond_0
    return-void
.end method

.method public stopTimer()V
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notiTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->notiTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 528
    :cond_0
    return-void
.end method

.method public updateStartLocation()V
    .locals 1

    .prologue
    .line 2217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->isManuallyDragged:Z

    .line 2218
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getCameraPosition()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->startLocation:Lcom/google/android/gms/maps/model/LatLng;

    .line 2219
    return-void
.end method

.method public updateUI(J)V
    .locals 17
    .param p1, "second"    # J

    .prologue
    .line 836
    const-wide/16 v3, 0x3c

    .line 837
    .local v3, "MINUTE":J
    const-wide/16 v1, 0xe10

    .line 838
    .local v1, "HOUR":J
    const-wide/16 v13, 0xe10

    div-long v6, p1, v13

    .line 839
    .local v6, "hour":J
    const-wide/16 v13, 0xe10

    rem-long p1, p1, v13

    .line 840
    const-wide/16 v13, 0x3c

    div-long v9, p1, v13

    .line 841
    .local v9, "minute":J
    const-wide/16 v13, 0x3c

    rem-long p1, p1, v13

    .line 842
    const-string v13, "%02d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 843
    .local v8, "hourText":Ljava/lang/String;
    const-string v13, "%02d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 844
    .local v11, "minuteText":Ljava/lang/String;
    const-string v13, "%02d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 845
    .local v12, "secondText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->dispDataTypes:Ljava/util/ArrayList;

    const/4 v14, 0x7

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 848
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeGoalType()I

    move-result v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->getDataTypeByGoalType(I)I

    move-result v5

    .line 849
    .local v5, "dataType":I
    const/4 v13, 0x1

    if-ne v5, v13, :cond_0

    .line 850
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateDataText(I)V

    .line 853
    .end local v5    # "dataType":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mTvClock:Landroid/widget/TextView;

    if-eqz v13, :cond_1

    .line 854
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mTvClock:Landroid/widget/TextView;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 856
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mTvClock:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->invalidate()V

    .line 857
    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateDataText(I)V

    .line 859
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->updateNotificationBarText()V

    .line 860
    return-void
.end method
