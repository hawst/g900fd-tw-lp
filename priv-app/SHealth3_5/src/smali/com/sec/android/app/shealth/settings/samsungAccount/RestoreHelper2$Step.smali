.class final enum Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
.super Ljava/lang/Enum;
.source "RestoreHelper2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Step"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum CHECK_DATA_TO_IMPORT:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum CHECK_PROFILE_FOR_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum CONFIRMATION_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum CONFIRMATION_WITH_BACKUP_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum LOADING_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum NETWORK_FAILURE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum NOTICE_FOR_NO_DATA_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum REQUEST_TOKEN:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum SERVER_CHECKING_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum START_BACKUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum START_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

.field public static final enum WIFI_NOTIFICATION_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 161
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 162
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "REQUEST_TOKEN"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->REQUEST_TOKEN:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 163
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "CHECK_PROFILE_FOR_RESTORE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_PROFILE_FOR_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 164
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "START_RESTORE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->START_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 165
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "START_BACKUP"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->START_BACKUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 166
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "CHECK_DATA_TO_IMPORT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_DATA_TO_IMPORT:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 168
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "SERVER_CHECKING_POPUP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->SERVER_CHECKING_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 169
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "LOADING_POPUP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->LOADING_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 170
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "CONFIRMATION_POPUP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CONFIRMATION_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 171
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "CONFIRMATION_WITH_BACKUP_POPUP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CONFIRMATION_WITH_BACKUP_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 172
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "RESTORATION_PROGRESS_POPUP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 173
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "NOTICE_FOR_NO_DATA_POPUP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NOTICE_FOR_NO_DATA_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 174
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "WIFI_NOTIFICATION_POPUP"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->WIFI_NOTIFICATION_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 175
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "FAILED_POPUP"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 176
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "DEVICE_STORAGE_LOW"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 179
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "STORAGE_LOW"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 180
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "SERVER_MAINTENANCE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 181
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    const-string v1, "NETWORK_FAILURE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NETWORK_FAILURE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 159
    const/16 v0, 0x12

    new-array v0, v0, [Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->REQUEST_TOKEN:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_PROFILE_FOR_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->START_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->START_BACKUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_DATA_TO_IMPORT:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->SERVER_CHECKING_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->LOADING_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CONFIRMATION_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CONFIRMATION_WITH_BACKUP_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->RESTORATION_PROGRESS_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NOTICE_FOR_NO_DATA_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->WIFI_NOTIFICATION_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->FAILED_POPUP:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->SERVER_MAINTENANCE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NETWORK_FAILURE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->$VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 159
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    .locals 1

    .prologue
    .line 159
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->$VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    return-object v0
.end method
