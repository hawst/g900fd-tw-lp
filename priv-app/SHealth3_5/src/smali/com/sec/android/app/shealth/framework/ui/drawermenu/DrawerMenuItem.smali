.class public Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;
.super Ljava/lang/Object;


# instance fields
.field action:Ljava/lang/String;

.field mDisplayName:Ljava/lang/String;

.field mPackageName:Ljava/lang/String;

.field mType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->mDisplayName:Ljava/lang/String;

    iput p2, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->mType:I

    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->mPackageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->action:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->action:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->mType:I

    return v0
.end method
