.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;
.super Ljava/lang/Object;
.source "AMapExerciseProMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->initLayout(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 466
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->isInCenter:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 468
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mMap:Lcom/amap/api/maps2d/AMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/AMap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/maps2d/AMap;->getCameraPosition()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v1

    invoke-static {v1}, Lcom/amap/api/maps2d/model/CameraPosition;->builder(Lcom/amap/api/maps2d/model/CameraPosition;)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v3

    iget-wide v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v5

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->target(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->build()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    .line 469
    .local v0, "pos":Lcom/amap/api/maps2d/model/CameraPosition;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->animationToCenterInProgress:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;Z)Z

    .line 470
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->mMap:Lcom/amap/api/maps2d/AMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;)Lcom/amap/api/maps2d/AMap;

    move-result-object v1

    invoke-static {v0}, Lcom/amap/api/maps2d/CameraUpdateFactory;->newCameraPosition(Lcom/amap/api/maps2d/model/CameraPosition;)Lcom/amap/api/maps2d/CameraUpdate;

    move-result-object v2

    const-wide/16 v3, 0x438

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity$7;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;->animationCallback:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/amap/api/maps2d/AMap;->animateCamera(Lcom/amap/api/maps2d/CameraUpdate;JLcom/amap/api/maps2d/AMap$CancelableCallback;)V

    .line 472
    .end local v0    # "pos":Lcom/amap/api/maps2d/model/CameraPosition;
    :cond_0
    return-void
.end method
