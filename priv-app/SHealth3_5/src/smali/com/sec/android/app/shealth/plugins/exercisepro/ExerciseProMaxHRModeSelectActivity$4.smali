.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$4;
.super Ljava/lang/Object;
.source "ExerciseProMaxHRModeSelectActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Z

    .prologue
    .line 207
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 209
    .local v1, "value":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;I)I

    .line 212
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 219
    .local v0, "realValue":I
    :goto_0
    const/16 v2, 0x64

    if-ge v0, v2, :cond_0

    .line 220
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->showMaxHrWarningPopup()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;)V

    .line 222
    :cond_0
    return-void

    .line 216
    .end local v0    # "realValue":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->mMaxValue:I
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;->access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;I)I

    .line 217
    const/4 v0, 0x0

    .restart local v0    # "realValue":I
    goto :goto_0
.end method
