.class public final enum Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
.super Ljava/lang/Enum;
.source "CignaConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LIFE_STYLE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field public static final enum EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field public static final enum FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field public static final enum SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field public static final enum STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field public static final enum WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;


# instance fields
.field private mStringResID:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 111
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    const-string v1, "EXERCISE"

    const v2, 0x7f090c85

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    const-string v1, "FOOD"

    const v2, 0x7f090c86

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    const-string v1, "SLEEP"

    const v2, 0x7f09017e

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    const-string v1, "STRESS"

    const v2, 0x7f090c87

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    const-string v1, "WEIGHT"

    const v2, 0x7f090c5f

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 110
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "stringResID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->mStringResID:I

    .line 116
    iput p3, p0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->mStringResID:I

    .line 117
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 110
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    return-object v0
.end method


# virtual methods
.method public getStringResID()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->mStringResID:I

    return v0
.end method
