.class public Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "CompatibleAccessoryDetailActivity.java"


# instance fields
.field mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

.field private mCompanyPicture:Landroid/widget/ImageView;

.field private mConnectivityImage:Landroid/widget/ImageView;

.field private mDevicePicture:Landroid/widget/ImageView;

.field private mDeviceText:Landroid/widget/TextView;

.field private mDeviceType:Landroid/widget/TextView;

.field private mManSiteText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->isBrowserSupported(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method private isBrowserSupported(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 275
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 276
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 278
    const/4 v2, 0x1

    .line 282
    :cond_0
    return v2
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 237
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 239
    new-instance v0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;)V

    .line 260
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 262
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207bc

    const v3, 0x7f0907b4

    invoke-direct {v1, v2, v4, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 263
    .local v1, "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 264
    return-void
.end method

.method public getConnectionTypeIcon(I)I
    .locals 2
    .param p1, "connectionType"    # I

    .prologue
    const v0, 0x7f02028f

    .line 213
    const/4 v1, 0x5

    if-ne p1, v1, :cond_1

    .line 215
    const v0, 0x7f02028e

    .line 230
    :cond_0
    :goto_0
    return v0

    .line 217
    :cond_1
    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    .line 221
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 223
    const v0, 0x7f020290

    goto :goto_0

    .line 224
    :cond_2
    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    .line 230
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getDeviceType(I)I
    .locals 3
    .param p1, "deviceType"    # I

    .prologue
    const v0, 0x7f09018d

    const v1, 0x7f090025

    .line 168
    const/16 v2, 0x2712

    if-ne p1, v2, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v0

    .line 172
    :cond_1
    const/16 v2, 0x271b

    if-ne p1, v2, :cond_2

    .line 174
    const v0, 0x7f090bb3

    goto :goto_0

    .line 176
    :cond_2
    const/16 v2, 0x2718

    if-ne p1, v2, :cond_3

    .line 178
    const v0, 0x7f090023

    goto :goto_0

    .line 180
    :cond_3
    const/16 v2, 0x271d

    if-ne p1, v2, :cond_4

    move v0, v1

    .line 182
    goto :goto_0

    .line 184
    :cond_4
    const/16 v2, 0x2714

    if-ne p1, v2, :cond_5

    .line 186
    const v0, 0x7f090e0f

    goto :goto_0

    .line 188
    :cond_5
    const/16 v2, 0x2713

    if-ne p1, v2, :cond_6

    .line 190
    const v0, 0x7f09018f

    goto :goto_0

    .line 192
    :cond_6
    const/16 v2, 0x2724

    if-ne p1, v2, :cond_7

    move v0, v1

    .line 194
    goto :goto_0

    .line 196
    :cond_7
    const/16 v2, 0x2726

    if-ne p1, v2, :cond_8

    move v0, v1

    .line 198
    goto :goto_0

    .line 200
    :cond_8
    const/16 v2, 0x2723

    if-ne p1, v2, :cond_0

    move v0, v1

    .line 202
    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "accessory_device_detail"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    check-cast v3, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    if-nez v3, :cond_0

    .line 68
    new-instance v3, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    .line 71
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    const v3, 0x7f03009d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->setContentView(I)V

    .line 75
    const v3, 0x7f0802f5

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mDevicePicture:Landroid/widget/ImageView;

    .line 76
    const v3, 0x7f0802f0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mCompanyPicture:Landroid/widget/ImageView;

    .line 77
    const v3, 0x7f0802f2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mDeviceText:Landroid/widget/TextView;

    .line 78
    const v3, 0x7f0802f3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mDeviceType:Landroid/widget/TextView;

    .line 80
    const v3, 0x7f08009a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mConnectivityImage:Landroid/widget/ImageView;

    .line 82
    const v3, 0x7f0802f6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mManSiteText:Landroid/widget/TextView;

    .line 84
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mDevicePicture:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getImagePath()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 85
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mCompanyPicture:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getLogoPath()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f090e0f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mDeviceText:Landroid/widget/TextView;

    const v4, 0x7f090e0d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mDeviceType:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getDeviceType()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getDeviceType(I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    const-string v2, ""

    .line 98
    .local v2, "mWebSite":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getWebsite()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getWebsite()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "samsung"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 99
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 100
    const-string v2, "m.samsung.com/sec"

    .line 108
    :goto_1
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 109
    .local v0, "content":Landroid/text/SpannableString;
    new-instance v3, Landroid/text/style/UnderlineSpan;

    invoke-direct {v3}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v3, v6, v4, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mManSiteText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mManSiteText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 113
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mManSiteText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090043

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mManSiteText:Landroid/widget/TextView;

    new-instance v4, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getConnectivity()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getConnectionTypeIcon(I)I

    move-result v1

    .line 144
    .local v1, "drawableType":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mConnectivityImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 147
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 157
    return-void

    .line 91
    .end local v0    # "content":Landroid/text/SpannableString;
    .end local v1    # "drawableType":I
    .end local v2    # "mWebSite":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mDeviceText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 102
    .restart local v2    # "mWebSite":Ljava/lang/String;
    :cond_3
    const-string v2, "m.samsung.com"

    goto :goto_1

    .line 105
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;->getWebsite()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 164
    return-void
.end method
