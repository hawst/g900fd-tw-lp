.class public Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;
.super Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;
.source "TGHContentView.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

.field private mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;-><init>(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method private initLeftBar()V
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getTextBalanceColor()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getTextUnbalanceColor()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getTextNoDataColor()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValueTextColors(III)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getBalanceLeftIconId()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getUnbalanceLeftIconId()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getNoDataLeftIconId()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValueIconIds(III)V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getUnitLeftIconId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setUnitIconIds(I)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getLeftBarStatisticsSource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setStatisticsSource(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getLeftBarTitleText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setTitle(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method private initRightBar()V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getTextBalanceColor()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getTextUnbalanceColor()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getTextNoDataColor()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValueTextColors(III)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getBalanceRightIconId()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getUnbalanceRightIconId()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getNoDataRightIconId()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValueIconIds(III)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getUnitRightIconId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setUnitIconIds(I)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getRightBarStatisticsSource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setStatisticsSource(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getRightBarTitleText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setTitle(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method private refreshUnitIcons()V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getUnitLeftIconId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setUnitIconIds(I)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getUnitRightIconId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setUnitIconIds(I)V

    .line 241
    return-void
.end method


# virtual methods
.method protected getBalanceLeftIconId()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method protected getBalanceRightIconId()I
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    return v0
.end method

.method protected getLeftBarStatisticsSource()Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09027c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLeftBarTitleText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNoDataLeftIconId()I
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return v0
.end method

.method protected getNoDataRightIconId()I
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method protected getRightBarStatisticsSource()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09027c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRightBarTitleText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getTextBalanceColor()I
    .locals 2

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method protected getTextNoDataColor()I
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method protected getTextUnbalanceColor()I
    .locals 2

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method protected getUnbalanceLeftIconId()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method protected getUnbalanceRightIconId()I
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method protected getUnitLeftIconId()I
    .locals 2

    .prologue
    .line 119
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v1, "F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    const v0, 0x7f0900ce

    .line 124
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0900cd

    goto :goto_0
.end method

.method protected getUnitRightIconId()I
    .locals 1

    .prologue
    .line 132
    const v0, 0x7f0910a9

    return v0
.end method

.method protected initializeContentView()V
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030253

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    const v0, 0x7f0804d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    .line 48
    const v0, 0x7f0804d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->initLeftBar()V

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->initRightBar()V

    .line 51
    return-void
.end method

.method public setAnimatorListener(Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setAnimatorListener(Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setAnimatorListener(Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;)V

    .line 273
    return-void
.end method

.method public setBalanceLine(FF)V
    .locals 1
    .param p1, "valueLeft"    # F
    .param p2, "valueRight"    # F

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setBalanceLineValue(F)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setBalanceLineValue(F)V

    .line 263
    return-void
.end method

.method public setBalanceLine(II)V
    .locals 1
    .param p1, "valueLeft"    # I
    .param p2, "valueRight"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setBalanceLineValue(I)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setBalanceLineValue(I)V

    .line 252
    return-void
.end method

.method public setValues(FFZZ)V
    .locals 4
    .param p1, "valueLeft"    # F
    .param p2, "valueRight"    # F
    .param p3, "updateLeft"    # Z
    .param p4, "updateRight"    # Z

    .prologue
    const/high16 v3, -0x3b860000    # -1000.0f

    .line 198
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->refreshUnitIcons()V

    .line 199
    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    .line 200
    cmpl-float v0, p1, v3

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v1, "F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    const-string v1, "C"

    const-string v2, "F"

    invoke-static {p1, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValue(F)V

    .line 206
    :goto_0
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_2

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValue(F)V

    .line 216
    :cond_0
    :goto_1
    return-void

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValue(F)V

    goto :goto_0

    .line 212
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setValues valueLeft = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValue(F)V

    goto :goto_1
.end method

.method public setValues(IIZZ)V
    .locals 4
    .param p1, "valueLeft"    # I
    .param p2, "valueRight"    # I
    .param p3, "updateLeft"    # Z
    .param p4, "updateRight"    # Z

    .prologue
    .line 225
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->refreshUnitIcons()V

    .line 226
    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    .line 227
    int-to-float v0, p1

    const/high16 v1, -0x3b860000    # -1000.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v1, "F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    int-to-float v1, p1

    const-string v2, "C"

    const-string v3, "F"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValue(F)V

    .line 233
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarRight:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValue(I)V

    .line 235
    :cond_0
    return-void

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->mContentBarLeft:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentBar;->setValue(I)V

    goto :goto_0
.end method
