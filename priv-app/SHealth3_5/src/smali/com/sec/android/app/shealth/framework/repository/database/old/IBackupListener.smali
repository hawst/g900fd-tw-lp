.class public interface abstract Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener;
.super Ljava/lang/Object;
.source "IBackupListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;
    }
.end annotation


# virtual methods
.method public abstract onAccountException(Ljava/lang/String;)V
.end method

.method public abstract onAccountNetworkError()V
.end method

.method public abstract onBackupProgress(Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;J)V
.end method

.method public abstract onException(Ljava/lang/Exception;)V
.end method

.method public abstract onLastDeviceId(Ljava/lang/String;)V
.end method

.method public abstract onNoData()V
.end method
