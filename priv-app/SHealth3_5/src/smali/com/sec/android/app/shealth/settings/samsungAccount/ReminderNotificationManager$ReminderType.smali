.class public final enum Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;
.super Ljava/lang/Enum;
.source "ReminderNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReminderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

.field public static final enum AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

.field public static final enum SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    const-string v1, "SAMSUNG_ACCOUNT_NOTIFICATION"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    .line 25
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    const-string v1, "AUTO_BACKUP_NOTIFICATION"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->AUTO_BACKUP_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->$VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->$VALUES:[Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    return-object v0
.end method
