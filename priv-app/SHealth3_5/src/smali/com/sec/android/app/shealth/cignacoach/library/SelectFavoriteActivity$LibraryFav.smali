.class Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;
.super Ljava/lang/Object;
.source "SelectFavoriteActivity.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LibraryFav"
.end annotation


# instance fields
.field private child:I

.field private grp:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 665
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "g"    # I
    .param p2, "c"    # I

    .prologue
    .line 660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 662
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->grp:I

    .line 663
    iput p2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->child:I

    .line 664
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 696
    if-ne p0, p1, :cond_1

    .line 713
    :cond_0
    :goto_0
    return v1

    .line 699
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 700
    goto :goto_0

    .line 702
    :cond_2
    instance-of v3, p1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;

    if-nez v3, :cond_3

    move v1, v2

    .line 703
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 705
    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;

    .line 707
    .local v0, "other":Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->child:I

    iget v4, v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->child:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 708
    goto :goto_0

    .line 710
    :cond_4
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->grp:I

    iget v4, v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->grp:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 711
    goto :goto_0
.end method

.method public getChild()I
    .locals 1

    .prologue
    .line 675
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->child:I

    return v0
.end method

.method public getGroup()I
    .locals 1

    .prologue
    .line 670
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->grp:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 684
    const/16 v0, 0x1f

    .line 685
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 686
    .local v1, "result":I
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->child:I

    add-int/lit8 v1, v2, 0x1f

    .line 687
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->grp:I

    add-int v1, v2, v3

    .line 688
    return v1
.end method
