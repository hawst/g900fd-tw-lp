.class public abstract Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/IBuildCacheCallback;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$4;,
        Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$IBackListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$HelpDialogButtonController;,
        Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;,
        Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;,
        Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;
    }
.end annotation


# static fields
.field private static final DRAWER_FULL_SHOWN:F = 1.0f

.field private static final NAVIGATE_TO_HOME:Ljava/lang/String; = "navigating_to_home"

.field private static final NOT_ENOUGH_MEMORY:Ljava/lang/String; = "not_enough_memory"

.field private static mLauncherActivity:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;",
            ">;"
        }
    .end annotation
.end field

.field protected static onStopClassName:Ljava/lang/String;


# instance fields
.field protected drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

.field public isBroadcastRegistered:Z

.field private mBStateLossOnsaveInstance:Z

.field private mCachePopupBitmap:Landroid/graphics/Bitmap;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mDrawerFlag:Z

.field private mDrawerIsOpened:Z

.field protected mDrawerLayout:Landroid/widget/LinearLayout;

.field private mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

.field protected mDrawerMenuList:Landroid/widget/ListView;

.field private mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

.field protected mDrawerStateShown:Z

.field private mDrawerToggle:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;

.field private mFragmentAnimationListener:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;

.field private mFragmentRootLayout:Landroid/widget/FrameLayout;

.field private mHardKeyBoardHiddenState:I

.field private mHomeClick:Z

.field private mInAnimation:Landroid/view/animation/Animation;

.field private mIsConfigChanged:Z

.field private mIsDrawShown:Z

.field private mIsDrawerPartialShown:Z

.field private mIsTitleShown:Z

.field private mLaunchAction:Ljava/lang/String;

.field private mMainContent:Landroid/view/View;

.field private mMenuList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private mMenuListMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;",
            ">;"
        }
    .end annotation
.end field

.field private mOldFragmentView:Landroid/widget/ImageView;

.field private mOutAnimation:Landroid/view/animation/Animation;

.field private mPreValueSlide:F

.field private mReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;

.field private mUserClicked:Z

.field private mValueSlide:F

.field private outStateBundle:Landroid/os/Bundle;

.field private registryContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;

.field protected showOnStart:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuListMap:Ljava/util/HashMap;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerIsOpened:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerFlag:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mBStateLossOnsaveInstance:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showOnStart:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mHomeClick:Z

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDialogControllerMap:Ljava/util/Map;

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mHardKeyBoardHiddenState:I

    return-void
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawerPartialShown:Z

    return p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawShown:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
    .locals 1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getClearableActivity()Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->selectItem(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->resetSelectedPosition()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mCachePopupBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Landroid/widget/FrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mFragmentRootLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->setDrawerItemContentDescription(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerFlag:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerFlag:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerIsOpened:Z

    return p1
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsTitleShown:Z

    return p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;F)F
    .locals 0

    iput p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mValueSlide:F

    return p1
.end method

.method private addToDrawerMenuList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;ZILjava/lang/String;ILjava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p6

    move v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;-><init>(Ljava/lang/String;Ljava/lang/Class;ZLjava/lang/String;I)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuListMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le p5, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p5

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getMenuListItem(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0, p5, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;

    invoke-direct {v1, p1, v6, p2, p6}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p5, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_2
    return-void
.end method

.method private checkActionForLogging(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "com.sec.shealth.action.Username"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR01"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "com.sec.shealth.HomeActivity"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR02"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR03"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "com.sec.shealth.action.EXERCISE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR04"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v0, "com.sec.shealth.action.HEART_RATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR05"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    const-string v0, "com.sec.shealth.action.FOOD"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR06"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    const-string v0, "com.sec.shealth.action.WEIGHT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR07"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "com.sec.shealth.action.SLEEP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR08"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "com.sec.shealth.action.STRESS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR09"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v0, "com.sec.shealth.action.COACH"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR10"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v0, "android.shealth.action.MOREAPPS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR12"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    const-string v0, "com.sec.shealth.action.UV"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR13"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const-string v0, "com.sec.shealth.action.SPO2"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR14"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    const-string v0, "com.sec.shealth.action.BLOOD_GLUCOSE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR15"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    const-string v0, "com.sec.shealth.action.ECG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR16"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    const-string v0, "com.sec.shealth.action.BODYTEMP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR17"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    const-string v0, "com.sec.shealth.action.bloodpressure.BLOODPRESSURE_MAIN_ACTIVITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR18"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_12
    const-string v0, "com.sec.shealth.action.thermohygrometer.THERMOHYGROMETER_MAIN_ACTIVITY"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    const-string v2, "DR19"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getClearableActivity()Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v3, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.home.HomeActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMenuListItem(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;
    .locals 2

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private init(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v3, 0x0

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$layout;->blank_screen:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->setContentView(I)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onStopClassName:Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-boolean v7, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsConfigChanged:Z

    const-string v0, "drawer_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawShown:Z

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fromHome"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mHomeClick:Z

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLaunchAction:Ljava/lang/String;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->drawer_layout:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->left_drawer:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$id;->list:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setSaveEnabled(Z)V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->initializeDrawerMenuList()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->registerDBlistener()V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->updateDrawerItemSelection()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "first_visible"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "top"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "first_visible"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "top"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "first_visible"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "top"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_2
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->createActionBar()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, p0, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarListener(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper$ActionBarListener;Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->tw_ic_ab_drawer_holo_light:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarUpButton(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->drawer_menu:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarUpButtonDescription(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->invalidateOptionsMenu()V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showOnStart:Z

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "showOnStart"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "showOnStart"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showOnStart:Z

    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$drawable;->ic_launcher:I

    sget v5, Lcom/sec/android/app/shealth/framework/ui/R$string;->s_health:I

    sget v6, Lcom/sec/android/app/shealth/framework/ui/R$string;->s_health:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Landroid/app/Activity;Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;III)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerToggle:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerToggle:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setDrawerListener(Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout$DrawerListener;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$DrawerItemClickListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showOnStart:Z

    if-eqz v0, :cond_4

    if-nez p1, :cond_4

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mFragmentAnimationListener:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->isBroadcastRegistered:Z

    if-nez v0, :cond_5

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;-><init>(Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v7, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->isBroadcastRegistered:Z

    :cond_5
    if-eqz p1, :cond_6

    const-string v0, "drawer_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerStateShown:Z

    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->invalidateOptionsMenu()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.home.HomeActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->tryClearingStack()V

    return-void
.end method

.method private initializeDrawerMenuList()V
    .locals 11

    const/4 v7, -0x1

    const/16 v5, 0x64

    const/4 v10, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->user_name:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth"

    const/4 v4, 0x0

    const-string v6, "com.sec.shealth.action.Username"

    move-object v0, p0

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToDrawerMenuList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;ZILjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->s_health_main:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth"

    const-string v6, "com.sec.shealth.HomeActivity"

    move-object v0, p0

    move v4, v10

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToDrawerMenuList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;ZILjava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    invoke-static {v10, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    const-string v2, "com.sec.shealth.action.COACH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->packagename:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    iget v7, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->pluginId:I

    move-object v0, p0

    move v4, v10

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToDrawerMenuList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;ZILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private performFramentAnimation(Z)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOutAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mInAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;

    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mFragmentRootLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mFragmentRootLayout:Landroid/widget/FrameLayout;

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v0, v5}, Landroid/view/View;->buildDrawingCache(Z)V

    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mCachePopupBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mCachePopupBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mFragmentRootLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    if-eqz p1, :cond_3

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$anim;->period_flipper_slide_left:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOutAnimation:Landroid/view/animation/Animation;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$anim;->period_flipper_slide_from_right:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mInAnimation:Landroid/view/animation/Animation;

    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOldFragmentView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mInAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mFragmentAnimationListener:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$FragmentSwitchAnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto/16 :goto_0

    :cond_3
    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$anim;->period_flipper_slide_right:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mOutAnimation:Landroid/view/animation/Animation;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$anim;->period_flipper_slide_from_left:I

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mInAnimation:Landroid/view/animation/Animation;

    goto :goto_1
.end method

.method private registerDBlistener()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->registryContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;-><init>(Landroid/os/Handler;Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->registryContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->registryContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method private resetSelectedPosition()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private selectItem(I)V
    .locals 7

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuListMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->clear()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    :cond_0
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->action_not_defined_for_this_item:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->action:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->isPlugin:Z

    if-eqz v1, :cond_1

    const-string v1, "com.sec.shealth.action.STEALTH_MODE"

    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v5, "pair"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v5, "pair"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->action:Ljava/lang/String;

    const-string v5, "com.sec.shealth.action.COACH"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    const-string v2, "com.sec.android.app.shealth.home.HomeActivity"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "com.sec.shealth.HomeActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v3

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    if-eqz v2, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setScrimColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawerQuick(Landroid/view/View;)V

    goto :goto_0

    :cond_6
    move v2, v4

    goto :goto_1

    :cond_7
    iget-boolean v6, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->isPlugin:Z

    if-eqz v6, :cond_c

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_b

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v6, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x4000000

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string/jumbo v0, "showOnStart"

    invoke-virtual {v6, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "com.sec.android.app.shealth.home.HomeActivity"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "com.sec.shealth.HomeActivity"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "fromHome"

    invoke-virtual {v6, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_8
    const-string v0, "com.sec.android.app.shealth.home.HomeActivity"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "com.sec.shealth.HomeActivity"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string/jumbo v0, "navigating_to_home"

    invoke-virtual {v6, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_a

    :goto_2
    const-string v0, "first_visible"

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v6, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "top"

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->checkActionForLogging(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    goto :goto_2

    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->action_not_defined_for_this_item:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    goto/16 :goto_0

    :cond_c
    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->activityName:Ljava/lang/Class;

    if-eqz v2, :cond_d

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->activityName:Ljava/lang/Class;

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x20000

    :try_start_0
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string/jumbo v0, "showOnStart"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->checkActionForLogging(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    :cond_d
    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->packageName:Ljava/lang/String;

    if-eqz v2, :cond_f

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->packageName:Ljava/lang/String;

    if-eqz v3, :cond_e

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$Launcher;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :cond_e
    const/high16 v0, 0x24000000

    :try_start_1
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string/jumbo v0, "showOnStart"

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->checkActionForLogging(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->action_not_defined_for_this_item:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    goto/16 :goto_0
.end method

.method private setDrawerItemContentDescription(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->ly_sub_action_title_holder:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->tvActionTitle:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->drawer_menu:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->drawer_menu:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_comma:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private tryClearingStack()V
    .locals 4

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsConfigChanged:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawerQuick(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mLauncherActivity:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private unRegisterDBListener()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->registryContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->registryContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->registryContentObserver:Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity$RegistryContentObserver;

    :cond_0
    return-void
.end method

.method private updateDrawerItemSelection()V
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x1

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->getAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuItem;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->setDrawerSelectedItemIndex(I)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.home.HomeActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0, v4, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->setDrawerSelectedItemIndex(I)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.award.AwardActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->setDrawerSelectedItemIndex(I)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.moreapps.MoreAppsMainActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->setDrawerSelectedItemIndex(I)V

    goto :goto_1
.end method

.method private updateHardKeyBoardState()V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mHardKeyBoardHiddenState:I

    return-void
.end method


# virtual methods
.method protected clearAndRefreshDrawerMenu()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuListMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->initializeDrawerMenuList()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public closeScreen()V
    .locals 0

    return-void
.end method

.method protected customizeActionBar()V
    .locals 0

    return-void
.end method

.method protected getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    return-object v0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1

    const-string/jumbo v0, "share_via_popup"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaContentInitializationListener;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public hideDrawMenu()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawers()V

    return-void
.end method

.method protected isDrawerMenuShown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawShown:Z

    return v0
.end method

.method protected launchSettingsActivity()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mBStateLossOnsaveInstance:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onBackPressed()V

    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->setInitialized(Z)V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isSHealthUpgradeNeeded(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->finishAffinity()V

    const-string v0, "BaseActivity"

    const-string v1, "isUpgradeNeeded = true, I am killing myself"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "I am killing myself - BaseActivity"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/framework/ui/base/SDKExceptionActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->finishAffinity()V

    const-string v0, "BaseActivity"

    const-string v1, "isHealthServiceOld = true, I am killing myself"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "I am killing myself - BaseActivity"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->init(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$menu;->main_activity_actions:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onDestroy()V

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mBStateLossOnsaveInstance:Z

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsConfigChanged:Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mReceiver:Lcom/sec/android/app/shealth/framework/ui/base/SettingsUpdateReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x1

    const/16 v1, 0x52

    if-ne p1, v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawerPartialShown:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawerPartialShown:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected abstract onLogSelected()V
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onStopClassName:Ljava/lang/String;

    const-string/jumbo v0, "showOnStart"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->tryClearingStack()V

    const-string/jumbo v0, "navigating_to_home"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    const-string v1, "drawer_shown"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setScrimColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawerQuick(Landroid/view/View;)V

    const-string/jumbo v0, "navigating_to_home"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_1
    const-string v0, "fromHome"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mHomeClick:Z

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerToggle:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->help:I

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getHelpItem()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.shealth.action.HELP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :goto_1
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string/jumbo v2, "onOptionsItemSelected"

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getHelpItem()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->settings_menu:I

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->launchSettingsActivity()V

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->sharevia:I

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->prepareShareView()V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onPause()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->unRegisterDBListener()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onStopClassName:Ljava/lang/String;

    const-string v0, "com.sec.android.app.shealth.home.HomeActivity"

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onStopClassName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsActivityPaused:Z

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->onActivityPause()V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerToggle:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->syncState()V

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onPostCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onResume()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    const-string v1, "drawer_shown"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->openDrawerQuick(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerToggle:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerToggle:Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/view/ActionBarDrawerToggle;->onDrawerSlide(Landroid/view/View;F)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarDefaultTitle()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    iput-boolean v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->mIsDrawMenuOpen:Z

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsDrawShown:Z

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->invalidateOptionsMenu()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->makeAllActionBarButtonsInvisible()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCustomViewVisibility(Z)V

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;->onDrawerOpened()V

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->outStateBundle:Landroid/os/Bundle;

    const-string v1, "drawer_shown"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mBStateLossOnsaveInstance:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuAdapter:Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->updateHardKeyBoardState()V

    sget v0, Lcom/sec/android/app/shealth/framework/ui/R$anim;->activity_fade_in:I

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$anim;->activity_fade_out:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->overridePendingTransition(II)V

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsActivityPaused:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setFocusOntheRootView()V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->prompt_double_tap_to_open:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->setDrawerItemContentDescription(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMenuList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v1, v0, :cond_3

    const-string v0, "BaseActivity"

    const-string/jumbo v1, "on resume, change of drawer menu"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->clearAndRefreshDrawerMenu()V

    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->updateDrawerItemSelection()V

    return-void

    :cond_4
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mIsActivityPaused:Z

    goto :goto_0
.end method

.method protected onResumeFragments()V
    .locals 2

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onResumeFragments()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->isDrawerOpen(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->clearScreensViewsList()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuList:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->refreshFocusables()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mBStateLossOnsaveInstance:Z

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    const-string v1, "drawer_shown"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->onStop()V

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mHomeClick:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onStopClassName:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showOnStart:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setScrimColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "widget"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mUserClicked:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->finish()V

    :cond_1
    return-void
.end method

.method public performPopStackAnimation()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->performFramentAnimation(Z)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMainContent:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mMainContent:Landroid/view/View;

    invoke-super {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setDrawerMenuListener(Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->mDrawerMenuListener:Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;

    return-void
.end method

.method public showDrawMenu()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->openDrawer(I)V

    return-void
.end method

.method public showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Z)V

    return-void
.end method

.method public showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    return-void
.end method

.method public showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :cond_0
    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->content_frame:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    if-eqz p3, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->performFramentAnimation(Z)V

    :cond_1
    return-void
.end method
