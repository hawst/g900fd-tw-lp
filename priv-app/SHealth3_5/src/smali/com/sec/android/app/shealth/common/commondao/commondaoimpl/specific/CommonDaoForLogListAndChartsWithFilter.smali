.class public abstract Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;
.source "CommonDaoForLogListAndChartsWithFilter.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/GoalContainable;
.implements Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperationsWithFilter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;",
        "Contract::",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;",
        ">",
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts",
        "<TT;TContract;>;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperationsWithFilter",
        "<TContract;>;",
        "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/GoalContainable;"
    }
.end annotation


# static fields
.field private static final PERIOD_COLUMN:Ljava/lang/String; = "period_column"


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field protected mContentRawQueryUri:Landroid/net/Uri;

.field protected final tableName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "timeColumnNameForCheckRules"    # Ljava/lang/String;
    .param p4, "idColumnName"    # Ljava/lang/String;
    .param p5, "keyValueColumnName"    # Ljava/lang/String;
    .param p6, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<TContract;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter<TT;TContract;>;"
    .local p7, "localContract":Ljava/util/Map;, "Ljava/util/Map<TContract;Ljava/lang/String;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndCharts;-><init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 43
    const-class v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->LOG_TAG:Ljava/lang/String;

    .line 46
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->mContentRawQueryUri:Landroid/net/Uri;

    .line 60
    iput-object p6, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->tableName:Ljava/lang/String;

    .line 61
    return-void
.end method

.method private createSQLViewForSubQuery(Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)V
    .locals 7
    .param p1, "viewName"    # Ljava/lang/String;
    .param p2, "periodForGoalFilterApply"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter<TT;TContract;>;"
    const/4 v2, 0x0

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CREATE VIEW IF NOT EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "*, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getPeriodGroupByValue(Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "period_column"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->tableName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 155
    .local v3, "createViewForSubquery":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->mContentRawQueryUri:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 159
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 160
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 164
    return-void

    .line 162
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check access to raw query and also if view is created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getInQueryString(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Ljava/util/Set;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;IZ)Ljava/lang/String;
    .locals 9
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "whereClause"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .param p7, "defaultGoalValue"    # I
    .param p9, "goalRangeInPercent"    # I
    .param p10, "goalAchievedStatus"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;ITContract;IZ)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 127
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter<TT;TContract;>;"
    .local p6, "convertedProjection":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p8, "fieldForGoalWillBeApplied":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;, "TContract;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getPeriodRangeFilter(JJ)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p5, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p5}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;->getSQLCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->getSQLCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 131
    .local v7, "completeWhereClause":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->convertCollectionToCsvString(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " FROM "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->tableName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " WHERE ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " GROUP BY "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getPeriodGroupByValue(Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "firstSelect":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SQL generated: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "period_column IN ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v6, "period_column"

    move-object v0, p0

    move/from16 v2, p7

    move-object/from16 v3, p8

    move/from16 v4, p9

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getDaySetWithCorrectGoalStatus(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;IZLjava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->convertCollectionToCsvString(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 127
    .end local v1    # "firstSelect":Ljava/lang/String;
    .end local v7    # "completeWhereClause":Ljava/lang/String;
    :cond_0
    const-string v0, ""

    goto/16 :goto_0

    :cond_1
    const-string v0, ""

    goto/16 :goto_1
.end method

.method private validateInputOfPeriodCursorWithGoalFilter(Ljava/util/Set;I)V
    .locals 3
    .param p2, "goalRangeInPercent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TContract;>;I)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter<TT;TContract;>;"
    .local p1, "projection":Ljava/util/Set;, "Ljava/util/Set<TContract;>;"
    if-nez p1, :cond_0

    .line 116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong input: projection shouldn\'t be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    if-ltz p2, :cond_1

    int-to-float v0, p2

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 120
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goalRangeInPercent should be 0~100, you set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_2
    return-void
.end method


# virtual methods
.method protected getDaySetWithCorrectGoalStatus(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;IZLjava/lang/String;)Ljava/util/Set;
    .locals 17
    .param p1, "selectQuery"    # Ljava/lang/String;
    .param p2, "defaultGoalValue"    # I
    .param p4, "goalRangeInPercent"    # I
    .param p5, "goalAchievedStatus"    # Z
    .param p6, "periodColumn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ITContract;IZ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter<TT;TContract;>;"
    .local p3, "fieldForGoalWillBeApplied":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;, "TContract;"
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 179
    .local v9, "dayFilterValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 182
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->mContentRawQueryUri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 185
    if-eqz v8, :cond_7

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 187
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getTimeColumnName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 188
    .local v14, "sampleTime":J
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v12

    .line 189
    .local v12, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-nez v12, :cond_3

    move/from16 v0, p2

    int-to-float v13, v0

    .line 190
    .local v13, "goalValue":F
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getContractColumnName(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v16

    .line 193
    .local v16, "value":F
    if-nez p4, :cond_5

    .line 194
    cmpl-float v2, v16, v13

    if-ltz v2, :cond_4

    const/4 v11, 0x1

    .line 200
    .local v11, "goalAchieved":Z
    :goto_1
    move/from16 v0, p5

    if-ne v11, v0, :cond_1

    .line 201
    move-object/from16 v0, p6

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 202
    .local v10, "dayTime":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 204
    .end local v10    # "dayTime":Ljava/lang/String;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 213
    if-eqz v8, :cond_2

    .line 214
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 216
    .end local v9    # "dayFilterValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v11    # "goalAchieved":Z
    .end local v12    # "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .end local v13    # "goalValue":F
    .end local v14    # "sampleTime":J
    .end local v16    # "value":F
    :cond_2
    :goto_2
    return-object v9

    .line 189
    .restart local v9    # "dayFilterValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v12    # "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .restart local v14    # "sampleTime":J
    :cond_3
    :try_start_1
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v13

    goto :goto_0

    .line 194
    .restart local v13    # "goalValue":F
    .restart local v16    # "value":F
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 196
    :cond_5
    const/high16 v2, 0x3f800000    # 1.0f

    move/from16 v0, p4

    int-to-float v3, v0

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    mul-float/2addr v2, v13

    cmpg-float v2, v16, v2

    if-gtz v2, :cond_6

    const/high16 v2, 0x3f800000    # 1.0f

    move/from16 v0, p4

    int-to-float v3, v0

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v2, v13

    cmpl-float v2, v16, v2

    if-ltz v2, :cond_6

    const/4 v11, 0x1

    .restart local v11    # "goalAchieved":Z
    :goto_3
    goto :goto_1

    .end local v11    # "goalAchieved":Z
    :cond_6
    const/4 v11, 0x0

    goto :goto_3

    .line 208
    .end local v12    # "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .end local v13    # "goalValue":F
    .end local v14    # "sampleTime":J
    .end local v16    # "value":F
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->LOG_TAG:Ljava/lang/String;

    const-string v3, "No dates found from getPeriodCursorWithGoalFilter()"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209
    const/4 v9, 0x0

    .line 213
    .end local v9    # "dayFilterValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v8, :cond_2

    .line 214
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 213
    .restart local v9    # "dayFilterValue":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_8

    .line 214
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v2
.end method

.method public getPeriodCursorWithGoalFilter(JJLjava/util/Set;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;ZIILcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Landroid/database/Cursor;
    .locals 17
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p6, "whereClause"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .param p8, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .param p9, "goalAchievedStatus"    # Z
    .param p10, "defaultGoalValue"    # I
    .param p11, "goalRangeInPercent"    # I
    .param p12, "periodForGoalFilterApply"    # Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/Set",
            "<TContract;>;",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            "TContract;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            "ZII",
            "Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;, "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter<TT;TContract;>;"
    .local p5, "projection":Ljava/util/Set;, "Ljava/util/Set<TContract;>;"
    .local p7, "fieldForGoalWillBeApplied":Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;, "TContract;"
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move/from16 v2, p11

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->validateInputOfPeriodCursorWithGoalFilter(Ljava/util/Set;I)V

    .line 77
    move-object/from16 v0, p5

    move-object/from16 v1, p7

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getContractColumnName(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v9

    .line 79
    .local v9, "convertedProjection":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p12

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getPeriodGroupByValue(Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "period_column"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getTimeColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getKeyValueColumnName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 82
    const-string/jumbo v3, "time_zone"

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 83
    const-string v3, "create_time"

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    const-string/jumbo v3, "update_time"

    invoke-interface {v9, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move-object/from16 v8, p6

    move/from16 v10, p10

    move-object/from16 v11, p7

    move/from16 v12, p11

    move/from16 v13, p9

    .line 86
    invoke-direct/range {v3 .. v13}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getInQueryString(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Ljava/util/Set;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;IZ)Ljava/lang/String;

    move-result-object v15

    .line 89
    .local v15, "inQuery":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "view_for_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->tableName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_with_period_for_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p12 .. p12}, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 91
    .local v16, "sqlViewName":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p12

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->createSQLViewForSubQuery(Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)V

    .line 95
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    move-object/from16 v0, p12

    if-ne v0, v3, :cond_0

    .line 96
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->contractMap:Ljava/util/Map;

    move-object/from16 v0, p7

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 101
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->convertCollectionToCsvString(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " FROM "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " WHERE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p8, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ORDER BY "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getTimeColumnName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p8 .. p8}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 107
    .local v6, "selectMain":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->mContentRawQueryUri:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 111
    .local v14, "cursor":Landroid/database/Cursor;
    return-object v14

    .line 98
    .end local v6    # "selectMain":Ljava/lang/String;
    .end local v14    # "cursor":Landroid/database/Cursor;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " GROUP BY "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p12

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getPeriodGroupByValue(Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_0

    .line 101
    :cond_1
    const-string v3, ""

    goto :goto_1
.end method
