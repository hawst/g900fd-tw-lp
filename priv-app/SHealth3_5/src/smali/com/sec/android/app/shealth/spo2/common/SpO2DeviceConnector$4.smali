.class Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$4;
.super Landroid/os/CountDownTimer;
.source "SpO2DeviceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 431
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 442
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onFinish!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$500(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$500(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;->onTimeout()V

    .line 445
    :cond_0
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 435
    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onTick: "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$500(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->mSpO2SensorListener:Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->access$500(Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/spo2/SpO2SensorListener;->onTick(J)V

    .line 438
    :cond_0
    return-void
.end method
