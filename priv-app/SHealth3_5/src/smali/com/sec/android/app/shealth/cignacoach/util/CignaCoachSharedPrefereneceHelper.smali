.class public Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;
.super Ljava/lang/Object;
.source "CignaCoachSharedPrefereneceHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper$SharedPreferencesKeys;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

.field public static prefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->TAG:Ljava/lang/String;

    .line 17
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    .line 18
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    return-void
.end method

.method public static clearAssessmentWeightHeightValue()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 142
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setAssessmentHeightUnit(I)V

    .line 143
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setAssessmentWeightUnit(I)V

    .line 144
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setAssessmentHeightValue(F)V

    .line 145
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setAssessmentWeightValue(F)V

    .line 146
    return-void
.end method

.method public static getAssessmentHeightUnit()I
    .locals 3

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_ASSESSMENT_HEIGHT_UNIT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getAssessmentHeightValue()F
    .locals 3

    .prologue
    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_ASSESSMENT_HEIGHT_VALUE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getAssessmentWeightUnit()I
    .locals 3

    .prologue
    .line 130
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_ASSESSMENT_WEIGHT_UNIT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getAssessmentWeightValue()F
    .locals 3

    .prologue
    .line 138
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_ASSESSMENT_WEIGHT_VALUE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static getCignaIntroMsg()Ljava/lang/String;
    .locals 3

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_INTRO_MSG"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCignaTermsOfUseVerionCode(Landroid/content/Context;)F
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 201
    if-nez p0, :cond_0

    .line 204
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v2, "CIGNA_TERMS_OF_USE_VERSION_CODE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getFloat(Ljava/lang/String;F)F

    move-result v0

    goto :goto_0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefs:Landroid/content/SharedPreferences;

    .line 24
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "PREF_VERSION"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 25
    return-void
.end method

.method public static isAgreeTermsOfUse()Z
    .locals 3

    .prologue
    .line 193
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CINGA_AGREE_TERMS_OF_USE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isCignaCoachCompleteAssessment()Z
    .locals 3

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_COMPLETE_ASSESSMENT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isCignaCoachInited()Z
    .locals 3

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "INITIALIZATION_CIGNA_COACH"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isFirstLoadingComplete()Z
    .locals 3

    .prologue
    .line 106
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_FIRST_LOADING_COMPLETE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static setAssessmentHeightUnit(I)V
    .locals 3
    .param p0, "heightUnit"    # I

    .prologue
    .line 110
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_ASSESSMENT_HEIGHT_UNIT"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public static setAssessmentHeightValue(F)V
    .locals 3
    .param p0, "height"    # F

    .prologue
    .line 118
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_ASSESSMENT_HEIGHT_VALUE"

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 119
    return-void
.end method

.method public static setAssessmentWeightUnit(I)V
    .locals 3
    .param p0, "weightUnit"    # I

    .prologue
    .line 126
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_ASSESSMENT_WEIGHT_UNIT"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 127
    return-void
.end method

.method public static setAssessmentWeightValue(F)V
    .locals 3
    .param p0, "weight"    # F

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_ASSESSMENT_WEIGHT_VALUE"

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 135
    return-void
.end method

.method public static setCignaIntroMsg(Ljava/lang/String;)V
    .locals 2
    .param p0, "introMsg"    # Ljava/lang/String;

    .prologue
    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_INTRO_MSG"

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public static setCingaTermsOfUseVersionCode(Landroid/content/Context;F)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "termsOfUseVersionCode"    # F

    .prologue
    .line 208
    if-eqz p0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_TERMS_OF_USE_VERSION_CODE"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static setFirstLoadingComplete(Z)V
    .locals 3
    .param p0, "isComplete"    # Z

    .prologue
    .line 102
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_FIRST_LOADING_COMPLETE"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 103
    return-void
.end method

.method public static setIsAgreeTermsOfUse()V
    .locals 3

    .prologue
    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CINGA_AGREE_TERMS_OF_USE"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 198
    return-void
.end method

.method public static setIsCignaCoachInited(Z)V
    .locals 3
    .param p0, "isInited"    # Z

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "INITIALIZATION_CIGNA_COACH"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method public static setValue(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "val"    # Ljava/lang/Object;

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 30
    return-void
.end method

.method public static setisCignaCoachCompleteAssessment(Z)V
    .locals 3
    .param p0, "isComplete"    # Z

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_COMPLETE_ASSESSMENT"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 58
    return-void
.end method

.method public static setisCignaCoachScoreViewMessageMode(Z)V
    .locals 3
    .param p0, "isMessageMode"    # Z

    .prologue
    .line 189
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->prefManager:Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;

    const-string v1, "CIGNA_COACH_SCORE_VIEW_MESSAGE_MODE"

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 190
    return-void
.end method
