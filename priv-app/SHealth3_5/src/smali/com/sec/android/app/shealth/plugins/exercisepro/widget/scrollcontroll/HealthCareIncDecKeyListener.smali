.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;
.super Ljava/lang/Object;
.source "HealthCareIncDecKeyListener.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static volatile sIsPressed:Z


# instance fields
.field private mDecreaseButton:Landroid/view/View;

.field private mIncreaseButton:Landroid/view/View;

.field private mTouchedView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->TAG:Ljava/lang/String;

    .line 9
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->sIsPressed:Z

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "increaseButton"    # Landroid/view/View;
    .param p2, "decreaseButton"    # Landroid/view/View;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mIncreaseButton:Landroid/view/View;

    .line 15
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mDecreaseButton:Landroid/view/View;

    .line 16
    return-void
.end method

.method private performOnClick()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mIncreaseButton:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mIncreaseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    .line 58
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mDecreaseButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    goto :goto_0
.end method


# virtual methods
.method public beforeTouchDownEvent()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 20
    const/16 v2, 0x42

    if-eq p2, v2, :cond_1

    const/16 v2, 0x17

    if-eq p2, v2, :cond_1

    move v0, v1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 21
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 48
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->sIsPressed:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    if-ne p1, v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 23
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    if-ne p1, v2, :cond_3

    .line 24
    invoke-virtual {p1, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->performOnClick()V

    goto :goto_0

    .line 28
    :cond_3
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->sIsPressed:Z

    if-nez v2, :cond_4

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->beforeTouchDownEvent()V

    .line 30
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 31
    sput-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->sIsPressed:Z

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    .line 33
    invoke-virtual {p1, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->performOnClick()V

    goto :goto_0

    .line 40
    :pswitch_1
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->sIsPressed:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    if-ne p1, v0, :cond_4

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 42
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->sIsPressed:Z

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    :cond_4
    move v0, v1

    .line 50
    goto :goto_0

    .line 21
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public stopQuickIncDec()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 64
    :cond_0
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->sIsPressed:Z

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/scrollcontroll/HealthCareIncDecKeyListener;->mTouchedView:Landroid/view/View;

    .line 66
    return-void
.end method
