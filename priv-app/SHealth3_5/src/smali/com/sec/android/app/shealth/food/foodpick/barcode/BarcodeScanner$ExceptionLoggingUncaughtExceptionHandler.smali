.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingUncaughtExceptionHandler;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ExceptionLoggingUncaughtExceptionHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$1;

    .prologue
    .line 585
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ExceptionLoggingUncaughtExceptionHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 588
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 589
    return-void
.end method
