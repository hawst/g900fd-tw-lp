.class public Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;
.super Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.source "HeartrateGraphFragmentSIC.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$4;
    }
.end annotation


# static fields
.field private static final FRAGMENT_SWITCHING_DELAY:I = 0xc8

.field public static final GRAPH_FONT:Ljava/lang/String; = "font/Roboto-Light.ttf"

.field private static MARKING_COUNT:I = 0x0

.field private static MAX_Y:F = 0.0f

.field private static final MIN_Y:F = 0.0f

.field private static final PATTERN_24_HOURS:Ljava/lang/String; = "HH"

.field private static final PATTERN_DAY_FORMAT:Ljava/lang/String; = "dd"

.field private static final PATTERN_MONTH_FORMAT:Ljava/lang/String; = "MM"

.field private static final PATTERN_TIME:Ljava/lang/String; = "HH:mm"

.field private static volatile mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# instance fields
.field private density:F

.field private isConfigurationChanged:Z

.field private mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

.field private mDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field

.field private mHeartrateActivity:Landroid/app/Activity;

.field private mHeartrateDataCount:I

.field private mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

.field private mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

.field private mLegendView:Landroid/view/View;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

.field private switchToHeartrateSummary:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const/high16 v0, 0x42f00000    # 120.0f

    sput v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MAX_Y:F

    .line 85
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MARKING_COUNT:I

    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;-><init>()V

    .line 86
    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 104
    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->switchToHeartrateSummary:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->showSummarayFragment()V

    return-void
.end method

.method private isDataCountChanged()Z
    .locals 2

    .prologue
    .line 212
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v0

    .line 213
    .local v0, "heartrateDataCount":I
    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateDataCount:I

    if-ne v0, v1, :cond_0

    .line 214
    const/4 v1, 0x0

    .line 217
    :goto_0
    return v1

    .line 216
    :cond_0
    iput v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateDataCount:I

    .line 217
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 268
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_1

    .line 270
    const-string v1, "HEARTRATE_CHARTTAB"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 282
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 283
    return-void

    .line 272
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_2

    .line 274
    const-string v1, "HEARTRATE_CHARTTAB"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 276
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_0

    .line 278
    const-string v1, "HEARTRATE_CHARTTAB"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private setChartData(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 6
    .param p1, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 462
    new-instance v4, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 463
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getHearteRateGraphDatasHourType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;

    move-result-object v2

    .line 466
    .local v2, "heartrateDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 467
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    .line 471
    :goto_0
    const/4 v1, 0x0

    .line 472
    .local v1, "dataSize":I
    if-eqz v2, :cond_0

    .line 473
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 474
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_4

    .line 475
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>()V

    .line 476
    .local v0, "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setTime(J)V

    .line 477
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v4, :cond_2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getMeanHeartRate()F

    move-result v4

    float-to-double v4, v4

    :goto_2
    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setValue(D)V

    .line 478
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v4, :cond_3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getMeanHeartRate()F

    move-result v4

    float-to-int v4, v4

    :goto_3
    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setMaxY(I)V

    .line 479
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 469
    .end local v0    # "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 477
    .restart local v0    # "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    .restart local v1    # "dataSize":I
    .restart local v3    # "i":I
    :cond_2
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v4

    float-to-double v4, v4

    goto :goto_2

    .line 478
    :cond_3
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v4

    float-to-int v4, v4

    goto :goto_3

    .line 482
    .end local v0    # "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    .line 483
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 486
    return-void
.end method

.method private setMaxY(I)V
    .locals 2
    .param p1, "compareY"    # I

    .prologue
    .line 536
    int-to-float v0, p1

    sget v1, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MAX_Y:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 537
    div-int/lit8 p1, p1, 0xa

    .line 538
    add-int/lit8 p1, p1, 0x2

    .line 539
    mul-int/lit8 p1, p1, 0xa

    .line 540
    int-to-float v0, p1

    sput v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MAX_Y:F

    .line 542
    :cond_0
    return-void
.end method

.method private setMultiChartData(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 15
    .param p1, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 493
    const-wide/16 v10, 0x0

    .line 494
    .local v10, "valueMax":D
    const-wide/16 v12, 0x0

    .line 495
    .local v12, "valueMin":D
    const-wide/16 v8, 0x0

    .line 496
    .local v8, "valueAvg":D
    const-wide/16 v6, 0x0

    .line 498
    .local v6, "time":J
    new-instance v14, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;

    invoke-direct {v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;-><init>()V

    iput-object v14, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    .line 499
    new-instance v14, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v14, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 500
    iget-object v14, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getHearteRateGraphDatasByType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;

    move-result-object v3

    .line 501
    .local v3, "heartDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    const/4 v2, 0x0

    .line 502
    .local v2, "dataSize":I
    if-eqz v3, :cond_0

    .line 503
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 505
    :cond_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_1

    .line 507
    new-instance v1, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;-><init>()V

    .line 508
    .local v1, "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;
    new-instance v5, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    invoke-direct {v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>()V

    .line 511
    .local v5, "tdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getMaxData()F

    move-result v14

    float-to-double v10, v14

    .line 512
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getMinData()F

    move-result v14

    float-to-double v12, v14

    .line 513
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getAverage()F

    move-result v14

    float-to-double v8, v14

    .line 514
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v6

    .line 516
    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->setTime(J)V

    .line 517
    invoke-virtual {v1, v10, v11}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->setValueHigh(D)V

    .line 518
    invoke-virtual {v1, v12, v13}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->setValueLow(D)V

    .line 520
    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setTime(J)V

    .line 521
    invoke-virtual {v5, v8, v9}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setValue(D)V

    .line 523
    double-to-int v14, v10

    invoke-direct {p0, v14}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setMaxY(I)V

    .line 525
    iget-object v14, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v14, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 526
    iget-object v14, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    invoke-virtual {v14, v1}, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;)V

    .line 505
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 529
    .end local v1    # "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;
    .end local v5    # "tdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    :cond_1
    iget-object v14, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 530
    iget-object v14, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 532
    return-void
.end method

.method private showSummarayFragment()V
    .locals 5

    .prologue
    .line 245
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .line 246
    .local v1, "heartrateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->switchFragmentToSummary()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    return-void

    .line 247
    .end local v1    # "heartrateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of HeartrateActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method protected createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .locals 2
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 450
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;-><init>()V

    .line 452
    .local v0, "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_0

    .line 453
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setChartData(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 457
    :goto_0
    return-object v0

    .line 455
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setMultiChartData(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0
.end method

.method protected customizeActionBarDisable(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 1
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 227
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 228
    return-void
.end method

.method protected customizeActionBarShow(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 1
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 231
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 232
    return-void
.end method

.method protected customizeChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V
    .locals 4
    .param p1, "interaction"    # Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    .line 288
    const/high16 v0, 0x40e00000    # 7.0f

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 289
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 290
    invoke-virtual {p1, v2, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalLimit(II)V

    .line 291
    invoke-virtual {p1, v2, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalStep(II)V

    .line 296
    :goto_0
    return-void

    .line 293
    :cond_0
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 294
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_0
.end method

.method protected customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 12
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/16 v11, 0xff

    const/4 v10, 0x1

    const/high16 v9, 0x41f80000    # 31.0f

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 643
    const-string v5, "HeartrateGraphFragmentSIC"

    const-string v6, "initChartStyle"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    new-instance v3, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 646
    .local v3, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a04fe

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 647
    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 648
    invoke-virtual {v3, v11, v7, v7, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 649
    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 650
    const-string/jumbo v5, "sec-roboto-light"

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 652
    new-instance v4, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 653
    .local v4, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a04fe

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 654
    invoke-virtual {v4, v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 655
    invoke-virtual {v4, v11, v7, v7, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 656
    invoke-virtual {v4, v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 657
    const-string/jumbo v5, "sec-roboto-light"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 658
    invoke-virtual {p1, v4, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 661
    const/high16 v5, 0x3f800000    # 1.0f

    iget v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->density:F

    mul-float/2addr v5, v6

    invoke-virtual {p1, v5, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 662
    invoke-virtual {p1, v7, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 663
    const v5, -0xcbb1ec

    invoke-virtual {p1, v5, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 664
    invoke-virtual {p1, v3, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 671
    const/high16 v5, 0x41800000    # 16.0f

    iget v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->density:F

    mul-float/2addr v5, v6

    invoke-virtual {p1, v8, v8, v8, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 674
    const/4 v1, 0x0

    .line 677
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v5, v6, :cond_0

    .line 678
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0203c2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 680
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->density:F

    mul-float/2addr v5, v9

    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 681
    const-string v5, "HH"

    invoke-virtual {p1, v5, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 688
    :goto_0
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 689
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 690
    invoke-virtual {p1, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerAnimation(Z)V

    .line 691
    const-wide/16 v5, 0x3e8

    invoke-virtual {p1, v5, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerAnimationTime(J)V

    .line 692
    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getTimeDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 694
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v2

    .line 695
    .local v2, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v5, 0x50

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 696
    const/high16 v5, 0x41400000    # 12.0f

    iget v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->density:F

    mul-float/2addr v5, v6

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 698
    return-void

    .line 683
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0203c2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 685
    iget v5, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->density:F

    mul-float/2addr v5, v9

    invoke-virtual {p1, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_0
.end method

.method protected customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 7
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/16 v6, 0x140

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 702
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 704
    const/4 v1, 0x0

    .line 705
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 706
    .local v2, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 707
    const/16 v3, 0xff

    invoke-virtual {v2, v3, v5, v5, v5}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 708
    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 709
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v3, v4, :cond_1

    .line 710
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 711
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    if-gt v3, v6, :cond_0

    .line 712
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0501

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 713
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0500

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 714
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04ff

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 715
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 719
    :goto_0
    const-string v3, "HH:mm"

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 720
    const-string v3, "HH"

    invoke-virtual {p1, v3, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 733
    :goto_1
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 734
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 735
    return-void

    .line 717
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    const/high16 v3, 0x426c0000    # 59.0f

    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->density:F

    mul-float/2addr v3, v4

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_0

    .line 722
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 723
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->compatScreenWidthDp:I

    if-gt v3, v6, :cond_2

    .line 724
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04fb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 725
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04fa

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 726
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04f9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 727
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    goto :goto_1

    .line 729
    :cond_2
    const/high16 v3, 0x41f80000    # 31.0f

    iget v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->density:F

    mul-float/2addr v3, v4

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_1
.end method

.method protected getContentUriList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 756
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    return-object v0
.end method

.method protected getLegendButtonBackgroundResourceId()I
    .locals 1

    .prologue
    .line 761
    const v0, 0x7f02078d

    return v0
.end method

.method protected getLegendMarks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 767
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getNoDataIcon()I
    .locals 1

    .prologue
    .line 750
    const v0, 0x7f0205b7

    return v0
.end method

.method protected getTimeDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;
    .locals 3
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 358
    const-string v0, "HeartrateGraphFragmentSIC"

    const-string v1, "getTimeDateFormat"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 360
    const-string v0, "dd"

    .line 364
    :goto_0
    return-object v0

    .line 361
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_1

    .line 362
    const-string v0, "MM"

    goto :goto_0

    .line 363
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    .line 364
    const-string v0, "HH"

    goto :goto_0

    .line 366
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal periodType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected getYAxisLabelTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 739
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0900d2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 143
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initDateBar()V

    .line 144
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 145
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 146
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 147
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 6

    .prologue
    const-wide/16 v4, 0xc8

    .line 312
    const-string v2, "HeartrateGraphFragmentSIC"

    const-string v3, "initGraphLegendArea"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 314
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03014b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f080596

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->switchToHeartrateSummary:Landroid/widget/ImageButton;

    .line 319
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->switchToHeartrateSummary:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$2;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 340
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->switchToHeartrateSummary:Landroid/widget/ImageButton;

    invoke-static {v4, v5, v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->disableClickFor(JLandroid/view/View;)V

    .line 343
    :try_start_0
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$3;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mLegendView:Landroid/view/View;

    return-object v2

    .line 350
    :catch_0
    move-exception v0

    .line 351
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 3
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 254
    const-string v1, "HeartrateGraphFragmentSIC"

    const-string v2, "initGraphView"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iput-object p3, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 256
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 257
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v0

    .line 258
    .local v0, "view":Landroid/view/View;
    const/high16 v1, 0x42f00000    # 120.0f

    sput v1, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MAX_Y:F

    .line 259
    return-object v0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 300
    const-string v0, "HeartrateGraphFragmentSIC"

    const-string v1, "initInformationArea"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    return-object v0
.end method

.method protected initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 13
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    .prologue
    .line 552
    new-instance v3, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 553
    .local v3, "lineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0062

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 554
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 555
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x106000c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 556
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 557
    const-string v9, "font/Roboto-Light.ttf"

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 559
    new-instance v2, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 560
    .local v2, "lineLabelStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0063

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 561
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 562
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x106000c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 563
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 564
    const-string v9, "font/Roboto-Light.ttf"

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 566
    iget-object v9, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_1

    .line 568
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203c0

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 570
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 572
    .local v4, "normalBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203c1

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    move-object v9, v0

    .line 574
    check-cast v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 576
    .local v1, "handlerOverBitmap":Landroid/graphics/Bitmap;
    new-instance v8, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 578
    .local v8, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    sget v12, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MAX_Y:F

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 579
    invoke-virtual {v8, v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 580
    invoke-virtual {v8, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 581
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 582
    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 583
    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 584
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v5, v9, Landroid/util/DisplayMetrics;->density:F

    .line 585
    .local v5, "scale":F
    const/high16 v9, 0x40a00000    # 5.0f

    mul-float/2addr v9, v5

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 587
    iget-object v9, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_0

    .line 588
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f07008a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 591
    :cond_0
    const/high16 v9, 0x40a00000    # 5.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 592
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f07008b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalOverColor(I)V

    .line 593
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setEnableDisconnectByTime(Z)V

    .line 595
    invoke-virtual {p1, v8}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 638
    .end local v8    # "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    :goto_0
    return-void

    .line 599
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "handlerOverBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "normalBitmap":Landroid/graphics/Bitmap;
    .end local v5    # "scale":F
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203c9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 600
    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 602
    .restart local v4    # "normalBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203ca

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    move-object v9, v0

    .line 603
    check-cast v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 605
    .restart local v1    # "handlerOverBitmap":Landroid/graphics/Bitmap;
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;

    invoke-direct {v6}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;-><init>()V

    .line 606
    .local v6, "seriesCandleStyle":Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;
    new-instance v7, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 609
    .local v7, "seriesLineStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    sget v12, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MAX_Y:F

    invoke-virtual {v6, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 610
    invoke-virtual {v6, v3}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 611
    invoke-virtual {v6, v2}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 614
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setValueMarkingVisible(Z)V

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v5, v9, Landroid/util/DisplayMetrics;->density:F

    .line 618
    .restart local v5    # "scale":F
    const/high16 v9, 0x40c00000    # 6.0f

    mul-float/2addr v9, v5

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setShadowWidth(F)V

    .line 619
    const/high16 v9, 0x40000000    # 2.0f

    mul-float/2addr v9, v5

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setShadowThickness(F)V

    .line 620
    const-string v9, "#8dd949"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setShadowColor(I)V

    .line 621
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setBodyWidth(F)V

    .line 624
    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    sget v12, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MAX_Y:F

    invoke-virtual {v7, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 625
    invoke-virtual {v7, v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 626
    invoke-virtual {v7, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 627
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 628
    invoke-virtual {v7, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 629
    invoke-virtual {v7, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 630
    const/high16 v9, 0x40a00000    # 5.0f

    mul-float/2addr v9, v5

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 631
    const-string v9, "#009082"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 632
    const/high16 v9, 0x40a00000    # 5.0f

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 634
    invoke-virtual {p1, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 635
    invoke-virtual {p1, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    goto/16 :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 773
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onAttach(Landroid/app/Activity;)V

    .line 774
    check-cast p1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateActivity:Landroid/app/Activity;

    .line 776
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 236
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->onConfiguarationLanguageChanged()V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->updateStringOnLocaleChange()V

    .line 240
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isConfigurationChanged:Z

    .line 241
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 109
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setHasOptionsMenu(Z)V

    .line 112
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 115
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v2, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 117
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 118
    const/high16 v2, 0x20000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 119
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->startActivity(Landroid/content/Intent;)V

    .line 121
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 123
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateDataCount:I

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a004d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->density:F

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "HEARTRATE_CHARTTAB"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 129
    .local v1, "mPeriod":I
    if-ne v1, v5, :cond_1

    .line 131
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 138
    :goto_0
    return-void

    .line 133
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 135
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0

    .line 137
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 208
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDestroyView()V

    .line 209
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 157
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onResume()V

    .line 158
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mHeartrateActivity:Landroid/app/Activity;

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 159
    .local v3, "mPref":Landroid/content/SharedPreferences;
    const-string v4, "FromLogScreen"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->updateTagOnResume()V

    .line 161
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 162
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "FromLogScreen"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 163
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 166
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .line 167
    .local v2, "heartrateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->customizeActionBarDisable(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isConfigurationChanged:Z

    if-nez v4, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isDataCountChanged()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->initGeneralView()V

    .line 173
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->switchToHeartrateSummary:Landroid/widget/ImageButton;

    const v5, 0x7f09005f

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 174
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->isConfigurationChanged:Z

    .line 177
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->switchToHeartrateSummary:Landroid/widget/ImageButton;

    if-eqz v4, :cond_3

    .line 179
    :try_start_1
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$1;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;)V

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 190
    :cond_3
    :goto_0
    return-void

    .line 168
    .end local v2    # "heartrateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v4, Ljava/lang/ClassCastException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-class v6, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " must be instance of HeartrateActivity"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 186
    .end local v0    # "e":Ljava/lang/ClassCastException;
    .restart local v2    # "heartrateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    :catch_1
    move-exception v0

    .line 187
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 5

    .prologue
    .line 195
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onStop()V

    .line 197
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    .line 198
    .local v1, "heartrateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->customizeActionBarShow(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    return-void

    .line 199
    .end local v1    # "heartrateActivity":Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of HeartrateActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
    .locals 13
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .prologue
    .line 379
    const/4 v1, 0x0

    .line 380
    .local v1, "level":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_4

    .line 381
    const/4 v1, 0x0

    .line 388
    :cond_0
    :goto_0
    const-wide/16 v9, 0x0

    .line 389
    .local v9, "selectedTime":J
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_6

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v9

    .line 394
    :goto_1
    const-wide/16 v7, 0x0

    .line 395
    .local v7, "dataToShow":J
    const-wide/16 v11, 0x0

    .line 396
    .local v11, "startTime":J
    sget v5, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->MARKING_COUNT:I

    .line 397
    .local v5, "markingCount":I
    const/4 v4, 0x1

    .line 398
    .local v4, "intervel":I
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 402
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC$4;->$SwitchMap$com$sec$android$app$shealth$framework$ui$graph$PeriodH:[I

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 419
    const-string v0, "%Y-%m"

    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/heartrate/utils/HeartRatePluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 420
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 421
    :cond_2
    const-wide v2, 0x59cce4400L

    sub-long v11, v7, v2

    .line 422
    const/16 v5, 0xc

    .line 425
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 427
    .local v6, "calendar":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v2

    cmp-long v0, v7, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v0, v2, :cond_3

    .line 429
    invoke-virtual {v6, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 430
    const/16 v0, 0xb

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 431
    const/16 v0, 0xc

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 432
    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 433
    const/16 v0, 0xe

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 434
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 436
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 437
    long-to-double v2, v11

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 438
    long-to-double v2, v7

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_c

    .line 441
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 447
    :goto_3
    return-void

    .line 382
    .end local v4    # "intervel":I
    .end local v5    # "markingCount":I
    .end local v6    # "calendar":Ljava/util/Calendar;
    .end local v7    # "dataToShow":J
    .end local v9    # "selectedTime":J
    .end local v11    # "startTime":J
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_5

    .line 383
    const/4 v1, 0x2

    goto/16 :goto_0

    .line 384
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_0

    .line 385
    const/4 v1, 0x5

    goto/16 :goto_0

    .line 392
    .restart local v9    # "selectedTime":J
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->getSelectedDateInChart()J

    move-result-wide v9

    :goto_4
    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v9

    goto :goto_4

    .line 405
    .restart local v4    # "intervel":I
    .restart local v5    # "markingCount":I
    .restart local v7    # "dataToShow":J
    .restart local v11    # "startTime":J
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_9

    const-string v0, "%Y-%m-%d"

    :goto_5
    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/heartrate/utils/HeartRatePluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 406
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 407
    :cond_8
    const-wide/32 v2, 0x2160ec0

    sub-long v2, v7, v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartHourToMillis(J)J

    move-result-wide v11

    .line 408
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setScrollRangeDepthLevel(I)V

    .line 409
    const/16 v4, 0x3c

    .line 410
    const/16 v5, 0xd

    .line 411
    goto/16 :goto_2

    .line 405
    :cond_9
    const-string v0, "%Y-%m"

    goto :goto_5

    .line 413
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v2, :cond_b

    const-string v0, "%Y-%m-%d"

    :goto_6
    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/heartrate/utils/HeartRatePluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 414
    const-wide/16 v2, 0x0

    cmp-long v0, v7, v2

    if-gtz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateGraphFragmentSIC;->mSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 415
    :cond_a
    const-wide/32 v2, 0x19bfcc00

    sub-long v11, v7, v2

    .line 416
    const/4 v5, 0x7

    .line 417
    goto/16 :goto_2

    .line 413
    :cond_b
    const-string v0, "%Y-%m"

    goto :goto_6

    .line 443
    .restart local v6    # "calendar":Ljava/util/Calendar;
    :cond_c
    const/4 v0, 0x0

    const/4 v2, 0x6

    invoke-virtual {p1, v0, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 444
    const/4 v0, 0x1

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    goto/16 :goto_3

    .line 402
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 152
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 153
    return-void
.end method

.method protected updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 746
    return-void
.end method
