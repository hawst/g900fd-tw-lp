.class public Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;
.super Ljava/lang/Object;
.source "LapClock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$1;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;
    }
.end annotation


# static fields
.field private static HOUR:J

.field private static MAX_CLOCK_MILLIS:J

.field private static MINUTE:J

.field private static SECOND:J


# instance fields
.field currentTimeMillis:J

.field elapsedTimeMillis:J

.field elapsedTimeOffsetMillis:J

.field hour:J

.field private mLapClocktimer:Ljava/util/Timer;

.field mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

.field private mTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

.field minute:J

.field resumeTimeMillis:J

.field second:J

.field startTimeMillis:J

.field state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x3c

    .line 7
    const-wide/16 v0, 0x3e8

    sput-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->SECOND:J

    .line 8
    sget-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->SECOND:J

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->MINUTE:J

    .line 9
    sget-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->MINUTE:J

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->HOUR:J

    .line 11
    const-wide/16 v0, 0x64

    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->HOUR:J

    mul-long/2addr v0, v2

    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->MINUTE:J

    mul-long/2addr v0, v2

    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->SECOND:J

    mul-long/2addr v0, v2

    sput-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->MAX_CLOCK_MILLIS:J

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->NULL:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    .line 40
    return-void
.end method


# virtual methods
.method public getElapsedTime()J
    .locals 6

    .prologue
    .line 47
    const-wide/16 v0, 0x0

    .line 48
    .local v0, "elapsedTime":J
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->PAUSE:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    if-ne v2, v3, :cond_1

    .line 49
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeOffsetMillis:J

    .line 57
    :cond_0
    :goto_0
    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->SECOND:J

    div-long v2, v0, v2

    return-wide v2

    .line 50
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    sget-object v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->RUN:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    if-ne v2, v3, :cond_0

    .line 51
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 52
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeOffsetMillis:J

    add-long v0, v2, v4

    goto :goto_0

    .line 54
    :cond_2
    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->startTimeMillis:J

    sub-long v0, v2, v4

    goto :goto_0
.end method

.method public getStartTimeMillis()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->startTimeMillis:J

    return-wide v0
.end method

.method public pause()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->RUN:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    if-eq v0, v1, :cond_0

    .line 110
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 98
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    .line 99
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

    .line 101
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->startTimeMillis:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 102
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    .line 104
    :cond_3
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    .line 105
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeOffsetMillis:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeOffsetMillis:J

    .line 109
    :goto_1
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->PAUSE:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    goto :goto_0

    .line 107
    :cond_4
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeOffsetMillis:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    iget-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->startTimeMillis:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeOffsetMillis:J

    goto :goto_1
.end method

.method public registerListener(Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

    .line 143
    return-void
.end method

.method public resume()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x3e8

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->PAUSE:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    if-eq v0, v1, :cond_0

    .line 123
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 117
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    .line 119
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 121
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    .line 122
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->RUN:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    goto :goto_0
.end method

.method public setCurentTimeMillis()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    .line 62
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 63
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeOffsetMillis:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    .line 67
    :goto_0
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    .line 68
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    .line 70
    :cond_0
    :goto_1
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->MAX_CLOCK_MILLIS:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 71
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->MAX_CLOCK_MILLIS:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    goto :goto_1

    .line 65
    :cond_1
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->currentTimeMillis:J

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->startTimeMillis:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    goto :goto_0

    .line 73
    :cond_2
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->SECOND:J

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->second:J

    .line 74
    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    sget-wide v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->SECOND:J

    rem-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeMillis:J

    .line 75
    return-void
.end method

.method public start()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0x0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->NULL:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    if-eq v0, v1, :cond_0

    .line 90
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->startTimeMillis:J

    .line 82
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->elapsedTimeOffsetMillis:J

    .line 83
    iput-wide v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->resumeTimeMillis:J

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    if-nez v0, :cond_1

    .line 85
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    .line 87
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->RUN:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->NULL:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    if-ne v0, v1, :cond_0

    .line 139
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 131
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mLapClocktimer:Ljava/util/Timer;

    .line 132
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mTimerTask:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;

    .line 134
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->second:J

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->minute:J

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->hour:J

    .line 138
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;->NULL:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->state:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$STATE;

    goto :goto_0
.end method

.method public unregisterListener()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

    .line 147
    return-void
.end method
