.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;
.super Ljava/lang/Object;
.source "RestoreActivity.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->startRestore()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field baseValue:I

.field denominatorValue:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)V
    .locals 1

    .prologue
    .line 576
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 578
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->baseValue:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->denominatorValue:I

    return-void
.end method


# virtual methods
.method public onFinished(II)V
    .locals 9
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I

    .prologue
    const-wide/16 v7, 0x64

    const/16 v6, 0x64

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Restore Completed. Type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    const/4 v0, 0x0

    .line 656
    .local v0, "cignaStatus":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v2, 0x7f0808b1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 657
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 659
    if-nez p2, :cond_4

    .line 660
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[SUCCESS] Restore Completed."

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCignaBackupRestoreEnable:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 662
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[SUCCESS] Restore Completed. calling cigna"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const-string v2, "RESTORE"

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->cignaBackupRestore(Ljava/lang/String;)Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Ljava/lang/String;)Z

    move-result v0

    .line 732
    :goto_0
    return-void

    .line 665
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "[SUCCESS] Restore Completed. cigna disabled"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 668
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 669
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 672
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 673
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v4, 0x7f090d15

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 676
    :cond_2
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;)V

    invoke-virtual {v1, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 688
    :cond_3
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$5;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;)V

    invoke-virtual {v1, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 702
    :cond_4
    const/16 v1, 0x8

    if-ne p2, v1, :cond_5

    .line 704
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "3.0 Restore onFinished() ShealthContentManager.ActivityListener.ERROR_PROFILE_NOT_EXIST"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 707
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->NODATA:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    goto/16 :goto_0

    .line 712
    :cond_5
    const/16 v1, 0xb

    if-ne p2, v1, :cond_6

    .line 714
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 715
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->requestCheckValication(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 719
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "3.0 Restore received : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->stopBackupOrRestore(Z)V

    .line 722
    const/16 v1, 0x10

    if-ne p2, v1, :cond_7

    .line 723
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isDeviceStorageLow:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->DEVICE_STORAGE_LOW:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    goto/16 :goto_0

    .line 726
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 727
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    sget-object v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;->FAIL_NETWORK_ERROR:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->showRestorePopup(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$RestoreStatus;)V

    goto/16 :goto_0
.end method

.method public onProgress(II)V
    .locals 5
    .param p1, "dataSyncType"    # I
    .param p2, "percent"    # I

    .prologue
    .line 635
    const/4 v0, 0x0

    .line 636
    .local v0, "percentage":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCBBackup:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 637
    const-wide v1, 0x3feccccccccccccdL    # 0.9

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->denominatorValue:I

    div-int v3, p2, v3

    int-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-int v0, v1

    .line 642
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->baseValue:I

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 646
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 647
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->baseValue:I

    add-int/2addr v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v4, 0x7f090d15

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    :cond_1
    return-void

    .line 639
    :cond_2
    const-wide v1, 0x3fee666666666666L    # 0.95

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->denominatorValue:I

    div-int v3, p2, v3

    int-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-int v0, v1

    goto :goto_0
.end method

.method public onStarted(IILjava/lang/String;)V
    .locals 6
    .param p1, "dataSyncType"    # I
    .param p2, "error"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0xc8

    const/4 v3, 0x1

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore Started. Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    if-nez p2, :cond_1

    .line 587
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mCBBackup:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->baseValue:I

    .line 589
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->denominatorValue:I

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 594
    :cond_1
    const/16 v0, 0x10

    if-ne p2, v0, :cond_2

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isDeviceStorageLow:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$802(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 596
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 624
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const v1, 0x7f0808b1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isOngoing:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    goto :goto_0

    .line 603
    :cond_2
    const/16 v0, 0x12

    if-ne p2, v0, :cond_3

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->isServerUnderMaintenance:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$902(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->SERVER_ERROR_MESSAGE:Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1002(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 606
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 614
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->mIsNetworkErrorOccured:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;->access$1102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity;Z)Z

    .line 615
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreActivity$8;)V

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
