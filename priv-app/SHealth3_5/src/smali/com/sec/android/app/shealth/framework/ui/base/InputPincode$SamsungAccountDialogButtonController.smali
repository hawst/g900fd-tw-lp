.class Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SamsungAccountDialogButtonController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SamsungAccountDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SamsungAccountDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SamsungAccountDialogButtonController;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$3;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SamsungAccountDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getAccountPasswordVerifyIntent()Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x1c6d

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode$SamsungAccountDialogButtonController;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->mIsDialogShown:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;->access$202(Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;Z)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
