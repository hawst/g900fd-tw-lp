.class public Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
.super Landroid/support/v4/app/Fragment;
.source "MealItemsFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_FRAGMENT_MODE:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

.field private static final DELETED_ITEM_NAME:Ljava/lang/String; = "DELETED_ITEM_NAME"

.field private static final DELETED_MEAL_ITEMS_KEY:Ljava/lang/String; = "DELETED_MEAL_ITEMS_KEY"

.field private static final DELETE_MEAL_ITEM_POSITION_KEY:Ljava/lang/String; = "DELETE_MEAL_ITEM_POSITION_KEY"

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MEAL_ITEMS_KEY:Ljava/lang/String; = "MEAL_ITEMS_KEY"

.field private static final MEAL_ITEM_VALUE:Ljava/lang/String; = "TEMP_QUICK_INPUT_KCAL_HASHMAP_KEY"

.field private static final QUICK_INPUT_KCAL_HASHMAP_KEY:Ljava/lang/String; = "QUICK_INPUT_KCAL_HASHMAP_KEY"

.field private static final QUICK_INPUT_POP_UP:Ljava/lang/String; = "quick_input_poup"

.field private static final QUICK_INPUT_VIEW_INDEX:Ljava/lang/String; = "QUICK_INPUT_VIEW_INDEX"

.field private static final SKIPPED_MEAL_CONFIRM_DIALOG:Ljava/lang/String; = "skipped_meal_confirm_dialog"

.field private static final TEMP_MEAL_ITEM_VIEW_INDEX_KEY:Ljava/lang/String; = "TEMP_MEAL_ITEM_VIEW_INDEX_KEY"

.field private static final TEMP_QUICK_INPUT_KCAL_HASHMAP_KEY:Ljava/lang/String; = "TEMP_QUICK_INPUT_KCAL_HASHMAP_KEY"


# instance fields
.field protected fragmentMode:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

.field isFragmentCreated:Z

.field private mAddFoodButton:Landroid/view/View;

.field mContentInitializationListener:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;

.field private mDeleteMealItemPosition:I

.field private mDeletedMealItemsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mDeletedMealName:Ljava/lang/String;

.field private mExtraFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;

.field private mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

.field private mIncorrectRangeToast:Landroid/widget/Toast;

.field private mIsNormalItemsPresent:Z

.field private mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

.field mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

.field protected mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

.field mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

.field protected mMealItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mMealItemsContainer:Landroid/widget/LinearLayout;

.field private mMealType:I

.field private mMealTypeSelector:Landroid/widget/TextView;

.field private mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

.field private mQuickInputKcalHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

.field private mQuickInputViewIndex:I

.field private mSummaryContaner:Landroid/widget/LinearLayout;

.field private mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

.field private mTempMealItemViewIndex:I

.field private mTempQIServerSourceType:I

.field private mTempQuickInputKcalHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mTextViewMealCaloriesInDivider:Landroid/widget/TextView;

.field protected mTextViewsMealNutrients:Landroid/widget/TextView;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

.field protected textViewMealCalories:Landroid/widget/TextView;

.field protected totalKcal:I

.field protected viewNutritionInfoButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->LOG_TAG:Ljava/lang/String;

    .line 88
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->DEFAULT_FRAGMENT_MODE:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0xf

    .line 81
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealItemsList:Ljava/util/List;

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempMealItemViewIndex:I

    .line 134
    const v0, 0x186a1

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealType:I

    .line 143
    const v0, 0x46cd6

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQIServerSourceType:I

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIsNormalItemsPresent:Z

    .line 147
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealName:Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQuickInputKcalHashMap:Ljava/util/HashMap;

    .line 152
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    .line 858
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->handleMealItemKcalAfterChangePortionSize(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->handleMemoFocus()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeleteMealItemPosition:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->deleteMealItemAt(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealTypeSelector:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealTypeSelector:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealType:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQIServerSourceType:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->isAnyDialogShown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->findMealItemViewPosition(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->showMealDeleteConfirmDialog(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    .param p2, "x2"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->onPortionSizeBtnClick(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    .param p2, "x2"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->showPortionPopup(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    return-void
.end method

.method private addMealItemViewToContainerByMealItemData(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 9
    .param p1, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    const/4 v8, -0x1

    .line 386
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->createMealItemView()Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    move-result-object v1

    .line 387
    .local v1, "mealItemView":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;

    invoke-direct {v2, p0, p1, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$4;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)V

    .line 398
    .local v2, "minusButtonListener":Landroid/view/View$OnClickListener;
    new-instance v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$5;

    invoke-direct {v3, p0, v1, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$5;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    .line 404
    .local v3, "portionSizeButtonListener":Landroid/view/View$OnClickListener;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v6

    invoke-interface {v5, v6, v7}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 405
    .local v0, "foodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isQuickInputType(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 406
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v8, :cond_2

    .line 425
    :cond_0
    :goto_0
    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$6;

    invoke-direct {v4, p0, v1, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$6;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    .line 437
    .local v4, "quickInputPopupListener":Landroid/view/View$OnClickListener;
    invoke-virtual {v1, p1, v2, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->init(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 438
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 440
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v8, :cond_4

    .line 442
    const v5, 0x7f0909c8

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setMealKcal(Ljava/lang/String;)V

    .line 452
    .end local v4    # "quickInputPopupListener":Landroid/view/View$OnClickListener;
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->fragmentMode:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-direct {p0, v1, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->setMealItemViewEditableAccordingToMode(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 453
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 454
    return-void

    .line 411
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    .line 412
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getServerSourceType()I

    move-result v5

    const v6, 0x46cd7

    if-ne v5, v6, :cond_3

    .line 414
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 418
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v7

    float-to-int v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 444
    .restart local v4    # "quickInputPopupListener":Landroid/view/View$OnClickListener;
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900b9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setMealKcal(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 448
    .end local v4    # "quickInputPopupListener":Landroid/view/View$OnClickListener;
    :cond_5
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIsNormalItemsPresent:Z

    .line 449
    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->init(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1
.end method

.method private addMealItemWithUpdateSumCalories(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 0
    .param p1, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 374
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->addMealItemWithoutUpdateSumCalories(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSummaryCalories()V

    .line 376
    return-void
.end method

.method private addMealItemWithoutUpdateSumCalories(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 2
    .param p1, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->addMealItemViewToContainerByMealItemData(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;->onMealItemsQuantityChanged(I)V

    .line 370
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->refreshVisibilityStateButtonNutritionInfo(Landroid/view/View;)V

    .line 371
    return-void
.end method

.method private deleteMealItemAt(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 815
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 832
    :goto_0
    return-void

    .line 819
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "removing meal confirmed, position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mealItems size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 824
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealItemsList:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 826
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIsNormalItemsPresent:Z

    .line 827
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSummaryCalories()V

    .line 828
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    if-eqz v0, :cond_1

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;->onMealItemsQuantityChanged(I)V

    .line 831
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->refreshVisibilityStateButtonNutritionInfo(Landroid/view/View;)V

    goto :goto_0
.end method

.method private findMealItemViewPosition(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)I
    .locals 2
    .param p1, "mealItemView"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    .prologue
    .line 791
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 792
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 796
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 791
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 796
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleMealItemKcalAfterChangePortionSize(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V
    .locals 5
    .param p1, "portionSizeHolder"    # Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    .prologue
    .line 800
    if-eqz p1, :cond_0

    .line 801
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempMealItemViewIndex:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    .line 802
    .local v1, "mealItemView":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    if-eqz v1, :cond_1

    .line 803
    iget-object v0, v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->mealItemData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 804
    .local v0, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getQuantity()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setAmount(F)V

    .line 805
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;->getUnit()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->setUnit(I)V

    .line 806
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealItemCalorieAndNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setMealKcal(Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;)V

    .line 807
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSummaryCalories()V

    .line 812
    .end local v0    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .end local v1    # "mealItemView":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    :cond_0
    :goto_0
    return-void

    .line 809
    .restart local v1    # "mealItemView":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "in mMealItemsContainer childAt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempMealItemViewIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleMemoFocus()V
    .locals 2

    .prologue
    .line 1016
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1017
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    if-eqz v1, :cond_0

    .line 1018
    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->clearMemoFocus()V

    .line 1020
    :cond_0
    return-void
.end method

.method private isAnyDialogShown()Z
    .locals 3

    .prologue
    .line 547
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->ALL_DIALOGS_TAGS:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 548
    .local v1, "tag":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 550
    :cond_1
    const/4 v2, 0x1

    .line 553
    .end local v1    # "tag":Ljava/lang/String;
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onPortionSizeBtnClick(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 7
    .param p1, "mealItemView"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    .param p2, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 466
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->isAnyDialogShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    :goto_0
    return-void

    .line 469
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v3

    .line 470
    .local v3, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isExtraFoodInfoDataDownloadRequired(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$7;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)V

    invoke-virtual {v6, v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    goto :goto_0

    .line 494
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->showPortionPopup(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    goto :goto_0
.end method

.method private setMealItemViewEditableAccordingToMode(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V
    .locals 2
    .param p1, "mealItemView"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    .param p2, "fragmentMode"    # Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .prologue
    .line 333
    iget-boolean v0, p2, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->isEditable:Z

    iget-boolean v1, p2, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->isPortionSizeButtonVisible:Z

    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setEditable(ZZ)V

    .line 334
    return-void
.end method

.method private showMealDeleteConfirmDialog(ILjava/lang/String;)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "mealName"    # Ljava/lang/String;

    .prologue
    .line 457
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeleteMealItemPosition:I

    .line 458
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealName:Ljava/lang/String;

    .line 459
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f09093d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f030009

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090035

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "REMOVE_MEAL_ITEM_DIALOG_TAG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 463
    return-void
.end method

.method private showMinMaxToast()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1024
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIncorrectRangeToast:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 1027
    const v1, 0x7f09098d

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/16 v3, 0x270f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1031
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIncorrectRangeToast:Landroid/widget/Toast;

    .line 1035
    .end local v0    # "text":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIncorrectRangeToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1038
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIncorrectRangeToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1041
    :cond_1
    return-void
.end method

.method private showPortionPopup(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 15
    .param p1, "mealItemView"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    .param p2, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 499
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->findMealItemViewPosition(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempMealItemViewIndex:I

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .line 502
    .local v13, "foodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mExtraFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v12

    .line 506
    .local v12, "extraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    if-eqz v12, :cond_0

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->isAddedByUser()Z

    move-result v0

    if-nez v0, :cond_0

    .line 508
    iget-object v14, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getAmount()F

    move-result v1

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v2

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v6

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getOzInKcal()F

    move-result v7

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v9

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v10

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v11

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;-><init>(FIFZLjava/lang/String;FFLjava/lang/String;FFF)V

    invoke-virtual {v14, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setPortionSizeData(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V

    .line 538
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_SET_UNIT_AMOUNT_FOR_FOOD:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->showPortionSizePopup(Landroid/support/v4/app/FragmentManager;)V

    .line 540
    return-void

    .line 515
    :cond_0
    if-eqz v12, :cond_1

    .line 517
    iget-object v14, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getAmount()F

    move-result v1

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v2

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const v8, 0x7f09017b

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v9

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v10

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v11

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;-><init>(FIFZLjava/lang/String;FFLjava/lang/String;FFF)V

    invoke-virtual {v14, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setPortionSizeData(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V

    goto :goto_0

    .line 522
    :cond_1
    iget-object v14, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getAmount()F

    move-result v1

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getUnit()I

    move-result v2

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getkCal()F

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const v8, 0x7f09017b

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;-><init>(FIFZLjava/lang/String;FFLjava/lang/String;FFF)V

    invoke-virtual {v14, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setPortionSizeData(Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeHolder;)V

    goto :goto_0
.end method

.method private showSkippedMealConfirmDialog()V
    .locals 6

    .prologue
    .line 955
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0909cd

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0909ce

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealType:I

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealTypeString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "skipped_meal_confirm_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 963
    return-void
.end method

.method private updateSkippedQuickInputRow()V
    .locals 3

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    if-eqz v0, :cond_0

    .line 1005
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0909c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setMealKcal(Ljava/lang/String;)V

    .line 1006
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1007
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008
    const v0, 0x46cd7

    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQIServerSourceType:I

    .line 1009
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSummaryCalories()V

    .line 1013
    :cond_0
    return-void
.end method


# virtual methods
.method public addMealItem(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 1
    .param p1, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->addMealItemWithUpdateSumCalories(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    .line 362
    :cond_0
    return-void
.end method

.method public addMealItemList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 601
    .local p1, "mealItemDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    if-nez p1, :cond_0

    .line 602
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "mealItemDataList can\'t be null "

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 604
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 605
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 606
    .local v1, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    if-eqz v1, :cond_1

    .line 607
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->addMealItemWithoutUpdateSumCalories(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    goto :goto_0

    .line 609
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "one of mealItemDataList is null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 612
    .end local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    if-nez v3, :cond_3

    .line 613
    new-instance v3, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .line 615
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    if-nez v3, :cond_4

    .line 616
    new-instance v3, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    .line 619
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_6

    .line 620
    const/4 v2, 0x0

    .line 621
    .local v2, "pos":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 623
    .restart local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_5

    .line 624
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->deleteMealItemAt(I)V

    .line 638
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .end local v2    # "pos":I
    :goto_2
    return-void

    .line 627
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .restart local v2    # "pos":I
    :cond_5
    add-int/lit8 v2, v2, 0x1

    .line 628
    goto :goto_1

    .line 630
    .end local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .end local v2    # "pos":I
    :cond_6
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->isFragmentCreated:Z

    if-eqz v3, :cond_7

    .line 631
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQuickInputKcalHashMap:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 633
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSummaryCalories()V

    goto :goto_2

    .line 636
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_8
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    goto :goto_2
.end method

.method protected calculateMealCalories()I
    .locals 2

    .prologue
    .line 774
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealCalories(Ljava/util/List;Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected calculateMealNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    .locals 1
    .param p1, "mealItemData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 782
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getMealItemCalorieAndNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;Landroid/content/Context;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    move-result-object v0

    return-object v0
.end method

.method protected createMealItemView()Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    .locals 2

    .prologue
    .line 382
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public getAllMealItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 652
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    return-object v0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 558
    const-string/jumbo v0, "quick_input_poup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$QIContentInitializationListener;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V

    .line 578
    :goto_0
    return-object v0

    .line 561
    :cond_0
    const-string v0, "REMOVE_MEAL_ITEM_DIALOG_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$8;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V

    goto :goto_0

    .line 578
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeletedMealItemsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 642
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealItemsList:Ljava/util/List;

    return-object v0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 583
    const-string v0, "REMOVE_MEAL_ITEM_DIALOG_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 584
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$9;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V

    .line 596
    :goto_0
    return-object v0

    .line 593
    :cond_0
    const-string v0, "PORTION_SIZE_POPUP_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0

    .line 596
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDialogNotSupported(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method public getQIServerType()I
    .locals 1

    .prologue
    .line 950
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQIServerSourceType:I

    return v0
.end method

.method public getQuickInputHashMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getTotalKcal()I
    .locals 1

    .prologue
    .line 647
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->totalKcal:I

    return v0
.end method

.method public hasQuickInputChanged()Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 1045
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v6

    if-eq v4, v6, :cond_0

    move v4, v5

    .line 1055
    :goto_0
    return v4

    .line 1047
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1049
    .local v0, "calEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 1050
    .local v1, "foodIdKey":J
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_2
    move v4, v5

    .line 1052
    goto :goto_0

    .line 1055
    .end local v0    # "calEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    .end local v1    # "foodIdKey":J
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public initQuickInputPopUp(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 3
    .param p1, "mealItemView"    # Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    .param p2, "mealItem"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 850
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 851
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    .line 852
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0909c1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f030113

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09004f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnOkClick()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "quick_input_poup"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 855
    return-void
.end method

.method protected initTotalKcalTextViews(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V
    .locals 4
    .param p1, "fragmentMode"    # Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .prologue
    const v2, 0x7f0804a3

    .line 342
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->EDIT_MY_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    if-ne p1, v0, :cond_1

    .line 343
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09091f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 352
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSummaryCalories()V

    .line 353
    return-void

    .line 350
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 162
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;-><init>(Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    .line 163
    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v2, p1}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .line 164
    new-instance v2, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;

    invoke-direct {v2, p1}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mExtraFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;

    .line 166
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    move-object v2, v0

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_0
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;-><init>(Landroid/app/Activity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    .line 174
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    new-instance v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->setPortionSizePopupResultListener(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$PortionSizePopupResultListener;)V

    .line 181
    return-void

    .line 167
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Activity not implement "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " interface. So some function will be disable"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 186
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->isFragmentCreated:Z

    .line 187
    if-eqz p3, :cond_1

    const-string v3, "MEAL_ITEMS_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 189
    const-string v3, "MEAL_ITEMS_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    .line 190
    const-string v3, "DELETE_MEAL_ITEM_POSITION_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeleteMealItemPosition:I

    .line 191
    const-string v3, "DELETED_MEAL_ITEMS_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealItemsList:Ljava/util/List;

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v3, p3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 193
    const-string v3, "TEMP_MEAL_ITEM_VIEW_INDEX_KEY"

    iget v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempMealItemViewIndex:I

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempMealItemViewIndex:I

    .line 194
    const-string v3, "DELETED_ITEM_NAME"

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealName:Ljava/lang/String;

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealName:Ljava/lang/String;

    .line 195
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->isFragmentCreated:Z

    .line 196
    const-string v3, "TEMP_QUICK_INPUT_KCAL_HASHMAP_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "QUICK_INPUT_KCAL_HASHMAP_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 198
    const-string v3, "QUICK_INPUT_KCAL_HASHMAP_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    .line 199
    const-string v3, "TEMP_QUICK_INPUT_KCAL_HASHMAP_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/HashMap;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQuickInputKcalHashMap:Ljava/util/HashMap;

    .line 201
    :cond_0
    const-string v3, "TEMP_QUICK_INPUT_KCAL_HASHMAP_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 202
    const-string v3, "TEMP_QUICK_INPUT_KCAL_HASHMAP_KEY"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 207
    :cond_1
    const v3, 0x7f0300eb

    invoke-virtual {p1, v3, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 208
    .local v1, "fragmentView":Landroid/view/View;
    const v3, 0x7f0803f5

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->viewNutritionInfoButton:Landroid/widget/Button;

    .line 209
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->viewNutritionInfoButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090932

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->viewNutritionInfoButton:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    const v3, 0x7f0803f3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    .line 223
    const v3, 0x7f0803f0

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mSummaryContaner:Landroid/widget/LinearLayout;

    .line 224
    const v3, 0x7f0803f1

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->textViewMealCalories:Landroid/widget/TextView;

    .line 225
    const v3, 0x7f0803f2

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTextViewsMealNutrients:Landroid/widget/TextView;

    .line 226
    const v3, 0x7f0804a5

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTextViewMealCaloriesInDivider:Landroid/widget/TextView;

    .line 227
    const v3, 0x7f0803db

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mAddFoodButton:Landroid/view/View;

    .line 228
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mAddFoodButton:Landroid/view/View;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f090930

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f09020a

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mAddFoodButton:Landroid/view/View;

    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 253
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 254
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_2

    instance-of v3, v0, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    if-eqz v3, :cond_2

    .line 256
    const v3, 0x7f0803f6

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 257
    .local v2, "nutritionButtonDivider":Landroid/view/View;
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mSummaryContaner:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_2

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mSummaryContaner:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 264
    .end local v2    # "nutritionButtonDivider":Landroid/view/View;
    :cond_2
    return-object v1
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 666
    const-string v1, "MEAL_ITEM_LIST"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 668
    .local v0, "mealItemDataList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    if-eqz v0, :cond_0

    .line 669
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->addMealItemList(Ljava/util/List;)V

    .line 671
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 271
    const-string v2, "MEAL_ITEMS_KEY"

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 272
    const-string v2, "DELETED_MEAL_ITEMS_KEY"

    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealItemsList:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 273
    const-string v2, "DELETE_MEAL_ITEM_POSITION_KEY"

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeleteMealItemPosition:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 274
    const-string v2, "TEMP_MEAL_ITEM_VIEW_INDEX_KEY"

    iget v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempMealItemViewIndex:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 276
    const-string v2, "DELETED_ITEM_NAME"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v2, "QUICK_INPUT_KCAL_HASHMAP_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 278
    const-string v2, "TEMP_QUICK_INPUT_KCAL_HASHMAP_KEY"

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 280
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "quick_input_poup"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 283
    .local v1, "quickInpuDialogFragment":Landroid/support/v4/app/DialogFragment;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->findMealItemViewPosition(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;)I

    move-result v0

    .line 285
    .local v0, "pos":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 286
    const-string v2, "QUICK_INPUT_VIEW_INDEX"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 292
    .end local v0    # "pos":I
    .end local v1    # "quickInpuDialogFragment":Landroid/support/v4/app/DialogFragment;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mPortionSizePopup:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 293
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 294
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 298
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 299
    sget-object v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->DEFAULT_FRAGMENT_MODE:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->setFragmentMode(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 301
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 302
    .local v1, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->addMealItemViewToContainerByMealItemData(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V

    goto :goto_0

    .line 306
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    if-eqz v3, :cond_1

    .line 307
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;->onMealItemsQuantityChanged(I)V

    .line 309
    :cond_1
    if-eqz p2, :cond_2

    const-string v3, "QUICK_INPUT_VIEW_INDEX"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 310
    const-string v3, "QUICK_INPUT_VIEW_INDEX"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 311
    .local v2, "viewPosition":I
    if-ltz v2, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-gt v2, v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    if-eqz v3, :cond_2

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 317
    .end local v2    # "viewPosition":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->refreshVisibilityStateButtonNutritionInfo(Landroid/view/View;)V

    .line 318
    return-void
.end method

.method protected refreshVisibilityStateButtonNutritionInfo(Landroid/view/View;)V
    .locals 2
    .param p1, "rootView"    # Landroid/view/View;

    .prologue
    .line 840
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->fragmentMode:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    iget v1, v1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->nutritionInfoButtonVisibility:I

    if-eqz v1, :cond_0

    .line 846
    :goto_0
    return-void

    .line 844
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIsNormalItemsPresent:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 845
    .local v0, "requiredVisibility":I
    :goto_1
    const v1, 0x7f0803f4

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 844
    .end local v0    # "requiredVisibility":I
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public resetAllData()V
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealItemsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 660
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->refreshVisibilityStateButtonNutritionInfo(Landroid/view/View;)V

    .line 661
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSummaryCalories()V

    .line 662
    return-void
.end method

.method public resetMealToSkipped()V
    .locals 3

    .prologue
    .line 970
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 972
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 974
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 970
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 979
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 980
    iput v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeleteMealItemPosition:I

    .line 981
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mDeletedMealItemsList:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 982
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 988
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSkippedQuickInputRow()V

    .line 989
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    if-eqz v1, :cond_3

    .line 991
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealActivityAction:Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;->onMealItemsQuantityChanged(I)V

    .line 993
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->refreshVisibilityStateButtonNutritionInfo(Landroid/view/View;)V

    .line 995
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    if-eqz v1, :cond_4

    .line 997
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;->cleanImageAndMemo()V

    .line 999
    :cond_4
    return-void
.end method

.method public setFragmentMode(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V
    .locals 4
    .param p1, "fragmentMode"    # Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .prologue
    .line 322
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    .line 324
    .local v1, "mealItemView":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->setMealItemViewEditableAccordingToMode(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 326
    .end local v1    # "mealItemView":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->viewNutritionInfoButton:Landroid/widget/Button;

    iget v3, p1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->nutritionInfoButtonVisibility:I

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 327
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mAddFoodButton:Landroid/view/View;

    iget v3, p1, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->addFoodButtonVisibility:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 328
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->fragmentMode:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    .line 329
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->initTotalKcalTextViews(Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;)V

    .line 330
    return-void
.end method

.method public setQuickInputMealType(I)V
    .locals 1
    .param p1, "mealType"    # I

    .prologue
    .line 1068
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 1070
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealType:I

    .line 1073
    :cond_0
    return-void
.end method

.method public updateQuickInput()V
    .locals 7

    .prologue
    .line 893
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    if-eqz v3, :cond_0

    .line 896
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    const v4, 0x7f08047e

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 897
    .local v0, "calorieEdit":Landroid/widget/EditText;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 899
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->showMinMaxToast()V

    .line 900
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 901
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 902
    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 903
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 939
    .end local v0    # "calorieEdit":Landroid/widget/EditText;
    :cond_0
    :goto_0
    return-void

    .line 907
    .restart local v0    # "calorieEdit":Landroid/widget/EditText;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 910
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string/jumbo v4, "quick_input_poup"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/DialogFragment;

    .line 911
    .local v2, "quickInputDialog":Landroid/support/v4/app/DialogFragment;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 913
    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 917
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->isSKippedSelected()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 919
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->showSkippedMealConfirmDialog()V

    goto :goto_0

    .line 923
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputPanel:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getQuickInputCalorieValue()I

    move-result v1

    .line 925
    .local v1, "calorieValue":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    if-eqz v3, :cond_0

    .line 927
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItem:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 928
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemView:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0900b9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemView;->setMealKcal(Ljava/lang/String;)V

    .line 930
    const v3, 0x46cd6

    iput v3, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTempQIServerSourceType:I

    .line 931
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->updateSummaryCalories()V

    goto/16 :goto_0
.end method

.method protected updateSummaryCalories()V
    .locals 17

    .prologue
    .line 677
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    if-nez v13, :cond_0

    .line 678
    sget-object v13, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->LOG_TAG:Ljava/lang/String;

    const-string v14, "did not updateSummaryCalories : getActivitiy() is null"

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    :goto_0
    return-void

    .line 682
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    if-nez v13, :cond_1

    .line 683
    new-instance v13, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .line 685
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    if-nez v13, :cond_2

    .line 686
    new-instance v13, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    .line 689
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->calculateMealCalories()I

    move-result v11

    .line 691
    .local v11, "summary":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mQuickInputKcalHashMap:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 692
    .local v3, "data":I
    const/4 v13, -0x1

    if-ne v3, v13, :cond_3

    .line 693
    const/4 v3, 0x0

    .line 695
    :cond_3
    add-int/2addr v11, v3

    .line 696
    goto :goto_1

    .line 698
    .end local v3    # "data":I
    :cond_4
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->totalKcal:I

    .line 699
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-lez v13, :cond_6

    const/4 v1, 0x0

    .line 702
    .local v1, "actualVisibility":I
    :goto_2
    if-nez v1, :cond_e

    .line 704
    const/4 v9, 0x0

    .line 705
    .local v9, "proteins":F
    const/4 v2, 0x0

    .line 706
    .local v2, "carbos":F
    const/4 v4, 0x0

    .line 707
    .local v4, "fat":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .line 709
    .local v7, "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIsNormalItemsPresent:Z

    if-nez v13, :cond_5

    .line 711
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;->getFoodInfoId()J

    move-result-wide v14

    invoke-interface {v13, v14, v15}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    invoke-static {v13}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isQuickInputType(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z

    move-result v13

    if-nez v13, :cond_7

    const/4 v13, 0x1

    :goto_4
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mIsNormalItemsPresent:Z

    .line 713
    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->calculateMealNutrients(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;

    move-result-object v6

    .line 714
    .local v6, "mFoodItemCaloriesAndNutrients":Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getProteins()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_8

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getProteins()F

    move-result v13

    :goto_5
    add-float/2addr v9, v13

    .line 715
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCarbos()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_9

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getCarbos()F

    move-result v13

    :goto_6
    add-float/2addr v2, v13

    .line 716
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getFat()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_a

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->getFat()F

    move-result v13

    :goto_7
    add-float/2addr v4, v13

    .line 717
    goto :goto_3

    .line 699
    .end local v1    # "actualVisibility":I
    .end local v2    # "carbos":F
    .end local v4    # "fat":F
    .end local v6    # "mFoodItemCaloriesAndNutrients":Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    .end local v7    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .end local v9    # "proteins":F
    :cond_6
    const/16 v1, 0x8

    goto :goto_2

    .line 711
    .restart local v1    # "actualVisibility":I
    .restart local v2    # "carbos":F
    .restart local v4    # "fat":F
    .restart local v7    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .restart local v9    # "proteins":F
    :cond_7
    const/4 v13, 0x0

    goto :goto_4

    .line 714
    .restart local v6    # "mFoodItemCaloriesAndNutrients":Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    :cond_8
    const/4 v13, 0x0

    goto :goto_5

    .line 715
    :cond_9
    const/4 v13, 0x0

    goto :goto_6

    .line 716
    :cond_a
    const/4 v13, 0x0

    goto :goto_7

    .line 719
    .end local v6    # "mFoodItemCaloriesAndNutrients":Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
    .end local v7    # "mealItemData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    :cond_b
    const/4 v13, 0x0

    cmpl-float v13, v9, v13

    if-gtz v13, :cond_c

    const/4 v13, 0x0

    cmpl-float v13, v2, v13

    if-gtz v13, :cond_c

    const/4 v13, 0x0

    cmpl-float v13, v4, v13

    if-lez v13, :cond_d

    .line 721
    :cond_c
    const-string v10, " "

    .line 722
    .local v10, "space":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTextViewsMealNutrients:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 724
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0909cb

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v4}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0900bf

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0909ca

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0900bf

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090966

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v9}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->convertToSingleDecimal(F)F

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0900bf

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 728
    .local v8, "nutrientsString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTextViewsMealNutrients:Landroid/widget/TextView;

    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 731
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mSummaryContaner:Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->textViewMealCalories:Landroid/widget/TextView;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 752
    .end local v2    # "carbos":F
    .end local v4    # "fat":F
    .end local v8    # "nutrientsString":Ljava/lang/String;
    .end local v9    # "proteins":F
    .end local v10    # "space":Ljava/lang/String;
    :goto_8
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f09008a

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ": "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0900b9

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 757
    .local v12, "totalKcalText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mSummaryContaner:Landroid/widget/LinearLayout;

    invoke-virtual {v13, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 758
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->fragmentMode:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    sget-object v14, Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;->VIEW_MEAL:Lcom/sec/android/app/shealth/food/mealinput/FragmentMode;

    if-ne v13, v14, :cond_f

    .line 760
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    const v14, 0x7f0803ef

    invoke-virtual {v13, v14}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 766
    :goto_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->textViewMealCalories:Landroid/widget/TextView;

    invoke-virtual {v13, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 767
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->textViewMealCalories:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14, v12}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 736
    .end local v12    # "totalKcalText":Ljava/lang/String;
    .restart local v2    # "carbos":F
    .restart local v4    # "fat":F
    .restart local v9    # "proteins":F
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTextViewsMealNutrients:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 737
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mSummaryContaner:Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->textViewMealCalories:Landroid/widget/TextView;

    invoke-virtual {v15}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v15

    invoke-interface {v15}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 743
    .end local v2    # "carbos":F
    .end local v4    # "fat":F
    .end local v9    # "proteins":F
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mTextViewsMealNutrients:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 764
    .restart local v12    # "totalKcalText":Ljava/lang/String;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    const v14, 0x7f0803ef

    invoke-virtual {v13, v14}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9
.end method
