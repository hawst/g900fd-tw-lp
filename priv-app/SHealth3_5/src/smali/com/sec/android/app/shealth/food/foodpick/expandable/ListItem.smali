.class abstract Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
.super Ljava/lang/Object;
.source "ListItem.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritableListItem;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/RingingCheckable;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/Selectable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;
    }
.end annotation


# instance fields
.field protected mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

.field private mIsChecked:Z

.field private mIsSoundEffectEnabled:Z

.field private mNameSelection:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mIsSoundEffectEnabled:Z

    .line 205
    return-void
.end method

.method private setNameText(Ljava/lang/String;)V
    .locals 1
    .param p1, "nameText"    # Ljava/lang/String;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->mealNameText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 3
    .param p1, "another"    # Ljava/lang/Object;

    .prologue
    .line 72
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .line 73
    .local v0, "listItem":Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method protected abstract getDescription(Landroid/content/res/Resources;)Ljava/lang/String;
.end method

.method public getExpandableIndicator()Landroid/view/View;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->expandableIndicator:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getFavoriteButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favorite:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public getFavoriteWrapper()Landroid/view/View;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    return-object v0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 135
    const v0, 0x7f0300cc

    return v0
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public getView(Landroid/view/LayoutInflater;Landroid/view/View;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "outState"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    .prologue
    .line 103
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getLayoutId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 105
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    const v1, 0x7f0803ad

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->expandableIndicator:Landroid/widget/ImageView;

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    const v2, 0x7f0803ae

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    const v1, 0x7f0803af

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favorite:Landroid/widget/CheckBox;

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    const v1, 0x7f0803b0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->mealNameText:Landroid/widget/TextView;

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    const v1, 0x7f0803b1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->caloriesText:Landroid/widget/TextView;

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    const v1, 0x7f0803ab

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 121
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 122
    .local v0, "resources":Landroid/content/res/Resources;
    invoke-virtual {p0, p4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->initExpandableIndicator(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/widget/ImageView;

    .line 123
    invoke-virtual {p0, p4, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->initFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;Landroid/content/res/Resources;)Landroid/view/View;

    .line 124
    invoke-virtual {p0, p4, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->initNameTextView(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;Landroid/content/res/Resources;)Landroid/widget/TextView;

    .line 125
    invoke-virtual {p0, p4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->initCheckbox(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/widget/CheckBox;

    .line 126
    invoke-virtual {p0, p4, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->initCaloriesTextView(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;Landroid/content/res/Resources;)Landroid/widget/TextView;

    .line 128
    return-object p2

    .line 118
    .end local v0    # "resources":Landroid/content/res/Resources;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    goto :goto_0
.end method

.method protected initCaloriesTextView(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;Landroid/content/res/Resources;)Landroid/widget/TextView;
    .locals 2
    .param p1, "state"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->caloriesText:Landroid/widget/TextView;

    invoke-virtual {p0, p2}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getDescription(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->caloriesText:Landroid/widget/TextView;

    return-object v0
.end method

.method protected initCheckbox(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/widget/CheckBox;
    .locals 2
    .param p1, "state"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v1, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->areItemsCheckable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    return-object v0

    .line 153
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected initExpandableIndicator(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;)Landroid/widget/ImageView;
    .locals 3
    .param p1, "state"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;

    .prologue
    const/4 v1, 0x0

    .line 196
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->areItemsExpandable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->areItemsCheckable()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 197
    .local v0, "isVisible":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->expandableIndicator:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->expandableIndicator:Landroid/widget/ImageView;

    return-object v1

    .end local v0    # "isVisible":Z
    :cond_0
    move v0, v1

    .line 196
    goto :goto_0

    .line 197
    .restart local v0    # "isVisible":Z
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method

.method protected initFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;Landroid/content/res/Resources;)Landroid/view/View;
    .locals 6
    .param p1, "state"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v1, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favorite:Landroid/widget/CheckBox;

    .line 180
    .local v1, "favoriteButton":Landroid/widget/CheckBox;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->getFavoriteStarsVisibility()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;->getFavoriteStarsVisibility()Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->isFavorite()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->isFavorite()Z

    move-result v2

    if-eqz v2, :cond_2

    const v0, 0x7f09092b

    .line 187
    .local v0, "actualStringResId":I
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favouriteWrapper:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->favorite:Landroid/widget/CheckBox;

    return-object v2

    .end local v0    # "actualStringResId":I
    :cond_0
    move v2, v4

    .line 180
    goto :goto_0

    :cond_1
    move v3, v4

    .line 181
    goto :goto_1

    .line 185
    :cond_2
    const v0, 0x7f09092a

    goto :goto_2
.end method

.method protected initNameTextView(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;Landroid/content/res/Resources;)Landroid/widget/TextView;
    .locals 6
    .param p1, "state"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItemStateHolder;
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->getName()Ljava/lang/String;

    move-result-object v0

    .line 160
    .local v0, "nameText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mNameSelection:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mNameSelection:Ljava/lang/String;

    const-string v5, "\\s+"

    invoke-virtual {v4, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 161
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mNameSelection:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 162
    .local v2, "selectionStart":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mNameSelection:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int v1, v2, v4

    .line 163
    .local v1, "selectionEnd":I
    if-ltz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v1, v4, :cond_0

    .line 164
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 165
    .local v3, "spannable":Landroid/text/Spannable;
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    const v5, 0x7f0701d1

    invoke-virtual {p2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v5, 0x21

    invoke-interface {v3, v4, v2, v1, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v4, v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->mealNameText:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    .end local v1    # "selectionEnd":I
    .end local v2    # "selectionStart":I
    .end local v3    # "spannable":Landroid/text/Spannable;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v4, v4, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->mealNameText:Landroid/widget/TextView;

    return-object v4

    .line 170
    .restart local v1    # "selectionEnd":I
    .restart local v2    # "selectionStart":I
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setNameText(Ljava/lang/String;)V

    goto :goto_0

    .line 173
    .end local v1    # "selectionEnd":I
    .end local v2    # "selectionStart":I
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setNameText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isCheckBoxSoundEffectsEnabled()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mIsSoundEffectEnabled:Z

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mIsChecked:Z

    return v0
.end method

.method public setCheckBoxSoundEffectsEnabled(Z)V
    .locals 0
    .param p1, "isEnabled"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mIsSoundEffectEnabled:Z

    .line 83
    return-void
.end method

.method public setChecked(Z)V
    .locals 2
    .param p1, "isChecked"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mIsChecked:Z

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 68
    :cond_0
    return-void
.end method

.method public setCheckedWithoutSoundEffect(Z)V
    .locals 1
    .param p1, "isChecked"    # Z

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setChecked(Z)V

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setCheckBoxSoundEffectsEnabled(Z)V

    .line 79
    return-void
.end method

.method public setSelection(Ljava/lang/String;)V
    .locals 0
    .param p1, "nameSelection"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->mNameSelection:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public toggleCheckedState()Z
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->setChecked(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->isChecked()Z

    move-result v0

    return v0

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
