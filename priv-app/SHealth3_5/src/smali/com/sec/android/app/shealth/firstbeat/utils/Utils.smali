.class public Lcom/sec/android/app/shealth/firstbeat/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAgeValue(III)I
    .locals 4
    .param p0, "birthdayDay"    # I
    .param p1, "birthdayMonth"    # I
    .param p2, "birthdayYear"    # I

    .prologue
    const/4 v3, 0x2

    .line 11
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    .line 12
    .local v1, "calendar":Ljava/util/Calendar;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int v0, v2, p2

    .line 13
    .local v0, "age":I
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ge v2, p1, :cond_1

    .line 14
    add-int/lit8 v0, v0, -0x1

    .line 18
    :cond_0
    :goto_0
    return v0

    .line 15
    :cond_1
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v2, p1, :cond_0

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ge v2, p0, :cond_0

    .line 16
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method
