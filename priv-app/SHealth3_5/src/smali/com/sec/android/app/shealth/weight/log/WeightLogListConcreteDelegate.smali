.class public Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;
.super Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;
.source "WeightLogListConcreteDelegate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$1;,
        Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;
    }
.end annotation


# static fields
.field private static final AS_STRING:Ljava/lang/String; = " as "

.field private static final GROUP_TEXT_FORMAT:Ljava/lang/String; = "%s (%s %.1f %s)"


# instance fields
.field private mFilter:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

.field private mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;)V
    .locals 0
    .param p1, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;-><init>(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;)V

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;)Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getDao()Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;J)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p2, "x2"    # J

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->countWeightAvgForDay(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;J)F

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v0

    return-object v0
.end method

.method private countWeightAvgForDay(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;J)F
    .locals 10
    .param p1, "dao"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;
    .param p2, "dayStartTime"    # J

    .prologue
    .line 243
    const/4 v9, 0x0

    .line 245
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x3

    :try_start_0
    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id as _id"

    aput-object v1, v6, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "sample_time as day_time"

    aput-object v1, v6, v0

    const/4 v0, 0x2

    const-string v1, "AVG(weight) as avg_value"

    aput-object v1, v6, v0

    .line 250
    .local v6, "projection":[Ljava/lang/String;
    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {p2, p3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v3

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->DAY:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p1

    invoke-interface/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v9

    .line 252
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->getColumnCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 253
    :cond_0
    const/4 v0, 0x0

    .line 258
    if-eqz v9, :cond_1

    .line 259
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_1
    :goto_0
    return v0

    .line 255
    :cond_2
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 256
    const-string v0, "avg_value"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getFloat(I)F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 258
    if-eqz v9, :cond_1

    .line 259
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 258
    .end local v6    # "projection":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_3

    .line 259
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private getGroupText(Landroid/content/Context;FJZ)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "monthAverageValue"    # F
    .param p3, "monthStartTime"    # J
    .param p5, "isFullname"    # Z

    .prologue
    .line 269
    if-eqz p5, :cond_0

    .line 270
    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 271
    .local v1, "dateString":Ljava/lang/String;
    const v3, 0x7f090204

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "averageString":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getFullName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 286
    .local v2, "unitString":Ljava/lang/String;
    :goto_0
    const-string v3, "%s (%s %.1f %s)"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v5, 0x2

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 280
    .end local v0    # "averageString":Ljava/lang/String;
    .end local v1    # "dateString":Ljava/lang/String;
    .end local v2    # "unitString":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getMonthGroupDateString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 282
    .restart local v1    # "dateString":Ljava/lang/String;
    const v3, 0x7f090f20

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 283
    .restart local v0    # "averageString":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "unitString":Ljava/lang/String;
    goto :goto_0
.end method

.method private getInsideFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 9

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getNormalRangeFromProfile()Landroid/util/Pair;

    move-result-object v0

    .line 123
    .local v0, "normalRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 124
    .local v2, "weightMin":F
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 126
    .local v1, "weightMax":F
    new-instance v3, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v4, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v5, "weight"

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    const-string v7, "<"

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    sget-object v4, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->AND:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    new-instance v5, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v6, "weight"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    const-string v8, ">"

    invoke-direct {v5, v6, v7, v8}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->add(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v3

    return-object v3
.end method

.method private getNormalRangeFromProfile()Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 141
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    new-instance v3, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getHeightUnit(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertUnitToCm(FLjava/lang/String;)F

    move-result v0

    .line 143
    .local v0, "heightInCm":F
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightNormalRangeHelper;->getNormalRangeValues(Landroid/content/Context;F)Landroid/util/Pair;

    move-result-object v2

    return-object v2
.end method

.method private getOutsideFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 9

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getNormalRangeFromProfile()Landroid/util/Pair;

    move-result-object v0

    .line 132
    .local v0, "normalRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 133
    .local v2, "weightMin":F
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 135
    .local v1, "weightMax":F
    new-instance v3, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v4, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v5, "weight"

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    const-string v7, ">="

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    sget-object v4, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;->OR:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;

    new-instance v5, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v6, "weight"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    const-string v8, "<="

    invoke-direct {v5, v6, v7, v8}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->add(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder$QueryAddRule;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v3

    return-object v3
.end method

.method private getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    if-nez v0, :cond_0

    .line 339
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    return-object v0
.end method


# virtual methods
.method protected bindChildView(Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 17
    .param p1, "view"    # Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isRowChanged"    # Z

    .prologue
    .line 148
    invoke-super/range {p0 .. p4}, Lcom/sec/android/app/shealth/logutils/log/LogListConcreteDelegate;->bindChildView(Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 149
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v16

    .line 150
    .local v16, "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    const-string v2, "amount"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v15

    .line 152
    .local v15, "weight":F
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setLeftTextViewMarginForText()V

    .line 153
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTextView()Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTextView()Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getFullName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 155
    const-string v2, "day_time"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 156
    .local v6, "dayTime":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v8

    .line 157
    .local v8, "calendar":Ljava/util/Calendar;
    invoke-virtual {v8, v6, v7}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 158
    const-string/jumbo v2, "memo_text"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "memo_text"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v13, 0x1

    .line 161
    .local v13, "isMemoIconVisible":Z
    :goto_0
    if-eqz v13, :cond_2

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setMemoImageVisible(I)V

    .line 162
    const-string v2, "input_source_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const v3, 0x3f7a2

    if-ne v2, v3, :cond_3

    const/4 v12, 0x1

    .line 165
    .local v12, "isAccessoryIconVisible":Z
    :goto_2
    if-eqz v12, :cond_4

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setAccessoryImageVisible(I)V

    .line 167
    const v2, 0x7f08066b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 169
    const v2, 0x7f08066b

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0907b4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 171
    if-eqz v13, :cond_0

    .line 173
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getMemoImageVisible()Landroid/widget/ImageView;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "memo_text"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getRightTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-static/range {p2 .. p2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->isCursorSetOnFirstMeasureInDay(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 178
    new-instance v10, Ljava/text/SimpleDateFormat;

    const-string v2, " dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v10, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 180
    .local v10, "dateFormatterNew":Ljava/text/SimpleDateFormat;
    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v11

    .line 181
    .local v11, "dateNew":Ljava/lang/String;
    new-instance v2, Ljava/text/DateFormatSymbols;

    invoke-direct {v2}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v2}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v8, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    aget-object v9, v2, v3

    .line 182
    .local v9, "dMonth":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v14

    .line 183
    .local v14, "language":Ljava/lang/String;
    const-string v2, "ko"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 184
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x2

    invoke-virtual {v8, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090212

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090213

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/text/DateFormatSymbols;

    invoke-direct {v4}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v8, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 196
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getDayGroupDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    new-instance v2, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getRightTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v5

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;-><init>(Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;Landroid/content/Context;Landroid/widget/TextView;J)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$AvgCounterTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 199
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setTeamHeaderVisible(Z)V

    .line 203
    .end local v9    # "dMonth":Ljava/lang/String;
    .end local v10    # "dateFormatterNew":Ljava/text/SimpleDateFormat;
    .end local v11    # "dateNew":Ljava/lang/String;
    .end local v14    # "language":Ljava/lang/String;
    :goto_5
    return-void

    .line 158
    .end local v12    # "isAccessoryIconVisible":Z
    .end local v13    # "isMemoIconVisible":Z
    :cond_1
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 161
    .restart local v13    # "isMemoIconVisible":Z
    :cond_2
    const/16 v2, 0x8

    goto/16 :goto_1

    .line 162
    :cond_3
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 165
    .restart local v12    # "isAccessoryIconVisible":Z
    :cond_4
    const/16 v2, 0x8

    goto/16 :goto_3

    .line 193
    .restart local v9    # "dMonth":Ljava/lang/String;
    .restart local v10    # "dateFormatterNew":Ljava/text/SimpleDateFormat;
    .restart local v11    # "dateNew":Ljava/lang/String;
    .restart local v14    # "language":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->getLeftTeamHeaderTextView()Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/text/DateFormatSymbols;

    invoke-direct {v4}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v4}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v8, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 201
    .end local v9    # "dMonth":Ljava/lang/String;
    .end local v10    # "dateFormatterNew":Ljava/text/SimpleDateFormat;
    .end local v11    # "dateNew":Ljava/lang/String;
    .end local v14    # "language":Ljava/lang/String;
    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/logutils/log/LogListChildRow;->setTeamHeaderVisible(Z)V

    goto :goto_5
.end method

.method public bindGroupView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "isExpanded"    # Z

    .prologue
    .line 293
    instance-of v0, p1, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;

    if-nez v0, :cond_0

    .line 297
    const-string v0, "WeightLogListConcreteDelegate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " is not an instance of LogListGroupHeader."

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :goto_0
    return-void

    :cond_0
    move-object v7, p1

    .line 301
    check-cast v7, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;

    .line 302
    .local v7, "groupHeaderView":Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;
    const-string/jumbo v0, "month_time"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 303
    .local v3, "monthStartTime":J
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    const-string v0, "avg_value"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getUnitHelper(Landroid/content/Context;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v2

    .line 306
    .local v2, "monthAverageValue":F
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v6

    .line 307
    .local v6, "calendar":Ljava/util/Calendar;
    invoke-virtual {v6, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 308
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getTextView()Landroid/widget/TextView;

    move-result-object v8

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getGroupText(Landroid/content/Context;FJZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getTextView()Landroid/widget/TextView;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getGroupText(Landroid/content/Context;FJZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f09021c

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p4, :cond_1

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f090217

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 315
    if-eqz p4, :cond_2

    .line 318
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getExpandButton()Landroid/widget/ImageButton;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f090b6e

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v8, ""

    invoke-static {v0, v1, v5, v8}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :goto_2
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getExpandButton()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_0

    .line 309
    :cond_1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0901ef

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 325
    :cond_2
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/logutils/log/LogListGroupHeader;->getExpandButton()Landroid/widget/ImageButton;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f090b6d

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v8, ""

    invoke-static {v0, v1, v5, v8}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public getChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 11
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x0

    .line 98
    sget-object v0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate$1;->$SwitchMap$com$sec$android$app$shealth$weight$log$WeightLogFilter:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->mFilter:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 106
    const/4 v7, 0x0

    .line 109
    .local v7, "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    :goto_0
    const/4 v0, 0x5

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id as _id"

    aput-object v1, v6, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "sample_time as day_time"

    aput-object v1, v6, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "weight as amount"

    aput-object v1, v6, v0

    const/4 v0, 0x3

    const-string v1, "comment as memo_text"

    aput-object v1, v6, v0

    const/4 v0, 0x4

    const-string v1, "input_source_type as input_source_type"

    aput-object v1, v6, v0

    .line 116
    .local v6, "projection":[Ljava/lang/String;
    const-string/jumbo v0, "month_time"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 117
    .local v9, "monthTime":J
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getDao()Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    move-result-object v0

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v1

    invoke-static {v9, v10}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v3

    move-object v8, v5

    invoke-interface/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 100
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v7    # "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .end local v9    # "monthTime":J
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getOutsideFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v7

    .line 101
    .restart local v7    # "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    goto :goto_0

    .line 103
    .end local v7    # "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getInsideFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v7

    .line 104
    .restart local v7    # "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getCommentColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 334
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getGroupCursor(Lcom/sec/android/app/shealth/logutils/log/LogFilter;)Landroid/database/Cursor;
    .locals 9
    .param p1, "logFilter"    # Lcom/sec/android/app/shealth/logutils/log/LogFilter;

    .prologue
    const/4 v7, 0x0

    .line 81
    instance-of v0, p1, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "logFilter should be instance of WeightLogFilter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    check-cast p1, Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    .end local p1    # "logFilter":Lcom/sec/android/app/shealth/logutils/log/LogFilter;
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->mFilter:Lcom/sec/android/app/shealth/weight/log/WeightLogFilter;

    .line 86
    const/4 v0, 0x3

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id as _id"

    aput-object v1, v6, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "sample_time as month_time"

    aput-object v1, v6, v0

    const/4 v0, 0x2

    const-string v1, "AVG(weight) as avg_value"

    aput-object v1, v6, v0

    .line 91
    .local v6, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogListConcreteDelegate;->getDao()Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;

    move-result-object v0

    const-wide/high16 v1, -0x8000000000000000L

    const-wide v3, 0x7fffffffffffffffL

    sget-object v5, Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;->MONTH:Lcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;

    move-object v8, v7

    invoke-interface/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;->getPeriodCursorForChartAndLogList(JJLcom/sec/android/app/shealth/common/utils/hcp/ChartUtils$PeriodType;[Ljava/lang/String;Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
