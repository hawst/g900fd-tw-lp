.class public Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
.super Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;
.source "WalkingTimePicker.java"


# static fields
.field public static final TIME_PICKER_DIALOG:Ljava/lang/String; = "time_picker_dialog"

.field public static final TIME_PICKER_INTERVAL:I = 0xa


# instance fields
.field private mCurrentHour:I

.field private mCurrentMin:I

.field private mFromDate:Ljava/util/Date;

.field private mIgnoreEvent:Z

.field private mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/widget/Button;Ljava/util/Date;Ljava/lang/String;)V
    .locals 1
    .param p1, "pickerCaller"    # Landroid/widget/Button;
    .param p2, "fromDate"    # Ljava/util/Date;
    .param p3, "tag"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;-><init>(Landroid/widget/Button;Ljava/util/Date;)V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mIgnoreEvent:Z

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mFromDate:Ljava/util/Date;

    .line 76
    iput-object p3, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mTag:Ljava/lang/String;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mFromDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mIgnoreEvent:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mIgnoreEvent:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mCurrentHour:I

    return p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mCurrentMin:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->calendar:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->calendar:Ljava/util/Calendar;

    return-object v0
.end method


# virtual methods
.method public getCurrentHour()I
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    .line 157
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mCurrentHour:I

    return v0
.end method

.method public getCurrentMinute()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mCurrentMin:I

    return v0
.end method

.method protected getDialogTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->mTag:Ljava/lang/String;

    return-object v0
.end method

.method protected getSimpleDateFormatter()Ljava/text/DateFormat;
    .locals 3

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "HH:mm"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 175
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm a"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method

.method protected initDialog()V
    .locals 4

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->context:Landroid/content/Context;

    const/4 v2, 0x2

    const v3, 0x7f09007a

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v1, 0x7f09004a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f030259

    new-instance v2, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRefreshFocusables(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/WalkingTimePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 142
    return-void
.end method
