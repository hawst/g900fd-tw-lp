.class Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;
.super Ljava/lang/Object;
.source "HeartRateScoverFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->stopSensorAndUpdateUI(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

.field final synthetic val$heartRate:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;I)V
    .locals 0

    .prologue
    .line 1146
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    iput p2, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;->val$heartRate:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1149
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondBpm:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$2000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;->val$heartRate:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1151
    sget-object v0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "stopSensorAndUpdateUI  runOnUiThread"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1152
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$800(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1153
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$800(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1155
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1156
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment$18;->this$0:Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;

    # getter for: Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;->access$000(Lcom/sec/android/app/shealth/heartrate/scover/HeartRateScoverFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1158
    :cond_1
    return-void
.end method
