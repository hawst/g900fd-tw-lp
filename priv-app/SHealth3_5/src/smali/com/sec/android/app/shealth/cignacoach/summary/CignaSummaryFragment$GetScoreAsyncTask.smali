.class Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;
.super Landroid/os/AsyncTask;
.source "CignaSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GetScoreAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;",
        "Ljava/lang/Void;",
        "Lcom/cigna/coach/apiobjects/Scores;",
        ">;"
    }
.end annotation


# instance fields
.field private mRequestCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;)V
    .locals 1

    .prologue
    .line 172
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->mRequestCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;
    .locals 2
    .param p1, "params"    # [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 184
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->mRequestCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 186
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->mRequestCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getScores(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 172
    check-cast p1, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->doInBackground([Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/Scores;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/cigna/coach/apiobjects/Scores;)V
    .locals 3
    .param p1, "scores"    # Lcom/cigna/coach/apiobjects/Scores;

    .prologue
    .line 191
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 193
    if-nez p1, :cond_0

    .line 194
    # getter for: Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetScoreAsyncTask Result is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    # getter for: Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GetScoreAsyncTask Canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_2

    .line 200
    # getter for: Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getActivity() is null"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateHeaderView(Lcom/cigna/coach/apiobjects/Scores;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->access$100(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;Lcom/cigna/coach/apiobjects/Scores;)V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->mRequestCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateNoScoreView(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->access$200(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;ILcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScoreViewTopMargin(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->access$300(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;I)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getCategoryInfo()Ljava/util/Hashtable;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->updateScore(Ljava/util/Hashtable;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->access$400(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;Ljava/util/Hashtable;)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Scores;->getOverallScore()I

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->mOverallScore:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->access$502(Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;I)I

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 172
    check-cast p1, Lcom/cigna/coach/apiobjects/Scores;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/summary/CignaSummaryFragment$GetScoreAsyncTask;->onPostExecute(Lcom/cigna/coach/apiobjects/Scores;)V

    return-void
.end method
