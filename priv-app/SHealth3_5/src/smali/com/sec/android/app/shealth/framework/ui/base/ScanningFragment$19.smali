.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Z)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 25

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopSearch()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v4, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-direct {v4, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x3f

    aput v13, v2, v12

    const/16 v12, -0x5588

    aput v12, v2, v11

    const/16 v11, -0x3d

    aput v11, v2, v10

    const/16 v10, -0x4c82

    aput v10, v2, v9

    const/16 v9, -0x3f

    aput v9, v2, v8

    const/16 v8, -0x37

    aput v8, v2, v7

    const/16 v7, -0x2cc2

    aput v7, v2, v6

    const/16 v6, -0x4f

    aput v6, v2, v5

    const/16 v5, -0x78

    aput v5, v2, v3

    const/16 v3, -0x44b0

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x5c

    aput v14, v1, v13

    const/16 v13, -0x55e5

    aput v13, v1, v12

    const/16 v12, -0x56

    aput v12, v1, v11

    const/16 v11, -0x4cf8

    aput v11, v1, v10

    const/16 v10, -0x4d

    aput v10, v1, v9

    const/16 v9, -0x54

    aput v9, v1, v8

    const/16 v8, -0x2c93

    aput v8, v1, v7

    const/16 v7, -0x2d

    aput v7, v1, v6

    const/16 v6, -0x1f

    aput v6, v1, v5

    const/16 v5, -0x44c4

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, -0x26

    aput v14, v2, v13

    const/16 v13, -0x3a

    aput v13, v2, v12

    const/4 v12, -0x1

    aput v12, v2, v11

    const/16 v11, -0x5c9f

    aput v11, v2, v10

    const/16 v10, -0x36

    aput v10, v2, v9

    const/16 v9, -0x46ef

    aput v9, v2, v8

    const/4 v8, -0x2

    aput v8, v2, v7

    const/16 v7, -0x8c5

    aput v7, v2, v3

    const/16 v3, -0x5d

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/4 v15, -0x6

    aput v15, v1, v14

    const/4 v14, -0x5

    aput v14, v1, v13

    const/16 v13, -0x21

    aput v13, v1, v12

    const/16 v12, -0x5cee

    aput v12, v1, v11

    const/16 v11, -0x5d

    aput v11, v1, v10

    const/16 v10, -0x46cf

    aput v10, v1, v9

    const/16 v9, -0x47

    aput v9, v1, v8

    const/16 v8, -0x886

    aput v8, v1, v7

    const/16 v7, -0x9

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v7, v1

    if-lt v3, v7, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x64a3

    aput v11, v2, v10

    const/16 v10, -0x2e

    aput v10, v2, v9

    const/16 v9, -0x4eda

    aput v9, v2, v8

    const/16 v8, -0x2e

    aput v8, v2, v7

    const/16 v7, 0x194a

    aput v7, v2, v6

    const/16 v6, -0x4891

    aput v6, v2, v5

    const/16 v5, -0x2e

    aput v5, v2, v3

    const/16 v3, -0x1dee

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x64e7

    aput v12, v1, v11

    const/16 v11, -0x65

    aput v11, v1, v10

    const/16 v10, -0x4ebd

    aput v10, v1, v9

    const/16 v9, -0x4f

    aput v9, v1, v8

    const/16 v8, 0x1923

    aput v8, v1, v7

    const/16 v7, -0x48e7

    aput v7, v1, v6

    const/16 v6, -0x49

    aput v6, v1, v5

    const/16 v5, -0x1daa

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x7a

    aput v13, v2, v12

    const/16 v12, 0x4a54

    aput v12, v2, v11

    const/16 v11, 0x322b

    aput v11, v2, v10

    const/16 v10, 0x787c

    aput v10, v2, v9

    const/16 v9, -0x2be3

    aput v9, v2, v8

    const/16 v8, -0x49

    aput v8, v2, v7

    const/16 v7, -0x7ed

    aput v7, v2, v6

    const/16 v6, -0x72

    aput v6, v2, v5

    const/4 v5, -0x4

    aput v5, v2, v3

    const/16 v3, -0x48

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x1d

    aput v14, v1, v13

    const/16 v13, 0x4a39

    aput v13, v1, v12

    const/16 v12, 0x324a

    aput v12, v1, v11

    const/16 v11, 0x7832

    aput v11, v1, v10

    const/16 v10, -0x2b88

    aput v10, v1, v9

    const/16 v9, -0x2c

    aput v9, v1, v8

    const/16 v8, -0x786

    aput v8, v1, v7

    const/4 v7, -0x8

    aput v7, v1, v6

    const/16 v6, -0x67

    aput v6, v1, v5

    const/4 v5, -0x4

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x2b

    aput v11, v2, v10

    const/16 v10, -0x30fe

    aput v10, v2, v9

    const/16 v9, -0x4a

    aput v9, v2, v8

    const/16 v8, -0x17

    aput v8, v2, v7

    const/16 v7, 0x2d23

    aput v7, v2, v6

    const/16 v6, 0x2d59

    aput v6, v2, v5

    const/16 v5, 0x324c

    aput v5, v2, v3

    const/16 v3, -0x708a

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x50

    aput v12, v1, v11

    const/16 v11, -0x308e

    aput v11, v1, v10

    const/16 v10, -0x31

    aput v10, v1, v9

    const/16 v9, -0x43

    aput v9, v1, v8

    const/16 v8, 0x2d42

    aput v8, v1, v7

    const/16 v7, 0x2d2d

    aput v7, v1, v6

    const/16 v6, 0x322d    # 1.8E-41f

    aput v6, v1, v5

    const/16 v5, -0x70ce

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceDataType()I

    move-result v1

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x58

    aput v13, v2, v12

    const/16 v12, -0x19

    aput v12, v2, v11

    const/16 v11, -0x16

    aput v11, v2, v10

    const/16 v10, 0x3021

    aput v10, v2, v9

    const/16 v9, -0xaab

    aput v9, v2, v8

    const/16 v8, -0x6a

    aput v8, v2, v7

    const/16 v7, -0x34

    aput v7, v2, v6

    const/16 v6, -0x35

    aput v6, v2, v5

    const/16 v5, -0x291

    aput v5, v2, v3

    const/16 v3, -0x47

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x33

    aput v14, v1, v13

    const/16 v13, -0x69

    aput v13, v1, v12

    const/16 v12, -0x6d

    aput v12, v1, v11

    const/16 v11, 0x3075

    aput v11, v1, v10

    const/16 v10, -0xad0

    aput v10, v1, v9

    const/16 v9, -0xb

    aput v9, v1, v8

    const/16 v8, -0x5b

    aput v8, v1, v7

    const/16 v7, -0x43

    aput v7, v1, v6

    const/16 v6, -0x2f6

    aput v6, v1, v5

    const/4 v5, -0x3

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v1

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x1f0d

    aput v19, v2, v18

    const/16 v18, 0x3c6f

    aput v18, v2, v17

    const/16 v17, -0x72bb

    aput v17, v2, v16

    const/16 v16, -0x27

    aput v16, v2, v15

    const/4 v15, -0x2

    aput v15, v2, v14

    const/16 v14, -0x44

    aput v14, v2, v13

    const/16 v13, 0x3f2c

    aput v13, v2, v12

    const/16 v12, 0x1849

    aput v12, v2, v11

    const/16 v11, -0x588f

    aput v11, v2, v10

    const/16 v10, -0x2d

    aput v10, v2, v9

    const/16 v9, 0x768

    aput v9, v2, v8

    const/16 v8, 0x662

    aput v8, v2, v7

    const/16 v7, 0x2a68

    aput v7, v2, v6

    const/16 v6, 0x5744

    aput v6, v2, v5

    const/16 v5, 0x5638

    aput v5, v2, v3

    const/16 v3, -0x1deb

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x1f68

    aput v20, v1, v19

    const/16 v19, 0x3c1f

    aput v19, v1, v18

    const/16 v18, -0x72c4

    aput v18, v1, v17

    const/16 v17, -0x73

    aput v17, v1, v16

    const/16 v16, -0x79

    aput v16, v1, v15

    const/16 v15, -0x38

    aput v15, v1, v14

    const/16 v14, 0x3f45

    aput v14, v1, v13

    const/16 v13, 0x183f

    aput v13, v1, v12

    const/16 v12, -0x58e8

    aput v12, v1, v11

    const/16 v11, -0x59

    aput v11, v1, v10

    const/16 v10, 0x70b

    aput v10, v1, v9

    const/16 v9, 0x607

    aput v9, v1, v8

    const/16 v8, 0x2a06

    aput v8, v1, v7

    const/16 v7, 0x572a

    aput v7, v1, v6

    const/16 v6, 0x5657

    aput v6, v1, v5

    const/16 v5, -0x1daa

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v1

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$19;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_f
    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_9
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x74

    aput v12, v2, v11

    const/16 v11, -0x21cc

    aput v11, v2, v10

    const/16 v10, -0x49

    aput v10, v2, v9

    const/16 v9, 0x2120

    aput v9, v2, v8

    const/16 v8, 0x2653

    aput v8, v2, v7

    const/16 v7, 0x6b43

    aput v7, v2, v6

    const/16 v6, -0x3cc8

    aput v6, v2, v5

    const/16 v5, -0x5f

    aput v5, v2, v4

    const/16 v4, -0x23

    aput v4, v2, v3

    const/16 v3, -0x7c

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x17

    aput v13, v1, v12

    const/16 v12, -0x21a9

    aput v12, v1, v11

    const/16 v11, -0x22

    aput v11, v1, v10

    const/16 v10, 0x2156

    aput v10, v1, v9

    const/16 v9, 0x2621

    aput v9, v1, v8

    const/16 v8, 0x6b26

    aput v8, v1, v7

    const/16 v7, -0x3c95

    aput v7, v1, v6

    const/16 v6, -0x3d

    aput v6, v1, v5

    const/16 v5, -0x4c

    aput v5, v1, v4

    const/16 v4, -0x18

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v4, v1

    if-lt v3, v4, :cond_f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v4, v1

    if-lt v3, v4, :cond_10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, -0x428c

    aput v23, v2, v22

    const/16 v22, -0x64

    aput v22, v2, v21

    const/16 v21, -0x13b9

    aput v21, v2, v20

    const/16 v20, -0x61

    aput v20, v2, v19

    const/16 v19, -0x6aa3

    aput v19, v2, v18

    const/16 v18, -0xa

    aput v18, v2, v17

    const/16 v17, -0x34e5

    aput v17, v2, v16

    const/16 v16, -0x43

    aput v16, v2, v15

    const/16 v15, 0x2840

    aput v15, v2, v14

    const/16 v14, 0x314c

    aput v14, v2, v13

    const/16 v13, 0x811

    aput v13, v2, v12

    const/16 v12, 0x276c

    aput v12, v2, v11

    const/16 v11, -0x4dbe

    aput v11, v2, v10

    const/16 v10, -0x40

    aput v10, v2, v9

    const/16 v9, -0x1a

    aput v9, v2, v8

    const/16 v8, -0x77

    aput v8, v2, v7

    const/16 v7, 0x72

    aput v7, v2, v6

    const/16 v6, -0xde0

    aput v6, v2, v5

    const/16 v5, -0x63

    aput v5, v2, v3

    const/16 v3, -0x7f

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x42ab

    aput v24, v1, v23

    const/16 v23, -0x43

    aput v23, v1, v22

    const/16 v22, -0x139a

    aput v22, v1, v21

    const/16 v21, -0x14

    aput v21, v1, v20

    const/16 v20, -0x6ac8

    aput v20, v1, v19

    const/16 v19, -0x6b

    aput v19, v1, v18

    const/16 v18, -0x348e

    aput v18, v1, v17

    const/16 v17, -0x35

    aput v17, v1, v16

    const/16 v16, 0x2825

    aput v16, v1, v15

    const/16 v15, 0x3128

    aput v15, v1, v14

    const/16 v14, 0x831

    aput v14, v1, v13

    const/16 v13, 0x2708

    aput v13, v1, v12

    const/16 v12, -0x4dd9

    aput v12, v1, v11

    const/16 v11, -0x4e

    aput v11, v1, v10

    const/16 v10, -0x71

    aput v10, v1, v9

    const/16 v9, -0x18

    aput v9, v1, v8

    const/16 v8, 0x22

    aput v8, v1, v7

    const/16 v7, -0xe00

    aput v7, v1, v6

    const/16 v6, -0xe

    aput v6, v1, v5

    const/16 v5, -0x31

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_11

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_f

    :cond_f
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_10
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_11
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_12
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :catch_1
    move-exception v1

    goto/16 :goto_e
.end method
