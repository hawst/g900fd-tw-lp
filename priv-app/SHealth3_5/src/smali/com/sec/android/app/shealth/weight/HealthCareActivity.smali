.class public abstract Lcom/sec/android/app/shealth/weight/HealthCareActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "HealthCareActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;


# static fields
.field private static final TAG:Ljava/lang/String; = "Weight HealthCareActvity"


# instance fields
.field private final SUMMARYVIEW_DEFAULT_ACTIONBAR_BUTTON_COUNT:I

.field private actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field private mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 92
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->SUMMARYVIEW_DEFAULT_ACTIONBAR_BUTTON_COUNT:I

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/HealthCareActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/HealthCareActivity;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->onLogSelected()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/HealthCareActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/HealthCareActivity;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->prepareShareView()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 137
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarTitleId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 140
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207c0

    new-instance v4, Lcom/sec/android/app/shealth/weight/HealthCareActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity$1;-><init>(Lcom/sec/android/app/shealth/weight/HealthCareActivity;)V

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 147
    .local v2, "logButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f09005a

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 151
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207bc

    new-instance v4, Lcom/sec/android/app/shealth/weight/HealthCareActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity$2;-><init>(Lcom/sec/android/app/shealth/weight/HealthCareActivity;)V

    invoke-direct {v1, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 158
    .local v1, "connectionsButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f0907b4

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 161
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207cf

    new-instance v4, Lcom/sec/android/app/shealth/weight/HealthCareActivity$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity$3;-><init>(Lcom/sec/android/app/shealth/weight/HealthCareActivity;)V

    invoke-direct {v0, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 172
    .local v0, "actionBarShareviaButtonBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f090033

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 173
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 177
    return-void
.end method

.method protected abstract getActionBarTitleId()Ljava/lang/String;
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    return-object v0
.end method

.method protected getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 195
    .local v1, "fragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;, "TT;"
    if-nez v1, :cond_0

    .line 197
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "fragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;, "TT;"
    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 204
    .restart local v1    # "fragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;, "TT;"
    :cond_0
    return-object v1

    .line 198
    .end local v1    # "fragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;, "TT;"
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/InstantiationException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 200
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/IllegalAccessException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected abstract getGraphFragmentClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getSummaryFragmentClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;",
            ">;"
        }
    .end annotation
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getGraphFragmentClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->switchFragmentToSummary()V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    if-nez p1, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f080091

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 105
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->isDrawerMenuShown()Z

    move-result v1

    .line 115
    .local v1, "isDrawerMenuShown":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getGraphFragmentClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->isVisible()Z

    move-result v2

    if-nez v2, :cond_2

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    .line 118
    .local v0, "i":I
    :goto_0
    if-le v0, v6, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    add-int/lit8 v5, v0, -0x1

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    goto :goto_0

    .line 122
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    if-nez v1, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {v5, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 131
    .end local v0    # "i":I
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;

    if-nez v1, :cond_4

    move v5, v3

    :goto_3
    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->setHasOptionsMenu(Z)V

    .line 132
    if-nez v1, :cond_5

    :goto_4
    return v3

    .restart local v0    # "i":I
    :cond_1
    move v2, v4

    .line 122
    goto :goto_1

    .line 124
    .end local v0    # "i":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v2

    if-ne v2, v6, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    if-eqz v2, :cond_3

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v5, v3, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->actionBarShareviaButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v6, v5, v4

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 129
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_2

    :cond_4
    move v5, v4

    .line 131
    goto :goto_3

    :cond_5
    move v3, v4

    .line 132
    goto :goto_4
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 109
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 110
    return-void
.end method

.method protected abstract startConnectivityActivity()V
.end method

.method public switchFragmentToGraph()V
    .locals 6

    .prologue
    .line 231
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    if-nez v4, :cond_0

    .line 232
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getGraphFragmentClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    .line 237
    .local v1, "graphFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    :goto_0
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->isAdded()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 250
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    :goto_1
    return-void

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    .line 235
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 247
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    :catch_0
    move-exception v0

    .line 248
    .local v0, "exception":Ljava/lang/IllegalStateException;
    const-string v4, "Weight HealthCareActvity"

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 241
    .end local v0    # "exception":Ljava/lang/IllegalStateException;
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    .line 242
    .local v2, "summaryFragment":Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;->addBundleToGraphFragment(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 244
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v4, 0x7f080091

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v1, v5}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 245
    invoke-virtual {v3, v2}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 246
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public switchFragmentToSummary()V
    .locals 4

    .prologue
    .line 216
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getGraphFragmentClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 218
    .local v1, "transaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 220
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    .end local v1    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :goto_0
    return-void

    .line 221
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "Weight HealthCareActvity"

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
