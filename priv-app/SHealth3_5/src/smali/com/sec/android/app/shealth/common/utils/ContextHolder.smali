.class public Lcom/sec/android/app/shealth/common/utils/ContextHolder;
.super Ljava/lang/Object;
.source "ContextHolder.java"


# static fields
.field private static volatile mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    sput-object p0, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method
