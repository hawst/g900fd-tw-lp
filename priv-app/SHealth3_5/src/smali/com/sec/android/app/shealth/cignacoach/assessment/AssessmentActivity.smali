.class public Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "AssessmentActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;
.implements Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask$OnFinishListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$6;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAllCategoryForReassess:Z

.field private mAssessmentCancelBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private mAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

.field private mAssessmentDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;",
            ">;"
        }
    .end annotation
.end field

.field private mAssessmentDisplay:Z

.field private mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

.field private mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field private mCategoryForNoti:I

.field private mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

.field private mFromCoachMessage:Z

.field private mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

.field private mPagerChanging:Z

.field private mShowWeightTrackingDialog:Z

.field private mTrackerDataLoadingTask:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

.field private mWeightCategoryIndex:I

.field private paused:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategoryForNoti:I

    .line 59
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 60
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 61
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->paused:I

    .line 68
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mWeightCategoryIndex:I

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAllCategoryForReassess:Z

    .line 71
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mPagerChanging:Z

    .line 72
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDisplay:Z

    .line 73
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mShowWeightTrackingDialog:Z

    .line 74
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mFromCoachMessage:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->showTrackingDialog(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getCategoryIconRes(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;)Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mTrackerDataLoadingTask:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;)Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mTrackerDataLoadingTask:Lcom/sec/android/app/shealth/cignacoach/tracker/TrackerDataLoadingAsyncTask;

    return-object p1
.end method

.method private dismissTrackingDialog()V
    .locals 3

    .prologue
    .line 560
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "TRACKING_DIALOG"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 561
    .local v0, "disalog":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    if-eqz v0, :cond_0

    .line 562
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 564
    :cond_0
    return-void
.end method

.method private displayAssessment()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 507
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDatas:Ljava/util/ArrayList;

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_3

    .line 510
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getAssessmentData(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDatas:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    :cond_0
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDatas:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mWeightCategoryIndex:I

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->setOnItemClickListener(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView$OnItemClickListener;)V

    .line 520
    const v0, 0x7f0800d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getCount()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;->NUMBER:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorType(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;II)V

    .line 528
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setPagingEnabled(Z)V

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eq v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    array-length v0, v0

    if-ne v0, v4, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aget-object v0, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-ne v0, v1, :cond_5

    .line 536
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorVisibility(Z)V

    .line 545
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getTrackingDialogTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0901f3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 547
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDisplay:Z

    .line 548
    return-void

    .line 513
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_0

    .line 514
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getAssessmentData([Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDatas:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 526
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;->NUMBER:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQADataCount()I

    move-result v0

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorType(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$TYPE;II)V

    goto :goto_1

    .line 538
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorVisibility(Z)V

    goto :goto_2

    .line 541
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setIndicatorVisibility(Z)V

    goto :goto_2
.end method

.method private getCategoryIconRes(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)I
    .locals 3
    .param p1, "category"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 454
    const/4 v0, 0x0

    .line 455
    .local v0, "iconResID":I
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$6;->$SwitchMap$com$sec$android$app$shealth$cignacoach$constants$CignaConstants$LIFE_STYLE:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 479
    :goto_0
    return v0

    .line 457
    :pswitch_0
    const v0, 0x7f020105

    .line 458
    goto :goto_0

    .line 460
    :pswitch_1
    const v0, 0x7f020107

    .line 461
    goto :goto_0

    .line 463
    :pswitch_2
    const v0, 0x7f020108

    .line 464
    goto :goto_0

    .line 466
    :pswitch_3
    const v0, 0x7f020106

    .line 467
    goto :goto_0

    .line 469
    :pswitch_4
    const v0, 0x7f020109

    .line 470
    goto :goto_0

    .line 455
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getCategoryString([Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Ljava/lang/String;
    .locals 6
    .param p1, "categorys"    # [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 576
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 577
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 578
    aget-object v1, p1, v0

    .line 579
    .local v1, "lifeStyle":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-ne v1, v4, :cond_0

    .line 580
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->getStringResID()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 577
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 593
    .end local v1    # "lifeStyle":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 594
    .local v2, "result":Ljava/lang/String;
    return-object v2
.end method

.method private getFirstCategory()Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .locals 2

    .prologue
    .line 567
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 572
    :goto_0
    return-object v0

    .line 569
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    .line 572
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getLifeStyleByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .locals 3
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 598
    const/4 v0, 0x0

    .line 599
    .local v0, "lifeStyle":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 616
    :goto_0
    return-object v0

    .line 601
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 602
    goto :goto_0

    .line 604
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 605
    goto :goto_0

    .line 607
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 608
    goto :goto_0

    .line 610
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 611
    goto :goto_0

    .line 613
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    goto :goto_0

    .line 599
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getTrackingDialogTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->getStringResID()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 503
    :goto_0
    return-object v0

    .line 498
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_1

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getCategoryString([Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 503
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showAssessmentFeedbackDialog()V
    .locals 3

    .prologue
    .line 411
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f090c62

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090c6f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentCancelBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentCancelBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 431
    return-void
.end method

.method private showTrackingDialog(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V
    .locals 3
    .param p1, "cagetory"    # Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .prologue
    .line 434
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->getStringResID()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f030086

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setDoNotDismissOnBackPressed(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "TRACKING_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 451
    return-void
.end method

.method private startCignaCoachActivity(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 385
    .local p1, "earnedBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 386
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 387
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 388
    const-string v1, "EXTRA_NAME_FIRST_ASSESSMENT_COMPLETE_CATEGORY"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 389
    const-string v1, "intent_need_cigna_main_update"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 390
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 391
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v2, 0x115c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 392
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 394
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 395
    return-void
.end method

.method private startCignaCoachActivityForAnswerQuestion(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 398
    .local p1, "earnedBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 399
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 400
    const-string v1, "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 401
    const-string v1, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 402
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 403
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v2, 0x115c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 404
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 406
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 407
    return-void
.end method

.method private updateTrackingData()V
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->updateTrackingData()V

    .line 621
    return-void
.end method


# virtual methods
.method public changeActionBar(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 3
    .param p1, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 174
    const/4 v0, 0x0

    .line 175
    .local v0, "lifeStyle":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 193
    :goto_0
    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->getStringResID()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 198
    :cond_0
    return-void

    .line 177
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 178
    goto :goto_0

    .line 180
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 181
    goto :goto_0

    .line 183
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 184
    goto :goto_0

    .line 186
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 187
    goto :goto_0

    .line 189
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    goto :goto_0

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public closeScreen()V
    .locals 1

    .prologue
    .line 166
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getAnswerCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 167
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->showAssessmentFeedbackDialog()V

    .line 171
    :goto_0
    return-void

    .line 169
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected customizeActionBar()V
    .locals 3

    .prologue
    .line 136
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->getStringResID()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->getStringResID()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_0
.end method

.method public goBackScreenForSwipe()V
    .locals 3

    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mPagerChanging:Z

    if-eqz v0, :cond_1

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getAnswerCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 378
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v0

    if-lez v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setCurrentItem(IZ)V

    goto :goto_0
.end method

.method public goNextScreenForSwipe()V
    .locals 3

    .prologue
    .line 360
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mPagerChanging:Z

    if-eqz v0, :cond_1

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getAnswerCount()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 366
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setCurrentItem(IZ)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v0, -0x1

    .line 552
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 554
    if-ne p2, v0, :cond_0

    .line 555
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->setResult(I)V

    .line 557
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 352
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getAnswerCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 353
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->showAssessmentFeedbackDialog()V

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;)V
    .locals 15
    .param p1, "v"    # Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentView;

    .prologue
    .line 223
    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    iget-object v13, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getAssessmentAndQAIndex(I)[I

    move-result-object v5

    .line 224
    .local v5, "currentPageInfo":[I
    const/4 v12, 0x0

    aget v12, v5, v12

    iget-object v13, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ge v12, v13, :cond_2

    .line 226
    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    const/4 v13, 0x0

    aget v13, v5, v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getAssessmentData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    move-result-object v4

    .line 228
    .local v4, "currentAssessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    const/4 v12, 0x1

    aget v12, v5, v12

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getQADataCount()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ne v12, v13, :cond_2

    .line 230
    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    const/4 v13, 0x0

    aget v13, v5, v13

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getAssessmentData(I)Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    move-result-object v11

    .line 232
    .local v11, "nextAssessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v12

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v13

    if-eq v12, v13, :cond_2

    .line 234
    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v12

    sget-object v13, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    if-eq v12, v13, :cond_0

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v12

    sget-object v13, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    if-ne v12, v13, :cond_2

    .line 237
    :cond_0
    iget-boolean v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mShowWeightTrackingDialog:Z

    if-nez v12, :cond_2

    .line 239
    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getLifeStyleByCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    move-result-object v9

    .line 240
    .local v9, "lifeStyle":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    if-nez v9, :cond_1

    .line 241
    sget-object v9, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 242
    :cond_1
    invoke-direct {p0, v9}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->showTrackingDialog(Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V

    .line 243
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mShowWeightTrackingDialog:Z

    .line 250
    .end local v4    # "currentAssessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v9    # "lifeStyle":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    .end local v11    # "nextAssessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    :cond_2
    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentPageAdapter:Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentPageAdapter;->getCount()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    if-ge v12, v13, :cond_4

    .line 252
    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    iget-object v13, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->getCurrentItem()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setCurrentItem(IZ)V

    .line 345
    :cond_3
    :goto_0
    return-void

    .line 256
    :cond_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 257
    .local v6, "earnedBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    if-eqz v12, :cond_a

    .line 259
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    invoke-virtual {v12, v13}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->sendAnswerData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v10

    .line 260
    .local v10, "messageBadgeResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;>;"
    if-nez v10, :cond_5

    .line 261
    sget-object v12, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->TAG:Ljava/lang/String;

    const-string v13, "Becuase messageBadgeResponse is null, finish"

    invoke-static {v12, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->finish()V

    goto :goto_0

    .line 265
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentData:Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v13

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCoachMessage(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 267
    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_6

    .line 268
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v13

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    invoke-virtual {v13, v12}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getBadgeIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 271
    :cond_6
    iget-boolean v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mFromCoachMessage:Z

    if-nez v12, :cond_8

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->isCignaCoachInited()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getAssessmentCategoryCount()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_8

    .line 274
    :cond_7
    const/4 v12, 0x1

    invoke-static {v12}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setIsCignaCoachInited(Z)V

    .line 275
    const/4 v12, 0x1

    invoke-static {v12}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->setisCignaCoachCompleteAssessment(Z)V

    .line 277
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->startCignaCoachActivity(Ljava/util/ArrayList;)V

    .line 291
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->finish()V

    goto :goto_0

    .line 281
    :cond_8
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 282
    .local v8, "intent":Landroid/content/Intent;
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lez v12, :cond_9

    .line 286
    const-string v12, "intent_lifestyle_category"

    iget-object v13, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 287
    const-string v12, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v8, v12, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 289
    :cond_9
    const/4 v12, -0x1

    invoke-virtual {p0, v12, v8}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1

    .line 293
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v10    # "messageBadgeResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;>;"
    :cond_a
    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDatas:Ljava/util/ArrayList;

    if-eqz v12, :cond_3

    .line 294
    const/4 v1, 0x0

    .line 295
    .local v1, "cancelGoalCoachMsg":Lcom/cigna/coach/apiobjects/CoachMessage;
    const/4 v3, 0x0

    .line 296
    .local v3, "cancelGoalMessage":Ljava/lang/String;
    const/4 v2, 0x0

    .line 299
    .local v2, "cancelGoalDescription":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalCount()I

    move-result v12

    if-lez v12, :cond_c

    const/4 v12, 0x1

    :goto_2
    iput-boolean v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAllCategoryForReassess:Z

    .line 301
    iget-object v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDatas:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_b
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;

    .line 303
    .local v0, "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v12

    invoke-virtual {v12, v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->sendAnswerData(Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v10

    .line 304
    .restart local v10    # "messageBadgeResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;>;"
    if-nez v10, :cond_d

    .line 305
    sget-object v12, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->TAG:Ljava/lang/String;

    const-string v13, "Becuase messageBadgeResponse is null, finish-1"

    invoke-static {v12, v13}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->finish()V

    goto/16 :goto_0

    .line 299
    .end local v0    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v10    # "messageBadgeResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;>;"
    :cond_c
    const/4 v12, 0x0

    goto :goto_2

    .line 309
    .restart local v0    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v10    # "messageBadgeResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;>;"
    :cond_d
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v12

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v13

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCoachMessage(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v1

    .line 311
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v12

    sget-object v13, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v12, v13, :cond_e

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_e

    .line 313
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessage()Ljava/lang/String;

    move-result-object v3

    .line 314
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v2

    .line 316
    :cond_e
    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_b

    .line 317
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v13

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    invoke-virtual {v13, v12}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getBadgeIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 323
    .end local v0    # "assessmentData":Lcom/sec/android/app/shealth/cignacoach/data/AssessmentData;
    .end local v10    # "messageBadgeResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;>;"
    :cond_f
    iget-boolean v12, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAllCategoryForReassess:Z

    if-eqz v12, :cond_11

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_11

    .line 326
    new-instance v8, Landroid/content/Intent;

    const-class v12, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;

    invoke-direct {v8, p0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 327
    .restart local v8    # "intent":Landroid/content/Intent;
    const-string v12, "intent_reassess_state"

    const/4 v13, 0x3

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 328
    const-string v12, "intent_cancel_goal_unaligned_message"

    invoke-virtual {v8, v12, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    const-string v12, "intent_cancel_goal_unaligned_description"

    invoke-virtual {v8, v12, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 331
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-lez v12, :cond_10

    .line 333
    const-string v12, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v13, 0x115c

    invoke-virtual {v8, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 334
    const-string v12, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v8, v12, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 336
    :cond_10
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->finish()V

    goto/16 :goto_0

    .line 340
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_11
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->startCignaCoachActivityForAnswerQuestion(Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 81
    .local v2, "receiveIntent":Landroid/content/Intent;
    if-eqz v2, :cond_3

    .line 82
    const-string v5, "intent_lifestyle_category"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 83
    const-string v5, "EXTRA_NAME_FROM_COACH_MESSAGE"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mFromCoachMessage:Z

    .line 85
    const-string v5, "intent_lifestyle_categorys"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, [Ljava/lang/Object;

    move-object v3, v5

    check-cast v3, [Ljava/lang/Object;

    .line 87
    .local v3, "serializableData":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-ne v5, v6, :cond_0

    .line 88
    iput v7, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mWeightCategoryIndex:I

    .line 91
    :cond_0
    if-eqz v3, :cond_2

    .line 92
    array-length v4, v3

    .line 93
    .local v4, "serializableDataLength":I
    if-lez v4, :cond_2

    .line 94
    new-array v5, v4, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 95
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 96
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aget-object v5, v3, v1

    check-cast v5, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aput-object v5, v6, v1

    .line 97
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategorys:[Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    aget-object v5, v5, v1

    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-ne v5, v6, :cond_1

    .line 98
    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mWeightCategoryIndex:I

    .line 95
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    .end local v1    # "i":I
    .end local v4    # "serializableDataLength":I
    :cond_2
    const-string v5, "EXTRA_NAME_ASSESSMENT_CATEGORY_NOTI"

    invoke-virtual {v2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategoryForNoti:I

    .line 106
    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategoryForNoti:I

    if-eq v5, v8, :cond_3

    .line 107
    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategoryForNoti:I

    const/16 v6, 0x68

    if-ne v5, v6, :cond_3

    .line 108
    iput v7, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mWeightCategoryIndex:I

    .line 109
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mCategory:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 115
    .end local v3    # "serializableData":[Ljava/lang/Object;
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 116
    const v5, 0x7f030028

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->setContentView(I)V

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getFirstCategory()Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    move-result-object v0

    .line 119
    .local v0, "firstCategory":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-eq v0, v5, :cond_4

    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    if-ne v0, v5, :cond_5

    .line 122
    :cond_4
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$1;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;)V

    const-wide/16 v7, 0x12c

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 132
    :goto_1
    return-void

    .line 130
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->displayAssessment()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 155
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentUserSelectAnswer;->clearSelectedAnswer()V

    .line 156
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->clearClickCount()V

    .line 157
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->clearAssessmentWeightHeightValue()V

    .line 158
    return-void
.end method

.method public onFinish(Z)V
    .locals 1
    .param p1, "result"    # Z

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->dismissTrackingDialog()V

    .line 486
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mAssessmentDisplay:Z

    if-nez v0, :cond_0

    .line 487
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->displayAssessment()V

    .line 489
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->updateTrackingData()V

    .line 490
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mPagerChanging:Z

    .line 203
    if-nez p1, :cond_0

    .line 204
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/AssessmentClickUtils;->clearClickCount()V

    .line 206
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->mPagerChanging:Z

    .line 210
    :cond_1
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 214
    return-void
.end method

.method public onPageSelected(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 218
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 625
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 626
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 627
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 629
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 630
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$5;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;Landroid/view/View;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 638
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 149
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onStop()V

    .line 150
    return-void
.end method
