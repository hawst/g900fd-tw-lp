.class public Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;
.super Landroid/database/ContentObserver;
.source "SleepMonitorGraphFragmentSIC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SleepDBContentObserver1"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .line 221
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 222
    return-void
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 225
    invoke-super {p0}, Landroid/database/ContentObserver;->deliverSelfNotifications()Z

    move-result v0

    return v0
.end method

.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .prologue
    .line 229
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->updateGraphFragment()V

    .line 232
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 0
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 235
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 236
    return-void
.end method
