.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;
.super Ljava/lang/Object;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndUpdateUI(IF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

.field final synthetic val$heartRate:I

.field final synthetic val$o2Data:F


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;FI)V
    .locals 0

    .prologue
    .line 1787
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iput p2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->val$o2Data:F

    iput p3, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->val$heartRate:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1791
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondO2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1300(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->val$o2Data:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1792
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondBpm:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->val$heartRate:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1793
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->val$o2Data:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->moveToPolygon(D)V

    .line 1794
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1795
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1796
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$1000(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1797
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1798
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$2600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$702(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    .line 1800
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->val$o2Data:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$26;->val$heartRate:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->insertData(FF)V

    .line 1801
    return-void
.end method
