.class public Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;
.super Ljava/lang/Object;
.source "TimeXYRealTimeScrollDataChangeListener.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;


# instance fields
.field bUserRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public chart:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

.field count:I

.field private mContext:Landroid/app/Activity;

.field mLastLeftDataInfo:J

.field mLastRightDataInfo:J

.field private mStartTime:J


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;Landroid/app/Activity;)V
    .locals 3
    .param p1, "chart"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    const-wide/16 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->count:I

    .line 23
    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->mLastRightDataInfo:J

    .line 24
    iput-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->mLastLeftDataInfo:J

    .line 27
    const-wide v0, 0x13eeba9e4a0L

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->mStartTime:J

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->chart:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->bUserRequest:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 31
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->mContext:Landroid/app/Activity;

    .line 32
    return-void
.end method


# virtual methods
.method public UserRequestData2()V
    .locals 20

    .prologue
    .line 67
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 68
    .local v10, "calendar":Ljava/util/Calendar;
    const-wide/16 v2, 0x1

    invoke-virtual {v10, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 69
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v10, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 71
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->chart:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->getEpochRightEndofData()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->mStartTime:J

    .line 73
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v16, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->clear()V

    .line 75
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v17, "listRawDatas2":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->clear()V

    .line 78
    const/4 v4, 0x0

    .line 79
    .local v4, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exercise__id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 80
    .local v5, "selectionClause":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND sync_status != 170004 AND create_time > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->mStartTime:J

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 81
    const/4 v4, 0x0

    .line 82
    const/4 v11, 0x0

    .line 84
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->mContext:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const-string v7, "create_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 85
    if-eqz v11, :cond_2

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2

    .line 86
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 87
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 89
    const-string v2, "create_time"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 90
    .local v18, "sampleTime":J
    const-string/jumbo v2, "speed_per_hour"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v6

    double-to-int v2, v2

    int-to-double v8, v2

    .line 91
    .local v8, "avg_speed":D
    const-string v2, "heart_rate_per_min"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v14

    .line 93
    .local v14, "heartRatePerMin":D
    new-instance v12, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v12}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 94
    .local v12, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-wide/from16 v0, v18

    invoke-virtual {v12, v0, v1, v8, v9}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 95
    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    new-instance v13, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v13}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 98
    .local v13, "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-wide/from16 v0, v18

    invoke-virtual {v13, v0, v1, v14, v15}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JD)V

    .line 99
    move-object/from16 v0, v17

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 108
    .end local v8    # "avg_speed":D
    .end local v12    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v13    # "data2":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    .end local v14    # "heartRatePerMin":D
    .end local v18    # "sampleTime":J
    :catchall_0
    move-exception v2

    if-eqz v11, :cond_0

    .line 109
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v2

    .line 104
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->chart:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;

    const/4 v3, 0x0

    const/16 v6, 0xc8

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->addDataWithAni(ILjava/util/List;I)V

    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/TimeXYRealTimeScrollDataChangeListener;->chart:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;

    const/4 v3, 0x1

    const/16 v6, 0xc8

    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v0, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->addDataWithAni(ILjava/util/List;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    :cond_2
    if-eqz v11, :cond_3

    .line 109
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 111
    :cond_3
    return-void
.end method

.method public onLeftScrollDataChange(J)V
    .locals 0
    .param p1, "dataInfo"    # J

    .prologue
    .line 39
    return-void
.end method

.method public onLeftScrollDataChangeComming(J)V
    .locals 0
    .param p1, "dataInfo"    # J

    .prologue
    .line 50
    return-void
.end method

.method public onRightScrollDataChange(J)V
    .locals 0
    .param p1, "dataInfo"    # J

    .prologue
    .line 44
    return-void
.end method

.method public onRightScrollDataChangeComming(J)V
    .locals 0
    .param p1, "dataInfo"    # J

    .prologue
    .line 56
    return-void
.end method
