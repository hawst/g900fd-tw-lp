.class public Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;
.super Landroid/widget/LinearLayout;
.source "FoodTrackerMainContainer.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$12;,
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$EditMealDialogButtonController;,
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;,
        Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;
    }
.end annotation


# static fields
.field private static final CAL_UNIT_TEXT_SIZE:I = 0x11

.field private static final DEFAULT_PLAN_IMAGE_RESOURCE:I = 0x7f0201b9

.field private static final MEAL_TYPE_TO_USER_INPUT_ACTION_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/constants/MealType;",
            "Lcom/sec/android/app/shealth/food/app/UserActionLog;",
            ">;"
        }
    .end annotation
.end field

.field private static final MEAL_TYPE_TO_USER_VIEW_ACTION_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/constants/MealType;",
            "Lcom/sec/android/app/shealth/food/app/UserActionLog;",
            ">;"
        }
    .end annotation
.end field

.field private static final SKIPPED_TEXT_SIZE:I = 0xf

.field public static final WRONG_MEAL_TYPE_MSG:Ljava/lang/String; = "WRONG_MEAL_TYPE_MSG"

.field private static final sBaseInstanceState:Ljava/lang/String; = "sBaseInstanceState"

.field private static final sCurrentMealData:Ljava/lang/String; = "sCurrentMealData"

.field private static final sDeleteOptionIndex:I = 0x1

.field private static final sEditDeleteDialogTag:Ljava/lang/String; = "sEditDeleteDialogTag"

.field private static final sOkCancelDeleteDialogTag:Ljava/lang/String; = "sOkCancelDeleteDialogTag"


# instance fields
.field private final CAL_UNIT_TEXT_COLOR:I

.field private final SKIPPED_TEXT_COLOR:I

.field private mCallbacksProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/android/app/shealth/food/mask/CallbacksProvider",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

.field private mMealDataHolders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;

    invoke-static {}, Lcom/sec/android/app/shealth/food/constants/MealType;->values()[Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$1;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->MEAL_TYPE_TO_USER_INPUT_ACTION_MAP:Ljava/util/Map;

    .line 104
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$2;

    invoke-static {}, Lcom/sec/android/app/shealth/food/constants/MealType;->values()[Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$2;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->MEAL_TYPE_TO_USER_VIEW_ACTION_MAP:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->SKIPPED_TEXT_COLOR:I

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->CAL_UNIT_TEXT_COLOR:I

    .line 151
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$3;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mDialogControllerMap:Ljava/util/Map;

    .line 163
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->init()V

    .line 165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 173
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 179
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->SKIPPED_TEXT_COLOR:I

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->CAL_UNIT_TEXT_COLOR:I

    .line 151
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$3;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mDialogControllerMap:Ljava/util/Map;

    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->init()V

    .line 183
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/food/summary/FoodTrackerProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getFoodTrackerProvider()Lcom/sec/android/app/shealth/food/summary/FoodTrackerProvider;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->showEditDeleteMealDialog(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->showOkCancelDeleteMealDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    return-object v0
.end method

.method private getFoodPickListener(Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;Lcom/sec/android/app/shealth/food/constants/MealType;)Landroid/view/View$OnClickListener;
    .locals 4
    .param p1, "holder"    # Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
    .param p2, "mealType"    # Lcom/sec/android/app/shealth/food/constants/MealType;

    .prologue
    .line 755
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/food/foodpick/FoodPickActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "MEAL_DATA_HOLDER"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 759
    .local v0, "foodPickIntent":Landroid/content/Intent;
    new-instance v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;

    sget-object v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_TO_INPUT_MEAL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->MEAL_TYPE_TO_USER_INPUT_ACTION_MAP:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-direct {v2, p0, v0, v3, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Landroid/content/Intent;Lcom/sec/android/app/shealth/food/app/UserActionLog;Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    return-object v2
.end method

.method private getFoodTrackerProvider()Lcom/sec/android/app/shealth/food/summary/FoodTrackerProvider;
    .locals 3

    .prologue
    .line 793
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerProvider;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 795
    :catch_0
    move-exception v0

    .line 797
    .local v0, "ex":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Activity should be instance of FoodTrackerProvider"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getMaskId(I)I
    .locals 1
    .param p1, "imagesCount"    # I

    .prologue
    .line 271
    invoke-static {p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$Mask;->getIdBySquaresCount(I)I

    move-result v0

    return v0
.end method

.method private getViewMealListener(Landroid/content/Intent;Lcom/sec/android/app/shealth/food/constants/MealType;)Landroid/view/View$OnClickListener;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "mealType"    # Lcom/sec/android/app/shealth/food/constants/MealType;

    .prologue
    .line 775
    new-instance v1, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_MEAL_DETAIL:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    sget-object v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->MEAL_TYPE_TO_USER_VIEW_ACTION_MAP:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-direct {v1, p0, p1, v2, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$LogAndStartActivityClickListener;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Landroid/content/Intent;Lcom/sec/android/app/shealth/food/app/UserActionLog;Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    return-object v1
.end method

.method private init()V
    .locals 1

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->initMealsDataHolders()V

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->initView()V

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodMaskedImageCollectorHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodMaskedImageCollectorHolder;->getMaskedImageCollector()Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    .line 197
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$4;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCallbacksProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;

    .line 211
    return-void
.end method

.method private initImageByPaths(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;Landroid/widget/ImageView;)V
    .locals 6
    .param p1, "dayMealDataHolder"    # Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    .param p2, "mealImageView"    # Landroid/widget/ImageView;

    .prologue
    const v5, 0x7f09020a

    .line 504
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getBitmapsPaths()Ljava/util/List;

    move-result-object v1

    .line 506
    .local v1, "imagePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 508
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$7;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$7;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Landroid/widget/ImageView;)V

    .line 521
    .local v0, "drawingResultListener":Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCallbacksProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->addCallBack(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getMaskId(I)I

    move-result v3

    invoke-virtual {v2, v3, v1, v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->registerTask(ILjava/util/List;Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;)V

    .line 524
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_MULTI_PIC:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getBitmapsPaths()Ljava/util/List;

    move-result-object v3

    invoke-static {p2, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/util/List;)V

    .line 525
    new-instance v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$8;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$8;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Ljava/util/List;)V

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 537
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v4

    iget v4, v4, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090233

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 565
    .end local v0    # "drawingResultListener":Lcom/sec/android/app/shealth/food/mask/MaskTools$DrawingResultListener;
    :goto_0
    return-void

    .line 542
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 544
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const-string v3, ""

    const-string v4, ""

    invoke-static {p2, v2, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    new-instance v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$9;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$9;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)V

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 560
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v4

    iget v4, v4, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090232

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private initMealsDataHolders()V
    .locals 6

    .prologue
    .line 253
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMealDataHolders:Ljava/util/List;

    .line 255
    invoke-static {}, Lcom/sec/android/app/shealth/food/constants/MealType;->values()[Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/shealth/food/constants/MealType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 257
    .local v3, "mealType":Lcom/sec/android/app/shealth/food/constants/MealType;
    sget-object v4, Lcom/sec/android/app/shealth/food/constants/MealType;->NOT_DEFINED:Lcom/sec/android/app/shealth/food/constants/MealType;

    if-eq v3, v4, :cond_0

    .line 259
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMealDataHolders:Ljava/util/List;

    new-instance v5, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    invoke-direct {v5, v3}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;-><init>(Lcom/sec/android/app/shealth/food/constants/MealType;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 265
    .end local v3    # "mealType":Lcom/sec/android/app/shealth/food/constants/MealType;
    :cond_1
    return-void
.end method

.method private initView()V
    .locals 13

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    .line 219
    .local v11, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03011c

    invoke-virtual {v11, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMealDataHolders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    .line 223
    .local v9, "dayMealDataHolder":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    invoke-virtual {v9}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getViewId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 225
    .local v12, "itemRootView":Landroid/view/View;
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;

    const v1, 0x7f0804b0

    invoke-virtual {v12, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0804b1

    invoke-virtual {v12, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0804b2

    invoke-virtual {v12, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0804b4

    invoke-virtual {v12, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0804b8

    invoke-virtual {v12, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0804b3

    invoke-virtual {v12, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0804b6

    invoke-virtual {v12, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0804b7

    invoke-virtual {v12, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 243
    .local v0, "mealViewsHolder":Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;
    invoke-virtual {v9, v0}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->setMealViewsHolder(Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;)V

    goto :goto_0

    .line 247
    .end local v0    # "mealViewsHolder":Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;
    .end local v9    # "dayMealDataHolder":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    .end local v12    # "itemRootView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private setContentDescriptions(Landroid/view/View;Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 4
    .param p1, "addButtonView"    # Landroid/view/View;
    .param p2, "mealImageView"    # Landroid/widget/ImageView;
    .param p3, "mealTypeString"    # Ljava/lang/String;

    .prologue
    .line 689
    const-string v0, " "

    .line 700
    .local v0, "space":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 705
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 711
    return-void
.end method

.method private shouldMealTextBeShownAsSingleLine(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)Z
    .locals 3
    .param p1, "dayMealDataHolder"    # Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    .prologue
    .line 571
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getDataState()Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->MEAL_DATA:Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->doesMealHaveMealItems(Landroid/content/Context;J)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showEditDeleteMealDialog(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)V
    .locals 6
    .param p1, "dayMealDataHolder"    # Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    .prologue
    .line 581
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 583
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getNameResourceId()I

    move-result v4

    const/16 v5, 0x9

    invoke-direct {v1, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 589
    .local v1, "dialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$10;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V

    .line 601
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 603
    const/4 v4, 0x2

    new-array v0, v4, [Z

    fill-array-data v0, :array_0

    .line 605
    .local v0, "checked":[Z
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setChecked([Z)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 607
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v3

    .line 609
    .local v3, "listChooseDialog":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string/jumbo v5, "sEditDeleteDialogTag"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 611
    return-void

    .line 603
    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
    .end array-data
.end method

.method private showOkCancelDeleteMealDialog()V
    .locals 4

    .prologue
    const v3, 0x7f090035

    .line 661
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09011c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "sOkCancelDeleteDialogTag"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 671
    return-void
.end method


# virtual methods
.method public clearImagesCache()V
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMaskedImageCollector:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector;->clearCache()V

    .line 946
    return-void
.end method

.method public getDataByTypeId(I)Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    .locals 4
    .param p1, "mealTypeId"    # I

    .prologue
    .line 817
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMealDataHolders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    .line 819
    .local v0, "dayMealDataHolder":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v2

    iget v2, v2, Lcom/sec/android/app/shealth/food/constants/MealType;->mealTypeId:I

    if-ne v2, p1, :cond_0

    .line 821
    return-object v0

    .line 827
    .end local v0    # "dayMealDataHolder":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "WRONG_MEAL_TYPE_MSG"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 681
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 619
    const-string/jumbo v0, "sEditDeleteDialogTag"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    new-instance v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$11;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;)V

    .line 653
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 929
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 931
    check-cast v0, Landroid/os/Bundle;

    .line 933
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "sCurrentMealData"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 935
    const-string/jumbo v1, "sBaseInstanceState"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    .line 939
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 941
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 907
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-nez v1, :cond_0

    .line 909
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    .line 913
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 915
    .local v0, "bundle":Landroid/os/Bundle;
    const-string/jumbo v1, "sBaseInstanceState"

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 917
    const-string/jumbo v1, "sCurrentMealData"

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCurrentMealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 919
    return-object v0
.end method

.method public resetMealsItemsContents(J)V
    .locals 3
    .param p1, "dayTime"    # J

    .prologue
    .line 313
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMealDataHolders:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    .line 315
    .local v0, "dayMealDataHolder":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->resetData(J)V

    goto :goto_0

    .line 319
    .end local v0    # "dayMealDataHolder":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    :cond_0
    return-void
.end method

.method public updateMealsItemsContents()V
    .locals 30

    .prologue
    .line 331
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mCallbacksProvider:Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mask/CallbacksProvider;->removeCallbacks()V

    .line 333
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->clearImagesCache()V

    .line 335
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->mMealDataHolders:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;

    .line 337
    .local v12, "dayMealDataHolder":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealViewsHolder()Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;

    move-result-object v26

    .line 340
    .local v26, "mealViewsHolder":Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v17

    .line 341
    .local v17, "language":Ljava/lang/String;
    const-string v4, "ar"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 343
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getMealCaloriesView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v18

    check-cast v18, Landroid/widget/LinearLayout;

    .line 344
    .local v18, "lowerViewParent":Landroid/widget/LinearLayout;
    if-eqz v18, :cond_0

    .line 346
    const/4 v4, 0x5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 351
    .end local v18    # "lowerViewParent":Landroid/widget/LinearLayout;
    :cond_0
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getDataState()Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;

    move-result-object v11

    .line 355
    .local v11, "dataState":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getMealImageView()Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/ImageView;

    .line 357
    .local v23, "mealImageView":Landroid/widget/ImageView;
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getMealViewButton()Landroid/view/View;

    move-result-object v25

    .line 359
    .local v25, "mealViewButton":Landroid/view/View;
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getSingleLineTextView()Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    .line 361
    .local v27, "singleLineTextView":Landroid/widget/TextView;
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getDoubleLineTextView()Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 363
    .local v13, "doubleLineTextView":Landroid/widget/TextView;
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getDoubleLineView()Landroid/view/View;

    move-result-object v14

    .line 365
    .local v14, "doubleLineView":Landroid/view/View;
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getMealAddButton()Landroid/view/View;

    move-result-object v19

    .line 367
    .local v19, "mealAddButton":Landroid/view/View;
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getMealCaloriesView()Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 368
    .local v21, "mealCaloriesView":Landroid/widget/TextView;
    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;->getMealCaloriesUnitView()Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 372
    .local v20, "mealCaloriesUnitView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->shouldMealTextBeShownAsSingleLine(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    :goto_1
    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 374
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->shouldMealTextBeShownAsSingleLine(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v4, 0x8

    :goto_2
    invoke-virtual {v14, v4}, Landroid/view/View;->setVisibility(I)V

    .line 376
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->setPressed(Z)V

    .line 382
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealDataHolder()Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    move-result-object v4

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getFoodPickListener(Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;Lcom/sec/android/app/shealth/food/constants/MealType;)Landroid/view/View$OnClickListener;

    move-result-object v15

    .line 384
    .local v15, "foodPickListener":Landroid/view/View$OnClickListener;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getNameResourceId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 388
    .local v24, "mealTypeString":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->setContentDescriptions(Landroid/view/View;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 392
    sget-object v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$12;->$SwitchMap$com$sec$android$app$shealth$food$summary$DayMealDataHolder$DataState:[I

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 490
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Wrong data state"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 372
    .end local v15    # "foodPickListener":Landroid/view/View$OnClickListener;
    .end local v24    # "mealTypeString":Ljava/lang/String;
    :cond_1
    const/16 v4, 0x8

    goto :goto_1

    .line 374
    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 396
    .restart local v15    # "foodPickListener":Landroid/view/View$OnClickListener;
    .restart local v24    # "mealTypeString":Ljava/lang/String;
    :pswitch_0
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const-string v5, ""

    const-string v6, ""

    move-object/from16 v0, v25

    invoke-static {v0, v4, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0201b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 402
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_NONE:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    const-string v5, ""

    const-string v6, ""

    move-object/from16 v0, v23

    invoke-static {v0, v4, v5, v6}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v6

    iget v6, v6, Lcom/sec/android/app/shealth/food/constants/MealType;->textResourceId:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090232

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 407
    new-instance v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$5;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v12}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$5;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 421
    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 494
    :goto_3
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 429
    :pswitch_1
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v22

    .line 431
    .local v22, "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->doesMealHaveMealItems(Landroid/content/Context;J)Z

    move-result v4

    if-nez v4, :cond_3

    .line 433
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getNameResourceId()I

    move-result v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 459
    :goto_4
    new-instance v28, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/food/mealinput/ViewMealActivity;

    move-object/from16 v0, v28

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 461
    .local v28, "viewIntent":Landroid/content/Intent;
    const-string v29, "MEAL_DATA_HOLDER"

    new-instance v4, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getId()J

    move-result-wide v5

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v7

    int-to-long v7, v7

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v9

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;-><init>(JJJ)V

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 463
    const-string v4, "STARTED_FROM_FOOD_TRACKER"

    const/4 v5, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 465
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getMealType()Lcom/sec/android/app/shealth/food/constants/MealType;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getViewMealListener(Landroid/content/Intent;Lcom/sec/android/app/shealth/food/constants/MealType;)Landroid/view/View$OnClickListener;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 467
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0900b9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-static {v0, v4, v1, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    new-instance v4, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$6;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v12}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer$6;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 483
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v12, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->initImageByPaths(Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;Landroid/widget/ImageView;)V

    goto/16 :goto_3

    .line 437
    .end local v28    # "viewIntent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->getNameResourceId()I

    move-result v4

    invoke-virtual {v13, v4}, Landroid/widget/TextView;->setText(I)V

    .line 441
    invoke-virtual {v12}, Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;->isSkipped()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 443
    const/high16 v4, 0x41700000    # 15.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 444
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->SKIPPED_TEXT_COLOR:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 445
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0909c8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446
    const/16 v4, 0x8

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 450
    :cond_4
    const/high16 v4, 0x41880000    # 17.0f

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 451
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->CAL_UNIT_TEXT_COLOR:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 452
    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 453
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerMainContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 498
    .end local v11    # "dataState":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder$DataState;
    .end local v12    # "dayMealDataHolder":Lcom/sec/android/app/shealth/food/summary/DayMealDataHolder;
    .end local v13    # "doubleLineTextView":Landroid/widget/TextView;
    .end local v14    # "doubleLineView":Landroid/view/View;
    .end local v15    # "foodPickListener":Landroid/view/View$OnClickListener;
    .end local v17    # "language":Ljava/lang/String;
    .end local v19    # "mealAddButton":Landroid/view/View;
    .end local v20    # "mealCaloriesUnitView":Landroid/widget/TextView;
    .end local v21    # "mealCaloriesView":Landroid/widget/TextView;
    .end local v22    # "mealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .end local v23    # "mealImageView":Landroid/widget/ImageView;
    .end local v24    # "mealTypeString":Ljava/lang/String;
    .end local v25    # "mealViewButton":Landroid/view/View;
    .end local v26    # "mealViewsHolder":Lcom/sec/android/app/shealth/food/summary/MealViewsHolder;
    .end local v27    # "singleLineTextView":Landroid/widget/TextView;
    :cond_5
    return-void

    .line 392
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
