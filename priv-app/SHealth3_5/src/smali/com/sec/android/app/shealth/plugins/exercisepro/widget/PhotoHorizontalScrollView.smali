.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;
.super Landroid/widget/RelativeLayout;
.source "PhotoHorizontalScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$Holder;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;
    }
.end annotation


# static fields
.field private static final SIZE_HEIGHT:I = 0x43

.field private static final SIZE_WIDTH:I = 0x43


# instance fields
.field gestureDetector:Landroid/view/GestureDetector;

.field isEnable:Z

.field private mContext:Landroid/content/Context;

.field private mDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field mImageClickListener:Landroid/view/View$OnClickListener;

.field private mItemLayout:Landroid/widget/LinearLayout;

.field private mObject:Ljava/lang/Object;

.field private mOldPosition:I

.field private mParams:Landroid/widget/LinearLayout$LayoutParams;

.field private mScreenSize:Landroid/graphics/Point;

.field private mScrollView:Landroid/widget/HorizontalScrollView;

.field private mSelectedPosition:I

.field private mTumbnailClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

.field mUpdateHandler:Landroid/os/Handler;

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->gestureDetector:Landroid/view/GestureDetector;

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mObject:Ljava/lang/Object;

    .line 39
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    .line 40
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mOldPosition:I

    .line 41
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScreenSize:Landroid/graphics/Point;

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->isEnable:Z

    .line 135
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mUpdateHandler:Landroid/os/Handler;

    .line 183
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mImageClickListener:Landroid/view/View$OnClickListener;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->initLayout()V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->gestureDetector:Landroid/view/GestureDetector;

    .line 38
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mObject:Ljava/lang/Object;

    .line 39
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    .line 40
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mOldPosition:I

    .line 41
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScreenSize:Landroid/graphics/Point;

    .line 42
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->isEnable:Z

    .line 135
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mUpdateHandler:Landroid/os/Handler;

    .line 183
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mImageClickListener:Landroid/view/View$OnClickListener;

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 58
    .local v0, "display":Landroid/view/Display;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScreenSize:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->initLayout()V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mObject:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mTumbnailClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;)Landroid/widget/HorizontalScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScrollView:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method private initLayout()V
    .locals 3

    .prologue
    .line 71
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 73
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0301d1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mView:Landroid/view/View;

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mView:Landroid/view/View;

    const v2, 0x7f080808

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/HorizontalScrollView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScrollView:Landroid/widget/HorizontalScrollView;

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mView:Landroid/view/View;

    const v2, 0x7f080809

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->addView(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method private scrollToSelection()V
    .locals 6

    .prologue
    .line 217
    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    if-ltz v4, :cond_0

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 218
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 219
    .local v1, "v":Landroid/view/View;
    if-nez v1, :cond_1

    .line 249
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 222
    .restart local v1    # "v":Landroid/view/View;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v4}, Landroid/widget/HorizontalScrollView;->computeScroll()V

    .line 224
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 225
    .local v2, "viewLeft":I
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    .line 228
    .local v3, "viewRight":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v4}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v4

    if-ge v2, v4, :cond_4

    .line 229
    move v0, v2

    .line 230
    .local v0, "scrollX":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScrollView:Landroid/widget/HorizontalScrollView;

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$4;

    invoke-direct {v5, p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;I)V

    invoke-virtual {v4, v5}, Landroid/widget/HorizontalScrollView;->post(Ljava/lang/Runnable;)Z

    .line 245
    .end local v0    # "scrollX":I
    :cond_2
    :goto_1
    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mOldPosition:I

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    if-eq v4, v5, :cond_3

    .line 246
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mOldPosition:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    .line 247
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    .line 236
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v4}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScreenSize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v4, v5

    if-ge v4, v3, :cond_2

    .line 237
    move v0, v3

    .line 238
    .restart local v0    # "scrollX":I
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mScrollView:Landroid/widget/HorizontalScrollView;

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$5;

    invoke-direct {v5, p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;I)V

    invoke-virtual {v4, v5}, Landroid/widget/HorizontalScrollView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method private setPhotoListtoBitmap(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->invalidate()V

    .line 83
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 108
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 109
    return-void
.end method


# virtual methods
.method public getPhotoItemList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mDatas:Ljava/util/List;

    return-object v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 171
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->isEnable:Z

    .line 172
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->setVisibility(I)V

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 174
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    return-void
.end method

.method public isShown()Z
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPress()V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->hide()V

    .line 117
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 203
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->scrollToSelection()V

    .line 205
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "oldWidth"    # I
    .param p4, "oldHeight"    # I

    .prologue
    .line 254
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->scrollToSelection()V

    .line 256
    return-void
.end method

.method public setOnTumbnailClickListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mTumbnailClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    .line 68
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 208
    if-gez p1, :cond_0

    .line 209
    const/4 p1, 0x0

    .line 211
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mOldPosition:I

    .line 212
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mSelectedPosition:I

    .line 213
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->scrollToSelection()V

    .line 214
    return-void
.end method

.method public show(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    const/4 v2, 0x0

    const/high16 v3, 0x42860000    # 67.0f

    .line 120
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mDatas:Ljava/util/List;

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 122
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 127
    .end local v0    # "i":I
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->setVisibility(I)V

    .line 128
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->isEnable:Z

    .line 129
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mParams:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->convertDpToPx(Landroid/content/Context;F)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 132
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->setPhotoListtoBitmap(Ljava/util/List;)V

    .line 133
    return-void
.end method
