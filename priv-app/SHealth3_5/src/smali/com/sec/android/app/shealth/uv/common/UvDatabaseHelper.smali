.class public Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;
.super Ljava/lang/Object;
.source "UvDatabaseHelper.java"


# static fields
.field private static instance:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method private getChildQuery(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 159
    const-string/jumbo v0, "sample_time>? AND sample_time<?"

    .line 160
    .local v0, "clause":Ljava/lang/String;
    const-string/jumbo v6, "sample_time"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 161
    .local v4, "time":J
    new-array v2, v10, [Ljava/lang/String;

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getMonthToStringByMillisecond(ZJ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v8

    invoke-static {v8, v4, v5}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getMonthToStringByMillisecond(ZJ)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v9

    .line 162
    .local v2, "selectionArgs":[Ljava/lang/String;
    sget v6, Lcom/sec/android/app/shealth/uv/log/UvLogActivity;->SELECTED_FILTER_INDEX:I

    if-nez v6, :cond_0

    .line 163
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "0==0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 166
    :cond_0
    const-string v6, "AND"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v3, v6, v10

    .line 168
    .local v3, "selectionClause":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "select A.*, B.AvgMonth, E.recommended_duration as RECOMMENDEDDURATION, E.sun_protection as SUNPROTECTION FROM ultraviolet_rays A  LEFT OUTER JOIN (SELECT C.[_id], C.[sample_time], AVG(C.[uv_index]) as AvgMonth from ultraviolet_rays C group by strftime(\"%d-%m-%Y\", (C.[sample_time]/1000),\'unixepoch\')) as B on A.[_id] = B.[_id] LEFT OUTER JOIN uv_protection E on A.[sample_time] = E.sample_time where A.sample_time >= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v8

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and A."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "sample_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " <= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v9

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " order by A.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "sample_time"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]/1000 DESC, B.["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "AvgMonth"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] DESC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 198
    .local v1, "query":Ljava/lang/String;
    return-object v1
.end method

.method private getData(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/uv/data/UvData;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, -0x1

    .line 80
    new-instance v0, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/uv/data/UvData;-><init>()V

    .line 81
    .local v0, "data":Lcom/sec/android/app/shealth/uv/data/UvData;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 82
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_3

    .line 83
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setId(J)V

    .line 84
    const-string v1, "application__id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->setApplicationId(Ljava/lang/String;)V

    .line 85
    const-string/jumbo v1, "user_device__id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->setUserDeviceId(Ljava/lang/String;)V

    .line 86
    const-string/jumbo v1, "uv_index"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setScore(D)V

    .line 87
    const-string/jumbo v1, "sample_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setSampleTime(J)V

    .line 88
    const-string v1, "create_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setCreateTime(J)V

    .line 89
    const-string/jumbo v1, "update_time"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setUpdateTime(J)V

    .line 90
    const-string v1, "comment"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->setComment(Ljava/lang/String;)V

    .line 91
    const-string v1, "MinScore"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v3, :cond_0

    .line 92
    const-string v1, "MinScore"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setMin(D)V

    .line 94
    :cond_0
    const-string v1, "MaxScore"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v3, :cond_1

    .line 95
    const-string v1, "MaxScore"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setMax(D)V

    .line 97
    :cond_1
    const-string v1, "AvgScore"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v3, :cond_2

    .line 98
    const-string v1, "AvgScore"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setAverage(D)V

    .line 101
    :cond_2
    const-string v1, "RECOMMENDEDDURATION"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/data/UvData;->setRecomendedTime(J)V

    .line 102
    const-string v1, "SUNPROTECTION"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->setSunProtection(I)V

    .line 104
    :cond_3
    return-object v0
.end method

.method private getDatas(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/uv/data/UvData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, -0x1

    .line 108
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v1, "datas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 110
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_5

    .line 111
    new-instance v0, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/uv/data/UvData;-><init>()V

    .line 112
    .local v0, "data":Lcom/sec/android/app/shealth/uv/data/UvData;
    const-string v5, "_id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setId(J)V

    .line 113
    const-string v5, "application__id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/uv/data/UvData;->setApplicationId(Ljava/lang/String;)V

    .line 114
    const-string/jumbo v5, "user_device__id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/uv/data/UvData;->setUserDeviceId(Ljava/lang/String;)V

    .line 115
    const-string/jumbo v5, "uv_index"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    int-to-double v5, v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setScore(D)V

    .line 116
    const-string/jumbo v5, "sample_time"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setSampleTime(J)V

    .line 117
    const-string v5, "create_time"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setCreateTime(J)V

    .line 118
    const-string/jumbo v5, "update_time"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setUpdateTime(J)V

    .line 119
    const-string v5, "comment"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/uv/data/UvData;->setComment(Ljava/lang/String;)V

    .line 120
    const-string v5, "MinScore"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v8, :cond_0

    .line 121
    const-string v5, "MinScore"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setMin(D)V

    .line 123
    :cond_0
    const-string v5, "MaxScore"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v8, :cond_1

    .line 124
    const-string v5, "MaxScore"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setMax(D)V

    .line 126
    :cond_1
    const-string v5, "AvgScore"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    if-eq v5, v8, :cond_2

    .line 127
    const-string v5, "AvgScore"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setAverage(D)V

    .line 131
    :cond_2
    :try_start_0
    const-string v5, "RECOMMENDEDDURATION"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 132
    .local v3, "mRecommendedTime":Ljava/lang/Long;
    const-string v5, "SUNPROTECTION"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 133
    .local v4, "mSunProtection":Ljava/lang/Integer;
    if-eqz v3, :cond_3

    .end local v3    # "mRecommendedTime":Ljava/lang/Long;
    :goto_1
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v0, v5, v6}, Lcom/sec/android/app/shealth/uv/data/UvData;->setRecomendedTime(J)V

    .line 134
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    int-to-long v5, v5

    :goto_2
    long-to-int v5, v5

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/uv/data/UvData;->setSunProtection(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    .end local v4    # "mSunProtection":Ljava/lang/Integer;
    :goto_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 141
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 133
    .restart local v3    # "mRecommendedTime":Ljava/lang/Long;
    .restart local v4    # "mSunProtection":Ljava/lang/Integer;
    :cond_3
    :try_start_1
    sget-object v3, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    goto :goto_1

    .line 134
    .end local v3    # "mRecommendedTime":Ljava/lang/Long;
    :cond_4
    sget-object v5, Lcom/sec/android/app/shealth/uv/common/UvConstants;->INVALID:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v5

    goto :goto_2

    .line 135
    .end local v4    # "mSunProtection":Ljava/lang/Integer;
    :catch_0
    move-exception v2

    .line 137
    .local v2, "e":Ljava/lang/IllegalStateException;
    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No Sun Protection and Recomended Messages are available "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 144
    .end local v0    # "data":Lcom/sec/android/app/shealth/uv/data/UvData;
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :cond_5
    return-object v1
.end method

.method private getDeviceName(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 369
    if-eqz p1, :cond_0

    .line 370
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 371
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    const-string v0, "custom_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 375
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const-class v1, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->instance:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->instance:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    .line 37
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->instance:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getShareData(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const v7, 0x7f090d14

    .line 464
    const-string v2, ""

    .line 465
    .local v2, "shareData":Ljava/lang/String;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 466
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_0

    .line 467
    const-string/jumbo v4, "sample_time"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 469
    .local v0, "sampleTime":J
    const-string/jumbo v4, "uv_index"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->generateState(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 486
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/shealth/uv/data/UvStateType;->INVALID:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getStringResource()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 489
    .local v3, "state":Ljava/lang/String;
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    const v6, 0x7f090db9

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    const v6, 0x7f090135

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v5, v0, v1}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 491
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    .line 471
    .end local v3    # "state":Ljava/lang/String;
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/shealth/uv/data/UvStateType;->LOW:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getStringResource()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 472
    .restart local v3    # "state":Ljava/lang/String;
    goto :goto_1

    .line 474
    .end local v3    # "state":Ljava/lang/String;
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/shealth/uv/data/UvStateType;->MODERATE:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getStringResource()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 475
    .restart local v3    # "state":Ljava/lang/String;
    goto :goto_1

    .line 477
    .end local v3    # "state":Ljava/lang/String;
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/shealth/uv/data/UvStateType;->HIGH:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getStringResource()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 478
    .restart local v3    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 480
    .end local v3    # "state":Ljava/lang/String;
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/shealth/uv/data/UvStateType;->VERY_HIGH:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getStringResource()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 481
    .restart local v3    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 483
    .end local v3    # "state":Ljava/lang/String;
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/sec/android/app/shealth/uv/data/UvStateType;->EXTREME:Lcom/sec/android/app/shealth/uv/data/UvStateType;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getStringResource()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 484
    .restart local v3    # "state":Ljava/lang/String;
    goto/16 :goto_1

    .line 493
    .end local v0    # "sampleTime":J
    .end local v3    # "state":Ljava/lang/String;
    :cond_0
    return-object v2

    .line 469
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public clearInstance()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 523
    iput-object v1, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    .line 525
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->instance:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    if-eqz v0, :cond_1

    .line 526
    sput-object v1, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->instance:Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    .line 528
    :cond_1
    return-void
.end method

.method public deleteAllData()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 228
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 229
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string/jumbo v3, "uv_warning_checked"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 230
    const-string/jumbo v3, "skin_type_checked"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 231
    const-string/jumbo v3, "spf_value"

    const/16 v4, 0x1e

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 232
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 235
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$Skin;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 236
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public deleteDailyDataByTime(J)Z
    .locals 9
    .param p1, "timeMillis"    # J

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 270
    const-string/jumbo v2, "sample_time>? AND sample_time<?"

    .line 271
    .local v2, "selection":Ljava/lang/String;
    new-array v3, v8, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    .line 273
    .local v3, "selectionArgs":[Ljava/lang/String;
    const-string/jumbo v0, "sample_time>? AND sample_time<?"

    .line 274
    .local v0, "protectionSelection":Ljava/lang/String;
    new-array v1, v8, [Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v5

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v4

    .line 275
    .local v1, "protectionSelectionArgs":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v0, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 277
    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6, v7, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_0

    :goto_0
    return v4

    :cond_0
    move v4, v5

    goto :goto_0
.end method

.method public deleteRowById(Ljava/lang/String;)Z
    .locals 11
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT sample_time FROM ultraviolet_rays  where _id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 246
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 248
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 249
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 250
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 251
    const-string/jumbo v7, "sample_time=?"

    .line 252
    .local v7, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v8, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "sample_time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v8, v0

    .line 253
    .local v8, "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtection;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    .end local v7    # "selection":Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 257
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 264
    :cond_1
    const-string v7, "_id=?"

    .line 265
    .restart local v7    # "selection":Ljava/lang/String;
    new-array v8, v9, [Ljava/lang/String;

    aput-object p1, v8, v10

    .line 266
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_3

    move v0, v9

    :goto_0
    return v0

    .line 256
    .end local v7    # "selection":Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 257
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .restart local v7    # "selection":Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    :cond_3
    move v0, v10

    .line 266
    goto :goto_0
.end method

.method public generateState(I)I
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 504
    const/4 v0, -0x1

    .line 506
    .local v0, "STATE":I
    const/4 v1, 0x2

    if-gt p1, v1, :cond_0

    .line 507
    const/4 v0, 0x0

    .line 518
    :goto_0
    return v0

    .line 508
    :cond_0
    const/4 v1, 0x5

    if-gt p1, v1, :cond_1

    .line 509
    const/4 v0, 0x1

    goto :goto_0

    .line 510
    :cond_1
    const/4 v1, 0x7

    if-gt p1, v1, :cond_2

    .line 511
    const/4 v0, 0x2

    goto :goto_0

    .line 512
    :cond_2
    const/16 v1, 0xa

    if-gt p1, v1, :cond_3

    .line 513
    const/4 v0, 0x3

    goto :goto_0

    .line 515
    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public getCalendarDatas(JJ)Ljava/util/ArrayList;
    .locals 10
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/uv/data/UvData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 202
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const-string/jumbo v0, "sample_time"

    aput-object v0, v2, v5

    .line 203
    .local v2, "projection":[Ljava/lang/String;
    const-string/jumbo v3, "sample_time>? AND sample_time<?"

    .line 204
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 205
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 208
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 209
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 210
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v8, "uvDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 212
    new-instance v7, Lcom/sec/android/app/shealth/uv/data/UvData;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/uv/data/UvData;-><init>()V

    .line 213
    .local v7, "uvData":Lcom/sec/android/app/shealth/uv/data/UvData;
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->setId(J)V

    .line 214
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/shealth/uv/data/UvData;->setSampleTime(J)V

    .line 216
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 217
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 221
    .end local v7    # "uvData":Lcom/sec/android/app/shealth/uv/data/UvData;
    .end local v8    # "uvDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_0

    .line 222
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v0

    .line 221
    .restart local v8    # "uvDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/uv/data/UvData;>;"
    :cond_1
    if-eqz v6, :cond_2

    .line 222
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    return-object v8
.end method

.method public getChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6
    .param p1, "groupCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getChildQuery(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getDataById(Ljava/lang/String;)Lcom/sec/android/app/shealth/uv/data/UvData;
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 331
    const/4 v6, 0x0

    .line 332
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select U.*,A.recommended_duration as RECOMMENDEDDURATION, A.sun_protection as SUNPROTECTION FROM ultraviolet_rays U LEFT OUTER JOIN uv_protection A ON U.[sample_time] = A.sample_time WHERE U.[_id] ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 341
    .local v3, "query":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 342
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 343
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getData(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/uv/data/UvData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 345
    if-eqz v6, :cond_0

    .line 346
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 345
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 346
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public getDataCount()I
    .locals 7

    .prologue
    .line 379
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 380
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 382
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 383
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 385
    if-eqz v6, :cond_0

    .line 386
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    .line 385
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 386
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public getDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select U.[custom_name] FROM ultraviolet_rays A, user_device U WHERE A.[_id] ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "A.[user_device__id] = U.[_id]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 357
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 359
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 360
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDeviceName(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 362
    if-eqz v6, :cond_0

    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 362
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public getGraphDatasByType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/uv/data/UvData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_1

    const-string v6, "STRFTIME(\"%d-%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

    .line 393
    .local v6, "condition1":Ljava/lang/String;
    :goto_0
    const-string v7, "0==0"

    .line 395
    .local v7, "condition2":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT A.*, B.MinScore, B.MaxScore, B.AvgScore FROM (SELECT C.[_id], MAX(C.[sample_time]) AS MaxSampleTime, MIN(C.[uv_index]) AS MinScore, MAX(C.[uv_index]) AS MaxScore, AVG(C.[uv_index]) AS AvgScore FROM ultraviolet_rays AS C GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AS B LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "ultraviolet_rays"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " A ON B.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MaxSampleTime"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] = A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " GROUP BY A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] ORDER BY A.["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 418
    .local v3, "query":Ljava/lang/String;
    const/4 v8, 0x0

    .line 420
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 421
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDatas(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 423
    if-eqz v8, :cond_0

    .line 424
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 392
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "condition1":Ljava/lang/String;
    .end local v7    # "condition2":Ljava/lang/String;
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    const-string v6, "STRFTIME(\"%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

    goto/16 :goto_0

    :cond_2
    const-string v6, "(C.[sample_time]/(1000 * 60 *5))"

    goto/16 :goto_0

    .line 423
    .restart local v3    # "query":Ljava/lang/String;
    .restart local v6    # "condition1":Ljava/lang/String;
    .restart local v7    # "condition2":Ljava/lang/String;
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 424
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getGroupCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "constraint"    # Ljava/lang/String;

    .prologue
    .line 148
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, " AVG (uv_index) AS AvgUv"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "sample_time"

    aput-object v1, v2, v0

    .line 149
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") GROUP BY (strftime(\"%m-%Y\",(["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "ultraviolet_rays"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "].["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]/1000),\'unixepoch\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 150
    .local v3, "selection":Ljava/lang/String;
    const-string/jumbo v5, "sample_time DESC"

    .line 151
    .local v5, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getLastTwoDataByTime(J)Ljava/util/ArrayList;
    .locals 7
    .param p1, "timeMills"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/uv/data/UvData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 290
    const/4 v6, 0x0

    .line 291
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "select U.*,A.recommended_duration as RECOMMENDEDDURATION, A.sun_protection as SUNPROTECTION FROM ultraviolet_rays U LEFT OUTER JOIN uv_protection A ON A.[sample_time] = U.sample_time WHERE U.[sample_time]  < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/uv/utils/DateFormatUtil;->getEndDayToMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " order by U.["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "sample_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DESC LIMIT 2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 302
    .local v3, "query":Ljava/lang/String;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 304
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 305
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getDatas(Landroid/database/Cursor;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 309
    if-eqz v6, :cond_0

    .line 310
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 309
    :cond_1
    if-eqz v6, :cond_0

    .line 310
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 309
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 310
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public getShareDataByIds(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 432
    .local p1, "checkedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_4

    .line 433
    const-string v8, ""

    .line 434
    .local v8, "selection":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 435
    .local v9, "size":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v9, :cond_1

    .line 436
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v0, v9, -0x1

    if-ne v7, v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 435
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 436
    :cond_0
    const-string v0, ","

    goto :goto_1

    .line 439
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT * FROM ultraviolet_rays WHERE _id IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") GROUP BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "sample_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 448
    .local v3, "query":Ljava/lang/String;
    const/4 v6, 0x0

    .line 450
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 451
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getShareData(Landroid/database/Cursor;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 453
    if-eqz v6, :cond_2

    .line 454
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 458
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "i":I
    .end local v8    # "selection":Ljava/lang/String;
    .end local v9    # "size":I
    :cond_2
    :goto_2
    return-object v0

    .line 453
    .restart local v3    # "query":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "i":I
    .restart local v8    # "selection":Ljava/lang/String;
    .restart local v9    # "size":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 454
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 458
    .end local v3    # "query":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "i":I
    .end local v8    # "selection":Ljava/lang/String;
    .end local v9    # "size":I
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    const v2, 0x7f09002f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public insertData(Lcom/sec/android/app/shealth/uv/data/UvStateType;Z)V
    .locals 8
    .param p1, "stateType"    # Lcom/sec/android/app/shealth/uv/data/UvStateType;
    .param p2, "isHideSkinSettings"    # Z

    .prologue
    .line 46
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 47
    .local v2, "sampleTime":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "10029_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "mDeviceId":Ljava/lang/String;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 49
    .local v4, "values":Landroid/content/ContentValues;
    const-string/jumbo v5, "uv_index"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/uv/data/UvStateType;->getScore()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 50
    const-string/jumbo v5, "sample_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 51
    const-string/jumbo v5, "user_device__id"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    if-eqz v5, :cond_0

    .line 54
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    const-string v5, "UvDatabaseHelper"

    const-string v6, "Got invalid data from sensor unable to insert Data"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateCommentById(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 281
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 282
    .local v2, "values":Landroid/content/ContentValues;
    const-string v5, "comment"

    invoke-virtual {v2, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v0, "_id=?"

    .line 284
    .local v0, "selection":Ljava/lang/String;
    new-array v1, v3, [Ljava/lang/String;

    aput-object p1, v1, v4

    .line 285
    .local v1, "selectionArgs":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/sdk/health/content/ShealthContract$UltravioletRays;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6, v2, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method
