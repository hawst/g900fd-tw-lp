.class final enum Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;
.super Ljava/lang/Enum;
.source "CignaLibraryBaseListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

.field public static final enum NO_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

.field public static final enum WITH_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    const-string v1, "WITH_ICON"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->WITH_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    const-string v1, "NO_ICON"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->NO_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->WITH_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->NO_ICON:Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->$VALUES:[Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryBaseListAdapter$TYPE;

    return-object v0
.end method
