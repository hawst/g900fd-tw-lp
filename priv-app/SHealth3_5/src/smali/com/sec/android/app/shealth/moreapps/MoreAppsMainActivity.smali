.class public Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "MoreAppsMainActivity.java"


# instance fields
.field private isMeasureCompleted:Z

.field private listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

.field private mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

.field private mServiceManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

.field private mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

.field private pincodelaunch:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 56
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->isMeasureCompleted:Z

    .line 57
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->pincodelaunch:Z

    .line 93
    new-instance v0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$1;-><init>(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Lcom/sec/android/app/shealth/moreapps/WebViewFragment;)Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->showErrorPopup(Ljava/lang/String;)V

    return-void
.end method

.method private showErrorPopup(Ljava/lang/String;)V
    .locals 4
    .param p1, "errorFlag"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f0900e3

    const/4 v2, 0x0

    .line 220
    const-string/jumbo v1, "network"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 223
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 224
    const v1, 0x7f090cd6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 225
    new-instance v1, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$2;-><init>(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 234
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 257
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    const-string/jumbo v1, "timeout"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 241
    .restart local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090cda

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 243
    new-instance v1, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$3;-><init>(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 253
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mFailedMsgDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showLoadingPopUp(Z)V
    .locals 2
    .param p1, "cancellable"    # Z

    .prologue
    .line 286
    new-instance v0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$4;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$4;-><init>(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    new-instance v1, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity$5;-><init>(Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mLoadingPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    .line 318
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 159
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 162
    return-void
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    const-string v0, "com.sec.shealth.help.action.MORE_APPS"

    return-object v0
.end method

.method public isPincodelaunch()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->pincodelaunch:Z

    return v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->webServer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->getWebView()Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    goto :goto_0

    .line 204
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mServiceManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mServiceManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->clearMoreAppsInfo()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/moreapps/service/Util;->isInternetOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mServiceManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    .line 83
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->showLoadingPopUp(Z)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mServiceManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->listener:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->requestMoreAppsInfo(Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager$MoreAppsInfoRequestListener;)V

    .line 91
    :goto_0
    return-void

    .line 87
    :cond_0
    const-string/jumbo v0, "network"

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->showErrorPopup(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100026

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x0

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onLogSelected()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 261
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->isMeasureCompleted:Z

    if-nez v1, :cond_0

    .line 264
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 274
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 267
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 268
    .local v0, "intentSetting":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    const-string v1, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 271
    const/4 v1, 0x1

    goto :goto_0

    .line 264
    :pswitch_data_0
    .packed-switch 0x7f080ca4
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    .line 176
    if-eqz p1, :cond_0

    .line 177
    :try_start_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f100026

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 179
    const v1, 0x7f0802db

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 180
    const v1, 0x7f080ca4

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 181
    const v1, 0x7f080c8a

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 182
    const v1, 0x7f080c90

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_0
    :goto_0
    return v3

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 139
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v3

    const-string/jumbo v4, "security_pin_enabled"

    invoke-virtual {v3, p0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->pincodelaunch:Z

    if-nez v3, :cond_0

    .line 141
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 142
    .local v0, "intent":Landroid/content/Intent;
    const-class v3, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    invoke-virtual {v0, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 143
    const/high16 v3, 0x20000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 144
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 146
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 147
    iget-object v3, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mServiceManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    if-eqz v3, :cond_1

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mServiceManager:Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->checkMoreAppsInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, "response":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/moreapps/service/Util;->PREF_SUB_TAB_KEY:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/moreapps/service/Util;->getPrefInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    .line 151
    .local v2, "tab":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->newInstance(ILjava/lang/String;Landroid/content/Context;)Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->mWebViewFlagment:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 155
    .end local v1    # "response":Ljava/lang/String;
    .end local v2    # "tab":I
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->pincodelaunch:Z

    .line 131
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onStart()V

    .line 132
    return-void
.end method

.method public setPincodelaunch(Z)V
    .locals 0
    .param p1, "pincodelaunch"    # Z

    .prologue
    .line 327
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->pincodelaunch:Z

    .line 328
    return-void
.end method
