.class public Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;
.super Ljava/lang/Object;
.source "TGHCache.java"


# static fields
.field public static infoTemperatureUnit:Ljava/lang/String;

.field static unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 10
    const-string v0, "F"

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    .line 11
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setInfoTemperatureUnit(Ljava/lang/String;)V
    .locals 1
    .param p0, "temperatureunit"    # Ljava/lang/String;

    .prologue
    .line 15
    if-eqz p0, :cond_1

    const-string v0, "C"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "F"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17
    :cond_0
    sput-object p0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    .line 23
    :cond_1
    return-void
.end method

.method public static updateTemperatureInfoUnit()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->unitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getTemperatureUnit()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sput-object v0, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    .line 28
    return-void

    .line 26
    :cond_0
    const-string v0, "F"

    goto :goto_0
.end method
