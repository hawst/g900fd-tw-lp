.class public Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;
.super Landroid/view/View;
.source "HoloCircleSeekBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$OnCircleSeekBarChangeListener;
    }
.end annotation


# static fields
.field private static final STATE_ANGLE:Ljava/lang/String; = "angle"

.field private static final STATE_PARENT:Ljava/lang/String; = "parent"


# instance fields
.field private arc_finish_radians:I

.field private conversion:I

.field private end_wheel:I

.field imageHandler:Landroid/os/Handler;

.field private init_position:I

.field private isTimerTaskRunning:Z

.field private mAngle:F

.field private final mAnimationIntervalTime:I

.field private mArcColor:Landroid/graphics/Paint;

.field private mBackupValue:I

.field private mCircleTextColor:Landroid/graphics/Paint;

.field private mColorCenterHalo:Landroid/graphics/Paint;

.field private mColorCenterHaloRectangle:Landroid/graphics/RectF;

.field private mColorWheelPaint:Landroid/graphics/Paint;

.field private mColorWheelRadius:F

.field private mColorWheelRectangle:Landroid/graphics/RectF;

.field private mContext:Landroid/content/Context;

.field private mCurrentAniValue:I

.field private mCurrentValue:I

.field private mMax:I

.field private mOnCircleSeekBarChangeListener:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$OnCircleSeekBarChangeListener;

.field private mPointerBitmap:Landroid/graphics/Bitmap;

.field private mPointerColor:Landroid/graphics/Paint;

.field private mPointerHaloPaint:Landroid/graphics/Paint;

.field private mPointerPressBitmap:Landroid/graphics/Bitmap;

.field private mPointerRadius:I

.field private mPressHaloPaint:I

.field private mTimerTask:Ljava/util/TimerTask;

.field private mTranslationOffset:F

.field private pointerPosition:[F

.field private pointer_color:I

.field private pointer_halo_color:I

.field private s:Landroid/graphics/SweepGradient;

.field private start_arc:I

.field private textPaint:Landroid/graphics/Paint;

.field private text_color:I

.field private text_size:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 46
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 51
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 57
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->conversion:I

    .line 58
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    .line 65
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 66
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    .line 70
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 358
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->imageHandler:Landroid/os/Handler;

    .line 82
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 51
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 57
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->conversion:I

    .line 58
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    .line 65
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 66
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    .line 70
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 358
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->imageHandler:Landroid/os/Handler;

    .line 88
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 89
    invoke-direct {p0, p2, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 93
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    .line 51
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAnimationIntervalTime:I

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->conversion:I

    .line 58
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    .line 65
    const/16 v0, 0x168

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 66
    const/16 v0, 0x10e

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    .line 70
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    .line 358
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->imageHandler:Landroid/os/Handler;

    .line 94
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mContext:Landroid/content/Context;

    .line 95
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->init(Landroid/util/AttributeSet;I)V

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$112(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentValue:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mBackupValue:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # F

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;I)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;[F)[F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # [F

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointerPosition:[F

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;F)[F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;
    .param p1, "x1"    # F

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v0

    return-object v0
.end method

.method private calculateAngleFromRadians(I)F
    .locals 4
    .param p1, "radians"    # I

    .prologue
    .line 252
    add-int/lit16 v0, p1, 0x10e

    int-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v0, v2

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private calculateAngleFromText(I)D
    .locals 10
    .param p1, "position"    # I

    .prologue
    const-wide v0, 0x4056800000000000L    # 90.0

    .line 230
    if-eqz p1, :cond_0

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    if-lt p1, v6, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-wide v0

    .line 233
    :cond_1
    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    int-to-double v6, v6

    int-to-double v8, p1

    div-double v2, v6, v8

    .line 234
    .local v2, "f":D
    const-wide v6, 0x4076800000000000L    # 360.0

    div-double v4, v6, v2

    .line 235
    .local v4, "f_r":D
    add-double/2addr v0, v4

    .line 237
    .local v0, "ang":D
    goto :goto_0
.end method

.method private calculatePointerPosition(F)[F
    .locals 6
    .param p1, "angle"    # F

    .prologue
    .line 303
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    float-to-double v2, v2

    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v0, v2

    .line 304
    .local v0, "x":F
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    float-to-double v2, v2

    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    .line 306
    .local v1, "y":F
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v0, v2, v3

    const/4 v3, 0x1

    aput v1, v2, v3

    return-object v2
.end method

.method private calculateRadiansFromAngle(F)I
    .locals 6
    .param p1, "angle"    # F

    .prologue
    .line 241
    float-to-double v2, p1

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    div-double/2addr v2, v4

    double-to-float v1, v2

    .line 242
    .local v1, "unit":F
    const/4 v2, 0x0

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 243
    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    .line 245
    :cond_0
    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v2, v1

    const/high16 v3, 0x43870000    # 270.0f

    sub-float/2addr v2, v3

    float-to-int v0, v2

    .line 246
    .local v0, "radians":I
    if-gez v0, :cond_1

    .line 247
    add-int/lit16 v0, v0, 0x168

    .line 248
    :cond_1
    return v0
.end method

.method private calculateTextFromStartAngle(F)I
    .locals 4
    .param p1, "angle"    # F

    .prologue
    .line 224
    move v1, p1

    .line 225
    .local v1, "m":F
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float v0, v2, v1

    .line 226
    .local v0, "f":F
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    float-to-int v2, v2

    return v2
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    const/high16 v5, 0x41b00000    # 22.0f

    const/4 v4, 0x1

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/R$styleable;->workout_HoloCircleSeekBar:[I

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 102
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->initAttributes(Landroid/content/res/TypedArray;)V

    .line 104
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 106
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->s:Landroid/graphics/SweepGradient;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointer_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 112
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    const v2, -0xff0001

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorCenterHalo:Landroid/graphics/Paint;

    const/16 v2, 0xcc

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 116
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointer_halo_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPressHaloPaint:I

    add-int/lit8 v2, v2, 0x3c

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 120
    new-instance v1, Landroid/graphics/Paint;

    const/16 v2, 0x41

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->text_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->textPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->text_size:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 126
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerRadius:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointer_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    const v2, -0xa041df

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 136
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCircleTextColor:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 140
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->init_position:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculateAngleFromText(I)D

    move-result-wide v1

    double-to-int v1, v1

    add-int/lit8 v1, v1, -0x5a

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 142
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->end_wheel:I

    if-le v1, v2, :cond_0

    .line 143
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->end_wheel:I

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 144
    :cond_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->end_wheel:I

    if-le v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->end_wheel:I

    :goto_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->invalidate()V

    .line 148
    return-void

    .line 144
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    goto :goto_0
.end method

.method private initAttributes(Landroid/content/res/TypedArray;)V
    .locals 3
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, 0x0

    .line 151
    const/16 v0, 0x1f

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerRadius:I

    .line 152
    const/16 v0, 0x29

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPressHaloPaint:I

    .line 153
    const/4 v0, 0x2

    const/16 v1, 0x64

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    .line 155
    const/4 v0, 0x6

    const/16 v1, 0x5f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->text_size:I

    .line 156
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->init_position:I

    .line 158
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    .line 159
    const/4 v0, 0x5

    const/16 v1, 0x168

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->end_wheel:I

    .line 161
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->init_position:I

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    if-ge v0, v1, :cond_0

    .line 162
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculateTextFromStartAngle(F)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->init_position:I

    .line 164
    :cond_0
    const v0, -0xa041df

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointer_color:I

    .line 165
    return-void
.end method


# virtual methods
.method public doTimerTask()V
    .locals 6

    .prologue
    .line 341
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 342
    .local v0, "t":Ljava/util/Timer;
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentAniValue:I

    .line 343
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mBackupValue:I

    .line 344
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    .line 345
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v1, :cond_0

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v1}, Ljava/util/TimerTask;->cancel()Z

    .line 347
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 349
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 355
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x64

    const-wide/16 v4, 0x1e

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 356
    return-void
.end method

.method public doUiChanged()V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->imageHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 386
    return-void
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->conversion:I

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 171
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerBitmap:Landroid/graphics/Bitmap;

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerPressBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerPressBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 175
    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerPressBitmap:Landroid/graphics/Bitmap;

    .line 177
    :cond_1
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 178
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 201
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTranslationOffset:F

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTranslationOffset:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    add-int/lit16 v0, v0, 0x10e

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->end_wheel:I

    if-le v0, v3, :cond_0

    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->end_wheel:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    :goto_0
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 206
    return-void

    .line 204
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->start_arc:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->getSuggestedMinimumHeight()I

    move-result v3

    invoke-static {v3, p2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->getDefaultSize(II)I

    move-result v0

    .line 212
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->getDefaultSize(II)I

    move-result v2

    .line 213
    .local v2, "width":I
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 214
    .local v1, "min":I
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->setMeasuredDimension(II)V

    .line 216
    int-to-float v3, v1

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTranslationOffset:F

    .line 217
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTranslationOffset:F

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPressHaloPaint:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    const/high16 v4, 0x41e00000    # 28.0f

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    .line 218
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRectangle:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v4, v4

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v5, v5

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 219
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorCenterHaloRectangle:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v4, v4

    div-float/2addr v4, v8

    iget v5, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    neg-float v5, v5

    div-float/2addr v5, v8

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    div-float/2addr v6, v8

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelRadius:F

    div-float/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 220
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointerPosition:[F

    .line 221
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 322
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    .line 324
    .local v0, "savedState":Landroid/os/Bundle;
    const-string/jumbo v2, "parent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .line 325
    .local v1, "superState":Landroid/os/Parcelable;
    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 327
    const-string v2, "angle"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    .line 328
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculateRadiansFromAngle(F)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 329
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointerPosition:[F

    .line 330
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 311
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 313
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 314
    .local v0, "state":Landroid/os/Bundle;
    const-string/jumbo v2, "parent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 315
    const-string v2, "angle"

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 317
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 289
    const/4 v0, 0x0

    return v0
.end method

.method public setMax(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 265
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    .line 266
    return-void
.end method

.method public setOnSeekBarChangeListener(Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$OnCircleSeekBarChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$OnCircleSeekBarChangeListener;

    .prologue
    .line 333
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mOnCircleSeekBarChangeListener:Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar$OnCircleSeekBarChangeListener;

    .line 334
    return-void
.end method

.method public setPointerColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerColor:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 190
    return-void
.end method

.method public setPressPointerColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mPointerHaloPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 194
    return-void
.end method

.method public setProgress(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    if-eqz v0, :cond_0

    .line 270
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mBackupValue:I

    .line 277
    :goto_0
    return-void

    .line 273
    :cond_0
    int-to-float v0, p1

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mMax:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    .line 274
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->arc_finish_radians:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculateAngleFromRadians(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    .line 275
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mAngle:F

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->calculatePointerPosition(F)[F

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->pointerPosition:[F

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->invalidate()V

    goto :goto_0
.end method

.method public setProgressBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mArcColor:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 182
    return-void
.end method

.method public setProgressColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mColorWheelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 186
    return-void
.end method

.method public setProgressWithAnimation(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->stopTask()V

    .line 283
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mCurrentValue:I

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->doTimerTask()V

    .line 285
    return-void
.end method

.method public stopTask()V
    .locals 1

    .prologue
    .line 389
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 392
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/widget/HoloCircleSeekBar;->mTimerTask:Ljava/util/TimerTask;

    .line 394
    :cond_0
    return-void
.end method
