.class public final enum Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;
.super Ljava/lang/Enum;
.source "SelectedPanelHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

.field public static final enum MEAL_DATA:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

.field public static final enum MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;


# instance fields
.field public uniqueKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 237
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    const-string v1, "MEAL_ITEM"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    const-string v1, "MEAL_DATA"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_DATA:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    .line 236
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_DATA:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 241
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->uniqueKey:Ljava/lang/String;

    .line 243
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 236
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;
    .locals 1

    .prologue
    .line 236
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    return-object v0
.end method
