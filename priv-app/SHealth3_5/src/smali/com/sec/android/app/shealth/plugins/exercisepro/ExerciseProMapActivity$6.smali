.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0

    .prologue
    .line 405
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->hrmContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;-><init>(Landroid/app/Activity;)V

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .line 412
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mFragmentHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setFragmentHandler(Landroid/os/Handler;)V

    .line 414
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "heartrate_warning_checked"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 415
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->showInfomationDialog()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    .line 424
    :goto_0
    return-void

    .line 417
    :cond_0
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 418
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v2

    const-string v3, "ExerciseProHRMFragment"

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V

    .line 420
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$6;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mFragmentHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method
