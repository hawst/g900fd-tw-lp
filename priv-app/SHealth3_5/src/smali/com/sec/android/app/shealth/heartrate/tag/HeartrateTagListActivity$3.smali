.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;
.super Ljava/lang/Object;
.source "HeartrateTagListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDeleteMode:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isSelectMode:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isItemClicked:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # setter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isItemClicked:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$302(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)Z

    .line 177
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 178
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isDeleteMode:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$100(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isSelectMode:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->isCheckAll()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setCheckAll(Z)V

    .line 185
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->isLogSelected(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setLogSelected(Ljava/lang/String;Z)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getSelectedLogCount()I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;I)V

    .line 192
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getSelectedLogCount()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getTotalChildCount()I

    move-result v2

    if-ne v1, v2, :cond_6

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->toggleSelectAllCheckBox(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    .line 200
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->isSelectMode:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getSelectedLogCount()I

    move-result v1

    if-nez v1, :cond_7

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    goto/16 :goto_0

    .line 189
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->setLogSelected(Ljava/lang/String;Z)V

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getSelectedLogCount()I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->updateSelectedCount(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;I)V

    goto :goto_1

    .line 196
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->toggleSelectAllCheckBox(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    goto :goto_2

    .line 204
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    goto/16 :goto_0

    .line 207
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->mHrTagListAdapter:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;)Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListAdapter;->getSelectedLogCount()I

    move-result v1

    if-nez v1, :cond_9

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->enableActionBarButtons(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    goto/16 :goto_0

    .line 210
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity$3;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->enableActionBarButtons(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;->access$800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateTagListActivity;Z)V

    goto/16 :goto_0
.end method
