.class Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;
.super Ljava/lang/Object;
.source "CignaMainExpandableAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ArrowClickListener"
.end annotation


# instance fields
.field groupPosition:I

.field lv:Landroid/widget/ExpandableListView;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;Landroid/widget/ExpandableListView;I)V
    .locals 0
    .param p2, "lv"    # Landroid/widget/ExpandableListView;
    .param p3, "groupPosition"    # I

    .prologue
    .line 563
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 564
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->lv:Landroid/widget/ExpandableListView;

    .line 565
    iput p3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->groupPosition:I

    .line 566
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 570
    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->groupPosition:I

    invoke-static {v3}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v1

    .line 571
    .local v1, "packedPostion":J
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->lv:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v1, v2}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v0

    .line 572
    .local v0, "flatPosition":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->lv:Landroid/widget/ExpandableListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ExpandableListView;->setSoundEffectsEnabled(Z)V

    .line 573
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->lv:Landroid/widget/ExpandableListView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->groupPosition:I

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->lv:Landroid/widget/ExpandableListView;

    iget v7, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->groupPosition:I

    invoke-virtual {v6, v7}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v6

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->lv:Landroid/widget/ExpandableListView;

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->lv:Landroid/widget/ExpandableListView;

    invoke-virtual {v5, v0}, Landroid/widget/ExpandableListView;->getItemIdAtPosition(I)J

    move-result-wide v5

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/widget/ExpandableListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 574
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter$ArrowClickListener;->lv:Landroid/widget/ExpandableListView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/ExpandableListView;->setSoundEffectsEnabled(Z)V

    .line 575
    return-void
.end method
