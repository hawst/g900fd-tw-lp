.class Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;
.super Ljava/lang/Object;
.source "AddFavoritesActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->onChange(ZLandroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 410
    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RegistryContentObserver "

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->access$1100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->access$1100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->access$1100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$1200(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->access$1100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mFitnessAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$1200(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->notifyDataSetChanged()V

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->access$1100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$500(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->access$1100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mCareAdapter:Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$500(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->notifyDataSetChanged()V

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver$1;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->mActivity:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;->access$1100(Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setAdapter()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$1300(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V

    .line 422
    return-void
.end method
