.class public final enum Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;
.super Ljava/lang/Enum;
.source "RestoreNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RestoreStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

.field public static final enum None:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

.field public static final enum Restore:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    const-string v1, "None"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;->None:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    const-string v1, "Restore"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;->Restore:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    .line 16
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;->None:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;->Restore:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;->$VALUES:[Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;->$VALUES:[Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreStatus;

    return-object v0
.end method
