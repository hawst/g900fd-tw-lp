.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "SpO2SummaryFragmentNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->initSCover()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)V
    .locals 0

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    const/4 v2, 0x1

    .line 440
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    if-ne v0, v2, :cond_4

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$402(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Landroid/os/Handler;)Landroid/os/Handler;

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2Activity:Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$600(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    if-nez v0, :cond_2

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorforSCover(I)V

    .line 467
    :cond_1
    :goto_0
    return-void

    .line 451
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSpO2DatabaseHelper:Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$700(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DatabaseHelper;->getDataCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstStatus:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mFirstSpO2ValueForTesting:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$800(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startSensorforSCover(I)V

    goto :goto_0

    .line 456
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iget-object v0, v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 460
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$400(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->sensorRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$500(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 465
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$2;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndUpdateUI(IF)V

    goto :goto_0
.end method
