.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;
.super Ljava/util/TimerTask;
.source "LapClock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LapClockTimerTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$1;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->setCurentTimeMillis()V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockTimerTask;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;

    iget-wide v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock;->second:J

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/LapClock$LapClockListener;->onUpdateDisplay(J)V

    .line 156
    :cond_0
    return-void
.end method
