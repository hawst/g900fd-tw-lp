.class Lcom/sec/android/app/shealth/moreapps/WebViewFragment$CustomWebView;
.super Landroid/webkit/WebViewClient;
.source "WebViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomWebView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/moreapps/WebViewFragment;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$CustomWebView;->this$0:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/moreapps/WebViewFragment;Lcom/sec/android/app/shealth/moreapps/WebViewFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/moreapps/WebViewFragment$1;

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$CustomWebView;-><init>(Lcom/sec/android/app/shealth/moreapps/WebViewFragment;)V

    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 8
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 159
    if-eqz p2, :cond_2

    const-string v4, "http://"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "https://"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 160
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v1, v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 162
    .local v1, "intent":Landroid/content/Intent;
    # getter for: Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->access$300()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 163
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v3, v1, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 164
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    move v2, v5

    .line 167
    .local v2, "isIntentSafe":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 169
    iget-object v4, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$CustomWebView;->this$0:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;

    invoke-virtual {v4, v6}, Lcom/sec/android/app/shealth/moreapps/MoreAppsMainActivity;->setPincodelaunch(Z)V

    .line 170
    iget-object v4, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$CustomWebView;->this$0:Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->startActivity(Landroid/content/Intent;)V

    :goto_1
    move v4, v5

    .line 179
    .end local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "isIntentSafe":Z
    .end local v3    # "packageManager":Landroid/content/pm/PackageManager;
    :goto_2
    return v4

    .restart local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v3    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_0
    move v2, v6

    .line 164
    goto :goto_0

    .line 174
    .restart local v2    # "isIntentSafe":Z
    :cond_1
    const-string v4, "WebViewFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Intent can not be resolved for Action : Intent.ACTION_VIEW and URI : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 179
    .end local v0    # "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "isIntentSafe":Z
    .end local v3    # "packageManager":Landroid/content/pm/PackageManager;
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v4

    goto :goto_2
.end method
