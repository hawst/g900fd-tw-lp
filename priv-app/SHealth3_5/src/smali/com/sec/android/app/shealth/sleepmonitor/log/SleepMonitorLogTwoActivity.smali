.class public Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;
.source "SleepMonitorLogTwoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;
    }
.end annotation


# static fields
.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field private static final MONTHS_QUERY:Ljava/lang/String; = "SELECT _id, avg(RANGE) as AVGSLEPT, rise_time, bed_time, dayDataLong, month FROM (SELECT a.rise_time as rise_time, a._id as _id, a.bed_time as bed_time, sum(a.rise_time-a.bed_time) AS RANGE, (a.bed_time-b.q) AS dayDataLong, a.rise_time as TIME, strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day, strftime(\'%Y-%m\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS month FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id WHERE a.sync_status != 170004 GROUP BY day) GROUP BY month ORDER BY rise_time DESC;"

.field public static final REQUEST_FOR_MEMO_EDIT:I = 0x2711

.field public static SELECTED_FILTER_INDEX:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;

.field public graphFragmentSIC:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

.field protected monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->TAG:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->SELECTED_FILTER_INDEX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;-><init>()V

    .line 258
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->executeQuery(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method private executeQuery(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "QueryIndex"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const-string v3, "SELECT _id, avg(RANGE) as AVGSLEPT, rise_time, bed_time, dayDataLong, month FROM (SELECT a.rise_time as rise_time, a._id as _id, a.bed_time as bed_time, sum(a.rise_time-a.bed_time) AS RANGE, (a.bed_time-b.q) AS dayDataLong, a.rise_time as TIME, strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day, strftime(\'%Y-%m\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS month FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id WHERE a.sync_status != 170004 GROUP BY day) GROUP BY month ORDER BY rise_time DESC;"

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->mCursor:Landroid/database/Cursor;

    .line 82
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mCursor = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-void
.end method


# virtual methods
.method protected applyFilter(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyFilter ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->executeQuery(Ljava/lang/CharSequence;)V

    .line 137
    return-void
.end method

.method protected getAdatper()Lcom/sec/android/app/shealth/framework/ui/base/LogAdapter;
    .locals 3

    .prologue
    .line 64
    const-string v0, "0==0"

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->executeQuery(Ljava/lang/CharSequence;)V

    .line 65
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;-><init>(Landroid/database/Cursor;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    return-object v0
.end method

.method protected getColumnNameForMemo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    const-string v0, "comment"

    return-object v0
.end method

.method protected getContextMenuHeader(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 102
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->TAG:Ljava/lang/String;

    const-string v1, "getContextMenuHeader"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDeleteList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "selectedTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x1

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v0, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 224
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 226
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-lt v4, v7, :cond_0

    .line 228
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v7

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 232
    .local v3, "tag":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 230
    .end local v3    # "tag":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "tag":Ljava/lang/String;
    goto :goto_1

    .line 235
    .end local v2    # "i":I
    .end local v3    # "tag":Ljava/lang/String;
    :cond_1
    sget v4, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->SELECTED_FILTER_INDEX:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getFilterType(I)Ljava/lang/String;

    move-result-object v1

    .line 236
    .local v1, "ft":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->applyFilter(Ljava/lang/CharSequence;)V

    .line 238
    :cond_2
    return-object v0
.end method

.method protected getFilterRange()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 142
    .local v0, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const v1, 0x7f090076

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    const v1, 0x7f0901ac

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    const v1, 0x7f0901ab

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    return-object v0
.end method

.method protected getFilterType(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 150
    sput p1, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->SELECTED_FILTER_INDEX:I

    .line 151
    const-string v0, "0==0"

    return-object v0
.end method

.method protected getLogDataTypeURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getSelectedLogDataForSharing()Ljava/lang/String;
    .locals 23

    .prologue
    .line 162
    const/4 v11, 0x0

    .line 163
    .local v11, "i":I
    new-instance v15, Ljava/lang/StringBuffer;

    const-string v1, ""

    invoke-direct {v15, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 164
    .local v15, "shareData":Ljava/lang/StringBuffer;
    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "efficiency"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "rise_time"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "bed_time"

    aput-object v2, v3, v1

    .line 165
    .local v3, "projection":[Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuffer;

    const-string v1, "_id IN ("

    invoke-direct {v14, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 166
    .local v14, "selectionClause":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 167
    .local v16, "size":I
    :goto_0
    add-int/lit8 v1, v16, -0x1

    if-ge v11, v1, :cond_0

    .line 168
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v9, "_"

    invoke-virtual {v1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x1

    aget-object v1, v1, v9

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 167
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 170
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->getCheckedItems()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v9, "_"

    invoke-virtual {v1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x1

    aget-object v1, v1, v9

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 172
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string/jumbo v6, "rise_time DESC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 176
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_3

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 178
    :cond_1
    const-string/jumbo v1, "rise_time"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 179
    .local v5, "rise_time":J
    const-string v1, "bed_time"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 181
    .local v7, "bed_time":J
    cmp-long v1, v7, v5

    if-lez v1, :cond_2

    .line 183
    move-wide/from16 v18, v7

    .line 184
    .local v18, "swapper":J
    move-wide v7, v5

    .line 185
    move-wide/from16 v5, v18

    .line 188
    .end local v18    # "swapper":J
    :cond_2
    sub-long v1, v5, v7

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v1

    const-wide/32 v21, 0xea60

    div-long v12, v1, v21

    .line 189
    .local v12, "minute":J
    const-string v1, "efficiency"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    .line 191
    .local v4, "MotionLess_Efficiency":F
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    new-instance v9, Ljava/sql/Date;

    invoke-direct {v9, v7, v8}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v7, v8}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 192
    .local v17, "sleep_time":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    new-instance v9, Ljava/sql/Date;

    invoke-direct {v9, v5, v6}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5, v6}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 194
    .local v20, "wake_time":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v9, 0x7f090d8f

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-static {v12, v13, v0}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeInRequiredFormat(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v9, 0x7f090d58

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v9, p0

    invoke-static/range {v4 .. v9}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getMotionlessSleepTime(FJJLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    float-to-int v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v9, 0x7f090d59

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v9, 0x7f090d5a

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 206
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 210
    .end local v4    # "MotionLess_Efficiency":F
    .end local v5    # "rise_time":J
    .end local v7    # "bed_time":J
    .end local v12    # "minute":J
    .end local v17    # "sleep_time":Ljava/lang/String;
    .end local v20    # "wake_time":Ljava/lang/String;
    :cond_3
    if-eqz v10, :cond_4

    .line 211
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 213
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.sleepmonitor"

    const-string v9, "SL02"

    invoke-static {v1, v2, v9}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 210
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_5

    .line 211
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1
.end method

.method protected isMemoVisible(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 109
    const-string v1, "comment"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "comment":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 112
    :cond_0
    const/4 v1, 0x0

    .line 114
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->SELECTED_FILTER_INDEX:I

    .line 41
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->graphFragmentSIC:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .line 43
    const v0, 0x7f0205b7

    const v1, 0x7f09006a

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->setNoLogImageAndText(II)V

    .line 44
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->isDeleteMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100020

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 249
    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onPause()V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 60
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 48
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->onResume()V

    .line 49
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity$SleepDBContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 51
    const-string v0, "0==0"

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->executeQuery(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->mCursor:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->setGroupCursor(Landroid/database/Cursor;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->monitorLogAdapter:Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogAdapter;->notifyDataSetChanged(Z)V

    .line 54
    return-void
.end method

.method protected setUpSelectMode()V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/LogActivity;->setUpSelectMode()V

    .line 255
    return-void
.end method

.method protected showDetailScreen(Ljava/lang/String;)V
    .locals 5
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 87
    sget-object v2, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "showDetailScreen = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v0, v2, v3

    .line 89
    .local v0, "_id":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    .local v1, "detailIntent":Landroid/content/Intent;
    const-string v2, "_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const/16 v2, 0x2711

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 93
    return-void
.end method

.method protected updateMemo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 119
    if-eqz p1, :cond_0

    .line 120
    const-string v4, "_"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    aget-object v0, v4, v6

    .line 121
    .local v0, "_id":Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 122
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "comment"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "_id=?"

    .line 124
    .local v2, "selectionClause":Ljava/lang/String;
    new-array v1, v6, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v1, v5

    .line 125
    .local v1, "mSelectionArgs":[Ljava/lang/String;
    aput-object v0, v1, v5

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/log/SleepMonitorLogTwoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v2, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 130
    .end local v0    # "_id":Ljava/lang/String;
    .end local v1    # "mSelectionArgs":[Ljava/lang/String;
    .end local v2    # "selectionClause":Ljava/lang/String;
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method
