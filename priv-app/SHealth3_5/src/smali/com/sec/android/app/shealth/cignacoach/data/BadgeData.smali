.class public Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;
.super Ljava/lang/Object;
.source "BadgeData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAchieveDate:Ljava/lang/String;

.field private mAchieveTime:Ljava/lang/String;

.field private mBadgeDescription:Ljava/lang/String;

.field private mBadgeId:I

.field private mBadgeImage:Ljava/lang/String;

.field private mBadgeName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "badgeId"    # I
    .param p2, "badgeName"    # Ljava/lang/String;
    .param p3, "badgeDescription"    # Ljava/lang/String;
    .param p4, "achieveDate"    # Ljava/lang/String;
    .param p5, "achieveTime"    # Ljava/lang/String;
    .param p6, "badgeImage"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeId:I

    .line 18
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeName:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeDescription:Ljava/lang/String;

    .line 20
    iput-object p4, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mAchieveDate:Ljava/lang/String;

    .line 21
    iput-object p5, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mAchieveTime:Ljava/lang/String;

    .line 22
    iput-object p6, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeImage:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->readFromParcel(Landroid/os/Parcel;)V

    .line 27
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public getAchieveDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mAchieveDate:Ljava/lang/String;

    return-object v0
.end method

.method public getAchieveTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mAchieveTime:Ljava/lang/String;

    return-object v0
.end method

.method public getBadgeId()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeId:I

    return v0
.end method

.method public getBadgeImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeImage:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeName:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeId:I

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeName:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeDescription:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mAchieveDate:Ljava/lang/String;

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mAchieveTime:Ljava/lang/String;

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeImage:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mAchieveDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mAchieveTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/data/BadgeData;->mBadgeImage:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 70
    return-void
.end method
