.class Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CignaLanguageUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocaleChangeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 57
    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CignaTranslateUtils.LocaleChangeReceiver action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const-string v0, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CignaTranslateUtils Becuase unsupport cigna language, finished ~~"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 68
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CignaTranslateUtils Support cigna language ~~"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mChangeLanguageListenerList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mChangeLanguageListenerList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mChangeLanguageListenerList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$LocaleChangeReceiver;->this$0:Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->mChangeLanguageListenerList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;->access$100(Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLanguageUtils$OnChangeLanguageListener;->onChangeLanguage(Z)V

    goto :goto_0
.end method
