.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WFLDataReceivedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0

    .prologue
    .line 1320
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseReceived(FFFIIIIIIIIJ)V
    .locals 7
    .param p1, "distance"    # F
    .param p2, "cal"    # F
    .param p3, "speed"    # F
    .param p4, "totalStep"    # I
    .param p5, "walkingStep"    # I
    .param p6, "runStep"    # I
    .param p7, "updownStep"    # I
    .param p8, "activeTime"    # I
    .param p9, "inactiveTime"    # I
    .param p10, "healthySteps"    # I
    .param p11, "deviceType"    # I
    .param p12, "updatTime"    # J

    .prologue
    .line 1326
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onResponseReceived :: totalStep = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1328
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v0

    if-ne v0, p4, :cond_1

    if-nez p4, :cond_1

    .line 1354
    :cond_0
    :goto_0
    return-void

    .line 1332
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1333
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;

    move-object v1, p0

    move/from16 v2, p11

    move v3, p1

    move v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;IFFI)V

    invoke-virtual {v6, v0}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onWalkingModeChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "walkingMode"    # Ljava/lang/String;

    .prologue
    .line 1374
    return-void
.end method

.method public onWearableSyncComplete(II)V
    .locals 2
    .param p1, "error_code"    # I
    .param p2, "deviceType"    # I

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncTimeoutRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2600(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1359
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startSync(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Z)V

    .line 1360
    packed-switch p1, :pswitch_data_0

    .line 1370
    :goto_0
    return-void

    .line 1365
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$WFLDataReceivedListener;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->syncHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v0

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1360
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
