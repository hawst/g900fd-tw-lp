.class public Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;
.super Landroid/os/AsyncTask;
.source "KiesRestorer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mCompleteListener:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;

.field private final mContext:Landroid/content/Context;

.field private mFileCopyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;",
            ">;"
        }
    .end annotation
.end field

.field private final mKiesIntentName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "completeIntentName"    # Ljava/lang/String;
    .param p4, "endListener"    # Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;",
            ">;",
            "Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "fileList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mKiesIntentName:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mContext:Landroid/content/Context;

    .line 38
    iput-object p4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mCompleteListener:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;

    .line 39
    iput-object p3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mFileCopyList:Ljava/util/List;

    .line 40
    return-void
.end method

.method private deleteFilesAfterCancel()V
    .locals 6

    .prologue
    .line 101
    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mFileCopyList:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 103
    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mFileCopyList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    .line 105
    .local v0, "copyPath":Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;
    new-instance v1, Ljava/io/File;

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;->dest:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 106
    .local v1, "deleteMe":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 108
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "delete file course of cancel ; file = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->deleteRecursive(Ljava/io/File;)V

    goto :goto_0

    .line 113
    .end local v0    # "copyPath":Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;
    .end local v1    # "deleteMe":Ljava/io/File;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 48
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doInBackground intent name : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mKiesIntentName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    const/4 v1, 0x0

    .line 50
    .local v1, "i":I
    const-wide/16 v3, 0x0

    sput-wide v3, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->fileSizeSum:J

    .line 51
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mFileCopyList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 53
    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mFileCopyList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    .line 56
    .local v2, "preparedPath":Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;
    :try_start_0
    iget-object v3, v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;->source:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;->dest:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->backupFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 64
    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 61
    .local v0, "ex":Ljava/io/IOException;
    sget-object v3, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IO exception occured while copying File"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 65
    .end local v0    # "ex":Ljava/io/IOException;
    .end local v2    # "preparedPath":Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;
    :cond_0
    const/4 v3, 0x0

    return-object v3
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->deleteFilesAfterCancel()V

    .line 93
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 94
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 7
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v6, 0x0

    .line 71
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onPostExecute intent name : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mKiesIntentName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 73
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "sending restore complete intent for Kies "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mKiesIntentName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fileZise = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->fileSizeSum:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mKiesIntentName:Ljava/lang/String;

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 75
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "RESULT"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 76
    const-string v2, "ERR_CODE"

    invoke-virtual {v0, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 77
    const-string v2, "REQ_SIZE"

    sget-wide v3, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils;->fileSizeSum:J

    long-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 80
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mCompleteListener:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;

    if-eqz v2, :cond_0

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mCompleteListener:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mKiesIntentName:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;->onRestoreEnd(Ljava/lang/String;Landroid/content/Intent;)V

    .line 82
    :cond_0
    new-instance v1, Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/lang/Boolean;-><init>(Z)V

    .line 83
    .local v1, "value":Ljava/lang/Boolean;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->initialize(Landroid/content/Context;)V

    .line 84
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->getInstance()Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;

    move-result-object v2

    const-string/jumbo v3, "restored_data_from_kies"

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/shealth/framework/repository/configuration/SharedPrefManager;->setValue(Ljava/lang/String;Ljava/lang/Object;)Z

    .line 87
    return-void
.end method
