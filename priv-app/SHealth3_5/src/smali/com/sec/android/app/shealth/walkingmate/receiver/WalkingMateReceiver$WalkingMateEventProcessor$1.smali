.class final Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor$1;
.super Ljava/lang/Object;
.source "WalkingMateReceiver.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 137
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    check-cast p2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;->getService()Landroid/app/Service;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor$1;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor$1;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onLocaleChanged()V

    .line 141
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->mLocaleChangedConnection:Landroid/content/ServiceConnection;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor;->access$400()Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 143
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver$WalkingMateEventProcessor$1;->mDayStepService:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .line 147
    return-void
.end method
