.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;
.super Ljava/lang/Object;
.source "WalkingMateLogActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TitleUpdater"
.end annotation


# instance fields
.field mTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;Ljava/lang/String;)V
    .locals 0
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 675
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676
    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->mTitle:Ljava/lang/String;

    .line 677
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 681
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->mTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 683
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    .line 684
    .local v0, "actionBarView":Landroid/view/View;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 687
    .local v1, "description":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity$TitleUpdater;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 690
    const v3, 0x7f080304

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 692
    .local v2, "layout":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_0

    .line 694
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 696
    :cond_0
    return-void
.end method
