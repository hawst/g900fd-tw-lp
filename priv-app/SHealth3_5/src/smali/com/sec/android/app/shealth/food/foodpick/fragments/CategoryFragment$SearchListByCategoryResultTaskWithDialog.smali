.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;
.super Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;
.source "CategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchListByCategoryResultTaskWithDialog"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final listItem:Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;)V
    .locals 1
    .param p2, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    .prologue
    .line 501
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$1;)V

    .line 502
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->listItem:Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    .line 503
    return-void
.end method


# virtual methods
.method protected doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 515
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->listItem:Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodCategoryListItem;->getData()Lcom/sec/android/app/shealth/food/foodpick/search/TextOnlyData;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    .line 516
    .local v0, "subCategory":Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$900(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$900(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)V

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performFoodSearchByCategory(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v1

    return-object v1
.end method

.method protected getDisplayStage()Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;
    .locals 1

    .prologue
    .line 545
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->FOODS:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    return-object v0
.end method

.method protected onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 521
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->isAlreadyOnRequiredStage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 524
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->updateFavoritesFromDB(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)V

    .line 525
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;->getRequest()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;->getSubCategory()Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/FoodSubCategory;->getSubCategoryName()Ljava/lang/String;

    move-result-object v3

    .line 526
    .local v3, "subCategoryName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->generateListItems(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)Ljava/util/List;

    move-result-object v6

    .line 527
    .local v6, "foodListItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;)V

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->createAdapterForFoodItems(Landroid/content/Context;Ljava/util/List;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;
    invoke-static {v0, v2, v6, v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2400(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Landroid/content/Context;Ljava/util/List;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;

    move-result-object v1

    .line 537
    .local v1, "searchResultAdapter":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2500(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    move-result-object v0

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->setResult(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->access$2600(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2500(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    move-result-object v0

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->setAdapter(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->access$2700(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;)V

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    const/4 v2, 0x0

    sget-object v4, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;->FOODS:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;
    invoke-static {v5}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$2500(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    move-result-object v5

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->setDisplayedItems(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$1300(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter$OnItemClickListener;Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$DisplayedStage;Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;)V

    goto :goto_0
.end method

.method protected bridge synthetic onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 497
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V

    return-void
.end method

.method protected onTaskStarted()V
    .locals 1

    .prologue
    .line 507
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$TaskWithDialog;->onTaskStarted()V

    .line 508
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->isAlreadyOnRequiredStage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$SearchListByCategoryResultTaskWithDialog;->onTaskCanceled()V

    .line 511
    :cond_0
    return-void
.end method
