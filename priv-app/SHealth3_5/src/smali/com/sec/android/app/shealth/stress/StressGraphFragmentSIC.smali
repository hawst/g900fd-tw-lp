.class public Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;
.super Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.source "StressGraphFragmentSIC.java"


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field private static final FRAGMENT_SWITCHING_DELAY:I = 0xc8

.field public static final HOURS_IN_DAY:B = 0x18t

.field private static MARKING_COUNT:I = 0x0

.field private static MAX_Y:F = 0.0f

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field private static final MIN_Y:F = -3.0f

.field private static final PATTERN_24_HOURS:Ljava/lang/String; = "HH"

.field private static final PATTERN_DAY_FORMAT:Ljava/lang/String; = "dd"

.field private static final PATTERN_MONTH_FORMAT:Ljava/lang/String; = "MM"

.field private static final PATTERN_TIME:Ljava/lang/String; = "HH:mm"

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static volatile mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# instance fields
.field private density:F

.field private isConfigurationChanged:Z

.field private mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

.field private mDataCount:I

.field private mDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

.field private mDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field

.field private mInfoView:Lcom/sec/android/app/shealth/stress/StressInformationAreaView;

.field private mLegendView:Landroid/view/View;

.field private mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private scaledDensity:F

.field private switchToBodyfatSummary:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/high16 v0, 0x40400000    # 3.0f

    sput v0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->MAX_Y:F

    .line 82
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->MARKING_COUNT:I

    .line 97
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;-><init>()V

    .line 95
    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 96
    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->showSummarayFragment()V

    return-void
.end method

.method private isDataCountChanged()Z
    .locals 2

    .prologue
    .line 214
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getDataCount()I

    move-result v0

    .line 215
    .local v0, "stressDataCount":I
    iget v1, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDataCount:I

    if-ne v0, v1, :cond_0

    .line 216
    const/4 v1, 0x0

    .line 219
    :goto_0
    return v1

    .line 218
    :cond_0
    iput v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDataCount:I

    .line 219
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 3
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 251
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_1

    .line 253
    const-string v1, "STRESS_CHARTTAB"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 265
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 266
    return-void

    .line 255
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_2

    .line 257
    const-string v1, "STRESS_CHARTTAB"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 259
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_0

    .line 261
    const-string v1, "STRESS_CHARTTAB"

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private setChartData(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 6
    .param p1, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 421
    new-instance v4, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v4}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 422
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getGraphDatasByType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;

    move-result-object v3

    .line 424
    .local v3, "stressDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/stress/data/StressData;>;"
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 425
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    .line 430
    :goto_0
    const/4 v1, 0x0

    .line 431
    .local v1, "dataSize":I
    if-eqz v3, :cond_0

    .line 432
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 433
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_2

    .line 434
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>()V

    .line 435
    .local v0, "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setTime(J)V

    .line 436
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/stress/data/StressData;->getScore()D

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setValue(D)V

    .line 437
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 427
    .end local v0    # "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    .end local v1    # "dataSize":I
    .end local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 440
    .restart local v1    # "dataSize":I
    .restart local v2    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatas:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->setAllData(Ljava/util/List;)V

    .line 441
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {p1, v4}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 442
    return-void
.end method

.method private setMultiChartData(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 15
    .param p1, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 446
    const-wide/16 v10, 0x0

    .line 447
    .local v10, "valueMax":D
    const-wide/16 v12, 0x0

    .line 448
    .local v12, "valueMin":D
    const-wide/16 v8, 0x0

    .line 449
    .local v8, "valueAvg":D
    const-wide/16 v6, 0x0

    .line 451
    .local v6, "time":J
    new-instance v14, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;

    invoke-direct {v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartCandleTimeSeries;-><init>()V

    iput-object v14, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    .line 452
    new-instance v14, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    iput-object v14, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    .line 453
    iget-object v14, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getGraphDatasByType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/util/ArrayList;

    move-result-object v4

    .line 455
    .local v4, "stressDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/stress/data/StressData;>;"
    const/4 v2, 0x0

    .line 456
    .local v2, "dataSize":I
    if-eqz v4, :cond_0

    .line 457
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 458
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 460
    new-instance v1, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;-><init>()V

    .line 461
    .local v1, "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;
    new-instance v5, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    invoke-direct {v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>()V

    .line 463
    .local v5, "tdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/stress/data/StressData;->getMax()D

    move-result-wide v10

    .line 464
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/stress/data/StressData;->getMin()D

    move-result-wide v12

    .line 465
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/stress/data/StressData;->getAverage()D

    move-result-wide v8

    .line 466
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/stress/data/StressData;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/stress/data/StressData;->getSampleTime()J

    move-result-wide v6

    .line 468
    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->setTime(J)V

    .line 469
    invoke-virtual {v1, v10, v11}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->setValueHigh(D)V

    .line 470
    invoke-virtual {v1, v12, v13}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;->setValueLow(D)V

    .line 472
    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setTime(J)V

    .line 473
    invoke-virtual {v5, v8, v9}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setValue(D)V

    .line 475
    iget-object v14, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v14, v5}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 476
    iget-object v14, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    invoke-virtual {v14, v1}, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;)V

    .line 458
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 479
    .end local v1    # "cdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeCandleData;
    .end local v5    # "tdata":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    :cond_1
    iget-object v14, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mCandleSeries:Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 480
    iget-object v14, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 481
    return-void
.end method

.method private setStartVisual(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 13
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 325
    const/4 v1, 0x0

    .line 326
    .local v1, "level":I
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_5

    .line 327
    const/4 v1, 0x0

    .line 334
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getSize()I

    move-result v0

    if-lez v0, :cond_4

    .line 335
    const-wide/16 v7, 0x0

    .line 336
    .local v7, "selectedTime":J
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_7

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    .line 341
    :goto_1
    const-wide/16 v11, 0x0

    .local v11, "startTimes":J
    const-wide/16 v9, 0x0

    .line 343
    .local v9, "startTime":J
    sget v5, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->MARKING_COUNT:I

    .line 344
    .local v5, "markingCount":I
    const/4 v4, 0x1

    .line 346
    .local v4, "intervel":I
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-nez v0, :cond_1

    .line 347
    sput-object p2, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 350
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_a

    .line 355
    invoke-static {}, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->getInstance()Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;

    move-result-object v2

    sget-object v0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v3, :cond_9

    const-string v0, "%Y-%m-%d"

    :goto_2
    invoke-virtual {v2, v0, v7, v8}, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v11

    .line 356
    const-wide/16 v2, 0x0

    cmp-long v0, v11, v2

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v11

    .line 357
    :cond_2
    const-wide/32 v2, 0x2160ec0

    sub-long v2, v11, v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/stress/utils/DateFormatUtil;->getStartHourToMillis(J)J

    move-result-wide v9

    .line 359
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setScrollRangeDepthLevel(I)V

    .line 360
    const/16 v4, 0x3c

    .line 361
    const/16 v5, 0xd

    .line 377
    :goto_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 379
    .local v6, "calendar":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v2

    cmp-long v0, v11, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v0, v2, :cond_3

    .line 381
    invoke-virtual {v6, v11, v12}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 382
    const/16 v0, 0xb

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 383
    const/16 v0, 0xc

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 384
    const/16 v0, 0xd

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 385
    const/16 v0, 0xe

    const/4 v2, 0x0

    invoke-virtual {v6, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 386
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v11

    .line 389
    :cond_3
    sput-object p2, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 390
    long-to-double v2, v9

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 391
    long-to-double v2, v11

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 393
    .end local v4    # "intervel":I
    .end local v5    # "markingCount":I
    .end local v6    # "calendar":Ljava/util/Calendar;
    .end local v7    # "selectedTime":J
    .end local v9    # "startTime":J
    .end local v11    # "startTimes":J
    :cond_4
    return-void

    .line 328
    :cond_5
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_6

    .line 329
    const/4 v1, 0x2

    goto/16 :goto_0

    .line 330
    :cond_6
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_0

    .line 331
    const/4 v1, 0x5

    goto/16 :goto_0

    .line 339
    .restart local v7    # "selectedTime":J
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/stress/StressInformationAreaView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/stress/StressInformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->getSelectedDateInChart()J

    move-result-wide v7

    :goto_4
    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v7

    goto :goto_4

    .line 355
    .restart local v4    # "intervel":I
    .restart local v5    # "markingCount":I
    .restart local v9    # "startTime":J
    .restart local v11    # "startTimes":J
    :cond_9
    const-string v0, "%Y-%m"

    goto/16 :goto_2

    .line 363
    :cond_a
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v0, :cond_d

    .line 364
    invoke-static {}, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->getInstance()Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;

    move-result-object v2

    sget-object v0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v3, :cond_c

    const-string v0, "%Y-%m-%d"

    :goto_5
    invoke-virtual {v2, v0, v7, v8}, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v11

    .line 365
    const-wide/16 v2, 0x0

    cmp-long v0, v11, v2

    if-gtz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v11

    .line 366
    :cond_b
    const-wide/32 v2, 0x19bfcc00

    sub-long v9, v11, v2

    .line 367
    const/4 v5, 0x7

    goto/16 :goto_3

    .line 364
    :cond_c
    const-string v0, "%Y-%m"

    goto :goto_5

    .line 370
    :cond_d
    invoke-static {}, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->getInstance()Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;

    move-result-object v0

    const-string v2, "%Y-%m"

    invoke-virtual {v0, v2, v7, v8}, Lcom/sec/android/app/shealth/stress/utils/StressPluginUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v11

    .line 371
    const-wide/16 v2, 0x0

    cmp-long v0, v11, v2

    if-gtz v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLineSeries:Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v11

    .line 372
    :cond_e
    const-wide v2, 0x59cce4400L

    sub-long v9, v11, v2

    .line 373
    const/16 v5, 0xc

    goto/16 :goto_3
.end method

.method private showSummarayFragment()V
    .locals 5

    .prologue
    .line 232
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/stress/StressActivity;

    .line 233
    .local v0, "activity":Lcom/sec/android/app/shealth/stress/StressActivity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->switchFragmentToSummary()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    return-void

    .line 234
    .end local v0    # "activity":Lcom/sec/android/app/shealth/stress/StressActivity;
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of StressActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method protected createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .locals 2
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 411
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;-><init>()V

    .line 412
    .local v0, "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v1, :cond_0

    .line 413
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->setChartData(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 417
    :goto_0
    return-object v0

    .line 415
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->setMultiChartData(Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0
.end method

.method protected customizeActionBarDisable(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 1
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 199
    return-void
.end method

.method protected customizeActionBarShow(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 1
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 202
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 203
    return-void
.end method

.method protected customizeChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V
    .locals 4
    .param p1, "chartInteraction"    # Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 397
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    .line 400
    const/high16 v0, 0x40e00000    # 7.0f

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 401
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 402
    invoke-virtual {p1, v2, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalLimit(II)V

    .line 403
    invoke-virtual {p1, v2, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalStep(II)V

    .line 408
    :goto_0
    return-void

    .line 405
    :cond_0
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 406
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    goto :goto_0
.end method

.method protected customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 10
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const v9, 0x7f0203c2

    const/high16 v8, 0x41f80000    # 31.0f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 580
    const/high16 v4, 0x41800000    # 16.0f

    iget v5, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->density:F

    mul-float/2addr v4, v5

    invoke-virtual {p1, v6, v6, v6, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 582
    const/4 v4, 0x3

    new-array v2, v4, [Ljava/lang/String;

    .line 583
    .local v2, "labels":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090c3c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v7

    .line 584
    const/4 v4, 0x1

    const-string v5, ""

    aput-object v5, v2, v4

    .line 585
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090c3d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 586
    invoke-virtual {p1, v2, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisCustomLabel([Ljava/lang/String;I)V

    .line 588
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0203b2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 589
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 590
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 592
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v4, v5, :cond_0

    .line 593
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 594
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    iget v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->density:F

    mul-float/2addr v4, v8

    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 595
    const-string v4, "HH"

    invoke-virtual {p1, v4, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    move-object v4, v1

    .line 597
    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 598
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 608
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getTimeDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 609
    iget v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->density:F

    const/high16 v5, 0x42080000    # 34.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 611
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v3

    .line 612
    .local v3, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v4, 0x50

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 613
    const/high16 v4, 0x41400000    # 12.0f

    iget v5, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->density:F

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 615
    return-void

    .line 600
    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    .end local v3    # "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 601
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    iget v4, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->density:F

    mul-float/2addr v4, v8

    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    move-object v4, v1

    .line 603
    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 604
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 4
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 619
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->customizeHandlerItemWhenVisible(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 621
    const/4 v1, 0x0

    .line 622
    .local v1, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v2, v3, :cond_0

    .line 623
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203c3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 624
    const/high16 v2, 0x426c0000    # 59.0f

    iget v3, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 625
    const-string v2, "HH:mm"

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 626
    const-string v2, "HH"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 633
    :goto_0
    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 634
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 635
    return-void

    .line 628
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0203c2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 630
    const/high16 v2, 0x41f80000    # 31.0f

    iget v3, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    goto :goto_0
.end method

.method protected getContentUriList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 693
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Stress;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/stress/StressInformationAreaView;

    return-object v0
.end method

.method protected getLegendButtonBackgroundResourceId()I
    .locals 1

    .prologue
    .line 699
    const/4 v0, 0x0

    return v0
.end method

.method protected getLegendMarks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 705
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getNoDataIcon()I
    .locals 1

    .prologue
    .line 675
    const v0, 0x7f0205b7

    return v0
.end method

.method protected getTimeDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;
    .locals 3
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 313
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 314
    const-string v0, "dd"

    .line 318
    :goto_0
    return-object v0

    .line 315
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_1

    .line 316
    const-string v0, "MM"

    goto :goto_0

    .line 317
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    .line 318
    const-string v0, "HH"

    goto :goto_0

    .line 320
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal periodType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected getYAxisLabelTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 664
    const/4 v0, 0x0

    return-object v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 142
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initDateBar()V

    .line 143
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 144
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 145
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 146
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 6

    .prologue
    const-wide/16 v4, 0xc8

    .line 277
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 278
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030237

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLegendView:Landroid/view/View;

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v3, 0x7f0809e7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    .line 280
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC$2;-><init>(Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    invoke-static {v4, v5, v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->disableClickFor(JLandroid/view/View;)V

    .line 292
    :try_start_0
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC$3;-><init>(Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 303
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mLegendView:Landroid/view/View;

    return-object v2

    .line 299
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 1
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 241
    iput-object p3, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 242
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->savePeriodType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 243
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 270
    new-instance v0, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/stress/StressInformationAreaView;

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/stress/StressInformationAreaView;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/stress/StressInformationAreaView;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mInfoView:Lcom/sec/android/app/shealth/stress/StressInformationAreaView;

    return-object v0
.end method

.method protected initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 13
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    .prologue
    .line 490
    new-instance v3, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 491
    .local v3, "lineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0062

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 492
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 493
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x106000c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 494
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 495
    const-string v9, "font/Roboto-Light.ttf"

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 497
    new-instance v2, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 498
    .local v2, "lineLabelStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0063

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 499
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 500
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x106000c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 501
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 502
    const-string v9, "font/Roboto-Light.ttf"

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 504
    iget-object v9, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_1

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020627

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 507
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 509
    .local v4, "normalBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020628

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    move-object v9, v0

    .line 510
    check-cast v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 512
    .local v1, "handlerOverBitmap":Landroid/graphics/Bitmap;
    new-instance v8, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 514
    .local v8, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    const/4 v9, 0x1

    const/4 v10, 0x1

    const/high16 v11, -0x3fc00000    # -3.0f

    sget v12, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->MAX_Y:F

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 515
    invoke-virtual {v8, v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 516
    invoke-virtual {v8, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 517
    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 518
    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 519
    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 520
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v5, v9, Landroid/util/DisplayMetrics;->density:F

    .line 521
    .local v5, "scale":F
    const/high16 v9, 0x40a00000    # 5.0f

    mul-float/2addr v9, v5

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 523
    iget-object v9, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v9, v10, :cond_0

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f07008a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 527
    :cond_0
    const/high16 v9, 0x40a00000    # 5.0f

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 528
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f07008b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalOverColor(I)V

    .line 529
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setEnableDisconnectByTime(Z)V

    .line 531
    invoke-virtual {p1, v8}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 575
    .end local v5    # "scale":F
    .end local v8    # "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    :goto_0
    return-void

    .line 535
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "handlerOverBitmap":Landroid/graphics/Bitmap;
    .end local v4    # "normalBitmap":Landroid/graphics/Bitmap;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203c9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 536
    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 538
    .restart local v4    # "normalBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0203ca

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    move-object v9, v0

    .line 539
    check-cast v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 541
    .restart local v1    # "handlerOverBitmap":Landroid/graphics/Bitmap;
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;

    invoke-direct {v6}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;-><init>()V

    .line 542
    .local v6, "seriesCandleStyle":Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;
    new-instance v7, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 545
    .local v7, "seriesLineStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    const/4 v9, 0x1

    const/4 v10, 0x1

    const/high16 v11, -0x3fc00000    # -3.0f

    sget v12, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->MAX_Y:F

    invoke-virtual {v6, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 546
    invoke-virtual {v6, v3}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 547
    invoke-virtual {v6, v2}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 550
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setValueMarkingVisible(Z)V

    .line 554
    const/high16 v9, 0x41900000    # 18.0f

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setShadowWidth(F)V

    .line 555
    const/high16 v9, 0x40c00000    # 6.0f

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setShadowThickness(F)V

    .line 556
    const-string v9, "#8dd949"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setShadowColor(I)V

    .line 557
    const/4 v9, 0x0

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->setBodyWidth(F)V

    .line 560
    const/4 v9, 0x1

    const/4 v10, 0x1

    const/high16 v11, -0x3fc00000    # -3.0f

    sget v12, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->MAX_Y:F

    invoke-virtual {v7, v9, v10, v11, v12}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 561
    invoke-virtual {v7, v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 562
    invoke-virtual {v7, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 563
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 564
    invoke-virtual {v7, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 565
    invoke-virtual {v7, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 566
    const/high16 v9, 0x41700000    # 15.0f

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 568
    const-string v9, "#009082"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 569
    const/high16 v9, 0x40a00000    # 5.0f

    invoke-virtual {v7, v9}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 571
    invoke-virtual {p1, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 572
    invoke-virtual {p1, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 225
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->onConfiguarationLanguageChanged()V

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->isConfigurationChanged:Z

    .line 228
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 118
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 120
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->setHasOptionsMenu(Z)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDatabaseHelper:Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/common/StressDatabaseHelper;->getDataCount()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mDataCount:I

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->density:F

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v1, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->scaledDensity:F

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "STRESS_CHARTTAB"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 127
    .local v0, "mPeriod":I
    if-ne v0, v4, :cond_0

    .line 129
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 137
    :goto_0
    return-void

    .line 131
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 133
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0

    .line 135
    :cond_1
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 210
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDestroyView()V

    .line 211
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 150
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onResume()V

    .line 151
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->isConfigurationChanged:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->isDataCountChanged()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->initGeneralView()V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    const v3, 0x7f09005f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 154
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->isConfigurationChanged:Z

    .line 158
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/stress/StressActivity;

    .line 159
    .local v1, "stressActivity":Lcom/sec/android/app/shealth/stress/StressActivity;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->customizeActionBarDisable(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->switchToBodyfatSummary:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    .line 166
    :try_start_1
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC$1;-><init>(Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 177
    :cond_2
    :goto_0
    return-void

    .line 160
    .end local v1    # "stressActivity":Lcom/sec/android/app/shealth/stress/StressActivity;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of StressActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 173
    .end local v0    # "e":Ljava/lang/ClassCastException;
    .restart local v1    # "stressActivity":Lcom/sec/android/app/shealth/stress/StressActivity;
    :catch_1
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 5

    .prologue
    .line 183
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onStop()V

    .line 185
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/stress/StressActivity;

    .line 186
    .local v1, "stressActivity":Lcom/sec/android/app/shealth/stress/StressActivity;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->customizeActionBarShow(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    return-void

    .line 187
    .end local v1    # "stressActivity":Lcom/sec/android/app/shealth/stress/StressActivity;
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v2, Ljava/lang/ClassCastException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " must be instance of StressActivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
    .locals 4
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->setStartVisual(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 683
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    .line 684
    invoke-virtual {p1, v3, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 689
    :goto_0
    return-void

    .line 686
    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p1, v3, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 687
    invoke-virtual {p1, v2, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    goto :goto_0
.end method

.method protected setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 309
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 310
    return-void
.end method

.method protected updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 671
    return-void
.end method
