.class Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;
.super Ljava/lang/Object;
.source "CignaCoachActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

.field final synthetic val$dialogTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->val$dialogTag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 545
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->val$dialogTag:Ljava/lang/String;

    const-string v1, "COACH_PROMPT_RESTORE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 546
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_2

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$502(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    .line 549
    const-string v0, "COACH_PROMPT_RESTORE"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->val$dialogTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->isInitialBackupRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->startCignaRestore(Z)V

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->startCignaRestore(Z)V

    goto :goto_0

    .line 566
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestorePopUpHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$600(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dismissRestoreTriggerPopup()V

    goto :goto_0

    .line 571
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->val$dialogTag:Ljava/lang/String;

    const-string v1, "RESTORE_PROGRESS_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->val$dialogTag:Ljava/lang/String;

    const-string v1, "RESTORE_ERROR_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->val$dialogTag:Ljava/lang/String;

    const-string v1, "RESTORE_SUCCESS_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->mRestoreHelper:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;->access$500(Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity;)Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachActivity$2;->val$dialogTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;->onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V

    goto :goto_0
.end method
