.class Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring$1;
.super Landroid/os/CountDownTimer;
.source "UvStateMeasuring.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    .line 69
    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StressStateMeasuring mCurrentStateTimer onFinish() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;

    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->isInCurrentState:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->access$100(Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    return-void
.end method

.method public onTick(J)V
    .locals 3
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 61
    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StressStateMeasuring mCurrentStateTimer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;

    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->isInCurrentState:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->access$100(Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring$1;->this$0:Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;

    # getter for: Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->isInCurrentState:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->access$100(Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring$1;->cancel()V

    .line 65
    :cond_0
    return-void
.end method
