.class public Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "TipArticleDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$CignaWebViewClient;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private favorite_checked:Z

.field private mArticleWebViewLayout:Landroid/widget/LinearLayout;

.field private mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

.field mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mToast:Landroid/widget/Toast;

    .line 223
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->favorite_checked:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->favorite_checked:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;Z)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getActionBarFavoriteResourceID(Z)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->prepareShareView()V

    return-void
.end method

.method private getActionBarFavoriteResourceID(Z)I
    .locals 1
    .param p1, "isSelected"    # Z

    .prologue
    .line 181
    if-eqz p1, :cond_0

    const v0, 0x7f0207b6

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0207b7

    goto :goto_0
.end method


# virtual methods
.method public closeScreen()V
    .locals 1

    .prologue
    .line 219
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->setResult(I)V

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->finish()V

    .line 221
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->closeScreen()V

    .line 215
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 50
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v5, 0x7f030062

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->setContentView(I)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 54
    .local v3, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_0

    .line 55
    const-string v5, "extra_name_article_id"

    invoke-virtual {v3, v5, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 56
    .local v0, "ARTICLE_ID":I
    const-string v5, "extra_name_sub_category_id"

    invoke-virtual {v3, v5, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 57
    .local v1, "SUB_CATEGORY_ID":I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getArticle(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 58
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    if-eqz v5, :cond_0

    .line 59
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    const-string v6, "article_resource_image"

    invoke-virtual {v3, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setBackground(I)V

    .line 63
    .end local v0    # "ARTICLE_ID":I
    .end local v1    # "SUB_CATEGORY_ID":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    if-nez v5, :cond_1

    .line 64
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->TAG:Ljava/lang/String;

    const-string v6, "Article Data is null"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->finish()V

    .line 97
    :goto_0
    return-void

    .line 72
    :cond_1
    const v5, 0x7f0801dd

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 73
    .local v2, "background":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitleImg()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getGoalImageResourceID(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 75
    const v5, 0x7f0801de

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mArticleWebViewLayout:Landroid/widget/LinearLayout;

    .line 77
    new-instance v4, Landroid/webkit/WebView;

    invoke-direct {v4, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 78
    .local v4, "webView":Landroid/webkit/WebView;
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mArticleWebViewLayout:Landroid/widget/LinearLayout;

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v6, v9, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 81
    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 82
    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-virtual {v5, v6}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 84
    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 87
    invoke-virtual {v4, v8}, Landroid/webkit/WebView;->setDrawingCacheEnabled(Z)V

    .line 89
    invoke-virtual {v4, v8}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 90
    const v5, 0x7f0801db

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ScrollView;

    invoke-virtual {v5, v8}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 92
    new-instance v5, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$CignaWebViewClient;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$CignaWebViewClient;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)V

    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 93
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getDetail()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->setActionBarNormalMode()V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 188
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 196
    const/4 v0, 0x1

    return v0
.end method

.method public setActionBarNormalMode()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    .line 121
    .local v3, "actoiniBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-nez v3, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 125
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v5

    if-nez v5, :cond_0

    .line 126
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)V

    .line 158
    .local v1, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isFavorite()Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->favorite_checked:Z

    .line 159
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isFavorite()Z

    move-result v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getActionBarFavoriteResourceID(Z)I

    move-result v4

    .line 161
    .local v4, "iconResId":I
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-direct {v0, v4, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 162
    .local v0, "actionBarButtonFavoriteBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v5, 0x7f090c55

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 163
    new-array v5, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 165
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v5, 0x7f020058

    invoke-direct {v2, v5, v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 166
    .local v2, "actionButton2Builder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v5, 0x7f090033

    invoke-virtual {v2, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 167
    new-array v5, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method
