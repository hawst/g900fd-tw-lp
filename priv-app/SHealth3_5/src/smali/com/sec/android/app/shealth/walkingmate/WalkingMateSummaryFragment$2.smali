.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$2;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getContentView(Landroid/content/Context;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncButton:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 615
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncProgressImage:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 617
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncStarted:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 621
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->startSync(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Z)V

    .line 623
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 625
    .local v0, "mDevice":I
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSyncing()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x2719

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    .line 628
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setSyncing(Z)Z

    .line 629
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->getWearableSyncIntent(I)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
