.class public Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;
.super Landroid/view/View;
.source "HeartBeatAnimationView.java"


# instance fields
.field private mBeforeTime:J

.field private mIsAnimated:Z

.field private mLeft:F

.field private mMovePerSecSize:I

.field private mWaveBgImage:Landroid/graphics/Bitmap;

.field private mWaveBgMaskImage:Landroid/graphics/Bitmap;

.field private mWaveEffectImage:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 41
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mMovePerSecSize:I

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mBeforeTime:J

    .line 61
    const/4 v0, 0x1

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->animaionInit()V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mMovePerSecSize:I

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mBeforeTime:J

    .line 55
    const/4 v0, 0x1

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->animaionInit()V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mMovePerSecSize:I

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mBeforeTime:J

    .line 49
    const/4 v0, 0x1

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->animaionInit()V

    .line 51
    return-void
.end method

.method private getShiftPadding()F
    .locals 9

    .prologue
    .line 238
    const/4 v4, 0x0

    .line 239
    .local v4, "shiftSize":F
    iget-wide v5, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mBeforeTime:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_0

    .line 240
    const/4 v4, 0x0

    .line 241
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mBeforeTime:J

    .line 250
    :goto_0
    return v4

    .line 243
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 244
    .local v0, "currentTime":J
    iget-wide v5, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mBeforeTime:J

    sub-long v2, v0, v5

    .line 246
    .local v2, "latancy":J
    iget v5, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mMovePerSecSize:I

    int-to-float v5, v5

    long-to-float v6, v2

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v6, v7

    mul-float v4, v5, v6

    .line 247
    iput-wide v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mBeforeTime:J

    goto :goto_0
.end method


# virtual methods
.method public animaionInit()V
    .locals 5

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0204bf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 67
    .local v0, "bgDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setWaveBgImage(Landroid/graphics/Bitmap;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0204c1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 69
    .local v2, "maskDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setWaveBgMaskImage(Landroid/graphics/Bitmap;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0204c0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 71
    .local v1, "effectDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setWaveEffectImage(Landroid/graphics/Bitmap;)V

    .line 73
    const/16 v3, 0x12c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setMovePerSecSize(I)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->start()Z

    .line 75
    return-void
.end method

.method public getMovePerSecSize()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mMovePerSecSize:I

    return v0
.end method

.method public getWaveBgImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveBgImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getWaveBgMaskImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveBgMaskImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getWaveEffectImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveEffectImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public isIsAnimated()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 207
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 209
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    if-nez v1, :cond_0

    .line 230
    :goto_0
    return-void

    .line 213
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 215
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 216
    .local v0, "bgPaint":Landroid/graphics/Paint;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveBgImage:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v1, v3, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 219
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveEffectImage:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 224
    iget v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    invoke-direct {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->getShiftPadding()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    .line 225
    iget v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    iget-object v2, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveBgImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveEffectImage:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    .line 229
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->invalidate()V

    goto :goto_0
.end method

.method public setIsAnimated(Z)V
    .locals 2
    .param p1, "isAnimated"    # Z

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveBgImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveEffectImage:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    .line 176
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    if-eqz v0, :cond_0

    .line 177
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mBeforeTime:J

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveBgImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mLeft:F

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->postInvalidate()V

    goto :goto_0
.end method

.method public setMovePerSecSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mMovePerSecSize:I

    .line 152
    return-void
.end method

.method public setWaveBgImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveBgImage:Landroid/graphics/Bitmap;

    .line 95
    return-void
.end method

.method public setWaveBgMaskImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveBgMaskImage:Landroid/graphics/Bitmap;

    .line 114
    return-void
.end method

.method public setWaveEffectImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mWaveEffectImage:Landroid/graphics/Bitmap;

    .line 133
    return-void
.end method

.method public start()Z
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setIsAnimated(Z)V

    .line 190
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    return v0
.end method

.method public stop()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 199
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->setIsAnimated(Z)V

    .line 200
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/widget/HeartBeatAnimationView;->mIsAnimated:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
