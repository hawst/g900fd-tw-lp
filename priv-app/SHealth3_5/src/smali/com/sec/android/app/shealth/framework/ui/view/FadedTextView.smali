.class public Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;
.super Landroid/widget/TextView;


# instance fields
.field private mFadeWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->mFadeWidth:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->mFadeWidth:I

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v0

    int-to-float v3, v2

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->getCurrentTextColor()I

    move-result v4

    iget v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->mFadeWidth:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    int-to-float v3, v2

    div-float v7, v0, v3

    new-instance v0, Landroid/graphics/LinearGradient;

    int-to-float v3, v2

    new-array v5, v11, [I

    aput v4, v5, v8

    aput v4, v5, v9

    const/16 v2, 0x6e

    const/16 v4, 0xb7

    const/16 v6, 0x11

    invoke-static {v2, v4, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v5, v10

    new-array v6, v11, [F

    aput v1, v6, v8

    aput v7, v6, v9

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v6, v10

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/view/FadedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_0
.end method
