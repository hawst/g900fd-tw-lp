.class Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider$1;
.super Landroid/support/v4/util/LruCache;
.source "MaskedImageCollector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;I)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider$1;->this$0:Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider;

    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 175
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mask/MaskedImageCollector$MaskedBitmapsProvider$1;->sizeOf(Ljava/util/List;Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/util/List;Landroid/graphics/Bitmap;)I
    .locals 1
    .param p2, "value"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")I"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "key":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    return v0
.end method
