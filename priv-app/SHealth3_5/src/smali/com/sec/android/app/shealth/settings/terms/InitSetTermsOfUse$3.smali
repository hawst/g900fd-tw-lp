.class Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$3;
.super Ljava/lang/Object;
.source "InitSetTermsOfUse.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f080a11

    if-ne v2, v3, :cond_2

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_Terms:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$800(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/CheckBox;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_Terms:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$800(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 136
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 132
    goto :goto_0

    .line 133
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f080a14

    if-ne v2, v3, :cond_0

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_PP:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$900(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/CheckBox;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse$3;->this$0:Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;

    # getter for: Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->mAgreeCheck_PP:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;->access$900(Lcom/sec/android/app/shealth/settings/terms/InitSetTermsOfUse;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
