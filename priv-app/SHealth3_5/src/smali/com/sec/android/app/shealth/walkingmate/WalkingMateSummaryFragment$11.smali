.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setDistanceValueAndUnitText()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

.field final synthetic val$unit:Ljava/lang/String;

.field final synthetic val$value:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1615
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->val$value:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->val$unit:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1619
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->val$value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1620
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnitTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->val$unit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1621
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mDistanceUnitTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$4100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$11;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    const v2, 0x7f0901c3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1623
    return-void
.end method
