.class Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;
.super Ljava/lang/Object;
.source "SelectGoalMissionActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->addSelectSpinner()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 0

    .prologue
    .line 594
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "selectedItemView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 599
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-lez p3, :cond_0

    .line 602
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090071

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->setSelectAll()V

    .line 612
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->refreshFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1600(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    .line 614
    return-void

    .line 606
    :cond_1
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090073

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$10;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->setUnSelectAll()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 619
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
