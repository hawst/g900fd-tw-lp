.class public Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;
.super Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.source "SleepMonitorGraphFragmentSIC.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;
    }
.end annotation


# static fields
.field private static final HOUR_COUNT:I = 0xa

.field private static final HOUR_INTERVAL:I = 0x3c

.field private static MARKING_COUNT:I = 0x0

.field private static MARKING_INTERVAL:I = 0x0

.field private static final PATTERN_24_HOURS:Ljava/lang/String; = "HH"

.field private static final PATTERN_24_HOURS_HANDLER:Ljava/lang/String; = "HH:mm"

.field private static final PATTERN_DAY_FORMAT:Ljava/lang/String; = "dd"

.field private static final PATTERN_MONTH_FORMAT:Ljava/lang/String; = "MM"

.field private static PRE_PERIOD:Ljava/lang/String; = null

.field private static final QUERY_HOUR:Ljava/lang/String; = "SELECT rise_time as RISE_TIME, bed_time as BED_TIME, quality AS QUALITY FROM sleep WHERE sync_status != 170004 AND _id = "

.field private static final QUERY_INDIVIUAL_PART:Ljava/lang/String; = "SELECT _id as SLEEP_ID, rise_time as RISE_TIME, bed_time as BED_TIME, user_device__id as DEVICE_ID, efficiency as EFFICIENCY FROM sleep WHERE sync_status != 170004 ORDER BY bed_time ASC"

.field private static final QUERY_PART_1:Ljava/lang/String; = "SELECT count(*) as count, sum(a.rise_time-a.bed_time) AS RANGE, strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day,\t\t\t\t\t\t\t\t\t(a.bed_time-b.q) AS dayDataLong,"

.field private static final QUERY_PART_2:Ljava/lang/String; = "sum(abs(a.rise_time-a.bed_time) * (a.efficiency/100)) as motionless_time_old,  max(a.rise_time) as rise_time, min(a.bed_time) as bed_time, sum(abs(a.rise_time-a.bed_time))*ROUND((SUM(abs(a.rise_time-a.bed_time)*a.efficiency/100)*100)/sum(abs(a.rise_time-a.bed_time)),0)/100 AS motionless_time, AVG(a.efficiency) AS efficiency FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id GROUP BY day"

.field private static final TAG:Ljava/lang/String;

.field private static volatile mDayChartViewStartTime:J

.field private static volatile mHourChartViewStartTime:J

.field private static mIsResetNeeded:Z

.field private static volatile mMonthChartViewStartTime:J

.field private static volatile mSelectedTime:J


# instance fields
.field private INCREMENT:F

.field private RecreateGraph:I

.field private bitmap:Landroid/graphics/Bitmap;

.field private chartExplanatoryNotes:Landroid/widget/LinearLayout;

.field private chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

.field contentObserver1:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;

.field density:F

.field editor:Landroid/content/SharedPreferences$Editor;

.field public graphFragmentSIC:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

.field private handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field private handlerOverBitmap:Landroid/graphics/Bitmap;

.field private lineBitmap:Landroid/graphics/Bitmap;

.field private lineBitmapWithouthandler:Landroid/graphics/Bitmap;

.field private mContents:Landroid/app/Activity;

.field private mLabel:Ljava/lang/String;

.field private mLabels:[Ljava/lang/String;

.field private mLegendView:Landroid/view/View;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private mTimeFormat:Ljava/lang/String;

.field private mTimeFormatHandler:Ljava/lang/String;

.field private normalBitmap:Landroid/graphics/Bitmap;

.field private pointDataDisplayDayMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;",
            ">;"
        }
    .end annotation
.end field

.field private pointDataDisplayMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;",
            ">;"
        }
    .end annotation
.end field

.field private pointDataDisplayMonthMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;",
            ">;"
        }
    .end annotation
.end field

.field scaledDensity:F

.field sleepMonitorInformationArea:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;

.field private sleepUnit:Ljava/lang/String;

.field private switchToWalkingSummary:Landroid/widget/ImageButton;

.field private timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

.field private timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const-wide/16 v1, -0x1

    .line 80
    const-class v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->TAG:Ljava/lang/String;

    .line 91
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->MARKING_INTERVAL:I

    .line 92
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->MARKING_COUNT:I

    .line 118
    sput-wide v1, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J

    .line 119
    sput-wide v1, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J

    .line 120
    sput-wide v1, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J

    .line 121
    sput-wide v1, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSelectedTime:J

    .line 122
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mIsResetNeeded:Z

    .line 123
    const-string v0, "Previous Graph Fragment State"

    sput-object v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->PRE_PERIOD:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;

    .line 105
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayDayMap:Ljava/util/Map;

    .line 106
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMonthMap:Ljava/util/Map;

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->RecreateGraph:I

    .line 628
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->INCREMENT:F

    .line 974
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$3;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .line 129
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mReadyToShown:Z

    return p1
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mReadyToShown:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1200()J
    .locals 2

    .prologue
    .line 78
    sget-wide v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J

    return-wide v0
.end method

.method static synthetic access$1202(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 78
    sput-wide p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J

    return-wide p0
.end method

.method static synthetic access$1300()J
    .locals 2

    .prologue
    .line 78
    sget-wide v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J

    return-wide v0
.end method

.method static synthetic access$1302(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 78
    sput-wide p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J

    return-wide p0
.end method

.method static synthetic access$1400()J
    .locals 2

    .prologue
    .line 78
    sget-wide v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J

    return-wide v0
.end method

.method static synthetic access$1402(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 78
    sput-wide p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J

    return-wide p0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartExplanatoryNotes:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmapWithouthandler:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$702(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 78
    sput-wide p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSelectedTime:J

    return-wide p0
.end method

.method static synthetic access$802(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 78
    sput-boolean p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mIsResetNeeded:Z

    return p0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    return-object v0
.end method

.method private drawDayData(Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;JD)V
    .locals 25
    .param p1, "pointData"    # Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    .param p2, "timeStamp"    # J
    .param p4, "max"    # D

    .prologue
    .line 649
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepDuration()J

    move-result-wide v4

    .line 650
    .local v4, "totaltime":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepAvgEfficy()F

    move-result v8

    .line 651
    .local v8, "val":F
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepRiseTime()J

    move-result-wide v9

    .line 652
    .local v9, "maxRiseTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepStartTime()J

    move-result-wide v6

    .line 653
    .local v6, "minBedTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepMotionlessDuration()J

    move-result-wide v14

    const-wide/32 v16, 0xea60

    div-long v11, v14, v16

    .line 654
    .local v11, "dayMotionless":J
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->INCREMENT:F

    float-to-double v14, v3

    mul-double v14, v14, p4

    long-to-double v0, v4

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v14, v15, v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeToValueCalcD(DD)D

    move-result-wide v21

    .line 655
    .local v21, "totalTimeBarValue":D
    const/high16 v3, 0x42c80000    # 100.0f

    div-float v3, v8, v3

    float-to-double v14, v3

    mul-double v19, v21, v14

    .line 657
    .local v19, "addedTimeBarValue":D
    new-instance v18, Ljava/util/LinkedList;

    invoke-direct/range {v18 .. v18}, Ljava/util/LinkedList;-><init>()V

    .line 658
    .local v18, "point":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    sub-double v14, v21, v19

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 659
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;

    move-object/from16 v23, v0

    move-wide/from16 v0, p2

    long-to-double v14, v0

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v14, v15, v3, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v24

    new-instance v3, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const-wide/16 v15, 0x0

    const/16 v17, 0x0

    invoke-direct/range {v3 .. v17}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;-><init>(JJFJJZZJZ)V

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 661
    new-instance v13, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v13}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .local v13, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-wide/from16 v14, p2

    move-wide/from16 v16, v19

    .line 662
    invoke-virtual/range {v13 .. v18}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JDLjava/util/LinkedList;)V

    .line 663
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3, v13}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 664
    return-void
.end method

.method private drawMonthData(Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;JD)V
    .locals 24
    .param p1, "pointData"    # Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    .param p2, "timeStamp"    # J
    .param p4, "max"    # D

    .prologue
    .line 632
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepDuration()J

    move-result-wide v3

    .line 633
    .local v3, "totaltime":J
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepAvgEfficy()F

    move-result v7

    .line 634
    .local v7, "val":F
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepMotionlessDuration()J

    move-result-wide v5

    const-wide/32 v8, 0xea60

    div-long v10, v5, v8

    .line 635
    .local v10, "motionLess":J
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->INCREMENT:F

    float-to-double v5, v2

    mul-double v5, v5, p4

    long-to-double v8, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6, v8, v9}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeToValueCalcD(DD)D

    move-result-wide v20

    .line 636
    .local v20, "totalTimeBarValue":D
    const/high16 v2, 0x42c80000    # 100.0f

    div-float v2, v7, v2

    float-to-double v5, v2

    mul-double v18, v20, v5

    .line 638
    .local v18, "addedTimeBarValue":D
    new-instance v17, Ljava/util/LinkedList;

    invoke-direct/range {v17 .. v17}, Ljava/util/LinkedList;-><init>()V

    .line 639
    .local v17, "point":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    sub-double v5, v20, v18

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 640
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;

    move-object/from16 v22, v0

    move-wide/from16 v0, p2

    long-to-double v5, v0

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-static {v5, v6, v2, v8}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v23

    new-instance v2, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;

    const-wide/16 v5, 0x0

    const-wide/16 v8, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-wide/16 v14, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v2 .. v16}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;-><init>(JJFJJZZJZ)V

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    new-instance v12, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v12}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .local v12, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-wide/from16 v13, p2

    move-wide/from16 v15, v18

    .line 643
    invoke-virtual/range {v12 .. v17}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->add(JDLjava/util/LinkedList;)V

    .line 644
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v2, v12}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 645
    return-void
.end method

.method private getEndOfSleep(J)J
    .locals 19
    .param p1, "currentTime"    # J

    .prologue
    .line 793
    move-wide/from16 v11, p1

    .line 794
    .local v11, "previousTime":J
    const-wide/16 v9, -0x1

    .line 795
    .local v9, "previousSleepId":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "sync_status != 170004 AND sample_time >= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 796
    .local v5, "selectionClause":Ljava/lang/String;
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "sleep_status"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "sample_time"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "sleep__id"

    aput-object v3, v4, v2

    .line 797
    .local v4, "projection":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const-string/jumbo v7, "sample_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 798
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    .line 799
    :cond_0
    if-eqz v8, :cond_1

    .line 800
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    move-wide v13, v11

    .line 815
    .end local v11    # "previousTime":J
    .local v13, "previousTime":J
    :goto_0
    return-wide v13

    .line 803
    .end local v13    # "previousTime":J
    .restart local v11    # "previousTime":J
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 805
    :cond_3
    const-string/jumbo v2, "sample_time"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    .line 806
    .local v15, "sampleTime":J
    const-string/jumbo v2, "sleep__id"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 807
    .local v17, "sleepId":J
    const-wide/16 v2, -0x1

    cmp-long v2, v17, v2

    if-eqz v2, :cond_4

    const-wide/16 v2, -0x1

    cmp-long v2, v9, v2

    if-eqz v2, :cond_4

    cmp-long v2, v17, v9

    if-eqz v2, :cond_4

    .line 813
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-wide v13, v11

    .line 815
    .end local v11    # "previousTime":J
    .restart local v13    # "previousTime":J
    goto :goto_0

    .line 810
    .end local v13    # "previousTime":J
    .restart local v11    # "previousTime":J
    :cond_4
    move-wide v11, v15

    .line 811
    move-wide/from16 v9, v17

    .line 812
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_1
.end method

.method private getMaximumTimeLessThanTargetTime(J)J
    .locals 9
    .param p1, "targetTime"    # J

    .prologue
    const/16 v8, 0xc

    .line 773
    iget-object v7, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v6

    .line 774
    .local v6, "size":I
    const/4 v3, 0x0

    .line 775
    .local v3, "index":I
    const-wide/16 v4, -0x1

    .line 777
    .local v4, "maximumTime":J
    const/4 v3, 0x0

    :goto_0
    if-ge v3, v6, :cond_0

    .line 778
    iget-object v7, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v1

    .line 779
    .local v1, "currentTime":J
    cmp-long v7, p1, v1

    if-gez v7, :cond_2

    .line 784
    .end local v1    # "currentTime":J
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 785
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 786
    const/16 v7, 0xb

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    if-ne v7, v8, :cond_1

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    if-nez v7, :cond_1

    .line 787
    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getEndOfSleep(J)J

    move-result-wide v4

    .line 789
    :cond_1
    return-wide v4

    .line 782
    .end local v0    # "calendar":Ljava/util/Calendar;
    .restart local v1    # "currentTime":J
    :cond_2
    move-wide v4, v1

    .line 777
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private getStartTime(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/Long;
    .locals 10
    .param p1, "currentPeriodH"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/16 v9, 0xc

    const/4 v8, 0x1

    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    .line 825
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v3

    if-nez v3, :cond_0

    .line 826
    const-wide/high16 v3, -0x8000000000000000L

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 861
    :goto_0
    return-object v3

    .line 829
    :cond_0
    sget-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSelectedTime:J

    cmp-long v3, v3, v6

    if-eqz v3, :cond_1

    .line 831
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 832
    .local v0, "c":Ljava/util/Calendar;
    sget-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSelectedTime:J

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 834
    const/16 v3, 0xe

    invoke-virtual {v0, v3, v5}, Ljava/util/Calendar;->set(II)V

    .line 835
    const/16 v3, 0xd

    invoke-virtual {v0, v3, v5}, Ljava/util/Calendar;->set(II)V

    .line 836
    invoke-virtual {v0, v9, v5}, Ljava/util/Calendar;->set(II)V

    .line 837
    const/16 v3, 0xb

    invoke-virtual {v0, v3, v9}, Ljava/util/Calendar;->set(II)V

    .line 838
    const/4 v3, 0x5

    invoke-virtual {v0, v3, v8}, Ljava/util/Calendar;->add(II)V

    .line 839
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    sput-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J

    sput-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J

    sput-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J

    .line 842
    .end local v0    # "c":Ljava/util/Calendar;
    :cond_1
    sget-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J

    cmp-long v3, v3, v6

    if-eqz v3, :cond_2

    sget-boolean v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mIsResetNeeded:Z

    if-ne v3, v8, :cond_3

    .line 844
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 847
    :cond_3
    const-wide/16 v1, -0x1

    .line 849
    .local v1, "startTime":J
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v3, :cond_5

    .line 850
    sget-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mHourChartViewStartTime:J

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getMaximumTimeLessThanTargetTime(J)J

    move-result-wide v1

    .line 857
    :goto_1
    cmp-long v3, v1, v6

    if-nez v3, :cond_4

    .line 858
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v1

    .line 861
    :cond_4
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 851
    :cond_5
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v3, :cond_6

    .line 852
    sget-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mDayChartViewStartTime:J

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getMaximumTimeLessThanTargetTime(J)J

    move-result-wide v1

    goto :goto_1

    .line 854
    :cond_6
    sget-wide v3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mMonthChartViewStartTime:J

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getMaximumTimeLessThanTargetTime(J)J

    move-result-wide v1

    goto :goto_1
.end method

.method private initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 19
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 1296
    new-instance v15, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v15}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1297
    .local v15, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v3, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v3, v4

    invoke-virtual {v15, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1298
    const/4 v3, 0x1

    invoke-virtual {v15, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1299
    const/16 v3, 0xff

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v15, v3, v4, v5, v6}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1300
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1301
    const-string/jumbo v3, "sans-serif-light"

    invoke-virtual {v15, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 1303
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v13

    .line 1304
    .local v13, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v3, 0x50

    invoke-virtual {v13, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 1305
    const/high16 v3, 0x41400000    # 12.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    mul-float/2addr v3, v4

    invoke-virtual {v13, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1307
    new-instance v17, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1308
    .local v17, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v3, 0x41700000    # 15.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v3, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1309
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1310
    const/16 v3, 0xff

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1311
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1312
    const-string/jumbo v3, "sans-serif-light"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 1314
    new-instance v16, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1315
    .local v16, "yLabelTitleStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v3, 0x41400000    # 12.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v3, v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1316
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1317
    const/16 v3, 0xff

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1318
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1319
    const-string/jumbo v3, "sans-serif-light"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 1321
    new-instance v10, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v10}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1322
    .local v10, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v3, 0x41900000    # 18.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v3, v4

    invoke-virtual {v10, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1323
    const/4 v3, 0x1

    invoke-virtual {v10, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1324
    const/16 v3, 0xff

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v10, v3, v4, v5, v6}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1325
    const/4 v3, 0x1

    invoke-virtual {v10, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1327
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x41800000    # 16.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    move/from16 v18, v0

    mul-float v6, v6, v18

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 1330
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 1331
    .local v8, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v14

    .line 1332
    .local v14, "width":I
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    .line 1333
    .local v11, "height":I
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v14, v11, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->bitmap:Landroid/graphics/Bitmap;

    .line 1334
    new-instance v2, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1335
    .local v2, "canvas":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 1336
    .local v7, "paint":Landroid/graphics/Paint;
    new-instance v12, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v12, v3, v4, v14, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1337
    .local v12, "rt":Landroid/graphics/Rect;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07008e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1338
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1339
    invoke-virtual {v2, v12, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1340
    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1341
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070088

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1342
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1343
    const/4 v3, 0x0

    const/4 v4, 0x0

    int-to-float v5, v14

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1344
    const/4 v3, 0x0

    int-to-float v4, v11

    int-to-float v5, v14

    int-to-float v6, v11

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1346
    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1347
    new-instance v3, Landroid/graphics/DashPathEffect;

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1348
    const/4 v3, 0x0

    int-to-float v4, v11

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    int-to-float v5, v14

    int-to-float v6, v11

    const/high16 v18, 0x40000000    # 2.0f

    div-float v6, v6, v18

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1349
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->bitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 1350
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700d8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setChartBackgroundColor(I)V

    .line 1353
    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    mul-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 1354
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 1355
    const v3, -0xcbb1ec

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 1356
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1357
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    const/high16 v4, 0x41000000    # 8.0f

    mul-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 1358
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mTimeFormat:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 1361
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 1362
    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 1363
    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1364
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitle(Ljava/lang/String;I)V

    .line 1365
    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 1366
    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1367
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    const/high16 v4, 0x41000000    # 8.0f

    mul-float/2addr v3, v4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextSpace(FI)V

    .line 1368
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 1369
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLabels:[Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisCustomLabel([Ljava/lang/String;I)V

    .line 1372
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1375
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p2

    if-ne v0, v3, :cond_0

    .line 1376
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 1379
    :goto_0
    check-cast v8, Landroid/graphics/drawable/BitmapDrawable;

    .end local v8    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v8}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->bitmap:Landroid/graphics/Bitmap;

    .line 1380
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->bitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    invoke-static {v3, v0, v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->swellHandler(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Landroid/graphics/Bitmap;)V

    .line 1381
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->bitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 1382
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 1383
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1384
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->handlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 1385
    const/high16 v3, 0x41f80000    # 31.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 1389
    const-wide/16 v3, 0x1388

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTimeOutDelay(J)V

    .line 1390
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mTimeFormatHandler:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 1393
    const/16 v3, 0x12c

    const/16 v4, 0x12c

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmap:Landroid/graphics/Bitmap;

    .line 1394
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 1395
    .local v9, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Landroid/graphics/Canvas;

    .end local v2    # "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1396
    .restart local v2    # "canvas":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v6

    invoke-virtual {v9, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1397
    invoke-virtual {v9, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1398
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 1400
    const/16 v3, 0x12c

    const/16 v4, 0x12c

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmapWithouthandler:Landroid/graphics/Bitmap;

    .line 1401
    new-instance v2, Landroid/graphics/Canvas;

    .end local v2    # "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->lineBitmapWithouthandler:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1402
    .restart local v2    # "canvas":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x3

    invoke-virtual {v9, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1403
    invoke-virtual {v9, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1408
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupEnable(Z)V

    .line 1409
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getSeparatorDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorDateFormat(Ljava/lang/String;)V

    .line 1410
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 1411
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 1412
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    const/high16 v4, 0x42080000    # 34.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 1413
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 1414
    return-void

    .line 1378
    .end local v9    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v8    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0203c2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    goto/16 :goto_0

    .line 1347
    :array_0
    .array-data 4
        0x41000000    # 8.0f
        0x41000000    # 8.0f
    .end array-data
.end method

.method private initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 12
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 1194
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0203c0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1195
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->normalBitmap:Landroid/graphics/Bitmap;

    .line 1196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v2, v8, Landroid/util/DisplayMetrics;->density:F

    .line 1203
    .local v2, "scale":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0203c1

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1204
    .restart local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->handlerOverBitmap:Landroid/graphics/Bitmap;

    .line 1206
    new-instance v1, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1207
    .local v1, "goalLineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v8, 0x41900000    # 18.0f

    iget v9, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->scaledDensity:F

    mul-float/2addr v8, v9

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1208
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1209
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f07021c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 1210
    const/4 v8, 0x2

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1211
    const-string/jumbo v8, "sans-serif-light"

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 1214
    new-instance v3, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;

    invoke-direct {v3}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;-><init>()V

    .line 1215
    .local v3, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09017e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 1216
    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/high16 v11, 0x42c80000    # 100.0f

    invoke-virtual {v3, v8, v9, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 1217
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineVisible(Z)V

    .line 1218
    const v8, 0x463b8000    # 12000.0f

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineValue(F)V

    .line 1219
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f07021c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineColor(I)V

    .line 1220
    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1221
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1222
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->normalBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1223
    const v8, 0xffffff

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setValueMarkingNormalColor(I)V

    .line 1224
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->handlerOverBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 1225
    const/high16 v8, 0x41900000    # 18.0f

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setValueMarkingSize(F)V

    .line 1226
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mContents:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070225

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 1227
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mContents:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070226

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v3, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 1230
    new-instance v5, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;

    invoke-direct {v5}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;-><init>()V

    .line 1231
    .local v5, "seriesStyleBG":Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09017e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 1232
    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/high16 v11, 0x42c80000    # 100.0f

    invoke-virtual {v5, v8, v9, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 1233
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setGoalLineVisible(Z)V

    .line 1234
    const v8, 0x453b8000    # 3000.0f

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setGoalLineValue(F)V

    .line 1235
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f07021c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setGoalLineColor(I)V

    .line 1236
    invoke-virtual {v5, v1}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1237
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1238
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->normalBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1239
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->handlerOverBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 1240
    const v8, 0x55e0f2d3

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setBarColor(I)V

    .line 1241
    const-wide/32 v8, 0x124f80

    invoke-virtual {v5, v8, v9}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setBarWidth(J)V

    .line 1242
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->setBarAlign(I)V

    .line 1245
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;

    invoke-direct {v6}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;-><init>()V

    .line 1246
    .local v6, "seriesStyleCurved":Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;
    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/high16 v11, 0x42cc0000    # 102.0f

    invoke-virtual {v6, v8, v9, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 1247
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setGoalLineVisible(Z)V

    .line 1248
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1249
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->normalBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1250
    const/high16 v8, 0x40a00000    # 5.0f

    mul-float/2addr v8, v2

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingSize(F)V

    .line 1251
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->handlerOverBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 1252
    const v8, -0xa15ace

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineColor(I)V

    .line 1253
    const/high16 v8, 0x40a00000    # 5.0f

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setLineThickness(F)V

    .line 1254
    const/4 v8, 0x3

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;->setValueMarkingVisibleOption(I)V

    .line 1257
    new-instance v7, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 1258
    .local v7, "seriesStyleHourLine":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09017e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 1259
    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/high16 v11, 0x42cc0000    # 102.0f

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 1260
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineVisible(Z)V

    .line 1261
    const v8, 0x453b8000    # 3000.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineValue(F)V

    .line 1262
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f07021c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineColor(I)V

    .line 1263
    invoke-virtual {v7, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1264
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1265
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->normalBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1266
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->handlerOverBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 1267
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mContents:Landroid/app/Activity;

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070219

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 1268
    const/high16 v8, 0x40a00000    # 5.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 1269
    const/high16 v8, 0x40a00000    # 5.0f

    mul-float/2addr v8, v2

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingSize(F)V

    .line 1272
    new-instance v4, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;

    invoke-direct {v4}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;-><init>()V

    .line 1273
    .local v4, "seriesStyleAC":Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;
    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/high16 v11, 0x42cc0000    # 102.0f

    invoke-virtual {v4, v8, v9, v10, v11}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 1274
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09017e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 1275
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setGoalLineVisible(Z)V

    .line 1276
    const/4 v8, 0x1

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1277
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->normalBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1278
    const/high16 v8, 0x40a00000    # 5.0f

    mul-float/2addr v8, v2

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingSize(F)V

    .line 1279
    iget-object v8, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->handlerOverBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 1280
    const v8, -0xa15ace

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setLineColor(I)V

    .line 1281
    const/high16 v8, 0x40a00000    # 5.0f

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setLineThickness(F)V

    .line 1282
    const/4 v8, 0x3

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setValueMarkingVisibleOption(I)V

    .line 1283
    const v8, 0x55e0f2d3

    invoke-virtual {v4, v8}, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->setAreaColor(I)V

    .line 1285
    sget-object v8, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p2, v8, :cond_0

    .line 1289
    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 1293
    :goto_0
    return-void

    .line 1291
    :cond_0
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    goto :goto_0
.end method

.method private processPointDataForGraphWithMax(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J
    .locals 32
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 680
    const-wide/16 v29, 0x0

    .line 681
    .local v29, "max":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayDayMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 683
    const-string v5, "SELECT count(*) as count, sum(a.rise_time-a.bed_time) AS RANGE, strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day,\t\t\t\t\t\t\t\t\t(a.bed_time-b.q) AS dayDataLong,sum(abs(a.rise_time-a.bed_time) * (a.efficiency/100)) as motionless_time_old,  max(a.rise_time) as rise_time, min(a.bed_time) as bed_time, sum(abs(a.rise_time-a.bed_time))*ROUND((SUM(abs(a.rise_time-a.bed_time)*a.efficiency/100)*100)/sum(abs(a.rise_time-a.bed_time)),0)/100 AS motionless_time, AVG(a.efficiency) AS efficiency FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id GROUP BY day"

    .line 684
    .local v5, "query":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 686
    .local v27, "cursor":Landroid/database/Cursor;
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 688
    :cond_0
    const-string v2, "bed_time"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 689
    .local v10, "bedTime":J
    const-string/jumbo v2, "rise_time"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 690
    .local v12, "riseTime":J
    const-string v2, "RANGE"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    .line 691
    .local v8, "sleepRange":J
    const-string v2, "efficiency"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v7

    .line 692
    .local v7, "efficiency":F
    const-string v2, "day"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 693
    .local v28, "dayData":Ljava/lang/String;
    const-string v2, "dayDataLong"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    .line 695
    .local v16, "dayDataLong":J
    const-string v2, "count"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 696
    long-to-float v2, v8

    mul-float/2addr v2, v7

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v31, v2, v3

    .line 701
    .local v31, "motionlessSleepRange":F
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayDayMap:Ljava/util/Map;

    move-object/from16 v0, v28

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;

    .line 702
    .local v6, "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    if-nez v6, :cond_4

    .line 703
    new-instance v6, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;

    .end local v6    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    invoke-static/range {v31 .. v31}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-long v14, v2

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;-><init>(FJJJJJ)V

    .line 704
    .restart local v6    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayDayMap:Ljava/util/Map;

    move-object/from16 v0, v28

    invoke-interface {v2, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 708
    :goto_1
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepDuration()J

    move-result-wide v2

    cmp-long v2, v29, v2

    if-gez v2, :cond_1

    .line 709
    invoke-virtual {v6}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepDuration()J

    move-result-wide v29

    .line 710
    :cond_1
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 712
    .end local v6    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    .end local v7    # "efficiency":F
    .end local v8    # "sleepRange":J
    .end local v10    # "bedTime":J
    .end local v12    # "riseTime":J
    .end local v16    # "dayDataLong":J
    .end local v28    # "dayData":Ljava/lang/String;
    .end local v31    # "motionlessSleepRange":F
    :cond_2
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 714
    return-wide v29

    .line 698
    .restart local v7    # "efficiency":F
    .restart local v8    # "sleepRange":J
    .restart local v10    # "bedTime":J
    .restart local v12    # "riseTime":J
    .restart local v16    # "dayDataLong":J
    .restart local v28    # "dayData":Ljava/lang/String;
    :cond_3
    const-string/jumbo v2, "motionless_time"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v31

    .line 699
    .restart local v31    # "motionlessSleepRange":F
    long-to-float v2, v8

    div-float v2, v31, v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v2, v2

    int-to-float v7, v2

    goto :goto_0

    .line 706
    .restart local v6    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    :cond_4
    invoke-static/range {v31 .. v31}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v25, v0

    move-object/from16 v18, v6

    move/from16 v19, v7

    move-wide/from16 v20, v12

    move-wide/from16 v22, v8

    move-object/from16 v24, p1

    invoke-virtual/range {v18 .. v26}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->addDataDisplayDay(FJJLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;J)V

    goto :goto_1
.end method

.method private processPointDataForGraphWithMaxMonth()J
    .locals 35

    .prologue
    .line 718
    const-wide/16 v29, 0x0

    .line 719
    .local v29, "max":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMonthMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 721
    const-string v5, "SELECT count(*) as count, sum(a.rise_time-a.bed_time) AS RANGE, strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day,\t\t\t\t\t\t\t\t\t(a.bed_time-b.q) AS dayDataLong,strftime(\'%Y-%m\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS month, sum(abs(a.rise_time-a.bed_time) * (a.efficiency/100)) as motionless_time_old,  max(a.rise_time) as rise_time, min(a.bed_time) as bed_time, sum(abs(a.rise_time-a.bed_time))*ROUND((SUM(abs(a.rise_time-a.bed_time)*a.efficiency/100)*100)/sum(abs(a.rise_time-a.bed_time)),0)/100 AS motionless_time, AVG(a.efficiency) AS efficiency FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id GROUP BY day"

    .line 722
    .local v5, "query":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 724
    .local v27, "cursor":Landroid/database/Cursor;
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 726
    :cond_0
    const-string/jumbo v2, "rise_time"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 727
    .local v10, "bedTime":J
    const-string/jumbo v2, "month"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 728
    .local v31, "month":Ljava/lang/String;
    const-string v2, "efficiency"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v7

    .line 729
    .local v7, "efficiency":F
    const-string v2, "RANGE"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    .line 730
    .local v8, "sleepRange":J
    const-string v2, "dayDataLong"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v16

    .line 732
    .local v16, "dayDataLong":J
    const-string v2, "count"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 733
    long-to-float v2, v8

    mul-float/2addr v2, v7

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v32, v2, v3

    .line 738
    .local v32, "motionlessSleepRange":F
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMonthMap:Ljava/util/Map;

    move-object/from16 v0, v31

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;

    .line 739
    .local v18, "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    if-nez v18, :cond_4

    .line 740
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMonthMap:Ljava/util/Map;

    new-instance v6, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;

    const-wide/16 v12, 0x0

    invoke-static/range {v32 .. v32}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-long v14, v3

    invoke-direct/range {v6 .. v17}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;-><init>(FJJJJJ)V

    move-object/from16 v0, v31

    invoke-interface {v2, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 744
    :goto_1
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 747
    .end local v7    # "efficiency":F
    .end local v8    # "sleepRange":J
    .end local v10    # "bedTime":J
    .end local v16    # "dayDataLong":J
    .end local v18    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    .end local v31    # "month":Ljava/lang/String;
    .end local v32    # "motionlessSleepRange":F
    :cond_1
    :try_start_0
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 751
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMonthMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v34

    .line 752
    .local v34, "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;>;"
    invoke-interface/range {v34 .. v34}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .local v28, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;

    .line 753
    .local v33, "value":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    invoke-virtual/range {v33 .. v33}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepDuration()J

    move-result-wide v2

    cmp-long v2, v29, v2

    if-gez v2, :cond_2

    .line 754
    invoke-virtual/range {v33 .. v33}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDaySleepDuration()J

    move-result-wide v29

    goto :goto_3

    .line 735
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v33    # "value":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    .end local v34    # "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;>;"
    .restart local v7    # "efficiency":F
    .restart local v8    # "sleepRange":J
    .restart local v10    # "bedTime":J
    .restart local v16    # "dayDataLong":J
    .restart local v31    # "month":Ljava/lang/String;
    :cond_3
    const-string/jumbo v2, "motionless_time"

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v27

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v32

    .line 736
    .restart local v32    # "motionlessSleepRange":F
    long-to-float v2, v8

    div-float v2, v32, v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v2, v2

    int-to-float v7, v2

    goto :goto_0

    .line 742
    .restart local v18    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    :cond_4
    sget-object v24, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static/range {v32 .. v32}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v25, v0

    move/from16 v19, v7

    move-wide/from16 v20, v8

    move-wide/from16 v22, v8

    invoke-virtual/range {v18 .. v26}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->addDataDisplayDay(FJJLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;J)V

    goto :goto_1

    .line 748
    .end local v7    # "efficiency":F
    .end local v8    # "sleepRange":J
    .end local v10    # "bedTime":J
    .end local v16    # "dayDataLong":J
    .end local v18    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    .end local v31    # "month":Ljava/lang/String;
    .end local v32    # "motionlessSleepRange":F
    :catch_0
    move-exception v2

    goto :goto_2

    .line 758
    .restart local v28    # "i$":Ljava/util/Iterator;
    .restart local v34    # "values":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;>;"
    :cond_5
    return-wide v29
.end method

.method private refreshChartView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 15
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 866
    new-instance v1, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .line 868
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .line 869
    .local v7, "activity":Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;
    if-eqz v7, :cond_0

    .line 870
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->addShareViaButton()V

    .line 873
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-object/from16 v0, p3

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 875
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-object/from16 v0, p3

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 877
    new-instance v10, Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    invoke-direct {v10}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    .line 878
    .local v10, "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v10, v1}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 883
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    if-eqz v1, :cond_1

    .line 884
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v12

    .line 885
    .local v12, "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 886
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v1, v12}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 887
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .line 890
    .end local v12    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    if-eqz v1, :cond_2

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-result-object v12

    .line 892
    .restart local v12    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 893
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    invoke-virtual {v1, v12}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 894
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    .line 897
    .end local v12    # "tempStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    :cond_2
    new-instance v1, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {v1, v2, v3, v10}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .line 898
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    new-instance v2, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$1;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setReadyToShowListener(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;)V

    .line 906
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p3

    if-ne v0, v1, :cond_3

    .line 908
    new-instance v1, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartStyle:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {v1, v2, v3, v10}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    .line 909
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    new-instance v2, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$2;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setReadyToShowListener(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;)V

    .line 918
    :cond_3
    new-instance v11, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct {v11}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 919
    .local v11, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 920
    const/4 v1, 0x1

    invoke-virtual {v11, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 921
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 922
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p3

    if-ne v0, v1, :cond_4

    .line 923
    const/16 v1, 0x3c

    const/16 v2, 0x3c

    invoke-virtual {v11, v1, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomIntervalStep(II)V

    .line 926
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 928
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p3

    if-ne v0, v1, :cond_5

    .line 929
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 931
    :cond_5
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 932
    .local v8, "c":Ljava/util/Calendar;
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p3

    if-ne v0, v1, :cond_8

    .line 933
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v1

    if-eqz v1, :cond_6

    .line 934
    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getStartTime(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v8, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 936
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setHandlerStartDate(D)V

    .line 937
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 938
    .local v9, "calendar":Ljava/util/Calendar;
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    const-wide/32 v3, 0x1b77400

    sub-long/2addr v1, v3

    invoke-virtual {v9, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 939
    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v9, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 941
    const/16 v1, 0xc

    invoke-virtual {v9, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0x1e

    if-le v1, v2, :cond_7

    .line 942
    const/16 v1, 0xb

    const/4 v2, 0x1

    invoke-virtual {v9, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 944
    :cond_7
    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v9, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 945
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    long-to-double v3, v3

    const/16 v5, 0x3c

    const/16 v6, 0xa

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setStartVisual(IDII)V

    .line 946
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setScrollRangeType(I)V

    .line 947
    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v3

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v5

    const-wide/32 v13, 0x36ee80

    add-long/2addr v5, v13

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;->setUserScrollRange(JJ)V

    .line 972
    .end local v9    # "calendar":Ljava/util/Calendar;
    :goto_0
    return-void

    .line 952
    :cond_8
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p3

    if-ne v0, v1, :cond_a

    .line 953
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v1

    if-eqz v1, :cond_9

    .line 954
    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getStartTime(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v8, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 956
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 957
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v2, 0x2

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const-wide/32 v5, 0x19bfcc00

    sub-long/2addr v3, v5

    long-to-double v3, v3

    sget v5, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->MARKING_INTERVAL:I

    const/4 v6, 0x7

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 960
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    goto :goto_0

    .line 963
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v1

    if-eqz v1, :cond_b

    .line 964
    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getStartTime(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v8, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 966
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    long-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 967
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v2, 0x5

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const-wide v5, 0x59cce4400L

    sub-long/2addr v3, v5

    long-to-double v3, v3

    sget v5, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->MARKING_INTERVAL:I

    sget v6, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->MARKING_COUNT:I

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 970
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    goto :goto_0
.end method

.method private setTimeData(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 1476
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_1

    .line 1477
    const-string v0, "dd"

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mTimeFormat:Ljava/lang/String;

    .line 1478
    const-string v0, "dd"

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mTimeFormatHandler:Ljava/lang/String;

    .line 1486
    :cond_0
    :goto_0
    return-void

    .line 1479
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_2

    .line 1480
    const-string v0, "MM"

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mTimeFormat:Ljava/lang/String;

    .line 1481
    const-string v0, "MM"

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mTimeFormatHandler:Ljava/lang/String;

    goto :goto_0

    .line 1482
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v0, :cond_0

    .line 1483
    const-string v0, "HH"

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mTimeFormat:Ljava/lang/String;

    .line 1484
    const-string v0, "HH:mm"

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mTimeFormatHandler:Ljava/lang/String;

    goto :goto_0
.end method

.method private setUpDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 66
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 267
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 270
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_2

    .line 273
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->processPointDataForGraphWithMaxMonth()J

    move-result-wide v42

    .line 275
    .local v42, "max":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v42

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->updateLabels(J)J

    move-result-wide v42

    .line 278
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMonthMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v39

    .line 283
    .local v39, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v39 .. v39}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v36

    .local v36, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v53

    check-cast v53, Ljava/lang/String;

    .line 285
    .local v53, "string":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMonthMap:Ljava/util/Map;

    move-object/from16 v0, v53

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;

    .line 288
    .local v5, "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    if-eqz v5, :cond_0

    .line 290
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDayData()J

    move-result-wide v6

    move-wide/from16 v0, v42

    long-to-double v8, v0

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->drawMonthData(Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;JD)V

    goto :goto_0

    .line 297
    .end local v5    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    .end local v53    # "string":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMonthMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 610
    .end local v36    # "i$":Ljava/util/Iterator;
    .end local v39    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v42    # "max":J
    :goto_1
    return-void

    .line 301
    :cond_2
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p1

    if-ne v0, v4, :cond_1a

    .line 302
    const/16 v20, 0x0

    .line 303
    .local v20, "isFirst":Z
    const/16 v21, 0x0

    .line 305
    .local v21, "isLast":Z
    const-wide/16 v46, 0x0

    .local v46, "sBedtime":J
    const-wide/16 v54, 0x0

    .line 306
    .local v54, "tBedtime":J
    const-wide/16 v51, 0x0

    .local v51, "sRisetime":J
    const-wide/16 v56, 0x0

    .line 307
    .local v56, "tRisetime":J
    const-wide/16 v60, 0x0

    .line 308
    .local v60, "temp_id":J
    const-wide/16 v44, 0x0

    .line 309
    .local v44, "prevPlottedTime":J
    const-wide/16 v28, 0x0

    .line 311
    .local v28, "PlotPoints":J
    const/16 v27, 0x0

    .line 312
    .local v27, "OverlapFlag":I
    const/16 v37, 0x0

    .line 314
    .local v37, "isLastFlag":I
    const-wide/16 v49, 0x0

    .line 315
    .local v49, "sEfficiency":D
    const-wide/16 v31, 0x0

    .line 316
    .local v31, "aggregate":J
    const-wide/32 v25, 0xea60

    .line 317
    .local v25, "Mins_to_Millis":J
    const/16 v24, 0x0

    .line 319
    .local v24, "falseOverlap":Z
    const/16 v30, 0x0

    .line 320
    .local v30, "accessory":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const-string v9, "SELECT _id as SLEEP_ID, rise_time as RISE_TIME, bed_time as BED_TIME, user_device__id as DEVICE_ID, efficiency as EFFICIENCY FROM sleep WHERE sync_status != 170004 ORDER BY bed_time ASC"

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v48

    .line 322
    .local v48, "sCursor":Landroid/database/Cursor;
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 326
    :cond_3
    const-string v4, "SLEEP_ID"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 327
    .local v22, "sleep_id":J
    const-string v4, "BED_TIME"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v46

    .line 328
    const-string v4, "RISE_TIME"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v51

    .line 329
    const-string v4, "EFFICIENCY"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v49

    .line 330
    const-string v4, "DEVICE_ID"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v35

    .line 331
    .local v35, "device":Ljava/lang/String;
    const-wide/16 v31, 0x0

    .line 332
    if-eqz v35, :cond_4

    .line 333
    const-string v4, "_"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 337
    :cond_4
    const/16 v21, 0x0

    .line 338
    invoke-static/range {v51 .. v52}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getAccurateSampleTime(J)J

    move-result-wide v6

    invoke-static/range {v46 .. v47}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getAccurateSampleTime(J)J

    move-result-wide v10

    sub-long/2addr v6, v10

    const-wide/16 v10, 0x14

    mul-long v10, v10, v25

    div-long/2addr v6, v10

    const-wide/16 v10, 0x1

    add-long v28, v6, v10

    .line 340
    const/4 v4, 0x1

    move/from16 v0, v27

    if-ne v0, v4, :cond_10

    .line 343
    const/16 v20, 0x0

    .line 356
    :goto_2
    invoke-static/range {v46 .. v47}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getAccurateSampleTime(J)J

    move-result-wide v62

    .line 358
    .local v62, "time":J
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->isLast()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 359
    const-wide/16 v60, 0x0

    .line 368
    :goto_3
    const/16 v37, 0x0

    .line 371
    const/4 v8, 0x0

    .line 373
    .local v8, "projection":[Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "sync_status != 170004 AND sleep__id="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v22

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " AND sample_time>="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v46

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 374
    .local v9, "selectionClause":Ljava/lang/String;
    const/4 v4, 0x4

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "projection":[Ljava/lang/String;
    const/4 v4, 0x0

    const-string v6, "_id"

    aput-object v6, v8, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "sleep_status"

    aput-object v6, v8, v4

    const/4 v4, 0x2

    const-string/jumbo v6, "sample_time"

    aput-object v6, v8, v4

    const/4 v4, 0x3

    const-string/jumbo v6, "sleep__id"

    aput-object v6, v8, v4

    .line 375
    .restart local v8    # "projection":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    const-string/jumbo v11, "sample_time ASC"

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v33

    .line 376
    .local v33, "cursor":Landroid/database/Cursor;
    const-wide/16 v64, 0x0

    .line 380
    .local v64, "val":D
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 384
    :cond_5
    :goto_4
    const-wide/16 v6, 0x0

    cmp-long v4, v28, v6

    if-lez v4, :cond_e

    .line 386
    if-nez v37, :cond_12

    .line 389
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->isLast()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 390
    const/16 v37, 0x1

    .line 391
    :cond_6
    const-string/jumbo v4, "sleep_status"

    move-object/from16 v0, v33

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v33

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v64

    .line 392
    move-wide/from16 v0, v31

    long-to-double v6, v0

    const-wide/high16 v10, 0x4034000000000000L    # 20.0

    mul-double v10, v10, v64

    move-wide/from16 v0, v25

    long-to-double v12, v0

    mul-double/2addr v10, v12

    add-double/2addr v6, v10

    double-to-long v0, v6

    move-wide/from16 v31, v0

    .line 426
    :cond_7
    :goto_5
    cmp-long v4, v62, v44

    if-nez v4, :cond_a

    if-nez v27, :cond_a

    .line 429
    const-wide/16 v6, 0x14

    mul-long v6, v6, v25

    add-long v62, v62, v6

    .line 430
    const-wide/16 v6, 0x1

    sub-long v28, v28, v6

    .line 431
    const/4 v4, 0x1

    move/from16 v0, v37

    if-eq v0, v4, :cond_9

    .line 433
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToNext()Z

    .line 434
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->isLast()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 435
    const/16 v37, 0x1

    .line 436
    :cond_8
    const-string/jumbo v4, "sleep_status"

    move-object/from16 v0, v33

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v33

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    add-double v6, v6, v64

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double v64, v6, v10

    .line 438
    :cond_9
    const/16 v24, 0x1

    .line 444
    :cond_a
    const/16 v38, 0x0

    .line 445
    .local v38, "isLastdecided":Z
    const/16 v27, 0x0

    .line 447
    const-wide/16 v6, 0x1

    cmp-long v4, v28, v6

    if-nez v4, :cond_d

    .line 449
    const-wide/16 v6, 0x0

    cmp-long v4, v60, v6

    if-eqz v4, :cond_17

    .line 459
    :cond_b
    invoke-static/range {v54 .. v55}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfMinute(J)J

    move-result-wide v6

    invoke-static/range {v51 .. v52}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfMinute(J)J

    move-result-wide v10

    cmp-long v4, v6, v10

    if-lez v4, :cond_14

    .line 463
    const/16 v21, 0x1

    .line 465
    const/16 v27, 0x0

    .line 467
    const/16 v38, 0x1

    .line 520
    :cond_c
    :goto_6
    if-eqz v38, :cond_b

    .line 534
    :cond_d
    :goto_7
    const/4 v4, 0x1

    move/from16 v0, v27

    if-ne v0, v4, :cond_18

    .line 568
    .end local v38    # "isLastdecided":Z
    :cond_e
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->close()V

    .line 571
    const-wide/16 v6, 0x0

    cmp-long v4, v60, v6

    if-nez v4, :cond_3

    .line 573
    .end local v8    # "projection":[Ljava/lang/String;
    .end local v9    # "selectionClause":Ljava/lang/String;
    .end local v22    # "sleep_id":J
    .end local v33    # "cursor":Landroid/database/Cursor;
    .end local v35    # "device":Ljava/lang/String;
    .end local v62    # "time":J
    .end local v64    # "val":D
    :cond_f
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 352
    .restart local v22    # "sleep_id":J
    .restart local v35    # "device":Ljava/lang/String;
    :cond_10
    const/16 v20, 0x1

    goto/16 :goto_2

    .line 362
    .restart local v62    # "time":J
    :cond_11
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->moveToNext()Z

    .line 363
    const-string v4, "SLEEP_ID"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v60

    .line 364
    const-string v4, "BED_TIME"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v54

    .line 365
    const-string v4, "RISE_TIME"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v56

    goto/16 :goto_3

    .line 397
    .restart local v8    # "projection":[Ljava/lang/String;
    .restart local v9    # "selectionClause":Ljava/lang/String;
    .restart local v33    # "cursor":Landroid/database/Cursor;
    .restart local v64    # "val":D
    :cond_12
    if-eqz v30, :cond_7

    const/4 v4, 0x0

    aget-object v4, v30, v4

    if-eqz v4, :cond_7

    const/4 v4, 0x0

    aget-object v4, v30, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v6, 0x2723

    if-eq v4, v6, :cond_7

    const/4 v4, 0x0

    aget-object v4, v30, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v6, 0x2727

    if-eq v4, v6, :cond_7

    .line 402
    const-string/jumbo v4, "sample_time"

    move-object/from16 v0, v33

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v33

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v40

    .line 403
    .local v40, "lastSampleTime":J
    invoke-static/range {v40 .. v41}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfMinute(J)J

    move-result-wide v6

    invoke-static/range {v51 .. v52}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfMinute(J)J

    move-result-wide v10

    cmp-long v4, v6, v10

    if-eqz v4, :cond_7

    .line 406
    sub-long v6, v51, v46

    long-to-double v6, v6

    mul-double v6, v6, v49

    move-wide/from16 v0, v31

    long-to-double v10, v0

    sub-double/2addr v6, v10

    sub-long v10, v51, v40

    long-to-double v10, v10

    div-double v58, v6, v10

    .line 410
    .local v58, "temp":D
    const-wide/16 v6, 0x0

    cmpl-double v4, v58, v6

    if-ltz v4, :cond_13

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    cmpg-double v4, v58, v6

    if-gtz v4, :cond_13

    .line 412
    move-wide/from16 v64, v58

    goto/16 :goto_5

    .line 417
    :cond_13
    const-wide v64, 0x4055400000000000L    # 85.0

    goto/16 :goto_5

    .line 472
    .end local v40    # "lastSampleTime":J
    .end local v58    # "temp":D
    .restart local v38    # "isLastdecided":Z
    :cond_14
    cmp-long v4, v54, v51

    if-gez v4, :cond_16

    cmp-long v4, v56, v51

    if-gez v4, :cond_16

    .line 478
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->isLast()Z

    move-result v4

    if-nez v4, :cond_15

    .line 480
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->moveToNext()Z

    .line 482
    const-string v4, "SLEEP_ID"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v60

    .line 484
    const-string v4, "BED_TIME"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v54

    .line 486
    const-string v4, "RISE_TIME"

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v48

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v56

    .line 489
    goto/16 :goto_6

    .line 493
    :cond_15
    const/16 v27, 0x0

    .line 494
    const/16 v21, 0x1

    .line 495
    const-wide/16 v60, 0x0

    .line 496
    const/16 v38, 0x1

    goto/16 :goto_6

    .line 508
    :cond_16
    invoke-static/range {v54 .. v55}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfMinute(J)J

    move-result-wide v6

    invoke-static/range {v51 .. v52}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfMinute(J)J

    move-result-wide v10

    cmp-long v4, v6, v10

    if-gtz v4, :cond_c

    .line 512
    const/16 v27, 0x1

    .line 513
    goto/16 :goto_7

    .line 524
    :cond_17
    const/16 v21, 0x1

    goto/16 :goto_7

    .line 541
    :cond_18
    cmp-long v4, v62, v44

    if-lez v4, :cond_19

    .line 544
    new-instance v34, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct/range {v34 .. v34}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 545
    .local v34, "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    move-object/from16 v0, v34

    move-wide/from16 v1, v64

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->setValue(DZ)V

    .line 546
    move-object/from16 v0, v34

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->setTime(J)V

    .line 547
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 549
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayMap:Ljava/util/HashMap;

    move-wide/from16 v0, v62

    long-to-double v6, v0

    sget-object v10, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    invoke-static {v6, v7, v10, v11}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->getDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    new-instance v10, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const/4 v15, 0x0

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    invoke-direct/range {v10 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplay;-><init>(JJFJJZZJZ)V

    invoke-virtual {v4, v6, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    move-wide/from16 v44, v62

    .line 551
    const/16 v24, 0x0

    .line 553
    .end local v34    # "data":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    :cond_19
    const/16 v20, 0x0

    .line 554
    const-wide/16 v6, 0x1

    sub-long v28, v28, v6

    .line 555
    const-wide/32 v6, 0x124f80

    add-long v62, v62, v6

    .line 558
    if-nez v37, :cond_5

    .line 559
    invoke-interface/range {v33 .. v33}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_4

    .line 579
    .end local v8    # "projection":[Ljava/lang/String;
    .end local v9    # "selectionClause":Ljava/lang/String;
    .end local v20    # "isFirst":Z
    .end local v21    # "isLast":Z
    .end local v22    # "sleep_id":J
    .end local v24    # "falseOverlap":Z
    .end local v25    # "Mins_to_Millis":J
    .end local v27    # "OverlapFlag":I
    .end local v28    # "PlotPoints":J
    .end local v30    # "accessory":[Ljava/lang/String;
    .end local v31    # "aggregate":J
    .end local v33    # "cursor":Landroid/database/Cursor;
    .end local v35    # "device":Ljava/lang/String;
    .end local v37    # "isLastFlag":I
    .end local v38    # "isLastdecided":Z
    .end local v44    # "prevPlottedTime":J
    .end local v46    # "sBedtime":J
    .end local v48    # "sCursor":Landroid/database/Cursor;
    .end local v49    # "sEfficiency":D
    .end local v51    # "sRisetime":J
    .end local v54    # "tBedtime":J
    .end local v56    # "tRisetime":J
    .end local v60    # "temp_id":J
    .end local v62    # "time":J
    .end local v64    # "val":D
    :cond_1a
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->processPointDataForGraphWithMax(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v42

    .line 581
    .restart local v42    # "max":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v42

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->updateLabels(J)J

    move-result-wide v42

    .line 584
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayDayMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v39

    .line 589
    .restart local v39    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v39 .. v39}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v36

    .restart local v36    # "i$":Ljava/util/Iterator;
    :cond_1b
    :goto_8
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1c

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v53

    check-cast v53, Ljava/lang/String;

    .line 591
    .restart local v53    # "string":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayDayMap:Ljava/util/Map;

    move-object/from16 v0, v53

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;

    .line 596
    .restart local v5    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    if-eqz v5, :cond_1b

    .line 598
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;->getDayData()J

    move-result-wide v12

    move-wide/from16 v0, v42

    long-to-double v14, v0

    move-object/from16 v10, p0

    move-object v11, v5

    invoke-direct/range {v10 .. v15}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->drawDayData(Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;JD)V

    goto :goto_8

    .line 605
    .end local v5    # "pointData":Lcom/sec/android/app/shealth/sleepmonitor/utils/PointDataDisplayDayMonth;
    .end local v53    # "string":Ljava/lang/String;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->pointDataDisplayDayMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    goto/16 :goto_1
.end method

.method private timeToValueCalcD(DD)D
    .locals 3
    .param p1, "max"    # D
    .param p3, "value"    # D

    .prologue
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    .line 762
    cmpl-double v2, p3, p1

    if-ltz v2, :cond_0

    .line 768
    :goto_0
    return-wide v0

    .line 764
    :cond_0
    cmpg-double v2, p3, p1

    if-gez v2, :cond_1

    .line 765
    mul-double/2addr v0, p3

    div-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    goto :goto_0

    .line 768
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private updateLabels(J)J
    .locals 13
    .param p1, "duration"    # J

    .prologue
    const-wide/high16 v11, 0x404e000000000000L    # 60.0

    .line 614
    long-to-double v7, p1

    const-wide v9, 0x40ed4c0000000000L    # 60000.0

    div-double v4, v7, v9

    .line 615
    .local v4, "mins":D
    div-double v7, v4, v11

    invoke-static {v7, v8}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    .line 616
    .local v0, "hr":D
    rem-double v7, v4, v11

    double-to-int v3, v7

    .line 617
    .local v3, "min":I
    int-to-float v7, v3

    const/high16 v8, 0x42700000    # 60.0f

    div-float/2addr v7, v8

    float-to-double v7, v7

    add-double/2addr v0, v7

    .line 618
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v2, v7

    .line 619
    .local v2, "max":I
    const/16 v6, 0xc

    .line 620
    .local v6, "ret":I
    const/16 v7, 0xc

    if-lt v2, v7, :cond_0

    .line 621
    int-to-float v7, v2

    const v8, 0x3f8ccccd    # 1.1f

    mul-float/2addr v7, v8

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v2, v7

    .line 622
    iget-object v7, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLabels:[Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 623
    move v6, v2

    .line 625
    :cond_0
    const v7, 0x36ee80

    mul-int/2addr v7, v6

    int-to-long v7, v7

    return-wide v7
.end method


# virtual methods
.method protected createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .locals 1
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 1537
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getContentUriList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1503
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
    .locals 1

    .prologue
    .line 1518
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLegendButtonBackgroundResourceId()I
    .locals 1

    .prologue
    .line 1508
    const/4 v0, 0x0

    return v0
.end method

.method protected getLegendMarks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1513
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getNoDataIcon()I
    .locals 1

    .prologue
    .line 1542
    const/4 v0, 0x0

    return v0
.end method

.method protected getSeparatorDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;
    .locals 6
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 1418
    const-string v2, "-"

    .line 1420
    .local v2, "REGEX_ONE_OR_MORE":Ljava/lang/String;
    const-string v0, "d"

    .line 1421
    .local v0, "DAY_CHAR":Ljava/lang/String;
    const-string v1, "M"

    .line 1423
    .local v1, "MONTH_CHAR":Ljava/lang/String;
    const-string v3, ""

    .line 1424
    .local v3, "dateFormat":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "date_format"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1425
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v4, :cond_1

    .line 1426
    const-string v4, "d"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1427
    const-string v4, "M"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1428
    const-string v4, "--"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1438
    :cond_0
    :goto_0
    const-string v4, "-"

    const-string v5, "/"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 1440
    return-object v3

    .line 1430
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v4, :cond_0

    .line 1431
    const-string v4, "dd-"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1432
    const-string v4, "-dd"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected getYAxisLabelTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1523
    const/4 v0, 0x0

    return-object v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 147
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initDateBar()V

    .line 148
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 150
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 151
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 3

    .prologue
    .line 1452
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1453
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030212

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLegendView:Landroid/view/View;

    .line 1454
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v2, 0x7f080941

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartExplanatoryNotes:Landroid/widget/LinearLayout;

    .line 1456
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLegendView:Landroid/view/View;

    const v2, 0x7f080942

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    .line 1457
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->switchToWalkingSummary:Landroid/widget/ImageButton;

    new-instance v2, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$4;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1466
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->isNoDataGraph()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1467
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartExplanatoryNotes:Landroid/widget/LinearLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1471
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLegendView:Landroid/view/View;

    return-object v1

    .line 1469
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->chartExplanatoryNotes:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 8
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 164
    new-instance v3, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/support/v4/app/FragmentActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 169
    .local v2, "sharedPref":Landroid/content/SharedPreferences;
    if-eqz v2, :cond_1

    const-string v3, "PRE_PERIOD"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->RecreateGraph:I

    if-gt v3, v6, :cond_1

    .line 171
    const-string v3, "PRE_PERIOD"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 172
    .local v1, "gtype":I
    if-eqz v1, :cond_0

    .line 174
    iget v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->RecreateGraph:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->RecreateGraph:I

    .line 175
    if-ne v1, v7, :cond_4

    .line 176
    sget-object p3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 182
    :cond_0
    :goto_0
    invoke-virtual {p0, p3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 184
    .end local v1    # "gtype":I
    :cond_1
    iput-object p3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 185
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v3, v4, :cond_6

    .line 186
    const-string v3, "%"

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLabel:Ljava/lang/String;

    .line 187
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v5

    const-string v4, "50"

    aput-object v4, v3, v7

    const-string v4, "100"

    aput-object v4, v3, v6

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLabels:[Ljava/lang/String;

    .line 193
    :goto_1
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->setTimeData(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 195
    invoke-direct {p0, p3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->setUpDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 198
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getSize()I

    move-result v3

    if-nez v3, :cond_7

    .line 200
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .line 201
    .local v0, "activity":Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;
    if-eqz v0, :cond_3

    .line 202
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->deleteShareViaButton()V

    .line 205
    :cond_3
    const v3, 0x7f0205b7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->showNoData(I)V

    .line 206
    new-instance v3, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 213
    .end local v0    # "activity":Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;
    :goto_2
    return-object v3

    .line 177
    .restart local v1    # "gtype":I
    :cond_4
    if-ne v1, v6, :cond_5

    .line 178
    sget-object p3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0

    .line 180
    :cond_5
    sget-object p3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    goto :goto_0

    .line 189
    .end local v1    # "gtype":I
    :cond_6
    const v3, 0x7f090d5b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLabel:Ljava/lang/String;

    .line 190
    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "0"

    aput-object v4, v3, v5

    const-string v4, "12"

    aput-object v4, v3, v7

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mLabels:[Ljava/lang/String;

    goto :goto_1

    .line 208
    :cond_7
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->refreshChartView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 210
    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p3, v3, :cond_8

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeAreaCurvedChartView;

    goto :goto_2

    .line 213
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->timeBarChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    goto :goto_2
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 1445
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->sleepMonitorInformationArea:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;

    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->sleepMonitorInformationArea:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 1447
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->sleepMonitorInformationArea:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorInformationArea;

    return-object v0
.end method

.method protected initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 0
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    .prologue
    .line 1529
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1490
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onAttach(Landroid/app/Activity;)V

    .line 1491
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.sleepmonitor"

    const-string v2, "SL06"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1492
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->registerContentObserver(Landroid/net/Uri;)V

    .line 1493
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 155
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 156
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->RecreateGraph:I

    .line 158
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mContents:Landroid/app/Activity;

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->density:F

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mContents:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    iput v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->scaledDensity:F

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->sleepUnit:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mContents:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090d5d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->sleepUnit:Ljava/lang/String;

    .line 141
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 1556
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDestroyView()V

    .line 1557
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 1497
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDetach()V

    .line 1498
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->unregisterContentObserver()V

    .line 1499
    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 248
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onPause()V

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 250
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->editor:Landroid/content/SharedPreferences$Editor;

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_0

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "PRE_PERIOD"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 258
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mContents:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->contentObserver1:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 261
    return-void

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_1

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "PRE_PERIOD"

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->editor:Landroid/content/SharedPreferences$Editor;

    const-string v2, "PRE_PERIOD"

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 241
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onResume()V

    .line 242
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->contentObserver1:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mContents:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->contentObserver1:Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC$SleepDBContentObserver1;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 244
    return-void
.end method

.method protected setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
    .locals 0
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .prologue
    .line 1548
    return-void
.end method

.method public setSelectedTime(JZ)V
    .locals 0
    .param p1, "selectedTime"    # J
    .param p3, "isResetNeeded"    # Z

    .prologue
    .line 819
    sput-wide p1, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mSelectedTime:J

    .line 820
    sput-boolean p3, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->mIsResetNeeded:Z

    .line 821
    return-void
.end method

.method protected updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 1533
    return-void
.end method
