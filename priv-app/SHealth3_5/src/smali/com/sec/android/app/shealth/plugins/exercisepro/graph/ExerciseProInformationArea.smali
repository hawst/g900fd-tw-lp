.class public Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;
.super Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;
.source "ExerciseProInformationArea.java"


# instance fields
.field public final MONTHS:[Ljava/lang/String;

.field private date:Landroid/widget/TextView;

.field private firstInfortitle:Ljava/lang/String;

.field private firstInforvalue:Ljava/lang/String;

.field private firstUIValue:Ljava/lang/String;

.field private first_info:Landroid/widget/LinearLayout;

.field private first_info_data:Landroid/widget/TextView;

.field private first_info_title:Landroid/widget/TextView;

.field private first_info_unit:Landroid/widget/TextView;

.field private informationAreaparent:Landroid/widget/LinearLayout;

.field private informationWeight:I

.field private isFirstInfoValueSet:Z

.field private isSecondInfoValueSet:Z

.field private isThirdInfoValueSet:Z

.field private secondInforvalue:Ljava/lang/String;

.field private secondUIValue:Ljava/lang/String;

.field private second_info:Landroid/widget/LinearLayout;

.field private second_info_data:Landroid/widget/TextView;

.field private second_info_title:Landroid/widget/TextView;

.field private second_info_unit:Landroid/widget/TextView;

.field private thirdInforvalue:Ljava/lang/String;

.field private thirdUIValue:Ljava/lang/String;

.field private third_info:Landroid/widget/LinearLayout;

.field private third_info_data:Landroid/widget/TextView;

.field private third_info_title:Landroid/widget/TextView;

.field private third_info_unit:Landroid/widget/TextView;

.field private time_str:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 57
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/graph/InformationAreaView;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->MONTHS:[Ljava/lang/String;

    .line 52
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isFirstInfoValueSet:Z

    .line 53
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isSecondInfoValueSet:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isThirdInfoValueSet:Z

    .line 55
    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->informationWeight:I

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->initLayout()V

    .line 59
    return-void
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f080357

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    .line 63
    const v0, 0x7f080358

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->date:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f08035a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info:Landroid/widget/LinearLayout;

    .line 65
    const v0, 0x7f08035c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info_data:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f08035b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info_title:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f08035d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info_unit:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f08035e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info:Landroid/widget/LinearLayout;

    .line 70
    const v0, 0x7f080360

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info_data:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f08035f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info_title:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f080362

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info_unit:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f080364

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info:Landroid/widget/LinearLayout;

    .line 75
    const v0, 0x7f080366

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info_data:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f080365

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info_title:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f080367

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info_unit:Landroid/widget/TextView;

    .line 79
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 246
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0301c0

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 10

    .prologue
    const v9, 0x7f090a4e

    const v8, 0x7f090a4c

    const v7, 0x3fa66666    # 1.3f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 88
    const-string v1, ""

    .line 89
    .local v1, "tempContentDescription":Ljava/lang/String;
    const/4 v2, 0x0

    .line 90
    .local v2, "threeValues":Z
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 91
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->date:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->date:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 95
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isFirstInfoValueSet:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isSecondInfoValueSet:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isThirdInfoValueSet:Z

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 96
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isFirstInfoValueSet:Z

    if-eqz v3, :cond_2

    .line 97
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info_data:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstInforvalue:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info_title:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstInfortitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info_unit:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstUIValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 103
    if-eqz v2, :cond_1

    .line 104
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 105
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 106
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstInfortitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstInforvalue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstUIValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 112
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isSecondInfoValueSet:Z

    if-eqz v3, :cond_4

    .line 113
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info_data:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info_title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info_unit:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->secondUIValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 118
    if-eqz v2, :cond_3

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 120
    .restart local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 124
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->secondInforvalue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->secondUIValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 128
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isThirdInfoValueSet:Z

    if-eqz v3, :cond_6

    .line 131
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info_data:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->thirdInforvalue:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info_title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info_unit:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->thirdUIValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 136
    if-eqz v2, :cond_5

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 138
    .restart local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 142
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->thirdInforvalue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->thirdUIValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 145
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->informationAreaparent:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 146
    return-void
.end method

.method public setDate(DLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 8
    .param p1, "realX"    # D
    .param p3, "periodH"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const v7, 0x7f090d5b

    const v6, 0x7f0900eb

    .line 149
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 150
    .local v2, "c":Ljava/util/Calendar;
    double-to-long v4, p1

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 152
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v4, "H"

    invoke-direct {v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 153
    .local v0, "bubbleTimeHourFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v4, "mm"

    invoke-direct {v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 154
    .local v1, "bubbleTimeMinFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    .line 155
    const-string v4, "0"

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 156
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f090f6d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    .line 176
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 163
    .local v3, "language":Ljava/lang/String;
    const-string v4, "ko"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 165
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    .line 166
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    .line 167
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    goto :goto_0

    .line 171
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    .line 172
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    .line 173
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->time_str:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public setFirstInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "unit"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 179
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isFirstInfoValueSet:Z

    .line 181
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstInforvalue:Ljava/lang/String;

    .line 182
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstUIValue:Ljava/lang/String;

    .line 183
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->firstInfortitle:Ljava/lang/String;

    .line 185
    :cond_0
    return-void
.end method

.method public setFirstInformationVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 212
    if-eqz p1, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 219
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->first_info:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setInformationWeight(I)V
    .locals 0
    .param p1, "weight"    # I

    .prologue
    .line 208
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->informationWeight:I

    .line 209
    return-void
.end method

.method public setSecondInformation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "unit"    # Ljava/lang/String;

    .prologue
    .line 189
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isSecondInfoValueSet:Z

    .line 191
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->secondInforvalue:Ljava/lang/String;

    .line 192
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->secondUIValue:Ljava/lang/String;

    .line 194
    :cond_0
    return-void
.end method

.method public setSecondInformationVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 222
    if-eqz p1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 229
    :goto_0
    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->second_info:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setThirdInformation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "unit"    # Ljava/lang/String;

    .prologue
    .line 198
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->isThirdInfoValueSet:Z

    .line 200
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->thirdInforvalue:Ljava/lang/String;

    .line 201
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->thirdUIValue:Ljava/lang/String;

    .line 203
    :cond_0
    return-void
.end method

.method public setThirdInformationVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 232
    if-eqz p1, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 240
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/graph/ExerciseProInformationArea;->third_info:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public update(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 252
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    return-void
.end method
