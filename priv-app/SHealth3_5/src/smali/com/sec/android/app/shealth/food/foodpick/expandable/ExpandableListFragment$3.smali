.class Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$3;
.super Ljava/lang/Object;
.source "ExpandableListFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->initAdapterListeners(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnItemClickListener",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;)V
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->mOnClickListener:Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;->access$200(Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;)Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;->createSelectedPanelHolder()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;->onSelectedPanelHolderSelected(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z

    .line 199
    return-void
.end method

.method public bridge synthetic onItemClick(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 195
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment$3;->onItemClick(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;)V

    return-void
.end method
