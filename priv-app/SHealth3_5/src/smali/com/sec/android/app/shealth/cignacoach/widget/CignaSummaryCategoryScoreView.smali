.class public Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;
.super Landroid/widget/RelativeLayout;
.source "CignaSummaryCategoryScoreView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;,
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;,
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;,
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;,
        Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;
    }
.end annotation


# static fields
.field private static final ANIMATION_DURATION:I = 0x44c

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAnimationEnable:Z

.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field private mContext:Landroid/content/Context;

.field private mCurrentScore:I

.field private mDisplayWidth:I

.field private mEndIconBGSize:F

.field private mGreenNPD:Landroid/graphics/drawable/NinePatchDrawable;

.field private mIconMoveAnimator:Landroid/animation/ValueAnimator;

.field private mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

.field private mNoScoreCategoryTxt:Landroid/widget/TextView;

.field private mNoScoreClickLayout:Landroid/widget/LinearLayout;

.field private mNoScoreIconBg:Landroid/widget/ImageView;

.field private mNoScoreIconImg:Landroid/widget/ImageView;

.field private mNoScoreLayout:Landroid/widget/LinearLayout;

.field private mOnNoScoreClickListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;

.field private mOnScoreClickListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;

.field private mOnScoreLayoutExpandCompleteListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;

.field private mScoreAnimationGreenBGBmp:Landroid/graphics/Bitmap;

.field private mScoreAnimationIconBgImg:Landroid/widget/ImageView;

.field private mScoreAnimationIconImg:Landroid/widget/ImageView;

.field private mScoreAnimationLayout:Landroid/widget/FrameLayout;

.field private mScoreAnimationWhiteBGBmp:Landroid/graphics/Bitmap;

.field private mScoreCategoryTxt:Landroid/widget/TextView;

.field private mScoreIconImg:Landroid/widget/ImageView;

.field private mScoreLayout:Landroid/widget/LinearLayout;

.field private mScoreScoreTxt:Landroid/widget/TextView;

.field private mStartIconBGSize:F

.field private mWhiteNPD:Landroid/graphics/drawable/NinePatchDrawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    .line 59
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mGreenNPD:Landroid/graphics/drawable/NinePatchDrawable;

    .line 60
    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mWhiteNPD:Landroid/graphics/drawable/NinePatchDrawable;

    .line 89
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 92
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 93
    .local v1, "outSize":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 94
    iget v2, v1, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mDisplayWidth:I

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a01fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    .line 97
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mDisplayWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mEndIconBGSize:F

    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initLayout()V

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initListner()V

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnNoScoreClickListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnScoreClickListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScoreView()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    return-object v0
.end method

.method private getIconGreenImageResource()I
    .locals 2

    .prologue
    .line 402
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 414
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 404
    :pswitch_0
    const v0, 0x7f02008d

    goto :goto_0

    .line 406
    :pswitch_1
    const v0, 0x7f020091

    goto :goto_0

    .line 408
    :pswitch_2
    const v0, 0x7f020095

    goto :goto_0

    .line 410
    :pswitch_3
    const v0, 0x7f020099

    goto :goto_0

    .line 412
    :pswitch_4
    const v0, 0x7f02009d

    goto :goto_0

    .line 402
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getIconImageResource(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;)I
    .locals 2
    .param p1, "iconColor"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    .prologue
    .line 363
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;->$SwitchMap$com$sec$android$app$shealth$cignacoach$widget$CignaSummaryCategoryScoreView$IconColor:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 382
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 366
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getIconRedImageResource()I

    move-result v0

    goto :goto_0

    .line 370
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getIconYellowImageResource()I

    move-result v0

    goto :goto_0

    .line 374
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getIconGreenImageResource()I

    move-result v0

    goto :goto_0

    .line 377
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getIconWhiteImageResource()I

    move-result v0

    goto :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getIconRedImageResource()I
    .locals 2

    .prologue
    .line 434
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 446
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 436
    :pswitch_0
    const v0, 0x7f02008e

    goto :goto_0

    .line 438
    :pswitch_1
    const v0, 0x7f020092

    goto :goto_0

    .line 440
    :pswitch_2
    const v0, 0x7f020096

    goto :goto_0

    .line 442
    :pswitch_3
    const v0, 0x7f02009a

    goto :goto_0

    .line 444
    :pswitch_4
    const v0, 0x7f02009e

    goto :goto_0

    .line 434
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getIconWhiteImageResource()I
    .locals 2

    .prologue
    .line 386
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 398
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 388
    :pswitch_0
    const v0, 0x7f02008f

    goto :goto_0

    .line 390
    :pswitch_1
    const v0, 0x7f020093

    goto :goto_0

    .line 392
    :pswitch_2
    const v0, 0x7f020097

    goto :goto_0

    .line 394
    :pswitch_3
    const v0, 0x7f02009b

    goto :goto_0

    .line 396
    :pswitch_4
    const v0, 0x7f02009f

    goto :goto_0

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getIconYellowImageResource()I
    .locals 2

    .prologue
    .line 418
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 430
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 420
    :pswitch_0
    const v0, 0x7f020090

    goto :goto_0

    .line 422
    :pswitch_1
    const v0, 0x7f020094

    goto :goto_0

    .line 424
    :pswitch_2
    const v0, 0x7f020098

    goto :goto_0

    .line 426
    :pswitch_3
    const v0, 0x7f02009c

    goto :goto_0

    .line 428
    :pswitch_4
    const v0, 0x7f0200a0

    goto :goto_0

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getScoreIconImgResourceId()I
    .locals 2

    .prologue
    .line 347
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCurrentScore:I

    const/16 v1, 0x1e

    if-gt v0, v1, :cond_0

    .line 349
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->RED:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getIconImageResource(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;)I

    move-result v0

    .line 357
    :goto_0
    return v0

    .line 351
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCurrentScore:I

    const/16 v1, 0x46

    if-gt v0, v1, :cond_1

    .line 353
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->YELLOW:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getIconImageResource(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;)I

    move-result v0

    goto :goto_0

    .line 357
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->GREEN:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getIconImageResource(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;)I

    move-result v0

    goto :goto_0
.end method

.method private initAnimationView()V
    .locals 8

    .prologue
    .line 200
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020088

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->loadBitmap(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationGreenBGBmp:Landroid/graphics/Bitmap;

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationGreenBGBmp:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 203
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this case should never occur. System failed to get bitmap from the resource"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :goto_0
    return-void

    .line 206
    :cond_0
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 207
    .local v4, "rt":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mGreenNPD:Landroid/graphics/drawable/NinePatchDrawable;

    if-nez v0, :cond_1

    .line 208
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationGreenBGBmp:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationGreenBGBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mGreenNPD:Landroid/graphics/drawable/NinePatchDrawable;

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mGreenNPD:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xdd

    const/16 v5, 0xdd

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mGreenNPD:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    .end local v4    # "rt":Landroid/graphics/Rect;
    :goto_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 239
    :goto_2
    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    float-to-int v0, v0

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    float-to-int v1, v1

    const/4 v2, 0x1

    invoke-direct {v7, v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 240
    .local v7, "socoreIconLP":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 213
    .end local v7    # "socoreIconLP":Landroid/widget/FrameLayout$LayoutParams;
    :catch_0
    move-exception v6

    .line 214
    .local v6, "e":Ljava/lang/NullPointerException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Context/image cache is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-virtual {v6}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 220
    .end local v6    # "e":Ljava/lang/NullPointerException;
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    const v1, 0x7f02008d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 223
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    const v1, 0x7f020091

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 226
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    const v1, 0x7f020095

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 229
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    const v1, 0x7f020099

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 232
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    const v1, 0x7f02009d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private initLayout()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    const v1, 0x7f03006e

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 107
    const v0, 0x7f080200

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreLayout:Landroid/widget/LinearLayout;

    .line 108
    const v0, 0x7f080201

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreClickLayout:Landroid/widget/LinearLayout;

    .line 109
    const v0, 0x7f080204

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconImg:Landroid/widget/ImageView;

    .line 110
    const v0, 0x7f080203

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    .line 111
    const v0, 0x7f080205

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreCategoryTxt:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f080206

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;

    .line 114
    const v0, 0x7f080208

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    .line 115
    const v0, 0x7f080207

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;

    .line 117
    const v0, 0x7f080209

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    .line 118
    const v0, 0x7f08020a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreIconImg:Landroid/widget/ImageView;

    .line 119
    const v0, 0x7f08020b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f08020c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreScoreTxt:Landroid/widget/TextView;

    .line 122
    return-void
.end method

.method private initListner()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreClickLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    return-void
.end method

.method private initNoScoreView()V
    .locals 7

    .prologue
    const v6, 0x7f090c87

    const v5, 0x7f090c86

    const v4, 0x7f090c85

    const v3, 0x7f090c5f

    const v2, 0x7f09017e

    .line 166
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 195
    :goto_0
    return-void

    .line 168
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f02008f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 173
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f020093

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 178
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f020097

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 183
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f02009b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 188
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f02009f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private initScoreView()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreScoreTxt:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$6;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 271
    :goto_0
    return-void

    .line 249
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f02008d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    const v1, 0x7f090c85

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 253
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f020091

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    const v1, 0x7f090c86

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 257
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f020095

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    const v1, 0x7f09017e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 261
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f020099

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    const v1, 0x7f090c87

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 265
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreIconImg:Landroid/widget/ImageView;

    const v1, 0x7f02009d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    const v1, 0x7f090c5f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 247
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private prepareAnimation()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 315
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 319
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01fd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 324
    .local v0, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 326
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 327
    return-void
.end method

.method private setIdForFocusableView()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 626
    const/4 v0, -0x1

    .line 627
    .local v0, "noScoreIconImgId":I
    const/4 v1, -0x1

    .line 629
    .local v1, "scoreLayoutId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 654
    :goto_0
    if-eq v0, v3, :cond_0

    .line 655
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setId(I)V

    .line 658
    :cond_0
    if-eq v1, v3, :cond_1

    .line 659
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 661
    :cond_1
    return-void

    .line 631
    :pswitch_0
    const/16 v0, 0x2710

    .line 632
    const/16 v1, 0x4e20

    .line 633
    goto :goto_0

    .line 635
    :pswitch_1
    const/16 v0, 0x2711

    .line 636
    const/16 v1, 0x4e21

    .line 637
    goto :goto_0

    .line 639
    :pswitch_2
    const/16 v0, 0x2712

    .line 640
    const/16 v1, 0x4e22

    .line 641
    goto :goto_0

    .line 643
    :pswitch_3
    const/16 v0, 0x2713

    .line 644
    const/16 v1, 0x4e23

    .line 645
    goto :goto_0

    .line 647
    :pswitch_4
    const/16 v0, 0x2714

    .line 648
    const/16 v1, 0x4e24

    .line 649
    goto :goto_0

    .line 629
    :pswitch_data_0
    .packed-switch 0x7f08027b
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private updateScoreView()V
    .locals 5

    .prologue
    .line 330
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreScoreTxt:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCurrentScore:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreIconImg:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getScoreIconImgResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 333
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimationEnable:Z

    if-eqz v2, :cond_0

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    const v3, 0x7f040003

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 335
    .local v0, "categoryAnim":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    const v3, 0x7f040004

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 337
    .local v1, "scoreAnim":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreScoreTxt:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 341
    .end local v0    # "categoryAnim":Landroid/view/animation/Animation;
    .end local v1    # "scoreAnim":Landroid/view/animation/Animation;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnScoreLayoutExpandCompleteListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;

    if-eqz v2, :cond_1

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnScoreLayoutExpandCompleteListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;->onScoreLayoutExpand()V

    .line 344
    :cond_1
    return-void
.end method


# virtual methods
.method public clearCategoryAnimation()V
    .locals 1

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->stopAnimation()V

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 595
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreScoreTxt:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 598
    return-void
.end method

.method public getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method public getCurrentScore()I
    .locals 1

    .prologue
    .line 617
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCurrentScore:I

    return v0
.end method

.method public initScoreView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;)V
    .locals 2
    .param p1, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p2, "imageCache"    # Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    .prologue
    const/16 v1, 0x8

    .line 144
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    .line 145
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initNoScoreView()V

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initAnimationView()V

    .line 153
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->initScoreView()V

    .line 154
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->setIdForFocusableView()V

    .line 155
    return-void
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    .line 498
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 12
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    const/16 v11, 0xdd

    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    const/4 v8, 0x0

    .line 514
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimationEnable:Z

    if-nez v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 517
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScoreView()V

    .line 574
    :goto_0
    return-void

    .line 521
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mImageCache:Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020089

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageCacheTool;->loadBitmap(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationWhiteBGBmp:Landroid/graphics/Bitmap;

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationWhiteBGBmp:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 524
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this case should never occur. System failed to get bitmap from the resource"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 527
    :cond_1
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 528
    .local v4, "rt":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mWhiteNPD:Landroid/graphics/drawable/NinePatchDrawable;

    if-nez v0, :cond_2

    .line 529
    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationWhiteBGBmp:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationWhiteBGBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mWhiteNPD:Landroid/graphics/drawable/NinePatchDrawable;

    .line 531
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mWhiteNPD:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, v8, v8, v11, v11}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconBgImg:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mWhiteNPD:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getScoreIconImgResourceId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 536
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mDisplayWidth:I

    int-to-double v0, v0

    div-double/2addr v0, v9

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    float-to-double v2, v2

    div-double/2addr v2, v9

    sub-double/2addr v0, v2

    double-to-int v7, v0

    .line 537
    .local v7, "start":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0201

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 539
    .local v6, "end":I
    const/4 v0, 0x2

    new-array v0, v0, [F

    int-to-float v1, v7

    aput v1, v0, v8

    const/4 v1, 0x1

    int-to-float v2, v6

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mIconMoveAnimator:Landroid/animation/ValueAnimator;

    .line 540
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mIconMoveAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x44c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mIconMoveAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 562
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mIconMoveAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mIconMoveAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto/16 :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    .line 580
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "anim"    # Landroid/animation/Animator;

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationIconImg:Landroid/widget/ImageView;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;->WHITE:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->getIconImageResource(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$IconColor;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 509
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 464
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 480
    :goto_0
    return-void

    .line 467
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnNoScoreClickListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;->onNoScoreClick(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    goto :goto_0

    .line 472
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnScoreClickListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;->onScoreClick(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    goto :goto_0

    .line 464
    :sswitch_data_0
    .sparse-switch
        0x7f080201 -> :sswitch_0
        0x7f080203 -> :sswitch_0
        0x7f080209 -> :sswitch_1
    .end sparse-switch
.end method

.method public setCategoryTxt(I)V
    .locals 1
    .param p1, "strId"    # I

    .prologue
    .line 621
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreCategoryTxt:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 623
    return-void
.end method

.method public setFocusForFocusableView()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 667
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 668
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreIconBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 670
    :cond_1
    return-void
.end method

.method public setOnNoScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnNoScoreClickListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnNoScoreClickListner;

    .line 451
    return-void
.end method

.method public setOnScoreClickListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;

    .prologue
    .line 454
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnScoreClickListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreClickListner;

    .line 455
    return-void
.end method

.method public setOnScoreLayoutExpandCompleteListner(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mOnScoreLayoutExpandCompleteListner:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$OnScoreLayoutExpandCompleteListner;

    .line 459
    return-void
.end method

.method public setVisibilityNoScoreLayout(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 162
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreLayout:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 163
    return-void

    .line 162
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mIconMoveAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mIconMoveAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mIconMoveAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 608
    :cond_1
    return-void
.end method

.method public updateScore(IZ)V
    .locals 3
    .param p1, "score"    # I
    .param p2, "animationEnable"    # Z

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 275
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimationEnable:Z

    .line 277
    if-gez p1, :cond_0

    .line 312
    :goto_0
    return-void

    .line 281
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->prepareAnimation()V

    .line 283
    iput p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mCurrentScore:I

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 286
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimationEnable:Z

    if-eqz v0, :cond_1

    .line 287
    const/4 v0, 0x2

    new-array v0, v0, [F

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mStartIconBGSize:F

    aput v1, v0, v2

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mEndIconBGSize:F

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x44c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mNoScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 310
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScoreView()V

    goto :goto_0
.end method
