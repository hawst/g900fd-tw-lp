.class Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomMediaScanner"
.end annotation


# instance fields
.field mContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field msc:Landroid/media/MediaScannerConnection;

.field path:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->msc:Landroid/media/MediaScannerConnection;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->path:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->mContext:Ljava/lang/ref/WeakReference;

    new-instance v0, Landroid/media/MediaScannerConnection;

    invoke-direct {v0, p1, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->msc:Landroid/media/MediaScannerConnection;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->connect()V

    return-void
.end method

.method public static aP31uMniye()Ljava/lang/String;
    .locals 72

    const/16 v0, 0x45

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, 0x1d

    const/16 v31, 0x1e

    const/16 v32, 0x1f

    const/16 v33, 0x20

    const/16 v34, 0x21

    const/16 v35, 0x22

    const/16 v36, 0x23

    const/16 v37, 0x24

    const/16 v38, 0x25

    const/16 v39, 0x26

    const/16 v40, 0x27

    const/16 v41, 0x28

    const/16 v42, 0x29

    const/16 v43, 0x2a

    const/16 v44, 0x2b

    const/16 v45, 0x2c

    const/16 v46, 0x2d

    const/16 v47, 0x2e

    const/16 v48, 0x2f

    const/16 v49, 0x30

    const/16 v50, 0x31

    const/16 v51, 0x32

    const/16 v52, 0x33

    const/16 v53, 0x34

    const/16 v54, 0x35

    const/16 v55, 0x36

    const/16 v56, 0x37

    const/16 v57, 0x38

    const/16 v58, 0x39

    const/16 v59, 0x3a

    const/16 v60, 0x3b

    const/16 v61, 0x3c

    const/16 v62, 0x3d

    const/16 v63, 0x3e

    const/16 v64, 0x3f

    const/16 v65, 0x40

    const/16 v66, 0x41

    const/16 v67, 0x42

    const/16 v68, 0x43

    const/16 v69, 0x44

    const/16 v70, 0x6d22

    aput v70, v1, v69

    const/16 v69, -0x42fc

    aput v69, v1, v68

    const/16 v68, -0x35

    aput v68, v1, v67

    const/16 v67, -0x20

    aput v67, v1, v66

    const/16 v66, -0x75

    aput v66, v1, v65

    const/16 v65, 0x6f21

    aput v65, v1, v64

    const/16 v64, -0x2bf2

    aput v64, v1, v63

    const/16 v63, -0x44

    aput v63, v1, v62

    const/16 v62, -0xc

    aput v62, v1, v61

    const/16 v61, -0x3c

    aput v61, v1, v60

    const/16 v60, -0x7dd9

    aput v60, v1, v59

    const/16 v59, -0x1b

    aput v59, v1, v58

    const/16 v58, -0x32

    aput v58, v1, v57

    const/16 v57, 0x3b4c

    aput v57, v1, v56

    const/16 v56, 0x3149

    aput v56, v1, v55

    const/16 v55, 0x6159

    aput v55, v1, v54

    const/16 v54, 0x5215

    aput v54, v1, v53

    const/16 v53, -0x428e

    aput v53, v1, v52

    const/16 v52, -0x27

    aput v52, v1, v51

    const/16 v51, -0x1a

    aput v51, v1, v50

    const/16 v50, -0x5dfe

    aput v50, v1, v49

    const/16 v49, -0x3f

    aput v49, v1, v48

    const/16 v48, -0x51

    aput v48, v1, v47

    const/16 v47, 0x579

    aput v47, v1, v46

    const/16 v46, 0x4c75

    aput v46, v1, v45

    const/16 v45, 0x186c

    aput v45, v1, v44

    const/16 v44, 0x5d6c

    aput v44, v1, v43

    const/16 v43, -0x3bce

    aput v43, v1, v42

    const/16 v42, -0x56

    aput v42, v1, v41

    const/16 v41, -0x12

    aput v41, v1, v40

    const/16 v40, 0x2075

    aput v40, v1, v39

    const/16 v39, 0x143

    aput v39, v1, v38

    const/16 v38, 0x2f2c

    aput v38, v1, v37

    const/16 v37, 0x3202

    aput v37, v1, v36

    const/16 v36, -0x3dee

    aput v36, v1, v35

    const/16 v35, -0x52

    aput v35, v1, v34

    const/16 v34, -0x76

    aput v34, v1, v33

    const/16 v33, -0x7e

    aput v33, v1, v32

    const/16 v32, 0x651a

    aput v32, v1, v31

    const/16 v31, 0x5045

    aput v31, v1, v30

    const/16 v30, -0x42dd

    aput v30, v1, v29

    const/16 v29, -0x2c

    aput v29, v1, v28

    const/16 v28, -0x31

    aput v28, v1, v27

    const/16 v27, -0x4f

    aput v27, v1, v26

    const/16 v26, -0x5200

    aput v26, v1, v25

    const/16 v25, -0x26

    aput v25, v1, v24

    const/16 v24, -0x2fb5

    aput v24, v1, v23

    const/16 v23, -0x49

    aput v23, v1, v22

    const/16 v22, -0x2e

    aput v22, v1, v21

    const/16 v21, -0x60bd

    aput v21, v1, v20

    const/16 v20, -0x19

    aput v20, v1, v19

    const/16 v19, -0xe

    aput v19, v1, v18

    const/16 v18, 0x4f6d

    aput v18, v1, v17

    const/16 v17, 0x5521

    aput v17, v1, v16

    const/16 v16, -0x6cc6

    aput v16, v1, v15

    const/16 v15, -0x30

    aput v15, v1, v14

    const/16 v14, 0x310

    aput v14, v1, v13

    const/16 v13, -0x3cdd

    aput v13, v1, v12

    const/16 v12, -0x4f

    aput v12, v1, v11

    const/16 v11, -0x52ac

    aput v11, v1, v10

    const/16 v10, -0x73

    aput v10, v1, v9

    const/16 v9, -0x35

    aput v9, v1, v8

    const/16 v8, -0x71

    aput v8, v1, v7

    const/16 v7, 0x2a47

    aput v7, v1, v6

    const/16 v6, -0x45a2

    aput v6, v1, v5

    const/16 v5, -0x2c

    aput v5, v1, v4

    const/16 v4, 0x3329

    aput v4, v1, v3

    const/16 v3, -0x3b90

    aput v3, v1, v2

    const/16 v2, -0x57

    aput v2, v1, v0

    const/16 v0, 0x45

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x1d

    const/16 v32, 0x1e

    const/16 v33, 0x1f

    const/16 v34, 0x20

    const/16 v35, 0x21

    const/16 v36, 0x22

    const/16 v37, 0x23

    const/16 v38, 0x24

    const/16 v39, 0x25

    const/16 v40, 0x26

    const/16 v41, 0x27

    const/16 v42, 0x28

    const/16 v43, 0x29

    const/16 v44, 0x2a

    const/16 v45, 0x2b

    const/16 v46, 0x2c

    const/16 v47, 0x2d

    const/16 v48, 0x2e

    const/16 v49, 0x2f

    const/16 v50, 0x30

    const/16 v51, 0x31

    const/16 v52, 0x32

    const/16 v53, 0x33

    const/16 v54, 0x34

    const/16 v55, 0x35

    const/16 v56, 0x36

    const/16 v57, 0x37

    const/16 v58, 0x38

    const/16 v59, 0x39

    const/16 v60, 0x3a

    const/16 v61, 0x3b

    const/16 v62, 0x3c

    const/16 v63, 0x3d

    const/16 v64, 0x3e

    const/16 v65, 0x3f

    const/16 v66, 0x40

    const/16 v67, 0x41

    const/16 v68, 0x42

    const/16 v69, 0x43

    const/16 v70, 0x44

    const/16 v71, 0x6d43

    aput v71, v0, v70

    const/16 v70, -0x4293

    aput v70, v0, v69

    const/16 v69, -0x43

    aput v69, v0, v68

    const/16 v68, -0x40

    aput v68, v0, v67

    const/16 v67, -0x12

    aput v67, v0, v66

    const/16 v66, 0x6f53

    aput v66, v0, v65

    const/16 v65, -0x2b91

    aput v65, v0, v64

    const/16 v64, -0x2c

    aput v64, v0, v63

    const/16 v63, -0x79

    aput v63, v0, v62

    const/16 v62, -0x1c

    aput v62, v0, v61

    const/16 v61, -0x7db1

    aput v61, v0, v60

    const/16 v60, -0x7e

    aput v60, v0, v59

    const/16 v59, -0x45

    aput v59, v0, v58

    const/16 v58, 0x3b23

    aput v58, v0, v57

    const/16 v57, 0x313b

    aput v57, v0, v56

    const/16 v56, 0x6131

    aput v56, v0, v55

    const/16 v55, 0x5261

    aput v55, v0, v54

    const/16 v54, -0x42ae

    aput v54, v0, v53

    const/16 v53, -0x43

    aput v53, v0, v52

    const/16 v52, -0x7d

    aput v52, v0, v51

    const/16 v51, -0x5d99

    aput v51, v0, v50

    const/16 v50, -0x5e

    aput v50, v0, v49

    const/16 v49, -0x40

    aput v49, v0, v48

    const/16 v48, 0x50b

    aput v48, v0, v47

    const/16 v47, 0x4c05

    aput v47, v0, v46

    const/16 v46, 0x184c

    aput v46, v0, v45

    const/16 v45, 0x5d18

    aput v45, v0, v44

    const/16 v44, -0x3ba3

    aput v44, v0, v43

    const/16 v43, -0x3c

    aput v43, v0, v42

    const/16 v42, -0x80

    aput v42, v0, v41

    const/16 v41, 0x2014

    aput v41, v0, v40

    const/16 v40, 0x120

    aput v40, v0, v39

    const/16 v39, 0x2f01

    aput v39, v0, v38

    const/16 v38, 0x322f

    aput v38, v0, v37

    const/16 v37, -0x3dce

    aput v37, v0, v36

    const/16 v36, -0x3e

    aput v36, v0, v35

    const/16 v35, -0x1a

    aput v35, v0, v34

    const/16 v34, -0x9

    aput v34, v0, v33

    const/16 v33, 0x6574

    aput v33, v0, v32

    const/16 v32, 0x5065

    aput v32, v0, v31

    const/16 v31, -0x42b0

    aput v31, v0, v30

    const/16 v30, -0x43

    aput v30, v0, v29

    const/16 v29, -0x11

    aput v29, v0, v28

    const/16 v28, -0x68

    aput v28, v0, v27

    const/16 v27, -0x51d8

    aput v27, v0, v26

    const/16 v26, -0x52

    aput v26, v0, v25

    const/16 v25, -0x2fd2

    aput v25, v0, v24

    const/16 v24, -0x30

    aput v24, v0, v23

    const/16 v23, -0x4

    aput v23, v0, v22

    const/16 v22, -0x60c9

    aput v22, v0, v21

    const/16 v21, -0x61

    aput v21, v0, v20

    const/16 v20, -0x69

    aput v20, v0, v19

    const/16 v19, 0x4f19

    aput v19, v0, v18

    const/16 v18, 0x554f

    aput v18, v0, v17

    const/16 v17, -0x6cab

    aput v17, v0, v16

    const/16 v16, -0x6d

    aput v16, v0, v15

    const/16 v15, 0x37d

    aput v15, v0, v14

    const/16 v14, -0x3cfd

    aput v14, v0, v13

    const/16 v13, -0x3d

    aput v13, v0, v12

    const/16 v12, -0x52c5

    aput v12, v0, v11

    const/16 v11, -0x53

    aput v11, v0, v10

    const/16 v10, -0x41

    aput v10, v0, v9

    const/16 v9, -0x9

    aput v9, v0, v8

    const/16 v8, 0x2a22

    aput v8, v0, v7

    const/16 v7, -0x45d6

    aput v7, v0, v6

    const/16 v6, -0x46

    aput v6, v0, v5

    const/16 v5, 0x3346

    aput v5, v0, v4

    const/16 v4, -0x3bcd

    aput v4, v0, v3

    const/16 v3, -0x3c

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .locals 15

    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->path:Ljava/lang/ref/WeakReference;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->path:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->msc:Landroid/media/MediaScannerConnection;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->path:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x5

    const/4 v3, 0x6

    const/4 v6, 0x7

    const/16 v7, 0x8

    const/16 v8, 0x9

    const/16 v9, 0x3d0b

    aput v9, v2, v8

    const/16 v8, -0x6ba8

    aput v8, v2, v7

    const/16 v7, -0x1c

    aput v7, v2, v6

    const/16 v6, 0x1b1b

    aput v6, v2, v3

    const/16 v3, 0x4f34

    aput v3, v2, v1

    const/16 v1, -0x1fd6

    aput v1, v2, v14

    const/16 v1, -0x79

    aput v1, v2, v13

    const/16 v1, -0x76

    aput v1, v2, v12

    const/16 v1, -0x16da

    aput v1, v2, v11

    const/16 v1, -0x80

    aput v1, v2, v4

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x5

    const/4 v6, 0x6

    const/4 v7, 0x7

    const/16 v8, 0x8

    const/16 v9, 0x9

    const/16 v10, 0x3d6c

    aput v10, v1, v9

    const/16 v9, -0x6bc3

    aput v9, v1, v8

    const/16 v8, -0x6c

    aput v8, v1, v7

    const/16 v7, 0x1b71

    aput v7, v1, v6

    const/16 v6, 0x4f1b

    aput v6, v1, v3

    const/16 v3, -0x1fb1

    aput v3, v1, v14

    const/16 v3, -0x20

    aput v3, v1, v13

    const/16 v3, -0x15

    aput v3, v1, v12

    const/16 v3, -0x16b5

    aput v3, v1, v11

    const/16 v3, -0x17

    aput v3, v1, v4

    move v3, v4

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    :goto_1
    array-length v3, v1

    if-lt v4, v3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_0
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    aget v3, v2, v4

    int-to-char v3, v3

    aput-char v3, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->mContext:Ljava/lang/ref/WeakReference;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3

    :try_start_2
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaPopup(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    :goto_2
    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->access$300()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->aP31uMniye()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->share_image_storage_error:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;->msc:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_2
.end method
