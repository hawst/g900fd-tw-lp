.class public Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "ExerciseProActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$ExerciseImageScaleCopyTask;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final TEMP_IMAGE_PREFIX:Ljava/lang/String; = "temp_"

.field private static final TEMP_IMAGE_TIME_FORMAT:Ljava/lang/String; = "HH_mm_ss_SSS"

.field public static chartFlag:Z

.field public static isGPSLogFlag:Z

.field static isShowGraphFrag:Z

.field public static mLastTakePhoto:Ljava/lang/String;

.field private static mPlayServicePopupDisplayed:Z


# instance fields
.field private IMAGE_CAPTURE_DATA:Ljava/lang/String;

.field private IMAGE_CAPTURE_URI:Ljava/lang/String;

.field private final RRECEIVER_CONTSTANT:I

.field actionBarButtonAccListener:Landroid/view/View$OnClickListener;

.field actionBarButtonCameraListener:Landroid/view/View$OnClickListener;

.field actionBarButtonHRMListener:Landroid/view/View$OnClickListener;

.field actionBarButtonLogListener:Landroid/view/View$OnClickListener;

.field actionBarButtonMapListener:Landroid/view/View$OnClickListener;

.field actionBarButtonShareViaListener:Landroid/view/View$OnClickListener;

.field actionBarUpButtonListener:Landroid/view/View$OnClickListener;

.field baseTypeUpButtonResId:I

.field private doNotShowCheckBoxLocation:Landroid/widget/CheckBox;

.field generalTypeUpButtonResId:I

.field private images:[I

.field private isCameraOn:Z

.field public isChinaLocationConfirmed:Z

.field private isHRMConnected:Z

.field public isMeasureCompleted:Z

.field private isOpenedFromNotification:Z

.field private isShareViaRunning:Z

.field mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

.field private mBTBroadcast:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

.field mChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

.field mChosenItemListener:Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

.field mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

.field private mDeviceTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mExerciseProActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

.field private mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

.field mFitnessFragment:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

.field public mFragmentHandler:Landroid/os/Handler;

.field mGraphState:I

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mInfoDialog:Landroid/app/Dialog;

.field private mInfoView:Landroid/view/View;

.field private mIsMapEnable:Z

.field mKcalFragment:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

.field private mMenu:Landroid/view/Menu;

.field mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

.field private mServiceConnection:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

.field private mShowAgain:Landroid/widget/CheckBox;

.field private mShowAgainCheckLayout:Landroid/widget/LinearLayout;

.field mState:I

.field mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

.field private mVideoView:Landroid/widget/ImageView;

.field mWokroutState:I

.field okOncliclListener:Landroid/view/View$OnClickListener;

.field private original_file:Ljava/io/File;

.field private photo:Landroid/graphics/Bitmap;

.field private uiThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 123
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->TAG:Ljava/lang/String;

    .line 125
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mPlayServicePopupDisplayed:Z

    .line 128
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isGPSLogFlag:Z

    .line 169
    sput-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShowGraphFrag:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 124
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->RRECEIVER_CONTSTANT:I

    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mIsMapEnable:Z

    .line 127
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mMenu:Landroid/view/Menu;

    .line 129
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->uiThreadHandler:Landroid/os/Handler;

    .line 140
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    .line 144
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mWokroutState:I

    .line 145
    const/16 v0, 0x3ec

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mGraphState:I

    .line 147
    const v0, 0x7f020852

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->baseTypeUpButtonResId:I

    .line 148
    const v0, 0x7f020851

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->generalTypeUpButtonResId:I

    .line 153
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isChinaLocationConfirmed:Z

    .line 156
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isHRMConnected:Z

    .line 157
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isCameraOn:Z

    .line 158
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isOpenedFromNotification:Z

    .line 168
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isMeasureCompleted:Z

    .line 170
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShareViaRunning:Z

    .line 172
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 174
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->photo:Landroid/graphics/Bitmap;

    .line 175
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 176
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 180
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->images:[I

    .line 790
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarUpButtonListener:Landroid/view/View$OnClickListener;

    .line 809
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonLogListener:Landroid/view/View$OnClickListener;

    .line 819
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonMapListener:Landroid/view/View$OnClickListener;

    .line 875
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonAccListener:Landroid/view/View$OnClickListener;

    .line 884
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonCameraListener:Landroid/view/View$OnClickListener;

    .line 897
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonHRMListener:Landroid/view/View$OnClickListener;

    .line 922
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonShareViaListener:Landroid/view/View$OnClickListener;

    .line 1362
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mServiceConnection:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    .line 1594
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$17;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->okOncliclListener:Landroid/view/View$OnClickListener;

    .line 1758
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFragmentHandler:Landroid/os/Handler;

    return-void

    .line 180
    nop

    :array_0
    .array-data 4
        0x7f020173
        0x7f020174
        0x7f0201fa
        0x7f0201fb
        0x7f0201fc
        0x7f0201fd
        0x7f0201fe
        0x7f0201ff
        0x7f0201fe
        0x7f0201ff
    .end array-data
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->uiThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShareViaRunning:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShareViaRunning:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isDrawerMenuShown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->doNotShowCheckBoxLocation:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->doNotShowCheckBoxLocation:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 120
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->saveInfoAndUpdateViewGrid(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isDrawerMenuShown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mIsMapEnable:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->launchConnectivityActivity()V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isCameraOn:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->doTakePhotoAction()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showInfomationDialog()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V

    return-void
.end method

.method private addTransparentFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;Ljava/lang/String;)V
    .locals 2
    .param p1, "_fragment"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 1748
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 1749
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f080091

    invoke-virtual {v0, v1, p1, p2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 1750
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 1751
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1752
    return-void
.end method

.method private createSaveCropFile()Landroid/net/Uri;
    .locals 9

    .prologue
    .line 1853
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1854
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 1855
    .local v3, "time":J
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v7, "HH_mm_ss_SSS"

    invoke-direct {v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1856
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "temp_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1857
    .local v6, "url":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/SHealth/Exercise/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1858
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1859
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 1860
    .local v5, "uri":Landroid/net/Uri;
    return-object v5
.end method

.method private deleteTempFile()V
    .locals 2

    .prologue
    .line 1839
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 1841
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1843
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1845
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1848
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private doTakePhotoAction()V
    .locals 3

    .prologue
    .line 1828
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->deleteTempFile()V

    .line 1829
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1830
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1831
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1832
    const-string/jumbo v1, "return-data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1833
    const/16 v1, 0x82

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1834
    return-void
.end method

.method private finishAndExerciseMateLunch()V
    .locals 3

    .prologue
    .line 1333
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1334
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth.exercisemate.command.exercisemate"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1335
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1336
    const-string v1, "isWokoutFinished"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1347
    return-void
.end method

.method private getActionbarBGDrawable()I
    .locals 3

    .prologue
    .line 775
    const v0, 0x7f020023

    .line 776
    .local v0, "resId":I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 777
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getCurrentMode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 778
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v1

    const/16 v2, 0x7d0

    if-eq v1, v2, :cond_0

    .line 779
    const v0, 0x7f020251

    .line 787
    :cond_0
    :goto_0
    return v0

    .line 782
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 783
    const v0, 0x7f020251

    goto :goto_0
.end method

.method private informationPopup()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 580
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 581
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 582
    const v1, 0x7f0900e3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 584
    const v1, 0x7f0300bb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 585
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 593
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 594
    return-void
.end method

.method private initFragmentSelection(Landroid/os/Bundle;Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isShowGraphFrag"    # Ljava/lang/Boolean;

    .prologue
    const/16 v2, 0x3e9

    .line 341
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    const/16 v1, 0x3ec

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showFragmentSeletion(I)V

    .line 368
    :goto_0
    return-void

    .line 345
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v1, :cond_3

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mWokroutState:I

    .line 349
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isWorkingOut()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 353
    if-eqz p1, :cond_1

    .line 354
    const-string v1, "currnet_fragment_state"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 355
    .local v0, "state":I
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showFragmentSeletion(I)V

    goto :goto_0

    .line 357
    .end local v0    # "state":I
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showFragmentSeletion(I)V

    goto :goto_0

    .line 361
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showFragmentSeletion(I)V

    goto :goto_0

    .line 365
    :cond_3
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showFragmentSeletion(I)V

    goto :goto_0
.end method

.method private initInfoVideo()V
    .locals 5

    .prologue
    .line 1698
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f08056b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mVideoView:Landroid/widget/ImageView;

    .line 1699
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mVideoView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->images:[I

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->animate(Landroid/widget/ImageView;[IIZ)V

    .line 1700
    return-void
.end method

.method private internalHrmAvailable()Z
    .locals 7

    .prologue
    .line 756
    const/4 v1, 0x0

    .line 758
    .local v1, "internalHrmAvailable":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getShealthDeviceFinderC()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 760
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getShealthDeviceFinderC()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    move-result-object v2

    const/4 v3, 0x4

    const/16 v4, 0x2718

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->isAvailable(IIILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 771
    :cond_0
    :goto_0
    return v1

    .line 763
    :catch_0
    move-exception v0

    .line 764
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 765
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 766
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 767
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 768
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private isCountryModel(Ljava/lang/String;)Z
    .locals 2
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 1386
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 1387
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isNetworkOnline()Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1807
    :try_start_0
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1809
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    .line 1810
    .local v3, "wifi":Landroid/net/NetworkInfo$State;
    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v3, v6, :cond_0

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    if-ne v3, v6, :cond_2

    :cond_0
    move v4, v5

    .line 1822
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v3    # "wifi":Landroid/net/NetworkInfo$State;
    :cond_1
    :goto_0
    return v4

    .line 1814
    .restart local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .restart local v3    # "wifi":Landroid/net/NetworkInfo$State;
    :cond_2
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    .line 1815
    .local v2, "mobile":Landroid/net/NetworkInfo$State;
    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v2, v6, :cond_3

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v2, v6, :cond_1

    :cond_3
    move v4, v5

    .line 1816
    goto :goto_0

    .line 1819
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v2    # "mobile":Landroid/net/NetworkInfo$State;
    .end local v3    # "wifi":Landroid/net/NetworkInfo$State;
    :catch_0
    move-exception v1

    .line 1820
    .local v1, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method private isWorkingOut()Z
    .locals 3

    .prologue
    .line 371
    const/4 v0, 0x0

    .line 372
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getCurrentMode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 373
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v1

    const/16 v2, 0x7d0

    if-eq v1, v2, :cond_0

    .line 374
    const/4 v0, 0x1

    .line 379
    :cond_0
    :goto_0
    return v0

    .line 376
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording()Z

    move-result v0

    goto :goto_0
.end method

.method private launchConnectivityActivity()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    .line 1026
    new-instance v3, Landroid/content/Intent;

    const-class v6, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {v3, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1027
    .local v3, "intent":Landroid/content/Intent;
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DATA_TYPE_KEY:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1030
    new-array v1, v9, [I

    fill-array-data v1, :array_0

    .line 1036
    .local v1, "contentIds":[I
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mDeviceTypes:Ljava/util/ArrayList;

    if-nez v6, :cond_0

    .line 1037
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mDeviceTypes:Ljava/util/ArrayList;

    .line 1040
    :cond_0
    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    const/16 v7, 0x3e9

    if-ne v6, v7, :cond_1

    .line 1041
    sget-boolean v6, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v6, :cond_2

    .line 1042
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v6, :cond_1

    .line 1043
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getWorkoutController()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1044
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getWorkoutController()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->stopScanning()V

    .line 1052
    :cond_1
    :goto_0
    new-array v5, v9, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090024

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090025

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 1055
    .local v5, "tabsContent":[Ljava/lang/String;
    new-array v4, v9, [I

    fill-array-data v4, :array_1

    .line 1059
    .local v4, "subTabsContent":[I
    new-array v2, v9, [I

    fill-array-data v2, :array_2

    .line 1063
    .local v2, "headerTxtResIds":[I
    const v0, 0x7f090186

    .line 1065
    .local v0, "actionBarTitleResId":I
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v6, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isWorkingOut()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1066
    const/4 v5, 0x0

    .line 1067
    const/4 v4, 0x0

    .line 1068
    const v0, 0x7f090023

    .line 1069
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v7, 0x7f0909e0

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1070
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPE_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v7, 0x7f090a9e

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1071
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->PAIRED_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v7, 0x7f090a9f

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1075
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mDeviceTypes:Ljava/util/ArrayList;

    const/16 v7, 0x2718

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1078
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_SUB_TABS_INFO:Ljava/lang/String;

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1079
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->SCANNING_SUB_TABS_INFO_RES_IDS_KEY:Ljava/lang/String;

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 1080
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->DEVICE_TYPES_KEY:Ljava/lang/String;

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mDeviceTypes:Ljava/util/ArrayList;

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1081
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->HEADER_TEXT_RES_ID_KEYS:Ljava/lang/String;

    invoke-virtual {v3, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 1082
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->ACTION_BAR_TITLE_RES_ID:Ljava/lang/String;

    invoke-virtual {v3, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1094
    const/16 v6, 0x7b

    invoke-virtual {p0, v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1096
    return-void

    .line 1046
    .end local v0    # "actionBarTitleResId":I
    .end local v2    # "headerTxtResIds":[I
    .end local v4    # "subTabsContent":[I
    .end local v5    # "tabsContent":[Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v6, :cond_1

    .line 1047
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->getWorkoutController()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1048
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->getWorkoutController()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->stopScanning()V

    goto/16 :goto_0

    .line 1073
    .restart local v0    # "actionBarTitleResId":I
    .restart local v2    # "headerTxtResIds":[I
    .restart local v4    # "subTabsContent":[I
    .restart local v5    # "tabsContent":[Ljava/lang/String;
    :cond_3
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->NO_DEVICES_TEXT_RES_ID_KEY:Ljava/lang/String;

    const v7, 0x7f090092

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1030
    nop

    :array_0
    .array-data 4
        0x7f0300b3
        0x7f0300b4
    .end array-data

    .line 1055
    :array_1
    .array-data 4
        0x7f090024
        0x7f090025
    .end array-data

    .line 1059
    :array_2
    .array-data 4
        0x7f090ae8
        0x7f090fba
    .end array-data
.end method

.method private saveInfoAndUpdateViewGrid(Ljava/lang/String;J)V
    .locals 2
    .param p1, "picturePath"    # Ljava/lang/String;
    .param p2, "exerciseId"    # J

    .prologue
    .line 1579
    if-eqz p1, :cond_0

    .line 1580
    new-instance v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>(Ljava/lang/String;)V

    .line 1581
    .local v0, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {v0, p2, p3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setExerciseId(J)V

    .line 1582
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->insertTempitem(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Z)J

    .line 1584
    .end local v0    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    :cond_0
    return-void
.end method

.method private setItemVisible(IZ)V
    .locals 2
    .param p1, "size"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 482
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 483
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mMenu:Landroid/view/Menu;

    invoke-interface {v1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 482
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 485
    :cond_0
    return-void
.end method

.method private setVisivibilityBasedOnRtl()V
    .locals 24

    .prologue
    .line 1604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    if-nez v20, :cond_0

    .line 1655
    :goto_0
    return-void

    .line 1607
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08056c

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 1608
    .local v10, "oneView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08056f

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    .line 1609
    .local v18, "twoView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080572

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 1610
    .local v15, "threeView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080575

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 1611
    .local v8, "fourView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080578

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1612
    .local v6, "fiveView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08057b

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 1614
    .local v13, "sixView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08056e

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 1615
    .local v11, "oneViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080571

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    .line 1616
    .local v19, "twoViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080574

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 1617
    .local v16, "threeViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080577

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 1618
    .local v9, "fourViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08057a

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1619
    .local v7, "fiveViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f08057d

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 1620
    .local v14, "sixViewForRtl":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080557

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 1621
    .local v12, "showagaintextview":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    move-object/from16 v20, v0

    const v21, 0x7f080568

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 1623
    .local v17, "titletextview":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    .line 1624
    .local v5, "config":Landroid/content/res/Configuration;
    invoke-virtual {v5}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    .line 1625
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1626
    const/16 v20, 0x8

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1627
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1628
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1629
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1630
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1631
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1632
    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    .line 1633
    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1634
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1635
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1636
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1637
    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0a058e

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v22

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1638
    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0a058e

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v22

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0

    .line 1640
    :cond_1
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1641
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1642
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1643
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1644
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1645
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1646
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1647
    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    .line 1648
    const/16 v20, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1649
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1650
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1651
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1652
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a058e

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1653
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0
.end method

.method private showInfomationDialog()V
    .locals 4

    .prologue
    .line 1658
    const-string v2, "layout_inflater"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1659
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030146

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    .line 1660
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setVisivibilityBasedOnRtl()V

    .line 1661
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->initInfoVideo()V

    .line 1662
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v3, 0x7f0804ee

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mShowAgainCheckLayout:Landroid/widget/LinearLayout;

    .line 1663
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v3, 0x7f0804ef

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mShowAgain:Landroid/widget/CheckBox;

    .line 1664
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mShowAgainCheckLayout:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$18;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$18;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1672
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v3, 0x7f08056a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1675
    .local v1, "okButton":Landroid/widget/Button;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->okOncliclListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1678
    new-instance v2, Landroid/app/Dialog;

    const v3, 0x7f0c0081

    invoke-direct {v2, p0, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    .line 1679
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 1680
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$19;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1694
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 1695
    return-void
.end method

.method private showWorkoutStopConfirmPopup()V
    .locals 5

    .prologue
    .line 1409
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    const v3, 0x7f090ab6

    invoke-direct {v0, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    .line 1411
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090ab7

    .line 1412
    .local v1, "message":I
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1413
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1426
    const v2, 0x7f09004e

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1428
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1439
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "RESET_DATA_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1440
    return-void
.end method


# virtual methods
.method public FroceRecreateActionbar()V
    .locals 0

    .prologue
    .line 1183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->invalidateOptionsMenu()V

    .line 1184
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->customizeActionBar()V

    .line 1185
    return-void
.end method

.method protected customizeActionBar()V
    .locals 14

    .prologue
    .line 631
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 632
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 633
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v6

    const v7, 0x7f080304

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 634
    .local v13, "view":Landroid/view/View;
    if-eqz v13, :cond_0

    .line 635
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarUpButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v13, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 636
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionbarBGDrawable()I

    move-result v6

    invoke-virtual {v13, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 637
    invoke-virtual {v13}, Landroid/view/View;->clearFocus()V

    .line 638
    const/4 v6, 0x0

    invoke-virtual {v13, v6}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 640
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090021

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 642
    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mWokroutState:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mWokroutState:I

    const/16 v7, 0x7d0

    if-eq v6, v7, :cond_3

    .line 643
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 648
    :goto_0
    const/4 v11, 0x1

    .line 649
    .local v11, "isHrSupported":Z
    sget-object v6, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v6}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    if-ne v6, v7, :cond_1

    .line 650
    const/4 v11, 0x0

    .line 655
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v1, 0x7f0207c0

    const/4 v2, 0x0

    const v3, 0x7f09005a

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonLogListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionbarBGDrawable()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 657
    .local v0, "actionButton0":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f020252

    const/4 v3, 0x0

    const v4, 0x7f09021e

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonMapListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionbarBGDrawable()I

    move-result v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 660
    .local v1, "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f0207bc

    const/4 v4, 0x0

    const v5, 0x7f0907b4

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonAccListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionbarBGDrawable()I

    move-result v7

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 668
    .local v2, "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v4, 0x7f020250

    const/4 v5, 0x0

    const v6, 0x7f0900df

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonCameraListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionbarBGDrawable()I

    move-result v8

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 671
    .local v3, "actionButton4":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->internalHrmAvailable()Z

    move-result v12

    .line 672
    .local v12, "isInternalHrmAvailable":Z
    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f020382

    const/4 v6, 0x0

    const v7, 0x7f090022

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonHRMListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionbarBGDrawable()I

    move-result v9

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 674
    .local v4, "actionButton5":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    new-instance v5, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v6, 0x7f0207cf

    const/4 v7, 0x0

    const v8, 0x7f090033

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->actionBarButtonShareViaListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionbarBGDrawable()I

    move-result v10

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;I)V

    .line 677
    .local v5, "actionButton6":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    instance-of v6, v6, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    if-eqz v6, :cond_4

    .line 678
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 679
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarUpButton()Landroid/widget/ImageButton;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->baseTypeUpButtonResId:I

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 680
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setDrawerLockMode(I)V

    .line 685
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    .line 686
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 744
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 747
    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    const/16 v7, 0x3ec

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    instance-of v6, v6, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    if-eqz v6, :cond_2

    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->isDataPresent()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 749
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 752
    .end local v0    # "actionButton0":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v1    # "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v2    # "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v3    # "actionButton4":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v4    # "actionButton5":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v5    # "actionButton6":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v11    # "isHrSupported":Z
    .end local v12    # "isInternalHrmAvailable":Z
    .end local v13    # "view":Landroid/view/View;
    :cond_2
    return-void

    .line 645
    .restart local v13    # "view":Landroid/view/View;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 688
    .restart local v0    # "actionButton0":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .restart local v1    # "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .restart local v2    # "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .restart local v3    # "actionButton4":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .restart local v4    # "actionButton5":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .restart local v5    # "actionButton6":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .restart local v11    # "isHrSupported":Z
    .restart local v12    # "isInternalHrmAvailable":Z
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 689
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getCurrentMode()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_7

    .line 690
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v6

    const/16 v7, 0x7d0

    if-ne v6, v7, :cond_5

    .line 691
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 692
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarUpButton()Landroid/widget/ImageButton;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->baseTypeUpButtonResId:I

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 693
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setDrawerLockMode(I)V

    .line 697
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    goto/16 :goto_1

    .line 699
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarUpButton()Landroid/widget/ImageButton;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->generalTypeUpButtonResId:I

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 700
    if-eqz v12, :cond_6

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isHRMConnected:Z

    if-nez v6, :cond_6

    if-eqz v11, :cond_6

    .line 701
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 702
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 703
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 704
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setDrawerLockMode(I)V

    .line 710
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    goto/16 :goto_1

    .line 713
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRecording()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 714
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 715
    if-eqz v12, :cond_8

    if-eqz v11, :cond_8

    .line 716
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 718
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarUpButton()Landroid/widget/ImageButton;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->generalTypeUpButtonResId:I

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 720
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setDrawerLockMode(I)V

    .line 726
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    goto/16 :goto_1

    .line 728
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 729
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarUpButton()Landroid/widget/ImageButton;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->baseTypeUpButtonResId:I

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 730
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->setDrawerLockMode(I)V

    .line 735
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    goto/16 :goto_1

    .line 739
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 740
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarUpButton()Landroid/widget/ImageButton;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->baseTypeUpButtonResId:I

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 741
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setActionbarDescription(Z)V

    goto/16 :goto_1
.end method

.method public getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 1016
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1755
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1493
    const-string v1, "location_dialog"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1494
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$15;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    .line 1527
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1395
    const/4 v2, 0x0

    .line 1398
    .local v2, "countryCode":Ljava/lang/String;
    :try_start_0
    const-string v5, "android.os.SystemProperties"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 1399
    .local v1, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "get"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 1400
    .local v4, "method":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "ro.csc.countryiso_code"

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1404
    .end local v1    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "method":Ljava/lang/reflect/Method;
    :goto_0
    return-object v2

    .line 1401
    :catch_0
    move-exception v3

    .line 1402
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1532
    const-string v0, "location_dialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1533
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$16;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    .line 1553
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 598
    const-string v0, "com.sec.shealth.help.action.WORK_OUT"

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1322
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    return-object v0
.end method

.method public hideDrawMenu()V
    .locals 0

    .prologue
    .line 619
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->hideDrawMenu()V

    .line 620
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->invalidateOptionsMenu()V

    .line 621
    return-void
.end method

.method public isDrawerMenuVisible()Z
    .locals 1

    .prologue
    .line 1864
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isDrawerMenuShown()Z

    move-result v0

    return v0
.end method

.method protected isNinePopup()Z
    .locals 11

    .prologue
    const/16 v10, 0x9

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 961
    const/4 v7, 0x0

    .line 962
    .local v7, "imageCount":I
    const/4 v6, 0x0

    .line 965
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exercise__id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeExerciseId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 967
    if-eqz v6, :cond_0

    .line 968
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 971
    :cond_0
    if-eqz v6, :cond_1

    .line 972
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 975
    :cond_1
    if-ge v7, v10, :cond_3

    move v0, v8

    .line 981
    :goto_0
    return v0

    .line 971
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 972
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 979
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090f73

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v9}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v9

    .line 981
    goto :goto_0
.end method

.method public isUSAModel()Z
    .locals 1

    .prologue
    .line 1391
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->USA:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isCountryModel(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 1100
    const/4 v0, 0x0

    .line 1101
    .local v0, "flagimagaeAdded":Z
    const/16 v1, 0x7b

    if-ne p1, v1, :cond_2

    .line 1102
    if-ne p2, v2, :cond_0

    .line 1165
    :cond_0
    :goto_0
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    const/16 v2, 0x3e9

    if-ne v1, v2, :cond_9

    if-nez v0, :cond_9

    .line 1166
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v1, :cond_8

    .line 1167
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v1, :cond_1

    .line 1168
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->setImageCaptureUri(Landroid/net/Uri;)V

    .line 1169
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1180
    :cond_1
    :goto_1
    return-void

    .line 1104
    :cond_2
    const/16 v1, 0x7e

    if-ne p1, v1, :cond_3

    .line 1105
    if-ne p2, v2, :cond_1

    .line 1106
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->finishAndExerciseMateLunch()V

    goto :goto_1

    .line 1109
    :cond_3
    const/16 v1, 0x7f

    if-ne p1, v1, :cond_4

    .line 1110
    if-ne p2, v2, :cond_1

    .line 1111
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->finishAndExerciseMateLunch()V

    goto :goto_1

    .line 1114
    :cond_4
    const/16 v1, 0x3039

    if-ne p1, v1, :cond_5

    .line 1115
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto :goto_1

    .line 1117
    :cond_5
    const/16 v1, 0x84

    if-ne p1, v1, :cond_6

    .line 1118
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    if-eqz v1, :cond_1

    .line 1120
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    const/16 v2, 0x3ec

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    instance-of v1, v1, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    if-eqz v1, :cond_1

    .line 1121
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->updateGraphFragment()V

    goto :goto_1

    .line 1124
    :cond_6
    const/16 v1, 0x83

    if-ne p1, v1, :cond_7

    .line 1125
    if-ne p2, v2, :cond_1

    .line 1127
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090adf

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1130
    :cond_7
    const/16 v1, 0x82

    if-ne p1, v1, :cond_0

    goto :goto_0

    .line 1171
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v1, :cond_1

    .line 1172
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->setImageCaptureUri(Landroid/net/Uri;)V

    .line 1173
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1

    .line 1175
    :cond_9
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_1

    .line 1176
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    if-eqz v1, :cond_1

    .line 1177
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/16 v2, 0x3e9

    .line 1274
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShareViaRunning:Z

    .line 1279
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isScreenLockState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1318
    :cond_0
    :goto_0
    return-void

    .line 1282
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .line 1284
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v0

    const/16 v1, 0x7d0

    if-eq v0, v1, :cond_6

    .line 1285
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    if-ne v0, v2, :cond_0

    .line 1286
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v0, :cond_3

    .line 1287
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->pauseWorkoutOnBackPress()V

    .line 1291
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->isWorkoutMinIntervalOver()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1292
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->showMinWorkoutIntervalPopup()V

    goto :goto_0

    .line 1288
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v0, :cond_2

    .line 1289
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->pauseWorkoutOnBackPress()V

    goto :goto_1

    .line 1293
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isWorkoutMinIntervalOver()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1294
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->showMinWorkoutIntervalPopup()V

    goto :goto_0

    .line 1296
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showWorkoutStopConfirmPopup()V

    goto :goto_0

    .line 1301
    :cond_6
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    if-eq v0, v2, :cond_a

    .line 1302
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShowGraphFrag:Z

    if-eqz v0, :cond_9

    .line 1303
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v0, :cond_7

    .line 1304
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->onBackPressed()V

    .line 1305
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v0, :cond_8

    .line 1306
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onBackPressed()V

    .line 1307
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->finish()V

    goto :goto_0

    .line 1309
    :cond_9
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showFragmentSeletion(I)V

    goto :goto_0

    .line 1311
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v0, :cond_b

    .line 1312
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->onBackPressed()V

    goto/16 :goto_0

    .line 1313
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v0, :cond_0

    .line 1314
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->onBackPressed()V

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 991
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1202
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->clearAndRefreshDrawerMenu()V

    .line 1203
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1212
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1213
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->updateStringOnLocaleChange()V

    .line 1214
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 189
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 190
    iput-object p0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProActivity:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "less_than_2min_tag"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 193
    .local v0, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 198
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mServiceConnection:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->setHealSerivceListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;)V

    .line 199
    sget-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v4, :cond_4

    .line 200
    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .line 205
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->doBindRealtimeService(Landroid/content/Context;)V

    .line 206
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "show_graph_fragment"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    sput-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShowGraphFrag:Z

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "ExerciseProHRMFragment"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isWorkingOut()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 216
    sput-boolean v6, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShowGraphFrag:Z

    .line 217
    :cond_1
    sget-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShowGraphFrag:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->initFragmentSelection(Landroid/os/Bundle;Ljava/lang/Boolean;)V

    .line 219
    sget-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v4, :cond_5

    .line 220
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v4, :cond_2

    .line 221
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->isHRMConnected()Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isHRMConnected:Z

    .line 226
    :cond_2
    :goto_1
    sget-boolean v4, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v4, :cond_6

    .line 228
    :try_start_0
    invoke-static {p0}, Lcom/amap/api/maps2d/MapsInitializer;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    :goto_2
    new-instance v2, Landroid/content/IntentFilter;

    const-string v4, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 240
    .local v2, "lFilter":Landroid/content/IntentFilter;
    const-string v4, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 241
    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$1;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mBTBroadcast:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    .line 242
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mBTBroadcast:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 244
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    if-eqz v4, :cond_3

    .line 245
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFragmentHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->setFragmentHandler(Landroid/os/Handler;)V

    .line 246
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 247
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    if-eqz v3, :cond_3

    .line 248
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 249
    const v4, 0x7f080091

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const-string v6, "ExerciseProHRMFragment"

    invoke-virtual {v3, v4, v5, v6}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 250
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 253
    .end local v3    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_3
    return-void

    .line 202
    .end local v2    # "lFilter":Landroid/content/IntentFilter;
    :cond_4
    new-instance v4, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    goto/16 :goto_0

    .line 223
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v4, :cond_2

    .line 224
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->isHRMConnected()Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isHRMConnected:Z

    goto :goto_1

    .line 229
    :catch_0
    move-exception v1

    .line 230
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 234
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_6
    :try_start_1
    invoke-static {p0}, Lcom/google/android/gms/maps/MapsInitializer;->initialize(Landroid/content/Context;)V
    :try_end_1
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 235
    :catch_1
    move-exception v1

    .line 236
    .local v1, "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    invoke-virtual {v1}, Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;->printStackTrace()V

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v4, 0x3e9

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 490
    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    if-ne v2, v4, :cond_5

    .line 491
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportHRM()Z

    move-result v2

    if-nez v2, :cond_3

    .line 492
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f10000d

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 501
    :goto_0
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mMenu:Landroid/view/Menu;

    .line 502
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .line 503
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v2

    const/16 v3, 0x7d0

    if-ne v2, v3, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isDrawerMenuShown()Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    if-eq v2, v4, :cond_6

    .line 505
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    :cond_2
    move v0, v1

    .line 514
    :goto_1
    return v0

    .line 493
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isUSAModel()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 494
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f10000b

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 496
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f10000c

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 499
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f10000e

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto :goto_0

    .line 509
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-nez v2, :cond_7

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_1

    .line 512
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 1256
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 1257
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mBTBroadcast:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1258
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->doUnbindRealtimeService(Landroid/content/Context;)V

    .line 1259
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->recursiveRecycle(Landroid/view/View;)V

    .line 1260
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFragmentHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1261
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFragmentHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1262
    :cond_0
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShareViaRunning:Z

    .line 1265
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v0

    const/16 v1, 0x7d0

    if-ne v0, v1, :cond_1

    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopHRM()V

    .line 1267
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopFE()V

    .line 1268
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->accuracyLevel:F

    .line 1270
    :cond_1
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1352
    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    .line 1354
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1359
    :goto_0
    return v0

    .line 1356
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isScreenLockState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1357
    const/4 v0, 0x1

    goto :goto_0

    .line 1359
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLogSelected()V
    .locals 3

    .prologue
    .line 1381
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseMateLogActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1382
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x84

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1383
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 257
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 258
    const-string v0, "exercisenotify"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "exercisenotify"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isCameraOn:Z

    if-eqz v0, :cond_0

    .line 259
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isOpenedFromNotification:Z

    .line 260
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->doTakePhotoAction()V

    .line 262
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 520
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 575
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 523
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMaxHRModeSelectActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 524
    .local v0, "intent":Landroid/content/Intent;
    const/16 v2, 0x83

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 528
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->informationPopup()V

    goto :goto_0

    .line 532
    :sswitch_2
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 536
    :sswitch_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.plugins.exercisepro"

    const-string v4, "P039"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    goto :goto_0

    .line 520
    :sswitch_data_0
    .sparse-switch
        0x7f080022 -> :sswitch_2
        0x7f08036a -> :sswitch_1
        0x7f080c8a -> :sswitch_3
        0x7f080c96 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 1251
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPause()V

    .line 1252
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 986
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 987
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 456
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->updateMenuItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    const/4 v0, 0x1

    .line 459
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 625
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 626
    const-string v0, "currnet_fragment_state"

    const/16 v1, 0x3e9

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    .line 627
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 1233
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 1234
    const v0, 0x7f090021

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setTitle(I)V

    .line 1235
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .line 1236
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mVideoView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1237
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->initInfoVideo()V

    .line 1239
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShareViaRunning:Z

    .line 1240
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 607
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 608
    const-string v0, "currnet_fragment_state"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 609
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 265
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onStart()V

    .line 266
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isOpenedFromNotification:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isCameraOn:Z

    .line 267
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isOpenedFromNotification:Z

    .line 268
    return-void
.end method

.method public reCreateActionbar(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 1188
    const/16 v0, 0x7d0

    if-ne p1, v0, :cond_0

    .line 1189
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarUpButton()Landroid/widget/ImageButton;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->baseTypeUpButtonResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1192
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->invalidateOptionsMenu()V

    .line 1193
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mWokroutState:I

    if-ne v0, p1, :cond_1

    .line 1198
    :goto_1
    return-void

    .line 1191
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarUpButton()Landroid/widget/ImageButton;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->generalTypeUpButtonResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1196
    :cond_1
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mWokroutState:I

    .line 1197
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->customizeActionBar()V

    goto :goto_1
.end method

.method protected resetAllDataConfirm()V
    .locals 5

    .prologue
    .line 1444
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    const v3, 0x7f090032

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    .line 1446
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v0, 0x7f090062

    .line 1447
    .local v0, "alertTextResId":I
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1448
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$14;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1485
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "RESET_DATA_DIALOG"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1486
    return-void
.end method

.method public setActionbarDescription(Z)V
    .locals 9
    .param p1, "isBaseMode"    # Z

    .prologue
    const v8, 0x7f09020e

    const v6, 0x7f09020b

    const v7, 0x7f080304

    .line 834
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    if-nez v4, :cond_1

    .line 873
    :cond_0
    :goto_0
    return-void

    .line 836
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->invalidate()V

    .line 837
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f080309

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 838
    .local v3, "mTitleText":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    .line 839
    .local v0, "actionBarView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 840
    if-eqz p1, :cond_4

    .line 841
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 842
    .local v2, "layout":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_0

    .line 843
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isJapanModel()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 844
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090214

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 848
    :cond_3
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 849
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090218

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 860
    .end local v2    # "layout":Landroid/widget/LinearLayout;
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 862
    .local v1, "description":Ljava/lang/StringBuilder;
    if-eqz v3, :cond_5

    .line 863
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 865
    :cond_5
    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 867
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 868
    .restart local v2    # "layout":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_0

    .line 869
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public setHRMStatus(Z)V
    .locals 2
    .param p1, "isHRMConnected"    # Z

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isHRMConnected:Z

    if-eq v0, p1, :cond_0

    .line 272
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isHRMConnected:Z

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->FroceRecreateActionbar()V

    .line 274
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v0, :cond_1

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActionBarTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 277
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->getActionBarTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_0
.end method

.method public setMapEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 1020
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mIsMapEnable:Z

    .line 1021
    return-void
.end method

.method public setOnSaveChosenItemListener(Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;)V
    .locals 0
    .param p1, "linstener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    .prologue
    .line 602
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mChosenItemListener:Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    .line 603
    return-void
.end method

.method public showDrawMenu()V
    .locals 0

    .prologue
    .line 613
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->showDrawMenu()V

    .line 614
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->invalidateOptionsMenu()V

    .line 615
    return-void
.end method

.method public showFragmentSeletion(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    const/16 v2, 0x7d0

    .line 383
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    .line 384
    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    packed-switch v1, :pswitch_data_0

    .line 392
    const/16 v1, 0x3e9

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    .line 393
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v1, :cond_3

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    instance-of v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v1, :cond_1

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-nez v1, :cond_2

    .line 400
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    .line 402
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 451
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 405
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    instance-of v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-nez v1, :cond_0

    .line 409
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-nez v1, :cond_5

    .line 410
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    .line 412
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    goto :goto_1

    .line 417
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    if-nez v1, :cond_6

    .line 418
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->chartFlag:Z

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;-><init>(Z)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    .line 422
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mChartFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProChartFragment;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    goto :goto_1

    .line 425
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v1

    if-eq v1, v2, :cond_7

    .line 426
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveUiLockState(Z)V

    .line 427
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.music.musicservicecommand.pause"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 428
    .local v0, "mediaIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->setWorkoutState(I)V

    .line 430
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->stopWorkout()V

    .line 431
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getRealtimeExerciseId()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->deleteItem(Landroid/content/Context;J)J

    .line 434
    .end local v0    # "mediaIntent":Landroid/content/Intent;
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mKcalFragment:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    if-nez v1, :cond_8

    .line 435
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mKcalFragment:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    .line 437
    :cond_8
    const/16 v1, 0x3ec

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mGraphState:I

    .line 438
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mKcalFragment:Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 439
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.plugins.exercisemate"

    const-string v3, "P035"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->customizeActionBar()V

    goto :goto_1

    .line 444
    :pswitch_2
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFitnessFragment:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    .line 445
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mFitnessFragment:Lcom/sec/android/app/shealth/plugins/exercisemate/fitness/ExerciseMateFitnessGraphFragmentSIC;

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 446
    const/16 v1, 0x3ed

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mGraphState:I

    goto/16 :goto_1

    .line 384
    nop

    :pswitch_data_0
    .packed-switch 0x3eb
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public startMapActivity()V
    .locals 6

    .prologue
    const/high16 v5, 0x24000000

    const/16 v4, 0x7f

    const/4 v3, 0x1

    .line 995
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mPlayServicePopupDisplayed:Z

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->TAG:Ljava/lang/String;

    invoke-static {v3, p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->updateGooglePlayServicesIfNeeded(ZLandroid/app/Activity;ZLjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 996
    sget-boolean v1, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v1, :cond_2

    .line 998
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 999
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1000
    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1009
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isNetworkOnline()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1011
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090f41

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1013
    :cond_1
    return-void

    .line 1003
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1004
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1005
    invoke-virtual {p0, v0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public updateMenuItem()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 463
    const/4 v0, 0x1

    .line 464
    .local v0, "ret":Z
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mMenu:Landroid/view/Menu;

    if-eqz v3, :cond_3

    .line 465
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mMenu:Landroid/view/Menu;

    invoke-interface {v3}, Landroid/view/Menu;->size()I

    move-result v1

    .line 466
    .local v1, "size":I
    if-ge v1, v5, :cond_0

    .line 478
    .end local v1    # "size":I
    :goto_0
    return v2

    .line 468
    .restart local v1    # "size":I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getWorkoutState()I

    move-result v3

    const/16 v4, 0x7d0

    if-ne v3, v4, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isDrawerMenuShown()Z

    move-result v3

    if-nez v3, :cond_2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mState:I

    const/16 v4, 0x3e9

    if-eq v3, v4, :cond_4

    .line 470
    :cond_2
    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setItemVisible(IZ)V

    .line 471
    const/4 v0, 0x0

    .end local v1    # "size":I
    :cond_3
    :goto_1
    move v2, v0

    .line 478
    goto :goto_0

    .line 473
    .restart local v1    # "size":I
    :cond_4
    invoke-direct {p0, v1, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setItemVisible(IZ)V

    .line 474
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public updateStringOnLocaleChange()V
    .locals 2

    .prologue
    .line 1216
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f08056a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1218
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f080568

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1219
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f08056d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090c17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1220
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f080570

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090c13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1221
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f080573

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090c15

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1222
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f080576

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090c16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1223
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f080579

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090c26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1224
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f08057c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090c2d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1225
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f080557

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09078c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1226
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mInfoView:Landroid/view/View;

    const v1, 0x7f080565

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090c07

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1227
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setVisivibilityBasedOnRtl()V

    .line 1229
    :cond_0
    return-void
.end method
