.class Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;
.super Ljava/lang/Object;
.source "MealActivityTopFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatePickerDialogButtonsController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$1;

    .prologue
    .line 254
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;-><init>(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 6
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 257
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v2, :cond_0

    .line 258
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_1

    .line 260
    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$800()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "trying to call btnClickListener.onClick(...) while fragment is not added"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getDateDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f080325

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    .line 268
    .local v0, "localInstance":Landroid/widget/DatePicker;
    invoke-virtual {v0}, Landroid/widget/DatePicker;->clearFocus()V

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$1100(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->setMeasureDateWithErrorCheck(III)V

    .line 270
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment$DatePickerDialogButtonsController;->this$0:Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;->access$700(Lcom/sec/android/app/shealth/food/mealinput/MealActivityTopFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/food/app/UserActionLog;->SET_MEAL_DATE_BY_SET_DATE_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v2, v3}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 271
    .end local v0    # "localInstance":Landroid/widget/DatePicker;
    :catch_0
    move-exception v1

    .line 273
    .local v1, "ne":Ljava/lang/NullPointerException;
    goto :goto_0
.end method
