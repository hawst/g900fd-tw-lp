.class public Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "FavoritesActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/IExpandableListFragmentContainer;
.implements Lcom/sec/android/app/shealth/food/foodpick/expandable/listener/OnSelectedPanelItemSelectedListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method

.method private initFoodFragment()V
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/ExpandableListFragment;-><init>()V

    .line 58
    .local v0, "myFoodFragment":Landroid/support/v4/app/Fragment;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;->prepareFragmentParams()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f080417

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 60
    return-void
.end method

.method private openEditMealActivity(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p1, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/food/mealinput/EditMealActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 45
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "MEAL_ITEM_LIST"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 46
    const-string v1, "MEAL_DATA_HOLDER"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "MEAL_DATA_HOLDER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 47
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;->startActivity(Landroid/content/Intent;)V

    .line 48
    return-void
.end method

.method private prepareFragmentParams()Landroid/os/Bundle;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 63
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v2, "params":Landroid/os/Bundle;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v1, "holdersList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;>;"
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    const v3, 0x7f090088

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;-><init>(I)V

    .line 67
    .local v0, "favoritesCategory":Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFavoriteStarsVisibility(Z)V

    .line 68
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsCheckable(Z)V

    .line 69
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setAreItemsExpandable(Z)V

    .line 70
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->specifyFavoritesCategory()V

    .line 71
    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritesCategoryCreator;-><init>()V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;->setFoodCategoryListItemsCreator(Lcom/sec/android/app/shealth/food/foodpick/expandable/IFoodCategoryListItemsCreator;)V

    .line 72
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    const-string v3, "CATEGORY_HOLDERS_LIST"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 75
    return-object v2
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 52
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090088

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 54
    return-void
.end method

.method public isDeleteMode()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f0300f7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;->setContentView(I)V

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;->initFoodFragment()V

    .line 41
    return-void
.end method

.method public onSelectedPanelHolderSelected(Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;)Z
    .locals 1
    .param p1, "selectedPanelHolder"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getMealItemDatas()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;->openEditMealActivity(Ljava/util/ArrayList;)V

    .line 81
    const/4 v0, 0x0

    return v0
.end method
