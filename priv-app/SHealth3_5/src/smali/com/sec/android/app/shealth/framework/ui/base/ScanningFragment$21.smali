.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getView(Lcom/sec/android/app/shealth/framework/ui/base/PairOfDeviceIdAndName;Z)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 24

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopSearch()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->stopDiscoveringSensorDevices()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$3800(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->isScanning:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$2902(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;Z)Z

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v4, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;

    invoke-direct {v4, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x35

    aput v13, v2, v12

    const/16 v12, -0x9

    aput v12, v2, v11

    const/16 v11, -0x5bb4

    aput v11, v2, v10

    const/16 v10, -0x2e

    aput v10, v2, v9

    const/16 v9, -0x7d8e

    aput v9, v2, v8

    const/16 v8, -0x19

    aput v8, v2, v7

    const/16 v7, -0x30

    aput v7, v2, v6

    const/16 v6, -0x49

    aput v6, v2, v5

    const/16 v5, -0x25

    aput v5, v2, v3

    const/16 v3, -0x3e

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x52

    aput v14, v1, v13

    const/16 v13, -0x6c

    aput v13, v1, v12

    const/16 v12, -0x5bdb

    aput v12, v1, v11

    const/16 v11, -0x5c

    aput v11, v1, v10

    const/16 v10, -0x7e00

    aput v10, v1, v9

    const/16 v9, -0x7e

    aput v9, v1, v8

    const/16 v8, -0x7d

    aput v8, v1, v7

    const/16 v7, -0x2b

    aput v7, v1, v6

    const/16 v6, -0x4e

    aput v6, v1, v5

    const/16 v5, -0x52

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x9

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, -0x638e

    aput v14, v2, v13

    const/16 v13, -0x5f

    aput v13, v2, v12

    const/16 v12, -0x72dd

    aput v12, v2, v11

    const/4 v11, -0x2

    aput v11, v2, v10

    const/16 v10, -0x2896

    aput v10, v2, v9

    const/16 v9, -0x9

    aput v9, v2, v8

    const/16 v8, -0x52

    aput v8, v2, v7

    const/16 v7, 0x2850

    aput v7, v2, v3

    const/16 v3, -0xd84

    aput v3, v2, v1

    const/16 v1, 0x9

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, -0x63ae

    aput v15, v1, v14

    const/16 v14, -0x64

    aput v14, v1, v13

    const/16 v13, -0x72fd

    aput v13, v1, v12

    const/16 v12, -0x73

    aput v12, v1, v11

    const/16 v11, -0x28fd

    aput v11, v1, v10

    const/16 v10, -0x29

    aput v10, v1, v9

    const/16 v9, -0x17

    aput v9, v1, v8

    const/16 v8, 0x2811

    aput v8, v1, v7

    const/16 v7, -0xdd8

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v7, v1

    if-lt v3, v7, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v7, v1

    if-lt v3, v7, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, -0x6e81

    aput v10, v2, v9

    const/16 v9, -0xc

    aput v9, v2, v8

    const/16 v8, 0x461e

    aput v8, v2, v7

    const/16 v7, 0x4f2f

    aput v7, v2, v6

    const/16 v6, 0x3239

    aput v6, v2, v5

    const/16 v5, -0x5da9

    aput v5, v2, v3

    const/16 v3, -0x1a

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x35

    aput v12, v1, v11

    const/16 v11, -0x6eca

    aput v11, v1, v10

    const/16 v10, -0x6f

    aput v10, v1, v9

    const/16 v9, 0x467d

    aput v9, v1, v8

    const/16 v8, 0x4f46

    aput v8, v1, v7

    const/16 v7, 0x324f

    aput v7, v1, v6

    const/16 v6, -0x5dce

    aput v6, v1, v5

    const/16 v5, -0x5e

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x1d

    aput v13, v2, v12

    const/16 v12, 0x4a5d

    aput v12, v2, v11

    const/16 v11, 0x6e2b

    aput v11, v2, v10

    const/16 v10, -0x4be0

    aput v10, v2, v9

    const/16 v9, -0x2f

    aput v9, v2, v8

    const/16 v8, 0xe21

    aput v8, v2, v7

    const/16 v7, -0x6599

    aput v7, v2, v6

    const/16 v6, -0x14

    aput v6, v2, v5

    const/16 v5, 0x1f33

    aput v5, v2, v3

    const/16 v3, 0x5f5b

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x7a

    aput v14, v1, v13

    const/16 v13, 0x4a30

    aput v13, v1, v12

    const/16 v12, 0x6e4a

    aput v12, v1, v11

    const/16 v11, -0x4b92

    aput v11, v1, v10

    const/16 v10, -0x4c

    aput v10, v1, v9

    const/16 v9, 0xe42

    aput v9, v1, v8

    const/16 v8, -0x65f2

    aput v8, v1, v7

    const/16 v7, -0x66

    aput v7, v1, v6

    const/16 v6, 0x1f56

    aput v6, v1, v5

    const/16 v5, 0x5f1f

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x2419

    aput v11, v2, v10

    const/16 v10, 0x3954

    aput v10, v2, v9

    const/16 v9, -0x43c0

    aput v9, v2, v8

    const/16 v8, -0x18

    aput v8, v2, v7

    const/16 v7, -0x17

    aput v7, v2, v6

    const/16 v6, -0x34

    aput v6, v2, v5

    const/16 v5, -0x15a5

    aput v5, v2, v3

    const/16 v3, -0x52

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x247c

    aput v12, v1, v11

    const/16 v11, 0x3924

    aput v11, v1, v10

    const/16 v10, -0x43c7

    aput v10, v1, v9

    const/16 v9, -0x44

    aput v9, v1, v8

    const/16 v8, -0x78

    aput v8, v1, v7

    const/16 v7, -0x48

    aput v7, v1, v6

    const/16 v6, -0x15c6

    aput v6, v1, v5

    const/16 v5, -0x16

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceDataType()I

    move-result v1

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x36

    aput v13, v2, v12

    const/16 v12, -0x3a

    aput v12, v2, v11

    const/16 v11, -0x62

    aput v11, v2, v10

    const/16 v10, 0x719

    aput v10, v2, v9

    const/16 v9, 0x6862

    aput v9, v2, v8

    const/16 v8, 0x600b

    aput v8, v2, v7

    const/16 v7, -0x44f7

    aput v7, v2, v6

    const/16 v6, -0x33

    aput v6, v2, v5

    const/16 v5, 0x32e

    aput v5, v2, v3

    const/16 v3, -0x11b9

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x51

    aput v14, v1, v13

    const/16 v13, -0x4a

    aput v13, v1, v12

    const/16 v12, -0x19

    aput v12, v1, v11

    const/16 v11, 0x74d

    aput v11, v1, v10

    const/16 v10, 0x6807

    aput v10, v1, v9

    const/16 v9, 0x6068

    aput v9, v1, v8

    const/16 v8, -0x44a0

    aput v8, v1, v7

    const/16 v7, -0x45

    aput v7, v1, v6

    const/16 v6, 0x34b

    aput v6, v1, v5

    const/16 v5, -0x11fd

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v5, v1

    if-lt v3, v5, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v5, v1

    if-lt v3, v5, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceType()I

    move-result v1

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x28

    aput v19, v2, v18

    const/16 v18, 0x5c0f

    aput v18, v2, v17

    const/16 v17, 0x725

    aput v17, v2, v16

    const/16 v16, 0x4453

    aput v16, v2, v15

    const/16 v15, -0x7dc3

    aput v15, v2, v14

    const/16 v14, -0xa

    aput v14, v2, v13

    const/16 v13, -0x57

    aput v13, v2, v12

    const/16 v12, -0x20

    aput v12, v2, v11

    const/16 v11, -0x6f

    aput v11, v2, v10

    const/16 v10, -0x1d

    aput v10, v2, v9

    const/16 v9, -0x42da

    aput v9, v2, v8

    const/16 v8, -0x28

    aput v8, v2, v7

    const/16 v7, -0x58

    aput v7, v2, v6

    const/16 v6, -0x27

    aput v6, v2, v5

    const/16 v5, 0x4045

    aput v5, v2, v3

    const/16 v3, 0x4503

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x43

    aput v20, v1, v19

    const/16 v19, 0x5c7f

    aput v19, v1, v18

    const/16 v18, 0x75c

    aput v18, v1, v17

    const/16 v17, 0x4407

    aput v17, v1, v16

    const/16 v16, -0x7dbc

    aput v16, v1, v15

    const/16 v15, -0x7e

    aput v15, v1, v14

    const/16 v14, -0x40

    aput v14, v1, v13

    const/16 v13, -0x6a

    aput v13, v1, v12

    const/4 v12, -0x8

    aput v12, v1, v11

    const/16 v11, -0x69

    aput v11, v1, v10

    const/16 v10, -0x42bb

    aput v10, v1, v9

    const/16 v9, -0x43

    aput v9, v1, v8

    const/16 v8, -0x3a

    aput v8, v1, v7

    const/16 v7, -0x49

    aput v7, v1, v6

    const/16 v6, 0x402a

    aput v6, v1, v5

    const/16 v5, 0x4540

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mPairedDeviceMap:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;->getDeviceConnectivityType()I

    move-result v1

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$21;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    const/16 v2, 0x3e9

    invoke-virtual {v1, v4, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_f
    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_9
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_b
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_c
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :catch_0
    move-exception v1

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x42

    aput v12, v2, v11

    const/16 v11, -0x64

    aput v11, v2, v10

    const/16 v10, -0x47

    aput v10, v2, v9

    const/16 v9, -0x2f

    aput v9, v2, v8

    const/16 v8, 0x5162

    aput v8, v2, v7

    const/16 v7, -0x30cc

    aput v7, v2, v6

    const/16 v6, -0x64

    aput v6, v2, v5

    const/16 v5, -0x2295

    aput v5, v2, v4

    const/16 v4, -0x4c

    aput v4, v2, v3

    const/16 v3, -0xa

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x25

    aput v13, v1, v12

    const/4 v12, -0x1

    aput v12, v1, v11

    const/16 v11, -0x30

    aput v11, v1, v10

    const/16 v10, -0x59

    aput v10, v1, v9

    const/16 v9, 0x5110

    aput v9, v1, v8

    const/16 v8, -0x30af

    aput v8, v1, v7

    const/16 v7, -0x31

    aput v7, v1, v6

    const/16 v6, -0x22f7

    aput v6, v1, v5

    const/16 v5, -0x23

    aput v5, v1, v4

    const/16 v4, -0x66

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v4, v1

    if-lt v3, v4, :cond_f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v4, v1

    if-lt v3, v4, :cond_10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x13

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, -0x70af

    aput v22, v2, v21

    const/16 v21, -0x52

    aput v21, v2, v20

    const/16 v20, -0x40bc

    aput v20, v2, v19

    const/16 v19, -0x26

    aput v19, v2, v18

    const/16 v18, -0x40e1

    aput v18, v2, v17

    const/16 v17, -0x2a

    aput v17, v2, v16

    const/16 v16, 0x4906

    aput v16, v2, v15

    const/16 v15, -0x6ed4

    aput v15, v2, v14

    const/16 v14, -0xb

    aput v14, v2, v13

    const/16 v13, -0x62

    aput v13, v2, v12

    const/16 v12, -0x55

    aput v12, v2, v11

    const/16 v11, -0x2b

    aput v11, v2, v10

    const/16 v10, 0x256d

    aput v10, v2, v9

    const/16 v9, -0x42b4

    aput v9, v2, v8

    const/16 v8, -0x24

    aput v8, v2, v7

    const/16 v7, -0x4395

    aput v7, v2, v6

    const/16 v6, -0x64

    aput v6, v2, v5

    const/16 v5, -0x4e6

    aput v5, v2, v3

    const/16 v3, -0x4b

    aput v3, v2, v1

    const/16 v1, 0x13

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, -0x7090

    aput v23, v1, v22

    const/16 v22, -0x71

    aput v22, v1, v21

    const/16 v21, -0x40c9

    aput v21, v1, v20

    const/16 v20, -0x41

    aput v20, v1, v19

    const/16 v19, -0x4084

    aput v19, v1, v18

    const/16 v18, -0x41

    aput v18, v1, v17

    const/16 v17, 0x4970

    aput v17, v1, v16

    const/16 v16, -0x6eb7

    aput v16, v1, v15

    const/16 v15, -0x6f

    aput v15, v1, v14

    const/16 v14, -0x42

    aput v14, v1, v13

    const/16 v13, -0x31

    aput v13, v1, v12

    const/16 v12, -0x50

    aput v12, v1, v11

    const/16 v11, 0x251f

    aput v11, v1, v10

    const/16 v10, -0x42db

    aput v10, v1, v9

    const/16 v9, -0x43

    aput v9, v1, v8

    const/16 v8, -0x43c5

    aput v8, v1, v7

    const/16 v7, -0x44

    aput v7, v1, v6

    const/16 v6, -0x48b

    aput v6, v1, v5

    const/4 v5, -0x5

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_12
    array-length v5, v1

    if-lt v3, v5, :cond_11

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_12

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_f

    :cond_f
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_10
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_11
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_12
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :catch_1
    move-exception v1

    goto/16 :goto_e
.end method
