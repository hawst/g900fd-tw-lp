.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$1;
.super Ljava/lang/Object;
.source "CategoryContentView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->initListView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;

.field final synthetic val$listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$1;->val$listView:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 3
    .param p1, "absListView"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 88
    add-int v0, p2, p3

    .line 89
    .local v0, "lastItem":I
    if-ne v0, p4, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->access$000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->mOnScrollToEndListener:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;->access$000(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView;)Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$1;->val$listView:Landroid/widget/ListView;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryContentView$OnScrollToEndListener;->onEnd(Landroid/widget/ListView;)V

    .line 92
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1, "absListView"    # Landroid/widget/AbsListView;
    .param p2, "i"    # I

    .prologue
    .line 83
    return-void
.end method
