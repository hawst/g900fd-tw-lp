.class Lcom/sec/android/app/shealth/home/EditFavoritesActivity$DiscardDialogButtonController;
.super Ljava/lang/Object;
.source "EditFavoritesActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/EditFavoritesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiscardDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V
    .locals 0

    .prologue
    .line 480
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$DiscardDialogButtonController;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;Lcom/sec/android/app/shealth/home/EditFavoritesActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/home/EditFavoritesActivity$1;

    .prologue
    .line 480
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$DiscardDialogButtonController;-><init>(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 485
    sget-object v0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$7;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 497
    :goto_0
    :pswitch_0
    return-void

    .line 488
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$DiscardDialogButtonController;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    # invokes: Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->revertChanges()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->access$600(Lcom/sec/android/app/shealth/home/EditFavoritesActivity;)V

    .line 489
    invoke-static {}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->logPrefFile()V

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/EditFavoritesActivity$DiscardDialogButtonController;->this$0:Lcom/sec/android/app/shealth/home/EditFavoritesActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/EditFavoritesActivity;->finish()V

    goto :goto_0

    .line 485
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
