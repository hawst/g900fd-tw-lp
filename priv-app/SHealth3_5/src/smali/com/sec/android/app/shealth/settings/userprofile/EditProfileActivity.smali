.class public Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "EditProfileActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$DismissDialogController;,
        Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;,
        Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;
    }
.end annotation


# static fields
.field private static final CROP_FROM_ALBUM:I = 0x3

.field private static final CROP_FROM_CAMERA:I = 0x2

.field private static final DATE:I = 0x3

.field private static final DELETE_PHOTO_ACTION:I = 0x2

.field private static final EMOTICONS:I = 0x6

.field private static final GENDER:I = 0x2

.field private static final HEIGHT:I = 0x4

.field private static final HEIGHT_DIALOG:Ljava/lang/String; = "height_dialog"

.field public static final MAX_CHARACTER_COUNT:I = 0x32

.field private static final NAME:I = 0x1

.field private static final NO_MEDIA_FILE:Ljava/lang/String;

.field private static final PASS:I = 0x0

.field private static final PICK_FROM_ALBUM:I = 0x1

.field private static final PICK_FROM_CAMERA:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final TAKE_ALBUM_ACTION:I = 0x0

.field private static final TAKE_PHOTO_ACTION:I = 0x1

.field private static final TEMP_IMAGE_PREFIX:Ljava/lang/String; = "temp_"

.field private static final TEMP_IMAGE_TIME_FORMAT:Ljava/lang/String; = "HH_mm_ss_SSS"

.field private static final WEIGHT:I = 0x5

.field private static final WEIGHT_DIALOG:Ljava/lang/String; = "weight_dialog"

.field private static gender:I


# instance fields
.field private DISCARD_CHANGES:Ljava/lang/String;

.field private IMAGE_CAPTURE_DATA:Ljava/lang/String;

.field private IMAGE_CAPTURE_URI:Ljava/lang/String;

.field private activityType:I

.field private afterActivityType:I

.field private beforeActivityType:I

.field private camera_flag:Z

.field private datePicker:Landroid/widget/DatePicker;

.field private dialogInterface:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

.field private femaleIcon:Landroid/widget/ImageView;

.field private femaleText:Landroid/widget/TextView;

.field private genderImage:Landroid/widget/ImageView;

.field private isChangeImage:Z

.field private isImage:Z

.field private lev_1:Landroid/widget/RelativeLayout;

.field private lev_2:Landroid/widget/RelativeLayout;

.field private lev_3:Landroid/widget/RelativeLayout;

.field private lev_4:Landroid/widget/RelativeLayout;

.field private lev_5:Landroid/widget/RelativeLayout;

.field private listItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDismissControlMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;",
            ">;"
        }
    .end annotation
.end field

.field private mEditNameWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

.field private mFemaleLayout:Landroid/widget/LinearLayout;

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mMaleLayout:Landroid/widget/LinearLayout;

.field private mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

.field private mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private maleIcon:Landroid/widget/ImageView;

.field private maleText:Landroid/widget/TextView;

.field private nameEditText:Landroid/widget/EditText;

.field oldText:Ljava/lang/String;

.field private original_file:Ljava/io/File;

.field private photo:Landroid/graphics/Bitmap;

.field private popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

.field protected profileImage:Landroid/widget/ImageView;

.field private profileImageMan:Landroid/widget/ImageButton;

.field private profilePressView:Landroid/view/View;

.field private rankingCheckContainer:Landroid/widget/RelativeLayout;

.field private rankingText:Landroid/widget/TextView;

.field private shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

.field private touchListener:Landroid/view/View$OnTouchListener;

.field private useRankingCheck:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 125
    const-class v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->CACHED_IMAGES_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".nomedia"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->NO_MEDIA_FILE:Ljava/lang/String;

    .line 140
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 155
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isImage:Z

    .line 156
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isChangeImage:Z

    .line 157
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->camera_flag:Z

    .line 165
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 167
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    .line 168
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 169
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 170
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->oldText:Ljava/lang/String;

    .line 172
    const-string v0, "discard_changes"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->DISCARD_CHANGES:Ljava/lang/String;

    .line 184
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 195
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mDismissControlMap:Ljava/util/Map;

    .line 244
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mEditNameWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

    .line 431
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->touchListener:Landroid/view/View$OnTouchListener;

    .line 1616
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$10;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->dialogInterface:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    .line 1898
    return-void
.end method

.method private SaveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 1397
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1398
    .local v1, "fileCacheItem":Ljava/io/File;
    const/4 v2, 0x0

    .line 1401
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 1402
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1403
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1413
    if-eqz v3, :cond_0

    .line 1414
    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 1422
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    :cond_1
    :goto_0
    return-void

    .line 1417
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 1419
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 1421
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_0

    .line 1405
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1407
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1413
    if-eqz v2, :cond_1

    .line 1414
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1417
    :catch_2
    move-exception v0

    .line 1419
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1411
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 1413
    :goto_2
    if-eqz v2, :cond_2

    .line 1414
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1420
    :cond_2
    :goto_3
    throw v4

    .line 1417
    :catch_3
    move-exception v0

    .line 1419
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1411
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 1405
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->genderImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mMaleLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mFemaleLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideParentKeyboard()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideDateKeyboard()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
    .param p1, "x1"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I

    return p1
.end method

.method static synthetic access$1802(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
    .param p1, "x1"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->afterActivityType:I

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
    .param p1, "x1"    # I

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->initActivityLevelView(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->checkEditFocus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->showDiscardPopup()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->checkInput()I

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->saveProfile()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->doTakePhotoAction()V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->doTakeAlbumAction()V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->doDeletePhotoAction()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->showRangeDialog(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isImage:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSetPicturePopup(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 123
    sget v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    return v0
.end method

.method static synthetic access$702(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 123
    sput p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    return p0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleText:Landroid/widget/TextView;

    return-object v0
.end method

.method private checkChangeProfile()Z
    .locals 5

    .prologue
    .line 748
    const/4 v1, 0x0

    .line 749
    .local v1, "isChangeProfile":Z
    const/4 v2, 0x0

    .line 750
    .local v2, "name":Landroid/text/Editable;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 752
    .local v0, "cal":Ljava/util/Calendar;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v3, :cond_0

    .line 753
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 755
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 757
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 758
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 759
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isChangeImage:Z

    if-nez v3, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v3

    sget v4, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    if-ne v3, v4, :cond_3

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getYear()I

    move-result v4

    if-ne v3, v4, :cond_3

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getMonth()I

    move-result v4

    if-ne v3, v4, :cond_3

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v4

    if-ne v3, v4, :cond_3

    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isUsePedometerRanking(Landroid/content/Context;)Z

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkChangeValue()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightValue()F

    move-result v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->checkChangeValue()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getWeight()F

    move-result v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightUnit()I

    move-result v4

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getWeightUnit()I

    move-result v4

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I

    if-eq v3, v4, :cond_4

    .line 760
    :cond_3
    const/4 v1, 0x1

    .line 763
    :cond_4
    return v1
.end method

.method private checkEditFocus()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1583
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1584
    .local v0, "v":Landroid/view/View;
    instance-of v2, v0, Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 1587
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1613
    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 1591
    :sswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->checkValueText()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1593
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    goto :goto_0

    .line 1603
    :sswitch_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkValueText()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1605
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 1606
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideDateKeyboard()V

    .line 1607
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideParentKeyboard()V

    goto :goto_0

    .line 1587
    :sswitch_data_0
    .sparse-switch
        0x7f08088b -> :sswitch_1
        0x7f08088c -> :sswitch_1
        0x7f08088d -> :sswitch_1
        0x7f0808a8 -> :sswitch_0
    .end sparse-switch
.end method

.method private checkInput()I
    .locals 2

    .prologue
    .line 1139
    sget v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    const v1, 0x2e635

    if-eq v0, v1, :cond_0

    sget v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    const v1, 0x2e636

    if-eq v0, v1, :cond_0

    .line 1141
    const/4 v0, 0x2

    .line 1157
    :goto_0
    return v0

    .line 1143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1145
    const/4 v0, 0x1

    goto :goto_0

    .line 1148
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 1150
    const/4 v0, 0x4

    goto :goto_0

    .line 1152
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->checkValidName(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1154
    const/4 v0, 0x6

    goto :goto_0

    .line 1157
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkValidName(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1126
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1128
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    sget-object v2, Ljava/lang/Character$UnicodeBlock;->HIGH_SURROGATES:Ljava/lang/Character$UnicodeBlock;

    if-ne v1, v2, :cond_0

    .line 1130
    const/4 v1, 0x0

    .line 1133
    :goto_1
    return v1

    .line 1126
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1133
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 4
    .param p0, "srcFile"    # Ljava/io/File;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    .line 1426
    const/4 v2, 0x0

    .line 1429
    .local v2, "result":Z
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1432
    .local v1, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 1436
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1443
    .end local v1    # "in":Ljava/io/InputStream;
    :goto_0
    return v2

    .line 1436
    .restart local v1    # "in":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1439
    .end local v1    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 1441
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x0

    .line 1450
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1453
    .local v3, "out":Ljava/io/OutputStream;
    const/16 v5, 0x1000

    :try_start_1
    new-array v0, v5, [B

    .line 1455
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    if-ltz v1, :cond_0

    .line 1457
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1462
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    throw v5

    .line 1466
    .end local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 1468
    :goto_1
    return v4

    .line 1462
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v3    # "out":Ljava/io/OutputStream;
    :cond_0
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1464
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private createSaveCropFile()Landroid/net/Uri;
    .locals 9

    .prologue
    .line 1385
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1386
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 1387
    .local v3, "time":J
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v7, "HH_mm_ss_SSS"

    invoke-direct {v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1388
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "temp_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1389
    .local v6, "url":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Camera"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 1390
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1391
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 1392
    .local v5, "uri":Landroid/net/Uri;
    return-object v5
.end method

.method private createSetPicturePopup(Z)V
    .locals 4
    .param p1, "picture"    # Z

    .prologue
    .line 1477
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v1, 0x7f0907a7

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 1479
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->listItem:Ljava/util/ArrayList;

    .line 1480
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->listItem:Ljava/util/ArrayList;

    const v2, 0x7f0907a9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1481
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->listItem:Ljava/util/ArrayList;

    const v2, 0x7f0907a8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1483
    if-eqz p1, :cond_0

    .line 1484
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->listItem:Ljava/util/ArrayList;

    const v2, 0x7f09003a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1486
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 1487
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 1489
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "SetPicture"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1490
    return-void
.end method

.method private deleteTempFile()V
    .locals 2

    .prologue
    .line 1371
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 1373
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1375
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1377
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1380
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private doDeletePhotoAction()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1194
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->deleteTempFile()V

    .line 1195
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    .line 1196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isChangeImage:Z

    .line 1197
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setDefaultImage()V

    .line 1201
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->clearProfileImageCache()V

    .line 1202
    return-void
.end method

.method private doTakeAlbumAction()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1219
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1220
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "set-as-image"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1221
    const-string/jumbo v1, "vnd.android.cursor.dir/image"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1222
    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1223
    return-void
.end method

.method private doTakePhotoAction()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1207
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->deleteTempFile()V

    .line 1208
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1210
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1211
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1212
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1213
    const-string/jumbo v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1214
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1215
    return-void
.end method

.method public static getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 451
    new-instance v4, Ljava/io/File;

    sget-object v7, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 452
    .local v4, "imgFile":Ljava/io/File;
    const/4 v1, 0x0

    .line 453
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 455
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 456
    .local v6, "uri":Landroid/net/Uri;
    const/4 v0, 0x0

    .line 459
    .local v0, "afd":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "r"

    invoke-virtual {v7, v6, v8}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 460
    if-eqz v0, :cond_0

    .line 462
    new-instance v5, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v5}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 463
    .local v5, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v7, 0x1

    iput v7, v5, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 464
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8, v5}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 475
    .end local v5    # "opt":Landroid/graphics/BitmapFactory$Options;
    :cond_0
    if-eqz v0, :cond_1

    .line 476
    :try_start_1
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    move-object v2, v1

    .line 485
    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "uri":Landroid/net/Uri;
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    return-object v2

    .line 478
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v6    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v3

    .line 480
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 467
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 469
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 475
    if-eqz v0, :cond_1

    .line 476
    :try_start_3
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 478
    :catch_2
    move-exception v3

    .line 480
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 473
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 475
    if-eqz v0, :cond_2

    .line 476
    :try_start_4
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 481
    :cond_2
    :goto_2
    throw v7

    .line 478
    :catch_3
    move-exception v3

    .line 480
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .end local v0    # "afd":Landroid/content/res/AssetFileDescriptor;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_3
    move-object v2, v1

    .line 485
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    goto :goto_1
.end method

.method private hideDateKeyboard()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1170
    const-string v9, "input_method"

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    .line 1171
    .local v4, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v4, :cond_0

    .line 1173
    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v9, v10}, Landroid/widget/DatePicker;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 1174
    .local v8, "v":Landroid/view/ViewGroup;
    invoke-virtual {v8, v10}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1176
    .local v0, "a":Landroid/view/ViewGroup;
    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 1177
    .local v5, "numberPicker0":Landroid/view/ViewGroup;
    invoke-virtual {v0, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 1178
    .local v6, "numberPicker1":Landroid/view/ViewGroup;
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 1180
    .local v7, "numberPicker2":Landroid/view/ViewGroup;
    invoke-virtual {v5, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 1181
    .local v3, "datePickerChildEditYear":Landroid/widget/EditText;
    invoke-virtual {v6, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 1182
    .local v2, "datePickerChildEditMonth":Landroid/widget/EditText;
    invoke-virtual {v7, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 1184
    .local v1, "datePickerChildEditDay":Landroid/widget/EditText;
    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v9

    invoke-virtual {v4, v9, v10}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1185
    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v9

    invoke-virtual {v4, v9, v10}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1186
    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v9

    invoke-virtual {v4, v9, v10}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1187
    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v9}, Landroid/widget/DatePicker;->clearFocus()V

    .line 1189
    .end local v0    # "a":Landroid/view/ViewGroup;
    .end local v1    # "datePickerChildEditDay":Landroid/widget/EditText;
    .end local v2    # "datePickerChildEditMonth":Landroid/widget/EditText;
    .end local v3    # "datePickerChildEditYear":Landroid/widget/EditText;
    .end local v5    # "numberPicker0":Landroid/view/ViewGroup;
    .end local v6    # "numberPicker1":Landroid/view/ViewGroup;
    .end local v7    # "numberPicker2":Landroid/view/ViewGroup;
    .end local v8    # "v":Landroid/view/ViewGroup;
    :cond_0
    return-void
.end method

.method private hideParentKeyboard()V
    .locals 3

    .prologue
    .line 1162
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1163
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 1164
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1165
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    .line 1166
    return-void
.end method

.method private initActivityLevelView(I)I
    .locals 21
    .param p1, "activity"    # I

    .prologue
    .line 801
    const/16 v18, -0x1

    move/from16 v0, p1

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 802
    const/4 v5, -0x1

    .line 871
    :cond_0
    :goto_0
    return v5

    .line 804
    :cond_1
    const/4 v5, 0x0

    .line 806
    .local v5, "lev":I
    packed-switch p1, :pswitch_data_0

    .line 825
    :goto_1
    if-nez v5, :cond_2

    .line 826
    const/4 v5, -0x1

    goto :goto_0

    .line 809
    :pswitch_0
    const/4 v5, 0x1

    .line 810
    goto :goto_1

    .line 812
    :pswitch_1
    const/4 v5, 0x2

    .line 813
    goto :goto_1

    .line 815
    :pswitch_2
    const/4 v5, 0x3

    .line 816
    goto :goto_1

    .line 818
    :pswitch_3
    const/4 v5, 0x4

    .line 819
    goto :goto_1

    .line 821
    :pswitch_4
    const/4 v5, 0x5

    goto :goto_1

    .line 828
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 834
    .local v6, "packName":Ljava/lang/String;
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_2
    const/16 v18, 0x6

    move/from16 v0, v18

    if-ge v3, v0, :cond_0

    .line 836
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "lev"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_image"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 837
    .local v8, "resID_image":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "s_health_profilesetting_lev_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_nor"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "drawable"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 838
    .local v9, "resID_image_nor":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "s_health_profilesetting_lev_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_sel"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "drawable"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 839
    .local v10, "resID_image_sel":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "lev"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_text"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 840
    .local v13, "resID_text":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "lev"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_title"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    .line 841
    .local v14, "resID_title":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "lev"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_sub_title"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 842
    .local v12, "resID_sub_title":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "lev"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_radio_button"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 844
    .local v11, "resID_radio":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 845
    .local v4, "image":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 846
    .local v16, "text":Landroid/widget/TextView;
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 847
    .local v17, "title":Landroid/widget/TextView;
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 848
    .local v15, "sub_title":Landroid/widget/TextView;
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    .line 850
    .local v7, "radio":Landroid/widget/RadioButton;
    if-ne v3, v5, :cond_3

    .line 852
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 853
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f070100

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 854
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0205ea

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 855
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f07011a

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setTextColor(I)V

    .line 856
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f07011a

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 857
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 834
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 862
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 863
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f070119

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 864
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 865
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f070119

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setTextColor(I)V

    .line 866
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f070119

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getColor(I)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 867
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_3

    .line 806
    :pswitch_data_0
    .packed-switch 0x2bf21
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private initValue()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    const v10, 0x7f09020b

    const v9, 0x7f09020a

    const v8, 0x7f0901f0

    .line 490
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v3, :cond_0

    .line 491
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 493
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getDistanceUnit()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileDisclose()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 497
    .local v1, "profileLog":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v3, :cond_5

    .line 501
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    .line 503
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->SaveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 504
    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    .line 505
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->onSaveImageView()V

    .line 528
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 530
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setSelection(I)V

    .line 531
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v3

    sput v3, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    .line 533
    sget v3, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    const v4, 0x2e635

    if-ne v3, v4, :cond_8

    .line 535
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205ee

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 536
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07011b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 537
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->genderImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205eb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 538
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mMaleLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09081e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901f1

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mFemaleLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09081f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901f2

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_1
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 551
    .local v0, "cal":Ljava/util/Calendar;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    if-eqz v3, :cond_2

    .line 553
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getMonth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Ljava/util/Calendar;->set(III)V

    .line 555
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 557
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    if-eqz v3, :cond_3

    .line 559
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v0, v11}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    new-instance v7, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$6;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$6;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 568
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileDisclose()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 570
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 571
    .local v2, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v3

    const v4, 0x249f1

    if-ne v3, v4, :cond_9

    .line 573
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setListValue(I)V

    .line 574
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const v4, 0x249f1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setHeightUnit(I)V

    .line 582
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_a

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v3

    const v4, 0x1fbd1

    if-ne v3, v4, :cond_a

    .line 584
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setDropDownListValue(I)V

    .line 585
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const v4, 0x1fbd1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightUnit(I)V

    .line 593
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_b

    .line 595
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const/high16 v4, 0x42480000    # 50.0f

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightPreviousValue(F)V

    .line 596
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const/high16 v4, 0x42480000    # 50.0f

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setValue(F)V

    .line 603
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_c

    .line 605
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const/high16 v4, 0x43160000    # 150.0f

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setValue(F)V

    .line 612
    :goto_5
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I

    .line 613
    iget v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->initActivityLevelView(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->beforeActivityType:I

    .line 615
    return-void

    .line 509
    .end local v0    # "cal":Ljava/util/Calendar;
    .end local v2    # "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setDefaultImage()V

    .line 510
    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 513
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->isUserFile()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 515
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    .line 516
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_6

    .line 518
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setImage(Landroid/graphics/Bitmap;)V

    .line 520
    :cond_6
    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 524
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setDefaultImage()V

    .line 525
    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    goto/16 :goto_0

    .line 541
    :cond_8
    sget v3, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    const v4, 0x2e636

    if-ne v3, v4, :cond_1

    .line 543
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205dd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 544
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07011b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 545
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->genderImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205db

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 546
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mMaleLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09081e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901f2

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mFemaleLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09081f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901f1

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 578
    .restart local v0    # "cal":Ljava/util/Calendar;
    .restart local v2    # "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v3, v11}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setListValue(I)V

    .line 579
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    const v4, 0x249f2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setHeightUnit(I)V

    goto/16 :goto_2

    .line 589
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v3, v11}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setDropDownListValue(I)V

    .line 590
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const v4, 0x1fbd2

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightUnit(I)V

    goto/16 :goto_3

    .line 600
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightPreviousValue(F)V

    .line 601
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setValue(F)V

    goto/16 :goto_4

    .line 609
    :cond_c
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setValue(F)V

    goto/16 :goto_5
.end method

.method public static isNoMediaFile()Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1736
    const/4 v1, 0x0

    .line 1739
    .local v1, "imgFile":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->NO_MEDIA_FILE:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1740
    .end local v1    # "imgFile":Ljava/io/File;
    .local v2, "imgFile":Ljava/io/File;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1742
    sget-object v3, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    const-string v4, "isNoMediaFile Exists"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1743
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 1750
    .end local v2    # "imgFile":Ljava/io/File;
    :goto_0
    return-object v3

    .line 1746
    .restart local v1    # "imgFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 1748
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_0

    .line 1750
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "imgFile":Ljava/io/File;
    .restart local v2    # "imgFile":Ljava/io/File;
    :cond_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_0

    .line 1746
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "imgFile":Ljava/io/File;
    .restart local v1    # "imgFile":Ljava/io/File;
    goto :goto_1
.end method

.method public static onCreateNoMediaFile()V
    .locals 4

    .prologue
    .line 1755
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->NO_MEDIA_FILE:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1758
    .local v1, "file":Ljava/io/File;
    :try_start_0
    sget-object v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onCreateNoMediaFile"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1767
    :goto_0
    return-void

    .line 1762
    :catch_0
    move-exception v0

    .line 1764
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "onCreateNoMediaFile Exception"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1765
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private onSaveImageView()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1227
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    .line 1228
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1229
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1230
    .local v1, "full_path":Ljava/lang/String;
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    .line 1231
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 1233
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setImage(Landroid/graphics/Bitmap;)V

    .line 1235
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v2, :cond_2

    .line 1237
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1239
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1241
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1244
    .end local v0    # "f":Ljava/io/File;
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->camera_flag:Z

    .line 1245
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    .line 1246
    return-void
.end method

.method private saveProfile()Z
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 619
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v6}, Landroid/widget/DatePicker;->clearFocus()V

    .line 620
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v6, :cond_0

    .line 621
    new-instance v6, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 622
    :cond_0
    const/4 v2, 0x0

    .line 623
    .local v2, "name":Landroid/text/Editable;
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 625
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 626
    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_0
    invoke-virtual {v9, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setName(Ljava/lang/String;)V

    .line 629
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    sget v9, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->gender:I

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setGender(I)V

    .line 631
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 632
    .local v0, "cal":Ljava/util/Calendar;
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v6}, Landroid/widget/DatePicker;->getYear()I

    move-result v6

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v9}, Landroid/widget/DatePicker;->getMonth()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    invoke-virtual {v10}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v10

    invoke-virtual {v0, v6, v9, v10}, Ljava/util/Calendar;->set(III)V

    .line 636
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->formatDateToProfileDob(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setBirthDate(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 644
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;

    invoke-virtual {v9}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setProfileDisclose(Z)V

    .line 645
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isUsePedometerRanking(Landroid/content/Context;)Z

    move-result v6

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;

    invoke-virtual {v9}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v9

    if-eq v6, v9, :cond_2

    .line 646
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsSendProfileDisclose(Landroid/content/Context;Z)V

    .line 647
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;

    invoke-virtual {v9}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v9

    invoke-static {v6, v9}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setIsUsePedometerRanking(Landroid/content/Context;Z)V

    .line 648
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isUsePedometerRanking(Landroid/content/Context;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setProfileDiscloseYN(Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 651
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setHeightValue()V

    .line 652
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setWeightValue()V

    .line 654
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightValue()F

    move-result v6

    cmpg-float v6, v6, v11

    if-gtz v6, :cond_8

    .line 656
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    .line 663
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getWeight()F

    move-result v6

    cmpg-float v6, v6, v11

    if-gtz v6, :cond_9

    .line 665
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    const/high16 v9, 0x40000000    # 2.0f

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 672
    :goto_2
    iget v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->beforeActivityType:I

    if-eq v6, v12, :cond_3

    iget v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->afterActivityType:I

    if-eq v6, v12, :cond_3

    iget v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->beforeActivityType:I

    iget v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->afterActivityType:I

    if-eq v6, v9, :cond_3

    .line 673
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v9, "com.sec.android.app.shealth"

    const-string v10, "4002"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget v12, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->beforeActivityType:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, "to"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->afterActivityType:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v9, v10, v11}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->activityType:I

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setActivityType(I)V

    .line 676
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setLastUpdated(Landroid/content/Context;)V

    .line 681
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightUnit()I

    move-result v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeightUnit(I)V

    .line 682
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getWeightUnit()I

    move-result v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeightUnit(I)V

    .line 684
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightUnit()I

    move-result v6

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getWeightUnit()I

    move-result v9

    invoke-static {v6, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->saveUnitforUnitHelper(II)V

    .line 687
    const/4 v4, 0x0

    .line 688
    .local v4, "resultCode":I
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 689
    .local v3, "newIntent":Landroid/content/Intent;
    new-instance v5, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    .line 692
    .local v5, "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_a

    iget-boolean v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->camera_flag:Z

    if-eqz v6, :cond_a

    .line 694
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    sget-object v9, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-static {v6, v9}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveImageToInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    .line 695
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isNoMediaFile()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_4

    .line 697
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->onCreateNoMediaFile()V

    .line 708
    :cond_4
    :goto_3
    :try_start_1
    const-string v6, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 709
    const-string v6, "distance_unit"

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 710
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V

    .line 713
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 714
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->updateUserProfile(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 721
    :cond_5
    :goto_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setCalorieGoal(Landroid/content/Context;)V

    .line 722
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v6

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v9

    invoke-direct {p0, v6, v9}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setWeightProfile(FF)V

    .line 725
    if-eqz v3, :cond_6

    .line 727
    invoke-virtual {p0, v4, v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setResult(ILandroid/content/Intent;)V

    .line 730
    if-eq v4, v8, :cond_6

    .line 732
    sget-object v6, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    const-string v8, "Send ACTION_UPDATE_PROFILE"

    invoke-static {v6, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 738
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideParentKeyboard()V

    .line 739
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideDateKeyboard()V

    .line 740
    const v6, 0x7f090835

    invoke-static {p0, v6, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 741
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->finish()V

    move v6, v7

    .line 743
    .end local v3    # "newIntent":Landroid/content/Intent;
    .end local v4    # "resultCode":I
    .end local v5    # "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :goto_5
    return v6

    .line 626
    .end local v0    # "cal":Ljava/util/Calendar;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v10, 0x7f09081c

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 638
    .restart local v0    # "cal":Ljava/util/Calendar;
    :catch_0
    move-exception v1

    .line 640
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090836

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->showToast(Ljava/lang/String;Z)V

    move v6, v8

    .line 641
    goto :goto_5

    .line 660
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getHeightValue()F

    move-result v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setHeight(F)V

    goto/16 :goto_1

    .line 669
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->shealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->getWeight()F

    move-result v9

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    goto/16 :goto_2

    .line 700
    .restart local v3    # "newIntent":Landroid/content/Intent;
    .restart local v4    # "resultCode":I
    .restart local v5    # "unitHelper":Lcom/sec/android/app/shealth/common/utils/UnitHelper;
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    if-nez v6, :cond_4

    .line 702
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->deleteProfileBitmap()Ljava/lang/Boolean;

    goto/16 :goto_3

    .line 716
    :catch_1
    move-exception v1

    .line 718
    .local v1, "e":Ljava/lang/Exception;
    const/4 v4, 0x1

    goto :goto_4
.end method

.method private setDatepickerSelectOnFocus(Z)V
    .locals 26
    .param p1, "flag"    # Z

    .prologue
    .line 370
    if-nez p1, :cond_0

    .line 429
    :goto_0
    return-void

    .line 372
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/DatePicker;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/view/ViewGroup;

    .line 373
    .local v21, "v":Landroid/view/ViewGroup;
    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 374
    .local v4, "a":Landroid/view/ViewGroup;
    const/16 v22, 0x0

    const/16 v23, 0x14

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 375
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 376
    .local v5, "al":Landroid/view/ViewGroup$LayoutParams;
    const/16 v22, -0x1

    move/from16 v0, v22

    iput v0, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 377
    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 379
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup;

    .line 380
    .local v18, "numberPicker0":Landroid/view/ViewGroup;
    invoke-virtual/range {v18 .. v18}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    .line 381
    .local v15, "nl0":Landroid/view/ViewGroup$LayoutParams;
    const/16 v22, -0x1

    move/from16 v0, v22

    iput v0, v15, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 382
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 384
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/view/ViewGroup;

    .line 385
    .local v19, "numberPicker1":Landroid/view/ViewGroup;
    invoke-virtual/range {v19 .. v19}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .line 386
    .local v16, "nl1":Landroid/view/ViewGroup$LayoutParams;
    const/16 v22, -0x1

    move/from16 v0, v22

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 387
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 389
    const/16 v22, 0x2

    move/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup;

    .line 390
    .local v20, "numberPicker2":Landroid/view/ViewGroup;
    invoke-virtual/range {v20 .. v20}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    .line 391
    .local v17, "nl2":Landroid/view/ViewGroup$LayoutParams;
    const/16 v22, -0x1

    move/from16 v0, v22

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 392
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 394
    const/16 v22, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/EditText;

    .line 395
    .local v12, "e1":Landroid/widget/EditText;
    const/16 v22, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/EditText;

    .line 396
    .local v13, "e2":Landroid/widget/EditText;
    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/EditText;

    .line 398
    .local v14, "e3":Landroid/widget/EditText;
    const/16 v22, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 399
    .local v7, "b1Up":Landroid/view/View;
    const/16 v22, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 401
    .local v6, "b1Down":Landroid/view/View;
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 402
    .local v9, "b2Up":Landroid/view/View;
    const/16 v22, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 404
    .local v8, "b2Down":Landroid/view/View;
    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 405
    .local v11, "b3Up":Landroid/view/View;
    const/16 v22, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 407
    .local v10, "b3Down":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->touchListener:Landroid/view/View$OnTouchListener;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->touchListener:Landroid/view/View$OnTouchListener;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->touchListener:Landroid/view/View$OnTouchListener;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->touchListener:Landroid/view/View$OnTouchListener;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->touchListener:Landroid/view/View$OnTouchListener;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->touchListener:Landroid/view/View$OnTouchListener;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 415
    new-instance v22, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$4;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    goto/16 :goto_0
.end method

.method private setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "audioManager"    # Landroid/media/AudioManager;

    .prologue
    const/4 v0, 0x1

    .line 1884
    if-eqz p2, :cond_0

    .line 1886
    invoke-virtual {p2, v0}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 1888
    :cond_0
    if-eqz p1, :cond_1

    .line 1890
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 1892
    :cond_1
    return v0
.end method

.method private setWeightProfile(FF)V
    .locals 5
    .param p1, "weight"    # F
    .param p2, "height"    # F

    .prologue
    .line 768
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->checkChangeValue()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->checkChangeValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 783
    :goto_0
    return-void

    .line 771
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 773
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "input_source_type"

    const v3, 0x3f7a1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 774
    const-string v2, "height"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 775
    const-string/jumbo v2, "weight"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 776
    const-string/jumbo v2, "sample_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 779
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 780
    :catch_0
    move-exception v0

    .line 781
    .local v0, "ie":Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
    sget-object v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insert setWeightProfile exception -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showDiscardPopup()V
    .locals 4

    .prologue
    .line 1100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->checkChangeProfile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1102
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1103
    .local v0, "discardDialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v1, 0x7f090081

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1104
    const v1, 0x7f090083

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1105
    const v1, 0x7f090f4d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1106
    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$8;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1114
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->DISCARD_CHANGES:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1122
    .end local v0    # "discardDialogBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :goto_0
    return-void

    .line 1118
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideParentKeyboard()V

    .line 1119
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->hideDateKeyboard()V

    .line 1120
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->finish()V

    goto :goto_0
.end method

.method private showRangeDialog(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0301e3

    .line 1629
    if-nez p1, :cond_0

    .line 1631
    const-string v2, "ProfileSecondView"

    const-string v3, "View is NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1732
    :goto_0
    return-void

    .line 1634
    :cond_0
    move-object v1, p1

    .line 1635
    .local v1, "v":Landroid/view/View;
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1636
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090ae2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1637
    const v2, 0x7f090047

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1638
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$11;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$11;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1657
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$12;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$12;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Landroid/view/View;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1674
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1729
    const/4 v0, 0x0

    goto :goto_0

    .line 1677
    :sswitch_0
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$13;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1700
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "height_dialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1703
    :sswitch_1
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$14;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v0, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1726
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string/jumbo v4, "weight_dialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1674
    :sswitch_data_0
    .sparse-switch
        0x7f08088b -> :sswitch_0
        0x7f08088d -> :sswitch_0
        0x7f0808a8 -> :sswitch_1
    .end sparse-switch
.end method

.method private showToast(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "isLightTheme"    # Z

    .prologue
    const/4 v4, 0x0

    .line 893
    if-eqz p2, :cond_0

    .line 895
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p1, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 896
    .local v0, "t":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    .line 897
    .local v2, "v":Landroid/view/View;
    const v3, 0x7f0208b2

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 898
    const v3, 0x102000b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 899
    .local v1, "text":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 900
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 907
    .end local v1    # "text":Landroid/widget/TextView;
    .end local v2    # "v":Landroid/view/View;
    :goto_0
    return-void

    .line 904
    .end local v0    # "t":Landroid/widget/Toast;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p1, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 905
    .restart local v0    # "t":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public checkProfileEmptyName()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 876
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 878
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 880
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090838

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->showToast(Ljava/lang/String;Z)V

    .line 888
    :goto_0
    return v0

    .line 884
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090837

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->showToast(Ljava/lang/String;Z)V

    goto :goto_0

    .line 888
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1048
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 1050
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$7;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    .line 1087
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setHomeButtonVisible(Z)V

    .line 1088
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v3

    const-string v4, "#73b90f"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1090
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090048

    invoke-direct {v1, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 1091
    .local v1, "actionButton1":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 1093
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f09004f

    invoke-direct {v2, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 1094
    .local v2, "actionButton2":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 1096
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1772
    const-string v8, "audio"

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1773
    .local v0, "audioManager":Landroid/media/AudioManager;
    const v8, 0x7f080332

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1774
    .local v6, "profileImageView":Landroid/view/View;
    const v8, 0x7f080335

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1775
    .local v5, "nameView":Landroid/view/View;
    const v8, 0x7f08084b

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1776
    .local v3, "femaleView":Landroid/view/View;
    const v8, 0x7f080848

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1777
    .local v4, "maleView":Landroid/view/View;
    const v8, 0x7f080325

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1779
    .local v2, "datePicker":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x14

    if-ne v8, v9, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_1

    .line 1781
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1783
    .local v1, "currentView":Landroid/view/View;
    if-eqz v1, :cond_0

    if-eqz v6, :cond_0

    if-ne v1, v6, :cond_0

    .line 1785
    if-eqz v2, :cond_1

    .line 1787
    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    .line 1859
    .end local v1    # "currentView":Landroid/view/View;
    :goto_0
    return v8

    .line 1792
    .restart local v1    # "currentView":Landroid/view/View;
    :cond_0
    if-eqz v1, :cond_1

    if-eqz v5, :cond_1

    if-ne v1, v5, :cond_1

    .line 1794
    if-eqz v4, :cond_1

    .line 1796
    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 1802
    .end local v1    # "currentView":Landroid/view/View;
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x13

    if-ne v8, v9, :cond_4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_4

    .line 1804
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1805
    .restart local v1    # "currentView":Landroid/view/View;
    if-eqz v1, :cond_2

    if-eqz v6, :cond_2

    if-ne v1, v6, :cond_2

    .line 1807
    if-eqz v5, :cond_4

    .line 1809
    const/16 v8, 0x21

    invoke-virtual {v5, v8}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v7

    .line 1810
    .local v7, "upFocusView":Landroid/view/View;
    if-eqz v7, :cond_4

    .line 1812
    invoke-direct {p0, v7, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 1817
    .end local v7    # "upFocusView":Landroid/view/View;
    :cond_2
    if-eqz v1, :cond_4

    if-eqz v4, :cond_4

    if-eqz v3, :cond_4

    if-eq v1, v4, :cond_3

    if-ne v1, v3, :cond_4

    .line 1819
    :cond_3
    if-eqz v5, :cond_4

    .line 1821
    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 1825
    .end local v1    # "currentView":Landroid/view/View;
    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x16

    if-ne v8, v9, :cond_6

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_6

    .line 1827
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1829
    .restart local v1    # "currentView":Landroid/view/View;
    if-eqz v1, :cond_5

    if-eqz v6, :cond_5

    if-ne v1, v6, :cond_5

    .line 1831
    if-eqz v5, :cond_6

    .line 1833
    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 1838
    :cond_5
    if-eqz v1, :cond_6

    if-eqz v5, :cond_6

    if-ne v1, v5, :cond_6

    .line 1840
    if-eqz v4, :cond_6

    .line 1842
    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto :goto_0

    .line 1846
    .end local v1    # "currentView":Landroid/view/View;
    :cond_6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x15

    if-ne v8, v9, :cond_7

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    if-nez v8, :cond_7

    .line 1848
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 1849
    .restart local v1    # "currentView":Landroid/view/View;
    if-eqz v1, :cond_7

    if-eqz v5, :cond_7

    if-ne v1, v5, :cond_7

    .line 1851
    if-eqz v6, :cond_7

    .line 1853
    invoke-direct {p0, v6, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setRequestFocus(Landroid/view/View;Landroid/media/AudioManager;)Z

    move-result v8

    goto/16 :goto_0

    .line 1859
    .end local v1    # "currentView":Landroid/view/View;
    :cond_7
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v8

    goto/16 :goto_0
.end method

.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1913
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mDismissControlMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 1495
    const-string v0, "SetPicture"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1497
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$9;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    .line 1517
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initLayout(I)V
    .locals 11
    .param p1, "Flag"    # I

    .prologue
    const v10, 0x7f0901fd

    const/4 v9, 0x2

    const v8, 0x7f090821

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 273
    const v1, 0x7f080333

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImage:Landroid/widget/ImageView;

    .line 274
    const v1, 0x7f080846

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImageMan:Landroid/widget/ImageButton;

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImageMan:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 276
    const v1, 0x7f080847

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profilePressView:Landroid/view/View;

    .line 277
    const v1, 0x7f080335

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    .line 278
    const v1, 0x7f080849

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleIcon:Landroid/widget/ImageView;

    .line 279
    const v1, 0x7f08084c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleIcon:Landroid/widget/ImageView;

    .line 280
    const v1, 0x7f08084a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->maleText:Landroid/widget/TextView;

    .line 281
    const v1, 0x7f08084d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->femaleText:Landroid/widget/TextView;

    .line 282
    const v1, 0x7f080850

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->useRankingCheck:Landroid/widget/CheckBox;

    .line 283
    const v1, 0x7f08084f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->rankingCheckContainer:Landroid/widget/RelativeLayout;

    .line 284
    const v1, 0x7f080851

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->rankingText:Landroid/widget/TextView;

    .line 285
    const-string/jumbo v1, "zh"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->rankingText:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 289
    :cond_0
    const v1, 0x7f080325

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/DatePicker;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->datePicker:Landroid/widget/DatePicker;

    .line 290
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setDatepickerSelectOnFocus(Z)V

    .line 292
    const v1, 0x7f080848

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mMaleLayout:Landroid/widget/LinearLayout;

    .line 293
    const v1, 0x7f08084b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mFemaleLayout:Landroid/widget/LinearLayout;

    .line 294
    const v1, 0x7f080861

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    .line 295
    const v1, 0x7f080862

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->dialogInterface:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->setOnProfileDialogListener(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mProfileWeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->dialogInterface:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setOnProfileDialogListener(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;)V

    .line 300
    const v1, 0x7f080860

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->genderImage:Landroid/widget/ImageView;

    .line 302
    const v1, 0x7f080864

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_1:Landroid/widget/RelativeLayout;

    .line 303
    const v1, 0x7f08086b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_2:Landroid/widget/RelativeLayout;

    .line 304
    const v1, 0x7f080872

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_3:Landroid/widget/RelativeLayout;

    .line 305
    const v1, 0x7f08087a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_4:Landroid/widget/RelativeLayout;

    .line 306
    const v1, 0x7f080882

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_5:Landroid/widget/RelativeLayout;

    .line 308
    const v1, 0x7f080332

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 309
    .local v0, "profileImageLayout":Landroid/widget/RelativeLayout;
    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 327
    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mMaleLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mFemaleLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->rankingCheckContainer:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnItemClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    const v1, 0x7f080867

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    const v1, 0x7f08086e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    const v1, 0x7f080875

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    const v1, 0x7f08087d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    const v1, 0x7f080885

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    const v1, 0x7f080870

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f09082c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    const v1, 0x7f080878

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f09082c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    const v1, 0x7f080880

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f09082c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 345
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_1:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_2:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_3:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_4:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->lev_5:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$OnLevelListClickListener;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    const v1, 0x7f080845

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090815

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 352
    const v1, 0x7f08084e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090816

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 353
    const v1, 0x7f08085f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090818

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 354
    const v1, 0x7f080863

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090819

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 357
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    const v2, 0x7f08088b

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setNextFocusForwardId(I)V

    .line 358
    packed-switch p1, :pswitch_data_0

    .line 366
    :goto_0
    return-void

    .line 361
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->initValue()V

    goto :goto_0

    .line 358
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected isTryActivity()Z
    .locals 1

    .prologue
    .line 1529
    const/4 v0, 0x0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1251
    sget-object v8, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "onActivityResult : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1252
    const/4 v8, 0x3

    if-ne p1, v8, :cond_0

    if-nez p2, :cond_0

    .line 1255
    :cond_0
    const/4 v8, -0x1

    if-eq p2, v8, :cond_2

    .line 1366
    :cond_1
    :goto_0
    return-void

    .line 1261
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1295
    :pswitch_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_5

    .line 1297
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1298
    .local v5, "mediaScanIntent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v5, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1299
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1301
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.android.camera.action.CROP"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1302
    .local v3, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    const-string v9, "image/*"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1304
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1305
    const-string/jumbo v8, "output"

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1306
    const-string v8, "aspectX"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1307
    const-string v8, "aspectY"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1308
    const/4 v8, 0x2

    invoke-virtual {p0, v3, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1309
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto :goto_0

    .line 1266
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "mediaScanIntent":Landroid/content/Intent;
    :pswitch_1
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    if-nez v8, :cond_4

    .line 1268
    :cond_3
    sget-object v8, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->TAG:Ljava/lang/String;

    const-string v9, "OnactivityResult: Data or data.getdata() is NULL"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1272
    :cond_4
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getImageFile(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->original_file:Ljava/io/File;

    .line 1273
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->original_file:Ljava/io/File;

    if-eqz v8, :cond_1

    .line 1278
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->deleteTempFile()V

    .line 1279
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1280
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1281
    .local v0, "copy_file":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->original_file:Ljava/io/File;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 1283
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.android.camera.action.CROP"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1284
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    const-string v9, "image/*"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1285
    const-string/jumbo v8, "output"

    iget-object v9, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1286
    const-string v8, "aspectX"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1287
    const-string v8, "aspectY"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1288
    const-string/jumbo v8, "set-as-image"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1289
    const/4 v8, 0x3

    invoke-virtual {p0, v3, v8}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1290
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto/16 :goto_0

    .line 1317
    .end local v0    # "copy_file":Ljava/io/File;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_5
    :pswitch_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-nez v8, :cond_6

    .line 1319
    if-eqz p3, :cond_6

    .line 1321
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getImageFile(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->original_file:Ljava/io/File;

    .line 1322
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1323
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1324
    .restart local v0    # "copy_file":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->original_file:Ljava/io/File;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 1327
    .end local v0    # "copy_file":Ljava/io/File;
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_a

    .line 1329
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1330
    .local v2, "full_path":Ljava/lang/String;
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1331
    .local v6, "tempPhotoBitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    .line 1332
    .local v4, "isSaved":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v7

    .line 1333
    .local v7, "tempUri":Landroid/net/Uri;
    if-eqz v6, :cond_7

    if-eqz v7, :cond_7

    .line 1335
    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveImageToInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    move-result v4

    .line 1338
    :cond_7
    if-eqz v4, :cond_9

    .line 1341
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_8

    .line 1343
    new-instance v1, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1345
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1347
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1350
    .end local v1    # "f":Ljava/io/File;
    :cond_8
    iput-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 1352
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->onSaveImageView()V

    .line 1354
    if-eqz v6, :cond_a

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_a

    .line 1356
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 1359
    .end local v2    # "full_path":Ljava/lang/String;
    .end local v4    # "isSaved":Z
    .end local v6    # "tempPhotoBitmap":Landroid/graphics/Bitmap;
    .end local v7    # "tempUri":Landroid/net/Uri;
    :cond_a
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isChangeImage:Z

    .line 1362
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->clearProfileImageCache()V

    goto/16 :goto_0

    .line 1261
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->showDiscardPopup()V

    .line 242
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 206
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth"

    const-string v3, "4001"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const v1, 0x7f0300b2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->setContentView(I)V

    .line 211
    if-eqz p1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    .line 216
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 218
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-gt v1, v2, :cond_1

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 222
    .local v0, "window":Landroid/view/Window;
    if-eqz v0, :cond_1

    .line 225
    const/16 v1, 0x400

    const/16 v2, 0x500

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 229
    .end local v0    # "window":Landroid/view/Window;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "TYPE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->initLayout(I)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->nameEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mEditNameWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 237
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->deleteTempFile()V

    .line 267
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 268
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 1864
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 1865
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 1866
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 1868
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1869
    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$15;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$15;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;Landroid/view/View;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1877
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 788
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 793
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 797
    :cond_1
    return-void
.end method

.method public setDefaultImage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1571
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07011e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 1572
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImageMan:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1573
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImageMan:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1574
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profilePressView:Landroid/view/View;

    const v1, 0x7f020280

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1575
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 1576
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImage:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1577
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isImage:Z

    .line 1578
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1560
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImageMan:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1561
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImage:Landroid/widget/ImageView;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 1562
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0205f0

    invoke-static {p1, v1, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1563
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profilePressView:Landroid/view/View;

    const v1, 0x7f020284

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1564
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 1565
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->profileImage:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090200

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1566
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isImage:Z

    .line 1567
    return-void
.end method
