.class public Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarMonthYearButton;
.super Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;
.source "CalendarMonthYearButton.java"


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public static newCalendarMonthYearButton(Landroid/content/Context;)Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarMonthYearButton;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarMonthYearButton;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarMonthYearButton;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method protected getButtonLayoutResourceId()I
    .locals 1

    .prologue
    .line 37
    const v0, 0x7f030020

    return v0
.end method

.method public setIsCurrent(Z)V
    .locals 1
    .param p1, "isCurrent"    # Z

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarMonthYearButton;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 66
    return-void
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1, "selected"    # Z

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarMonthYearButton;->mButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setSelected(Z)V

    .line 57
    return-void
.end method
