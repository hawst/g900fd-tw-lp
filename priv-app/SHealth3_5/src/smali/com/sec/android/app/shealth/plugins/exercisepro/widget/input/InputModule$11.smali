.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->showAbnormalGoalWarningPopupCalorie()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field goalValue:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;)V
    .locals 0

    .prologue
    .line 1013
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v2, 0x0

    .line 1018
    invoke-virtual {p3}, Landroid/app/Dialog;->dismiss()V

    .line 1019
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getValue()F

    move-result v0

    const v1, 0x461c1800    # 9990.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1020
    const/16 v0, 0x2706

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;->goalValue:I

    .line 1021
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;->goalValue:I

    int-to-float v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    .line 1027
    :goto_0
    return-void

    .line 1023
    :cond_0
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;->goalValue:I

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule$11;->goalValue:I

    int-to-float v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    goto :goto_0
.end method
