.class public abstract Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;
    }
.end annotation


# static fields
.field private static final DISABLED_BUTTON_ALPHA:F = 0.4f

.field private static final ENABLED_BUTTON_ALPHA:F = 1.0f

.field public static final FUTURE_ALERT_DIALOG:Ljava/lang/String; = "future_alert_dialog"

.field private static final MEASURE_TIME:Ljava/lang/String; = "measure_time"

.field private static mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;


# instance fields
.field protected button:Landroid/widget/Button;

.field protected calendar:Ljava/util/Calendar;

.field protected context:Landroid/content/Context;

.field protected dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mFormatter:Ljava/text/DateFormat;

.field private mFromDate:Ljava/util/Date;

.field private mOnBeforeClickListener:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    return-void
.end method

.method protected constructor <init>(Landroid/widget/Button;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "input button must be != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/Button;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getSimpleDateFormatter()Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mFormatter:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->initButton(Landroid/widget/Button;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->initDialog()V

    return-void
.end method

.method protected constructor <init>(Landroid/widget/Button;Ljava/util/Date;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mFromDate:Ljava/util/Date;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "input button must be != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/Button;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getSimpleDateFormatter()Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mFormatter:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->initButton(Landroid/widget/Button;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->initDialog()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;)Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mOnBeforeClickListener:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;

    return-object v0
.end method

.method private initButton(Landroid/widget/Button;)V
    .locals 2

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/FocusRemoverClickListenerDecorator;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;Landroid/widget/Button;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/FocusRemoverClickListenerDecorator;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->refresh()V

    return-void
.end method

.method private showAlertDialogForFuture()V
    .locals 4

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    const/4 v2, 0x0

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->popup_invalid_input_data:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->future_date_alert_message:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "future_alert_dialog"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showAlertDialogForPastBirthdate()V
    .locals 4

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    const/4 v2, 0x0

    sget v3, Lcom/sec/android/app/shealth/framework/ui/R$string;->popup_invalid_input_data:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->date_before_birth:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "future_alert_dialog"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method protected abstract getDialogTag()Ljava/lang/String;
.end method

.method protected abstract getSimpleDateFormatter()Ljava/text/DateFormat;
.end method

.method public getTimeInMillis()J
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method protected abstract initDialog()V
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    const-string/jumbo v1, "measure_time"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->refresh()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getDialogTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getDialogTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getDialogTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input Bundle == null. You should put not null Bundle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getDialogTag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getDialogTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_0
    const-string/jumbo v0, "measure_time"

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getDialogTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "input Bundle == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public refresh()V
    .locals 4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getSimpleDateFormatter()Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mFormatter:Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mFromDate:Ljava/util/Date;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mFormatter:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mFormatter:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mFromDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 2

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setAlpha(F)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->button:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    return-void

    :cond_0
    const v0, 0x3ecccccd    # 0.4f

    goto :goto_0
.end method

.method public setMeasureTime(J)Z
    .locals 2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->refresh()V

    const/4 v0, 0x1

    return v0
.end method

.method public setMeasureTimeWithErrorCheck(J)Z
    .locals 6

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->showAlertDialogForFuture()V

    :goto_0
    return v0

    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    cmp-long v2, v4, v2

    if-gez v2, :cond_1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->showAlertDialogForPastBirthdate()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->refresh()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setOnBeforeClickListener(Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->mOnBeforeClickListener:Lcom/sec/android/app/shealth/framework/ui/common/BasePicker$OnBeforeClickListener;

    return-void
.end method

.method public setTimeInMillis(J)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->calendar:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->refresh()V

    return-void
.end method

.method public showPopup()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->initDialog()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->context:Landroid/content/Context;

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/common/BasePicker;->getDialogTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void
.end method
