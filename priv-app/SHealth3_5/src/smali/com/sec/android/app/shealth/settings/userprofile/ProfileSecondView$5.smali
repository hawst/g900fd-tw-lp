.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$5;
.super Ljava/lang/Object;
.source "ProfileSecondView.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->showRangeDialog(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 9
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v3, 0x5

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 301
    const v2, 0x7f080836

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 302
    .local v0, "dialogText":Landroid/widget/TextView;
    const v2, 0x7f080837

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 304
    .local v1, "rangeText":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isRtlLanguage()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 306
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 307
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 309
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090845

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->mProfileHeightView:Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getListValue()I

    move-result v2

    if-nez v2, :cond_1

    .line 312
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090843

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v8, [Ljava/lang/Object;

    const/16 v5, 0x14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/16 v5, 0x12c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    :goto_0
    return-void

    .line 314
    :cond_1
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView$5;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileSecondView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090840

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    const/16 v5, 0x9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x3

    const/16 v6, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
