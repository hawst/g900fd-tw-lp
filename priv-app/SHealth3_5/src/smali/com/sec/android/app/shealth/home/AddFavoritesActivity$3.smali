.class Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;
.super Ljava/lang/Object;
.source "AddFavoritesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->registryContentObserver:Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$700(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Lcom/sec/android/app/shealth/home/AddFavoritesActivity$RegistryContentObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$800(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mSHealthList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$000(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setFavoritesStatus(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$900(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPluginlist:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$800(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    # getter for: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->mPartnersList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$400(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;)Ljava/util/ArrayList;

    move-result-object v2

    # invokes: Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->setFavoritesStatus(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->access$900(Lcom/sec/android/app/shealth/home/AddFavoritesActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/AddFavoritesActivity$3;->this$0:Lcom/sec/android/app/shealth/home/AddFavoritesActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/AddFavoritesActivity;->finish()V

    .line 333
    return-void
.end method
