.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;
.super Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;
.source "ProHorizontalInputModule.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method


# virtual methods
.method public bridge synthetic dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "x0"    # Landroid/view/KeyEvent;

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected getControllerView(Landroid/view/View;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 39
    const v1, 0x7f0807e9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    .line 40
    .local v0, "controller":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->setOrientation(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;)V

    .line 41
    return-object v0
.end method

.method public bridge synthetic getKmFromMeterByLocale(J)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # J

    .prologue
    .line 10
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getKmFromMeterByLocale(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getMilesFromMetersByLocale(J)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # J

    .prologue
    .line 10
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getMilesFromMetersByLocale(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRootLayoutId()I
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f0301cb

    return v0
.end method

.method public bridge synthetic getValue()F
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->getValue()F

    move-result v0

    return v0
.end method

.method public getValueEditText()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->mValueEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method public bridge synthetic hideEdittextBackground()V
    .locals 0

    .prologue
    .line 10
    invoke-super {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->hideEdittextBackground()V

    return-void
.end method

.method public bridge synthetic hideKeyboard()V
    .locals 0

    .prologue
    .line 10
    invoke-super {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->hideKeyboard()V

    return-void
.end method

.method protected initAdditionalViews(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 47
    return-void
.end method

.method public bridge synthetic isValueEmpty()Z
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->isValueEmpty()Z

    move-result v0

    return v0
.end method

.method public removeEditTextFocus()V
    .locals 0

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ProHorizontalInputModule;->moveFocusFromValueEditText()V

    .line 51
    return-void
.end method

.method public bridge synthetic setEditTextSize(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEditTextSize(I)V

    return-void
.end method

.method public bridge synthetic setEdittextChars()V
    .locals 0

    .prologue
    .line 10
    invoke-super {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEdittextChars()V

    return-void
.end method

.method public bridge synthetic setEnabled(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setEnabled(Z)V

    return-void
.end method

.method public bridge synthetic setFilterText(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setFilterText(Z)V

    return-void
.end method

.method public bridge synthetic setIncDecEnabled(IZ)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Z

    .prologue
    .line 10
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setIncDecEnabled(IZ)V

    return-void
.end method

.method public bridge synthetic setInputRange(FF)V
    .locals 0
    .param p1, "x0"    # F
    .param p2, "x1"    # F

    .prologue
    .line 10
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setInputRange(FF)V

    return-void
.end method

.method public bridge synthetic setMoveDistance(F)V
    .locals 0
    .param p1, "x0"    # F

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setMoveDistance(F)V

    return-void
.end method

.method public bridge synthetic setNormalRange(FF)V
    .locals 0
    .param p1, "x0"    # F
    .param p2, "x1"    # F

    .prologue
    .line 10
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setNormalRange(FF)V

    return-void
.end method

.method public bridge synthetic setType(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setType(I)V

    return-void
.end method

.method public bridge synthetic setUnit(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/CharSequence;

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setUnit(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic setValue(F)V
    .locals 0
    .param p1, "x0"    # F

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(F)V

    return-void
.end method

.method public bridge synthetic setValue(FZ)V
    .locals 0
    .param p1, "x0"    # F
    .param p2, "x1"    # Z

    .prologue
    .line 10
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValue(FZ)V

    return-void
.end method

.method public bridge synthetic setValueToController(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 10
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/InputModule;->setValueToController(I)V

    return-void
.end method
