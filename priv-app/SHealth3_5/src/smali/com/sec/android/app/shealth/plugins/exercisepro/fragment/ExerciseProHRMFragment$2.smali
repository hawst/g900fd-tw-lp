.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;
.super Ljava/lang/Object;
.source "ExerciseProHRMFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->createMeasurementDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    const/4 v3, 0x0

    .line 263
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "sth  AlertHRMMeasure onContentInitialization"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f0805ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f0805cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f090c22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f0805c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f0805c9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCircleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$802(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f080595

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondLayout:Landroid/widget/FrameLayout;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f0805c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseLayout:Landroid/widget/FrameLayout;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/FrameLayout;)Landroid/widget/FrameLayout;

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f0805c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewLayout:Landroid/widget/LinearLayout;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f0805c5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1202(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Lcom/sec/android/app/shealth/heartrate/HRMGIFView;)Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterPulseViewGreenIcon:Lcom/sec/android/app/shealth/heartrate/HRMGIFView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/heartrate/HRMGIFView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->pulseAnimationID:[I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)[I

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/HRMGIFView;->setMultiMovieResource([IZ)V

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const v0, 0x7f0805c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mSecondCenterIconBig:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1402(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mCurrentActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->initAoudioFile()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)V

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mScaleAnimation:Landroid/view/animation/Animation;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsRunPulseView:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1802(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)Z

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsOnConfigChanged:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_HRM_Measure:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mAlertBuild_Error:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mIsOnConfigChanged:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$1902(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;Z)Z

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->isMeasureCompleted:Z

    if-nez v0, :cond_2

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->startSensorAndDelayUIForDetectedFinger(I)V

    goto :goto_0

    .line 319
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHeartrateDatabaseHelper:Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$2300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getDataCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mFirstStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->startSensorAndDelayUIForDetectedFinger(I)V

    goto :goto_0

    .line 325
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment$2;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
