.class public Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;
.super Landroid/app/Service;
.source "CignaDaybreakService.java"


# static fields
.field private static final END_OF_DAY_REQUEST:I = 0xe

.field public static final HANDLE_MIDNIGHT:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.HANDLE_MIDNIGHT"

.field private static final TAG:Ljava/lang/String;

.field private static isRun:Z


# instance fields
.field private final mBinder:Landroid/os/IBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->TAG:Ljava/lang/String;

    .line 23
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 42
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->mBinder:Landroid/os/IBinder;

    return-void
.end method

.method public static cancelMidNightAlarm()V
    .locals 6

    .prologue
    .line 119
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 120
    .local v3, "i":Landroid/content/Intent;
    const-string v4, "com.sec.android.app.shealth.cignacoach.HANDLE_MIDNIGHT"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 122
    .local v1, "appContext":Landroid/content/Context;
    const/16 v4, 0xe

    const/high16 v5, 0x8000000

    invoke-static {v1, v4, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 124
    .local v2, "endOfDayPendingIntent":Landroid/app/PendingIntent;
    const-string v4, "alarm"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 125
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 126
    return-void
.end method

.method public static isRun()Z
    .locals 1

    .prologue
    .line 29
    sget-boolean v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun:Z

    return v0
.end method

.method public static setMidNightAlarm()V
    .locals 9

    .prologue
    .line 106
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 107
    .local v8, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth.cignacoach.HANDLE_MIDNIGHT"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 109
    .local v7, "appContext":Landroid/content/Context;
    const/16 v1, 0xe

    const/high16 v2, 0x8000000

    invoke-static {v7, v1, v8, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 111
    .local v6, "endOfDayPendingIntent":Landroid/app/PendingIntent;
    const-string v1, "alarm"

    invoke-virtual {v7, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 112
    .local v0, "am":Landroid/app/AlarmManager;
    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 114
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 9

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 62
    sget-boolean v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun:Z

    if-eqz v1, :cond_0

    .line 63
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->TAG:Ljava/lang/String;

    const-string v2, "CignaDaybreakService is already running!!"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :goto_0
    return-void

    .line 66
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->TAG:Ljava/lang/String;

    const-string v2, "CignaDaybreakService onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun:Z

    .line 70
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 71
    .local v8, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.shealth.cignacoach.HANDLE_MIDNIGHT"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 73
    .local v7, "appContext":Landroid/content/Context;
    const/16 v1, 0xe

    const/high16 v2, 0x8000000

    invoke-static {v7, v1, v8, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 75
    .local v6, "endOfDayPendingIntent":Landroid/app/PendingIntent;
    const-string v1, "alarm"

    invoke-virtual {v7, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 76
    .local v0, "am":Landroid/app/AlarmManager;
    const/4 v1, 0x0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 83
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->TAG:Ljava/lang/String;

    const-string v1, "CignaDaybreakService onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->isRun:Z

    .line 85
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->cancelMidNightAlarm()V

    .line 86
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->TAG:Ljava/lang/String;

    const-string v1, "CignaDaybreakService onStartCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->TAG:Ljava/lang/String;

    const-string v1, "CignaDaybreakService onTaskRemoved"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-super {p0, p1}, Landroid/app/Service;->onTaskRemoved(Landroid/content/Intent;)V

    .line 48
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/receiver/CignaDaybreakService;->TAG:Ljava/lang/String;

    const-string v1, "CignaDaybreakService onUnbind"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
