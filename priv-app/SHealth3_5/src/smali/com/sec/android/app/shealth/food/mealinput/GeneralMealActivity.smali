.class public abstract Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "GeneralMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/food/mealinput/IMealActivityAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$SkippedMealConfirmController;,
        Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$QuickInputDialogController;,
        Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DeleteMealButtonController;,
        Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$AddToMyMealButtonController;,
        Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$DiscardChangesDialogButtonController;
    }
.end annotation


# static fields
.field private static final ADD_TO_MY_MEALS_DIALOG:Ljava/lang/String; = "add_to_saved_meals_dialog"

.field private static final DELETE_DIALOG_TAG:Ljava/lang/String; = "delete_dialog_tag"

.field private static final DISCARD_CHANGES_DIALOG_TAG:Ljava/lang/String; = "discard_changes_dialog"

.field protected static final KEY_FRAGMENTS_INITED:Ljava/lang/String; = "fragment_inited"

.field private static final KEY_MEAL_DATA_HOLDER:Ljava/lang/String; = "fragment_meal_data_holder"

.field private static final LOG_TAG:Ljava/lang/String;

.field protected static final NO_BACKGROUND_RESOURCE:I = 0x0

.field private static final OUT_OF_RANGE_POPUP:Ljava/lang/String; = "out_of_range_popup"

.field private static final QUICK_INPUT_POP_UP:Ljava/lang/String; = "quick_input_poup"

.field protected static final SAVE_ACTION_BAR_BUTTON_INDEX:I = 0x1

.field private static final SKIPPED_MEAL_CONFIRM_DIALOG:Ljava/lang/String; = "skipped_meal_confirm_dialog"


# instance fields
.field protected fromQuickInput:Z

.field private mAddToMyMealDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field protected mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

.field private mMaxAcceptableCaloriesValue:F

.field private mMealImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealImageData;",
            ">;"
        }
    .end annotation
.end field

.field private mMealItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mMyMealEditTextFromPopup:Landroid/widget/EditText;

.field mToast:Landroid/widget/Toast;

.field protected mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

.field protected mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

.field protected mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

.field protected mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

.field protected mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

.field protected mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

.field protected mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

.field protected mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    const-class v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 149
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$1;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 688
    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    .prologue
    .line 84
    iget v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMaxAcceptableCaloriesValue:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMyMealEditTextFromPopup:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;
    .param p1, "x1"    # Landroid/widget/EditText;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMyMealEditTextFromPopup:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->generateDefaultNameForMyMeal()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mAddToMyMealDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method private generateDefaultNameForMyMeal()Ljava/lang/String;
    .locals 8

    .prologue
    .line 501
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

    invoke-interface {v4}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;->getAllDatas()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 502
    .local v0, "allMyMealSize":I
    const-string v2, ""

    .line 505
    .local v2, "stringMyMeal":Ljava/lang/String;
    :cond_0
    const v4, 0x7f090935

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 506
    .local v3, "tempMealName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

    invoke-interface {v4, v3}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;->getMealsByName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 507
    .local v1, "mealDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 508
    move-object v2, v3

    .line 511
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    const v4, 0x7fffffff

    if-ne v0, v4, :cond_0

    .line 512
    :cond_2
    return-object v2
.end method

.method private initDaoObjects()V
    .locals 1

    .prologue
    .line 372
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    .line 373
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    .line 374
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    .line 375
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFoodImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

    .line 376
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mFoodInfoDao:Lcom/sec/android/app/shealth/food/fooddao/FoodInfoDao;

    .line 377
    return-void
.end method

.method private showDeleteDialog()V
    .locals 3

    .prologue
    const v2, 0x7f090035

    .line 516
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09078e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "delete_dialog_tag"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 521
    return-void
.end method

.method private showDiscardChangesPopup()V
    .locals 3

    .prologue
    .line 334
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    const v2, 0x7f090081

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v1, 0x7f090083

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "discard_changes_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 339
    return-void
.end method


# virtual methods
.method protected actionCancel()V
    .locals 1

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealWasEdited()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->showDiscardChangesPopup()V

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->finish()V

    goto :goto_0
.end method

.method protected areFragmentsInitialized(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 240
    if-eqz p1, :cond_0

    const-string v1, "fragment_inited"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected checkCaloriesRangeAndShowPopup(F)Z
    .locals 4
    .param p1, "max"    # F

    .prologue
    const/4 v0, 0x0

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getTotalKcal()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v1, v1, p1

    if-lez v1, :cond_0

    .line 430
    iput p1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMaxAcceptableCaloriesValue:F

    .line 431
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v2, 0x7f0900e3

    invoke-direct {v1, p0, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v2, 0x7f030009

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string/jumbo v3, "out_of_range_popup"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 436
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected checkCaloriesRangeAndShowPopupDespiteTodayData()Z
    .locals 1

    .prologue
    .line 417
    const v0, 0x47c34f80    # 99999.0f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->checkCaloriesRangeAndShowPopup(F)Z

    move-result v0

    return v0
.end method

.method protected createMealItemFragment()Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    .locals 1

    .prologue
    .line 407
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;-><init>()V

    return-object v0
.end method

.method protected doesMyMealNameExistInDb(Ljava/lang/String;)Z
    .locals 3
    .param p1, "mealName"    # Ljava/lang/String;

    .prologue
    .line 492
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;->getAllDatas()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 493
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 494
    const/4 v2, 0x1

    .line 497
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 546
    const-string/jumbo v0, "out_of_range_popup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 547
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$2;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V

    .line 600
    :goto_0
    return-object v0

    .line 563
    :cond_0
    const-string v0, "PORTION_SIZE_POPUP_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 564
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0

    .line 565
    :cond_1
    const-string v0, "add_to_saved_meals_dialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 566
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$3;-><init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V

    goto :goto_0

    .line 591
    :cond_2
    const-string/jumbo v0, "quick_input_poup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    if-eqz v0, :cond_4

    .line 593
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0

    .line 595
    :cond_3
    const-string v0, "REMOVE_MEAL_ITEM_DIALOG_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 596
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    if-eqz v0, :cond_4

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    check-cast v0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0

    .line 600
    :cond_4
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    .line 255
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onDialogNotSupported(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 222
    const v0, 0x7f0300e2

    return v0
.end method

.method protected handleReceivedMealItems(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->onNewIntent(Landroid/content/Intent;)V

    .line 304
    return-void
.end method

.method protected initFragments(Z)V
    .locals 6
    .param p1, "isFirstInit"    # Z

    .prologue
    const v5, 0x7f0803d8

    const v4, 0x7f0803a9

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 388
    .local v0, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    if-eqz p1, :cond_0

    .line 389
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 390
    .local v1, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->createMealItemFragment()Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    move-result-object v3

    .line 391
    .local v3, "mealItemsFragment":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    iput-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    .line 392
    invoke-virtual {v1, v4, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 393
    new-instance v2, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;-><init>()V

    .line 394
    .local v2, "mealImagesFragment":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;
    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    .line 395
    invoke-virtual {v1, v5, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 396
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 401
    .end local v1    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    .end local v2    # "mealImagesFragment":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;
    .end local v3    # "mealItemsFragment":Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;
    :goto_0
    return-void

    .line 398
    :cond_0
    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    .line 399
    invoke-virtual {v0, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    iput-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    goto :goto_0
.end method

.method protected abstract initMemo()V
.end method

.method protected insertMealDataToFragments()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealImages:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->addListMealImages(Ljava/util/List;)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealItems:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->addMealItemList(Ljava/util/List;)V

    .line 291
    return-void
.end method

.method protected loadMealDataFromDb(J)V
    .locals 3
    .param p1, "mealId"    # J

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    if-nez v0, :cond_0

    .line 267
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->calculateMealTypeAccordingToCurrentTime()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealType(I)V

    .line 269
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get MealData by id= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealImageDao:Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/food/fooddao/MealImageDao;->getImagesListByMealId(J)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealImages:Ljava/util/List;

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealImages:Ljava/util/List;

    if-nez v0, :cond_1

    .line 273
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealImages:Ljava/util/List;

    .line 274
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get images list by id= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemDao:Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/food/fooddao/MealItemDao;->getMealItemsListByMealId(J)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealItems:Ljava/util/List;

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealItems:Ljava/util/List;

    if-nez v0, :cond_2

    .line 278
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealItems:Ljava/util/List;

    .line 279
    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t get meal items list by id= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_2
    return-void
.end method

.method protected mealWasEdited()Z
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const-wide/high16 v6, -0x8000000000000000L

    .line 162
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    if-nez v2, :cond_0

    .line 164
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 165
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v2, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 167
    const/high16 v2, 0x4000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 168
    const/high16 v2, 0x20000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 169
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->startActivity(Landroid/content/Intent;)V

    .line 171
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    .line 173
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->initDaoObjects()V

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 175
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "MEAL_DATA_HOLDER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    if-nez v2, :cond_1

    .line 177
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'s inheritance must start with extra: key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "MEAL_DATA_HOLDER"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 180
    :cond_1
    if-eqz p1, :cond_2

    .line 181
    const-string v2, "fragment_meal_data_holder"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    .line 182
    const-string v2, "MAX_ACCEPTABLE_CALORIES_VALUE"

    iget v4, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMaxAcceptableCaloriesValue:F

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMaxAcceptableCaloriesValue:F

    .line 185
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v4

    cmp-long v2, v4, v6

    if-eqz v2, :cond_4

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->loadMealDataFromDb(J)V

    .line 203
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 204
    sget-object v2, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onCreate "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getLayoutId()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->setContentView(I)V

    .line 206
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->areFragmentsInitialized(Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_9

    move v2, v3

    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->initFragments(Z)V

    .line 208
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    instance-of v2, v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    if-eqz v2, :cond_3

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    check-cast v2, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->setQuickInputMealType(I)V

    .line 215
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->initMemo()V

    .line 216
    return-void

    .line 188
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v4

    cmp-long v2, v4, v6

    if-nez v2, :cond_6

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealType()J

    move-result-wide v4

    cmp-long v2, v4, v6

    if-nez v2, :cond_5

    .line 190
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->fromQuickInput:Z

    .line 192
    :cond_5
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 194
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealTime()J

    move-result-wide v4

    cmp-long v2, v4, v6

    if-eqz v2, :cond_7

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealTime(J)V

    .line 197
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealType()J

    move-result-wide v4

    cmp-long v2, v4, v6

    if-eqz v2, :cond_8

    .line 198
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealType()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealType(I)V

    goto/16 :goto_0

    .line 200
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealData:Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->calculateMealTypeAccordingToCurrentTime()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->setMealType(I)V

    goto/16 :goto_0

    .line 206
    :cond_9
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 442
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f100010

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 443
    const/4 v1, 0x1

    return v1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 295
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 296
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->handleReceivedMealItems(Landroid/content/Intent;)V

    .line 297
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 458
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 480
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    .line 460
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->checkCaloriesRangeAndShowPopupDespiteTodayData()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 463
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDaoMyFood:Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;

    invoke-interface {v2}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoMyFood;->getAllDatas()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 464
    .local v0, "myFoodSize":I
    const/16 v2, 0x1e

    if-lt v0, v2, :cond_1

    .line 465
    const v2, 0x7f090925

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->showToast(Ljava/lang/String;)Landroid/widget/Toast;

    goto :goto_0

    .line 468
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v3, 0x2

    const v4, 0x7f09093a

    invoke-direct {v2, p0, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    const v3, 0x7f0300c1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f09004f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->doNotDismissOnOkClick()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mAddToMyMealDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 474
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mAddToMyMealDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "add_to_saved_meals_dialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 477
    .end local v0    # "myFoodSize":I
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->showDeleteDialog()V

    goto :goto_0

    .line 458
    :sswitch_data_0
    .sparse-switch
        0x7f080c8d -> :sswitch_1
        0x7f080c97 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->getMealId()J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->areFragmentsInitialized(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->insertMealDataToFragments()V

    .line 231
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 232
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 448
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/app/IFoodConfigurationHolder;->getConfiguration()Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;

    move-result-object v1

    .line 449
    .local v1, "foodConfig":Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;
    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/app/IFoodConfiguration;->isMyMealEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v3}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->getAllMealItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v2, 0x1

    .line 451
    .local v2, "isMyMealVisible":Z
    :goto_0
    const v3, 0x7f080c97

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 452
    .local v0, "addToMyMeals":Landroid/view/MenuItem;
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 453
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3

    .line 449
    .end local v0    # "addToMyMeals":Landroid/view/MenuItem;
    .end local v2    # "isMyMealVisible":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 352
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 353
    const-string v0, "fragment_inited"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 354
    const-string v0, "fragment_meal_data_holder"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealDataHolder:Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 355
    const-string v0, "MAX_ACCEPTABLE_CALORIES_VALUE"

    iget v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMaxAcceptableCaloriesValue:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 356
    return-void
.end method

.method protected resetDataInFragments()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealItemsFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/IMealItemsFragmentAction;->resetAllData()V

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mealImagesFragmentAction:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/IMealImagesFragmentAction;->resetAllData()V

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealImages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMealItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 366
    return-void
.end method

.method protected setDoneButtonState(Z)V
    .locals 2
    .param p1, "isEnabled"    # Z

    .prologue
    const/4 v1, 0x1

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    .line 313
    .local v0, "actionBarHelper":Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    if-eqz v0, :cond_0

    .line 314
    if-eqz p1, :cond_1

    .line 315
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->enableActionBarButton(I)V

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    goto :goto_0
.end method

.method protected showToast(Ljava/lang/String;)Landroid/widget/Toast;
    .locals 2
    .param p1, "toastText"    # Ljava/lang/String;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 534
    const-string v0, ""

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mToast:Landroid/widget/Toast;

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 541
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mToast:Landroid/widget/Toast;

    return-object v0
.end method
