.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "ExerciseProWebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "mWebChromeClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$1;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "newProgress"    # I

    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    .line 76
    const/16 v0, 0x46

    if-lt p2, v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;->dismiss()V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity$mWebChromeClient;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->dialog:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;->access$202(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWebViewActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDlg;

    .line 82
    :cond_0
    return-void
.end method
