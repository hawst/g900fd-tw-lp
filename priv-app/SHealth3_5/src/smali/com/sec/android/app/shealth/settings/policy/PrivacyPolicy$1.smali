.class Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;
.super Ljava/lang/Object;
.source "PrivacyPolicy.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 178
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$000(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setSelected(Z)V

    .line 180
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 186
    :cond_0
    :goto_0
    return v1

    .line 181
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$000(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Landroid/widget/CheckBox;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$1;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$000(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 183
    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 182
    goto :goto_1
.end method
