.class Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;
.super Ljava/lang/Object;
.source "WeightInputActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->showAlertDialog(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

.field final synthetic val$input:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;->val$input:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/app/Activity;)V
    .locals 1
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;->val$input:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;->dismiss()V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$000(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$000(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->requestFocus()Z

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->isConncetedToHWKeyboard:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$100(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$2;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$000(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->showKeyboard()V

    .line 262
    :cond_0
    return-void
.end method
