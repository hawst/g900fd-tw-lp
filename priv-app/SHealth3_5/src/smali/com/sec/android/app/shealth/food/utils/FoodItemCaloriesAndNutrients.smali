.class public Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;
.super Ljava/lang/Object;
.source "FoodItemCaloriesAndNutrients.java"


# instance fields
.field private calories:I

.field private carbohydrates:F

.field private fat:F

.field private proteins:F


# direct methods
.method public constructor <init>(IFFF)V
    .locals 2
    .param p1, "calories"    # I
    .param p2, "proteins"    # F
    .param p3, "carbohydrates"    # F
    .param p4, "fat"    # F

    .prologue
    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->calories:I

    .line 11
    iput v1, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->proteins:F

    .line 12
    iput v1, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->carbohydrates:F

    .line 13
    iput v1, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->fat:F

    .line 17
    iput p1, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->calories:I

    .line 18
    iput p2, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->proteins:F

    .line 19
    iput p3, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->carbohydrates:F

    .line 20
    iput p4, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->fat:F

    .line 21
    return-void
.end method


# virtual methods
.method public getCalories()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->calories:I

    return v0
.end method

.method public getCarbos()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->carbohydrates:F

    return v0
.end method

.method public getFat()F
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->fat:F

    return v0
.end method

.method public getProteins()F
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/shealth/food/utils/FoodItemCaloriesAndNutrients;->proteins:F

    return v0
.end method
