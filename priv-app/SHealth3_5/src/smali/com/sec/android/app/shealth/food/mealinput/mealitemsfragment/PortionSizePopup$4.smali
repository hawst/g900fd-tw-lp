.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$4;
.super Ljava/lang/Object;
.source "PortionSizePopup.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText$OnFloatInputChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->initPortionSizeInputModule(Landroid/view/View;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValueChanged(Ljava/lang/Float;)V
    .locals 2
    .param p1, "newValue"    # Ljava/lang/Float;

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mPortionSizeInputModule:Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/mealinput/portionsize/PortionSizeInputModule;->isOutOfRange()Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mInputModuleValue:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$802(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;F)F

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->mTextViewTotalKcalInPrecisePopup:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$900(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->calculateAndSetTotalKcalTexView(Landroid/widget/TextView;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;->access$1000(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/PortionSizePopup;Landroid/widget/TextView;)V

    .line 340
    :cond_0
    return-void
.end method
