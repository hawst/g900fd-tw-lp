.class public final enum Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;
.super Ljava/lang/Enum;
.source "FeatureTable.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/config/FeatureTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BloodPressure"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;",
        ">;",
        "Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

.field public static final enum NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

.field public static final enum SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    const-string v1, "SUPPORTED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    .line 68
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    const-string v1, "NOT_SUPPORTED"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    const-class v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$BloodPressure;

    return-object v0
.end method
