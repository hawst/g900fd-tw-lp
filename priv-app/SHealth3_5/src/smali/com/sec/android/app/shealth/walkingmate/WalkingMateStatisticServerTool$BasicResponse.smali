.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;
.super Ljava/lang/Object;
.source "WalkingMateStatisticServerTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BasicResponse"
.end annotation


# instance fields
.field private avgStep:I

.field private bestStep:I

.field private calorie:I

.field private createTime:Ljava/lang/String;

.field private distance:I

.field private jsonObject:Lorg/json/JSONObject;

.field private percentile:D

.field private ranking:I

.field private step:I

.field private totalUserNum:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "jsonString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->createTime:Ljava/lang/String;

    .line 276
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->step:I

    .line 277
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->distance:I

    .line 278
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->calorie:I

    .line 279
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->ranking:I

    .line 280
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->percentile:D

    .line 281
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->totalUserNum:I

    .line 282
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->avgStep:I

    .line 283
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->bestStep:I

    .line 302
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "basicResponse"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$ServerException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->createTime:Ljava/lang/String;

    .line 276
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->step:I

    .line 277
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->distance:I

    .line 278
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->calorie:I

    .line 279
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->ranking:I

    .line 280
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->percentile:D

    .line 281
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->totalUserNum:I

    .line 282
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->avgStep:I

    .line 283
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->bestStep:I

    .line 287
    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->checkJson(Lorg/json/JSONObject;)V
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->access$000(Lorg/json/JSONObject;)V

    .line 288
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->jsonObject:Lorg/json/JSONObject;

    .line 289
    const-string v0, "createTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->createTime:Ljava/lang/String;

    .line 290
    const-string/jumbo v0, "step"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->step:I

    .line 291
    const-string v0, "distance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->distance:I

    .line 292
    const-string v0, "calorie"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->calorie:I

    .line 293
    const-string/jumbo v0, "ranking"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->ranking:I

    .line 294
    const-string/jumbo v0, "percentile"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "percentile"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->percentile:D

    .line 295
    :cond_0
    const-string/jumbo v0, "totalUserNum"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->totalUserNum:I

    .line 296
    const-string v0, "avgStep"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->avgStep:I

    .line 297
    const-string v0, "bestStep"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->bestStep:I

    .line 298
    return-void
.end method


# virtual methods
.method public getAvgStep()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->avgStep:I

    return v0
.end method

.method public getBestStep()I
    .locals 1

    .prologue
    .line 341
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->bestStep:I

    return v0
.end method

.method public getCalorie()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->calorie:I

    return v0
.end method

.method public getCreateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->createTime:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance()I
    .locals 1

    .prologue
    .line 313
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->distance:I

    return v0
.end method

.method public getJsonObject()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->jsonObject:Lorg/json/JSONObject;

    return-object v0
.end method

.method public getPercentile()D
    .locals 2

    .prologue
    .line 329
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->percentile:D

    return-wide v0
.end method

.method public getRanking()I
    .locals 1

    .prologue
    .line 325
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->ranking:I

    return v0
.end method

.method public getStep()I
    .locals 1

    .prologue
    .line 309
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->step:I

    return v0
.end method

.method public getTotalUserNum()I
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->totalUserNum:I

    return v0
.end method

.method public setCalorie(I)V
    .locals 0
    .param p1, "calorie"    # I

    .prologue
    .line 321
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicResponse;->calorie:I

    .line 322
    return-void
.end method
