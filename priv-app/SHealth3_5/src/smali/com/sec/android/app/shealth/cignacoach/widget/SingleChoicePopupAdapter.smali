.class public Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;
.super Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;
.source "SingleChoicePopupAdapter.java"


# instance fields
.field protected isChecked:[Z


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;[Z)V
    .locals 0
    .param p3, "checked"    # [Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[Z)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "descriptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialogAdapter;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 37
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->isChecked:[Z

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->onDialogListItemClick:Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->onDialogListItemClick:Lcom/sec/android/app/shealth/common/commonui/dialog/OnDialogListItemClick;

    return-object v0
.end method


# virtual methods
.method public getIndexChecked()I
    .locals 2

    .prologue
    .line 122
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->isChecked:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->isChecked:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_1

    .line 127
    :cond_0
    return v0

    .line 122
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 43
    new-instance v2, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->descriptions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->isChecked:[Z

    aget-boolean v3, v3, p1

    invoke-direct {v2, v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v2
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 149
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v10, 0x7f09020b

    const/4 v6, 0x0

    .line 50
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;

    .line 51
    .local v2, "itemModel":Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;
    if-nez p2, :cond_1

    .line 53
    if-nez p3, :cond_0

    .line 54
    const/4 v6, 0x0

    .line 110
    :goto_0
    return-object v6

    .line 55
    :cond_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 56
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f0301f3

    invoke-virtual {v1, v7, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 59
    new-instance v7, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;)V

    invoke-virtual {p2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    const v7, 0x7f08080a

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 81
    .local v5, "tv":Landroid/widget/TextView;
    if-eqz v5, :cond_2

    .line 83
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    :cond_2
    const v7, 0x7f080778

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 87
    .local v3, "radioButton":Landroid/widget/RadioButton;
    if-eqz v3, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 88
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->isChecked()Z

    move-result v7

    invoke-virtual {v3, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 89
    :cond_3
    invoke-virtual {p2, p1}, Landroid/view/View;->setId(I)V

    .line 91
    const-string v4, " "

    .line 93
    .local v4, "space":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->getDescription()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09022e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09020a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/CheckableTextItemModel;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 99
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09022b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->mdisabledIndex:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const/4 v6, 0x1

    :cond_4
    invoke-virtual {p2, v6}, Landroid/view/View;->setEnabled(Z)V

    move-object v6, p2

    .line 110
    goto/16 :goto_0

    .line 104
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09022c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setIndexChecked(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 133
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->isChecked:[Z

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-gt p1, v1, :cond_0

    if-gez p1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->isChecked:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->isChecked:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    .line 137
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/SingleChoicePopupAdapter;->isChecked:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, p1

    goto :goto_0
.end method
