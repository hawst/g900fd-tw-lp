.class Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;
.super Ljava/lang/Object;
.source "EveryoneRankingsFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0

    .prologue
    .line 814
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemCleck(Landroid/view/View;I)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 818
    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mIsBlocked:Z
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1700()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 839
    :cond_0
    :goto_0
    return-void

    .line 819
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->blockManyClick()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    .line 823
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;

    move-object v3, v0

    .line 824
    .local v3, "rankingsItem":Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
    if-eqz v3, :cond_0

    .line 825
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getValuesHolder()Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v5

    if-ne v4, v5, :cond_2

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;-><init>(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V

    .line 828
    .local v1, "dialog":Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;
    :goto_1
    if-eqz v1, :cond_0

    .line 829
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 832
    .end local v1    # "dialog":Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;
    .end local v3    # "rankingsItem":Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
    :catch_0
    move-exception v2

    .line 833
    .local v2, "e":Ljava/lang/ClassCastException;
    invoke-virtual {v2}, Ljava/lang/ClassCastException;->printStackTrace()V

    .line 834
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "ClassCastException"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->E(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 825
    .end local v2    # "e":Ljava/lang/ClassCastException;
    .restart local v3    # "rankingsItem":Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
    :cond_2
    :try_start_1
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;->getValuesHolder()Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/dialog/StatisticsDialogFragment;-><init>(Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 835
    .end local v3    # "rankingsItem":Lcom/sec/android/app/shealth/walkingmate/listview/RankingsItem;
    :catch_1
    move-exception v2

    .line 836
    .local v2, "e":Ljava/lang/IllegalStateException;
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$8;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "IllegaStateExceotion because of processing multiple events"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
