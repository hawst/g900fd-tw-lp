.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;
.super Landroid/os/Handler;
.source "ExerciseProMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V
    .locals 0

    .prologue
    .line 1974
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1976
    iget v4, p1, Landroid/os/Message;->arg1:I

    packed-switch v4, :pswitch_data_0

    .line 2002
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1979
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1980
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->createMeasurementDialog()V

    goto :goto_0

    .line 1985
    :pswitch_2
    iget v1, p1, Landroid/os/Message;->arg2:I

    .line 1986
    .local v1, "hrmValue":I
    if-eqz v1, :cond_1

    .line 1987
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->insertHRMData(I)V

    .line 1990
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 1991
    .local v2, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 1992
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1993
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1997
    .end local v2    # "manager":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .line 1998
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->getHeartRateData()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$3000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)V

    .line 1999
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->addHRMMarkerToMap()V

    goto :goto_0

    .line 1994
    :catch_0
    move-exception v0

    .line 1995
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 1976
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
