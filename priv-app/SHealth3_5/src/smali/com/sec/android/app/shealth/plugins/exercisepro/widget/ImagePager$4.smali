.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;
.super Ljava/lang/Object;
.source "ImagePager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 265
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mSynObject:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 266
    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_0

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 267
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;

    .line 268
    .local v0, "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v1

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;->mData:Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->cashMap:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;->mData:Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->checkVisibility()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->notifyDataSetChanged()V

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->notifyImagesCountChanged()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V

    .line 274
    .end local v0    # "holder":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;
    :cond_0
    const/4 v1, 0x1

    monitor-exit v2

    return v1

    .line 275
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
