.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver$1;
.super Ljava/lang/Object;
.source "WalkingMateActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;->onChange(ZLandroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->mWalkingMateSummaryFragment:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->access$200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateConnectionStatus()V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->allowDeviceConnectionSwitch(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver$1;->this$1:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateActivity;->switchToWearableScreen()V

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    const-string/jumbo v0, "mSummaryFrag is not attached"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto :goto_0
.end method
