.class public Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;
.super Ljava/lang/Object;
.source "AwardMedalActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field intentLogAction:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mView"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mView:Landroid/view/View;

    .line 397
    new-instance v0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity$1;-><init>(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->intentLogAction:Landroid/view/View$OnClickListener;

    .line 62
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mView:Landroid/view/View;

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->initializeMedalArea()V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getBurnCalorieList()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 327
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT total(T.[total_calorie]) as total_cal, strftime(\"%Y-%m-%d\", (T.[start_time]/1000),\'unixepoch\',\"localtime\") as sample_time  from ( SELECT A.[total_calorie], A.[start_time]  From exercise A where A.exercise_type != 20003 Union Select B.[calorie], B.[start_time] From ( walk_info join user_device on walk_info.user_device__id == user_device._id)  B  where B.sync_status != 170004 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getDeviceTypQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") T "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " group by strftime(\"%Y-%m-%d\", (T.[start_time]/1000),\'unixepoch\',\"localtime\") ORDER BY T.[start_time] ASC "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 335
    .local v3, "selectionClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 336
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 337
    .local v8, "exerciseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;>;"
    if-eqz v6, :cond_1

    .line 338
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 339
    const/4 v7, 0x0

    .line 340
    .local v7, "exerciseData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    new-instance v7, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    .end local v7    # "exerciseData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    invoke-direct {v7}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;-><init>()V

    .line 342
    .restart local v7    # "exerciseData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    const-string/jumbo v0, "total_cal"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->setValue(F)V

    .line 343
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->setSampleTime(Ljava/lang/String;)V

    .line 344
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 347
    .end local v7    # "exerciseData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 349
    :cond_1
    return-object v8
.end method

.method private getBurnCalorieMedalNum(Landroid/widget/LinearLayout;)V
    .locals 14
    .param p1, "item"    # Landroid/widget/LinearLayout;

    .prologue
    const v13, 0x7f080066

    const v12, 0x7f080065

    .line 209
    const v8, 0x9c4b

    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getGoalList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 210
    .local v2, "goalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getBurnCalorieList()Ljava/util/ArrayList;

    move-result-object v0

    .line 211
    .local v0, "burnCalorieList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;>;"
    const/4 v7, 0x0

    .line 212
    .local v7, "medalget":I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v8

    int-to-long v8, v8

    long-to-double v8, v8

    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getBMR()D

    move-result-wide v10

    sub-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    double-to-int v1, v8

    .line 213
    .local v1, "defaultValue":I
    int-to-long v3, v1

    .line 215
    .local v3, "goalValue":J
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v5, v8, :cond_3

    .line 216
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v6, v8, :cond_1

    .line 217
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->getGoalTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->getSampleTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 218
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->getValue()J

    move-result-wide v3

    .line 216
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 222
    :cond_1
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->getValue()F

    move-result v8

    long-to-float v9, v3

    cmpl-float v8, v8, v9

    if-ltz v8, :cond_2

    .line 223
    add-int/lit8 v7, v7, 0x1

    .line 215
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 226
    .end local v6    # "j":I
    :cond_3
    invoke-virtual {p1, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 227
    if-lez v7, :cond_4

    .line 228
    invoke-virtual {p1, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const v9, 0x7f020238

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 229
    invoke-virtual {p1, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 230
    invoke-virtual {p1, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    invoke-virtual {p1, v13}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const-string v9, "#ff000000"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 237
    :goto_2
    sget-object v8, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "exerciseList medal get : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    return-void

    .line 233
    :cond_4
    invoke-virtual {p1, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const v9, 0x7f02023c

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2
.end method

.method private getDeviceTypQuery()Ljava/lang/String;
    .locals 4

    .prologue
    .line 268
    const/4 v1, 0x0

    .line 269
    .local v1, "query":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    .line 270
    .local v0, "deviceType":I
    const/16 v2, 0x272f

    if-ne v0, v2, :cond_0

    .line 271
    const-string v1, " AND (device_type != 10009 AND device_type != 10023)"

    .line 276
    :goto_0
    return-object v1

    .line 274
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " AND device_type == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getExerciseList()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 242
    const-string v3, "SELECT sum(E.[total_calorie]) as total_cal ,strftime(\"%Y-%m-%d\", (E.[start_time]/1000),\'unixepoch\',\"localtime\") as exercise_time  from exercise as E  group by strftime(\"%Y-%m-%d\", (E.[start_time]/1000),\'unixepoch\',\"localtime\") "

    .line 250
    .local v3, "selectionClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 251
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v8, "exerciseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;>;"
    if-eqz v6, :cond_1

    .line 253
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 254
    const/4 v7, 0x0

    .line 255
    .local v7, "exerciseData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    new-instance v7, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    .end local v7    # "exerciseData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    invoke-direct {v7}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;-><init>()V

    .line 257
    .restart local v7    # "exerciseData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    const-string/jumbo v0, "total_cal"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->setValue(F)V

    .line 258
    const-string v0, "exercise_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->setSampleTime(Ljava/lang/String;)V

    .line 259
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 262
    .end local v7    # "exerciseData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 264
    :cond_1
    return-object v8
.end method

.method private getExerciseMedalNum(Landroid/widget/LinearLayout;)V
    .locals 14
    .param p1, "item"    # Landroid/widget/LinearLayout;

    .prologue
    .line 175
    const v10, 0x9c42

    invoke-direct {p0, v10}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getGoalList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 176
    .local v2, "goalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getExerciseList()Ljava/util/ArrayList;

    move-result-object v1

    .line 177
    .local v1, "exerciseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;>;"
    const/4 v7, 0x0

    .line 179
    .local v7, "medalget":I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v10

    int-to-long v10, v10

    long-to-double v10, v10

    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getBMR()D

    move-result-wide v12

    sub-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    double-to-int v0, v10

    .line 180
    .local v0, "defaultValue":I
    int-to-long v3, v0

    .line 182
    .local v3, "goalValue":J
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v5, v10, :cond_3

    .line 183
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v6, v10, :cond_1

    .line 184
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->getGoalTime()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->getSampleTime()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 185
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->getValue()J

    move-result-wide v3

    .line 183
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 188
    :cond_1
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->getValue()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    int-to-float v10, v10

    long-to-float v11, v3

    div-float v9, v10, v11

    .line 189
    .local v9, "result":F
    const/high16 v10, 0x42c80000    # 100.0f

    mul-float/2addr v10, v9

    float-to-int v8, v10

    .line 190
    .local v8, "per":I
    const/16 v10, 0x64

    if-lt v8, v10, :cond_2

    .line 191
    add-int/lit8 v7, v7, 0x1

    .line 182
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 194
    .end local v6    # "j":I
    .end local v8    # "per":I
    .end local v9    # "result":F
    :cond_3
    const v10, 0x7f080068

    invoke-virtual {p1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 195
    if-lez v7, :cond_4

    .line 196
    const v10, 0x7f080068

    invoke-virtual {p1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    const v11, 0x7f020239

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 197
    const v10, 0x7f080068

    invoke-virtual {p1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 198
    const v10, 0x7f080069

    invoke-virtual {p1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " x"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    const v10, 0x7f080069

    invoke-virtual {p1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    const-string v11, "#ff000000"

    invoke-static {v11}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 205
    :goto_2
    sget-object v10, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "exerciseList medal get : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    return-void

    .line 201
    :cond_4
    const v10, 0x7f080068

    invoke-virtual {p1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    const v11, 0x7f02023d

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2
.end method

.method private getFoodMedalNum(Landroid/widget/LinearLayout;)V
    .locals 16
    .param p1, "item"    # Landroid/widget/LinearLayout;

    .prologue
    .line 143
    const v8, 0x9c43

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getGoalList(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 144
    .local v1, "goalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;>;"
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getMealList()Ljava/util/ArrayList;

    move-result-object v6

    .line 145
    .local v6, "mealList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;>;"
    const/4 v7, 0x0

    .line 146
    .local v7, "medalget":I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v8

    int-to-long v2, v8

    .line 147
    .local v2, "goalValue":J
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v4, v8, :cond_3

    .line 148
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v5, v8, :cond_1

    .line 149
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->getGoalTime()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->getSampleTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 150
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->getValue()J

    move-result-wide v2

    .line 148
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 153
    :cond_1
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->getValue()F

    move-result v8

    float-to-double v8, v8

    long-to-double v10, v2

    long-to-double v12, v2

    const-wide v14, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    cmpl-double v8, v8, v10

    if-ltz v8, :cond_2

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->getValue()F

    move-result v8

    float-to-double v8, v8

    long-to-double v10, v2

    long-to-double v12, v2

    const-wide v14, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    cmpg-double v8, v8, v10

    if-gtz v8, :cond_2

    .line 154
    add-int/lit8 v7, v7, 0x1

    .line 147
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 157
    .end local v5    # "j":I
    :cond_3
    const v8, 0x7f08006b

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 158
    if-lez v7, :cond_4

    .line 159
    const v8, 0x7f08006b

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const v9, 0x7f02023a

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 160
    const v8, 0x7f08006b

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 161
    const v8, 0x7f08006c

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " x"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    const v8, 0x7f08006c

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const-string v9, "#ff000000"

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 168
    :goto_2
    sget-object v8, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "mealList medal get : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    return-void

    .line 164
    :cond_4
    const v8, 0x7f08006b

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    const v9, 0x7f02023e

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2
.end method

.method private getGoalList(I)Ljava/util/ArrayList;
    .locals 11
    .param p1, "goalType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 353
    const/4 v3, 0x0

    .line 355
    .local v3, "selectionClause":Ljava/lang/String;
    const v0, 0x9c4b

    if-eq p1, v0, :cond_0

    const v0, 0x9c43

    if-ne p1, v0, :cond_1

    .line 357
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT G.[value] as goal_value ,strftime(\"%Y-%m-%d\", (G.[update_time]/1000),\'unixepoch\',\"localtime\") as goal_time  from goal as G where goal_type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " group by strftime(\"%Y-%m-%d\", (G.[update_time]/1000),\'unixepoch\',\"localtime\") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " order by G.[update_time] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 380
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 381
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 382
    .local v10, "goalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;>;"
    if-eqz v6, :cond_4

    .line 383
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 384
    const/4 v9, 0x0

    .line 385
    .local v9, "goalData":Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 386
    new-instance v9, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    .end local v9    # "goalData":Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;
    invoke-direct {v9}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;-><init>()V

    .line 387
    .restart local v9    # "goalData":Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;
    const-string v0, "goal_value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    float-to-long v0, v0

    invoke-virtual {v9, v0, v1}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->setValue(J)V

    .line 388
    const-string v0, "goal_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->setGoalTime(Ljava/lang/String;)V

    .line 389
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 364
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v9    # "goalData":Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;
    .end local v10    # "goalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;>;"
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v8

    .line 366
    .local v8, "deviceType":I
    const/16 v0, 0x272f

    if-ne v8, v0, :cond_2

    .line 367
    const-string v7, " AND (ifnull(device_type, 10009) != 10009 AND ifnull(device_type, 10009) != 10023)"

    .line 373
    .local v7, "deviceQuery":Ljava/lang/String;
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT G.[value] as goal_value,  strftime(\"%Y-%m-%d\", (G.[update_time]/1000),\'unixepoch\',\"localtime\") as goal_time   from (goal left join user_device on goal.user_device__id == user_device._id) as G  where G.sync_status != 170004  AND goal_type = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " group by strftime(\"%Y-%m-%d\", (G.[update_time]/1000),\'unixepoch\',\"localtime\") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 370
    .end local v7    # "deviceQuery":Ljava/lang/String;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " AND ifnull(device_type, 10009) == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "deviceQuery":Ljava/lang/String;
    goto :goto_2

    .line 392
    .end local v7    # "deviceQuery":Ljava/lang/String;
    .end local v8    # "deviceType":I
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "goalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;>;"
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 394
    :cond_4
    return-object v10
.end method

.method private getMealList()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 306
    const-string v3, "SELECT sum(M.[total_kilo_calorie]) as total_kcal ,strftime(\"%Y-%m-%d\", (M.[sample_time]/1000),\'unixepoch\',\"localtime\") as meal_sample_time  from meal as M where sample_time != 0  group by strftime(\"%Y-%m-%d\", (M.[sample_time]/1000),\'unixepoch\',\"localtime\") "

    .line 309
    .local v3, "selectionClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 310
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 311
    .local v8, "mealList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;>;"
    if-eqz v6, :cond_1

    .line 312
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 313
    const/4 v7, 0x0

    .line 314
    .local v7, "mealData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    new-instance v7, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;

    .end local v7    # "mealData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    invoke-direct {v7}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;-><init>()V

    .line 316
    .restart local v7    # "mealData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    const-string/jumbo v0, "total_kcal"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->setValue(F)V

    .line 317
    const-string/jumbo v0, "meal_sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;->setSampleTime(Ljava/lang/String;)V

    .line 318
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 321
    .end local v7    # "mealData":Lcom/sec/android/app/shealth/award/medaldata/AwardCalorieData;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 323
    :cond_1
    return-object v8
.end method

.method private getMedalNum(ILandroid/widget/LinearLayout;)V
    .locals 0
    .param p1, "i"    # I
    .param p2, "item"    # Landroid/widget/LinearLayout;

    .prologue
    .line 91
    packed-switch p1, :pswitch_data_0

    .line 111
    :goto_0
    return-void

    .line 93
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getPedometerMedalNum(Landroid/widget/LinearLayout;)V

    goto :goto_0

    .line 97
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getFoodMedalNum(Landroid/widget/LinearLayout;)V

    goto :goto_0

    .line 101
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getBurnCalorieMedalNum(Landroid/widget/LinearLayout;)V

    goto :goto_0

    .line 105
    :pswitch_3
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getExerciseMedalNum(Landroid/widget/LinearLayout;)V

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getPedometerList()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT total (total_step) as total_step ,  strftime(\"%Y-%m-%d\", (P.[start_time]/1000),\'unixepoch\',\"localtime\") as pedometer_time   from (walk_info join user_device on walk_info.user_device__id == user_device._id) as P  where P.sync_status != 170004 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getDeviceTypQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " group by strftime(\"%Y-%m-%d\", (P.[start_time]/1000),\'unixepoch\',\"localtime\") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 287
    .local v3, "selectionClause":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 288
    .local v6, "cursor":Landroid/database/Cursor;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 289
    .local v8, "pedometerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;>;"
    if-eqz v6, :cond_1

    .line 290
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 291
    const/4 v7, 0x0

    .line 292
    .local v7, "pedometerData":Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    new-instance v7, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;

    .end local v7    # "pedometerData":Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;
    invoke-direct {v7}, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;-><init>()V

    .line 294
    .restart local v7    # "pedometerData":Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;
    const-string/jumbo v0, "total_step"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;->setValue(I)V

    .line 295
    const-string/jumbo v0, "pedometer_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;->setSampleTime(Ljava/lang/String;)V

    .line 296
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 299
    .end local v7    # "pedometerData":Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 302
    :cond_1
    return-object v8
.end method

.method private getPedometerMedalNum(Landroid/widget/LinearLayout;)V
    .locals 11
    .param p1, "item"    # Landroid/widget/LinearLayout;

    .prologue
    const v10, 0x7f080063

    const v9, 0x7f080062

    .line 114
    const v7, 0x9c41

    invoke-direct {p0, v7}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getGoalList(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 115
    .local v0, "goalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;>;"
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getPedometerList()Ljava/util/ArrayList;

    move-result-object v6

    .line 116
    .local v6, "pedometerList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;>;"
    const/4 v5, 0x0

    .line 117
    .local v5, "medalget":I
    const-wide/16 v1, 0x2710

    .line 118
    .local v1, "goalValue":J
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_3

    .line 119
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_1

    .line 120
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->getGoalTime()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;->getSampleTime()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 121
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/award/medaldata/AwardGoalData;->getValue()J

    move-result-wide v1

    .line 119
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 124
    :cond_1
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/award/medaldata/AwardStepsData;->getValue()I

    move-result v7

    int-to-long v7, v7

    cmp-long v7, v7, v1

    if-ltz v7, :cond_2

    .line 125
    add-int/lit8 v5, v5, 0x1

    .line 118
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 128
    .end local v4    # "j":I
    :cond_3
    invoke-virtual {p1, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 129
    if-lez v5, :cond_4

    .line 130
    invoke-virtual {p1, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    const v8, 0x7f02023b

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 131
    invoke-virtual {p1, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 132
    invoke-virtual {p1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {p1, v10}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const-string v8, "#ff000000"

    invoke-static {v8}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 139
    :goto_2
    sget-object v7, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "pedometerList medal get : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return-void

    .line 135
    :cond_4
    invoke-virtual {p1, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    const v8, 0x7f02023f

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_2
.end method

.method private initializeMedalArea()V
    .locals 7

    .prologue
    const v6, 0x7f080074

    const v5, 0x7f030013

    .line 68
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 69
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 70
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    .line 71
    .local v2, "item":Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    invoke-virtual {v1, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .end local v2    # "item":Landroid/widget/LinearLayout;
    check-cast v2, Landroid/widget/LinearLayout;

    .line 72
    .restart local v2    # "item":Landroid/widget/LinearLayout;
    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setId(I)V

    .line 74
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->setInitMedalDim(Landroid/widget/LinearLayout;)V

    .line 75
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    .line 76
    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->getMedalNum(ILandroid/widget/LinearLayout;)V

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/award/summary/activity/AwardMedalActivity;->mView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 81
    return-void
.end method

.method private setInitMedalDim(Landroid/widget/LinearLayout;)V
    .locals 2
    .param p1, "item"    # Landroid/widget/LinearLayout;

    .prologue
    .line 84
    const v0, 0x7f080063

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "#40000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    const v0, 0x7f08006c

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "#40000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 86
    const v0, 0x7f080066

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "#40000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 87
    const v0, 0x7f080069

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "#40000000"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 88
    return-void
.end method
