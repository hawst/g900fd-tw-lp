.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$WearableStatusObserver;
.super Landroid/database/ContentObserver;
.source "ExerciseProWorkoutController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WearableStatusObserver"
.end annotation


# static fields
.field private static final SETTING_WEARABLE_ID:Ljava/lang/String; = "content://settings/system/connected_wearable_id"


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Landroid/os/Handler;)V
    .locals 0
    .param p2, "observerHandler"    # Landroid/os/Handler;

    .prologue
    .line 704
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    .line 705
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 706
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 710
    const-string v0, "WearableStatusObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WearableStatusObserver.onChange("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 713
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 717
    if-eqz p2, :cond_0

    const-string v0, "content://settings/system/connected_wearable_id"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    const-string v0, "WearableStatusObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WearableStatusObserver.onChange("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") URI = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$WearableStatusObserver;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->updateGearConnectedStatus()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V

    .line 721
    :cond_0
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 722
    return-void
.end method
