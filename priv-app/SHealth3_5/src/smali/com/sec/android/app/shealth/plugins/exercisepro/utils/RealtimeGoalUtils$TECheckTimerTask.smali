.class Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;
.super Ljava/util/TimerTask;
.source "RealtimeGoalUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TECheckTimerTask"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 522
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$1;

    .prologue
    .line 522
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$TECheckTimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x1

    const/4 v11, -0x1

    const/4 v10, 0x0

    .line 525
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHearbeatTime:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$000()I

    move-result v7

    if-nez v7, :cond_0

    .line 526
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentHR:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$100()I

    move-result v7

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentSpeed:F
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$200()F

    move-result v8

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mCurrentRealElevation:F
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$300()F

    move-result v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->inputBpmTrainingData(IFF)I

    .line 528
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getETEResult()I

    .line 529
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getSecToTarget()I

    move-result v7

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mSecToTarget:I
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$402(I)I

    .line 531
    const/4 v3, 0x0

    .line 533
    .local v3, "fPlay":Z
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->processResult()I

    .line 534
    const-string v7, "Goal"

    const-string v8, "[TECheckTimerTask] analyze"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getAudioGuideId()I

    move-result v1

    .line 537
    .local v1, "audioGuide":I
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getAudioGuideString()Ljava/lang/String;

    move-result-object v2

    .line 539
    .local v2, "audioGuideString":Ljava/lang/String;
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    if-eqz v7, :cond_1

    .line 540
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEtrainingEffect()I

    move-result v6

    .line 541
    .local v6, "value":I
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    const/4 v8, 0x2

    invoke-interface {v7, v8, v11, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;->onGoalEvnet(III)V

    .line 542
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEAchievedPercent()I

    move-result v8

    invoke-interface {v7, v13, v11, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;->onGoalEvnet(III)V

    .line 543
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getPaceGuideId()I

    move-result v7

    const/4 v8, 0x4

    if-eq v7, v8, :cond_1

    .line 544
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getLastETEAchievedPercent()I

    move-result v7

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->checkGoal(I)V
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$500(I)V

    .line 547
    .end local v6    # "value":I
    :cond_1
    # ++operator for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mAnalyzeCount:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$604()I

    move-result v7

    const/16 v8, 0x1e

    if-ne v7, v8, :cond_2

    .line 548
    const/4 v3, 0x1

    .line 550
    :cond_2
    if-eqz v1, :cond_3

    .line 551
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mLastAudioGuideId:I
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$700()I

    move-result v7

    if-eq v7, v1, :cond_3

    .line 552
    const/4 v3, 0x1

    .line 556
    :cond_3
    if-eqz v3, :cond_5

    .line 557
    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mAnalyzeCount:I
    invoke-static {v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$602(I)I

    .line 558
    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mLastAudioGuideId:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$702(I)I

    .line 559
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getAudioGuideArg()I

    move-result v7

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mLastAudioGuideParam:I
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->access$802(I)I

    .line 560
    if-eqz v2, :cond_6

    .line 561
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getPaceGuideId()I

    move-result v7

    if-eq v7, v13, :cond_4

    .line 562
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 563
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 565
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getPaceGuideId()I

    move-result v7

    invoke-static {v2, v7}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->logUserGuide(Ljava/lang/String;I)V

    .line 566
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    if-eqz v7, :cond_5

    .line 567
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getPaceGuideId()I

    move-result v8

    invoke-interface {v7, v12, v8, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;->onGoalEvnet(III)V

    .line 587
    :cond_5
    :goto_0
    return-void

    .line 569
    :cond_6
    if-eqz v1, :cond_5

    .line 570
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 571
    .local v5, "stringCheck":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getAudioGuideArg()I

    move-result v0

    .line 572
    .local v0, "arg":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 573
    .local v4, "string":Ljava/lang/String;
    const-string v7, "%d"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_7

    if-gtz v0, :cond_7

    .line 574
    const/4 v1, -0x1

    .line 576
    :cond_7
    if-eq v1, v11, :cond_8

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getPaceGuideId()I

    move-result v7

    if-eq v7, v13, :cond_8

    .line 577
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;

    .line 578
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeTtsUtils;->play(Ljava/lang/String;)Z

    .line 580
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getPaceGuideId()I

    move-result v7

    invoke-static {v4, v7}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->logUserGuide(Ljava/lang/String;I)V

    .line 582
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    if-eqz v7, :cond_5

    .line 583
    sget-object v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils;->mListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getPaceGuideId()I

    move-result v8

    invoke-interface {v7, v12, v8, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeGoalUtils$GoalListener;->onGoalEvnet(III)V

    goto :goto_0
.end method
