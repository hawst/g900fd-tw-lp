.class Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;
.super Ljava/lang/Object;
.source "HoloCircleSeekBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->doUiChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/high16 v3, 0x43b40000    # 360.0f

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->isTimerTaskRunning:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$000(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 373
    :goto_0
    return-void

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$100(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v1

    if-lt v0, v1, :cond_2

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mBackupValue:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mBackupValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;I)I

    .line 361
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->calculateAngleFromRadians(F)F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;F)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mAngle:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$602(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;F)F

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->invalidate()V

    .line 364
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->stopTask()V

    goto :goto_0

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;I)I

    goto :goto_1

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    const/4 v1, 0x1

    # += operator for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$112(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;I)I

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mCurrentAniValue:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$100(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$500(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$402(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;I)I

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->arc_finish_radians:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;)I

    move-result v2

    int-to-float v2, v2

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->calculateAngleFromRadians(F)F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;F)F

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->mAngle:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->access$602(Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;F)F

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar$2;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/HoloCircleSeekBar;->invalidate()V

    goto/16 :goto_0
.end method
