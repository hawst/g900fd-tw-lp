.class public Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;
.super Ljava/lang/Object;
.source "QuickInputCalorieProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    }
.end annotation


# static fields
.field private static final BREAKFAST_CALORIE_KEY:Ljava/lang/String; = "qi_breakfast_calorie_key"

.field private static final DINNER_CALORIE_KEY:Ljava/lang/String; = "qi_dinner_calorie_key"

.field private static final LUNCH_CALORIE_KEY:Ljava/lang/String; = "qi_lunch_calorie_key"

.field private static final QUICK_INPUT_DEFAULT_VALUE:I = 0x258

.field private static final QUICK_INPUT_PREF:Ljava/lang/String; = "food_quick_input_pref"

.field private static final QUICK_INPUT_SNACKS_DEFAULT_VALUE:I = 0xc8

.field private static final SNACKS_CALORIE_KEY:Ljava/lang/String; = "qi_snacks_calorie_key"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    return-void
.end method

.method public static getCalorieForMealType(Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;)I
    .locals 4
    .param p0, "type"    # Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    .prologue
    const/16 v3, 0x258

    .line 68
    invoke-static {}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getQuickInputSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 69
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const/16 v0, 0x258

    .line 70
    .local v0, "calorieValue":I
    if-eqz v1, :cond_0

    .line 73
    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->BREAK_FAST:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne p0, v2, :cond_1

    .line 75
    const-string/jumbo v2, "qi_breakfast_calorie_key"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 91
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->LUNCH:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne p0, v2, :cond_2

    .line 79
    const-string/jumbo v2, "qi_lunch_calorie_key"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 81
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->DINNER:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne p0, v2, :cond_3

    .line 83
    const-string/jumbo v2, "qi_dinner_calorie_key"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 85
    :cond_3
    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->SNACKS:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne p0, v2, :cond_0

    .line 87
    const-string/jumbo v2, "qi_snacks_calorie_key"

    const/16 v3, 0xc8

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public static getCalories()[I
    .locals 5

    .prologue
    const/16 v4, 0x258

    .line 99
    invoke-static {}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getQuickInputSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 101
    .local v1, "prefs":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 104
    const/4 v2, 0x4

    new-array v0, v2, [I

    .line 106
    .local v0, "calories":[I
    const/4 v2, 0x0

    const-string/jumbo v3, "qi_breakfast_calorie_key"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v0, v2

    .line 107
    const/4 v2, 0x1

    const-string/jumbo v3, "qi_lunch_calorie_key"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v0, v2

    .line 108
    const/4 v2, 0x2

    const-string/jumbo v3, "qi_dinner_calorie_key"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v0, v2

    .line 109
    const/4 v2, 0x3

    const-string/jumbo v3, "qi_snacks_calorie_key"

    const/16 v4, 0xc8

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    aput v3, v0, v2

    .line 113
    .end local v0    # "calories":[I
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getQuickInputSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 123
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 124
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 126
    const-string v1, "food_quick_input_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 129
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setCalorieForMealType(Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;I)V
    .locals 3
    .param p0, "type"    # Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    .param p1, "value"    # I

    .prologue
    .line 32
    invoke-static {}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->getQuickInputSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 34
    .local v0, "prefs":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_1

    .line 36
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 37
    .local v1, "quickInputEditor":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->BREAK_FAST:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne p0, v2, :cond_2

    .line 39
    const-string/jumbo v2, "qi_breakfast_calorie_key"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 56
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 59
    .end local v1    # "quickInputEditor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    return-void

    .line 41
    .restart local v1    # "quickInputEditor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->LUNCH:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne p0, v2, :cond_3

    .line 43
    const-string/jumbo v2, "qi_lunch_calorie_key"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 46
    :cond_3
    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->DINNER:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne p0, v2, :cond_4

    .line 48
    const-string/jumbo v2, "qi_dinner_calorie_key"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 51
    :cond_4
    sget-object v2, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->SNACKS:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    if-ne p0, v2, :cond_0

    .line 53
    const-string/jumbo v2, "qi_snacks_calorie_key"

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method
