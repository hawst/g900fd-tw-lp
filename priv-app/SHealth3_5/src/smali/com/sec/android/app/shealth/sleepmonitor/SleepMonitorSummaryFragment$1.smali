.class Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$1;
.super Ljava/lang/Object;
.source "SleepMonitorSummaryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getContentView(Landroid/content/Context;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$000(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getSelecteddate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->setSelectedTime(JZ)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 180
    return-void
.end method
