.class public Lcom/sec/android/app/shealth/common/commonui/SummaryButton;
.super Landroid/widget/LinearLayout;
.source "SummaryButton.java"


# static fields
.field private static final DEFAULT_BACKGROUND:I

.field private static final DEFAULT_ICON:I


# instance fields
.field private mButtonIcon:Landroid/widget/ImageView;

.field private mButtonTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->summary_button_selector:I

    sput v0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->DEFAULT_BACKGROUND:I

    .line 31
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->summary_button_left_drawable_selector:I

    sput v0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->DEFAULT_ICON:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->initialize(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->initialize(Landroid/content/Context;)V

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->initAttr(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->initialize(Landroid/content/Context;)V

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->initAttr(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method private initAttr(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v7, 0x0

    .line 69
    sget-object v8, Lcom/sec/android/app/shealth/common/commonui/R$styleable;->SummaryButton:[I

    invoke-virtual {p1, p2, v8, v7, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 72
    .local v6, "typedArray":Landroid/content/res/TypedArray;
    if-eqz v6, :cond_0

    .line 76
    :try_start_0
    sget v8, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->DEFAULT_BACKGROUND:I

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->setBackgroundResource(I)V

    .line 78
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 80
    .local v0, "drawableId":I
    if-nez v0, :cond_1

    .line 81
    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonIcon:Landroid/widget/ImageView;

    sget v9, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->DEFAULT_ICON:I

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    :goto_0
    const/4 v8, 0x3

    const/4 v9, 0x1

    invoke-virtual {v6, v8, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 88
    .local v1, "iconVisibility":Z
    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonIcon:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 90
    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 92
    .local v2, "marginRight":I
    iget-object v7, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 94
    .local v3, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v3, v7, v8, v2, v9}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 96
    iget-object v7, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    .line 100
    .local v5, "titleId":I
    if-eqz v5, :cond_3

    .line 101
    iget-object v7, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :goto_2
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 114
    .end local v0    # "drawableId":I
    .end local v1    # "iconVisibility":Z
    .end local v2    # "marginRight":I
    .end local v3    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v5    # "titleId":I
    :cond_0
    return-void

    .line 83
    .restart local v0    # "drawableId":I
    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 109
    .end local v0    # "drawableId":I
    :catchall_0
    move-exception v7

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    throw v7

    .line 88
    .restart local v0    # "drawableId":I
    .restart local v1    # "iconVisibility":Z
    :cond_2
    const/16 v7, 0x8

    goto :goto_1

    .line 103
    .restart local v2    # "marginRight":I
    .restart local v3    # "params":Landroid/widget/LinearLayout$LayoutParams;
    .restart local v5    # "titleId":I
    :cond_3
    const/4 v7, 0x0

    :try_start_2
    invoke-virtual {v6, v7}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 104
    .local v4, "title":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonTitle:Landroid/widget/TextView;

    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private initialize(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->setOrientation(I)V

    .line 58
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->setGravity(I)V

    .line 59
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$layout;->custom_button_with_icon:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 62
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$id;->button_icon:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonIcon:Landroid/widget/ImageView;

    .line 63
    sget v0, Lcom/sec/android/app/shealth/common/commonui/R$id;->button_text:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/SummaryButton;->mButtonTitle:Landroid/widget/TextView;

    .line 65
    return-void
.end method
