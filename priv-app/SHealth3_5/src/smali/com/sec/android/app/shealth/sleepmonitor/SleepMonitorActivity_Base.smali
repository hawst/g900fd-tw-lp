.class public Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "SleepMonitorActivity_Base.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$BTreceiver;
    }
.end annotation


# static fields
.field private static ACCESSARY_ADD_REQ_CODE:I = 0x0

.field private static final ACCESSORIES_DUPLICATE_DIALOG:Ljava/lang/String; = "accessories_duplicate_dialog"

.field private static final ACTION_BAR_BUTTON_ACCESSORY:I = 0x1

.field private static final ACTION_BAR_BUTTON_LOG:I = 0x0

.field private static final ACTION_BAR_MAX:I = 0x3

.field private static final ACTION_BAR_SHARE_VIA:I = 0x2

.field private static final FRAGMENT_CONTAINER_VIEW_ID:I = 0x7f080091

.field public static final RESET_DATA_DIALOG:Ljava/lang/String; = "reset_data_dialog"

.field private static final WARNING_STRING:Ljava/lang/String; = "is_warning_checked"


# instance fields
.field aa:Landroid/os/Handler;

.field private actionButton1:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field private actionButton2:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field private actionButton3:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private doNotShowCheckBox:Landroid/widget/CheckBox;

.field private isCommingFromHome:Z

.field private isGraph:Z

.field private mBTBroadcast:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$BTreceiver;

.field private mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

.field private mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

.field private mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

.field private uiThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const/16 v0, 0x7b

    sput v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->ACCESSARY_ADD_REQ_CODE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 70
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    .line 71
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isCommingFromHome:Z

    .line 78
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->uiThreadHandler:Landroid/os/Handler;

    .line 213
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$4;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 221
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$5;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->aa:Landroid/os/Handler;

    .line 502
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->doNotShowCheckBox:Landroid/widget/CheckBox;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->ACCESSARY_ADD_REQ_CODE:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->prepareShareView()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->uiThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getDataCount()I
    .locals 7

    .prologue
    .line 453
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 454
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 456
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 457
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 459
    if-eqz v6, :cond_0

    .line 460
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    return v0

    .line 459
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 460
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method private launchSleepMonitor()V
    .locals 7

    .prologue
    const-wide/16 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 124
    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .line 125
    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 129
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    check-cast v1, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v1, v5, v6, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->setSelectedTime(JZ)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->showFragment2(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 152
    :goto_0
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "is_warning_checked"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 154
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const v2, 0x7f0907be

    invoke-direct {v1, p0, v4, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 157
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$1;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 166
    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$2;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 173
    const v1, 0x7f030218

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 175
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "accessories_duplicate_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 177
    .end local v0    # "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_0
    return-void

    .line 135
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "show_graph_fragment"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 137
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    .line 138
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isCommingFromHome:Z

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    check-cast v1, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;

    invoke-virtual {v1, v5, v6, v3}, Lcom/sec/android/app/shealth/sleepmonitor/graph/SleepMonitorGraphFragmentSIC;->setSelectedTime(JZ)V

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->showFragment2(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 144
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->showFragment2(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0
.end method


# virtual methods
.method public addShareViaButton()V
    .locals 4

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton3:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    if-eqz v0, :cond_0

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton3:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 293
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->invalidateOptionsMenu()V

    .line 294
    return-void
.end method

.method public closeScreen()V
    .locals 0

    .prologue
    .line 379
    return-void
.end method

.method protected customizeActionBar()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 383
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f090d56

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 387
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$8;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    .line 423
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207c0

    const v3, 0x7f09005a

    invoke-direct {v1, v2, v4, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton1:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 424
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton1:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 425
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207bc

    const v3, 0x7f0907b4

    invoke-direct {v1, v2, v4, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton2:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 426
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton2:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 427
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207cf

    const v3, 0x7f090033

    invoke-direct {v1, v2, v4, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton3:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v5, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton3:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 430
    return-void
.end method

.method public deleteShareViaButton()V
    .locals 2

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getTotalActionBarButtonCount()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    .line 284
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->invalidateOptionsMenu()V

    .line 285
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 103
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f080304

    if-ne v1, v2, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08030e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 109
    const/4 v1, 0x1

    .line 114
    .end local v0    # "currentView":Landroid/view/View;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 180
    const-string v0, "accessories_duplicate_dialog"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$3;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    .line 209
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0
.end method

.method public getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 439
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v1, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton1:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v1, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton2:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v1, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->actionButton3:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 443
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getDataCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    .line 446
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    .line 447
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->invalidateOptionsMenu()V

    .line 448
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 345
    const-string v0, "com.sec.shealth.help.action.SLEEP"

    return-object v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 490
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    if-eqz v0, :cond_2

    .line 491
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    .line 492
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "show_graph_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isCommingFromHome:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->showFragment2(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 500
    :goto_0
    return-void

    .line 493
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->finish()V

    .line 499
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/SamsungAccountSharedPreferencesHelper;->initialize(Landroid/content/Context;)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->setContext(Landroid/content/Context;)V

    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->launchSleepMonitor()V

    .line 90
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 91
    .local v0, "lFilter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 92
    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$BTreceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$BTreceiver;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$1;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mBTBroadcast:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$BTreceiver;

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mBTBroadcast:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$BTreceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 96
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    const/4 v0, 0x0

    .line 312
    :goto_0
    return v0

    .line 308
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    if-nez v0, :cond_1

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100022

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeActionButton(I)V

    .line 312
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mBTBroadcast:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$BTreceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 120
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 121
    return-void
.end method

.method protected onLogSelected()V
    .locals 0

    .prologue
    .line 435
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 317
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 338
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    :goto_1
    return v2

    .line 319
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.sleepmonitor"

    const-string v5, "SL01"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->prepareShareView()V

    goto :goto_1

    .line 327
    :sswitch_1
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 328
    .local v1, "intent1":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.shealth"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    const-string v3, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 331
    .end local v1    # "intent1":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 332
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "Application is not available on your device !!!"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 336
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.sleepmonitor"

    const-string v4, "SL04"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080c8a -> :sswitch_2
        0x7f080c90 -> :sswitch_0
        0x7f080ca4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 467
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 469
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    if-eqz v0, :cond_0

    .line 470
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 473
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->makeAllActionBarButtonsInvisible()V

    .line 478
    :cond_1
    return-void
.end method

.method public showFragment2(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V
    .locals 4
    .param p1, "fragmentClass"    # Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->invalidateOptionsMenu()V

    .line 298
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->aa:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 300
    return-void
.end method

.method public showResetDialog()V
    .locals 5

    .prologue
    const v3, 0x7f090032

    .line 349
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;II)V

    .line 350
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v0, 0x7f090062

    .line 351
    .local v0, "alertTextResId":I
    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 352
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 353
    new-instance v2, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$6;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 366
    new-instance v2, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base$7;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 373
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string/jumbo v4, "reset_data_dialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 374
    return-void
.end method

.method public showSummaryFragment()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 481
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isGraph:Z

    .line 482
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->isCommingFromHome:Z

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->mSummaryFragment:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorActivity_Base;->showFragment2(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 486
    :cond_0
    return-void
.end method
