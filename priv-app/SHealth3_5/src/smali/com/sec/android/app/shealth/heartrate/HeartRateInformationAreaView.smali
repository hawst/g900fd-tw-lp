.class public Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;
.super Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.source "HeartRateInformationAreaView.java"


# static fields
.field private static final DATE_SEPARATOR:Ljava/lang/String; = "/"

.field private static final DAY_CHAR:Ljava/lang/String; = "d"

.field private static final DAY_FORMAT_PATTERN:Ljava/lang/String; = "dd"

.field private static final MONTH_CHAR:Ljava/lang/String; = "M"

.field private static final MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MM"

.field private static final REGEX_END_OF_LINE:Ljava/lang/String; = "$"

.field private static final REGEX_ONE_OR_MORE:Ljava/lang/String; = "+"

.field private static final TIME_FORMAT_12HOUR:Ljava/lang/String; = "h:mm a"

.field private static final TIME_FORMAT_24HOUR:Ljava/lang/String; = "HH:mm"

.field private static final YEAR_CHAR:Ljava/lang/String; = "y"

.field private static final YEAR_FORMAT_PATTERN:Ljava/lang/String; = "yyyy"


# instance fields
.field private avgDayMonthBpm:Landroid/widget/TextView;

.field private dateFormat:Ljava/text/DateFormat;

.field private dateValue:Ljava/lang/String;

.field private ivTag:Landroid/widget/ImageView;

.field private mAvgDayMonth:Landroid/widget/TextView;

.field private mDateFormatOrder:[C

.field private mDayMonthLayout:Landroid/widget/LinearLayout;

.field private mHourLayout:Landroid/widget/LinearLayout;

.field private mMaxDayMonth:Landroid/widget/TextView;

.field private mMinDayMonth:Landroid/widget/TextView;

.field private mTimeFormatPattern:Ljava/lang/String;

.field private maxDayMonthBpm:Landroid/widget/TextView;

.field private minDayMonthBpm:Landroid/widget/TextView;

.field private periodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private tvBpm:Landroid/widget/TextView;

.field private tvBpmText:Landroid/widget/TextView;

.field private tvDate:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;-><init>(Landroid/content/Context;)V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->periodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateValue:Ljava/lang/String;

    .line 78
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    .line 79
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mDateFormatOrder:[C

    .line 80
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "HH:mm"

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    .line 85
    :goto_0
    iput-object p2, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->periodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->initLayout()V

    .line 87
    return-void

    .line 83
    :cond_0
    const-string v0, "h:mm a"

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    goto :goto_0
.end method

.method private initLayout()V
    .locals 1

    .prologue
    .line 104
    const v0, 0x7f0805a3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->tvDate:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f080598

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->tvBpm:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f080599

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->tvBpmText:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f080597

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    .line 108
    const v0, 0x7f0805a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mHourLayout:Landroid/widget/LinearLayout;

    .line 109
    const v0, 0x7f0805a5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mDayMonthLayout:Landroid/widget/LinearLayout;

    .line 110
    const v0, 0x7f08059d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->maxDayMonthBpm:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f08059f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->minDayMonthBpm:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0805a1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->avgDayMonthBpm:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f08059a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mMaxDayMonth:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f08059b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mMinDayMonth:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f08059c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mAvgDayMonth:Landroid/widget/TextView;

    .line 116
    return-void
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 0

    .prologue
    .line 100
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03014e

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 0

    .prologue
    .line 96
    return-void
.end method

.method public setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 4
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 119
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mDateFormatOrder:[C

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    .line 122
    .local v0, "dateFormatPattern":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_1

    .line 123
    const-string v1, ""

    .line 128
    .local v1, "dayFormatPattern":Ljava/lang/String;
    :goto_0
    const-string v2, "d+"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    const-string v2, "M+"

    const-string v3, "MM/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string/jumbo v2, "y+"

    const-string/jumbo v3, "yyyy/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    const-string v2, "/$"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_0

    .line 138
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 141
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    .line 142
    return-void

    .line 125
    .end local v1    # "dayFormatPattern":Ljava/lang/String;
    :cond_1
    const-string v1, "dd/"

    .restart local v1    # "dayFormatPattern":Ljava/lang/String;
    goto :goto_0
.end method

.method public update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 12
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const/16 v9, 0x8

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 149
    if-eqz p1, :cond_1

    .line 150
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateValue:Ljava/lang/String;

    .line 151
    const/4 v1, 0x0

    .line 152
    .local v1, "date":Ljava/util/Date;
    new-instance v1, Ljava/util/Date;

    .end local v1    # "date":Ljava/util/Date;
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateValue:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-direct {v1, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 153
    .restart local v1    # "date":Ljava/util/Date;
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateValue:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->setSelectedDateInChart(J)V

    .line 154
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->tvDate:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v8, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->periodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v8, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v7, v8, :cond_3

    .line 156
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mDayMonthLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 157
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mHourLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 158
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->tvBpmText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0900d2

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->tvBpm:Landroid/widget/TextView;

    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->floatValue()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 165
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v3

    .line 166
    .local v3, "helper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateValue:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTagsByTime(J)Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "data":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090c0d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 169
    .local v6, "tagName":Ljava/lang/String;
    const v4, 0x7f02053d

    .line 170
    .local v4, "iconId":I
    if-eqz v0, :cond_0

    .line 171
    const-string v7, ":"

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 172
    .local v5, "mTagInfo":[Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 174
    const/4 v7, 0x0

    :try_start_0
    aget-object v7, v5, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 180
    :goto_0
    array-length v7, v5

    if-le v7, v11, :cond_0

    .line 181
    aget-object v6, v5, v11

    .line 184
    .end local v5    # "mTagInfo":[Ljava/lang/String;
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    const v8, 0x7f02051c

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 185
    if-eqz v4, :cond_2

    const v7, 0x7f02053d

    if-eq v4, v7, :cond_2

    .line 186
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 192
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    sget-object v8, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090f19

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9, v6}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    .end local v0    # "data":Ljava/lang/String;
    .end local v1    # "date":Ljava/util/Date;
    .end local v3    # "helper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .end local v4    # "iconId":I
    .end local v6    # "tagName":Ljava/lang/String;
    :cond_1
    :goto_2
    return-void

    .line 176
    .restart local v0    # "data":Ljava/lang/String;
    .restart local v1    # "date":Ljava/util/Date;
    .restart local v3    # "helper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .restart local v4    # "iconId":I
    .restart local v5    # "mTagInfo":[Ljava/lang/String;
    .restart local v6    # "tagName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 177
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-string v7, "HeartRateInformationAreaView"

    invoke-virtual {v2}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 189
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    .end local v5    # "mTagInfo":[Ljava/lang/String;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    const v8, 0x7f02051d

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 195
    .end local v0    # "data":Ljava/lang/String;
    .end local v3    # "helper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .end local v4    # "iconId":I
    .end local v6    # "tagName":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mHourLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 196
    iget-object v7, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mDayMonthLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 198
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->maxDayMonthBpm:Landroid/widget/TextView;

    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->floatValue()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v7

    if-le v7, v11, :cond_4

    .line 204
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->minDayMonthBpm:Landroid/widget/TextView;

    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->floatValue()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    :cond_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le v7, v11, :cond_1

    .line 211
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->avgDayMonthBpm:Landroid/widget/TextView;

    invoke-virtual {p1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->floatValue()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public updateStringOnLocaleChange()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mMaxDayMonth:Landroid/widget/TextView;

    const v1, 0x7f090c2e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mMinDayMonth:Landroid/widget/TextView;

    const v1, 0x7f090c2f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mAvgDayMonth:Landroid/widget/TextView;

    const v1, 0x7f090c30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 264
    return-void
.end method

.method public updateTagOnResume()V
    .locals 12

    .prologue
    const/4 v10, 0x1

    .line 221
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->periodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v9, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v8, v9, :cond_1

    .line 222
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v4

    .line 223
    .local v4, "helper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    const-wide/16 v1, -0x1

    .line 225
    .local v1, "date":J
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->dateValue:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    .line 231
    invoke-virtual {v4, v1, v2}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getTagsByTime(J)Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "data":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090c0d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 234
    .local v7, "tagName":Ljava/lang/String;
    const v5, 0x7f02053d

    .line 235
    .local v5, "iconId":I
    if-eqz v0, :cond_0

    .line 236
    const-string v8, ":"

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 237
    .local v6, "mTagInfo":[Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 239
    const/4 v8, 0x0

    :try_start_1
    aget-object v8, v6, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 245
    :goto_0
    array-length v8, v6

    if-le v8, v10, :cond_0

    .line 246
    aget-object v7, v6, v10

    .line 249
    .end local v6    # "mTagInfo":[Ljava/lang/String;
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    const v9, 0x7f02051c

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 250
    if-eqz v5, :cond_2

    const v8, 0x7f02053d

    if-eq v5, v8, :cond_2

    .line 251
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 257
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    sget-object v9, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_USER_CUSTOM:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090f19

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10, v7}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    .end local v0    # "data":Ljava/lang/String;
    .end local v1    # "date":J
    .end local v4    # "helper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    .end local v5    # "iconId":I
    .end local v7    # "tagName":Ljava/lang/String;
    :cond_1
    :goto_2
    return-void

    .line 227
    .restart local v1    # "date":J
    .restart local v4    # "helper":Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;
    :catch_0
    move-exception v3

    .line 228
    .local v3, "e":Ljava/lang/NumberFormatException;
    const-string v8, "HeartRateInformationAreaView"

    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 241
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    .restart local v0    # "data":Ljava/lang/String;
    .restart local v5    # "iconId":I
    .restart local v6    # "mTagInfo":[Ljava/lang/String;
    .restart local v7    # "tagName":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 242
    .restart local v3    # "e":Ljava/lang/NumberFormatException;
    const-string v8, "HeartRateInformationAreaView"

    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 254
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    .end local v6    # "mTagInfo":[Ljava/lang/String;
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/heartrate/HeartRateInformationAreaView;->ivTag:Landroid/widget/ImageView;

    const v9, 0x7f02051d

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
