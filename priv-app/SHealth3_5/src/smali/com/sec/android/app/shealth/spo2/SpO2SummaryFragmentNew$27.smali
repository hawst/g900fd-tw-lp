.class Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;
.super Ljava/lang/Object;
.source "SpO2SummaryFragmentNew.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->startTimer(Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

.field final synthetic val$timerType:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;)V
    .locals 0

    .prologue
    .line 1845
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    iput-object p2, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->val$timerType:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1849
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->cancel()V

    .line 1850
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1851
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1853
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$902(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 1854
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopCountDownTimer()V

    .line 1860
    :goto_0
    sget-object v0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$29;->$SwitchMap$com$sec$android$app$shealth$spo2$data$SpO2TimerType:[I

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->val$timerType:Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/data/SpO2TimerType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1882
    :goto_1
    return-void

    .line 1857
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->stopCountDownTimer()V

    goto :goto_0

    .line 1862
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->readyTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_1

    .line 1865
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1867
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v1

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$902(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    .line 1868
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startCountDownTimer()V

    goto :goto_1

    .line 1872
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mDeviceConnector:Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->startCountDownTimer()V

    goto :goto_1

    .line 1876
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mErrorType:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$5100(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->setErrorType(I)V

    .line 1877
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$27;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->mSensorErrorTimer:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->access$3200(Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew$SensorErrorTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_1

    .line 1860
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
