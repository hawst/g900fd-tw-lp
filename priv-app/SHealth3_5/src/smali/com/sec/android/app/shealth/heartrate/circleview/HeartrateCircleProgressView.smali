.class public Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;
.super Landroid/widget/FrameLayout;
.source "HeartrateCircleProgressView.java"


# instance fields
.field private R1:F

.field private animationRunner:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

.field private heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

.field private r1:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    sget-object v1, Lcom/sec/android/app/shealth/R$styleable;->HeartrateCircleProgressView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 46
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->r1:F

    .line 47
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->R1:F

    .line 48
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    return-void
.end method

.method private addCircleView(FFZ)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;
    .locals 4
    .param p1, "innerRadius"    # F
    .param p2, "externalRadius"    # F
    .param p3, "isGreen"    # Z

    .prologue
    .line 84
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, p2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 85
    .local v1, "dim":I
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 86
    .local v0, "circlesLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 87
    new-instance v2, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->setInnerRadius(F)V

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->setExternalRadius(F)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    invoke-virtual {v2, p3}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->setColor(Z)V

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    return-object v2
.end method


# virtual methods
.method public changeCircleColor(Z)V
    .locals 1
    .param p1, "isGreen"    # Z

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->setColor(Z)V

    .line 71
    return-void
.end method

.method public isRun()Z
    .locals 2

    .prologue
    .line 74
    const/4 v0, 0x0

    .line 75
    .local v0, "isRun":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->animationRunner:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    if-nez v1, :cond_0

    .line 76
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->animationRunner:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->isRun()Z

    move-result v0

    goto :goto_0
.end method

.method public startAnimation()V
    .locals 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->removeView(Landroid/view/View;)V

    .line 55
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->r1:F

    iget v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->R1:F

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->addCircleView(FFZ)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    .line 56
    new-instance v0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    const/4 v2, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;-><init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;FF)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->animationRunner:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->animationRunner:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    const/16 v1, 0x9

    const/16 v2, 0x168

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->start(II)V

    .line 58
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->heartrateCircle:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->removeView(Landroid/view/View;)V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->animationRunner:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleProgressView;->animationRunner:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->stop()V

    .line 67
    :cond_1
    return-void
.end method
