.class Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;
.super Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;
.source "CategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->onEnd(Landroid/widget/ListView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

.field final synthetic val$request:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Landroid/content/Context;Landroid/widget/ListView;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Landroid/widget/ListView;

    .prologue
    .line 399
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    iput-object p4, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->val$request:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/LoadMoreFoodInfoDataTask;-><init>(Landroid/content/Context;Landroid/widget/ListView;)V

    return-void
.end method


# virtual methods
.method protected doTask()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->mSearchApi:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->access$900(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->val$request:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->performFoodSearchByCategory(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchByCategoryListRequest;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v0

    return-object v0
.end method

.method protected onDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403
    .local p1, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult<Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 411
    :goto_0
    return-void

    .line 406
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->this$0:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->generateListItems(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;)Ljava/util/List;

    move-result-object v0

    .line 407
    .local v0, "foodListItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mItemsAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->access$1700(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->addListItems(Ljava/util/List;)V

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mItemsAdapter:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->access$1700(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ItemsAdapter;->notifyDataSetChanged()V

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->mSearchByCategoryListResult:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->access$1802(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    .line 410
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->this$1:Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->isTaskRunning:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;->access$1902(Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener;Z)Z

    goto :goto_0
.end method

.method protected bridge synthetic onDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    .prologue
    .line 399
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/fragments/CategoryFragment$OnScrollToCategoryEndListener$1;->onDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListByCategoryResult;)V

    return-void
.end method
