.class public Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever$WFLDataReceivedListener;
.super Ljava/lang/Object;
.source "ClearCoverBroadcastReciever.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData$OnPedoMeterSensorDataReceiveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WFLDataReceivedListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponseReceived(FFFIIIIIIIIJ)V
    .locals 1
    .param p1, "distance"    # F
    .param p2, "cal"    # F
    .param p3, "speed"    # F
    .param p4, "totalStep"    # I
    .param p5, "walkingStep"    # I
    .param p6, "runStep"    # I
    .param p7, "updownStep"    # I
    .param p8, "activeTime"    # I
    .param p9, "inactiveTime"    # I
    .param p10, "healthySteps"    # I
    .param p11, "deviceType"    # I
    .param p12, "updateTime"    # J

    .prologue
    .line 377
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->isCheckedCoverInSettings(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 378
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->onUpdate(Landroid/content/Context;I)V

    .line 380
    :cond_0
    return-void
.end method

.method public onWalkingModeChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 390
    return-void
.end method

.method public onWearableSyncComplete(II)V
    .locals 0
    .param p1, "error_code"    # I
    .param p2, "deviceType"    # I

    .prologue
    .line 385
    return-void
.end method
