.class public final enum Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;
.super Ljava/lang/Enum;
.source "IBackupListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BackupStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

.field public static final enum DONE:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

.field public static final enum DONE_BULK_MODE:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

.field public static final enum ONGOING:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

.field public static final enum STARTED:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

.field public static final enum STOPPED:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->STARTED:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    .line 16
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    const-string v1, "ONGOING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->ONGOING:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    .line 21
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->DONE:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    .line 26
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    const-string v1, "DONE_BULK_MODE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->DONE_BULK_MODE:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    .line 31
    new-instance v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->STOPPED:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->STARTED:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->ONGOING:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->DONE:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->DONE_BULK_MODE:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->STOPPED:Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->$VALUES:[Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->$VALUES:[Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/framework/repository/database/old/IBackupListener$BackupStatus;

    return-object v0
.end method
