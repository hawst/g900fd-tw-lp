.class Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;
.super Ljava/lang/Object;
.source "ProfileWeightView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditTextFocusHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$1;

    .prologue
    .line 436
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 439
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->isChangeFocus:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$1102(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;Z)Z

    .line 440
    if-nez p2, :cond_0

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->checkValueText()Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    # getter for: Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->mListener:Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;)Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFocusChangeInterface;->onProfileDialog(Landroid/view/View;)V

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView$EditTextFocusHandler;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileWeightView;->setMinValueText()V

    .line 449
    :cond_0
    return-void
.end method
