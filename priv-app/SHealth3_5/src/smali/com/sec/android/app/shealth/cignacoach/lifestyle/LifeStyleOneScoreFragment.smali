.class public Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "LifeStyleOneScoreFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCignaLifestyleScoreHeader:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

.field private mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

.field private mProgressTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mScoreReassessMessage:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;)Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaLifestyleScoreHeader:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    return-object v0
.end method


# virtual methods
.method public initOneScoreContents()V
    .locals 2

    .prologue
    .line 57
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mScoreReassessMessage:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getScoreReassessMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->isTimeToReassess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    const/4 v0, 0x0

    .line 114
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 136
    :goto_0
    return-void

    .line 117
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 118
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 119
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 121
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v1, "remainCategory":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;>;"
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 129
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 130
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v2, "intent_lifestyle_categorys"

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x7f0801e4
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 38
    const v1, 0x7f030069

    invoke-virtual {p1, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 40
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x7f0801f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaLifestyleScoreHeader:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaLifestyleScoreHeader:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getOverallScore()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getOutOfMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getScoreCoachDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v5, v4, v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setCurrentScoreInfo(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const v1, 0x7f0801e4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    .line 46
    const v1, 0x7f0801f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mScoreReassessMessage:Landroid/widget/TextView;

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->initOneScoreContents()V

    .line 50
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mCignaLifestyleScoreHeader:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setCustomHeaderMode()V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->updateHeaderProgress()V

    .line 53
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mProgressTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mProgressTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 106
    :cond_0
    return-void
.end method

.method public updateHeaderProgress()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mProgressTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mProgressTask:Landroid/os/AsyncTask;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 73
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleOneScoreFragment;->mProgressTask:Landroid/os/AsyncTask;

    .line 98
    return-void
.end method
