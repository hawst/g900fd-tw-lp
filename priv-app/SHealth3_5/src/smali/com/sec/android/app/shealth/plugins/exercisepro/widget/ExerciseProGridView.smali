.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;
.super Ljava/lang/Object;
.source "ExerciseProGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$GridAdapter;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;
    }
.end annotation


# static fields
.field private static final ID_IMAGE_BACKGROUND:I = 0x7f0807dd

.field private static final ID_IMAGE_DELETE_ICON:I = 0x7f0807de

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private bitmapLoader:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

.field private btnDelete:Landroid/widget/ImageButton;

.field private btnPhoto:Landroid/widget/ImageButton;

.field private context:Landroid/content/Context;

.field private isEditMode:Z

.field public mPhotoDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field private onGridItemListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowId"    # J
    .param p4, "isEdit"    # Z

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->InitExerciseProGridView(Landroid/content/Context;JZ)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JZZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowId"    # J
    .param p4, "isEdit"    # Z
    .param p5, "isInfoEdit"    # Z

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->InitExerciseProGridView(Landroid/content/Context;JZ)V

    .line 44
    return-void
.end method

.method private InitExerciseProGridView(Landroid/content/Context;JZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowId"    # J
    .param p4, "isEdit"    # Z

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->context:Landroid/content/Context;

    .line 51
    iput-boolean p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->isEditMode:Z

    .line 52
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->updatePhotoData(J)V

    .line 53
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->bitmapLoader:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->bitmapLoader:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->onGridItemListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->isEditMode:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnPhoto:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;
    .param p1, "x1"    # Landroid/view/ViewGroup;
    .param p2, "x2"    # Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->initViewBySize(Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V

    return-void
.end method

.method private initViewBySize(Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V
    .locals 7
    .param p1, "parentView"    # Landroid/view/ViewGroup;
    .param p2, "photoData"    # Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .prologue
    const v4, 0x7f0807dd

    const/4 v6, 0x0

    .line 173
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 174
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 177
    .local v0, "imageResource":Landroid/widget/ImageButton;
    const v2, 0x7f0807de

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;

    .line 180
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->isEditMode:Z

    if-eqz v2, :cond_1

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 185
    :goto_0
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnPhoto:Landroid/widget/ImageButton;

    .line 187
    const v2, -0x4c4c4d

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 188
    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 189
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901e5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->bitmapLoader:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseProBitmapLoader;->download(Ljava/lang/String;Landroid/widget/ImageView;II)V

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 192
    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 194
    :cond_0
    return-void

    .line 183
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getImageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPhotoGridView()Landroid/view/View;
    .locals 10

    .prologue
    .line 72
    const/4 v5, 0x0

    .line 73
    .local v5, "mainContainer":Landroid/view/ViewGroup;
    const/4 v3, 0x0

    .line 74
    .local v3, "idMainConteiner":I
    const/4 v1, 0x0

    .line 75
    .local v1, "extra_pics":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 104
    const v3, 0x7f0301ca

    .line 105
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v1, v8, -0x9

    .line 129
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->context:Landroid/content/Context;

    const/4 v9, 0x0

    invoke-static {v8, v3, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .end local v5    # "mainContainer":Landroid/view/ViewGroup;
    check-cast v5, Landroid/view/ViewGroup;

    .line 130
    .restart local v5    # "mainContainer":Landroid/view/ViewGroup;
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 131
    .local v4, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    add-int/2addr v8, v1

    if-gt v2, v8, :cond_1

    .line 132
    move v6, v2

    .line 133
    .local v6, "pos":I
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 134
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 135
    .local v0, "exercise":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    if-ge v2, v8, :cond_0

    .line 136
    invoke-virtual {v5, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .local v7, "viewItem":Landroid/view/View;
    move-object v8, v7

    .line 137
    check-cast v8, Landroid/view/ViewGroup;

    invoke-direct {p0, v8, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->initViewBySize(Landroid/view/ViewGroup;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V

    .line 138
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 139
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;

    new-instance v9, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnDelete:Landroid/widget/ImageButton;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 152
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->btnPhoto:Landroid/widget/ImageButton;

    new-instance v9, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$2;

    invoke-direct {v9, p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;I)V

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    .end local v0    # "exercise":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .end local v7    # "viewItem":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 77
    .end local v2    # "i":I
    .end local v4    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    .end local v6    # "pos":I
    :pswitch_0
    const v3, 0x7f0301c2

    .line 78
    goto :goto_0

    .line 80
    :pswitch_1
    const v3, 0x7f0301c3

    .line 81
    goto :goto_0

    .line 83
    :pswitch_2
    const v3, 0x7f0301c4

    .line 84
    goto :goto_0

    .line 86
    :pswitch_3
    const v3, 0x7f0301c5

    .line 87
    goto :goto_0

    .line 89
    :pswitch_4
    const v3, 0x7f0301c6

    .line 90
    goto :goto_0

    .line 92
    :pswitch_5
    const v3, 0x7f0301c7

    .line 93
    goto :goto_0

    .line 95
    :pswitch_6
    const v3, 0x7f0301c8

    .line 96
    goto :goto_0

    .line 98
    :pswitch_7
    const v3, 0x7f0301c9

    .line 99
    goto :goto_0

    .line 101
    :pswitch_8
    const v3, 0x7f0301ca

    .line 102
    goto :goto_0

    .line 165
    .restart local v2    # "i":I
    .restart local v4    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    :cond_1
    return-object v5

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public setExerciseGridItemListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;)V
    .locals 0
    .param p1, "onExerciseGridItemListener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->onGridItemListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView$OnExerciseGridItemListener;

    .line 198
    return-void
.end method

.method public updatePhotoData(J)V
    .locals 7
    .param p1, "rowId"    # J

    .prologue
    .line 57
    const/4 v6, 0x0

    .line 60
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 61
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 62
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getExercisePhotoDataList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    if-eqz v6, :cond_0

    .line 67
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 69
    :cond_0
    return-void

    .line 66
    .end local v3    # "selection":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 67
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method public viewSize()I
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProGridView;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
