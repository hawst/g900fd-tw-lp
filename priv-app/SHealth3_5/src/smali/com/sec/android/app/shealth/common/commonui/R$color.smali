.class public final Lcom/sec/android/app/shealth/common/commonui/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final accessory_connection_view_state_text_color:I = 0x7f070051

.field public static final alert_dialog_title_dark_color:I = 0x7f070067

.field public static final alert_dialog_title_light_color:I = 0x7f070068

.field public static final cancel_ok_button_text_color:I = 0x7f070041

.field public static final cancel_ok_button_text_selector:I = 0x7f07029b

.field public static final cancel_ok_green_text_color:I = 0x7f070064

.field public static final cursor_color:I = 0x7f07007e

.field public static final default_background_color:I = 0x7f07004f

.field public static final default_text_color:I = 0x7f07004e

.field public static final dialog_item_text:I = 0x7f07004c

.field public static final dialog_secondary_text:I = 0x7f07004d

.field public static final dialog_title:I = 0x7f07004b

.field public static final dialog_top_text_color:I = 0x7f070065

.field public static final health_care_input_background:I = 0x7f07006d

.field public static final health_care_summary_bg_dark:I = 0x7f07006c

.field public static final input_field_normal_bottom_line:I = 0x7f070077

.field public static final input_field_ubnormal_bottom_line:I = 0x7f070078

.field public static final input_view_normal_color:I = 0x7f07006e

.field public static final input_view_systolic_input_color:I = 0x7f07006f

.field public static final inputmodule_abnormal_color:I = 0x7f070075

.field public static final inputmodule_normal_color:I = 0x7f070074

.field public static final list_popup_item_default_text_color:I = 0x7f07007a

.field public static final list_popup_item_selected_text_color:I = 0x7f070079

.field public static final menu_background:I = 0x7f070046

.field public static final menu_background_white:I = 0x7f07004a

.field public static final menu_selected:I = 0x7f070045

.field public static final progress_text_color:I = 0x7f07007f

.field public static final spinner_item_normal_text:I = 0x7f07007c

.field public static final spinner_item_selected_text:I = 0x7f07007d

.field public static final spinner_text_color_selector:I = 0x7f0702b3

.field public static final summary_animated_counter_bad_color:I = 0x7f070071

.field public static final summary_animated_counter_default_color:I = 0x7f070070

.field public static final summary_animated_counter_good_color:I = 0x7f070072

.field public static final summary_animated_counter_shadow_color:I = 0x7f070073

.field public static final summary_view_abnormal_color:I = 0x7f070069

.field public static final summary_view_background_color:I = 0x7f070055

.field public static final summary_view_balance_interval_color:I = 0x7f07006b

.field public static final summary_view_bottom_background_color:I = 0x7f070056

.field public static final summary_view_button_text_color:I = 0x7f070053

.field public static final summary_view_button_text_color_disabled:I = 0x7f070054

.field public static final summary_view_button_text_shadow_color:I = 0x7f070052

.field public static final summary_view_content_bar_state_and_info_text_color:I = 0x7f07005b

.field public static final summary_view_content_bar_title_color:I = 0x7f07005c

.field public static final summary_view_content_bar_value_text_balance_color:I = 0x7f070058

.field public static final summary_view_content_bar_value_text_no_data_color:I = 0x7f07005a

.field public static final summary_view_content_bar_value_text_unbalance_color:I = 0x7f070059

.field public static final summary_view_content_divider_color:I = 0x7f07005d

.field public static final summary_view_header_balance_state_text_color:I = 0x7f070050

.field public static final summary_view_health_care_background_color:I = 0x7f070057

.field public static final summary_view_normal_color:I = 0x7f07006a

.field public static final summary_view_progress_bar_aftermeal_column_unbalance_color:I = 0x7f070061

.field public static final summary_view_progress_bar_column_balance_color:I = 0x7f07005e

.field public static final summary_view_progress_bar_fasting_column_unbalance_color:I = 0x7f070060

.field public static final summary_view_progress_bar_label_color:I = 0x7f070063

.field public static final summary_view_progress_bar_slider_balance_color:I = 0x7f07005f

.field public static final summary_view_progress_bar_title_color:I = 0x7f070062

.field public static final text_regular:I = 0x7f07007b

.field public static final text_view_item_color:I = 0x7f070066

.field public static final vertical_progress_mark_color:I = 0x7f070076


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
