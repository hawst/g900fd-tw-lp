.class public final enum Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
.super Ljava/lang/Enum;
.source "QuickInputCalorieProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MealType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

.field public static final enum BREAK_FAST:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

.field public static final enum DINNER:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

.field public static final enum LUNCH:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

.field public static final enum SNACKS:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 118
    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    const-string v1, "BREAK_FAST"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->BREAK_FAST:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    const-string v1, "LUNCH"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->LUNCH:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    const-string v1, "DINNER"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->DINNER:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    new-instance v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    const-string v1, "SNACKS"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->SNACKS:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    .line 116
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    sget-object v1, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->BREAK_FAST:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->LUNCH:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->DINNER:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->SNACKS:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->$VALUES:[Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 116
    const-class v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->$VALUES:[Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    return-object v0
.end method
