.class public Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;
.super Landroid/widget/LinearLayout;
.source "UvStateSummaryChineseBar.java"


# instance fields
.field private ivPolygon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const v0, 0x7f030274

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 25
    const v0, 0x7f0804f9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->ivPolygon:Landroid/widget/ImageView;

    .line 26
    return-void
.end method


# virtual methods
.method public moveToPolygon(DZ)V
    .locals 13
    .param p1, "uvScore"    # D
    .param p3, "hasNoUvSensor"    # Z

    .prologue
    .line 30
    const-wide/high16 v9, 0x4026000000000000L    # 11.0

    cmpl-double v9, p1, v9

    if-lez v9, :cond_0

    const-wide/high16 p1, 0x4026000000000000L    # 11.0

    .line 31
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->getInstance()Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;

    move-result-object v9

    const/16 v10, 0xaa

    iget-object v11, p0, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/shealth/uv/utils/UvPluginUtils;->convertDptoPx(ILandroid/content/Context;)I

    move-result v8

    .line 32
    .local v8, "width":I
    const-wide/16 v6, 0x0

    .line 33
    .local v6, "uvIndexMargin":D
    const-wide/16 v2, 0x0

    .line 34
    .local v2, "conversionScore":D
    const-wide/16 v4, 0x0

    .line 39
    .local v4, "tempUvScore":D
    move-wide v4, p1

    .line 40
    const-wide/high16 v9, 0x4008000000000000L    # 3.0

    cmpl-double v9, v4, v9

    if-ltz v9, :cond_1

    const-wide/high16 v9, 0x4014000000000000L    # 5.0

    cmpg-double v9, v4, v9

    if-gtz v9, :cond_1

    const-wide v9, 0x3fd999999999999aL    # 0.4

    sub-double/2addr v4, v9

    .line 41
    :cond_1
    const-wide/high16 v9, 0x4018000000000000L    # 6.0

    cmpl-double v9, v4, v9

    if-ltz v9, :cond_2

    const-wide/high16 v9, 0x401c000000000000L    # 7.0

    cmpg-double v9, v4, v9

    if-gtz v9, :cond_2

    const-wide v9, 0x3fe6666666666666L    # 0.7

    sub-double/2addr v4, v9

    .line 42
    :cond_2
    const-wide/high16 v9, 0x4020000000000000L    # 8.0

    cmpl-double v9, v4, v9

    if-ltz v9, :cond_3

    const-wide/high16 v9, 0x4022000000000000L    # 9.0

    cmpg-double v9, v4, v9

    if-gtz v9, :cond_3

    const-wide v9, 0x3fe999999999999aL    # 0.8

    sub-double/2addr v4, v9

    .line 43
    :cond_3
    const-wide/high16 v9, 0x4024000000000000L    # 10.0

    cmpl-double v9, v4, v9

    if-ltz v9, :cond_4

    const-wide/high16 v9, 0x4026000000000000L    # 11.0

    cmpg-double v9, v4, v9

    if-gtz v9, :cond_4

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v9

    .line 45
    :cond_4
    const-wide/16 v9, 0x0

    cmpg-double v9, v4, v9

    if-gez v9, :cond_6

    .line 46
    const-wide/16 v2, 0x0

    .line 53
    :goto_0
    const-wide/high16 v9, 0x4024000000000000L    # 10.0

    div-double v9, v2, v9

    const-wide/high16 v11, 0x4059000000000000L    # 100.0

    mul-double v0, v9, v11

    .line 54
    .local v0, "converScorePer":D
    const-wide/high16 v9, 0x4059000000000000L    # 100.0

    div-double v9, v0, v9

    int-to-double v11, v8

    mul-double v6, v9, v11

    .line 55
    iget-object v9, p0, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->ivPolygon:Landroid/widget/ImageView;

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    long-to-int v10, v10

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setX(F)V

    .line 57
    if-eqz p3, :cond_5

    const-wide/high16 v9, -0x4000000000000000L    # -2.0

    cmpl-double v9, p1, v9

    if-nez v9, :cond_5

    .line 58
    iget-object v9, p0, Lcom/sec/android/app/shealth/uv/custom/UvStateSummaryChineseBar;->ivPolygon:Landroid/widget/ImageView;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 61
    :cond_5
    return-void

    .line 47
    .end local v0    # "converScorePer":D
    :cond_6
    const-wide/high16 v9, 0x4024000000000000L    # 10.0

    cmpl-double v9, v4, v9

    if-lez v9, :cond_7

    .line 48
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    goto :goto_0

    .line 50
    :cond_7
    move-wide v2, v4

    goto :goto_0
.end method
