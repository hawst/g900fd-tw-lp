.class public interface abstract Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;
.super Ljava/lang/Object;
.source "FavoritableListItem.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/IListItem;


# virtual methods
.method public abstract addToFavorites()V
.end method

.method public abstract getFavoriteWrapper()Landroid/view/View;
.end method

.method public abstract isFavorite()Z
.end method

.method public abstract removeFromFavorites()V
.end method
