.class public Lcom/sec/android/app/shealth/CheckVersionCode;
.super Ljava/lang/Object;
.source "CheckVersionCode.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mCignaTermsOfUseUpdate:F

.field private mContext:Landroid/content/Context;

.field private mPrivacyUpdate:F

.field private mTermOfUseUpdate:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-class v0, Lcom/sec/android/app/shealth/CheckVersionCode;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mContext:Landroid/content/Context;

    .line 20
    iput v1, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mTermOfUseUpdate:F

    .line 21
    iput v1, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mPrivacyUpdate:F

    .line 22
    iput v1, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mCignaTermsOfUseUpdate:F

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method

.method private getVersionParser(Ljava/lang/String;)V
    .locals 8
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 98
    iget-object v5, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string v6, "getVersionParser "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 101
    .local v2, "json":Lorg/json/JSONObject;
    const-string v5, "TermsOfUse_version"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 102
    .local v4, "termsVersion":Ljava/lang/String;
    const-string v5, "Privacy_version"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 103
    .local v3, "privacyVersion":Ljava/lang/String;
    const-string v5, "Cinga_TermsOfUse_version"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "cignaTermsVersion":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mTermOfUseUpdate:F

    .line 106
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mPrivacyUpdate:F

    .line 107
    if-eqz v0, :cond_0

    .line 108
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    iput v5, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mCignaTermsOfUseUpdate:F

    .line 111
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getVersionParser mTermOfUseUpdate : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mTermOfUseUpdate:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",mPrivacyUpdate : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mPrivacyUpdate:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    .end local v0    # "cignaTermsVersion":Ljava/lang/String;
    .end local v2    # "json":Lorg/json/JSONObject;
    .end local v3    # "privacyVersion":Ljava/lang/String;
    .end local v4    # "termsVersion":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Lorg/json/JSONException;
    iget-object v5, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string v6, "getVersionParser JSONException"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getCignaTermsOfUseUpdateCode()F
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mCignaTermsOfUseUpdate:F

    return v0
.end method

.method public getPrivacyUpdateCode()F
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTermsOfUseUpdateCode getPrivacyUpdateCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mPrivacyUpdate:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mPrivacyUpdate:F

    return v0
.end method

.method public getTermsOfUseUpdateCode()F
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTermsOfUseUpdateCode mTermOfUseUpdate : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mTermOfUseUpdate:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mTermOfUseUpdate:F

    return v0
.end method

.method public isCignaTermsOfUseUpdate()Z
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachSharedPrefereneceHelper;->getCignaTermsOfUseVerionCode(Landroid/content/Context;)F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mCignaTermsOfUseUpdate:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 78
    const/4 v0, 0x1

    .line 80
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrivacyUpdate()Z
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getPrivacyVerionCode(Landroid/content/Context;)F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mPrivacyUpdate:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 72
    const/4 v0, 0x1

    .line 74
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTermsOfUseUpdate()Z
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getTermsOfUseVerionCode(Landroid/content/Context;)F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mTermOfUseUpdate:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 66
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readAssertFile()V
    .locals 10

    .prologue
    .line 30
    const/4 v3, 0x0

    .line 31
    .local v3, "is":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 33
    .local v5, "versions":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mContext:Landroid/content/Context;

    if-eqz v7, :cond_1

    .line 34
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "readAssertFile"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    const-string v8, "CHECK_VERSION_CODE.txt"

    invoke-virtual {v7, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 38
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v4

    .line 40
    .local v4, "size":I
    new-array v0, v4, [B

    .line 42
    .local v0, "buffer":[B
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    .line 44
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    .end local v5    # "versions":Ljava/lang/String;
    .local v6, "versions":Ljava/lang/String;
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "readAssertFile size : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",versions :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/CheckVersionCode;->getVersionParser(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 55
    if-eqz v3, :cond_0

    .line 56
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v5, v6

    .line 63
    .end local v0    # "buffer":[B
    .end local v4    # "size":I
    .end local v6    # "versions":Ljava/lang/String;
    .restart local v5    # "versions":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 58
    .end local v5    # "versions":Ljava/lang/String;
    .restart local v0    # "buffer":[B
    .restart local v4    # "size":I
    .restart local v6    # "versions":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "readAssertFile IOException e "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    .line 61
    .end local v6    # "versions":Ljava/lang/String;
    .restart local v5    # "versions":Ljava/lang/String;
    goto :goto_0

    .line 49
    .end local v0    # "buffer":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "size":I
    :catch_1
    move-exception v2

    .line 50
    .local v2, "ex":Ljava/io/IOException;
    :goto_1
    :try_start_3
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "readAssertFile IOException"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 55
    if-eqz v3, :cond_1

    .line 56
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 58
    :catch_2
    move-exception v1

    .line 59
    .restart local v1    # "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "readAssertFile IOException e "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 51
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "ex":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 52
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "readAssertFile Exception"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 55
    if-eqz v3, :cond_1

    .line 56
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 58
    :catch_4
    move-exception v1

    .line 59
    .local v1, "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string/jumbo v8, "readAssertFile IOException e "

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 54
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 55
    :goto_3
    if-eqz v3, :cond_2

    .line 56
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 60
    :cond_2
    :goto_4
    throw v7

    .line 58
    :catch_5
    move-exception v1

    .line 59
    .restart local v1    # "e":Ljava/io/IOException;
    iget-object v8, p0, Lcom/sec/android/app/shealth/CheckVersionCode;->TAG:Ljava/lang/String;

    const-string/jumbo v9, "readAssertFile IOException e "

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 54
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "versions":Ljava/lang/String;
    .restart local v0    # "buffer":[B
    .restart local v4    # "size":I
    .restart local v6    # "versions":Ljava/lang/String;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "versions":Ljava/lang/String;
    .restart local v5    # "versions":Ljava/lang/String;
    goto :goto_3

    .line 51
    .end local v5    # "versions":Ljava/lang/String;
    .restart local v6    # "versions":Ljava/lang/String;
    :catch_6
    move-exception v1

    move-object v5, v6

    .end local v6    # "versions":Ljava/lang/String;
    .restart local v5    # "versions":Ljava/lang/String;
    goto :goto_2

    .line 49
    .end local v5    # "versions":Ljava/lang/String;
    .restart local v6    # "versions":Ljava/lang/String;
    :catch_7
    move-exception v2

    move-object v5, v6

    .end local v6    # "versions":Ljava/lang/String;
    .restart local v5    # "versions":Ljava/lang/String;
    goto :goto_1
.end method
