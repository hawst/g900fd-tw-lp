.class Lcom/sec/android/app/shealth/uv/UvActivity$21;
.super Ljava/lang/Object;
.source "UvActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvActivity;)V
    .locals 0

    .prologue
    .line 918
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 6
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 922
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2900(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 924
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2900(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    move-result-object v2

    iput-object v3, v2, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinDatabaseHelper:Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    .line 925
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2900(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    if-eqz v2, :cond_0

    .line 926
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2900(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;->clear()V

    .line 927
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSkinTypeDialog:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$2900(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    move-result-object v2

    iput-object v3, v2, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mUserSkinToneType:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;

    .line 930
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "skin_type_checked"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 931
    .local v0, "checked":Z
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/common/SkinDatabaseHelper;->getData()Lcom/sec/android/app/shealth/uv/data/SkinData;

    move-result-object v1

    .line 932
    .local v1, "skinData":Lcom/sec/android/app/shealth/uv/data/SkinData;
    if-nez v1, :cond_2

    if-eqz v0, :cond_4

    .line 933
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v2

    iput-boolean v5, v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->needsLocationPopup:Z

    .line 934
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/uv/UvActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 935
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isFragmentInitialized:Z

    if-eqz v2, :cond_3

    .line 936
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string/jumbo v4, "skin_type_checked"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v2, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->isHideSkinSettings:Z

    .line 937
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    # getter for: Lcom/sec/android/app/shealth/uv/UvActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;
    invoke-static {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->access$400(Lcom/sec/android/app/shealth/uv/UvActivity;)Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->updateLayout()V

    .line 943
    :cond_3
    :goto_0
    return-void

    .line 940
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/uv/UvActivity$21;->this$0:Lcom/sec/android/app/shealth/uv/UvActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/uv/UvActivity;->finish()V

    goto :goto_0
.end method
