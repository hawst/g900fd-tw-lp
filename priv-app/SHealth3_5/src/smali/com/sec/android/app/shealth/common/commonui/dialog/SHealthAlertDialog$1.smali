.class Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;
.super Ljava/lang/Object;
.source "SHealthAlertDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 160
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    .line 163
    .local v1, "parent":Landroid/view/View;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 164
    .local v2, "r":Landroid/graphics/Rect;
    invoke-virtual {v1, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 165
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v3

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    iget v5, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    sub-int v0, v3, v4

    .line 167
    .local v0, "hdiff":I
    const/16 v3, 0x80

    if-gt v0, v3, :cond_0

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$1;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->mFocusedView:Landroid/view/View;
    invoke-static {v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Landroid/view/View;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->toggleSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 173
    .end local v0    # "hdiff":I
    .end local v1    # "parent":Landroid/view/View;
    .end local v2    # "r":Landroid/graphics/Rect;
    :cond_0
    return-void
.end method
