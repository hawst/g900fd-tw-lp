.class public Lcom/sec/android/app/shealth/uv/state/UvStateFinished;
.super Lcom/sec/android/app/shealth/uv/state/UvState;
.source "UvStateFinished.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 0
    .param p1, "summaryFragment"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/state/UvState;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    .line 15
    return-void
.end method


# virtual methods
.method public clearForNextState()V
    .locals 2

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->TAG:Ljava/lang/String;

    const-string v1, "clearForNextState()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    return-void
.end method

.method public reStartStates()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->updateViewWithAnimation(Z)V

    .line 22
    return-void
.end method

.method public terminateState()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public updateNextState(II)V
    .locals 3
    .param p1, "rate"    # I
    .param p2, "interval"    # I

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateNextState() rate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    const/16 v0, -0x3e9

    if-ne p1, v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateViewWithAnimation(Z)V

    .line 31
    :cond_0
    return-void
.end method

.method public updateViewWithAnimation(Z)V
    .locals 2
    .param p1, "hasAnimation"    # Z

    .prologue
    .line 35
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 36
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    .line 37
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 38
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 39
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateFinished;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 40
    return-void

    .line 37
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
