.class Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->SwitchTabs(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v2, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHeaderTextResIds:[I
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)[I

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHeaderTextResIds:[I
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)[I

    move-result-object v0

    array-length v0, v0

    if-le v0, v5, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mHeaderTextResIds:[I
    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->access$000(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)[I

    move-result-object v4

    aget v4, v4, v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->updateScanningFragment(IZLjava/util/ArrayList;II)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->scanfragment:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDataType:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->access$100(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity$2;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->mDeviceTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;->access$200(Lcom/sec/android/app/shealth/framework/ui/base/ConnectivityActivity;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, -0x1

    const/4 v5, 0x2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->updateScanningFragment(IZLjava/util/ArrayList;II)V

    goto :goto_0
.end method
