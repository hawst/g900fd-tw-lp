.class Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->requestPairedDevices()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->no_connected_devices:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevicesText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$900(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$color;->no_connected_accessories_text:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->nodevices:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1000(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mScannedlist:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$1100(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment$10;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->mInformationView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;->access$700(Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
