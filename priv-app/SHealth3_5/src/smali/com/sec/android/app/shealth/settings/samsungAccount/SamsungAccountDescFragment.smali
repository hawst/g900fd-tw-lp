.class public Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;
.super Landroid/support/v4/app/Fragment;
.source "SamsungAccountDescFragment.java"


# instance fields
.field private mCount:I

.field private mPageDesc:Ljava/lang/String;

.field private mPageNumber:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static create(I)Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;
    .locals 3
    .param p0, "pageNumber"    # I

    .prologue
    .line 40
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;-><init>()V

    .line 41
    .local v1, "fragment":Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 42
    .local v0, "args":Landroid/os/Bundle;
    const-string/jumbo v2, "page"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 43
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->setArguments(Landroid/os/Bundle;)V

    .line 44
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mPageNumber:I

    .line 51
    sget v0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->PAGE_COUNT:I

    iput v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mCount:I

    .line 52
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f0808b5

    const/4 v5, 0x0

    .line 57
    const v1, 0x7f0301f7

    invoke-virtual {p1, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 59
    .local v0, "rootView":Landroid/view/ViewGroup;
    iget v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mPageNumber:I

    packed-switch v1, :pswitch_data_0

    .line 72
    :goto_0
    const v1, 0x7f0808b6

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mPageDesc:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mPageDesc:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f090c8d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mPageNumber:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget v5, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 75
    return-object v0

    .line 61
    :pswitch_0
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020293

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 62
    const v1, 0x7f090e82

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mPageDesc:Ljava/lang/String;

    goto :goto_0

    .line 65
    :pswitch_1
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020294

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 66
    const v1, 0x7f090e84

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/SamsungAccountDescFragment;->mPageDesc:Ljava/lang/String;

    goto/16 :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
