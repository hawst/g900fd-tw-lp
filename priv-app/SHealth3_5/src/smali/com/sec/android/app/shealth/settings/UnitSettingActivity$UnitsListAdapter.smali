.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "UnitSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UnitsListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mUnitSettingsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 643
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    .line 644
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 645
    iput-object p4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->mUnitSettingsList:Ljava/util/List;

    .line 646
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;)V
    .locals 1
    .param p1, "object"    # Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    .prologue
    .line 665
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->mUnitSettingsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666
    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 640
    check-cast p1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->add(Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;)V

    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->mUnitSettingsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 672
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->notifyDataSetChanged()V

    .line 674
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->mUnitSettingsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 655
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->mUnitSettingsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 640
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->getItem(I)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 660
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 678
    const/4 v0, 0x0

    .line 680
    .local v0, "vh":Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;
    new-instance v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;

    .end local v0    # "vh":Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;
    invoke-direct {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;-><init>()V

    .line 681
    .restart local v0    # "vh":Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03025b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 683
    const v1, 0x7f080a8e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitLayout:Landroid/widget/RelativeLayout;

    .line 684
    const v1, 0x7f080a8f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitTypeText:Landroid/widget/TextView;

    .line 685
    const v1, 0x7f080a90

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitvalueText:Landroid/widget/TextView;

    .line 687
    iget-object v2, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitTypeText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->mUnitSettingsList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;->mCategoryTitle:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 688
    iget-object v2, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitvalueText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->mUnitSettingsList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;->mCurrentUnit:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 690
    iget-object v2, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->mUnitSettingsList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitSettingsListItem;->mType:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 692
    iget-object v1, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitLayout:Landroid/widget/RelativeLayout;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitTypeText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitvalueText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 694
    iget-object v1, v0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$ViewHodler;->mUnitLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->popupCreateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 696
    return-object p2
.end method
