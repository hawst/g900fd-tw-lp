.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;
.super Landroid/view/View;
.source "ProfileFifthView.java"


# instance fields
.field private final height:I

.field private mBirthDayText:Landroid/widget/TextView;

.field private mContentObserver:Landroid/database/ContentObserver;

.field private mContext:Landroid/content/Context;

.field private mGenderImage:Landroid/widget/ImageView;

.field private mHeightParentLaout:Landroid/widget/LinearLayout;

.field private mHeightUnitText:Landroid/widget/TextView;

.field private mHeightValueText:Landroid/widget/TextView;

.field private mLevImage:Landroid/widget/ImageView;

.field private mLevelText:Landroid/widget/TextView;

.field private mNameText:Landroid/widget/TextView;

.field private mProfileImage:Landroid/widget/ImageView;

.field private mProfileLockImage:Landroid/widget/ImageView;

.field private mView:Landroid/view/View;

.field private mWeightParentLayout:Landroid/widget/LinearLayout;

.field private mWeightUnit:Landroid/widget/TextView;

.field private mWeightValueText:Landroid/widget/TextView;

.field private mshealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shealthProfile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 54
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    .line 55
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    .line 57
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevelText:Landroid/widget/TextView;

    .line 58
    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevImage:Landroid/widget/ImageView;

    .line 61
    const/4 v0, 0x1

    const v1, 0x438e8000    # 285.0f

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->height:I

    .line 113
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView$1;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContentObserver:Landroid/database/ContentObserver;

    .line 67
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    .line 68
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mshealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 70
    const v0, 0x7f0301e4

    invoke-static {p1, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    .line 72
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->initView(Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->registerDateFormatObserver()V

    .line 76
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->isUserFile()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mProfileImage:Landroid/widget/ImageView;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mProfileImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getProfileBitmap(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f02061a

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mProfileImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 100
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;)Lcom/samsung/android/sdk/health/content/ShealthProfile;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mshealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->initView(Lcom/samsung/android/sdk/health/content/ShealthProfile;)V

    return-void
.end method

.method public static getSystemDateFormat()Ljava/text/SimpleDateFormat;
    .locals 4

    .prologue
    .line 231
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 234
    .local v0, "pattern":Ljava/lang/String;
    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090714

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 246
    :goto_0
    return-object v1

    .line 239
    :cond_0
    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090715

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 246
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090716

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0
.end method

.method public static getSystemDateFormatProfile(Z)Ljava/text/SimpleDateFormat;
    .locals 4
    .param p0, "timeFormat24State"    # Z

    .prologue
    .line 254
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    check-cast v1, Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, "pattern":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 259
    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090717

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 289
    :goto_0
    return-object v1

    .line 264
    :cond_0
    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 266
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090718

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 271
    :cond_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090719

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 277
    :cond_2
    const-string v1, "dd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 279
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09071a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 282
    :cond_3
    const-string v1, "MM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 284
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09071b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    .line 289
    :cond_4
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09071c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto/16 :goto_0
.end method

.method private initView(Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 6
    .param p1, "shealthProfile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    .line 125
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f080333

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mProfileImage:Landroid/widget/ImageView;

    .line 127
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f080839

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mProfileLockImage:Landroid/widget/ImageView;

    .line 128
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f08083b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mGenderImage:Landroid/widget/ImageView;

    .line 129
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f08083a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mNameText:Landroid/widget/TextView;

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f08083c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mBirthDayText:Landroid/widget/TextView;

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f08083d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightParentLaout:Landroid/widget/LinearLayout;

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f080840

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightParentLayout:Landroid/widget/LinearLayout;

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f08083e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightValueText:Landroid/widget/TextView;

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f08083f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightUnitText:Landroid/widget/TextView;

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f080841

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightValueText:Landroid/widget/TextView;

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f080842

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightUnit:Landroid/widget/TextView;

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f080843

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevImage:Landroid/widget/ImageView;

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    const v4, 0x7f080844

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevelText:Landroid/widget/TextView;

    .line 143
    if-nez p1, :cond_0

    .line 144
    new-instance p1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .end local p1    # "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p1, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 147
    .restart local p1    # "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v3

    const v4, 0x2e635

    if-ne v3, v4, :cond_2

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mGenderImage:Landroid/widget/ImageView;

    const v4, 0x7f020241

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 154
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mshealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileDisclose()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mProfileLockImage:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 159
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mNameText:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    new-instance v1, Ljava/sql/Date;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Ljava/sql/Date;-><init>(J)V

    .line 162
    .local v1, "date":Ljava/sql/Date;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getSystemDateFormat()Ljava/text/SimpleDateFormat;

    move-result-object v2

    .line 163
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mBirthDayText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mBirthDayText:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/sql/Date;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v3

    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->setHeightvalue(ILcom/samsung/android/sdk/health/content/ShealthProfile;)V

    .line 166
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v3

    invoke-virtual {p0, v3, p1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->setWeightValue(ILcom/samsung/android/sdk/health/content/ShealthProfile;)V

    .line 170
    invoke-virtual {p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getActivityType()I

    move-result v0

    .line 172
    .local v0, "activityType":I
    const v3, 0x2bf21

    if-ne v0, v3, :cond_4

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205e1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevelText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090827

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    :cond_1
    :goto_2
    return-void

    .line 150
    .end local v0    # "activityType":I
    .end local v1    # "date":Ljava/sql/Date;
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mGenderImage:Landroid/widget/ImageView;

    const v4, 0x7f020240

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 157
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mProfileLockImage:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 175
    .restart local v0    # "activityType":I
    .restart local v1    # "date":Ljava/sql/Date;
    .restart local v2    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_4
    const v3, 0x2bf22

    if-ne v0, v3, :cond_5

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevelText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090828

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 178
    :cond_5
    const v3, 0x2bf23

    if-ne v0, v3, :cond_6

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205e5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 180
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevelText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090829

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 181
    :cond_6
    const v3, 0x2bf24

    if-ne v0, v3, :cond_7

    .line 182
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205e7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevelText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09082a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 184
    :cond_7
    const v3, 0x2bf25

    if-ne v0, v3, :cond_1

    .line 185
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205e9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 186
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mLevelText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09082b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method private registerDateFormatObserver()V
    .locals 4

    .prologue
    .line 108
    const-string v1, "date_format"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 109
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 111
    return-void
.end method

.method private setHeightvalue(ILcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 10
    .param p1, "type"    # I
    .param p2, "shealthProfile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    const v9, 0x7f09020b

    const v8, 0x7f09012f

    .line 209
    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v4

    .line 210
    .local v4, "value":F
    const v5, 0x249f1

    if-ne p1, v5, :cond_0

    .line 211
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->convertDecimalFormat(F)Ljava/lang/String;

    move-result-object v0

    .line 212
    .local v0, "data":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightUnitText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    const v7, 0x7f0900bc

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightValueText:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightParentLaout:Landroid/widget/LinearLayout;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightValueText:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightUnitText:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 227
    :goto_0
    return-void

    .line 217
    .end local v0    # "data":Ljava/lang/String;
    :cond_0
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileHeightView;->getFeetandInch(F)Ljava/util/HashMap;

    move-result-object v1

    .line 218
    .local v1, "feetInch":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-string v5, "feet"

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 219
    .local v2, "feetStr":Ljava/lang/String;
    const-string v5, "inch"

    invoke-virtual {v1, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 220
    .local v3, "inchStr":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 221
    .restart local v0    # "data":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightValueText:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightUnitText:Landroid/widget/TextView;

    const-string v6, ""

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mHeightParentLaout:Landroid/widget/LinearLayout;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901c0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901cd

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method


# virtual methods
.method cleanUp()V
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 299
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mView:Landroid/view/View;

    return-object v0
.end method

.method public setWeightValue(ILcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 7
    .param p1, "type"    # I
    .param p2, "shealthProfile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    .line 190
    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v2

    .line 192
    .local v2, "value":F
    const v3, 0x1fbd1

    if-ne p1, v3, :cond_0

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightUnit:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    const v5, 0x7f0900c0

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightUnit:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901d1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 196
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->convertDecimalFormat(F)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "data":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightValueText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightParentLayout:Landroid/widget/LinearLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09002c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightValueText:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightUnit:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 206
    return-void

    .line 198
    .end local v0    # "data":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightUnit:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mContext:Landroid/content/Context;

    const v5, 0x7f0900c2

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    const-string v3, "lb"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertKgToUnit(FLjava/lang/String;)F

    move-result v1

    .line 200
    .local v1, "lbValue":F
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->convertDecimalFormat(F)Ljava/lang/String;

    move-result-object v0

    .line 201
    .restart local v0    # "data":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFifthView;->mWeightUnit:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901d5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
