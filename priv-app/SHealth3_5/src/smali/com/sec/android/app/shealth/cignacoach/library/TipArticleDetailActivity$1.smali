.class Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;
.super Ljava/lang/Object;
.source "TipArticleDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->setActionBarNormalMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 129
    instance-of v4, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v4, :cond_0

    move-object v0, p1

    .line 130
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 132
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v4

    if-nez v4, :cond_4

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->favorite_checked:Z
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)Z

    move-result v2

    .line 135
    .local v2, "isChecked":Z
    if-eqz v2, :cond_3

    .line 136
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v4

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-static {v7}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getId()I

    move-result v7

    invoke-virtual {v4, v7}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->removeFavorite(I)Z

    move-result v3

    .line 137
    .local v3, "removeResult":Z
    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    if-nez v3, :cond_1

    move v4, v5

    :goto_0
    # setter for: Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->favorite_checked:Z
    invoke-static {v7, v4}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->access$002(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;Z)Z

    .line 138
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    if-nez v3, :cond_2

    :goto_1
    # invokes: Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getActionBarFavoriteResourceID(Z)I
    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;Z)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setActionIcon(I)V

    .line 139
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090c73

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mToast:Landroid/widget/Toast;

    .line 140
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    iget-object v4, v4, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 155
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    .end local v2    # "isChecked":Z
    .end local v3    # "removeResult":Z
    :cond_0
    :goto_2
    return-void

    .restart local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    .restart local v2    # "isChecked":Z
    .restart local v3    # "removeResult":Z
    :cond_1
    move v4, v6

    .line 137
    goto :goto_0

    :cond_2
    move v5, v6

    .line 138
    goto :goto_1

    .line 144
    .end local v3    # "removeResult":Z
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mSelectedArticleData:Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-static {v5}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->access$100(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->addFavorite(I)Z

    move-result v1

    .line 145
    .local v1, "addResult":Z
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->favorite_checked:Z
    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->access$002(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;Z)Z

    .line 146
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getActionBarFavoriteResourceID(Z)I
    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;Z)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->setActionIcon(I)V

    .line 147
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090c72

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v6}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mToast:Landroid/widget/Toast;

    .line 148
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    iget-object v4, v4, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->mToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 150
    .end local v1    # "addResult":Z
    .end local v2    # "isChecked":Z
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v4

    if-ne v4, v5, :cond_0

    .line 151
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->prepareShareView()V
    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)V

    .line 152
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "com.sec.android.app.shealth.cignacoach"

    const-string v6, "0033"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
