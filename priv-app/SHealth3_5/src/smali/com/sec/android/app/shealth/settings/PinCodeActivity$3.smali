.class Lcom/sec/android/app/shealth/settings/PinCodeActivity$3;
.super Ljava/lang/Object;
.source "PinCodeActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setListenersAccordingToAction()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    const/4 v0, 0x1

    .line 375
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->mPinString:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$800(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->updateTitleAndContinueButtonState(Landroid/text/Editable;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$900(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Landroid/text/Editable;)V

    .line 383
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->refreshFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$1100(Lcom/sec/android/app/shealth/settings/PinCodeActivity;)V

    .line 384
    return-void

    .line 381
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/PinCodeActivity$3;->this$0:Lcom/sec/android/app/shealth/settings/PinCodeActivity;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-lt v2, v0, :cond_1

    :goto_1
    # invokes: Lcom/sec/android/app/shealth/settings/PinCodeActivity;->setButtonStyle(Z)V
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/PinCodeActivity;->access$1000(Lcom/sec/android/app/shealth/settings/PinCodeActivity;Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 370
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 365
    return-void
.end method
