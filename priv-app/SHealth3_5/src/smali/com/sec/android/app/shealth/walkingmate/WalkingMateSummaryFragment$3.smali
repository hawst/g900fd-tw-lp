.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$3;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->getContentView(Landroid/content/Context;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0

    .prologue
    .line 685
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivetimeChanged(J)V
    .locals 0
    .param p1, "duration"    # J

    .prologue
    .line 707
    return-void
.end method

.method public onHealthyStepCountChanged(J)V
    .locals 0
    .param p1, "steps"    # J

    .prologue
    .line 689
    return-void
.end method

.method public onHealthyStepStatusChanged(Z)V
    .locals 3
    .param p1, "isHealthyStep"    # Z

    .prologue
    .line 693
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isHealthyStep ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateCurrentMode()V

    .line 695
    return-void
.end method

.method public onInactiveTimeChanged(ZJZ)V
    .locals 3
    .param p1, "isInactive"    # Z
    .param p2, "duration"    # J
    .param p4, "isNoti"    # Z

    .prologue
    .line 699
    const-string v0, "ACTIVETIME"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inactive duration ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    const-string v0, "ACTIVETIME"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "inactive timeout ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->updateCurrentMode()V

    .line 703
    return-void
.end method
