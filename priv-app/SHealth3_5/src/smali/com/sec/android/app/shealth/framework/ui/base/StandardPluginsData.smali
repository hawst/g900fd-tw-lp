.class public Lcom/sec/android/app/shealth/framework/ui/base/StandardPluginsData;
.super Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/StandardPluginsData;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public addToRegistryDB()V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/StandardPluginsData;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->addAppRegistryData(Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;Landroid/content/Context;)Landroid/net/Uri;

    goto :goto_0

    :cond_0
    return-void
.end method
