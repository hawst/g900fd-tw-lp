.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;
.super Ljava/lang/Object;
.source "ExerciseSContextManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$2;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final T_DEVICE_FEATURE_VALUE:I = 0x8

.field static mCurrentExerciseType:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;


# instance fields
.field mContext:Landroid/content/Context;

.field mGender:I

.field mHeight:F

.field mIsAvalableService:Z

.field mSContextListener:Landroid/hardware/scontext/SContextListener;

.field mSContextManager:Landroid/hardware/scontext/SContextManager;

.field mWeight:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x2

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 29
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mIsAvalableService:Z

    .line 36
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mContext:Landroid/content/Context;

    .line 38
    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 45
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v2, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v1

    .line 47
    .local v1, "version":I
    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 48
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "scontext"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/scontext/SContextManager;

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v2, :cond_0

    .line 51
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    invoke-virtual {v2, v5}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mIsAvalableService:Z

    .line 52
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FeatureLevel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    invoke-virtual {v4, v5}, Landroid/hardware/scontext/SContextManager;->getFeatureLevel(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Manager: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " service: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mIsAvalableService:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    return-void
.end method


# virtual methods
.method checkCurrentProfileAndUpdate()V
    .locals 6

    .prologue
    .line 111
    sget-object v4, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->TAG:Ljava/lang/String;

    const-string v5, "checkCurrentProfileAndUpdate()"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    new-instance v2, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 113
    .local v2, "shProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v0

    .line 114
    .local v0, "gender":I
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v1

    .line 115
    .local v1, "height":F
    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    .line 117
    .local v3, "weight":F
    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mGender:I

    if-ne v0, v4, :cond_0

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mHeight:F

    cmpl-float v4, v1, v4

    if-nez v4, :cond_0

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mWeight:F

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_1

    .line 118
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->deInitSContextManager()V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->initSContextManager()V

    .line 121
    :cond_1
    return-void
.end method

.method public deInitSContextManager()V
    .locals 3

    .prologue
    .line 103
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->TAG:Ljava/lang/String;

    const-string v1, "deInitSContextManager()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mIsAvalableService:Z

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 106
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->EXERCISE_TYPE_NONE:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mCurrentExerciseType:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    .line 108
    :cond_0
    return-void
.end method

.method public initSContextManager()V
    .locals 9

    .prologue
    const/4 v2, 0x2

    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->TAG:Ljava/lang/String;

    const-string v1, "initSContextManager()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mIsAvalableService:Z

    if-eqz v0, :cond_0

    .line 92
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->TAG:Ljava/lang/String;

    const-string v1, "Update the ShealthProfile"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    new-instance v8, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mContext:Landroid/content/Context;

    invoke-direct {v8, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 94
    .local v8, "shProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mGender:I

    .line 95
    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mHeight:F

    .line 96
    invoke-virtual {v8}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mWeight:F

    .line 97
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mGender:I

    const v1, 0x2e635

    if-ne v0, v1, :cond_1

    const/4 v3, 0x1

    .line 98
    .local v3, "scontextGender":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mHeight:F

    float-to-double v4, v4

    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mWeight:F

    float-to-double v6, v6

    invoke-virtual/range {v0 .. v7}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;IIDD)Z

    .line 100
    .end local v3    # "scontextGender":I
    .end local v8    # "shProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_0
    return-void

    .restart local v8    # "shProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    :cond_1
    move v3, v2

    .line 97
    goto :goto_0
.end method

.method public setCurrentExerciseStatus(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;Z)V
    .locals 4
    .param p1, "type"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;
    .param p2, "skipCheckingProfile"    # Z

    .prologue
    const/4 v3, 0x2

    .line 60
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mCurrentExerciseType:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mIsAvalableService:Z

    if-nez v0, :cond_1

    .line 87
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 63
    :cond_1
    if-nez p2, :cond_2

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->checkCurrentProfileAndUpdate()V

    .line 66
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setCurrentExerciseStatus() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    sput-object p1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mCurrentExerciseType:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$2;->$SwitchMap$com$sec$android$app$shealth$plugins$exercisepro$utils$ExerciseSContextManager$ExerciseType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager$ExerciseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 71
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2}, Landroid/hardware/scontext/SContextManager;->changeParameters(Landroid/hardware/scontext/SContextListener;II)Z

    goto :goto_0

    .line 75
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/hardware/scontext/SContextManager;->changeParameters(Landroid/hardware/scontext/SContextListener;II)Z

    goto :goto_0

    .line 83
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseSContextManager;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v3, v3}, Landroid/hardware/scontext/SContextManager;->changeParameters(Landroid/hardware/scontext/SContextListener;II)Z

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
