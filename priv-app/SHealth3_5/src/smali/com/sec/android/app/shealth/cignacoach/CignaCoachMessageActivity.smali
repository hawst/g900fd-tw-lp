.class public Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "CignaCoachMessageActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$2;,
        Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field private mCignaCoachMessageGaugeView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;

.field private mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

.field private mReceiveBadgeIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiveScore:I

.field private mToDoStartBadgeActivity:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mToDoStartBadgeActivity:Z

    .line 375
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;Ljava/util/Hashtable;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;
    .param p1, "x1"    # Ljava/util/Hashtable;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->updateScore(Ljava/util/Hashtable;)V

    return-void
.end method

.method private categoryMessageView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 5
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 403
    const v3, 0x7f0800ff

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 404
    .local v1, "categoryTextViewRed":Landroid/widget/TextView;
    const v3, 0x7f080105

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 405
    .local v2, "categoryTextViewYellow":Landroid/widget/TextView;
    const v3, 0x7f08010b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 408
    .local v0, "categoryTextViewGreen":Landroid/widget/TextView;
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$2;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 437
    :goto_0
    return-void

    .line 410
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090311

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090316

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 412
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09031b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 415
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090312

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090317

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09031c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 420
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090313

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090318

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09031d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 425
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090314

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 426
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090319

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09031e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 430
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090315

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09031a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09031f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getCategoryMsg()Ljava/lang/String;
    .locals 6

    .prologue
    .line 95
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->convertCategoryTypeToSpecific(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    move-result-object v3

    .line 98
    .local v3, "messageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    const/4 v0, 0x0

    .line 100
    .local v0, "categoryMsg":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 101
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCoachCategoryMessage(Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v1

    .line 103
    .local v1, "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    if-eqz v1, :cond_1

    .line 107
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageDescription()Ljava/lang/String;

    move-result-object v0

    .line 116
    .end local v1    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_0
    :goto_0
    return-object v0

    .line 109
    .restart local v1    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->TAG:Ljava/lang/String;

    const-string v5, "coach msg list is null"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    .end local v1    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    :catch_0
    move-exception v2

    .line 114
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getCategoryTypeReqest(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .locals 2
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 440
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$2;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 452
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    :goto_0
    return-object v0

    .line 442
    :pswitch_0
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 444
    :pswitch_1
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 446
    :pswitch_2
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 448
    :pswitch_3
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 450
    :pswitch_4
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    goto :goto_0

    .line 440
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V
    .locals 3
    .param p1, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 311
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$GetScoreAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 312
    return-void
.end method

.method private getScoreUpdateStringId(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I
    .locals 3
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    const v0, 0x7f090c74

    .line 457
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$2;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 469
    :goto_0
    :pswitch_0
    return v0

    .line 461
    :pswitch_1
    const v0, 0x7f090c75

    goto :goto_0

    .line 463
    :pswitch_2
    const v0, 0x7f090c71

    goto :goto_0

    .line 465
    :pswitch_3
    const v0, 0x7f090c76

    goto :goto_0

    .line 467
    :pswitch_4
    const v0, 0x7f090c77

    goto :goto_0

    .line 457
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getTitleResId(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I
    .locals 2
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 221
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$2;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 235
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 223
    :pswitch_0
    const v0, 0x7f090324

    goto :goto_0

    .line 225
    :pswitch_1
    const v0, 0x7f090325

    goto :goto_0

    .line 227
    :pswitch_2
    const v0, 0x7f090326

    goto :goto_0

    .line 229
    :pswitch_3
    const v0, 0x7f090327

    goto :goto_0

    .line 231
    :pswitch_4
    const v0, 0x7f090328

    goto :goto_0

    .line 221
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setCategorySpecificMessage(I)V
    .locals 4
    .param p1, "score"    # I

    .prologue
    const/4 v3, 0x0

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v1, p1, v2, v3, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setCurrentScoreInfo(ILcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const v1, 0x7f0800fa

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 91
    .local v0, "scoreReassessTextView":Landroid/widget/TextView;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getCategoryMsg()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-void
.end method

.method private startAssessmentActivity(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 5
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 244
    const/4 v2, 0x0

    .line 245
    .local v2, "requestCode":I
    const/4 v0, 0x0

    .line 247
    .local v0, "category":Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$2;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 272
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 273
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "intent_lifestyle_category"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 274
    const-string v3, "EXTRA_NAME_FROM_COACH_MESSAGE"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 275
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 276
    return-void

    .line 249
    .end local v1    # "intent":Landroid/content/Intent;
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 250
    const/16 v2, 0x64

    .line 251
    goto :goto_0

    .line 253
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 254
    const/16 v2, 0xc8

    .line 255
    goto :goto_0

    .line 257
    :pswitch_2
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 258
    const/16 v2, 0x12c

    .line 259
    goto :goto_0

    .line 261
    :pswitch_3
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 262
    const/16 v2, 0x190

    .line 263
    goto :goto_0

    .line 265
    :pswitch_4
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    .line 266
    const/16 v2, 0x1f4

    .line 267
    goto :goto_0

    .line 247
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private startBadgeActivity(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 362
    .local p1, "badgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 363
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v2, 0x115c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 364
    const-string v1, "EXTRA_NAME_NEED_UPATE_MAIN_SCORE_ANIMATION"

    const/16 v2, 0x56ce

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 365
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 366
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->startActivity(Landroid/content/Intent;)V

    .line 367
    return-void
.end method

.method private updateGauge(I)V
    .locals 1
    .param p1, "score"    # I

    .prologue
    .line 370
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCignaCoachMessageGaugeView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCignaCoachMessageGaugeView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;->setScore(I)V

    .line 373
    :cond_0
    return-void
.end method

.method private updateLifestyleScoreView()V
    .locals 5

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EXTRA_NAME_SCORE"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveScore:I

    .line 73
    const v2, 0x7f0800f6

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    .line 74
    const v2, 0x7f0800f8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCignaCoachMessageGaugeView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaCoachMessageGaugeView;

    .line 75
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveScore:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->setCategorySpecificMessage(I)V

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getTitleResId(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->setLifestyleTitleTxt(I)V

    .line 78
    const v2, 0x7f0801df

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 79
    .local v0, "cignaScoreReassessBtnLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    const v2, 0x7f0801e1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 81
    .local v1, "cignaScoreSetGoalBtnLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveScore:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->updateGauge(I)V

    .line 84
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->categoryMessageView(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 85
    return-void
.end method

.method private updateScore(Ljava/util/Hashtable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 316
    .local p1, "categoryInfo":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 318
    .local v0, "ect":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 319
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 320
    .local v2, "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {p1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 322
    .local v1, "i":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    if-ne v2, v3, :cond_0

    .line 324
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->setCategorySpecificMessage(I)V

    .line 325
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->updateGauge(I)V

    .line 330
    .end local v1    # "i":Ljava/lang/Integer;
    .end local v2    # "key":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_1
    return-void
.end method


# virtual methods
.method public closeScreen()V
    .locals 3

    .prologue
    .line 352
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveScore:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getCurrentScore()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 353
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 354
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "intent_category_type"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 355
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->setResult(ILandroid/content/Intent;)V

    .line 358
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->closeScreen()V

    .line 359
    return-void
.end method

.method protected customizeActionBar()V
    .locals 7

    .prologue
    const v6, 0x7f090033

    .line 161
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 163
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;)V

    .line 185
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "intent_category_type"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 186
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$2;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v4}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 203
    const/4 v2, -0x1

    .line 206
    .local v2, "title_id":I
    :goto_0
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090351

    const v5, 0x7f090350

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(II)V

    .line 213
    :goto_1
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207cf

    invoke-direct {v1, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 214
    .local v1, "shareViaButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f020022

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setBackgroundResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 215
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 216
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v5, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 218
    return-void

    .line 188
    .end local v1    # "shareViaButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    .end local v2    # "title_id":I
    :pswitch_0
    const v2, 0x7f090c85

    .line 189
    .restart local v2    # "title_id":I
    goto :goto_0

    .line 191
    .end local v2    # "title_id":I
    :pswitch_1
    const v2, 0x7f090c86

    .line 192
    .restart local v2    # "title_id":I
    goto :goto_0

    .line 194
    .end local v2    # "title_id":I
    :pswitch_2
    const v2, 0x7f09017e

    .line 195
    .restart local v2    # "title_id":I
    goto :goto_0

    .line 197
    .end local v2    # "title_id":I
    :pswitch_3
    const v2, 0x7f090c87

    .line 198
    .restart local v2    # "title_id":I
    goto :goto_0

    .line 200
    .end local v2    # "title_id":I
    :pswitch_4
    const v2, 0x7f090c5f

    .line 201
    .restart local v2    # "title_id":I
    goto :goto_0

    .line 210
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_1

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 292
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 294
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 296
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getScoreUpdateStringId(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getCategoryTypeReqest(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)V

    .line 300
    if-eqz p3, :cond_0

    .line 301
    const-string v0, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mToDoStartBadgeActivity:Z

    .line 307
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 335
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveScore:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCignaLifestyleScoreView:Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaLifestyleScoreView;->getCurrentScore()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 336
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 337
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "intent_category_type"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 338
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->setResult(ILandroid/content/Intent;)V

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->finish()V

    .line 344
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 343
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 120
    const/4 v0, 0x0

    .line 122
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 157
    :goto_0
    :pswitch_0
    return-void

    .line 126
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->startAssessmentActivity(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    goto :goto_0

    .line 131
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedGoalsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 132
    .restart local v0    # "intent":Landroid/content/Intent;
    const/4 v1, -0x1

    .line 133
    .local v1, "suggestedGoalCategory":I
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity$2;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mCategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 152
    :goto_1
    const-string v2, "EXTRA_NAME_SUGGESTED_GOAL_CATEGORY"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 135
    :pswitch_3
    const/16 v1, 0x64

    .line 136
    goto :goto_1

    .line 138
    :pswitch_4
    const/16 v1, 0xc8

    .line 139
    goto :goto_1

    .line 141
    :pswitch_5
    const/16 v1, 0x12c

    .line 142
    goto :goto_1

    .line 144
    :pswitch_6
    const/16 v1, 0x190

    .line 145
    goto :goto_1

    .line 147
    :pswitch_7
    const/16 v1, 0x1f4

    .line 148
    goto :goto_1

    .line 122
    :pswitch_data_0
    .packed-switch 0x7f0801df
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 133
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->setContentView(I)V

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->updateLifestyleScoreView()V

    .line 67
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 280
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 282
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mToDoStartBadgeActivity:Z

    if-eqz v0, :cond_0

    .line 283
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->TAG:Ljava/lang/String;

    const-string v1, "Beaucase there is received badge, started BadgeActivity."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mToDoStartBadgeActivity:Z

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->mReceiveBadgeIds:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->startBadgeActivity(Ljava/util/ArrayList;)V

    .line 287
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 8

    .prologue
    const v7, 0x7f080100

    const v6, 0x7f0800fe

    const v5, 0x7f0800fd

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 475
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onStart()V

    .line 477
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 478
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 480
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 481
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 482
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 483
    const v1, 0x7f080101

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 484
    const v1, 0x7f080103

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 485
    const v1, 0x7f080104

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 486
    const v1, 0x7f080106

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 487
    const v1, 0x7f080107

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 488
    const v1, 0x7f080109

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 489
    const v1, 0x7f08010a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 490
    const v1, 0x7f08010c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 491
    const v1, 0x7f08010d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 508
    :goto_0
    return-void

    .line 495
    :cond_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 496
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 497
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 498
    const v1, 0x7f080101

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 499
    const v1, 0x7f080103

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 500
    const v1, 0x7f080104

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 501
    const v1, 0x7f080106

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 502
    const v1, 0x7f080107

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 503
    const v1, 0x7f080109

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 504
    const v1, 0x7f08010a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 505
    const v1, 0x7f08010c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 506
    const v1, 0x7f08010d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/CignaCoachMessageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
