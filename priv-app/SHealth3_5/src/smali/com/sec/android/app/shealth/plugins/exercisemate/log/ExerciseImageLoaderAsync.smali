.class public Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;
.super Landroid/os/AsyncTask;
.source "ExerciseImageLoaderAsync.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private ExerciseId:J

.field private mContext:Landroid/content/Context;

.field private mIloadImageReturn:Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

.field private mPhotoDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field private maskHeight:I

.field private maskWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;)V
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "imageReturn"    # Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 33
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    .line 34
    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskHeight:I

    .line 38
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mIloadImageReturn:Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mPhotoDatas:Ljava/util/ArrayList;

    .line 40
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mContext:Landroid/content/Context;

    .line 41
    return-void
.end method

.method private BitmapColorChange(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "Image"    # Landroid/graphics/Bitmap;
    .param p2, "ChangeColor"    # I

    .prologue
    const/4 v2, 0x0

    .line 245
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 246
    .local v3, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 247
    .local v7, "height":I
    mul-int v0, v3, v7

    new-array v1, v0, [I

    .local v1, "maskPixels":[I
    move-object v0, p1

    move v4, v2

    move v5, v2

    move v6, v3

    .line 248
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 249
    const/4 v9, 0x0

    .local v9, "x":I
    :goto_0
    array-length v0, v1

    if-ge v9, v0, :cond_1

    .line 250
    aget v0, v1, v9

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    const/16 v2, 0xff

    if-ne v0, v2, :cond_0

    aget v0, v1, v9

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v0

    const/16 v2, 0xe6

    if-ge v0, v2, :cond_0

    .line 251
    aput p2, v1, v9

    .line 249
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    invoke-static {v1, v3, v7, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 255
    .local v8, "Image2":Landroid/graphics/Bitmap;
    return-object v8
.end method

.method private LoaderMask(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "size"    # I

    .prologue
    .line 182
    const/4 v1, 0x0

    .line 183
    .local v1, "MaskId":I
    packed-switch p1, :pswitch_data_0

    .line 197
    const v1, 0x7f020693

    .line 200
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 201
    .local v0, "MaskD":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    .line 202
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskHeight:I

    .line 203
    return-object v0

    .line 185
    .end local v0    # "MaskD":Landroid/graphics/Bitmap;
    :pswitch_0
    const v1, 0x7f020690

    .line 186
    goto :goto_0

    .line 188
    :pswitch_1
    const v1, 0x7f020691

    .line 189
    goto :goto_0

    .line 191
    :pswitch_2
    const v1, 0x7f020692

    .line 192
    goto :goto_0

    .line 194
    :pswitch_3
    const v1, 0x7f020693

    .line 195
    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 6
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 225
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 226
    .local v2, "height":I
    iget v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 227
    .local v4, "width":I
    const/4 v3, 0x1

    .line 229
    .local v3, "inSampleSize":I
    if-gt v2, p2, :cond_0

    if-le v4, p1, :cond_1

    .line 231
    :cond_0
    div-int/lit8 v0, v2, 0x2

    .line 232
    .local v0, "halfHeight":I
    div-int/lit8 v1, v4, 0x2

    .line 236
    .local v1, "halfWidth":I
    :goto_0
    div-int v5, v0, v3

    if-le v5, p2, :cond_2

    div-int v5, v1, v3

    if-le v5, p1, :cond_2

    .line 237
    mul-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 240
    .end local v0    # "halfHeight":I
    .end local v1    # "halfWidth":I
    :cond_1
    const/4 v3, 0x4

    .line 241
    :cond_2
    return v3
.end method


# virtual methods
.method public LoadBitmap(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "PhotoDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x4

    if-le v3, v4, :cond_1

    const/16 v18, 0x4

    .line 91
    .local v18, "size":I
    :goto_0
    new-instance v13, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v13}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 92
    .local v13, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v13, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 93
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskHeight:I

    invoke-static {v13, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v3

    iput v3, v13, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 94
    const/4 v3, 0x0

    iput-boolean v3, v13, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 95
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskHeight:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 96
    .local v15, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v16, Landroid/graphics/Canvas;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 97
    .local v16, "resultCanvas":Landroid/graphics/Canvas;
    const/4 v12, 0x0

    .local v12, "indexCase":I
    :goto_1
    move/from16 v0, v18

    if-ge v12, v0, :cond_4

    .line 98
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 99
    .local v14, "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    if-nez v14, :cond_2

    .line 97
    :cond_0
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 90
    .end local v12    # "indexCase":I
    .end local v13    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v14    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .end local v15    # "resultBitmap":Landroid/graphics/Bitmap;
    .end local v16    # "resultCanvas":Landroid/graphics/Canvas;
    .end local v18    # "size":I
    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v18

    goto :goto_0

    .line 101
    .restart local v12    # "indexCase":I
    .restart local v13    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v14    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .restart local v15    # "resultBitmap":Landroid/graphics/Bitmap;
    .restart local v16    # "resultCanvas":Landroid/graphics/Canvas;
    .restart local v18    # "size":I
    :cond_2
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 102
    .local v2, "LoadImage":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 104
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-le v3, v5, :cond_3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    :goto_3
    int-to-float v3, v3

    div-float v17, v4, v3

    .line 107
    .local v17, "scale":F
    :try_start_0
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 108
    .local v7, "matrix":Landroid/graphics/Matrix;
    new-instance v11, Landroid/media/ExifInterface;

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v11, v3}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 109
    .local v11, "exif":Landroid/media/ExifInterface;
    move/from16 v0, v17

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 110
    const-string v3, "Orientation"

    const/4 v4, 0x1

    invoke-virtual {v11, v3, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v9

    .line 111
    .local v9, "ExifOri":I
    packed-switch v9, :pswitch_data_0

    .line 123
    :goto_4
    :pswitch_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 129
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v9    # "ExifOri":I
    .end local v11    # "exif":Landroid/media/ExifInterface;
    :goto_5
    packed-switch v12, :pswitch_data_1

    goto :goto_2

    .line 131
    :pswitch_1
    packed-switch v18, :pswitch_data_2

    goto :goto_2

    .line 133
    :pswitch_2
    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 104
    .end local v17    # "scale":F
    :cond_3
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    goto :goto_3

    .line 113
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v9    # "ExifOri":I
    .restart local v11    # "exif":Landroid/media/ExifInterface;
    .restart local v17    # "scale":F
    :pswitch_3
    const/high16 v3, 0x42b40000    # 90.0f

    :try_start_1
    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 125
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v9    # "ExifOri":I
    .end local v11    # "exif":Landroid/media/ExifInterface;
    :catch_0
    move-exception v10

    .line 126
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 116
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v7    # "matrix":Landroid/graphics/Matrix;
    .restart local v9    # "ExifOri":I
    .restart local v11    # "exif":Landroid/media/ExifInterface;
    :pswitch_4
    const/high16 v3, 0x43340000    # 180.0f

    :try_start_2
    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z

    goto :goto_4

    .line 120
    :pswitch_5
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_4

    .line 136
    .end local v7    # "matrix":Landroid/graphics/Matrix;
    .end local v9    # "ExifOri":I
    .end local v11    # "exif":Landroid/media/ExifInterface;
    :pswitch_6
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 139
    :pswitch_7
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 142
    :pswitch_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    neg-int v3, v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 148
    :pswitch_9
    packed-switch v18, :pswitch_data_3

    goto/16 :goto_2

    .line 151
    :pswitch_a
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 154
    :pswitch_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    neg-int v3, v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 157
    :pswitch_c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 163
    :pswitch_d
    packed-switch v18, :pswitch_data_4

    goto/16 :goto_2

    .line 165
    :pswitch_e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 168
    :pswitch_f
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 174
    :pswitch_10
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskHeight:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 178
    .end local v2    # "LoadImage":Landroid/graphics/Bitmap;
    .end local v14    # "photoData":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .end local v17    # "scale":F
    :cond_4
    return-object v15

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch

    .line 129
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_9
        :pswitch_d
        :pswitch_10
    .end packed-switch

    .line 131
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 148
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 163
    :pswitch_data_4
    .packed-switch 0x3
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 10
    .param p1, "arg0"    # [Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 45
    aget-object v6, p1, v8

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->ExerciseId:J

    .line 46
    iget-wide v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->ExerciseId:J

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->updatePhotoData(J)V

    .line 47
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mPhotoDatas:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 48
    :cond_0
    const/4 v6, 0x0

    .line 70
    :goto_0
    return-object v6

    .line 51
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->LoaderMask(I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 52
    .local v1, "Mask":Landroid/graphics/Bitmap;
    invoke-direct {p0, v1, v8}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->BitmapColorChange(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 53
    .local v2, "Mask2":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->LoadBitmap(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 55
    .local v0, "LoadImage":Landroid/graphics/Bitmap;
    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskWidth:I

    iget v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->maskHeight:I

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 56
    .local v5, "resultBitmap":Landroid/graphics/Bitmap;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 57
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 58
    .local v4, "paint":Landroid/graphics/Paint;
    new-instance v6, Landroid/graphics/PorterDuffXfermode;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v7}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 59
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 61
    .local v3, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    .line 62
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v3, v0, v9, v9, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 63
    invoke-virtual {v3, v1, v9, v9, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 64
    invoke-virtual {v3}, Landroid/graphics/Canvas;->restore()V

    .line 65
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v3, v2, v9, v9, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 67
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 68
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 69
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 70
    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->doInBackground([Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Object;

    .prologue
    .line 208
    if-eqz p1, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mIloadImageReturn:Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->ExerciseId:J

    check-cast p1, Landroid/graphics/drawable/Drawable;

    .end local p1    # "result":Ljava/lang/Object;
    invoke-interface {v0, v1, v2, p1}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;->onReturnLoadImage(JLandroid/graphics/drawable/Drawable;)V

    .line 213
    :goto_0
    return-void

    .line 211
    .restart local p1    # "result":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mIloadImageReturn:Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->ExerciseId:J

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisemate/log/IloadImageReturn;->onNotReturnLoadImage(J)V

    goto :goto_0
.end method

.method public updatePhotoData(J)V
    .locals 7
    .param p1, "rowId"    # J

    .prologue
    .line 74
    const/4 v6, 0x0

    .line 77
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "exercise__id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 81
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExercisePhoto;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 82
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/common/utils/ExercisePhotoUtils;->getExercisePhotoDataList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisemate/log/ExerciseImageLoaderAsync;->mPhotoDatas:Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    if-eqz v6, :cond_0

    .line 85
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 87
    :cond_0
    return-void

    .line 84
    .end local v3    # "selection":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 85
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method
