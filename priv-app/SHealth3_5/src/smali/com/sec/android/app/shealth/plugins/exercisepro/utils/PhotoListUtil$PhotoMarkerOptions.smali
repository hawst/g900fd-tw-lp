.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;
.super Ljava/lang/Object;
.source "PhotoListUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoMarkerOptions"
.end annotation


# instance fields
.field private aMarkerOptionis:Lcom/amap/api/maps2d/model/MarkerOptions;

.field private mMarkerOptions:Lcom/google/android/gms/maps/model/MarkerOptions;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->mMarkerOptions:Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 267
    new-instance v0, Lcom/amap/api/maps2d/model/MarkerOptions;

    invoke-direct {v0}, Lcom/amap/api/maps2d/model/MarkerOptions;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->aMarkerOptionis:Lcom/amap/api/maps2d/model/MarkerOptions;

    .line 269
    return-void
.end method


# virtual methods
.method public getMarkerOption()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 272
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mIsAmap:Z

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->aMarkerOptionis:Lcom/amap/api/maps2d/model/MarkerOptions;

    .line 275
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->mMarkerOptions:Lcom/google/android/gms/maps/model/MarkerOptions;

    goto :goto_0
.end method

.method public setMarkerOptions(Ljava/lang/Object;)V
    .locals 1
    .param p1, "marker"    # Ljava/lang/Object;

    .prologue
    .line 279
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil;->mIsAmap:Z

    if-eqz v0, :cond_0

    .line 280
    check-cast p1, Lcom/amap/api/maps2d/model/MarkerOptions;

    .end local p1    # "marker":Ljava/lang/Object;
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->aMarkerOptionis:Lcom/amap/api/maps2d/model/MarkerOptions;

    .line 283
    :goto_0
    return-void

    .line 282
    .restart local p1    # "marker":Ljava/lang/Object;
    :cond_0
    check-cast p1, Lcom/google/android/gms/maps/model/MarkerOptions;

    .end local p1    # "marker":Ljava/lang/Object;
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$PhotoMarkerOptions;->mMarkerOptions:Lcom/google/android/gms/maps/model/MarkerOptions;

    goto :goto_0
.end method
