.class public Lcom/sec/android/app/shealth/logutils/graph/data/GraphInformationItemFactory;
.super Ljava/lang/Object;
.source "GraphInformationItemFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/logutils/graph/data/GraphInformationItemFactory$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method public static getGraphInformationItem(Landroid/content/Context;ILcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "seriesId"    # I
    .param p2, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 38
    sget-object v1, Lcom/sec/android/app/shealth/logutils/graph/data/GraphInformationItemFactory$1;->$SwitchMap$com$sec$android$app$shealth$framework$ui$graph$PeriodH:[I

    invoke-virtual {p2}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 44
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;-><init>(Landroid/content/Context;I)V

    .line 47
    .local v0, "graphInformationItem":Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    :goto_0
    return-object v0

    .line 41
    .end local v0    # "graphInformationItem":Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/shealth/logutils/graph/data/AverageGraphInformationItem;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/data/AverageGraphInformationItem;-><init>(Landroid/content/Context;I)V

    .line 42
    .restart local v0    # "graphInformationItem":Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
