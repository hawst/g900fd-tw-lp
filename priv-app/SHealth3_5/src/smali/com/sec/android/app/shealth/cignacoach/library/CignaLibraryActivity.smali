.class public Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "CignaLibraryActivity.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mLibraryFragment:Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 28
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c59

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 30
    return-void
.end method

.method public getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v0, 0x7f03005c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->setContentView(I)V

    .line 23
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->showLibraryFragment()V

    .line 24
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 70
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onResume()V

    .line 37
    const v0, 0x7f0801cb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 38
    const v0, 0x7f0801cc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 40
    return-void
.end method

.method public showLibraryFragment()V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->mLibraryFragment:Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->mLibraryFragment:Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0801cb

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/CignaLibraryActivity;->mLibraryFragment:Lcom/sec/android/app/shealth/cignacoach/library/LibraryHomeFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 49
    return-void
.end method
