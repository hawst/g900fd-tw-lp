.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;)V
    .locals 0

    .prologue
    .line 1847
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 1852
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v1, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v0, 0x7f08008a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressMsg:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4402(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1853
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressMsg:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget v2, v2, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->val$popup_msg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1855
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v1, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v0, 0x7f08008b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$102(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/ProgressBar;)Landroid/widget/ProgressBar;

    .line 1856
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v1, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v0, 0x7f08008d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgress:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4502(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1857
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgress:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$4500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1858
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v1, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const v0, 0x7f08008c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mTextPercentage:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 1860
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1861
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26$1;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$26;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1863
    return-void
.end method
