.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;
.super Ljava/lang/Object;
.source "ImagePager.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private scrollCallBackCalled:Z

.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V
    .locals 1

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->scrollCallBackCalled:Z

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->scrollCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 118
    if-nez p1, :cond_0

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->scrollCallBackCalled:Z

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 125
    :cond_1
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->scrollCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 130
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->scrollCallBackCalled:Z

    if-nez v0, :cond_0

    const v0, 0x3ca3d70a    # 0.02f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->scrollCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;->onPageScrolled()V

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->scrollCallBackCalled:Z

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 139
    :cond_1
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->selectPage(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 113
    :cond_0
    return-void
.end method
