.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$7;
.super Ljava/lang/Object;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0

    .prologue
    .line 1102
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$7;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 1106
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1108
    check-cast v0, Landroid/widget/TextView;

    .line 1109
    .local v0, "textView":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$7;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$7;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMaxAcceptableCaloriesValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$2000(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)F

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->prepareTextForOutOfRangePouup(Landroid/content/Context;F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1111
    .end local v0    # "textView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method
