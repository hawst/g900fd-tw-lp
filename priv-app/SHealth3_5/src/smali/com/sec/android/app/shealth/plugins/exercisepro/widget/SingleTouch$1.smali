.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;
.super Landroid/view/TouchDelegate;
.source "SingleTouch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->getTouchDelegate(Landroid/view/View;)Landroid/view/TouchDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

.field final synthetic val$id:I

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;Landroid/graphics/Rect;Landroid/view/View;Landroid/view/View;I)V
    .locals 0
    .param p2, "x0"    # Landroid/graphics/Rect;
    .param p3, "x1"    # Landroid/view/View;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$view:Landroid/view/View;

    iput p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$id:I

    invoke-direct {p0, p2, p3}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, -0x1

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 27
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$view:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 28
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->lastId:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;)I

    move-result v1

    if-eq v1, v4, :cond_0

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$id:I

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->lastId:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 29
    const/4 v1, 0x1

    .line 43
    :goto_0
    return v1

    .line 31
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 43
    :goto_1
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/TouchDelegate;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 33
    :pswitch_1
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "view touched: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$view:Landroid/view/View;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    iget v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$id:I

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->lastId:I
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;I)I

    goto :goto_1

    .line 39
    :pswitch_2
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "view untouched: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$view:Landroid/view/View;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->val$id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->lastId:I
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;I)I

    goto :goto_1

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
