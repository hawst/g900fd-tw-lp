.class public Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;
.super Ljava/lang/Object;
.source "WalkingMateStatisticServerTool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BasicUser"
.end annotation


# instance fields
.field private final accountId:Ljava/lang/String;

.field private final imgUrl:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final ranking:I

.field private final totalDistance:Ljava/lang/String;

.field private userInfo:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 1
    .param p1, "object"    # Lorg/json/JSONObject;
    .param p2, "userToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 570
    const-string/jumbo v0, "ranking"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->ranking:I

    .line 571
    const-string v0, "accountId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->accountId:Ljava/lang/String;

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->accountId:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool;->getUserInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->userInfo:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;

    .line 573
    const-string/jumbo v0, "totalDistance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->totalDistance:Ljava/lang/String;

    .line 574
    const-string/jumbo v0, "name"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->name:Ljava/lang/String;

    .line 575
    const-string v0, "imgurl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->imgUrl:Ljava/lang/String;

    .line 576
    return-void
.end method


# virtual methods
.method public getAge()Ljava/lang/String;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->userInfo:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->age:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()Ljava/lang/String;
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->userInfo:Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$UserInfo;->gender:Ljava/lang/String;

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->imgUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalDistance()J
    .locals 2

    .prologue
    .line 579
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->totalDistance:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 588
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateStatisticServerTool$BasicUser;->accountId:Ljava/lang/String;

    return-object v0
.end method
