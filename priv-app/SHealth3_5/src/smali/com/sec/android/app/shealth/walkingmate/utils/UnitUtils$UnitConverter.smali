.class public Lcom/sec/android/app/shealth/walkingmate/utils/UnitUtils$UnitConverter;
.super Ljava/lang/Object;
.source "UnitUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/utils/UnitUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UnitConverter"
.end annotation


# static fields
.field private static final GLUCOSE_CONVERT_VALUE:F = 18.0182f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static convertKgIntoMiles(D)D
    .locals 2
    .param p0, "valueInKg"    # D

    .prologue
    .line 35
    const-wide v0, 0x3fe3e245d68a2112L    # 0.621371192

    mul-double/2addr v0, p0

    return-wide v0
.end method

.method public static convertKgIntoMiles(F)F
    .locals 1
    .param p0, "valueInKg"    # F

    .prologue
    .line 31
    const v0, 0x3f1f122f

    mul-float/2addr v0, p0

    return v0
.end method

.method public static convertMetersToMiles(I)F
    .locals 2
    .param p0, "valueInMeters"    # I

    .prologue
    .line 47
    int-to-float v0, p0

    const v1, 0x3a22e36f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static convertMgIntoMmol(F)F
    .locals 1
    .param p0, "valueInMg"    # F

    .prologue
    .line 39
    const v0, 0x41902546    # 18.0182f

    div-float v0, p0, v0

    return v0
.end method

.method public static convertMilesToMeters(F)I
    .locals 1
    .param p0, "valueInMiles"    # F

    .prologue
    .line 51
    const v0, 0x3a22e36f

    div-float v0, p0, v0

    float-to-int v0, v0

    return v0
.end method

.method public static convertMmolIntoMg(F)F
    .locals 1
    .param p0, "valueInMmol"    # F

    .prologue
    .line 43
    const v0, 0x41902546    # 18.0182f

    mul-float/2addr v0, p0

    return v0
.end method
