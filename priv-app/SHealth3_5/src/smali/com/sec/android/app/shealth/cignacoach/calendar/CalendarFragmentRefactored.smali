.class public Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
.super Landroid/support/v4/app/Fragment;
.source "CalendarFragmentRefactored.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;,
        Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;
    }
.end annotation


# static fields
.field private static final COLUMNS_IN_DAY_VIEW:I = 0x7

.field private static final COLUMNS_IN_MONTH_AND_YEAR_VIEW:I = 0x4

.field private static final ROWS_IN_DAY_VIEW:I = 0x6

.field private static final ROWS_IN_MONTH_AND_YEAR_VIEW:I = 0x3

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mContext:Landroid/content/Context;

.field private mDaysInfo:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mInputButtonType:Ljava/lang/String;

.field private mPeriodStartTodayButtonMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;",
            ">;"
        }
    .end annotation
.end field

.field private mPeriodType:I

.field private mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

.field private mSelectedTime:J

.field private mStartTime:J

.field private mTime:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 57
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;JZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
    .param p1, "x1"    # J
    .param p3, "x2"    # Z

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->onCalendarDayClicked(JZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;)Ljava/util/TreeMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mDaysInfo:Ljava/util/TreeMap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/util/TreeMap;)Ljava/util/TreeMap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
    .param p1, "x1"    # Ljava/util/TreeMap;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mDaysInfo:Ljava/util/TreeMap;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/util/Map;Ljava/util/TreeMap;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
    .param p1, "x1"    # Ljava/util/Map;
    .param p2, "x2"    # Ljava/util/TreeMap;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->checkIfDataExists(Ljava/util/Map;Ljava/util/TreeMap;)V

    return-void
.end method

.method private checkIfClickAllowed(J)Z
    .locals 3
    .param p1, "check"    # J

    .prologue
    .line 928
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 930
    .local v0, "system":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    cmp-long v1, v1, p1

    if-ltz v1, :cond_0

    .line 931
    const/4 v1, 0x1

    .line 933
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkIfDataExists(Ljava/util/Map;Ljava/util/TreeMap;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;",
            ">;",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 861
    .local p1, "dayButtonMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;>;"
    .local p2, "uDaysInfo":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;>;"
    invoke-virtual {p2}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 864
    invoke-virtual {p2}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 866
    .local v4, "day":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    .line 867
    .local v1, "currentDay":J
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;

    .line 868
    .local v0, "c":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;
    invoke-virtual {p2, v4}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    .line 870
    .local v3, "d":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    if-eqz v0, :cond_0

    .line 872
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->checkIfClickAllowed(J)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 873
    new-instance v6, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, 0x1

    invoke-direct {v6, p0, v7, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/lang/Long;Z)V

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 875
    :cond_1
    if-eqz v3, :cond_0

    .line 880
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 882
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;

    invoke-direct {v7, p0, v3, v0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;)V

    invoke-virtual {v6, v7}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 923
    .end local v0    # "c":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;
    .end local v1    # "currentDay":J
    .end local v3    # "d":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    .end local v4    # "day":Ljava/lang/Long;
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method private disablePeriodSelectedDays(J)V
    .locals 19
    .param p1, "mTime"    # J

    .prologue
    .line 370
    const-wide/32 v6, 0x5265c00

    .line 371
    .local v6, "checkTime":J
    const-wide/16 v2, 0x0

    .line 372
    .local v2, "SelectedPeriod":J
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v15

    sub-long v15, v15, p1

    div-long v4, v15, v6

    .line 374
    .local v4, "checkDay":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v13

    .line 376
    .local v13, "startOfToday":J
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v15

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v11

    .line 378
    .local v11, "selectedTime":J
    const-string v10, ""

    .line 379
    .local v10, "isSunday":Ljava/lang/String;
    const/4 v8, 0x0

    .line 381
    .local v8, "getSundayFlag":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mInputButtonType:Ljava/lang/String;

    const-string v16, "StartDatePicker"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 382
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    int-to-long v15, v9

    cmp-long v15, v15, v4

    if-gtz v15, :cond_3

    .line 383
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v15

    int-to-long v0, v9

    move-wide/from16 v17, v0

    mul-long v17, v17, v6

    sub-long v2, v15, v17

    .line 384
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 387
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    if-eqz v15, :cond_0

    .line 388
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getToSunDay(J)Ljava/lang/String;

    move-result-object v10

    .line 389
    const-string v15, "Sun"

    invoke-virtual {v10, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 390
    cmp-long v15, v13, v11

    if-nez v15, :cond_1

    .line 391
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/16 v16, 0xa

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 382
    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 395
    :cond_1
    if-eqz v8, :cond_2

    .line 396
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/16 v16, 0x9

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_1

    .line 399
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_1

    .line 407
    .end local v9    # "i":I
    :cond_3
    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mStartTime:J

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v15

    sub-long v15, p1, v15

    div-long/2addr v15, v6

    const-wide/16 v17, 0x6

    cmp-long v15, v15, v17

    if-ltz v15, :cond_6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mInputButtonType:Ljava/lang/String;

    const-string v16, "EndDatePicker"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 409
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_2
    int-to-long v15, v9

    cmp-long v15, v15, v4

    if-gtz v15, :cond_6

    .line 410
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v15

    int-to-long v0, v9

    move-wide/from16 v17, v0

    mul-long v17, v17, v6

    sub-long v2, v15, v17

    .line 411
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 413
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    if-eqz v15, :cond_4

    .line 414
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getToSunDay(J)Ljava/lang/String;

    move-result-object v10

    .line 415
    const-string v15, "Sun"

    invoke-virtual {v10, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 416
    if-eqz v8, :cond_5

    .line 417
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/16 v16, 0x9

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 409
    :cond_4
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 420
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_3

    .line 426
    .end local v9    # "i":I
    :cond_6
    return-void
.end method

.method private disableStartDay()V
    .locals 9

    .prologue
    .line 333
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 336
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    .line 338
    .local v4, "startOfToday":J
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 340
    .local v2, "selectedTime":J
    const-string v1, ""

    .line 341
    .local v1, "isSunday":Ljava/lang/String;
    const/4 v0, 0x0

    .line 343
    .local v0, "getSundayFlag":Z
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getToSunDay(J)Ljava/lang/String;

    move-result-object v1

    .line 345
    const-string v6, "Sun"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 347
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    if-eqz v6, :cond_0

    .line 348
    cmp-long v6, v4, v2

    if-nez v6, :cond_1

    .line 349
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    if-eqz v0, :cond_2

    .line 354
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/16 v7, 0x9

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_0

    .line 357
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/4 v7, 0x7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_0
.end method

.method private getDayView(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 22
    .param p1, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 182
    const v2, 0x7f030091

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 183
    .local v10, "content":Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getSundayIndex()I

    move-result v19

    .line 184
    .local v19, "sundayIndex":I
    const v2, 0x7f0800a0

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 186
    .local v12, "daysOfWeekContainer":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v12}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->initDaysOfWeekContainer(ILandroid/widget/LinearLayout;)V

    .line 187
    const v2, 0x7f0802b4

    invoke-virtual {v10, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/LinearLayout;

    .line 190
    .local v20, "tableLayout":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    if-nez v2, :cond_0

    .line 247
    :goto_0
    return-object v10

    .line 193
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v11

    .line 194
    .local v11, "currentDay":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v11, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 195
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->shiftCalendarToStartOfMonth(Ljava/util/Calendar;)V

    .line 196
    invoke-virtual {v11}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 197
    .local v4, "startOfMonth":J
    const/4 v2, 0x2

    invoke-static {v4, v5, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v6

    .line 199
    .local v6, "endOfMonth":J
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v13

    .line 201
    .local v13, "firstDayOfWeek":I
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v13}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->shiftCalendarToFirstDayOfWeek(Ljava/util/Calendar;I)V

    .line 202
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    .line 203
    new-instance v14, Ljava/util/TreeMap;

    invoke-direct {v14}, Ljava/util/TreeMap;-><init>()V

    .line 204
    .local v14, "periodStartToWeekViewMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Landroid/view/View;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v11, v2, v14}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getDayViewAndFillViewMaps(Landroid/widget/LinearLayout;Ljava/util/Calendar;Ljava/util/Map;Ljava/util/Map;)V

    .line 207
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedTime:J

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v15

    .line 210
    .local v15, "startOfSelectedDay":J
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v2

    const-wide/16 v8, 0x0

    cmp-long v2, v2, v8

    if-nez v2, :cond_1

    .line 211
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedTime:J

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setOldSelectedTime(J)V

    .line 214
    :cond_1
    cmp-long v2, v15, v4

    if-ltz v2, :cond_2

    cmp-long v2, v15, v6

    if-gtz v2, :cond_2

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->getOldSelectedTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    if-eqz v2, :cond_2

    .line 221
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 222
    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->checkIfClickAllowed(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/lang/Long;Z)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->periodSelectedDays()V

    .line 229
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v17

    .line 232
    .local v17, "startOfToday":J
    cmp-long v2, v17, v4

    if-ltz v2, :cond_3

    cmp-long v2, v17, v6

    if-gtz v2, :cond_3

    .line 233
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 235
    .local v21, "todayDayButton":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;
    const/4 v2, 0x5

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 236
    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->checkIfClickAllowed(J)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 237
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/lang/Long;Z)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    .end local v21    # "todayDayButton":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;
    :cond_3
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    const/4 v9, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;JJLjava/util/Map;Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$1;)V

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$CalendarDataObtainer;->start()V

    .line 245
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->refreshFocusable(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private getDayViewAndFillViewMaps(Landroid/widget/LinearLayout;Ljava/util/Calendar;Ljava/util/Map;Ljava/util/Map;)V
    .locals 11
    .param p1, "tableLayout"    # Landroid/widget/LinearLayout;
    .param p2, "currentDay"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "Ljava/util/Calendar;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 435
    .local p3, "mPeriodStartTodayButtonMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarButton;>;"
    .local p4, "periodStartToWeekViewMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Landroid/view/View;>;"
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/FocusWorker;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;-><init>()V

    .line 436
    .local v2, "focusWorker":Lcom/sec/android/app/shealth/common/utils/FocusWorker;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    const/4 v8, 0x6

    if-ge v5, v8, :cond_6

    .line 438
    mul-int/lit8 v8, v5, 0x2

    invoke-virtual {p1, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 439
    .local v6, "rowLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {p4, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    const/4 v8, 0x7

    if-ge v4, v8, :cond_5

    .line 444
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    mul-int/lit8 v8, v4, 0x2

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-direct {v1, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;-><init>(Landroid/view/View;)V

    .line 447
    .local v1, "dayButton":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;
    const/4 v8, 0x7

    invoke-virtual {p2, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    const/4 v0, 0x1

    .line 450
    .local v0, "buttonNeedsSundayHighlight":Z
    :goto_2
    const/4 v8, 0x1

    invoke-virtual {p2, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    if-ne v8, v9, :cond_0

    const/4 v8, 0x2

    invoke-virtual {p2, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    if-eq v8, v9, :cond_3

    .line 454
    :cond_0
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setUnderlineState(I)V

    .line 456
    if-eqz v0, :cond_2

    .line 457
    const/4 v8, 0x3

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 462
    :goto_3
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->getView()Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->getAllFocusableChildViews(Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v3

    .line 484
    :goto_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x5

    invoke-virtual {p2, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setText(Ljava/lang/String;)V

    .line 485
    const/4 v8, 0x6

    const/4 v9, 0x1

    invoke-virtual {p2, v8, v9}, Ljava/util/Calendar;->add(II)V

    .line 442
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 447
    .end local v0    # "buttonNeedsSundayHighlight":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 459
    .restart local v0    # "buttonNeedsSundayHighlight":Z
    :cond_2
    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_3

    .line 471
    :cond_3
    if-eqz v0, :cond_4

    .line 472
    const/4 v8, 0x2

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 477
    :goto_5
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->getView()Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 479
    .local v7, "viewGroup":Landroid/view/ViewGroup;
    const/4 v8, 0x6

    invoke-virtual {p2, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    mul-int/lit16 v8, v8, 0x3e8

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setId(I)V

    .line 480
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {p3, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 474
    .end local v7    # "viewGroup":Landroid/view/ViewGroup;
    :cond_4
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_5

    .line 436
    .end local v0    # "buttonNeedsSundayHighlight":Z
    .end local v1    # "dayButton":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 488
    .end local v4    # "i":I
    .end local v6    # "rowLayout":Landroid/widget/LinearLayout;
    :cond_6
    return-void
.end method

.method private getFirstDayOfWeek()I
    .locals 2

    .prologue
    .line 561
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 563
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v1

    return v1
.end method

.method public static getInstance(Landroid/content/Context;JIJLjava/lang/String;JJLjava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # J
    .param p3, "periodType"    # I
    .param p4, "selectedTime"    # J
    .param p6, "dataType"    # Ljava/lang/String;
    .param p7, "startTime"    # J
    .param p9, "endTime"    # J
    .param p11, "inputButtonType"    # Ljava/lang/String;
    .param p12, "timeColumnName"    # Ljava/lang/String;

    .prologue
    .line 81
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;-><init>()V

    .line 82
    .local v0, "fragment":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->setContext(Landroid/content/Context;)V

    .line 83
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v1

    .line 84
    .local v1, "month":Ljava/util/Calendar;
    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 86
    iput-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    .line 87
    iput p3, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodType:I

    .line 88
    iput-wide p7, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mStartTime:J

    .line 89
    iput-wide p4, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedTime:J

    .line 90
    iput-object p11, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mInputButtonType:Ljava/lang/String;

    .line 92
    return-object v0
.end method

.method private getSundayIndex()I
    .locals 6

    .prologue
    const/4 v0, 0x5

    const/4 v1, 0x4

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 543
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v5

    if-ne v5, v4, :cond_1

    .line 544
    const/4 v0, 0x7

    .line 556
    :cond_0
    :goto_0
    return v0

    .line 545
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v5

    if-ne v5, v3, :cond_2

    .line 546
    const/4 v0, 0x6

    goto :goto_0

    .line 547
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v5

    if-eq v5, v2, :cond_0

    .line 549
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v5

    if-ne v5, v1, :cond_3

    move v0, v1

    .line 550
    goto :goto_0

    .line 551
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v1

    if-ne v1, v0, :cond_4

    move v0, v2

    .line 552
    goto :goto_0

    .line 553
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getFirstDayOfWeek()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    move v0, v3

    .line 554
    goto :goto_0

    :cond_5
    move v0, v4

    .line 556
    goto :goto_0
.end method

.method private static getToSunDay(J)Ljava/lang/String;
    .locals 3
    .param p0, "time"    # J

    .prologue
    .line 365
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "E"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 366
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private initDaysOfWeekContainer(ILandroid/widget/LinearLayout;)V
    .locals 7
    .param p1, "sundayIndex"    # I
    .param p2, "daysOfWeekContainer"    # Landroid/widget/LinearLayout;

    .prologue
    const/4 v6, -0x1

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 132
    .local v2, "daysOfWeek":[Ljava/lang/String;
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v6, v6, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 133
    .local v0, "dayLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 136
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 137
    .local v1, "dayText":Landroid/widget/TextView;
    invoke-direct {p0, p1, v3, v1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->setCorrectDayTextColor(IILandroid/widget/TextView;)V

    .line 138
    sub-int v4, v3, p1

    add-int/lit8 v4, v4, 0x7

    rem-int/lit8 v4, v4, 0x7

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    const/4 v4, 0x1

    const/high16 v5, 0x41400000    # 12.0f

    invoke-virtual {v1, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 140
    const/16 v4, 0x11

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 141
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 143
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 133
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 149
    .end local v1    # "dayText":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method private onCalendarDayClicked(JZ)V
    .locals 9
    .param p1, "mTime"    # J
    .param p3, "containsMeasurements"    # Z

    .prologue
    const/4 v8, 0x0

    .line 723
    const-wide/32 v0, 0x5265c00

    .line 725
    .local v0, "checkTime":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 727
    .local v2, "startOfToday":J
    cmp-long v4, p1, v2

    if-ltz v4, :cond_0

    const-wide/16 v4, 0x6

    mul-long/2addr v4, v0

    add-long/2addr v4, v2

    cmp-long v4, p1, v4

    if-lez v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mInputButtonType:Ljava/lang/String;

    const-string v5, "StartDatePicker"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 731
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mContext:Landroid/content/Context;

    const v5, 0x7f090c81

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v8}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 745
    :goto_0
    return-void

    .line 735
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/framework/ui/calendar/OnPeriodSelectedListener;

    iget v5, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodType:I

    invoke-interface {v4, p1, p2, v5, p3}, Lcom/sec/android/app/shealth/framework/ui/calendar/OnPeriodSelectedListener;->onPeriodSelected(JIZ)V

    .line 738
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->disablePeriodSelectedDays(J)V

    .line 739
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mInputButtonType:Ljava/lang/String;

    const-string v5, "StartDatePicker"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 740
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->disableStartDay()V

    .line 741
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->selectedDay(J)V

    .line 743
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->buttonOnClick()V

    goto :goto_0
.end method

.method private periodSelectedDays()V
    .locals 25

    .prologue
    .line 252
    const-wide/32 v7, 0x5265c00

    .line 253
    .local v7, "checkTime":J
    const-wide/16 v10, 0x0

    .line 254
    .local v10, "dimSelectedPeriod":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v9

    .line 255
    .local v9, "currentDay":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v21

    move-wide/from16 v0, v21

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 256
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    .line 257
    .local v17, "startOfMonth":J
    const/16 v21, 0x2

    move-wide/from16 v0, v17

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v12

    .line 259
    .local v12, "endOfMonth":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v19

    .line 262
    .local v19, "startOfToday":J
    sub-long v21, v19, v17

    div-long v5, v21, v7

    .line 263
    .local v5, "checkStartDay":J
    const-wide/16 v21, 0x6

    mul-long v21, v21, v7

    add-long v21, v21, v19

    sub-long v21, v12, v21

    div-long v3, v21, v7

    .line 265
    .local v3, "checkEndDay":J
    const-string v16, ""

    .line 266
    .local v16, "isSunday":Ljava/lang/String;
    const/4 v14, 0x0

    .line 268
    .local v14, "getSundayFlag":Z
    const/4 v15, 0x1

    .local v15, "i":I
    :goto_0
    int-to-long v0, v15

    move-wide/from16 v21, v0

    cmp-long v21, v21, v5

    if-gtz v21, :cond_2

    .line 269
    int-to-long v0, v15

    move-wide/from16 v21, v0

    mul-long v21, v21, v7

    sub-long v10, v19, v21

    .line 270
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    move-object/from16 v21, v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 272
    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getToSunDay(J)Ljava/lang/String;

    move-result-object v16

    .line 273
    const-string v21, "Sun"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v21, v0

    if-eqz v21, :cond_0

    .line 276
    if-eqz v14, :cond_1

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 268
    :cond_0
    :goto_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 281
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v21, v0

    const/16 v22, 0x6

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_1

    .line 287
    :cond_2
    const/4 v15, 0x1

    :goto_2
    int-to-long v0, v15

    move-wide/from16 v21, v0

    cmp-long v21, v21, v3

    if-gtz v21, :cond_5

    .line 288
    const-wide/16 v21, 0x6

    mul-long v21, v21, v7

    add-long v21, v21, v19

    int-to-long v0, v15

    move-wide/from16 v23, v0

    mul-long v23, v23, v7

    add-long v10, v21, v23

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    move-object/from16 v21, v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 292
    invoke-static {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getToSunDay(J)Ljava/lang/String;

    move-result-object v16

    .line 293
    const-string v21, "Sun"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v21, v0

    if-eqz v21, :cond_3

    .line 296
    if-eqz v14, :cond_4

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v21, v0

    const/16 v22, 0x3

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 287
    :cond_3
    :goto_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 301
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    move-object/from16 v21, v0

    const/16 v22, 0x6

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    goto :goto_3

    .line 306
    :cond_5
    return-void
.end method

.method private refreshFocusable(Landroid/view/View;)V
    .locals 4
    .param p1, "content"    # Landroid/view/View;

    .prologue
    .line 832
    if-eqz p1, :cond_0

    .line 834
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Landroid/view/View;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 846
    :cond_0
    return-void
.end method

.method private selectedDay(J)V
    .locals 11
    .param p1, "selectedTime"    # J

    .prologue
    .line 309
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    iput-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 311
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    if-eqz v8, :cond_0

    .line 312
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedDayButton:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 315
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v5

    .line 317
    .local v5, "startOfToday":J
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 318
    .local v0, "currentDay":Ljava/util/Calendar;
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 319
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 320
    .local v3, "startOfMonth":J
    const/4 v8, 0x2

    invoke-static {v3, v4, v8}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfCurrentPeriod(JI)J

    move-result-wide v1

    .line 323
    .local v1, "endOfMonth":J
    cmp-long v8, v5, v3

    if-ltz v8, :cond_1

    cmp-long v8, v5, v1

    if-gtz v8, :cond_1

    .line 324
    iget-object v8, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodStartTodayButtonMap:Ljava/util/Map;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;

    .line 326
    .local v7, "todayDayButton":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;
    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;->setDayType(I)V

    .line 328
    .end local v7    # "todayDayButton":Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarDayButton;
    :cond_1
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/cignacoach/calendar/CoachCalendarActivity;->setOldSelectedTime(J)V

    .line 330
    return-void
.end method

.method private setCorrectDayTextColor(IILandroid/widget/TextView;)V
    .locals 2
    .param p1, "sundayIndex"    # I
    .param p2, "dayIndex"    # I
    .param p3, "dayText"    # Landroid/widget/TextView;

    .prologue
    .line 153
    if-ne p2, p1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private shiftCalendarToFirstDayOfWeek(Ljava/util/Calendar;I)V
    .locals 2
    .param p1, "calendar"    # Ljava/util/Calendar;
    .param p2, "firstDayOfWeek"    # I

    .prologue
    .line 175
    :goto_0
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 176
    const/4 v0, 0x6

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 178
    :cond_0
    return-void
.end method

.method private shiftCalendarToStartOfMonth(Ljava/util/Calendar;)V
    .locals 3
    .param p1, "calendar"    # Ljava/util/Calendar;

    .prologue
    const/4 v2, 0x0

    .line 165
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 166
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 167
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 168
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 169
    const/16 v0, 0xe

    invoke-virtual {p1, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 171
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    if-eqz p3, :cond_0

    .line 101
    const-string v0, "TIME"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    .line 102
    const-string v0, "PERIOD_TYPE"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodType:I

    .line 103
    const-string v0, "START_TIME"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mStartTime:J

    .line 104
    const-string v0, "SELECTED_TIME"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedTime:J

    .line 105
    const-string v0, "INPUT_BUTTON_TYPE"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mInputButtonType:Ljava/lang/String;

    .line 108
    :cond_0
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodType:I

    packed-switch v0, :pswitch_data_0

    .line 123
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 110
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->getDayView(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 981
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 983
    const-string v0, "TIME"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mTime:Ljava/util/Calendar;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 984
    const-string v0, "PERIOD_TYPE"

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mPeriodType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 985
    const-string v0, "START_TIME"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mStartTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 986
    const-string v0, "SELECTED_TIME"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mSelectedTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 987
    const-string v0, "INPUT_BUTTON_TYPE"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mInputButtonType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->mContext:Landroid/content/Context;

    .line 61
    return-void
.end method
