.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.source "ExtraFoodInfoData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private calcium:I

.field private carbohydrate:F

.field private cholesterol:I

.field private defaultNumber:F

.field private dietary:F

.field private fat:F

.field private foodInfoId:J

.field private grammInKcal:F

.field private iron:I

.field private monosaturated:F

.field private ozInKcal:F

.field private polysaturated:F

.field private potassium:I

.field private protein:F

.field private saturated:F

.field private servingDescription:Ljava/lang/String;

.field private sodium:I

.field private sugar:F

.field private transFat:F

.field private unit:I

.field private unitName:Ljava/lang/String;

.field private vitaminA:I

.field private vitaminC:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 355
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/high16 v2, -0x40800000    # -1.0f

    .line 62
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 11
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->foodInfoId:J

    .line 12
    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unit:I

    .line 13
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->fat:F

    .line 14
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->saturated:F

    .line 15
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->polysaturated:F

    .line 16
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->monosaturated:F

    .line 17
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->transFat:F

    .line 18
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->carbohydrate:F

    .line 19
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->dietary:F

    .line 20
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sugar:F

    .line 21
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->protein:F

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unitName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->servingDescription:Ljava/lang/String;

    .line 24
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->defaultNumber:F

    .line 25
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->grammInKcal:F

    .line 26
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->ozInKcal:F

    .line 27
    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->cholesterol:I

    .line 28
    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sodium:I

    .line 29
    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->potassium:I

    .line 30
    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminA:I

    .line 31
    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminC:I

    .line 32
    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->calcium:I

    .line 33
    iput v3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->iron:I

    .line 63
    return-void
.end method

.method public constructor <init>(JJIFFFFFFFFFLjava/lang/String;Ljava/lang/String;FFFIIIIIII)V
    .locals 26
    .param p1, "id"    # J
    .param p3, "foodInfoId"    # J
    .param p5, "unit"    # I
    .param p6, "fat"    # F
    .param p7, "saturated"    # F
    .param p8, "polysaturated"    # F
    .param p9, "monosaturated"    # F
    .param p10, "transFat"    # F
    .param p11, "carbohydrate"    # F
    .param p12, "dietary"    # F
    .param p13, "sugar"    # F
    .param p14, "protein"    # F
    .param p15, "unitName"    # Ljava/lang/String;
    .param p16, "servingDescription"    # Ljava/lang/String;
    .param p17, "defaultNumber"    # F
    .param p18, "grammInKcal"    # F
    .param p19, "ozInKcal"    # F
    .param p20, "cholesterol"    # I
    .param p21, "sodium"    # I
    .param p22, "potassium"    # I
    .param p23, "vitaminA"    # I
    .param p24, "vitaminC"    # I
    .param p25, "calcium"    # I
    .param p26, "iron"    # I

    .prologue
    .line 42
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(J)V

    .line 11
    const-wide/16 v1, -0x1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->foodInfoId:J

    .line 12
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unit:I

    .line 13
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->fat:F

    .line 14
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->saturated:F

    .line 15
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->polysaturated:F

    .line 16
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->monosaturated:F

    .line 17
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->transFat:F

    .line 18
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->carbohydrate:F

    .line 19
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->dietary:F

    .line 20
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sugar:F

    .line 21
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->protein:F

    .line 22
    const-string v1, ""

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unitName:Ljava/lang/String;

    .line 23
    const-string v1, ""

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->servingDescription:Ljava/lang/String;

    .line 24
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->defaultNumber:F

    .line 25
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->grammInKcal:F

    .line 26
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->ozInKcal:F

    .line 27
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->cholesterol:I

    .line 28
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sodium:I

    .line 29
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->potassium:I

    .line 30
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminA:I

    .line 31
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminC:I

    .line 32
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->calcium:I

    .line 33
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->iron:I

    move-object/from16 v1, p0

    move-wide/from16 v2, p3

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    move/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v15, p16

    move/from16 v16, p17

    move/from16 v17, p18

    move/from16 v18, p19

    move/from16 v19, p20

    move/from16 v20, p21

    move/from16 v21, p22

    move/from16 v22, p23

    move/from16 v23, p24

    move/from16 v24, p25

    move/from16 v25, p26

    .line 43
    invoke-direct/range {v1 .. v25}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->init(JIFFFFFFFFFLjava/lang/String;Ljava/lang/String;FFFIIIIIII)V

    .line 46
    return-void
.end method

.method public constructor <init>(JJIFFFFFFFFFLjava/lang/String;Ljava/lang/String;FFFIIIIIIIJJI)V
    .locals 26
    .param p1, "id"    # J
    .param p3, "foodInfoId"    # J
    .param p5, "unit"    # I
    .param p6, "fat"    # F
    .param p7, "saturated"    # F
    .param p8, "polysaturated"    # F
    .param p9, "monosaturated"    # F
    .param p10, "transFat"    # F
    .param p11, "carbohydrate"    # F
    .param p12, "dietary"    # F
    .param p13, "sugar"    # F
    .param p14, "protein"    # F
    .param p15, "unitName"    # Ljava/lang/String;
    .param p16, "servingDescription"    # Ljava/lang/String;
    .param p17, "defaultNumber"    # F
    .param p18, "grammInKcal"    # F
    .param p19, "ozInKcal"    # F
    .param p20, "cholesterol"    # I
    .param p21, "sodium"    # I
    .param p22, "potassium"    # I
    .param p23, "vitaminA"    # I
    .param p24, "vitaminC"    # I
    .param p25, "calcium"    # I
    .param p26, "iron"    # I
    .param p27, "createTime"    # J
    .param p29, "updateTime"    # J
    .param p31, "timeZone"    # I

    .prologue
    .line 56
    move-object/from16 v1, p0

    move-wide/from16 v2, p1

    move-wide/from16 v4, p27

    move-wide/from16 v6, p29

    move/from16 v8, p31

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJI)V

    .line 11
    const-wide/16 v1, -0x1

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->foodInfoId:J

    .line 12
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unit:I

    .line 13
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->fat:F

    .line 14
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->saturated:F

    .line 15
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->polysaturated:F

    .line 16
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->monosaturated:F

    .line 17
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->transFat:F

    .line 18
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->carbohydrate:F

    .line 19
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->dietary:F

    .line 20
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sugar:F

    .line 21
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->protein:F

    .line 22
    const-string v1, ""

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unitName:Ljava/lang/String;

    .line 23
    const-string v1, ""

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->servingDescription:Ljava/lang/String;

    .line 24
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->defaultNumber:F

    .line 25
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->grammInKcal:F

    .line 26
    const/high16 v1, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->ozInKcal:F

    .line 27
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->cholesterol:I

    .line 28
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sodium:I

    .line 29
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->potassium:I

    .line 30
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminA:I

    .line 31
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminC:I

    .line 32
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->calcium:I

    .line 33
    const/4 v1, -0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->iron:I

    move-object/from16 v1, p0

    move-wide/from16 v2, p3

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    move/from16 v11, p12

    move/from16 v12, p13

    move/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v15, p16

    move/from16 v16, p17

    move/from16 v17, p18

    move/from16 v18, p19

    move/from16 v19, p20

    move/from16 v20, p21

    move/from16 v21, p22

    move/from16 v22, p23

    move/from16 v23, p24

    move/from16 v24, p25

    move/from16 v25, p26

    .line 57
    invoke-direct/range {v1 .. v25}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->init(JIFFFFFFFFFLjava/lang/String;Ljava/lang/String;FFFIIIIIII)V

    .line 60
    return-void
.end method

.method private init(JIFFFFFFFFFLjava/lang/String;Ljava/lang/String;FFFIIIIIII)V
    .locals 1
    .param p1, "foodInfoId"    # J
    .param p3, "unit"    # I
    .param p4, "fat"    # F
    .param p5, "saturated"    # F
    .param p6, "polysaturated"    # F
    .param p7, "monosaturated"    # F
    .param p8, "transFat"    # F
    .param p9, "carbohydrate"    # F
    .param p10, "dietary"    # F
    .param p11, "sugar"    # F
    .param p12, "protein"    # F
    .param p13, "unitName"    # Ljava/lang/String;
    .param p14, "servingDescription"    # Ljava/lang/String;
    .param p15, "defaultNumber"    # F
    .param p16, "grammInKcal"    # F
    .param p17, "ozInKcal"    # F
    .param p18, "cholesterol"    # I
    .param p19, "sodium"    # I
    .param p20, "potassium"    # I
    .param p21, "vitaminA"    # I
    .param p22, "vitaminC"    # I
    .param p23, "calcium"    # I
    .param p24, "iron"    # I

    .prologue
    .line 71
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->foodInfoId:J

    .line 72
    iput p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unit:I

    .line 73
    iput p4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->fat:F

    .line 74
    iput p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->saturated:F

    .line 75
    iput p6, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->polysaturated:F

    .line 76
    iput p7, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->monosaturated:F

    .line 77
    iput p8, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->transFat:F

    .line 78
    iput p9, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->carbohydrate:F

    .line 79
    iput p10, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->dietary:F

    .line 80
    iput p11, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sugar:F

    .line 81
    iput p12, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->protein:F

    .line 82
    iput-object p13, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unitName:Ljava/lang/String;

    .line 83
    iput-object p14, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->servingDescription:Ljava/lang/String;

    .line 84
    move/from16 v0, p15

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->defaultNumber:F

    .line 85
    move/from16 v0, p16

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->grammInKcal:F

    .line 86
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->ozInKcal:F

    .line 87
    move/from16 v0, p18

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->cholesterol:I

    .line 88
    move/from16 v0, p19

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sodium:I

    .line 89
    move/from16 v0, p20

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->potassium:I

    .line 90
    move/from16 v0, p21

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminA:I

    .line 91
    move/from16 v0, p22

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminC:I

    .line 92
    move/from16 v0, p23

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->calcium:I

    .line 93
    move/from16 v0, p24

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->iron:I

    .line 95
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x0

    return v0
.end method

.method public getCalcium()I
    .locals 1

    .prologue
    .line 305
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->calcium:I

    return v0
.end method

.method public getCarbohydrate()F
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->carbohydrate:F

    return v0
.end method

.method public getCholesterol()I
    .locals 1

    .prologue
    .line 255
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->cholesterol:I

    return v0
.end method

.method public getDefaultNumber()F
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->defaultNumber:F

    return v0
.end method

.method public getDietary()F
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->dietary:F

    return v0
.end method

.method public getFat()F
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->fat:F

    return v0
.end method

.method public getFoodInfoId()J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->foodInfoId:J

    return-wide v0
.end method

.method public getGrammInKcal()F
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->grammInKcal:F

    return v0
.end method

.method public getIron()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->iron:I

    return v0
.end method

.method public getMonosaturated()F
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->monosaturated:F

    return v0
.end method

.method public getOzInKcal()F
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->ozInKcal:F

    return v0
.end method

.method public getPolysaturated()F
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->polysaturated:F

    return v0
.end method

.method public getPotassium()I
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->potassium:I

    return v0
.end method

.method public getProtein()F
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->protein:F

    return v0
.end method

.method public getSaturated()F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->saturated:F

    return v0
.end method

.method public getServingDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->servingDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getSodium()I
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sodium:I

    return v0
.end method

.method public getSugar()F
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sugar:F

    return v0
.end method

.method public getTransFat()F
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->transFat:F

    return v0
.end method

.method public getUnit()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unit:I

    return v0
.end method

.method public getUnitName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unitName:Ljava/lang/String;

    return-object v0
.end method

.method public getVitaminA()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminA:I

    return v0
.end method

.method public getVitaminC()I
    .locals 1

    .prologue
    .line 295
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminC:I

    return v0
.end method

.method public setCalcium(I)V
    .locals 0
    .param p1, "calcium"    # I

    .prologue
    .line 310
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->calcium:I

    .line 311
    return-void
.end method

.method public setCarbohydrate(F)V
    .locals 0
    .param p1, "carbohydrate"    # F

    .prologue
    .line 172
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->carbohydrate:F

    .line 173
    return-void
.end method

.method public setCholesterol(I)V
    .locals 0
    .param p1, "cholesterol"    # I

    .prologue
    .line 260
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->cholesterol:I

    .line 261
    return-void
.end method

.method public setDefaultNumber(F)V
    .locals 0
    .param p1, "defaultNumber"    # F

    .prologue
    .line 230
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->defaultNumber:F

    .line 231
    return-void
.end method

.method public setDietary(F)V
    .locals 0
    .param p1, "dietary"    # F

    .prologue
    .line 182
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->dietary:F

    .line 183
    return-void
.end method

.method public setFat(F)V
    .locals 0
    .param p1, "fat"    # F

    .prologue
    .line 124
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->fat:F

    .line 125
    return-void
.end method

.method public setFoodInfoId(J)V
    .locals 0
    .param p1, "foodInfoId"    # J

    .prologue
    .line 104
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->foodInfoId:J

    .line 105
    return-void
.end method

.method public setGrammInKcal(F)V
    .locals 0
    .param p1, "grammInKcal"    # F

    .prologue
    .line 240
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->grammInKcal:F

    .line 241
    return-void
.end method

.method public setIron(I)V
    .locals 0
    .param p1, "iron"    # I

    .prologue
    .line 320
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->iron:I

    .line 321
    return-void
.end method

.method public setMonosaturated(F)V
    .locals 0
    .param p1, "monosaturated"    # F

    .prologue
    .line 154
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->monosaturated:F

    .line 155
    return-void
.end method

.method public setOzInKcal(F)V
    .locals 0
    .param p1, "ozInKcal"    # F

    .prologue
    .line 250
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->ozInKcal:F

    .line 251
    return-void
.end method

.method public setPolysaturated(F)V
    .locals 0
    .param p1, "polysaturated"    # F

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->polysaturated:F

    .line 145
    return-void
.end method

.method public setPotassium(I)V
    .locals 0
    .param p1, "potassium"    # I

    .prologue
    .line 280
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->potassium:I

    .line 281
    return-void
.end method

.method public setProtein(F)V
    .locals 0
    .param p1, "protein"    # F

    .prologue
    .line 202
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->protein:F

    .line 203
    return-void
.end method

.method public setSaturated(F)V
    .locals 0
    .param p1, "saturated"    # F

    .prologue
    .line 134
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->saturated:F

    .line 135
    return-void
.end method

.method public setServingDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "servingDescription"    # Ljava/lang/String;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->servingDescription:Ljava/lang/String;

    .line 221
    return-void
.end method

.method public setSodium(I)V
    .locals 0
    .param p1, "sodium"    # I

    .prologue
    .line 270
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sodium:I

    .line 271
    return-void
.end method

.method public setSugar(F)V
    .locals 0
    .param p1, "sugar"    # F

    .prologue
    .line 192
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sugar:F

    .line 193
    return-void
.end method

.method public setTransFat(F)V
    .locals 0
    .param p1, "transFat"    # F

    .prologue
    .line 162
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->transFat:F

    .line 163
    return-void
.end method

.method public setUnit(I)V
    .locals 0
    .param p1, "unit"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unit:I

    .line 115
    return-void
.end method

.method public setUnitName(Ljava/lang/String;)V
    .locals 0
    .param p1, "unitName"    # Ljava/lang/String;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unitName:Ljava/lang/String;

    .line 213
    return-void
.end method

.method public setVitaminA(I)V
    .locals 0
    .param p1, "vitaminA"    # I

    .prologue
    .line 290
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminA:I

    .line 291
    return-void
.end method

.method public setVitaminC(I)V
    .locals 0
    .param p1, "vitaminC"    # I

    .prologue
    .line 300
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminC:I

    .line 301
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 327
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->foodInfoId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 328
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 329
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->fat:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 330
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->saturated:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 331
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->polysaturated:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 332
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->monosaturated:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 333
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->transFat:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 334
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->carbohydrate:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 335
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->dietary:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 336
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sugar:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 337
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->protein:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->unitName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->servingDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 340
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->defaultNumber:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 341
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->grammInKcal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 342
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->ozInKcal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 343
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->cholesterol:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 344
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->sodium:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 345
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->potassium:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 346
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminA:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 347
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->vitaminC:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 348
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->calcium:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 349
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->iron:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getUpdateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getTimeZone()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 353
    return-void
.end method
