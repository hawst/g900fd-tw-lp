.class Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->getPositiveButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->hideParentKeyboard(Landroid/view/View;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->access$300(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getDateDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getDateDialog()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->datepicker:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    invoke-virtual {v0}, Landroid/widget/DatePicker;->clearFocus()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mCal:Ljava/util/Calendar;
    invoke-static {v2}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->access$400(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getMonth()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->setMeasureDateWithErrorCheck(Ljava/util/Calendar;III)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    # getter for: Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->dateFormatter:Ljava/text/SimpleDateFormat;
    invoke-static {v1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->access$500(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)Ljava/text/SimpleDateFormat;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mDateTimePickerHelper:Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/DateTimePickerHelper;->getMeasureTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->mSelectedDate:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity$3;->this$0:Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;

    # invokes: Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->setSelectedDateTime()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->access$600(Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
