.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;
.super Ljava/lang/Object;
.source "ExerciseProWorkoutController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->doUiChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)V
    .locals 0

    .prologue
    .line 2608
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 2610
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mPersent:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$5300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    .line 2611
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isLongPress:Z
    invoke-static {v0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$5402(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Z)Z

    .line 2612
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mProgress:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->setVisibility(I)V

    .line 2613
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mMainLayout:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/RealtimeTouchLockView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$3900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/RealtimeTouchLockView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/RealtimeTouchLockView;->isLocked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2614
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2615
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;

    move-result-object v0

    invoke-interface {v0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$IHealthControllerListener;->onShownUnlockProgress(Z)V

    .line 2616
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v8

    .line 2618
    .local v8, "event":Landroid/view/MotionEvent;
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mUnlockBtn:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$5500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2619
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setLockModeWithAnimation(Z)V
    invoke-static {v0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Z)V

    .line 2621
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f090a46

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v9

    .line 2622
    .local v9, "toast":Landroid/widget/Toast;
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mContext:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0593

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v9, v0, v7, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 2623
    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 2625
    .end local v8    # "event":Landroid/view/MotionEvent;
    .end local v9    # "toast":Landroid/widget/Toast;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isTimerTaskRunning:Z
    invoke-static {v0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$5702(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;Z)Z

    .line 2641
    :cond_2
    :goto_0
    return-void

    .line 2628
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->isTimerTaskRunning:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$5700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2630
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    const/high16 v1, 0x40200000    # 2.5f

    # += operator for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mPersent:F
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$5316(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;F)F

    .line 2631
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mProgress:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mPersent:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$5300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->setProgress(F)V

    .line 2632
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mProgress:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    .line 2633
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mProgress:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProCircleSeekBar;->setVisibility(I)V

    .line 2636
    :cond_4
    sget v0, Lcom/sec/android/app/shealth/plugins/common/utils/UtilsCommon;->SOUND_MODE:I

    if-nez v0, :cond_2

    .line 2637
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController$36;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->mPersent:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->access$5300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;)F

    move-result v0

    const/high16 v1, 0x41a00000    # 20.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    goto :goto_0
.end method
