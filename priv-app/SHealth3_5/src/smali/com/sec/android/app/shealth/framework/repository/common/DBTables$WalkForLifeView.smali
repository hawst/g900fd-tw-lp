.class public Lcom/sec/android/app/shealth/framework/repository/common/DBTables$WalkForLifeView;
.super Ljava/lang/Object;
.source "DBTables.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/repository/common/DBTables;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WalkForLifeView"
.end annotation


# static fields
.field public static final KEY_AVG_SPEED:Ljava/lang/String; = "avgSpeed"

.field public static final KEY_COUNT:Ljava/lang/String; = "count"

.field public static final KEY_CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final KEY_DAY_START:Ljava/lang/String; = "day_st"

.field public static final KEY_MAX_SPEED:Ljava/lang/String; = "maxSpeed"

.field public static final KEY_MAX_TOTAL:Ljava/lang/String; = "maxTotal"

.field public static final KEY_MIN_SPEED:Ljava/lang/String; = "minSpeed"

.field public static final KEY_MIN_TOTAL:Ljava/lang/String; = "minTotal"

.field public static final KEY_SUM_DISTANCE:Ljava/lang/String; = "sumDistance"

.field public static final KEY_SUM_KCAL:Ljava/lang/String; = "sumKcal"

.field public static final KEY_SUM_RUN:Ljava/lang/String; = "sumRun"

.field public static final KEY_SUM_TOTAL:Ljava/lang/String; = "sumTotal"

.field public static final KEY_SUM_UPDOWN:Ljava/lang/String; = "sumUD"

.field public static final KEY_SUM_WALK:Ljava/lang/String; = "sumWalk"

.field public static final VIEW_NAME:Ljava/lang/String; = "walk_for_life_view"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
