.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;
.super Ljava/lang/Object;
.source "CignaWeightView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "adpater":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v5, 0x1fbd2

    const v4, 0x1fbd1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getValue()F

    move-result v0

    .line 102
    .local v0, "value":F
    if-nez p3, :cond_2

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)I

    move-result v1

    if-ne v1, v5, :cond_0

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevLbValue:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$300(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)F

    move-result v1

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$402(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Z)Z

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertLbToKg(F)F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$502(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;F)F

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$500(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)F

    move-result v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->floorToDigit(FI)F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevKgValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$602(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;F)F

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevLbValue:F
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$302(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;F)F

    .line 115
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I
    invoke-static {v1, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$202(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;I)I

    .line 136
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$500(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->setValue(F)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mEditText:Lcom/sec/android/app/shealth/common/commonui/EditableTextView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$700(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)Lcom/sec/android/app/shealth/common/commonui/EditableTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/EditableTextView;->selectAll()V

    .line 138
    :cond_0
    return-void

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$402(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Z)Z

    goto :goto_0

    .line 120
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$200(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevKgValue:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$600(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)F

    move-result v1

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_3

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$402(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Z)Z

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->getValue()F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$502(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;F)F

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$500(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;)F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mPrevKgValue:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$602(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;F)F

    .line 130
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mWeightUnit:I
    invoke-static {v1, v5}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$202(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;I)I

    goto :goto_1

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;

    # setter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->mIsWeightValueChanged:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;->access$402(Lcom/sec/android/app/shealth/cignacoach/widget/CignaWeightView;Z)Z

    goto :goto_2
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
