.class public Lcom/sec/android/app/shealth/common/utils/FormatUtils;
.super Ljava/lang/Object;
.source "FormatUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static roundValueToPrecision(FI)F
    .locals 5
    .param p0, "value"    # F
    .param p1, "precisionCount"    # I

    .prologue
    .line 30
    const-wide/high16 v1, 0x4024000000000000L    # 10.0

    int-to-double v3, p1

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float v0, v1

    .line 31
    .local v0, "multiplier":F
    mul-float v1, p0, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v0

    return v1
.end method
