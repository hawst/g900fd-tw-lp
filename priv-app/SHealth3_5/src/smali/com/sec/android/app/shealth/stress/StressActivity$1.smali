.class Lcom/sec/android/app/shealth/stress/StressActivity$1;
.super Ljava/lang/Object;
.source "StressActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/StressActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$1;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 96
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 97
    .local v0, "actionBarButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 99
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$1;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->onLogSelected()V

    goto :goto_0

    .line 102
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$1;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth.stress"

    const-string v3, "ST10"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$1;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mGraphFragment:Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$000(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressGraphFragmentSIC;->getChartReadyToShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$1;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # invokes: Lcom/sec/android/app/shealth/stress/StressActivity;->prepareShareView()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$100(Lcom/sec/android/app/shealth/stress/StressActivity;)V

    goto :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
