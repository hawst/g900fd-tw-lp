.class Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$2;
.super Ljava/lang/Object;
.source "QuickInputImagePanel.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->initViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "focused"    # Z

    .prologue
    .line 138
    if-nez p2, :cond_0

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$100(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$100(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel$2;->this$0:Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->mCalorieValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;->access$100(Lcom/sec/android/app/shealth/food/quickinput/QuickInputImagePanel;)Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    .line 146
    :cond_0
    return-void
.end method
