.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$16;
.super Ljava/lang/Object;
.source "ExerciseProActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0

    .prologue
    .line 1533
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 1
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 1536
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_2

    .line 1537
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->doNotShowCheckBoxLocation:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1538
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->ex_saveLocationPopupWasShown(Z)V

    .line 1542
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v0, :cond_0

    .line 1543
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->locationOKPressed()V

    .line 1549
    :cond_0
    :goto_1
    return-void

    .line 1540
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->ex_saveLocationPopupWasShown(Z)V

    goto :goto_0

    .line 1546
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 1547
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->finish()V

    goto :goto_1
.end method
