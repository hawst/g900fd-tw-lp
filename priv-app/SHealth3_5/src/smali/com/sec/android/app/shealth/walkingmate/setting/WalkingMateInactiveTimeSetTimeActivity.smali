.class public Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WalkingMateInactiveTimeSetTimeActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# static fields
.field private static final INACTIVE_TIME_SET_TIME_FRAGMENT:Ljava/lang/String; = "inactive_time_set_time_fragment"


# instance fields
.field private mWalkingMateInactiveTimeSetTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->mWalkingMateInactiveTimeSetTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907c3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 55
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 61
    const-string v0, "from"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->mWalkingMateInactiveTimeSetTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getFromButtonController()Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    .line 63
    :cond_0
    const-string/jumbo v0, "to"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->mWalkingMateInactiveTimeSetTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->getToButtonController()Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    goto :goto_0

    .line 65
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const v1, 0x7f030295

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->setContentView(I)V

    .line 44
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->mWalkingMateInactiveTimeSetTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 46
    .local v0, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f080ba8

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->mWalkingMateInactiveTimeSetTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    const-string v3, "inactive_time_set_time_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 47
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 49
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeActivity;->mWalkingMateInactiveTimeSetTimeFragment:Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/setting/WalkingMateInactiveTimeSetTimeFragment;->updateDate()V

    .line 74
    return-void
.end method
