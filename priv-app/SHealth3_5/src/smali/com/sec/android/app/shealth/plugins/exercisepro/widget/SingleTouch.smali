.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;
.super Ljava/lang/Object;
.source "SingleTouch.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;


# instance fields
.field private lastId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->TAG:Ljava/lang/String;

    .line 12
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->instance:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->lastId:I

    .line 19
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    .prologue
    .line 9
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->lastId:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;
    .param p1, "x1"    # I

    .prologue
    .line 9
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->lastId:I

    return p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;->instance:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;

    return-object v0
.end method


# virtual methods
.method public getTouchDelegate(Landroid/view/View;)Landroid/view/TouchDelegate;
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 22
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v5

    .line 23
    .local v5, "id":I
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/SingleTouch;Landroid/graphics/Rect;Landroid/view/View;Landroid/view/View;I)V

    return-object v0
.end method
