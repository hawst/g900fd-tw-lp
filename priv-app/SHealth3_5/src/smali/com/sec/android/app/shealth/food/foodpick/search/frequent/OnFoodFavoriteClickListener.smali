.class public Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;
.super Ljava/lang/Object;
.source "OnFoodFavoriteClickListener.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter$OnFavoriteClickListener",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "taskRunner"    # Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "taskRunner"    # Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
    .param p3, "userActionLogger"    # Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    .line 60
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mContext:Landroid/content/Context;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;

    .prologue
    .line 37
    check-cast p1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V

    return-void
.end method

.method public onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V
    .locals 2
    .param p1, "listItem"    # Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;->getFoodInfoItem()Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isExtraFoodInfoDataDownloadRequired(Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ADD_FAVORITE_AMONG_THE_SEARCH_RESULT:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;->mTaskRunner:Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/OnFoodFavoriteClickListener;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FoodListItem;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;->addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 119
    :cond_1
    return-void
.end method
