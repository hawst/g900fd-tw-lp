.class public Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
.super Ljava/lang/Object;
.source "LogManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;,
        Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;
    }
.end annotation


# static fields
.field private static final ACTION_LOG_AVAILABLE:Ljava/lang/String; = "available"

.field private static final ACTION_LOG_SAMPLE:Ljava/lang/String; = "com.sec.logsample"

.field private static final ACTION_LOG_SAMPLE_DLC_FILTER:Ljava/lang/String; = "com.sec.logsample.DLC_FILTER"

.field private static final ACTION_LOG_UNAVAILABLE:Ljava/lang/String; = "unavailable"

.field private static final ACTION_REGI:Ljava/lang/String; = "com.sec.spp.push.REQUEST_REGISTER"

.field private static final ACTION_RESULT_DEREGI:Ljava/lang/String; = "ACTION_RESULT_DEREGISTER"

.field private static final ACTION_RESULT_REGI:Ljava/lang/String; = "ACTION_RESULT_REGISTER"

.field public static final BA_LIBRARY:Ljava/lang/String; = "libbawrapper.dex"

.field static final BUF_SIZE:I = 0x2000

.field private static final DATA_ERROR_FIXED:Ljava/lang/String; = "DataErrorFixed_"

.field private static final DATA_ERROR_IGNORED:Ljava/lang/String; = "DataErrorIgnored_"

.field private static final DATA_TYPE_STRING:Ljava/lang/String; = "dataType"

.field private static final DATE_FORM:Ljava/lang/String; = "MMdd"

.field private static final DELAY_FOR_INTERNAL_TASK:I = 0x7530

.field private static final DELAY_WAITING_RESPONSE:I = 0xea60

.field public static final DYNAMIC_LIBRARY_PATH:Ljava/lang/String; = "dynamicLib"

.field private static final EXTRA_INTENTFILTER:Ljava/lang/String; = "EXTRA_INTENTFILTER"

.field private static final EXTRA_NAME_OFF:Ljava/lang/String; = "OFF"

.field private static final EXTRA_NAME_ON:Ljava/lang/String; = "ON"

.field private static final EXTRA_NAME_WIFI_ONLY:Ljava/lang/String; = "WIFI_ONLY"

.field private static final EXTRA_PACKAGENAME:Ljava/lang/String; = "EXTRA_PACKAGENAME"

.field private static final EXTRA_RESULT_CODE:Ljava/lang/String; = "EXTRA_RESULT_CODE"

.field private static final EXTRA_STR:Ljava/lang/String; = "EXTRA_STR"

.field private static final EXTRA_STR_ACTION:Ljava/lang/String; = "EXTRA_STR_ACTION"

.field private static final FEATURE_NAME_ERROR_DATA_FIXED:Ljava/lang/String; = "ERR_DATA_FIXED"

.field private static final FEATURE_NAME_ERROR_DATA_IGNORED:Ljava/lang/String; = "ERR_DATA_IGNORED"

.field private static final FEATURE_NAME_FOR_ACTIVITY_CHANGE:Ljava/lang/String; = "UAPP"

.field private static final FEATURE_NAME_PWD_STATUS:Ljava/lang/String; = "PWD_STATUS"

.field private static final FEATURE_NAME_SA_STATUS:Ljava/lang/String; = "SA_STATUS"

.field private static final FEATURE_NAME_SYNC_STATUS:Ljava/lang/String; = "SYNC_STATUS"

.field public static final GA_LIBRARY:Ljava/lang/String; = "libgawrapper.dex"

.field private static final INTENTFILTER:Ljava/lang/String; = "com.sec.logsample.DLC_FILTER"

.field private static final IS_FIXED_STRING:Ljava/lang/String; = "isFixed"

.field public static final LOG_APP_ID_STRING:Ljava/lang/String; = "app_id"

.field public static final LOG_EXTRA_STRING:Ljava/lang/String; = "extra"

.field public static final LOG_FEATURE_STRING:Ljava/lang/String; = "feature"

.field public static final LOG_FILE_NAME:Ljava/lang/String; = "servicelog"

.field private static final LOG_SENT_DATE:Ljava/lang/String; = "LogSentDate"

.field public static final LOG_VALUE_STRING:Ljava/lang/String; = "value"

.field public static final MSG_ACTIVITY_STRING:Ljava/lang/String; = "activity"

.field private static final MSG_CHECK_RESPONSE:I = 0x11

.field public static final POSTFIX_FAILED:Ljava/lang/String; = "failed"

.field private static final PREF_KEY_AUTO_BACKUP:Ljava/lang/String; = "auto_backup_switch_status"

.field private static final PREF_KEY_AUTO_BACKUP_INTERVAL:Ljava/lang/String; = "auto_backup_interval"

.field private static final PREF_KEY_AUTO_BACKUP_WIFI_ONLY:Ljava/lang/String; = "auto_backup_wifi_enable"

.field private static final PROCESSING_DAYS:I = 0x3

.field private static final PROFILE_BIRTHDAY:Ljava/lang/String; = "ProfileBirthday"

.field private static final PROFILE_GENDER:Ljava/lang/String; = "ProfileGender"

.field private static final SHARED_PREF_FILE:Ljava/lang/String; = "SHealthSerivceLog"

.field private static final SHEALTH_SHARED_PREF_FILE:Ljava/lang/String; = "com.sec.android.app.shealth_preferences"

.field public static final STATSERVICE_CLASS:Ljava/lang/String; = "com.sec.android.app.shealth.common.utils.logging.service.StatServiceWrapper"

.field public static final TAG:Ljava/lang/String; = "LogManager"

.field public static final TRACKER_CLASS:Ljava/lang/String; = "com.sec.android.app.shealth.common.utils.logging.service.EasyTrackerWrapper"

.field public static final USE_APP_SURVEY_ACTIVITY:Ljava/lang/String; = "001"

.field private static mIsRegistered:Z = false

.field private static sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager; = null

.field private static sIsARTMode:Z = false

.field private static sIsRuntimeChecked:Z = false

.field private static sIsSupportBa:Z = false

.field private static final sIsSupportCf:Z = true

.field private static sIsSupportGa:Z


# instance fields
.field private isProfileReady:Z

.field private isProfileSet:Z

.field private isUploaderRunning:Z

.field mActivityLifecycleCallback:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private mActivityName:Ljava/lang/String;

.field private mActivityStartTime:J

.field private mCfVersion:I

.field protected mContext:Landroid/content/Context;

.field private final mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

.field private mIsReceiverRegistered:Z

.field private mIsResponseReceived:Z

.field private mLogFilePrefix:Ljava/lang/String;

.field private mLogWriter:Ljava/io/OutputStreamWriter;

.field private final mLooper:Landroid/os/Looper;

.field mPref:Landroid/content/SharedPreferences;

.field private mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

.field private mSppReceiver:Landroid/content/BroadcastReceiver;

.field private mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

.field private mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

.field private mUserId:Ljava/lang/String;

.field private final mWorkerThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 125
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .line 128
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z

    .line 129
    sput-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z

    .line 130
    sput-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsARTMode:Z

    .line 131
    sput-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsRuntimeChecked:Z

    .line 1295
    sput-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsRegistered:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object v3, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    .line 133
    iput-object v3, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogFilePrefix:Ljava/lang/String;

    .line 141
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mCfVersion:I

    .line 146
    iput-object v3, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    .line 147
    iput-object v3, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    .line 1296
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsResponseReceived:Z

    .line 1297
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsReceiverRegistered:Z

    .line 1305
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z

    .line 1454
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$2;-><init>(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityLifecycleCallback:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 156
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "LogManager"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mWorkerThread:Landroid/os/HandlerThread;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mWorkerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLooper:Landroid/os/Looper;

    .line 159
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLooper:Landroid/os/Looper;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    .line 160
    iput-object v3, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mUserId:Ljava/lang/String;

    .line 161
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isProfileSet:Z

    .line 162
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isProfileReady:Z

    .line 163
    return-void
.end method

.method static synthetic access$000()V
    .locals 0

    .prologue
    .line 72
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->initGa()V

    return-void
.end method

.method static synthetic access$100()V
    .locals 0

    .prologue
    .line 72
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->initBa()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->registSPPReceiver()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->register()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsResponseReceived:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsResponseReceived:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityStartTime:J

    return-wide v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    .param p1, "x1"    # J

    .prologue
    .line 72
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityStartTime:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isProfileSet:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->setRegistered()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->setDeregistered()V

    return-void
.end method

.method static synthetic access$1900()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z

    return v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 72
    sget-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z

    return v0
.end method

.method static synthetic access$2000()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    return-object v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 72
    sput-boolean p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z

    return p0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->performDailyTask()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->startSendLog()V

    return-void
.end method

.method static synthetic access$500(ZLandroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Z
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->appendLog(ZLandroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getSamsungAccountUserId()Z

    move-result v0

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mUserId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    .param p1, "x1"    # Z

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z

    return p1
.end method

.method private static appendLog(ZLandroid/os/Bundle;)V
    .locals 12
    .param p0, "isActivityLog"    # Z
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 835
    const-string v0, "LogManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Append Log : "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, "app_id"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, ", "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v11, "feature"

    invoke-virtual {p1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    const-string v0, "feature"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 838
    .local v2, "feature":Ljava/lang/String;
    const-string v0, "extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 839
    .local v3, "extra":Ljava/lang/String;
    const-string/jumbo v0, "value"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 840
    .local v10, "strValue":Ljava/lang/String;
    if-nez p0, :cond_2

    .line 841
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    iget v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mCfVersion:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_2

    .line 843
    :try_start_0
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 844
    .local v9, "row":Landroid/content/ContentValues;
    const-string v0, "app_id"

    const-string v1, "app_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    const-string v0, "feature"

    invoke-virtual {v9, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    if-eqz v3, :cond_0

    .line 847
    const-string v0, "extra"

    invoke-virtual {v9, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    :cond_0
    if-eqz v10, :cond_1

    .line 850
    const-string/jumbo v0, "value"

    invoke-virtual {v9, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    :cond_1
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 854
    .local v6, "broadcastIntent":Landroid/content/Intent;
    const-string v0, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 855
    const-string v0, "data"

    invoke-virtual {v6, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 856
    const-string v0, "com.samsung.android.providers.context"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 857
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 866
    .end local v6    # "broadcastIntent":Landroid/content/Intent;
    .end local v9    # "row":Landroid/content/ContentValues;
    :cond_2
    :goto_0
    sget-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    if-eqz v0, :cond_3

    if-nez p0, :cond_3

    .line 868
    :try_start_1
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    invoke-interface {v0, p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;->send(Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 875
    :cond_3
    :goto_1
    sget-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    if-eqz v0, :cond_6

    if-nez p0, :cond_6

    .line 876
    if-nez v3, :cond_4

    .line 877
    const-string v3, ""

    .line 881
    :cond_4
    if-eqz v10, :cond_8

    .line 882
    :try_start_2
    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 883
    .local v4, "value":J
    const-string v0, "ERR_DATA_FIXED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ERR_DATA_IGNORED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 884
    :cond_5
    const-wide/16 v0, 0x3e8

    div-long v0, v4, v0

    long-to-int v7, v0

    .line 885
    .local v7, "count":I
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, v2, v3, v7}, Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 898
    .end local v4    # "value":J
    .end local v7    # "count":I
    :cond_6
    :goto_2
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->appendLogToFile(Landroid/os/Bundle;)V

    .line 899
    return-void

    .line 858
    :catch_0
    move-exception v8

    .line 859
    .local v8, "e":Ljava/lang/Exception;
    const-string v0, "LogManager"

    const-string v1, "Error while using the ContextProvider"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    const-string v0, "LogManager"

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 869
    .end local v8    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v8

    .line 870
    .restart local v8    # "e":Ljava/lang/Exception;
    const-string v0, "LogManager"

    const-string v1, "(GA) Failed to send event."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 887
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v4    # "value":J
    :cond_7
    :try_start_3
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;->onEventDuration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 892
    .end local v4    # "value":J
    :catch_2
    move-exception v8

    .line 893
    .restart local v8    # "e":Ljava/lang/Exception;
    const-string v0, "LogManager"

    const-string v1, "(BA) Failed to send event."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 890
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_8
    :try_start_4
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2
.end method

.method private appendLogToFile(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 946
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->getUseAppFeatureBody(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 947
    .local v1, "sppLog":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 948
    const/4 v2, 0x0

    .line 950
    .local v2, "writer":Ljava/io/OutputStreamWriter;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getOutputStream()Ljava/io/OutputStreamWriter;

    move-result-object v2

    .line 951
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStreamWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 952
    invoke-virtual {v2}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 965
    .end local v2    # "writer":Ljava/io/OutputStreamWriter;
    :cond_0
    :goto_0
    return-void

    .line 953
    .restart local v2    # "writer":Ljava/io/OutputStreamWriter;
    :catch_0
    move-exception v0

    .line 954
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "LogManager"

    const-string v4, "fails to append log to log file"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static checkProfileItems()V
    .locals 3

    .prologue
    .line 462
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 478
    .local v0, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :cond_0
    :goto_0
    return-void

    .line 467
    .end local v0    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z

    if-eqz v1, :cond_0

    .line 468
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v0

    .line 469
    .restart local v0    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    iget-object v1, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    if-nez v1, :cond_2

    .line 470
    const-string v1, "LogManager"

    const-string v2, "GA is not initialized yet, so set profile later."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isProfileReady:Z

    goto :goto_0

    .line 472
    :cond_2
    iget-boolean v1, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isProfileSet:Z

    if-nez v1, :cond_0

    .line 473
    const-string v1, "LogManager"

    const-string v2, "Request to check user profile for GA."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iget-object v1, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public static disableBa()V
    .locals 3

    .prologue
    .line 262
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 274
    .local v0, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :goto_0
    return-void

    .line 267
    .end local v0    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v0

    .line 268
    .restart local v0    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    iget-object v1, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 269
    const-string v1, "LogManager"

    const-string v2, "The instance is not initialized yet."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 273
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public static enableBa()V
    .locals 3

    .prologue
    .line 247
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 259
    .local v0, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :goto_0
    return-void

    .line 252
    .end local v0    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v0

    .line 253
    .restart local v0    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    iget-object v1, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 254
    const-string v1, "LogManager"

    const-string v2, "The instance is not initialized yet."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 258
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    .line 169
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sInstance:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    return-object v0
.end method

.method public static getMidnightTimeBefore(I)J
    .locals 4
    .param p0, "i"    # I

    .prologue
    const/4 v3, 0x0

    .line 570
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 571
    .local v0, "cal":Ljava/util/Calendar;
    const/4 v1, 0x5

    neg-int v2, p0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 572
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 573
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 574
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 575
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 576
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method private getOutputStream()Ljava/io/OutputStreamWriter;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v13, 0x5f

    const/4 v12, 0x0

    .line 913
    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string v10, "MMdd"

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v9, v10, v11}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-static {v12}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getMidnightTimeBefore(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 914
    .local v0, "dateString":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogFilePrefix:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogWriter:Ljava/io/OutputStreamWriter;

    if-eqz v9, :cond_0

    .line 915
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogWriter:Ljava/io/OutputStreamWriter;

    .line 942
    :goto_0
    return-object v9

    .line 917
    :cond_0
    const/4 v8, 0x0

    .line 918
    .local v8, "targetName":Ljava/lang/String;
    const/4 v9, 0x1

    new-array v4, v9, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "servicelog"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v12

    .line 919
    .local v4, "fileName":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 920
    .local v7, "target":Ljava/io/File;
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 921
    .local v1, "dir":Ljava/io/File;
    new-instance v6, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;

    invoke-direct {v6, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;-><init>([Ljava/lang/String;)V

    .line 922
    .local v6, "fnFilter":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;
    invoke-virtual {v1, v6}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v5

    .line 923
    .local v5, "files":[Ljava/io/File;
    if-eqz v5, :cond_2

    array-length v9, v5

    if-eqz v9, :cond_2

    .line 924
    aget-object v7, v5, v12

    .line 925
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    .line 931
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogWriter:Ljava/io/OutputStreamWriter;

    if-eqz v9, :cond_1

    .line 933
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogWriter:Ljava/io/OutputStreamWriter;

    invoke-virtual {v9}, Ljava/io/OutputStreamWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 939
    :cond_1
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    const v10, 0x8000

    invoke-virtual {v9, v8, v10}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v3

    .line 940
    .local v3, "fOut":Ljava/io/FileOutputStream;
    new-instance v9, Ljava/io/OutputStreamWriter;

    invoke-direct {v9, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    iput-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogWriter:Ljava/io/OutputStreamWriter;

    .line 941
    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogFilePrefix:Ljava/lang/String;

    .line 942
    iget-object v9, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mLogWriter:Ljava/io/OutputStreamWriter;

    goto :goto_0

    .line 927
    .end local v3    # "fOut":Ljava/io/FileOutputStream;
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "servicelog"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_-1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 928
    const-string v10, "LogManager"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    array-length v9, v5

    if-nez v9, :cond_3

    const-string v9, "No log file"

    :goto_3
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, " for "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v10, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    const-string v9, "Something inconsistent"

    goto :goto_3

    .line 934
    :catch_0
    move-exception v2

    .line 935
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method private getSamsungAccountUserId()Z
    .locals 6

    .prologue
    .line 1589
    :try_start_0
    const-string v1, "LogManager"

    const-string v2, "Reading account information"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1590
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    const-string v3, "1y90e30264"

    const-string v4, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v5, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$3;-><init>(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1623
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1619
    :catch_0
    move-exception v0

    .line 1620
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "LogManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1621
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static init(Landroid/app/Application;Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    .locals 7
    .param p0, "application"    # Landroid/app/Application;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 174
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-object v1

    .line 179
    :cond_1
    if-eqz p1, :cond_0

    .line 182
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v1

    .line 184
    .local v1, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_2

    .line 185
    const-string v3, "LogManager"

    const-string v4, "LogManager was already initialized."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 189
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 190
    sput-boolean v6, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z

    .line 191
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z

    .line 194
    :cond_3
    iput-object p1, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    .line 195
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->init(Landroid/content/Context;)V

    .line 196
    const-string v3, "SHealthSerivceLog"

    invoke-virtual {p1, v3, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    .line 198
    if-eqz p0, :cond_4

    .line 199
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mActivityLifecycleCallback:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p0, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 204
    :cond_4
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 206
    .local v2, "pInfo":Landroid/content/pm/PackageInfo;
    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mCfVersion:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    .end local v2    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    sget-boolean v3, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z

    if-eqz v3, :cond_5

    .line 213
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    .line 216
    :cond_5
    const-string v3, "LogManager"

    const-string v4, "LogManager is initialized."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private static initBa()V
    .locals 21

    .prologue
    .line 372
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 459
    .local v14, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :cond_0
    :goto_0
    return-void

    .line 377
    .end local v14    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v14

    .line 378
    .restart local v14    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    if-nez v17, :cond_2

    .line 379
    const-string v17, "LogManager"

    const-string v18, "The instance is not initialized yet."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 383
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 384
    const/16 v17, 0x1

    sput-boolean v17, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z

    .line 387
    :cond_3
    sget-boolean v17, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportBa:Z

    if-nez v17, :cond_4

    .line 388
    const-string v17, "LogManager"

    const-string v18, "BA logging is not supported."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 392
    :cond_4
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    .line 393
    const-string v17, "LogManager"

    const-string v18, "BA is already initialized."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 397
    :cond_5
    const/16 v16, 0x0

    .line 399
    .local v16, "statService":Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;
    new-instance v12, Ljava/io/File;

    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const-string v18, "dynamicLib"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v17

    const-string v18, "libbawrapper.dex"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v12, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 400
    .local v12, "libFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_7

    .line 402
    const/4 v4, 0x0

    .line 403
    .local v4, "bis":Ljava/io/BufferedInputStream;
    const/4 v9, 0x0

    .line 405
    .local v9, "fWriter":Ljava/io/OutputStream;
    :try_start_0
    new-instance v5, Ljava/io/BufferedInputStream;

    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v17

    const-string v18, "libbawrapper.dex"

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 406
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .local v5, "bis":Ljava/io/BufferedInputStream;
    :try_start_1
    new-instance v10, Ljava/io/BufferedOutputStream;

    new-instance v17, Ljava/io/FileOutputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 407
    .end local v9    # "fWriter":Ljava/io/OutputStream;
    .local v10, "fWriter":Ljava/io/OutputStream;
    const/16 v17, 0x2000

    :try_start_2
    move/from16 v0, v17

    new-array v6, v0, [B

    .line 409
    .local v6, "buf":[B
    :goto_1
    const/16 v17, 0x0

    const/16 v18, 0x2000

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v6, v0, v1}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v11

    .local v11, "len":I
    if-lez v11, :cond_6

    .line 410
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v10, v6, v0, v11}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 414
    .end local v6    # "buf":[B
    .end local v11    # "len":I
    :catch_0
    move-exception v8

    move-object v9, v10

    .end local v10    # "fWriter":Ljava/io/OutputStream;
    .restart local v9    # "fWriter":Ljava/io/OutputStream;
    move-object v4, v5

    .line 415
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .local v8, "e":Ljava/io/IOException;
    :goto_2
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 412
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "fWriter":Ljava/io/OutputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v6    # "buf":[B
    .restart local v10    # "fWriter":Ljava/io/OutputStream;
    .restart local v11    # "len":I
    :cond_6
    :try_start_3
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V

    .line 413
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 420
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v6    # "buf":[B
    .end local v10    # "fWriter":Ljava/io/OutputStream;
    .end local v11    # "len":I
    :cond_7
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const-string/jumbo v18, "outdex"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v15

    .line 422
    .local v15, "optimizedDexOutputPath":Ljava/io/File;
    :try_start_4
    new-instance v7, Ldalvik/system/DexClassLoader;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v7, v0, v1, v2, v3}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 426
    .local v7, "cl":Ldalvik/system/DexClassLoader;
    const/4 v13, 0x0

    .line 427
    .local v13, "libStatServiceClazz":Ljava/lang/Class;
    const-string v17, "com.sec.android.app.shealth.common.utils.logging.service.StatServiceWrapper"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    .line 428
    invoke-virtual {v13}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    move-object/from16 v16, v0

    .line 429
    const-string v17, "LogManager"

    const-string v18, "Success to get tracker instance through "

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 434
    .end local v7    # "cl":Ldalvik/system/DexClassLoader;
    .end local v13    # "libStatServiceClazz":Ljava/lang/Class;
    :goto_3
    if-eqz v16, :cond_0

    .line 439
    :try_start_5
    move-object/from16 v0, v16

    iput-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    .line 440
    iget-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    move-object/from16 v17, v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    const-string/jumbo v19, "shealth"

    const/16 v20, 0x1

    invoke-interface/range {v17 .. v20}, Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;->setAppChannel(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 442
    :catch_1
    move-exception v8

    .line 443
    .local v8, "e":Ljava/lang/Exception;
    const-string v17, "LogManager"

    const-string v18, "Failed to initialize BA."

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 445
    const/16 v17, 0x0

    move-object/from16 v0, v17

    iput-object v0, v14, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mStatService:Lcom/sec/android/app/shealth/common/utils/logging/service/IBaiduAnalyticsStatService;

    goto/16 :goto_0

    .line 430
    .end local v8    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v8

    .line 431
    .restart local v8    # "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 414
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v15    # "optimizedDexOutputPath":Ljava/io/File;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v9    # "fWriter":Ljava/io/OutputStream;
    :catch_3
    move-exception v8

    goto/16 :goto_2

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    :catch_4
    move-exception v8

    move-object v4, v5

    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    goto/16 :goto_2
.end method

.method private static initGa()V
    .locals 23

    .prologue
    .line 277
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 369
    .local v16, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :cond_0
    :goto_0
    return-void

    .line 282
    .end local v16    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v16

    .line 283
    .restart local v16    # "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    if-nez v19, :cond_2

    .line 284
    const-string v19, "LogManager"

    const-string v20, "The instance is not initialized yet."

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 288
    :cond_2
    sget-boolean v19, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z

    if-nez v19, :cond_3

    .line 289
    const-string v19, "LogManager"

    const-string v20, "GA logging is not enabled."

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 293
    :cond_3
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    .line 294
    const-string v19, "LogManager"

    const-string v20, "GA is already initialized."

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 298
    :cond_4
    const/16 v18, 0x0

    .line 300
    .local v18, "tracker":Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v20, "dynamicLib"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v19

    const-string v20, "libgawrapper.dex"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v14, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 301
    .local v14, "libFile":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v19

    if-nez v19, :cond_6

    .line 303
    const/4 v5, 0x0

    .line 304
    .local v5, "bis":Ljava/io/BufferedInputStream;
    const/4 v10, 0x0

    .line 306
    .local v10, "fWriter":Ljava/io/OutputStream;
    :try_start_0
    new-instance v6, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v19

    const-string v20, "libgawrapper.dex"

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .local v6, "bis":Ljava/io/BufferedInputStream;
    :try_start_1
    new-instance v11, Ljava/io/BufferedOutputStream;

    new-instance v19, Ljava/io/FileOutputStream;

    move-object/from16 v0, v19

    invoke-direct {v0, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 308
    .end local v10    # "fWriter":Ljava/io/OutputStream;
    .local v11, "fWriter":Ljava/io/OutputStream;
    const/16 v19, 0x2000

    :try_start_2
    move/from16 v0, v19

    new-array v7, v0, [B

    .line 310
    .local v7, "buf":[B
    :goto_1
    const/16 v19, 0x0

    const/16 v20, 0x2000

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v7, v0, v1}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v13

    .local v13, "len":I
    if-lez v13, :cond_5

    .line 311
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v7, v0, v13}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    .line 313
    .end local v7    # "buf":[B
    .end local v13    # "len":I
    :catch_0
    move-exception v9

    move-object v10, v11

    .end local v11    # "fWriter":Ljava/io/OutputStream;
    .restart local v10    # "fWriter":Ljava/io/OutputStream;
    move-object v5, v6

    .line 314
    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .local v9, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 319
    :try_start_4
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 324
    :goto_3
    :try_start_5
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 325
    :catch_1
    move-exception v9

    .line 326
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 319
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "fWriter":Ljava/io/OutputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "buf":[B
    .restart local v11    # "fWriter":Ljava/io/OutputStream;
    .restart local v13    # "len":I
    :cond_5
    :try_start_6
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 324
    :goto_4
    :try_start_7
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 332
    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "buf":[B
    .end local v11    # "fWriter":Ljava/io/OutputStream;
    .end local v13    # "len":I
    :cond_6
    :goto_5
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string/jumbo v20, "outdex"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v17

    .line 334
    .local v17, "optimizedDexOutputPath":Ljava/io/File;
    :try_start_8
    new-instance v8, Ldalvik/system/DexClassLoader;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-direct {v8, v0, v1, v2, v3}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 338
    .local v8, "cl":Ldalvik/system/DexClassLoader;
    const/4 v15, 0x0

    .line 339
    .local v15, "libTrackerClazz":Ljava/lang/Class;
    const-string v19, "com.sec.android.app.shealth.common.utils.logging.service.EasyTrackerWrapper"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v15

    .line 340
    invoke-virtual {v15}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v19

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    move-object/from16 v18, v0

    .line 341
    const-string v19, "LogManager"

    const-string v20, "Success to get tracker instance through "

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    .line 346
    .end local v8    # "cl":Ldalvik/system/DexClassLoader;
    .end local v15    # "libTrackerClazz":Ljava/lang/Class;
    :goto_6
    if-eqz v18, :cond_0

    .line 351
    :try_start_9
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;->getTracker(Landroid/content/Context;)Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    .line 352
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    sget-object v21, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->FIRMWARE_VERSION:Ljava/lang/String;

    invoke-interface/range {v19 .. v21}, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;->setCustomDimension(ILjava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    .line 360
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "ProfileGender"

    const/16 v21, -0x1

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v12

    .line 361
    .local v12, "gender":I
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v19, v0

    const-string v20, "ProfileBirthday"

    const-string v21, ""

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 363
    .local v4, "birthday":Ljava/lang/String;
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v12, v0, :cond_7

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v19

    if-lez v19, :cond_7

    .line 364
    invoke-static {v12, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->setProfileItems(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 320
    .end local v4    # "birthday":Ljava/lang/String;
    .end local v12    # "gender":I
    .end local v17    # "optimizedDexOutputPath":Ljava/io/File;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "buf":[B
    .restart local v11    # "fWriter":Ljava/io/OutputStream;
    .restart local v13    # "len":I
    :catch_2
    move-exception v9

    .line 321
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 325
    .end local v9    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v9

    .line 326
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 320
    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .end local v7    # "buf":[B
    .end local v11    # "fWriter":Ljava/io/OutputStream;
    .end local v13    # "len":I
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v10    # "fWriter":Ljava/io/OutputStream;
    :catch_4
    move-exception v9

    .line 321
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 318
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v19

    .line 319
    :goto_7
    :try_start_a
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 324
    :goto_8
    :try_start_b
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 327
    :goto_9
    throw v19

    .line 320
    :catch_5
    move-exception v9

    .line 321
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 325
    .end local v9    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v9

    .line 326
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 342
    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "fWriter":Ljava/io/OutputStream;
    .restart local v17    # "optimizedDexOutputPath":Ljava/io/File;
    :catch_7
    move-exception v9

    .line 343
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 353
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v9

    .line 354
    .restart local v9    # "e":Ljava/lang/Exception;
    const-string v19, "LogManager"

    const-string v20, "Failed to initialize GA."

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 356
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    goto/16 :goto_0

    .line 365
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v4    # "birthday":Ljava/lang/String;
    .restart local v12    # "gender":I
    :cond_7
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isProfileReady:Z

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 366
    const-string v19, "LogManager"

    const-string v20, "Request to check user profile for GA. (pending)"

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    move-object/from16 v19, v0

    const/16 v20, 0xb

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 318
    .end local v4    # "birthday":Ljava/lang/String;
    .end local v12    # "gender":I
    .end local v17    # "optimizedDexOutputPath":Ljava/io/File;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v10    # "fWriter":Ljava/io/OutputStream;
    :catchall_1
    move-exception v19

    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_7

    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .end local v10    # "fWriter":Ljava/io/OutputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v11    # "fWriter":Ljava/io/OutputStream;
    :catchall_2
    move-exception v19

    move-object v10, v11

    .end local v11    # "fWriter":Ljava/io/OutputStream;
    .restart local v10    # "fWriter":Ljava/io/OutputStream;
    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    goto :goto_7

    .line 313
    :catch_9
    move-exception v9

    goto/16 :goto_2

    .end local v5    # "bis":Ljava/io/BufferedInputStream;
    .restart local v6    # "bis":Ljava/io/BufferedInputStream;
    :catch_a
    move-exception v9

    move-object v5, v6

    .end local v6    # "bis":Ljava/io/BufferedInputStream;
    .restart local v5    # "bis":Ljava/io/BufferedInputStream;
    goto/16 :goto_2
.end method

.method public static insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "value"    # Ljava/lang/Long;

    .prologue
    .line 580
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 613
    :cond_0
    :goto_0
    return-void

    .line 585
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v1

    .line 586
    .local v1, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 591
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 594
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 598
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 599
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "app_id"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    const-string v3, "feature"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    if-eqz p3, :cond_2

    .line 603
    const-string v3, "extra"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_2
    if-eqz p4, :cond_3

    .line 606
    const-string/jumbo v3, "value"

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_3
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 610
    .local v2, "msg":Landroid/os/Message;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 611
    const/4 v3, 0x1

    iput v3, v2, Landroid/os/Message;->what:I

    .line 612
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public static isARTWithKitkat()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1632
    sget-boolean v2, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsRuntimeChecked:Z

    if-eqz v2, :cond_0

    .line 1634
    sget-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsARTMode:Z

    .line 1656
    :goto_0
    return v0

    .line 1636
    :cond_0
    const-string v2, "java.vm.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1638
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-ge v2, v3, :cond_1

    .line 1640
    sput-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsARTMode:Z

    .line 1641
    sput-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsRuntimeChecked:Z

    goto :goto_0

    .line 1646
    :cond_1
    sput-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsRuntimeChecked:Z

    .line 1647
    sput-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsARTMode:Z

    move v0, v1

    .line 1648
    goto :goto_0

    .line 1653
    :cond_2
    sput-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsARTMode:Z

    .line 1654
    sput-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsRuntimeChecked:Z

    move v0, v1

    .line 1656
    goto :goto_0
.end method

.method private performDailyTask()V
    .locals 27

    .prologue
    .line 973
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v13

    .line 976
    .local v13, "isSamsungAccountExist":Z
    if-eqz v13, :cond_3

    .line 977
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "SA_STATUS"

    const-string v23, "ON"

    const/16 v24, 0x0

    invoke-static/range {v20 .. v24}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 983
    :goto_0
    if-eqz v13, :cond_6

    .line 984
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth_preferences"

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v16

    .line 985
    .local v16, "pref":Landroid/content/SharedPreferences;
    if-eqz v16, :cond_0

    .line 986
    const-string v20, "auto_backup_switch_status"

    const/16 v21, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    .line 987
    .local v12, "isAutoBackupEnabled":Z
    if-eqz v12, :cond_5

    .line 988
    const-string v20, "auto_backup_wifi_enable"

    const/16 v21, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    .line 989
    .local v14, "isWifiOnly":Z
    const-string v20, "auto_backup_interval"

    const-wide/16 v21, 0x2a30

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v17

    .line 991
    .local v17, "syncInterval":J
    if-eqz v14, :cond_4

    .line 992
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "SYNC_STATUS"

    const-string v23, "WIFI_ONLY"

    const-wide/16 v24, 0x3e8

    mul-long v24, v24, v17

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    invoke-static/range {v20 .. v24}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1006
    .end local v12    # "isAutoBackupEnabled":Z
    .end local v14    # "isWifiOnly":Z
    .end local v16    # "pref":Landroid/content/SharedPreferences;
    .end local v17    # "syncInterval":J
    :cond_0
    :goto_1
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 1007
    .local v10, "extras":Landroid/os/Bundle;
    const-string v20, "key"

    const-string/jumbo v21, "security_pin_enabled"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    const-string/jumbo v20, "value"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1011
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    sget-object v21, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v22, "CONFIG_OPTION_GET"

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3, v10}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v5

    .line 1012
    .local v5, "bundle":Landroid/os/Bundle;
    const-string/jumbo v20, "value"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v19

    .line 1014
    .local v19, "value":Z
    if-eqz v19, :cond_7

    .line 1015
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "PWD_STATUS"

    const-string v23, "ON"

    const/16 v24, 0x0

    invoke-static/range {v20 .. v24}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1025
    .end local v5    # "bundle":Landroid/os/Bundle;
    .end local v19    # "value":Z
    :goto_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v4

    .line 1026
    .local v4, "allEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 1027
    .local v9, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 1028
    .local v15, "key":Ljava/lang/String;
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1030
    .local v19, "value":Ljava/lang/String;
    if-eqz v15, :cond_1

    if-eqz v19, :cond_1

    .line 1033
    const-string v20, "DataErrorFixed_"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 1034
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 1035
    .local v6, "count":I
    if-lez v6, :cond_1

    .line 1036
    const-string v20, "DataErrorFixed_"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 1037
    .local v7, "dataType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "ERR_DATA_FIXED"

    int-to-long v0, v6

    move-wide/from16 v23, v0

    const-wide/16 v25, 0x3e8

    mul-long v23, v23, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-static {v0, v1, v2, v7, v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v0, v15}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 1052
    .end local v4    # "allEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    .end local v6    # "count":I
    .end local v7    # "dataType":Ljava/lang/String;
    .end local v9    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v15    # "key":Ljava/lang/String;
    .end local v19    # "value":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 1053
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 1055
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_2
    return-void

    .line 979
    .end local v10    # "extras":Landroid/os/Bundle;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "SA_STATUS"

    const-string v23, "OFF"

    const/16 v24, 0x0

    invoke-static/range {v20 .. v24}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_0

    .line 994
    .restart local v12    # "isAutoBackupEnabled":Z
    .restart local v14    # "isWifiOnly":Z
    .restart local v16    # "pref":Landroid/content/SharedPreferences;
    .restart local v17    # "syncInterval":J
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "SYNC_STATUS"

    const-string v23, "ON"

    const-wide/16 v24, 0x3e8

    mul-long v24, v24, v17

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    invoke-static/range {v20 .. v24}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 998
    .end local v14    # "isWifiOnly":Z
    .end local v17    # "syncInterval":J
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "SYNC_STATUS"

    const-string v23, "OFF"

    const/16 v24, 0x0

    invoke-static/range {v20 .. v24}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 1002
    .end local v12    # "isAutoBackupEnabled":Z
    .end local v16    # "pref":Landroid/content/SharedPreferences;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "SYNC_STATUS"

    const-string v23, "OFF"

    const/16 v24, 0x0

    invoke-static/range {v20 .. v24}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 1017
    .restart local v5    # "bundle":Landroid/os/Bundle;
    .restart local v10    # "extras":Landroid/os/Bundle;
    .local v19, "value":Z
    :cond_7
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "PWD_STATUS"

    const-string v23, "OFF"

    const/16 v24, 0x0

    invoke-static/range {v20 .. v24}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 1019
    .end local v5    # "bundle":Landroid/os/Bundle;
    .end local v19    # "value":Z
    :catch_1
    move-exception v8

    .line 1020
    .restart local v8    # "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 1040
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v4    # "allEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    .restart local v9    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .restart local v11    # "i$":Ljava/util/Iterator;
    .restart local v15    # "key":Ljava/lang/String;
    .local v19, "value":Ljava/lang/String;
    :cond_8
    :try_start_3
    const-string v20, "DataErrorIgnored_"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 1041
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 1042
    .restart local v6    # "count":I
    if-lez v6, :cond_1

    .line 1043
    const-string v20, "DataErrorIgnored_"

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 1044
    .restart local v7    # "dataType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "com.sec.android.app.shealth"

    const-string v22, "ERR_DATA_IGNORED"

    int-to-long v0, v6

    move-wide/from16 v23, v0

    const-wide/16 v25, 0x3e8

    mul-long v23, v23, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-static {v0, v1, v2, v7, v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1045
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v0, v15}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_3
.end method

.method private realStartSendLog()V
    .locals 2

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessage(I)Z

    .line 1067
    return-void
.end method

.method private registSPPReceiver()V
    .locals 5

    .prologue
    .line 1367
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsReceiverRegistered:Z

    if-eqz v1, :cond_1

    .line 1368
    const-string v1, "LogManager"

    const-string v2, "BR is already registered."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    :cond_0
    :goto_0
    return-void

    .line 1372
    :cond_1
    const-string v1, "LogManager"

    const-string v2, "Register SPP broadcast receiver"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsResponseReceived:Z

    .line 1376
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.logsample"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1377
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.logsample.DLC_FILTER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1379
    new-instance v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$1;-><init>(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSppReceiver:Landroid/content/BroadcastReceiver;

    .line 1445
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSppReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1446
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsReceiverRegistered:Z

    .line 1449
    sget-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsRegistered:Z

    if-nez v1, :cond_0

    .line 1450
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    const/16 v2, 0x11

    const-wide/32 v3, 0xea60

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private register()V
    .locals 3

    .prologue
    .line 1336
    const-string v1, "LogManager"

    const-string v2, "Register to spp"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1337
    sget-boolean v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsRegistered:Z

    if-eqz v1, :cond_0

    .line 1338
    const-string v1, "LogManager"

    const-string v2, "Already registered."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->realStartSendLog()V

    .line 1350
    :goto_0
    return-void

    .line 1343
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.REQUEST_REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1345
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_PACKAGENAME"

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1346
    const-string v1, "EXTRA_INTENTFILTER"

    const-string v2, "com.sec.logsample.DLC_FILTER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1348
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1349
    const-string v1, "LogManager"

    const-string v2, "Send broadcast : ACTION_REGI"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static reportValidationError(Ljava/lang/String;Z)V
    .locals 6
    .param p0, "dataType"    # Ljava/lang/String;
    .param p1, "isFixed"    # Z

    .prologue
    .line 222
    const-string v3, "LogManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "reportValidationError() dataType=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] isFixed=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v1

    .line 230
    .local v1, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 235
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 236
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "dataType"

    invoke-virtual {v0, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v3, "isFixed"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 239
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 240
    .local v2, "msg":Landroid/os/Message;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 241
    const/16 v3, 0xc

    iput v3, v2, Landroid/os/Message;->what:I

    .line 243
    iget-object v3, v1, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private sendLines(Ljava/io/File;I)Z
    .locals 17
    .param p1, "file"    # Ljava/io/File;
    .param p2, "lineNum"    # I

    .prologue
    .line 617
    const/4 v5, 0x0

    .line 618
    .local v5, "fIn":Ljava/io/FileInputStream;
    const/4 v10, 0x0

    .line 619
    .local v10, "reader":Ljava/io/InputStreamReader;
    const/4 v7, 0x0

    .line 621
    .local v7, "lineReader":Ljava/io/LineNumberReader;
    const/4 v11, 0x1

    .line 624
    .local v11, "result":Z
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v5    # "fIn":Ljava/io/FileInputStream;
    .local v6, "fIn":Ljava/io/FileInputStream;
    move-object v5, v6

    .line 628
    .end local v6    # "fIn":Ljava/io/FileInputStream;
    .restart local v5    # "fIn":Ljava/io/FileInputStream;
    :goto_0
    if-eqz v5, :cond_5

    .line 629
    new-instance v10, Ljava/io/InputStreamReader;

    .end local v10    # "reader":Ljava/io/InputStreamReader;
    invoke-direct {v10, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 630
    .restart local v10    # "reader":Ljava/io/InputStreamReader;
    new-instance v7, Ljava/io/LineNumberReader;

    .end local v7    # "lineReader":Ljava/io/LineNumberReader;
    invoke-direct {v7, v10}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;)V

    .line 633
    .restart local v7    # "lineReader":Ljava/io/LineNumberReader;
    const/4 v1, 0x0

    .local v1, "count":I
    move v2, v1

    .line 634
    .end local v1    # "count":I
    .local v2, "count":I
    :goto_1
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "count":I
    .restart local v1    # "count":I
    move/from16 v0, p2

    if-ge v2, v0, :cond_1

    :try_start_1
    invoke-virtual {v7}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .local v8, "log":Ljava/lang/String;
    if-eqz v8, :cond_1

    move v2, v1

    .end local v1    # "count":I
    .restart local v2    # "count":I
    goto :goto_1

    .line 625
    .end local v2    # "count":I
    .end local v8    # "log":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 626
    .local v3, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 646
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    .restart local v1    # "count":I
    .restart local v8    # "log":Ljava/lang/String;
    :cond_0
    add-int/lit8 p2, p2, 0x1

    .line 636
    .end local v8    # "log":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v7}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "log":Ljava/lang/String;
    if-eqz v8, :cond_3

    .line 638
    const-string v12, "002"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v8}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sendLogViaSpp(Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    if-gez v12, :cond_0

    .line 639
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "_"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v15

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 640
    .local v9, "newFileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v9}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 642
    const-string v12, "LogManager"

    const-string v13, "Renaming the target file is failed"

    invoke-static {v12, v13}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 644
    :cond_2
    const/4 v12, 0x0

    .line 656
    :try_start_3
    invoke-virtual {v10}, Ljava/io/InputStreamReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 661
    :goto_2
    :try_start_4
    invoke-virtual {v7}, Ljava/io/LineNumberReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 666
    :goto_3
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 672
    .end local v1    # "count":I
    .end local v8    # "log":Ljava/lang/String;
    .end local v9    # "newFileName":Ljava/lang/String;
    :goto_4
    return v12

    .line 657
    .restart local v1    # "count":I
    .restart local v8    # "log":Ljava/lang/String;
    .restart local v9    # "newFileName":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 658
    .local v4, "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 662
    .end local v4    # "eSub":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 663
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 667
    .end local v4    # "eSub":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 668
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 649
    .end local v4    # "eSub":Ljava/io/IOException;
    .end local v9    # "newFileName":Ljava/lang/String;
    :cond_3
    :try_start_6
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 650
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->delete()Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v11

    .line 656
    :cond_4
    :try_start_7
    invoke-virtual {v10}, Ljava/io/InputStreamReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 661
    :goto_5
    :try_start_8
    invoke-virtual {v7}, Ljava/io/LineNumberReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 666
    :goto_6
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .end local v1    # "count":I
    .end local v8    # "log":Ljava/lang/String;
    :cond_5
    :goto_7
    move v12, v11

    .line 672
    goto :goto_4

    .line 657
    .restart local v1    # "count":I
    .restart local v8    # "log":Ljava/lang/String;
    :catch_4
    move-exception v4

    .line 658
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 662
    .end local v4    # "eSub":Ljava/io/IOException;
    :catch_5
    move-exception v4

    .line 663
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 667
    .end local v4    # "eSub":Ljava/io/IOException;
    :catch_6
    move-exception v4

    .line 668
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 652
    .end local v4    # "eSub":Ljava/io/IOException;
    .end local v8    # "log":Ljava/lang/String;
    :catch_7
    move-exception v3

    .line 653
    .local v3, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 656
    :try_start_b
    invoke-virtual {v10}, Ljava/io/InputStreamReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 661
    :goto_8
    :try_start_c
    invoke-virtual {v7}, Ljava/io/LineNumberReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a

    .line 666
    :goto_9
    :try_start_d
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8

    goto :goto_7

    .line 667
    :catch_8
    move-exception v4

    .line 668
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 657
    .end local v4    # "eSub":Ljava/io/IOException;
    :catch_9
    move-exception v4

    .line 658
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 662
    .end local v4    # "eSub":Ljava/io/IOException;
    :catch_a
    move-exception v4

    .line 663
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 655
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "eSub":Ljava/io/IOException;
    :catchall_0
    move-exception v12

    .line 656
    :try_start_e
    invoke-virtual {v10}, Ljava/io/InputStreamReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 661
    :goto_a
    :try_start_f
    invoke-virtual {v7}, Ljava/io/LineNumberReader;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_c

    .line 666
    :goto_b
    :try_start_10
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_d

    .line 669
    :goto_c
    throw v12

    .line 657
    :catch_b
    move-exception v4

    .line 658
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 662
    .end local v4    # "eSub":Ljava/io/IOException;
    :catch_c
    move-exception v4

    .line 663
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 667
    .end local v4    # "eSub":Ljava/io/IOException;
    :catch_d
    move-exception v4

    .line 668
    .restart local v4    # "eSub":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c
.end method

.method private setDeregistered()V
    .locals 2

    .prologue
    .line 1359
    const-string v0, "LogManager"

    const-string/jumbo v1, "setDeregistered()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsRegistered:Z

    .line 1363
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    .line 1364
    return-void
.end method

.method public static setProfileItems(ILjava/lang/String;)V
    .locals 10
    .param p0, "gender"    # I
    .param p1, "birthday"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x4

    .line 481
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getInstance()Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;

    move-result-object v4

    .line 487
    .local v4, "manager":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;
    iget-object v7, v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    if-eqz v7, :cond_0

    iget-object v7, v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    if-eqz v7, :cond_0

    .line 491
    sget-boolean v7, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sIsSupportGa:Z

    if-eqz v7, :cond_0

    .line 492
    const/4 v7, 0x1

    iput-boolean v7, v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isProfileSet:Z

    .line 494
    const/4 v1, 0x0

    .line 495
    .local v1, "birthdayNumber":I
    const/4 v6, 0x0

    .line 498
    .local v6, "usBirthday":Ljava/lang/String;
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 499
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 505
    :goto_1
    iget-object v7, v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "ProfileGender"

    invoke-interface {v7, v8, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 506
    iget-object v7, v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "ProfileBirthday"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 509
    const v7, 0x2e635

    if-ne p0, v7, :cond_2

    .line 510
    const-string v3, "M"

    .line 518
    .local v3, "genderString":Ljava/lang/String;
    :goto_2
    :try_start_1
    iget-object v7, v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    const/4 v8, 0x2

    invoke-interface {v7, v8, v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;->setCustomDimension(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 525
    :goto_3
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v9, :cond_4

    .line 526
    const/4 v7, 0x0

    invoke-virtual {v6, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 532
    .local v0, "birthYear":Ljava/lang/String;
    :goto_4
    :try_start_2
    iget-object v7, v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    const/4 v8, 0x3

    invoke-interface {v7, v8, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;->setCustomDimension(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 538
    :goto_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 541
    .local v5, "profile":Ljava/lang/String;
    :try_start_3
    iget-object v7, v4, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mTracker:Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;

    const/4 v8, 0x4

    invoke-interface {v7, v8, v5}, Lcom/sec/android/app/shealth/common/utils/logging/service/IGoogleAnalyticsEasyTracker;->setCustomDimension(ILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 547
    :goto_6
    const-string v7, "LogManager"

    const-string v8, "Profile for GA logging is set successfully."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 500
    .end local v0    # "birthYear":Ljava/lang/String;
    .end local v3    # "genderString":Ljava/lang/String;
    .end local v5    # "profile":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 501
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 502
    const-string v6, "00000000"

    goto :goto_1

    .line 511
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    const v7, 0x2e636

    if-ne p0, v7, :cond_3

    .line 512
    const-string v3, "F"

    .restart local v3    # "genderString":Ljava/lang/String;
    goto :goto_2

    .line 514
    .end local v3    # "genderString":Ljava/lang/String;
    :cond_3
    const-string v3, "U"

    .restart local v3    # "genderString":Ljava/lang/String;
    goto :goto_2

    .line 519
    :catch_1
    move-exception v2

    .line 520
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v7, "LogManager"

    const-string v8, "Failed to set dimension (gender)."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 528
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    const-string v0, "0000"

    .restart local v0    # "birthYear":Ljava/lang/String;
    goto :goto_4

    .line 533
    :catch_2
    move-exception v2

    .line 534
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v7, "LogManager"

    const-string v8, "Failed to set dimension (birthYear)."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 542
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v5    # "profile":Ljava/lang/String;
    :catch_3
    move-exception v2

    .line 543
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v7, "LogManager"

    const-string v8, "Failed to set dimension (profile)."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6
.end method

.method private setRegistered()V
    .locals 2

    .prologue
    .line 1353
    const-string v0, "LogManager"

    const-string/jumbo v1, "setRegistered()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1354
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsRegistered:Z

    .line 1355
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->realStartSendLog()V

    .line 1356
    return-void
.end method

.method private startSendLog()V
    .locals 4

    .prologue
    .line 1058
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z

    if-nez v0, :cond_0

    .line 1059
    const-string v0, "LogManager"

    const-string v1, "Start Sending log"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1060
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isUploaderRunning:Z

    .line 1061
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1063
    :cond_0
    return-void
.end method


# virtual methods
.method public sendLogViaSpp(Ljava/lang/String;Ljava/lang/String;)I
    .locals 10
    .param p1, "activity"    # Ljava/lang/String;
    .param p2, "partialLog"    # Ljava/lang/String;

    .prologue
    .line 1070
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 1071
    .local v3, "timestampLong":J
    const-string v2, "706"

    .line 1072
    .local v2, "svcCode":Ljava/lang/String;
    const-string v1, "BIZ"

    .line 1073
    .local v1, "logType":Ljava/lang/String;
    const-string v8, "99.01.00"

    .line 1075
    .local v8, "appLogVersion":Ljava/lang/String;
    const-string v0, "LogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Send header= ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1076
    const-string v0, "LogManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Send body= ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1077
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mUserId:Ljava/lang/String;

    move-object v5, p1

    move-object v9, p2

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->sendLog(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public sendLogsViaSpp()V
    .locals 36

    .prologue
    .line 676
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->isARTWithKitkat()Z

    move-result v33

    if-eqz v33, :cond_1

    .line 813
    :cond_0
    :goto_0
    return-void

    .line 681
    :cond_1
    const/4 v11, 0x0

    .line 682
    .local v11, "fIn":Ljava/io/FileInputStream;
    const/16 v26, 0x0

    .line 683
    .local v26, "reader":Ljava/io/InputStreamReader;
    const/4 v5, 0x0

    .line 685
    .local v5, "bufReader":Ljava/io/BufferedReader;
    const/16 v30, 0x0

    .line 686
    .local v30, "target":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    .line 687
    .local v8, "dir":Ljava/io/File;
    if-nez v8, :cond_2

    .line 688
    const-string v33, "LogManager"

    const-string v34, "Not exist log directory"

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 692
    :cond_2
    new-instance v29, Ljava/text/SimpleDateFormat;

    const-string v33, "MMdd"

    sget-object v34, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v29

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 695
    .local v29, "sdFormat":Ljava/text/SimpleDateFormat;
    const/16 v33, 0x4

    move/from16 v0, v33

    new-array v7, v0, [Ljava/lang/String;

    .line 696
    .local v7, "dateStrings":[Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    const/16 v33, 0x4

    move/from16 v0, v16

    move/from16 v1, v33

    if-ge v0, v1, :cond_3

    .line 697
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->getMidnightTimeBefore(I)J

    move-result-wide v33

    invoke-static/range {v33 .. v34}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v33

    move-object/from16 v0, v29

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v7, v16

    .line 696
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 700
    :cond_3
    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v25, v0

    .line 701
    .local v25, "onefileName":[Ljava/lang/String;
    const/16 v33, 0x0

    const-string v34, "_servicelog_"

    aput-object v34, v25, v33

    .line 702
    new-instance v15, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;

    move-object/from16 v0, v25

    invoke-direct {v15, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;-><init>([Ljava/lang/String;)V

    .line 703
    .local v15, "fnFilter":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;
    invoke-virtual {v8, v15}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v14

    .line 704
    .local v14, "files":[Ljava/io/File;
    move-object v4, v14

    .local v4, "arr$":[Ljava/io/File;
    array-length v0, v4

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_2
    move/from16 v0, v17

    move/from16 v1, v19

    if-ge v0, v1, :cond_7

    aget-object v13, v4, v17

    .line 705
    .local v13, "file":Ljava/io/File;
    const/16 v18, 0x0

    .line 706
    .local v18, "isTarget":Z
    const/16 v16, 0x0

    :goto_3
    const/16 v33, 0x3

    move/from16 v0, v16

    move/from16 v1, v33

    if-ge v0, v1, :cond_5

    .line 707
    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v33

    aget-object v34, v7, v16

    invoke-virtual/range {v33 .. v34}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_4

    .line 708
    const-string v33, "LogManager"

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "   Deletion candidate = "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    const/16 v18, 0x1

    .line 706
    :cond_4
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 712
    :cond_5
    if-nez v18, :cond_6

    .line 713
    const-string v33, "LogManager"

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "       Deleted ! file : "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 704
    :cond_6
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 719
    .end local v13    # "file":Ljava/io/File;
    .end local v18    # "isTarget":Z
    :cond_7
    const/16 v16, 0x3

    move-object v6, v5

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .local v6, "bufReader":Ljava/io/BufferedReader;
    move-object/from16 v27, v26

    .end local v26    # "reader":Ljava/io/InputStreamReader;
    .local v27, "reader":Ljava/io/InputStreamReader;
    move-object v12, v11

    .end local v11    # "fIn":Ljava/io/FileInputStream;
    .local v12, "fIn":Ljava/io/FileInputStream;
    :goto_4
    if-lez v16, :cond_0

    .line 722
    const/16 v33, 0x0

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v35, v7, v16

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x5f

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string/jumbo v35, "servicelog"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x5f

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    aput-object v34, v25, v33

    .line 723
    new-instance v15, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;

    .end local v15    # "fnFilter":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;
    move-object/from16 v0, v25

    invoke-direct {v15, v0}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;-><init>([Ljava/lang/String;)V

    .line 724
    .restart local v15    # "fnFilter":Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogFilenameFilter;
    invoke-virtual {v8, v15}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v14

    .line 725
    array-length v0, v14

    move/from16 v33, v0

    const/16 v34, 0x1

    move/from16 v0, v33

    move/from16 v1, v34

    if-ne v0, v1, :cond_e

    .line 726
    const/16 v33, 0x0

    aget-object v30, v14, v33

    .line 727
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v31

    .line 728
    .local v31, "targetName":Ljava/lang/String;
    const-string v33, "LogManager"

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Target File name = "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    const/16 v33, 0x0

    aget-object v33, v25, v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->length()I

    move-result v33

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v34

    move-object/from16 v0, v31

    move/from16 v1, v33

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    .line 731
    .local v24, "number":Ljava/lang/String;
    :try_start_0
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_10

    move-result v21

    .line 732
    .local v21, "lineNum":I
    const/16 v33, -0x1

    move/from16 v0, v21

    move/from16 v1, v33

    if-ne v0, v1, :cond_10

    .line 736
    :try_start_1
    new-instance v11, Ljava/io/FileInputStream;

    move-object/from16 v0, v30

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_10

    .line 740
    .end local v12    # "fIn":Ljava/io/FileInputStream;
    .restart local v11    # "fIn":Ljava/io/FileInputStream;
    :goto_5
    if-nez v11, :cond_8

    move-object v5, v6

    .end local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    move-object/from16 v26, v27

    .line 719
    .end local v21    # "lineNum":I
    .end local v24    # "number":Ljava/lang/String;
    .end local v27    # "reader":Ljava/io/InputStreamReader;
    .end local v31    # "targetName":Ljava/lang/String;
    .restart local v26    # "reader":Ljava/io/InputStreamReader;
    :goto_6
    add-int/lit8 v16, v16, -0x1

    move-object v6, v5

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v6    # "bufReader":Ljava/io/BufferedReader;
    move-object/from16 v27, v26

    .end local v26    # "reader":Ljava/io/InputStreamReader;
    .restart local v27    # "reader":Ljava/io/InputStreamReader;
    move-object v12, v11

    .end local v11    # "fIn":Ljava/io/FileInputStream;
    .restart local v12    # "fIn":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .line 737
    .restart local v21    # "lineNum":I
    .restart local v24    # "number":Ljava/lang/String;
    .restart local v31    # "targetName":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 738
    .local v9, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_10

    move-object v11, v12

    .end local v12    # "fIn":Ljava/io/FileInputStream;
    .restart local v11    # "fIn":Ljava/io/FileInputStream;
    goto :goto_5

    .line 744
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    :cond_8
    :try_start_3
    const-string v33, "LogManager"

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Calculate and send data size of log file = "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    new-instance v26, Ljava/io/InputStreamReader;

    move-object/from16 v0, v26

    invoke-direct {v0, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_11

    .line 746
    .end local v27    # "reader":Ljava/io/InputStreamReader;
    .restart local v26    # "reader":Ljava/io/InputStreamReader;
    :try_start_4
    new-instance v5, Ljava/io/BufferedReader;

    move-object/from16 v0, v26

    invoke-direct {v5, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_12

    .line 748
    .end local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    const/16 v22, 0x0

    .line 749
    .local v22, "logSize":I
    const/16 v32, 0x0

    .line 750
    .local v32, "totalSize":I
    const/4 v3, 0x0

    .line 752
    .local v3, "addition":I
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    invoke-static/range {v33 .. v33}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v33

    if-eqz v33, :cond_9

    .line 753
    const-string v33, "LogManager"

    const-string v34, "User id exist, Addition the log size 10"

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    const/16 v3, 0xa

    .line 756
    :cond_9
    :goto_7
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v20

    .local v20, "line":Ljava/lang/String;
    if-eqz v20, :cond_a

    .line 757
    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->getBytes()[B

    move-result-object v33

    move-object/from16 v0, v33

    array-length v0, v0

    move/from16 v33, v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    add-int/lit8 v22, v33, 0x1e

    .line 758
    add-int v22, v22, v3

    .line 759
    add-int v32, v32, v22

    goto :goto_7

    .line 769
    :cond_a
    :try_start_6
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStreamReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_1

    .line 774
    :goto_8
    :try_start_7
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_1

    .line 779
    :goto_9
    :try_start_8
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_8} :catch_1

    .line 785
    .end local v20    # "line":Ljava/lang/String;
    :goto_a
    if-lez v22, :cond_c

    .line 786
    :try_start_9
    const-string v33, "007"

    move/from16 v0, v16

    move/from16 v1, v32

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogGenerator;->getDataSizeBody(II)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sendLogViaSpp(Ljava/lang/String;Ljava/lang/String;)I

    move-result v28

    .line 787
    .local v28, "resultCode":I
    if-gez v28, :cond_b

    .line 788
    const-string v33, "LogManager"

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Fail to send data size : errCode = "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 804
    .end local v3    # "addition":I
    .end local v22    # "logSize":I
    .end local v28    # "resultCode":I
    .end local v32    # "totalSize":I
    :catch_1
    move-exception v9

    .line 805
    .end local v21    # "lineNum":I
    .local v9, "e":Ljava/lang/NumberFormatException;
    :goto_b
    goto/16 :goto_6

    .line 770
    .end local v9    # "e":Ljava/lang/NumberFormatException;
    .restart local v3    # "addition":I
    .restart local v20    # "line":Ljava/lang/String;
    .restart local v21    # "lineNum":I
    .restart local v22    # "logSize":I
    .restart local v32    # "totalSize":I
    :catch_2
    move-exception v10

    .line 771
    .local v10, "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 775
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_3
    move-exception v10

    .line 776
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 780
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_4
    move-exception v10

    .line 781
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_a

    .line 763
    .end local v10    # "eSub":Ljava/io/IOException;
    .end local v20    # "line":Ljava/lang/String;
    :catch_5
    move-exception v9

    .line 764
    .local v9, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 769
    :try_start_b
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStreamReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7
    .catch Ljava/lang/NumberFormatException; {:try_start_b .. :try_end_b} :catch_1

    .line 774
    :goto_c
    :try_start_c
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8
    .catch Ljava/lang/NumberFormatException; {:try_start_c .. :try_end_c} :catch_1

    .line 779
    :goto_d
    :try_start_d
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_d} :catch_1

    goto :goto_a

    .line 780
    :catch_6
    move-exception v10

    .line 781
    .restart local v10    # "eSub":Ljava/io/IOException;
    :try_start_e
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 770
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_7
    move-exception v10

    .line 771
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 775
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_8
    move-exception v10

    .line 776
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V
    :try_end_e
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_e} :catch_1

    goto :goto_d

    .line 765
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_9
    move-exception v9

    .line 766
    .local v9, "e":Ljava/lang/NullPointerException;
    :try_start_f
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 769
    :try_start_10
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStreamReader;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_b
    .catch Ljava/lang/NumberFormatException; {:try_start_10 .. :try_end_10} :catch_1

    .line 774
    :goto_e
    :try_start_11
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_c
    .catch Ljava/lang/NumberFormatException; {:try_start_11 .. :try_end_11} :catch_1

    .line 779
    :goto_f
    :try_start_12
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_a
    .catch Ljava/lang/NumberFormatException; {:try_start_12 .. :try_end_12} :catch_1

    goto :goto_a

    .line 780
    :catch_a
    move-exception v10

    .line 781
    .restart local v10    # "eSub":Ljava/io/IOException;
    :try_start_13
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a

    .line 770
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_b
    move-exception v10

    .line 771
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 775
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_c
    move-exception v10

    .line 776
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V
    :try_end_13
    .catch Ljava/lang/NumberFormatException; {:try_start_13 .. :try_end_13} :catch_1

    goto :goto_f

    .line 768
    .end local v9    # "e":Ljava/lang/NullPointerException;
    .end local v10    # "eSub":Ljava/io/IOException;
    :catchall_0
    move-exception v33

    .line 769
    :try_start_14
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStreamReader;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d
    .catch Ljava/lang/NumberFormatException; {:try_start_14 .. :try_end_14} :catch_1

    .line 774
    :goto_10
    :try_start_15
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_e
    .catch Ljava/lang/NumberFormatException; {:try_start_15 .. :try_end_15} :catch_1

    .line 779
    :goto_11
    :try_start_16
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_f
    .catch Ljava/lang/NumberFormatException; {:try_start_16 .. :try_end_16} :catch_1

    .line 782
    :goto_12
    :try_start_17
    throw v33

    .line 770
    :catch_d
    move-exception v10

    .line 771
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_10

    .line 775
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_e
    move-exception v10

    .line 776
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_11

    .line 780
    .end local v10    # "eSub":Ljava/io/IOException;
    :catch_f
    move-exception v10

    .line 781
    .restart local v10    # "eSub":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_12

    .line 791
    .end local v10    # "eSub":Ljava/io/IOException;
    .restart local v28    # "resultCode":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v35, 0x0

    aget-object v35, v25, v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x0

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v23

    .line 792
    .local v23, "newTarget":Ljava/io/File;
    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 793
    move-object/from16 v30, v23

    .line 794
    const/16 v21, 0x0

    .line 799
    .end local v3    # "addition":I
    .end local v22    # "logSize":I
    .end local v23    # "newTarget":Ljava/io/File;
    .end local v28    # "resultCode":I
    .end local v32    # "totalSize":I
    :cond_c
    :goto_13
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->sendLines(Ljava/io/File;I)Z

    move-result v33

    if-eqz v33, :cond_d

    .line 800
    const-string v33, "LogManager"

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Success to send whole log of the file : "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x0

    aget-object v35, v25, v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 802
    :cond_d
    const-string v33, "LogManager"

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Fail to send whole log of the file : "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x0

    aget-object v35, v25, v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v33 .. v34}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_17
    .catch Ljava/lang/NumberFormatException; {:try_start_17 .. :try_end_17} :catch_1

    goto/16 :goto_6

    .line 809
    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .end local v11    # "fIn":Ljava/io/FileInputStream;
    .end local v21    # "lineNum":I
    .end local v24    # "number":Ljava/lang/String;
    .end local v26    # "reader":Ljava/io/InputStreamReader;
    .end local v31    # "targetName":Ljava/lang/String;
    .restart local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v12    # "fIn":Ljava/io/FileInputStream;
    .restart local v27    # "reader":Ljava/io/InputStreamReader;
    :cond_e
    const-string v34, "LogManager"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    array-length v0, v14

    move/from16 v33, v0

    if-nez v33, :cond_f

    const-string v33, "No sending log file"

    :goto_14
    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v35, " for "

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const/16 v35, 0x0

    aget-object v35, v25, v35

    move-object/from16 v0, v33

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    .end local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    move-object/from16 v26, v27

    .end local v27    # "reader":Ljava/io/InputStreamReader;
    .restart local v26    # "reader":Ljava/io/InputStreamReader;
    move-object v11, v12

    .end local v12    # "fIn":Ljava/io/FileInputStream;
    .restart local v11    # "fIn":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .end local v11    # "fIn":Ljava/io/FileInputStream;
    .end local v26    # "reader":Ljava/io/InputStreamReader;
    .restart local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v12    # "fIn":Ljava/io/FileInputStream;
    .restart local v27    # "reader":Ljava/io/InputStreamReader;
    :cond_f
    const-string v33, "Something inconsistent"

    goto :goto_14

    .line 804
    .restart local v24    # "number":Ljava/lang/String;
    .restart local v31    # "targetName":Ljava/lang/String;
    :catch_10
    move-exception v9

    move-object v5, v6

    .end local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    move-object/from16 v26, v27

    .end local v27    # "reader":Ljava/io/InputStreamReader;
    .restart local v26    # "reader":Ljava/io/InputStreamReader;
    move-object v11, v12

    .end local v12    # "fIn":Ljava/io/FileInputStream;
    .restart local v11    # "fIn":Ljava/io/FileInputStream;
    goto/16 :goto_b

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .end local v26    # "reader":Ljava/io/InputStreamReader;
    .restart local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v21    # "lineNum":I
    .restart local v27    # "reader":Ljava/io/InputStreamReader;
    :catch_11
    move-exception v9

    move-object v5, v6

    .end local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    move-object/from16 v26, v27

    .end local v27    # "reader":Ljava/io/InputStreamReader;
    .restart local v26    # "reader":Ljava/io/InputStreamReader;
    goto/16 :goto_b

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .restart local v6    # "bufReader":Ljava/io/BufferedReader;
    :catch_12
    move-exception v9

    move-object v5, v6

    .end local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    goto/16 :goto_b

    .end local v5    # "bufReader":Ljava/io/BufferedReader;
    .end local v11    # "fIn":Ljava/io/FileInputStream;
    .end local v26    # "reader":Ljava/io/InputStreamReader;
    .restart local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v12    # "fIn":Ljava/io/FileInputStream;
    .restart local v27    # "reader":Ljava/io/InputStreamReader;
    :cond_10
    move-object v5, v6

    .end local v6    # "bufReader":Ljava/io/BufferedReader;
    .restart local v5    # "bufReader":Ljava/io/BufferedReader;
    move-object/from16 v26, v27

    .end local v27    # "reader":Ljava/io/InputStreamReader;
    .restart local v26    # "reader":Ljava/io/InputStreamReader;
    move-object v11, v12

    .end local v12    # "fIn":Ljava/io/FileInputStream;
    .restart local v11    # "fIn":Ljava/io/FileInputStream;
    goto/16 :goto_13
.end method

.method sppBinding()V
    .locals 2

    .prologue
    .line 1308
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    if-nez v0, :cond_0

    .line 1309
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mHandler:Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;-><init>(Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager$LogHandler;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    .line 1311
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->bindService(Landroid/content/Context;)V

    .line 1312
    return-void
.end method

.method sppUnbinding()V
    .locals 2

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    if-eqz v0, :cond_0

    .line 1316
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSPPBinder:Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/logging/service/SppBinder;->unbindService(Landroid/content/Context;)V

    .line 1318
    :cond_0
    return-void
.end method

.method public unregistSPPReceiver()V
    .locals 2

    .prologue
    .line 1321
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsReceiverRegistered:Z

    if-nez v0, :cond_1

    .line 1322
    const-string v0, "LogManager"

    const-string v1, "BR is not registered yet, don\'t need to unregister BR."

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1333
    :cond_0
    :goto_0
    return-void

    .line 1326
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSppReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1327
    const-string v0, "LogManager"

    const-string v1, "Unregister SPP broadcast receiver"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1329
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSppReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1330
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mIsReceiverRegistered:Z

    .line 1331
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/utils/logging/service/LogManager;->mSppReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method
