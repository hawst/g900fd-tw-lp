.class Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;
.super Ljava/lang/Object;
.source "ExportDataActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/ExportDataActivity;->startExport()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

.field final synthetic val$calendar:Ljava/util/Calendar;

.field final synthetic val$formatter:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/ExportDataActivity;Ljava/text/DateFormat;Ljava/util/Calendar;)V
    .locals 0

    .prologue
    .line 460
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$formatter:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$calendar:Ljava/util/Calendar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 466
    :try_start_0
    new-instance v0, Lcom/sec/android/app/shealth/settings/ExportToCSV;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/settings/ExportToCSV;-><init>()V

    .line 467
    .local v0, "generate":Lcom/sec/android/app/shealth/settings/ExportToCSV;
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bg:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$1300(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 469
    const-string v1, "blood_glucose"

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    const v5, 0x7f09026b

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$formatter:Ljava/text/DateFormat;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$calendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/settings/ExportToCSV;->generateCSV(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)I

    .line 472
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->bp:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$1400(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 474
    const-string v1, "blood_pressure"

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    const v5, 0x7f0901b4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$formatter:Ljava/text/DateFormat;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$calendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/settings/ExportToCSV;->generateCSV(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)I

    .line 477
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->hr:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$1500(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 479
    const-string v1, "heart_rate"

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    const v5, 0x7f090022

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$formatter:Ljava/text/DateFormat;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$calendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/settings/ExportToCSV;->generateCSV(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)I

    .line 482
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/ExportDataActivity;->sr:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->access$1600(Lcom/sec/android/app/shealth/settings/ExportDataActivity;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 484
    const-string/jumbo v1, "stress"

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/ExportDataActivity;

    const v5, 0x7f090026

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/settings/ExportDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$formatter:Ljava/text/DateFormat;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/ExportDataActivity$8;->val$calendar:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/settings/ExportToCSV;->generateCSV(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    .end local v0    # "generate":Lcom/sec/android/app/shealth/settings/ExportToCSV;
    :cond_3
    :goto_0
    return-void

    .line 488
    :catch_0
    move-exception v1

    goto :goto_0
.end method
