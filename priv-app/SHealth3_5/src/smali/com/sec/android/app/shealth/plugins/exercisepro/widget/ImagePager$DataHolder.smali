.class Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;
.super Ljava/lang/Object;
.source "ImagePager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DataHolder"
.end annotation


# instance fields
.field public mBitmap:Landroid/graphics/Bitmap;

.field public mData:Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "data"    # Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;->mData:Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .line 257
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;->mBitmap:Landroid/graphics/Bitmap;

    .line 258
    return-void
.end method
