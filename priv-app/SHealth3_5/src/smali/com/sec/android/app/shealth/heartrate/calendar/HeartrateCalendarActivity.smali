.class public Lcom/sec/android/app/shealth/heartrate/calendar/HeartrateCalendarActivity;
.super Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;
.source "HeartrateCalendarActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarRefactoringActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getDaysStatuses(JJ)Ljava/util/TreeMap;
    .locals 7
    .param p1, "startMonthTime"    # J
    .param p3, "endMonthTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 35
    .local v1, "calendarDayInfoMap":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Ljava/lang/Long;Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;>;"
    invoke-static {p0}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/heartrate/common/HeartrateDatabaseHelper;->getCalendarDatas(JJ)Ljava/util/ArrayList;

    move-result-object v3

    .line 36
    .local v3, "heartrateDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;>;"
    if-eqz v3, :cond_0

    .line 37
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;

    .line 38
    .local v2, "each":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;-><init>()V

    .line 39
    .local v0, "calendarDayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;->getStartTime()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getStartDayToMillis(J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 42
    .end local v0    # "calendarDayInfo":Lcom/sec/android/app/shealth/framework/ui/calendar/CalendarDayInfo;
    .end local v2    # "each":Lcom/sec/android/app/shealth/heartrate/data/HeartrateData;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_0
    return-object v1
.end method
