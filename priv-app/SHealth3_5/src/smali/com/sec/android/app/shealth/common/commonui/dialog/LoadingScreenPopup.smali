.class public abstract Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
.super Landroid/app/Dialog;
.source "LoadingScreenPopup.java"


# instance fields
.field protected context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->context:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public abstract doAfterDialogIsShown()V
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    .line 49
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 50
    new-instance v8, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v8}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 51
    .local v8, "lpWindow":Landroid/view/WindowManager$LayoutParams;
    const/4 v1, 0x2

    iput v1, v8, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 52
    const v1, 0x3e99999a    # 0.3f

    iput v1, v8, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 54
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->requestWindowFeature(I)Z

    .line 55
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$layout;->loading_dialog:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setContentView(I)V

    .line 58
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->loading_popup_background:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 60
    .local v7, "dialogLayout":Landroid/widget/RelativeLayout;
    if-eqz v7, :cond_0

    .line 62
    invoke-virtual {v7, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 64
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setCancelable(Z)V

    .line 65
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$string;->loading_data:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setText(I)V

    .line 66
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->loading_img:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 67
    .local v9, "receivingView":Landroid/view/View;
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 68
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 69
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 70
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 71
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 72
    if-eqz v9, :cond_1

    .line 74
    invoke-virtual {v9, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 76
    :cond_1
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 87
    return-void
.end method

.method protected setText(I)V
    .locals 2
    .param p1, "resourceId"    # I

    .prologue
    .line 89
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->txt_receiving:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 90
    .local v0, "Tv":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    :cond_0
    return-void
.end method
