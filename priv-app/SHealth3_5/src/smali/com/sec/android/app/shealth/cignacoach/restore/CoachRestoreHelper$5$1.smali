.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$1;
.super Ljava/lang/Object;
.source "CoachRestoreHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;)V
    .locals 0

    .prologue
    .line 423
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->isBackup:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1200(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const-string v1, "BACKUP"

    # invokes: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->cignaBackupRestore(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Ljava/lang/String;)Z

    .line 432
    :goto_0
    return-void

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5$1;->this$1:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;

    iget-object v0, v0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper$5;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;

    const-string v1, "RESTORE"

    # invokes: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->cignaBackupRestore(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;->access$1100(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestoreHelper;Ljava/lang/String;)Z

    goto :goto_0
.end method
