.class Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;
.super Ljava/lang/Object;
.source "EditFavGridAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IconViewHolder"
.end annotation


# instance fields
.field private iconDelete:Landroid/widget/ImageView;

.field private iconImage:Landroid/widget/ImageView;

.field private iconText:Landroid/widget/TextView;

.field private imageContainer:Landroid/widget/RelativeLayout;

.field private thirdPartyIcon:Landroid/widget/ImageView;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const v0, 0x7f08033b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->imageContainer:Landroid/widget/RelativeLayout;

    .line 73
    const v0, 0x7f0804f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconText:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0804f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconImage:Landroid/widget/ImageView;

    .line 75
    const v0, 0x7f0804f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconDelete:Landroid/widget/ImageView;

    .line 76
    const v0, 0x7f08033d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->thirdPartyIcon:Landroid/widget/ImageView;

    .line 78
    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$1;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;-><init>(Landroid/view/View;)V

    return-void
.end method

.method private getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 161
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.shealth.action.STEALTH_MODE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 162
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 164
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    :try_start_0
    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/Intent;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 176
    :cond_0
    :goto_0
    return-object v0

    .line 169
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 172
    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02020b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private getAppIconDrawable(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 138
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    const-string v3, "home"

    const-string v4, "favorite"

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 142
    .local v2, "favIconString":Ljava/lang/String;
    :try_start_0
    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    .line 144
    :catch_0
    move-exception v1

    .line 146
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    if-nez v0, :cond_0

    .line 148
    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 150
    :cond_0
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method build(Ljava/lang/Object;)V
    .locals 10
    .param p1, "icon"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const v7, 0x7f09003f

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 82
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/home/data/IconData;

    .line 83
    .local v0, "iconData":Lcom/sec/android/app/shealth/home/data/IconData;
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconDelete:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f090035

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getIconType()I

    move-result v1

    if-ne v1, v9, :cond_0

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->imageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->thirdPartyIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconDelete:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconText:Landroid/widget/TextView;

    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconImage:Landroid/widget/ImageView;

    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201e1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconImage:Landroid/widget/ImageView;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;->TYPE_TOOL_TIP:Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;

    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils;->setHoverPopupListener(Landroid/view/View;Lcom/sec/android/app/shealth/framework/ui/hovering/HoverUtils$HoverWindowType;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconText:Landroid/widget/TextView;

    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconDelete:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconText:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconText:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppType()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppType()I

    move-result v1

    if-ne v1, v9, :cond_2

    .line 101
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->thirdPartyIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 108
    :goto_1
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppDisplayPlugInIcons()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 111
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v1

    if-lez v1, :cond_3

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppDisplayPlugInIcons()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->getAppIconDrawable(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 105
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->thirdPartyIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->imageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 117
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 122
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->iconImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/home/favorite/EditFavGridAdapter$IconViewHolder;->getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
