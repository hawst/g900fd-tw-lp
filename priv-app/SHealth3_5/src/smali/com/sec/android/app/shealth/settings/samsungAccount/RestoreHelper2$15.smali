.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0

    .prologue
    .line 1349
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 5
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/4 v4, 0x0

    .line 1353
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_2

    .line 1355
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1357
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1358
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 1359
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1361
    :cond_0
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InSide onDialogButtonClick currentStep: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1363
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_PROFILE_FOR_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    if-ne v0, v1, :cond_3

    .line 1365
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1367
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const/16 v3, 0x65

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->cancelRequest(Ljava/lang/Object;I)Z

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1902(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z

    .line 1369
    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InSide onDialogButtonClick mIsCancelRequestHandled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mIsCancelRequestHandled:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1900(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1371
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1373
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1374
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;

    .line 1383
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData2:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z

    .line 1384
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mMigrationState:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)I

    move-result v0

    if-nez v0, :cond_2

    .line 1386
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->finish()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V

    .line 1389
    :cond_2
    return-void

    .line 1378
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_DATA_TO_IMPORT:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    if-ne v0, v1, :cond_1

    .line 1380
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$15;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->cancel()V

    goto :goto_0
.end method
