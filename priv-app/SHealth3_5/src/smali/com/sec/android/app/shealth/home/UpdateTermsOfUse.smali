.class public Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "UpdateTermsOfUse.java"


# static fields
.field public static mUpdateTermsOfUse:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;


# instance fields
.field private isAgreeCheck:Z

.field private mAgreeCheck:Landroid/widget/CheckBox;

.field private mCheckLayout:Landroid/widget/RelativeLayout;

.field private mPrivacyPolicyValue:F

.field private mTermsOfUseValue:F

.field private nextButtonLayout:Landroid/widget/RelativeLayout;

.field private termsInInitialWithPP:Landroid/view/View;

.field private termsInInitialWithoutPP:Landroid/view/View;

.field private termsInSettings:Landroid/view/View;

.field private titleEraseData:Landroid/widget/TextView;

.field private titlePrivacyPolicy:Landroid/widget/TextView;

.field private titleTermsOfUse:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mUpdateTermsOfUse:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mCheckLayout:Landroid/widget/RelativeLayout;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mAgreeCheck:Landroid/widget/CheckBox;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->isAgreeCheck:Z

    .line 31
    iput v2, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mTermsOfUseValue:F

    .line 32
    iput v2, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mPrivacyPolicyValue:F

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titleTermsOfUse:Landroid/widget/TextView;

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titlePrivacyPolicy:Landroid/widget/TextView;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titleEraseData:Landroid/widget/TextView;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mAgreeCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->isAgreeCheck:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->isAgreeCheck:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mPrivacyPolicyValue:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mTermsOfUseValue:F

    return v0
.end method

.method private initLayout()V
    .locals 12

    .prologue
    const v11, 0x7f080a19

    const v10, 0x7f080a26

    const v9, 0x7f080a18

    const v8, 0x7f080a17

    const/4 v7, 0x0

    .line 99
    const v3, 0x7f030244

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->setContentView(I)V

    .line 101
    const v3, 0x7f080a0c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    .line 102
    const v3, 0x7f080a10

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    .line 104
    const v3, 0x7f080a14

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mCheckLayout:Landroid/widget/RelativeLayout;

    .line 105
    const v3, 0x7f080a15

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mAgreeCheck:Landroid/widget/CheckBox;

    .line 107
    const v3, 0x7f080613

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    .line 108
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0901f6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 110
    const v3, 0x7f080a16

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titleTermsOfUse:Landroid/widget/TextView;

    .line 111
    const v3, 0x7f08068e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titlePrivacyPolicy:Landroid/widget/TextView;

    .line 112
    const v3, 0x7f080693

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titleEraseData:Landroid/widget/TextView;

    .line 113
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isRtlLanguage()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titleTermsOfUse:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e85

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titlePrivacyPolicy:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e44

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->titleEraseData:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e6d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e94

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e95

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e96

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e97

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090ea0

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e94

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e95

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e96

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e97

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090ea0

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e94

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e95

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e96

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090e97

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "\u200f"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f090ea0

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v2, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 139
    .local v2, "systemLocale":Ljava/util/Locale;
    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "strLanguage":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "countryName":Ljava/lang/String;
    if-eqz v1, :cond_d

    const-string v3, "ko"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 143
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 146
    const v3, 0x7f080a0a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 147
    const v3, 0x7f080a0e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 156
    :goto_0
    if-eqz v1, :cond_1

    const-string v3, "gl"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "ca"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "eu"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    const-string v3, "es"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "US"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 158
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 167
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MA"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 169
    if-eqz v1, :cond_4

    const-string v3, "fr"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eba

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eba

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 178
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eba

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 184
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MX"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 186
    if-eqz v1, :cond_5

    const-string v3, "es"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 189
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 190
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eab

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eab

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 196
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 197
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 198
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eab

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 206
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "RU"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 208
    if-eqz v1, :cond_6

    const-string v3, "kk"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 210
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a16

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ead

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eae

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eaf

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 213
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a1c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb2

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a20

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb3

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a21

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 218
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 219
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 221
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a16

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ead

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 222
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eae

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 223
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eaf

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a1c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 226
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb2

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 227
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a20

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb3

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 228
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a21

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 229
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 230
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a16

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ead

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 233
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eae

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 234
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eaf

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 235
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 236
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a1c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb2

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 238
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a20

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb3

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a21

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 241
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090eb6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 247
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "DE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 249
    if-eqz v1, :cond_7

    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 251
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a16

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ebd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 252
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ebe

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 253
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 255
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a1c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec3

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 256
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 257
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a20

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a21

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a22

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec7

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a16

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ebd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 264
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ebe

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 265
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 266
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a1c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec3

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a20

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a21

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a22

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec7

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 272
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a16

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ebd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 276
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ebe

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 277
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 278
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a1a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 279
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a1c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec3

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 280
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a1f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a20

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 282
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a21

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 283
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a22

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec7

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a23

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ec9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 287
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a27

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 288
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a28

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 289
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a27

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a28

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a27

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 292
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a28

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 297
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AT"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 299
    if-eqz v1, :cond_8

    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 301
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 302
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 303
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 308
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "BE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 310
    if-eqz v1, :cond_9

    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecc

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecc

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 314
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecc

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 319
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CH"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 321
    if-eqz v1, :cond_a

    const-string v3, "de"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 323
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecc

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecc

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 325
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ecc

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 326
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 327
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 328
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a24

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 333
    :cond_a
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GB"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 335
    if-eqz v1, :cond_b

    const-string v3, "en"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    const-string v3, "US"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 337
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 338
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 339
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 340
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 341
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 342
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 344
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 345
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 346
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 351
    :cond_b
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AU"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 353
    if-eqz v1, :cond_c

    const-string v3, "en"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 356
    const-string v3, "US"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 357
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 358
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 359
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 360
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 361
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    const v4, 0x7f080a25

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 362
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 363
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 364
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 365
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f090ea6

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 370
    :cond_c
    return-void

    .line 149
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithoutPP:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInInitialWithPP:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->termsInSettings:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 152
    const v3, 0x7f080a0a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 153
    const v3, 0x7f080a0e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public closeScreen()V
    .locals 0

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->finish()V

    .line 385
    return-void
.end method

.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 395
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 398
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907eb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 399
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const-string v1, "#73b90f"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 401
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->finish()V

    .line 375
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    sput-object p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mUpdateTermsOfUse:Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "termOfUseVersion"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mTermsOfUseValue:F

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "privacyVersion"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mPrivacyPolicyValue:F

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->initLayout()V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mCheckLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$1;-><init>(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->mAgreeCheck:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$2;-><init>(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;->nextButtonLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/home/UpdateTermsOfUse$3;-><init>(Lcom/sec/android/app/shealth/home/UpdateTermsOfUse;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 389
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 390
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 379
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    .line 380
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 405
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 406
    return-void
.end method
