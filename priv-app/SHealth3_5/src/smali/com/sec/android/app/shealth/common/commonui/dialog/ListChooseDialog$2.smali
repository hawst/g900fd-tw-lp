.class Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$2;
.super Ljava/lang/Object;
.source "ListChooseDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "itemView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;

    move-result-object v0

    .line 210
    .local v0, "onSaveListener":Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    if-eqz v0, :cond_0

    .line 211
    invoke-interface {v0, p3}, Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;->onSave(I)V

    .line 212
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$2;->this$0:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->dismissAllowStateLoss()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)V

    .line 213
    return-void
.end method
