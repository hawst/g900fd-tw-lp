.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$9;
.super Ljava/lang/Object;
.source "ExerciseProDetailsMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setHeartRateMarkerOnMap()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V
    .locals 0

    .prologue
    .line 1104
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMarkerClick(Lcom/google/android/gms/maps/model/Marker;)Z
    .locals 6
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    const/4 v5, 0x1

    .line 1107
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getSnippet()Ljava/lang/String;

    move-result-object v0

    .line 1108
    .local v0, "snippet":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1110
    const-string v2, "HRM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1111
    const-string v2, "_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1112
    .local v1, "val":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 1113
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    aget-object v3, v1, v5

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->showHRdialog(I)V

    .line 1119
    .end local v1    # "val":[Ljava/lang/String;
    :cond_0
    :goto_0
    return v5

    .line 1117
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$9;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->getScreenPhotoList(I)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->show(Ljava/util/List;)V

    goto :goto_0
.end method
