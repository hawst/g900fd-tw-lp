.class public Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;
.super Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
.source "SpO2InformationAreaView.java"


# static fields
.field private static final DATE_SEPARATOR:Ljava/lang/String; = "/"

.field private static final DAY_CHAR:Ljava/lang/String; = "d"

.field private static final DAY_FORMAT_PATTERN:Ljava/lang/String; = "dd"

.field private static final MONTH_CHAR:Ljava/lang/String; = "M"

.field private static final MONTH_FORMAT_PATTERN:Ljava/lang/String; = "MM"

.field private static final REGEX_END_OF_LINE:Ljava/lang/String; = "$"

.field private static final REGEX_ONE_OR_MORE:Ljava/lang/String; = "+"

.field private static final TIME_FORMAT_12HOUR:Ljava/lang/String; = "h:mm a"

.field private static final TIME_FORMAT_24HOUR:Ljava/lang/String; = "HH:mm"

.field private static final YEAR_CHAR:Ljava/lang/String; = "y"

.field private static final YEAR_FORMAT_PATTERN:Ljava/lang/String; = "yyyy"


# instance fields
.field private dateFormat:Ljava/text/DateFormat;

.field private lHRMLayout:Landroid/widget/LinearLayout;

.field private lSpo2Layout:Landroid/widget/LinearLayout;

.field private mDateFormatOrder:[C

.field private mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

.field private mTimeFormatPattern:Ljava/lang/String;

.field private mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private tvBpm:Landroid/widget/TextView;

.field private tvBpmAvg:Landroid/widget/TextView;

.field private tvBpmHeader:Landroid/widget/TextView;

.field private tvBpmUnit:Landroid/widget/TextView;

.field private tvDate:Landroid/widget/TextView;

.field private tvSpO2:Landroid/widget/TextView;

.field private tvSpO2Avg:Landroid/widget/TextView;

.field private tvSpO2Header:Landroid/widget/TextView;

.field private tvSpO2Unit:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;-><init>(Landroid/content/Context;)V

    .line 62
    iput-object p2, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 63
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->dateFormat:Ljava/text/DateFormat;

    .line 64
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mDateFormatOrder:[C

    .line 65
    invoke-static {p1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "HH:mm"

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    .line 71
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->initLayout()V

    .line 72
    return-void

    .line 68
    :cond_0
    const-string v0, "h:mm a"

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    goto :goto_0
.end method

.method private convertDptoPx(I)I
    .locals 5
    .param p1, "dp"    # I

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 197
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 198
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v3, p1

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, v3, v4

    .line 199
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method

.method private initLayout()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 89
    const v0, 0x7f0809a6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->lSpo2Layout:Landroid/widget/LinearLayout;

    .line 90
    const v0, 0x7f0809ac

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->lHRMLayout:Landroid/widget/LinearLayout;

    .line 91
    const v0, 0x7f0809a5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvDate:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0809a7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Header:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0809a9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0809aa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Unit:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0809ab

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Avg:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0809ae

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpm:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0809ad

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmHeader:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0809af

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmUnit:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0809b0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmAvg:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0809a8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Unit:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Header:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Avg:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 117
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Unit:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Avg:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Header:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->lSpo2Layout:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v2, 0xce

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->convertDptoPx(I)I

    move-result v2

    invoke-direct {v1, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->lHRMLayout:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v2, 0x9a

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->convertDptoPx(I)I

    move-result v2

    invoke-direct {v1, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public dimInformationAreaView()V
    .locals 0

    .prologue
    .line 85
    return-void
.end method

.method protected initInformationAreaView()Landroid/view/View;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030224

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public refreshInformationAreaView()V
    .locals 0

    .prologue
    .line 81
    return-void
.end method

.method public setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 4
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 120
    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mDateFormatOrder:[C

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    .line 122
    .local v0, "dateFormatPattern":Ljava/lang/String;
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 124
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_1

    .line 125
    const-string v1, ""

    .line 130
    .local v1, "dayFormatPattern":Ljava/lang/String;
    :goto_0
    const-string v2, "d+"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    const-string v2, "M+"

    const-string v3, "MM/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    const-string/jumbo v2, "y+"

    const-string/jumbo v3, "yyyy/"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    const-string v2, "/$"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v2, :cond_0

    .line 140
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mTimeFormatPattern:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 143
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->dateFormat:Ljava/text/DateFormat;

    .line 144
    return-void

    .line 127
    .end local v1    # "dayFormatPattern":Ljava/lang/String;
    :cond_1
    const-string v1, "dd/"

    .restart local v1    # "dayFormatPattern":Ljava/lang/String;
    goto :goto_0
.end method

.method public update(Ljava/util/ArrayList;Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V
    .locals 10
    .param p2, "handlerUpdateDataManager"    # Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;",
            "Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "pointData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    const v9, 0x7f09006e

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/16 v5, 0x8

    const/4 v6, 0x0

    .line 149
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesXValue()Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "dateValue":Ljava/lang/String;
    const/4 v0, 0x0

    .line 152
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->SPO2:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/SupportUtils;->isMedical(Landroid/content/Context;Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 153
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Unit:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Header:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Avg:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 158
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2:Landroid/widget/TextView;

    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->floatValue()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    new-instance v0, Ljava/util/Date;

    .end local v0    # "date":Ljava/util/Date;
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 173
    .restart local v0    # "date":Ljava/util/Date;
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->setSelectedDateInChart(J)V

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvDate:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Header:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090240

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmHeader:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090f98

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Unit:Landroid/widget/TextView;

    const-string v4, "%"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmUnit:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900d2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Avg:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmAvg:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v4, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpm:Landroid/widget/TextView;

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->floatValue()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v3, v4, :cond_1

    .line 184
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Unit:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 185
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmUnit:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmAvg:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900d2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Avg:Landroid/widget/TextView;

    const-string v4, "%"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    :goto_1
    return-void

    .line 160
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Unit:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Avg:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Header:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setGraphAverageVisibility(Z)V

    .line 165
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setVisibility(I)V

    .line 166
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->getSeriesYValueList()Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->floatValue()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 167
    .local v2, "value":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->setIsFromGraphInfo(Z)V

    .line 168
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->mSmallBarMiddle:Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;

    int-to-double v4, v2

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/spo2/common/Spo2StateSummarySmallBar;->moveToPolygon(D)V

    goto/16 :goto_0

    .line 190
    .end local v2    # "value":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvBpmAvg:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090204

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v3, p0, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->tvSpO2Avg:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2InformationAreaView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090204

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
