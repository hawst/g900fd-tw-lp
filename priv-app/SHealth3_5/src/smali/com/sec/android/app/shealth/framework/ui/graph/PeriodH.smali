.class public final enum Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field public static final enum DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field public static final enum HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field public static final enum MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    const-string v1, "HOURS_IN_DAY"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    const-string v1, "DAYS_IN_MONTH"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    const-string v1, "MONTHS_IN_YEAR"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->$VALUES:[Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1

    const-class v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->$VALUES:[Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method
