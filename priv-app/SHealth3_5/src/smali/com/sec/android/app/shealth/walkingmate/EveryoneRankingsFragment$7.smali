.class Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;
.super Landroid/os/AsyncTask;
.source "EveryoneRankingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->loadWalkersListItemsIconsImages()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

.field final synthetic val$holder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    iput-object p2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->val$holder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1, "strings"    # [Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 583
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 592
    :cond_0
    :goto_0
    return-object v0

    .line 586
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "rank: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->val$holder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - doInBackground string: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    aget-object v1, p1, v4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 590
    aget-object v1, p1, v4

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/BitmapUtil;->getDrawableFromUrl(Ljava/lang/String;Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 592
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 580
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->doInBackground([Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 599
    if-eqz p1, :cond_0

    .line 600
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onPostExecute setImageMasking "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "rank: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->val$holder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 603
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->setImageMasking(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 604
    .local v0, "draw":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->val$holder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->setIconSrc(Landroid/graphics/drawable/Drawable;)V

    .line 605
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->val$holder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;->getRanking()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->updateRankingListItem(I)V

    .line 608
    .end local v0    # "draw":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 580
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$7;->onPostExecute(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
