.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;
.super Ljava/lang/Object;
.source "RealtimeServiceHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;


# instance fields
.field private mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

.field private mIsBound:Z

.field private mIsConnected:Z

.field private mRealtimeConnection:Landroid/content/ServiceConnection;

.field private mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mRealtimeConnection:Landroid/content/ServiceConnection;

    .line 29
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->TAG:Ljava/lang/String;

    const-string v1, "created"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;)Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mIsConnected:Z

    return p1
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;
    .locals 2

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->TAG:Ljava/lang/String;

    const-string v1, "DBManager getInstance"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->instance:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->instance:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    .line 43
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->instance:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;

    return-object v0
.end method


# virtual methods
.method public doBindRealtimeService(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isRun()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->initService(Landroid/content/Context;)V

    .line 61
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->TAG:Ljava/lang/String;

    const-string v1, "RealtimeHealthService is started"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mIsBound:Z

    if-nez v0, :cond_1

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mRealtimeConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mIsBound:Z

    .line 69
    :cond_1
    return-void
.end method

.method public doUnbindRealtimeService(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mIsBound:Z

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mRealtimeConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mIsBound:Z

    .line 77
    :cond_0
    return-void
.end method

.method public getRealtimeService()Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mRealtimeService:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    return-object v0
.end method

.method public setGpsTracking(Z)V
    .locals 0
    .param p1, "isTracking"    # Z

    .prologue
    .line 115
    return-void
.end method

.method public setHealSerivceListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mIsConnected:Z

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;->onServiceConnected()V

    .line 51
    :cond_0
    return-void
.end method

.method public unsetHealSerivceListener()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper;->mHealthListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeServiceHelper$IHealthServiecListener;

    .line 55
    return-void
.end method
