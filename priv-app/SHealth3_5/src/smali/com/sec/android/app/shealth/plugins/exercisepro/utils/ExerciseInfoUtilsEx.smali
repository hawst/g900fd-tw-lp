.class public Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseInfoUtilsEx;
.super Ljava/lang/Object;
.source "ExerciseInfoUtilsEx.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cursorToExerciseInfo(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;
    .locals 2
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 20
    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;-><init>()V

    .line 25
    .local v0, "exerciseInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;
    const-string/jumbo v1, "met"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->setMet(F)V

    .line 26
    const-string/jumbo v1, "name"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->setName(Ljava/lang/String;)V

    .line 27
    const-string/jumbo v1, "source_type"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->setSourceType(I)V

    .line 28
    const-string v1, "favorite"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->setFavorite(I)V

    .line 29
    const-string/jumbo v1, "sorting1"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->setSorting1(Ljava/lang/String;)V

    .line 30
    const-string/jumbo v1, "sorting2"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;->setSorting2(Ljava/lang/String;)V

    .line 34
    .end local v0    # "exerciseInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getExerciseInfoById(Landroid/content/Context;J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    const/4 v7, 0x0

    .line 48
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-object v7

    .line 52
    :cond_1
    const/4 v7, 0x0

    .line 53
    .local v7, "exerciseInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;
    const/4 v6, 0x0

    .line 56
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 57
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 58
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 59
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseInfoUtilsEx;->cursorToExerciseInfo(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 62
    :cond_2
    if-eqz v6, :cond_0

    .line 63
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 64
    const/4 v6, 0x0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 63
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 64
    const/4 v6, 0x0

    :cond_3
    throw v0
.end method

.method public static getExerciseInfoById(Landroid/content/Context;Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exercisedata"    # Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    .prologue
    .line 39
    if-nez p1, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 43
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getExerciseId()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseInfoUtilsEx;->getExerciseInfoById(Landroid/content/Context;J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExerciseInfoData;

    move-result-object v0

    goto :goto_0
.end method
