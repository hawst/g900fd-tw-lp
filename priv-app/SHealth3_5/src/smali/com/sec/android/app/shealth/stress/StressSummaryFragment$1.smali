.class Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;
.super Landroid/os/Handler;
.source "StressSummaryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    const-string v1, "Fragment is not currently added to the activity. Return without effects."

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :goto_0
    return-void

    .line 216
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 220
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mSecondCenterIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$000(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setReadyUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->access$100(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;I)V

    goto :goto_0

    .line 224
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setMeasuringUI(I)V

    goto :goto_0

    .line 228
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setMeasuringWarningUI(I)V

    goto :goto_0

    .line 231
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setMeasuringFailUI(I)V

    goto :goto_0

    .line 234
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setMeasuringEndUI()V

    goto :goto_0

    .line 237
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressSummaryFragment$1;->this$0:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->dataCollectingUI(II)V

    goto :goto_0

    .line 218
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
