.class Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$2;
.super Ljava/lang/Object;
.source "CustomGraphFragment.java"

# interfaces
.implements Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$2;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChartReadyToShow(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V
    .locals 2
    .param p1, "arg0"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .prologue
    const/4 v1, 0x1

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$2;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    # setter for: Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->mReadyToShown:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->access$002(Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;Z)Z

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$2;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getGraphSwitchButton()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$2;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getGraphSwitchButton()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment$2;->this$0:Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->getGraphSwitchButton()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 473
    :cond_0
    return-void
.end method
