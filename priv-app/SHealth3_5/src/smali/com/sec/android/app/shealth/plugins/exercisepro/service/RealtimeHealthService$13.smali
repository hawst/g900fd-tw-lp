.class Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;
.super Ljava/lang/Object;
.source "RealtimeHealthService.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)V
    .locals 0

    .prologue
    .line 2134
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 10
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v8, 0x0

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    const v7, 0x7f7fffff    # Float.MAX_VALUE

    .line 2138
    const/4 v2, 0x6

    if-ne p1, v2, :cond_a

    .line 2140
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GPSDataListener -> onReceived"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, p2

    .line 2141
    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 2142
    .local v1, "loco":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    if-eqz p3, :cond_5

    .line 2146
    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_b

    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_b

    .line 2147
    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mUsePedoState:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3302(Z)Z

    .line 2148
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsHandler:Landroid/os/Handler;

    const/16 v3, 0xbbf

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2156
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v2

    cmp-long v2, v2, v8

    if-nez v2, :cond_0

    .line 2157
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-wide v3, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3402(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J

    .line 2159
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2160
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    add-float/2addr v3, v4

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    .line 2161
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    add-float/2addr v3, v4

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineDistance:F

    .line 2162
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineTime:J

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineTime:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->declineTime:J

    .line 2163
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatDistance:F

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatDistance:F

    add-float/2addr v3, v4

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatDistance:F

    .line 2164
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatTime:J

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatTime:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->flatTime:J

    .line 2165
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    add-float/2addr v3, v4

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineDistance:F

    .line 2166
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineTime:J

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-wide v5, v5, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineTime:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->inclineTime:J

    .line 2167
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    add-float/2addr v3, v4

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    .line 2168
    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreDurationTime:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3600(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v4

    add-long/2addr v2, v4

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float v0, v2, v3

    .line 2169
    .local v0, "durationSec":F
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-object v3, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    div-float/2addr v3, v0

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    .line 2170
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxSpeed:F

    iget-object v3, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxSpeed:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 2171
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxSpeed:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxSpeed:F

    .line 2172
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    iget-object v3, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 2173
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->maxAltitude:F

    .line 2174
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    iget-object v3, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 2175
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->minAltitude:F

    .line 2178
    .end local v0    # "durationSec":F
    :cond_3
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsStartTime:J
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3700()J

    move-result-wide v2

    cmp-long v2, v2, v8

    if-nez v2, :cond_4

    .line 2179
    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsStartTime:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3702(J)J

    .line 2180
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3802(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 2183
    :cond_5
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-nez v2, :cond_6

    .line 2184
    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->latitude_sdk:Ljava/lang/Double;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3902(Ljava/lang/Double;)Ljava/lang/Double;

    .line 2185
    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->longitude_sdk:Ljava/lang/Double;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4002(Ljava/lang/Double;)Ljava/lang/Double;

    .line 2187
    :cond_6
    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->time_sdk:J
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4102(J)J

    .line 2189
    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->altitude:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk_check:Ljava/lang/Double;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4202(Ljava/lang/Double;)Ljava/lang/Double;

    .line 2190
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk_check:Ljava/lang/Double;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4200()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_7

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isAvailableBarometerFlag:Z
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$1900()Z

    move-result v2

    if-nez v2, :cond_c

    .line 2191
    :cond_7
    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4302(F)F

    .line 2195
    :goto_1
    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_d

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_d

    .line 2196
    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->accuracy_sdk:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4402(F)F

    .line 2200
    :goto_2
    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_e

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_e

    .line 2201
    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->speed_sdk:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4502(F)F

    .line 2205
    :goto_3
    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->bearing:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_8

    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->bearing:F

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_8

    .line 2206
    iget v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->bearing:F

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->bearing_sdk:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4602(F)F

    .line 2210
    :cond_8
    sget-boolean v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->SamsungLocationMonitor:Z

    if-eqz v2, :cond_9

    .line 2211
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_f

    .line 2212
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->totalDistance_sdk:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4702(F)F

    .line 2216
    :goto_4
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_10

    .line 2217
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->consumedCalorie_sdk:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4802(F)F

    .line 2221
    :goto_5
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_11

    .line 2222
    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->averageSpeed_sdk:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4902(F)F

    .line 2227
    :cond_9
    :goto_6
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsHandler:Landroid/os/Handler;

    const/16 v3, 0xbbe

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2229
    .end local v1    # "loco":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    :cond_a
    return-void

    .line 2152
    .restart local v1    # "loco":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    :cond_b
    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mUsePedoState:Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3302(Z)Z

    .line 2153
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGpsHandler:Landroid/os/Handler;

    const/16 v3, 0xbc0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 2193
    :cond_c
    iget-wide v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->altitude:D

    double-to-float v2, v2

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->altitude_sdk:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$4302(F)F

    goto/16 :goto_1

    .line 2198
    :cond_d
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "accuracy value is invalid ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->accuracy:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2203
    :cond_e
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "speed value is invalid ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->speed:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 2214
    :cond_f
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "totalDistance value is invalid ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->totalDistance:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 2219
    :cond_10
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "consumedCalorie value is invalid ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->consumedCalorie:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 2224
    :cond_11
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "averageSpeed value is invalid ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->extra:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;

    iget v4, v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location$Extra;->averageSpeed:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 0
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 2276
    return-void
.end method

.method public onStarted(II)V
    .locals 5
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 2234
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getGpsSensorDevice(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V

    .line 2236
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mExerciseId:J
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->insertExerciseActivityItem(Landroid/content/Context;JJ)J

    .line 2237
    return-void
.end method

.method public onStopped(II)V
    .locals 6
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    .line 2241
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GPSDataListener -> onStoped"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2242
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mWorkoutState:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)I

    move-result v1

    const/16 v2, 0x7d1

    if-ne v1, v2, :cond_5

    .line 2243
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    const/16 v2, 0x4654

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurWorkoutResult:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    iget v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    const/16 v2, 0x4655

    if-ne v1, v2, :cond_5

    .line 2245
    :cond_0
    const/16 v1, 0x8

    if-eq p2, v1, :cond_1

    if-ne p2, v3, :cond_5

    .line 2246
    :cond_1
    if-ne p2, v3, :cond_4

    .line 2247
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2248
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3502(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 2249
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v1

    cmp-long v1, v1, v4

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2250
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationData:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3800(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v2

    iget-wide v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->time:J

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3400(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    # += operator for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mPreDurationTime:J
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3614(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J

    .line 2252
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    const-wide/16 v2, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mCurrLocationStartTime:J
    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$3402(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;J)J

    .line 2254
    :cond_4
    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->isReadyGpsOn:Z
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$5002(Z)Z

    .line 2255
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService$13;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->mGPSDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeHealthService;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2272
    :cond_5
    :goto_0
    return-void

    .line 2257
    :catch_0
    move-exception v0

    .line 2259
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 2260
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 2262
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 2263
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 2265
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 2266
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 2268
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method
