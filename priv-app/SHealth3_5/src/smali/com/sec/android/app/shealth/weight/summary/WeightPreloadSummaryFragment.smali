.class public Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;
.super Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;
.source "WeightPreloadSummaryFragment.java"


# static fields
.field private static final SET_GOAL_ENABLE:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBmiColumn:Landroid/widget/LinearLayout;

.field private mDeviceConnectedStatusView:Landroid/view/View;

.field private mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

.field private mDivider:Landroid/view/View;

.field private mGoalLeftTextView:Landroid/widget/TextView;

.field private mHeightTextView:Landroid/widget/TextView;

.field private mHeightUnitTextView:Landroid/widget/TextView;

.field private mHeightView:Landroid/widget/LinearLayout;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field protected mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

.field private mUpdateButton:Landroid/widget/LinearLayout;

.field private mUpdateTimeTextView:Landroid/widget/TextView;

.field private mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

.field private mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

.field protected mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

.field private mlastDate:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    const-class v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;-><init>()V

    .line 111
    new-instance v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mServiceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;)Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method private addWeightDataFromProfile()V
    .locals 6

    .prologue
    .line 319
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 323
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getLastProfileUpdateTime()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 324
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getTimeWithoutSeconds(J)J

    move-result-wide v3

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(FJ)V

    .line 329
    .local v0, "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :goto_0
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setHeight(F)V

    .line 330
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getDao()Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 331
    return-void

    .line 326
    .end local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getLastProfileUpdateTime()J

    move-result-wide v3

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(FJ)V

    .restart local v0    # "data":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    goto :goto_0
.end method

.method private clearWeightData()V
    .locals 4

    .prologue
    const/4 v1, 0x4

    .line 569
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUpdateTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mGoalLeftTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    const/4 v1, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setWeightWithoutGoal(FFZ)V

    .line 573
    return-void
.end method

.method private getHeightInFtInch(FZ)Ljava/lang/String;
    .locals 12
    .param p1, "height"    # F
    .param p2, "isTalkback"    # Z

    .prologue
    const/4 v9, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/high16 v8, 0x41200000    # 10.0f

    .line 625
    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertInchToCm(F)F

    move-result v5

    .line 626
    .local v5, "valueInInch":F
    float-to-int v6, v5

    div-int/lit8 v0, v6, 0xc

    .line 627
    .local v0, "feet":I
    const/high16 v6, 0x41400000    # 12.0f

    rem-float v1, v5, v6

    .line 628
    .local v1, "inch":F
    if-nez v0, :cond_0

    const/high16 v6, 0x41000000    # 8.0f

    cmpg-float v6, v1, v6

    if-gez v6, :cond_0

    .line 629
    const/high16 v1, 0x41000000    # 8.0f

    .line 630
    :cond_0
    mul-float v6, v1, v8

    const/high16 v7, 0x3f000000    # 0.5f

    add-float/2addr v6, v7

    div-float/2addr v6, v8

    float-to-int v2, v6

    .line 632
    .local v2, "inches":I
    if-eqz p2, :cond_1

    .line 633
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901c0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901cd

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 641
    :goto_0
    return-object v4

    .line 637
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget-object v6, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 638
    .local v3, "language":Ljava/lang/String;
    const-string v6, "ar"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 639
    sget-object v6, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v7, "%1$d \' %2$d \""

    new-array v8, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 641
    :cond_2
    const-string v6, "%1$d \' %2$d \""

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private initializeConnectedDeviceView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 399
    const v0, 0x7f080020

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceConnectedStatusView:Landroid/view/View;

    .line 400
    return-void
.end method

.method private initializeDeviceFinder()V
    .locals 3

    .prologue
    .line 211
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceConnectedStatusView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getDeviceConnectionCheckListener(Landroid/view/View;)Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;

    move-result-object v0

    .line 212
    .local v0, "listener":Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;
    new-instance v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$2;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$2;-><init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;Landroid/content/Context;Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder$DeviceConnectionCheckListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    .line 219
    return-void
.end method

.method private initializeGoalView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 403
    const v0, 0x7f080c49

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mGoalLeftTextView:Landroid/widget/TextView;

    .line 404
    return-void
.end method

.method private initializeGraphButton(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 426
    const v0, 0x7f080c84

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$SummaryToGraphFragmentSwitchController;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils$SummaryToGraphFragmentSwitchController;-><init>(Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 428
    return-void
.end method

.method private initializeUIComponents(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 391
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeGoalView(Landroid/view/View;)V

    .line 392
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeUpdateButton(Landroid/view/View;)V

    .line 393
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeGraphButton(Landroid/view/View;)V

    .line 394
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeValueTextViews(Landroid/view/View;)V

    .line 395
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeConnectedDeviceView(Landroid/view/View;)V

    .line 396
    return-void
.end method

.method private initializeUnitHelper(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->NOT_DEFINED:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    if-ne v0, v1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->putWeightUnit(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getHeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->NOT_DEFINED:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    if-ne v0, v1, :cond_1

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    sget-object v1, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->CM_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->putHeightUnit(Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)V

    .line 229
    :cond_1
    return-void
.end method

.method private initializeUpdateButton(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v2, 0x7f080c83

    .line 407
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$4;-><init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 418
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUpdateButton:Landroid/widget/LinearLayout;

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUpdateButton:Landroid/widget/LinearLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090055

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUpdateButton:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 423
    return-void
.end method

.method private launchGoalActivity()V
    .locals 3

    .prologue
    .line 387
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x2713

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 388
    return-void
.end method

.method private setHeight(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)V
    .locals 10
    .param p1, "entryHeight"    # F
    .param p2, "heightUnit"    # Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    .prologue
    const v9, 0x7f090c93

    const v7, 0x7f090c92

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 593
    new-instance v4, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v0

    .line 594
    .local v0, "height":F
    const/4 v3, 0x0

    .line 595
    .local v3, "value":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->CM_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    if-ne p2, v4, :cond_3

    .line 597
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 598
    .local v2, "language":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v4, "ar"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 600
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isWholeNumber(F)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 610
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightUnitTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v4, v5, v3}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->setViewInfoValue(Landroid/widget/TextView;Landroid/view/View;Ljava/lang/String;)V

    .line 611
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightUnitTextView:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 612
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightView:Landroid/widget/LinearLayout;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901bf

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 622
    .end local v2    # "language":Ljava/lang/String;
    :goto_1
    return-void

    .line 600
    .restart local v2    # "language":Ljava/lang/String;
    :cond_0
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 606
    :cond_1
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isWholeNumber(F)Z

    move-result v4

    if-eqz v4, :cond_2

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {p0, v9, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    goto :goto_0

    :cond_2
    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {p0, v7, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 616
    .end local v2    # "language":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v0, v8}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getHeightInFtInch(FZ)Ljava/lang/String;

    move-result-object v1

    .line 617
    .local v1, "heightValue":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 618
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightUnitTextView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 619
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightView:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v6}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getHeightInFtInch(FZ)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private startAnimation()V
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->startIndicatorAnimation()V

    .line 677
    return-void
.end method

.method private stopAnimation()V
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->stopIndicatorAnimation()V

    .line 681
    return-void
.end method

.method private updateHeightValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V
    .locals 2
    .param p1, "weightData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .prologue
    .line 587
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getHeight()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getHeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->setHeight(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$HeightType;)V

    .line 588
    return-void
.end method

.method private updateUnitHelper()V
    .locals 3

    .prologue
    .line 766
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 767
    .local v0, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeightUnit()I

    move-result v1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeightUnit()I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->saveUnitforUnitHelper(II)V

    .line 768
    return-void
.end method

.method private updateUpdateTime(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V
    .locals 9
    .param p1, "weightData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .prologue
    const/4 v8, 0x0

    .line 576
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getLastWeightDataBefore(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 577
    .local v1, "latestSampleDate":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 578
    .local v0, "date":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 579
    .local v2, "time":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUpdateTimeTextView:Landroid/widget/TextView;

    const v4, 0x7f090cad

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 580
    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUpdateTimeTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 581
    return-void
.end method

.method private updateWeightIndicatorValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;Z)V
    .locals 15
    .param p1, "weightData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .param p2, "needAnimation"    # Z

    .prologue
    .line 506
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-interface {v11, v12, v13}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v2

    .line 508
    .local v2, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-eqz v2, :cond_0

    sget-object v11, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    sget-object v12, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->PRELOAD:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    invoke-virtual {v11, v12}, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    const/4 v4, 0x1

    .line 510
    .local v4, "isNeedShowGoal":Z
    :goto_0
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v9

    .line 511
    .local v9, "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v11

    invoke-static {v11, v9}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v10

    .line 513
    .local v10, "weightValue":F
    if-eqz v4, :cond_2

    .line 514
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v11

    invoke-static {v11, v9}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v3

    .line 516
    .local v3, "goalValue":F
    sub-float v1, v10, v3

    .line 517
    .local v1, "diffWeight":F
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    move/from16 v0, p2

    invoke-virtual {v11, v10, v3, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setWeightWithGoal(FFZ)V

    .line 519
    const/4 v11, 0x0

    invoke-static {v1, v11}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v11

    if-lez v11, :cond_1

    .line 520
    const v11, 0x7f090c96

    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 522
    .local v8, "strValue":Ljava/lang/String;
    const v11, 0x7f090ca3

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v8, v12, v13

    const/4 v13, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {p0, v11, v12}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 525
    .local v7, "strGoalLeftText":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mGoalLeftTextView:Landroid/widget/TextView;

    invoke-virtual {v11, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 526
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mGoalLeftTextView:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 547
    .end local v1    # "diffWeight":F
    .end local v3    # "goalValue":F
    .end local v7    # "strGoalLeftText":Ljava/lang/String;
    .end local v8    # "strValue":Ljava/lang/String;
    :goto_1
    return-void

    .line 508
    .end local v4    # "isNeedShowGoal":Z
    .end local v9    # "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .end local v10    # "weightValue":F
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 528
    .restart local v1    # "diffWeight":F
    .restart local v3    # "goalValue":F
    .restart local v4    # "isNeedShowGoal":Z
    .restart local v9    # "weightUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .restart local v10    # "weightValue":F
    :cond_1
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mGoalLeftTextView:Landroid/widget/TextView;

    const/4 v12, 0x4

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 531
    .end local v1    # "diffWeight":F
    .end local v3    # "goalValue":F
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v12

    invoke-interface {v11, v12, v13}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getSecondLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v11

    if-eqz v11, :cond_4

    .line 532
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v12

    invoke-interface {v11, v12, v13}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getSecondLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v6

    .line 534
    .local v6, "prevWeight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v11

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-lez v11, :cond_3

    .line 536
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v12

    invoke-interface {v11, v12, v13}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getSecondLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v11

    invoke-static {v11, v9}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v5

    .line 539
    .local v5, "prevValueInUnits":F
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    move/from16 v0, p2

    invoke-virtual {v11, v10, v5, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setWeightWithoutGoal(FFZ)V

    .line 545
    .end local v5    # "prevValueInUnits":F
    .end local v6    # "prevWeight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :cond_3
    :goto_2
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mGoalLeftTextView:Landroid/widget/TextView;

    const/4 v12, 0x4

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 543
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    const/high16 v12, -0x40800000    # -1.0f

    move/from16 v0, p2

    invoke-virtual {v11, v10, v12, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setWeightWithoutGoal(FFZ)V

    goto :goto_2
.end method


# virtual methods
.method protected clearMainView()V
    .locals 4

    .prologue
    .line 485
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 486
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getLastWeightData()Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v2

    .line 487
    .local v2, "weightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-eqz v2, :cond_0

    .line 489
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setWeight(F)V

    .line 490
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->save()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 500
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateMainView(Z)V

    .line 502
    return-void

    .line 491
    :catch_0
    move-exception v0

    .line 492
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->TAG:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 497
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->addWeightDataFromProfile()V

    goto :goto_0
.end method

.method protected deleteAllDataForDay()V
    .locals 5

    .prologue
    .line 471
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->deleteAllDataForDay()V

    .line 472
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    .line 473
    .local v0, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    if-eqz v0, :cond_0

    .line 474
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getCreateTime()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastDataForDay(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 475
    .local v1, "weightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-nez v1, :cond_0

    .line 476
    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;->deleteDataById(J)Z

    .line 481
    .end local v1    # "weightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    :cond_0
    return-void
.end method

.method public getCalendarActivityClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 207
    const-class v0, Lcom/sec/android/app/shealth/weight/WeightCalendarActivity;

    return-object v0
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 720
    const-string/jumbo v0, "sample_time"

    return-object v0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 715
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, 0x8

    .line 192
    new-instance v2, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    .line 193
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 194
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getContentViewId()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 195
    .local v1, "view":Landroid/view/View;
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeUnitHelper(Landroid/content/Context;)V

    .line 196
    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeUIComponents(Landroid/view/View;)V

    .line 197
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->initializeDeviceFinder()V

    .line 198
    sget-object v2, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->Weight:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;->NOT_MEDICAL_ONLY:Lcom/sec/android/app/shealth/common/config/FeatureTable$Weight;

    if-ne v2, v3, :cond_0

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mBmiColumn:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDivider:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 202
    :cond_0
    return-object v1
.end method

.method protected getContentViewId()I
    .locals 1

    .prologue
    .line 436
    const v0, 0x7f0302c3

    return v0
.end method

.method protected getDao()Lcom/sec/android/app/shealth/common/commondao/CommonDaoWithDateTime;
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    return-object v0
.end method

.method protected getDataForInputActivity(Ljava/util/Date;)Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;
    .locals 7
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 731
    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getWeightData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v4

    .line 733
    .local v4, "weightData":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-nez v4, :cond_0

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getLastWeightDataBefore(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 735
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getLastWeightDataBefore(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 736
    .local v0, "latestSampleDate":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getWeightData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v4

    .line 741
    .end local v0    # "latestSampleDate":Ljava/util/Date;
    :cond_0
    if-nez v4, :cond_2

    new-instance v5, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v3

    .line 742
    .local v3, "weight":F
    :goto_0
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/DateFormatHelper;->getCurrentTime(Ljava/util/Date;)J

    move-result-wide v1

    .line 743
    .local v1, "time":J
    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-gez v5, :cond_1

    .line 744
    const-wide/16 v1, 0x0

    .line 745
    :cond_1
    new-instance v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-direct {v5, v3, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(FJ)V

    return-object v5

    .line 741
    .end local v1    # "time":J
    .end local v3    # "weight":F
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v3

    goto :goto_0
.end method

.method protected getInputActivityClass()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 750
    const-class v0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    return-object v0
.end method

.method protected getLastWeightData()Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 3

    .prologue
    .line 663
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    return-object v0
.end method

.method protected getLastWeightDataBefore(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method protected getWeightData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 656
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastDataForDay(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    return-object v0
.end method

.method protected initializeValueTextViews(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 445
    const v0, 0x7f080c4c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightView:Landroid/widget/LinearLayout;

    .line 446
    const v0, 0x7f080c4d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightTextView:Landroid/widget/TextView;

    .line 447
    const v0, 0x7f080c4e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightUnitTextView:Landroid/widget/TextView;

    .line 448
    const v0, 0x7f080c48

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUpdateTimeTextView:Landroid/widget/TextView;

    .line 449
    const v0, 0x7f080c45

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    .line 450
    const v0, 0x7f080c43

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mBmiColumn:Landroid/widget/LinearLayout;

    .line 451
    const v0, 0x7f080c80

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDivider:Landroid/view/View;

    .line 452
    return-void
.end method

.method protected isDataForDateExist(Ljava/util/Date;)Z
    .locals 3
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 647
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getLastDataForDay(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 236
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->onCreate(Landroid/os/Bundle;)V

    .line 237
    sget-object v3, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    iget-boolean v3, v3, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->isAccessorySupported:Z

    if-eqz v3, :cond_0

    .line 238
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/weight/service/WeightService;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 239
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v5, 0x1

    invoke-virtual {v3, v1, v4, v5}, Landroid/support/v4/app/FragmentActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 240
    sget-object v3, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "onCreate(), bindService()"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    new-instance v4, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$3;-><init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->setDrawerMenuListener(Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;)V

    .line 262
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v3

    if-nez v3, :cond_1

    .line 264
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.shealth.SplashScreenActivity.restart"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 265
    .local v0, "i":Landroid/content/Intent;
    const-string/jumbo v3, "temps"

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/service/health/keyManager/KeyManager;->getRandomKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    const/high16 v3, 0x10000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 267
    const/high16 v3, 0x4000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 268
    const/high16 v3, 0x20000000

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 269
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->startActivity(Landroid/content/Intent;)V

    .line 271
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 284
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    new-instance v2, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 285
    .local v2, "weightDaoImplDb":Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;
    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->deleteAllGoals()I

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mlastDate:Ljava/util/Date;

    .line 290
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 374
    const v0, 0x7f100031

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 375
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 344
    sget-object v0, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->isAccessorySupported:Z

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    if-eqz v0, :cond_0

    .line 346
    sget-object v0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "onDestroy(), unbindService()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 348
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    .line 349
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->release()V

    .line 353
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightIndicator:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    .line 354
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    .line 356
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightView:Landroid/widget/LinearLayout;

    .line 357
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightTextView:Landroid/widget/TextView;

    .line 358
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mHeightUnitTextView:Landroid/widget/TextView;

    .line 359
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mUpdateTimeTextView:Landroid/widget/TextView;

    .line 360
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mGoalLeftTextView:Landroid/widget/TextView;

    .line 361
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceConnectedStatusView:Landroid/view/View;

    .line 362
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mBmiColumn:Landroid/widget/LinearLayout;

    .line 363
    iput-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDivider:Landroid/view/View;

    .line 364
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->onDestroy()V

    .line 365
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    if-eqz v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->clearListener()V

    .line 758
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->release()V

    .line 759
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    .line 762
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->onDestroyView()V

    .line 763
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 3
    .param p1, "hidden"    # Z

    .prologue
    .line 772
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->onHiddenChanged(Z)V

    .line 773
    if-nez p1, :cond_0

    .line 775
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 776
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 777
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setText(Ljava/util/Date;)V

    .line 778
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->setNextAndPrevDates()V

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment$5;-><init>(Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 788
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 379
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f080c99

    if-ne v0, v1, :cond_0

    .line 380
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->launchGoalActivity()V

    .line 381
    const/4 v0, 0x1

    .line 383
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->stopAnimation()V

    .line 336
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mlastDate:Ljava/util/Date;

    .line 338
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->onPause()V

    .line 339
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 295
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateUnitHelper()V

    .line 296
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getLastWeightData()Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v1

    if-nez v1, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->addWeightDataFromProfile()V

    .line 299
    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/weight/common/WeightConstants;->APP_TYPE:Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;

    iget-boolean v1, v1, Lcom/sec/android/app/shealth/weight/common/WeightAppTypes;->isAccessorySupported:Z

    if-eqz v1, :cond_1

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    if-eqz v1, :cond_1

    .line 301
    sget-object v1, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "onResume(), we call searchpairedDeviceList"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mWeightBinder:Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/weight/service/WeightService$WeightBinder;->startWeightService()V

    .line 305
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/shealth/weight/summary/HealthCareSummaryFragment;->onResume()V

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mlastDate:Ljava/util/Date;

    if-eqz v1, :cond_2

    .line 308
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v0

    .line 309
    .local v0, "mlatestDate":Ljava/util/Date;
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mlastDate:Ljava/util/Date;

    invoke-virtual {v1, v0}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 310
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateMainView(Z)V

    .line 311
    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mlastDate:Ljava/util/Date;

    .line 316
    .end local v0    # "mlatestDate":Ljava/util/Date;
    :cond_2
    return-void
.end method

.method protected resetViewInfo(Landroid/widget/TextView;Landroid/view/View;)V
    .locals 1
    .param p1, "valueTextView"    # Landroid/widget/TextView;
    .param p2, "unitView"    # Landroid/view/View;

    .prologue
    .line 702
    const v0, 0x7f090ca2

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 703
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 704
    return-void
.end method

.method protected setViewInfoValue(Landroid/widget/TextView;Landroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p1, "valueTextView"    # Landroid/widget/TextView;
    .param p2, "unitView"    # Landroid/view/View;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 691
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 692
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 693
    return-void
.end method

.method protected updateMainView(Z)V
    .locals 2
    .param p1, "needAnimation"    # Z

    .prologue
    .line 461
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->mDeviceFinder:Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/ConnectedDeviceFinder;->updateDeviceConnectionStatus()V

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getLastWeightDataBefore(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateWeightData(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;Z)V

    .line 466
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->startAnimation()V

    .line 467
    return-void
.end method

.method protected updateWeightData(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;Z)V
    .locals 2
    .param p1, "weightData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .param p2, "needAnimation"    # Z

    .prologue
    .line 553
    if-eqz p1, :cond_1

    .line 554
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateWeightIndicatorValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;Z)V

    .line 555
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateUpdateTime(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V

    .line 556
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateHeightValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V

    .line 566
    :cond_0
    :goto_0
    return-void

    .line 559
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->getLastWeightData()Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    .line 560
    .local v0, "weightD":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    if-eqz v0, :cond_0

    .line 562
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateWeightIndicatorValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;Z)V

    .line 563
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/WeightPreloadSummaryFragment;->updateHeightValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)V

    goto :goto_0
.end method
