.class Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;
.super Ljava/lang/Object;
.source "SearchEditText.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->init(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->access$000(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->access$100(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Landroid/widget/EditText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->access$100(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mOnSearchKeyClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->access$200(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$1;->this$0:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->mOnSearchKeyClickListener:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->access$200(Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;)Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;->onSearchClick()V

    .line 105
    :cond_0
    return-void
.end method
