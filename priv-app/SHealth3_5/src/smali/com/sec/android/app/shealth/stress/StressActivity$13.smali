.class Lcom/sec/android/app/shealth/stress/StressActivity$13;
.super Ljava/lang/Object;
.source "StressActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressActivity;->animate(Landroid/widget/ImageView;Landroid/widget/ImageView;[I[Ljava/lang/String;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

.field final synthetic val$imageView1:Landroid/widget/ImageView;

.field final synthetic val$imageView2:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressActivity;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 641
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$13;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/stress/StressActivity$13;->val$imageView2:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/sec/android/app/shealth/stress/StressActivity$13;->val$imageView1:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 643
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$13;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/stress/StressActivity;->animationFadeIn:Landroid/view/animation/AnimationSet;

    if-ne p1, v0, :cond_1

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$13;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/stress/StressActivity;->isImage1Found:Z

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$13;->val$imageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 647
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$13;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/stress/StressActivity;->isImage2Found:Z

    if-eqz v0, :cond_1

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$13;->val$imageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 651
    :cond_1
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 654
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 657
    return-void
.end method
