.class public Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;
.source "WeightInputActivity.java"


# instance fields
.field private mComment:Ljava/lang/String;

.field protected mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

.field private mId:J

.field private mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

.field private mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getContentView()Landroid/view/View;
    .locals 5

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "WEIGHT_ID_KEY"

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mId:J

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MODE_KEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mMode:Ljava/lang/String;

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "COMMENT_KEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mComment:Ljava/lang/String;

    .line 65
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0302b7

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f08053a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "TIME_DATE_KEY"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    const v1, 0x7f08053c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "weight"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    return-object v0
.end method

.method protected isInputChanged()Z
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mComment:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/InputActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v0, 0x7f0803e8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mComment:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setSelection(I)V

    .line 51
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 58
    return-void
.end method

.method protected onSaveButtonSelect(Z)V
    .locals 3
    .param p1, "isAdd"    # Z

    .prologue
    .line 75
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->setComment(Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->insertOrUpdateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    .line 77
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/weight/log/WeightLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 79
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/log/WeightInputActivity;->startActivity(Landroid/content/Intent;)V

    .line 80
    return-void
.end method
