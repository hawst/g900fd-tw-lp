.class final Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorConstants;
.super Ljava/lang/Object;
.source "WeightIndicatorConstants.java"


# static fields
.field static final ANGLE_DIRECTION:I = -0x1

.field static final ANGLE_PER_DIALS:F = 1.0f

.field static final COUNT_OF_DIALS:I = 0x10e

.field static final COUNT_OF_VISIBLE_DIALS_HALF:I = 0x18

.field static final DIALS_STEP:F = 0.1f

.field static final DRAW_AREA_ANGLE:I = 0x10e

.field static final MAX_ALPHA:I = 0xff

.field static final NO_DATA:F = -1.0f

.field static final TEXT_FAMILY_ROBOTO_LIGHT:Ljava/lang/String; = "Roboto-Light"

.field static final WEIGHT_DIALS_VIEW_ANIMATION_PROPERTY_NAME:Ljava/lang/String; = "rotation"

.field static final WEIGHT_GOAL_ALPHA_VIEW_ANIMATION_PROPERTY_NAME:Ljava/lang/String; = "paintAlpha"

.field static final WEIGHT_GOAL_RANGE_VIEW_ANIMATION_PROPERTY_NAME:Ljava/lang/String; = "rangeAngleDiff"

.field static final WEIGHT_GOAL_VIEW_ANIMATION_PROPERTY_NAME:Ljava/lang/String; = "arcAngle"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method
