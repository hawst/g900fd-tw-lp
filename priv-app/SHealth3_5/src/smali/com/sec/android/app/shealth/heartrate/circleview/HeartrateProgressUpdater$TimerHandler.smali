.class Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$TimerHandler;
.super Landroid/os/Handler;
.source "HeartrateProgressUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$TimerHandler;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$1;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$TimerHandler;-><init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 91
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$TimerHandler;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater$TimerHandler;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->counter:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->access$200(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->onProgressUpdate(ZF)V

    .line 93
    return-void
.end method
