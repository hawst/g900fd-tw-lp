.class Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->doMigrationStartFrom2xto25()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 545
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mRestoreHelper:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    move-result-object v1

    const v2, 0x7f090804

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->updateProgressPopupText(I)V

    .line 547
    invoke-static {}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->getInstance()Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/cp/datamigration/DataMigrationManager;->migrateLocalDataFrom2xto25(Landroid/content/Context;Lcom/sec/android/service/health/cp/datamigration/IMigrationStatusListener;)Z

    move-result v0

    .line 574
    .local v0, "result":Z
    if-nez v0, :cond_0

    .line 575
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "MIGRATION - migrateLocalDataFrom2xto25 Failed:"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->closeProgressPopupAsStatus()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)V

    .line 577
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mPopupHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$800(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$11;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 584
    :cond_0
    return-void
.end method
