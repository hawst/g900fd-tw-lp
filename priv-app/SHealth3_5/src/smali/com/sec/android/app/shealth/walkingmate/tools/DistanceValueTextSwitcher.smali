.class public Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;
.super Landroid/widget/TextSwitcher;
.source "DistanceValueTextSwitcher.java"

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# static fields
.field private static final ANIMATION_DURATION_LONG:J = 0x190L

.field private static final ANIMATION_DURATION_SHORT:J = 0xc8L

.field private static final ANIMATION_IN_OFFSET_VALUE:F = 0.15f

.field private static final ANIMATION_OUT_OFFSET_VALUE:F = 0.25f

.field private static final AVERAGE_TEXT_SIZE_DIMEN_ID:I = 0x7f0a0a8e

.field private static final AVG_TEXT_SIZE_DIGITS_QUANTITY:I = 0xc

.field private static final DEFAULT_TEXT_SIZE_DIMEN_ID:I = 0x7f0a0a8d

.field private static final MAXIMAL_ALLOWED_DISTANCE_VALUE:J = 0x2386f26fc0ffffL

.field private static final MINIMAL_TEXT_SIZE_DIMEN_ID:I = 0x7f0a0a8f

.field private static final MIN_TEXT_SIZE_DIGITS_QUANTITY:J = 0xfL


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/widget/TextSwitcher;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;->init()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/TextSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;->init()V

    .line 51
    return-void
.end method

.method private init()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;->initDistanceValueSwitchAnimation()V

    .line 55
    invoke-virtual {p0, p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    .line 56
    return-void
.end method

.method private initDistanceValueSwitchAnimation()V
    .locals 14

    .prologue
    .line 68
    new-instance v11, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v11, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 69
    .local v11, "botCurAnimation":Landroid/view/animation/AnimationSet;
    const-wide/16 v2, 0xc8

    invoke-virtual {v11, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 70
    new-instance v10, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v10, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 71
    .local v10, "botCurAlpha":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 72
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v10, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 73
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const v6, 0x3e19999a    # 0.15f

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 75
    .local v0, "botCurMove":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 76
    invoke-virtual {v11, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 77
    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 79
    new-instance v13, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v13, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 80
    .local v13, "curTopAnimation":Landroid/view/animation/AnimationSet;
    new-instance v12, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-direct {v12, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 81
    .local v12, "curTopAlpha":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v12, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 82
    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v12, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 83
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/high16 v9, -0x41800000    # -0.25f

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 85
    .local v1, "curTopMove":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 86
    invoke-virtual {v13, v12}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 87
    invoke-virtual {v13, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 89
    invoke-virtual {p0, v11}, Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 90
    invoke-virtual {p0, v13}, Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 91
    return-void
.end method


# virtual methods
.method public makeView()Landroid/view/View;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 61
    .local v0, "textView":Landroid/widget/TextView;
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/tools/DistanceValueTextSwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 63
    return-object v0
.end method
