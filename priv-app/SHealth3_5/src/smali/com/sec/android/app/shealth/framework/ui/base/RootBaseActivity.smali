.class public Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;
.super Landroid/support/v4/app/FragmentActivity;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/IBuildCacheCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$CustomMediaScanner;
    }
.end annotation


# static fields
.field protected static final CANCEL_PIN_REQUEST_CODE:I = 0x457

.field private static MEDIA_FORMAT:Ljava/lang/String; = null

.field protected static PLUG_IN_WIDGET_BORADCAST:Ljava/lang/String; = null

.field public static final PRIVATE_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION:I = 0x2000

.field public static final PRIVATE_FLAG_SOFT_INPUT_ADJUST_RESIZE_FULLSCREEN:I = 0x1000

.field private static final SCALED_BITMAP_HEIGHT:I = 0x280

.field private static final SCALED_BITMAP_WIDTH:I = 0x1e0

.field private static TAG:Ljava/lang/String;

.field public static WIDGET_LAUNCH_PASSWORD:Ljava/lang/String;

.field public static requestPassword:Z

.field public static sIsDownloadedPlugin:Z

.field private static securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field public static startedAnotherActivity:Z


# instance fields
.field protected cancelPinRequest:I

.field protected mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

.field private mBackGroundView:Landroid/view/View;

.field protected mContent:Landroid/view/View;

.field private mFragmentRootLayout:Landroid/view/ViewGroup;

.field protected mIsActivityPaused:Z

.field protected mIsShareViaRunning:Z

.field private mOldFragmentView:Landroid/view/View;

.field private mScaledBitmap:Landroid/graphics/Bitmap;

.field private mShareViewBmp:Landroid/graphics/Bitmap;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private mTitleBmp:Landroid/graphics/Bitmap;

.field private screenViewsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 27

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, -0x26af

    aput v11, v1, v10

    const/16 v10, -0x44

    aput v10, v1, v9

    const/16 v9, -0x52b0

    aput v9, v1, v8

    const/16 v8, -0x39

    aput v8, v1, v7

    const/16 v7, -0x4ec4

    aput v7, v1, v6

    const/16 v6, -0x2c

    aput v6, v1, v5

    const/16 v5, -0x1c

    aput v5, v1, v4

    const/16 v4, -0x79c5

    aput v4, v1, v3

    const/16 v3, -0x15

    aput v3, v1, v2

    const/16 v2, 0x631

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x26ca

    aput v12, v0, v11

    const/16 v11, -0x27

    aput v11, v0, v10

    const/16 v10, -0x52e0

    aput v10, v0, v9

    const/16 v9, -0x53

    aput v9, v0, v8

    const/16 v8, -0x4eed

    aput v8, v0, v7

    const/16 v7, -0x4f

    aput v7, v0, v6

    const/16 v6, -0x7d

    aput v6, v0, v5

    const/16 v5, -0x79a6

    aput v5, v0, v4

    const/16 v4, -0x7a

    aput v4, v0, v3

    const/16 v3, 0x658

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->MEDIA_FORMAT:Ljava/lang/String;

    const/16 v0, 0x18

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, -0xc

    aput v25, v1, v24

    const/16 v24, 0x7416

    aput v24, v1, v23

    const/16 v23, 0x1315

    aput v23, v1, v22

    const/16 v22, -0x5090

    aput v22, v1, v21

    const/16 v21, -0x35

    aput v21, v1, v20

    const/16 v20, -0x45

    aput v20, v1, v19

    const/16 v19, -0x6a

    aput v19, v1, v18

    const/16 v18, -0x26

    aput v18, v1, v17

    const/16 v17, -0x50

    aput v17, v1, v16

    const/16 v16, 0x2c6e

    aput v16, v1, v15

    const/16 v15, 0x6358

    aput v15, v1, v14

    const/16 v14, -0x46fa

    aput v14, v1, v13

    const/16 v13, -0x22

    aput v13, v1, v12

    const/16 v12, 0x2077

    aput v12, v1, v11

    const/16 v11, -0x32b7

    aput v11, v1, v10

    const/16 v10, -0x46

    aput v10, v1, v9

    const/16 v9, -0x2dae

    aput v9, v1, v8

    const/16 v8, -0x44

    aput v8, v1, v7

    const/16 v7, -0x56e3

    aput v7, v1, v6

    const/16 v6, -0xa

    aput v6, v1, v5

    const/16 v5, -0x5a

    aput v5, v1, v4

    const/16 v4, -0x9

    aput v4, v1, v3

    const/16 v3, -0x34

    aput v3, v1, v2

    const/16 v2, -0x7e

    aput v2, v1, v0

    const/16 v0, 0x18

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, -0x80

    aput v26, v0, v25

    const/16 v25, 0x7465

    aput v25, v0, v24

    const/16 v24, 0x1374

    aput v24, v0, v23

    const/16 v23, -0x50ed

    aput v23, v0, v22

    const/16 v22, -0x51

    aput v22, v0, v21

    const/16 v21, -0x26

    aput v21, v0, v20

    const/16 v20, -0x7

    aput v20, v0, v19

    const/16 v19, -0x58

    aput v19, v0, v18

    const/16 v18, -0x2e

    aput v18, v0, v17

    const/16 v17, 0x2c31

    aput v17, v0, v16

    const/16 v16, 0x632c

    aput v16, v0, v15

    const/16 v15, -0x469d

    aput v15, v0, v14

    const/16 v14, -0x47

    aput v14, v0, v13

    const/16 v13, 0x2013

    aput v13, v0, v12

    const/16 v12, -0x32e0

    aput v12, v0, v11

    const/16 v11, -0x33

    aput v11, v0, v10

    const/16 v10, -0x2df3

    aput v10, v0, v9

    const/16 v9, -0x2e

    aput v9, v0, v8

    const/16 v8, -0x568c

    aput v8, v0, v7

    const/16 v7, -0x57

    aput v7, v0, v6

    const/16 v6, -0x3f

    aput v6, v0, v5

    const/16 v5, -0x7e

    aput v5, v0, v4

    const/16 v4, -0x60

    aput v4, v0, v3

    const/16 v3, -0xe

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v3, v0

    if-lt v2, v3, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v3, v0

    if-lt v2, v3, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->PLUG_IN_WIDGET_BORADCAST:Ljava/lang/String;

    const/16 v0, 0x16

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x693c

    aput v23, v1, v22

    const/16 v22, 0x5e1b

    aput v22, v1, v21

    const/16 v21, 0x631

    aput v21, v1, v20

    const/16 v20, -0x2d8f

    aput v20, v1, v19

    const/16 v19, -0x5f

    aput v19, v1, v18

    const/16 v18, -0x2a

    aput v18, v1, v17

    const/16 v17, -0x57aa

    aput v17, v1, v16

    const/16 v16, -0x28

    aput v16, v1, v15

    const/16 v15, 0x5338

    aput v15, v1, v14

    const/16 v14, 0x253b

    aput v14, v1, v13

    const/16 v13, -0x4eba

    aput v13, v1, v12

    const/16 v12, -0x21

    aput v12, v1, v11

    const/16 v11, -0x7bb3

    aput v11, v1, v10

    const/16 v10, -0x1b

    aput v10, v1, v9

    const/16 v9, -0x10ce

    aput v9, v1, v8

    const/16 v8, -0x50

    aput v8, v1, v7

    const/16 v7, -0x1a

    aput v7, v1, v6

    const/16 v6, -0x67

    aput v6, v1, v5

    const/16 v5, 0x5306

    aput v5, v1, v4

    const/16 v4, -0x5c9

    aput v4, v1, v3

    const/16 v3, -0x6d

    aput v3, v1, v2

    const/16 v2, -0xe

    aput v2, v1, v0

    const/16 v0, 0x16

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x6958

    aput v24, v0, v23

    const/16 v23, 0x5e69

    aput v23, v0, v22

    const/16 v22, 0x65e

    aput v22, v0, v21

    const/16 v21, -0x2dfa

    aput v21, v0, v20

    const/16 v20, -0x2e

    aput v20, v0, v19

    const/16 v19, -0x5b

    aput v19, v0, v18

    const/16 v18, -0x57c9

    aput v18, v0, v17

    const/16 v17, -0x58

    aput v17, v0, v16

    const/16 v16, 0x5367

    aput v16, v0, v15

    const/16 v15, 0x2553

    aput v15, v0, v14

    const/16 v14, -0x4edb

    aput v14, v0, v13

    const/16 v13, -0x4f

    aput v13, v0, v12

    const/16 v12, -0x7bc8

    aput v12, v0, v11

    const/16 v11, -0x7c

    aput v11, v0, v10

    const/16 v10, -0x10a2

    aput v10, v0, v9

    const/16 v9, -0x11

    aput v9, v0, v8

    const/16 v8, -0x6e

    aput v8, v0, v7

    const/4 v7, -0x4

    aput v7, v0, v6

    const/16 v6, 0x5361

    aput v6, v0, v5

    const/16 v5, -0x5ad

    aput v5, v0, v4

    const/4 v4, -0x6

    aput v4, v0, v3

    const/16 v3, -0x7b

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v3, v0

    if-lt v2, v3, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v3, v0

    if-lt v2, v3, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->WIDGET_LAUNCH_PASSWORD:Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->startedAnotherActivity:Z

    const/16 v0, 0x10

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, -0x43

    aput v17, v1, v16

    const/16 v16, -0x40

    aput v16, v1, v15

    const/16 v15, -0x52

    aput v15, v1, v14

    const/16 v14, -0x1298

    aput v14, v1, v13

    const/16 v13, -0x7c

    aput v13, v1, v12

    const/16 v12, -0x63

    aput v12, v1, v11

    const/16 v11, -0x7bf4

    aput v11, v1, v10

    const/16 v10, -0x3b

    aput v10, v1, v9

    const/16 v9, -0x10

    aput v9, v1, v8

    const/16 v8, 0x5344

    aput v8, v1, v7

    const/16 v7, 0x3f32

    aput v7, v1, v6

    const/16 v6, -0x2083

    aput v6, v1, v5

    const/16 v5, -0x55

    aput v5, v1, v4

    const/16 v4, -0x39cc

    aput v4, v1, v3

    const/16 v3, -0x57

    aput v3, v1, v2

    const/16 v2, -0x65e4

    aput v2, v1, v0

    const/16 v0, 0x10

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, -0x3c

    aput v18, v0, v17

    const/16 v17, -0x4c

    aput v17, v0, v16

    const/16 v16, -0x39

    aput v16, v0, v15

    const/16 v15, -0x12e2

    aput v15, v0, v14

    const/16 v14, -0x13

    aput v14, v0, v13

    const/16 v13, -0x17

    aput v13, v0, v12

    const/16 v12, -0x7b91

    aput v12, v0, v11

    const/16 v11, -0x7c

    aput v11, v0, v10

    const/16 v10, -0x6b

    aput v10, v0, v9

    const/16 v9, 0x5337

    aput v9, v0, v8

    const/16 v8, 0x3f53

    aput v8, v0, v7

    const/16 v7, -0x20c1

    aput v7, v0, v6

    const/16 v6, -0x21

    aput v6, v0, v5

    const/16 v5, -0x39a5

    aput v5, v0, v4

    const/16 v4, -0x3a

    aput v4, v0, v3

    const/16 v3, -0x65b2

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_6
    array-length v3, v0

    if-lt v2, v3, :cond_6

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_7
    array-length v3, v0

    if-lt v2, v3, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4

    :cond_5
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_6
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_7
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    iput v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->cancelPinRequest:I

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->refreshFocusablesByForce()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->disableMotionEventSplittingForChildren(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private addBackgroundView()Landroid/view/View;
    .locals 54

    const/4 v1, 0x2

    new-array v3, v1, [J

    const/4 v1, 0x1

    const-wide/16 v4, 0x1

    aput-wide v4, v3, v1

    move-object/from16 v0, p0

    instance-of v5, v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v1

    const/4 v2, 0x0

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    aget-wide v6, v3, v4

    long-to-int v4, v6

    if-gtz v4, :cond_0

    new-instance v1, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v4, 0x0

    int-to-long v1, v1

    const/16 v6, 0x20

    shl-long/2addr v1, v6

    const/16 v6, 0x20

    ushr-long v6, v1, v6

    aget-wide v1, v3, v4

    const-wide/16 v8, 0x0

    cmp-long v8, v1, v8

    if-eqz v8, :cond_1

    const-wide v8, -0x2f9a8ccba8139ec6L    # -1.9869938675542424E79

    xor-long/2addr v1, v8

    :cond_1
    const/16 v8, 0x20

    ushr-long/2addr v1, v8

    const/16 v8, 0x20

    shl-long/2addr v1, v8

    xor-long/2addr v1, v6

    const-wide v6, -0x2f9a8ccba8139ec6L    # -1.9869938675542424E79

    xor-long/2addr v1, v6

    aput-wide v1, v3, v4

    const/4 v1, 0x0

    if-eqz v5, :cond_2

    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getTitleBackgroundFromTheme()Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v2, v1

    :goto_0
    const v1, 0x1020002

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    :try_start_1
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mFragmentRootLayout:Landroid/view/ViewGroup;
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_3} :catch_3

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v7, -0x1

    invoke-direct {v1, v4, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    sget v7, Lcom/sec/android/app/shealth/framework/ui/R$layout;->action_share_home_top:I

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mFragmentRootLayout:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    invoke-virtual {v2, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->share_content_frame:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    aget-wide v7, v3, v4

    long-to-int v4, v7

    if-gtz v4, :cond_b

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/16 v3, -0x37ff

    aput v3, v2, v1

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/16 v5, -0x37cf

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v2, v1

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_0

    :catch_1
    move-exception v1

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    const/16 v1, 0x12

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, -0xe

    aput v21, v2, v20

    const/16 v20, -0x5a9d

    aput v20, v2, v19

    const/16 v19, -0x10

    aput v19, v2, v18

    const/16 v18, 0x1e73

    aput v18, v2, v17

    const/16 v17, 0x453e

    aput v17, v2, v16

    const/16 v16, -0x5eca

    aput v16, v2, v15

    const/16 v15, -0x38

    aput v15, v2, v14

    const/16 v14, 0x7771

    aput v14, v2, v13

    const/16 v13, -0x6500

    aput v13, v2, v12

    const/4 v12, -0x2

    aput v12, v2, v11

    const/16 v11, -0x6b

    aput v11, v2, v10

    const/16 v10, -0x2bde

    aput v10, v2, v9

    const/16 v9, -0xc

    aput v9, v2, v8

    const/16 v8, 0xf4d

    aput v8, v2, v7

    const/16 v7, -0x7483

    aput v7, v2, v6

    const/16 v6, -0x16

    aput v6, v2, v5

    const/16 v5, -0x6d

    aput v5, v2, v3

    const/16 v3, -0x6a94

    aput v3, v2, v1

    const/16 v1, 0x12

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, -0x42

    aput v22, v1, v21

    const/16 v21, -0x5ad1

    aput v21, v1, v20

    const/16 v20, -0x5b

    aput v20, v1, v19

    const/16 v19, 0x1e3d

    aput v19, v1, v18

    const/16 v18, 0x451e

    aput v18, v1, v17

    const/16 v17, -0x5ebb

    aput v17, v1, v16

    const/16 v16, -0x5f

    aput v16, v1, v15

    const/16 v15, 0x7751

    aput v15, v1, v14

    const/16 v14, -0x6489

    aput v14, v1, v13

    const/16 v13, -0x65

    aput v13, v1, v12

    const/4 v12, -0x4

    aput v12, v1, v11

    const/16 v11, -0x2bac

    aput v11, v1, v10

    const/16 v10, -0x2c

    aput v10, v1, v9

    const/16 v9, 0xf28

    aput v9, v1, v8

    const/16 v8, -0x74f1

    aput v8, v1, v7

    const/16 v7, -0x75

    aput v7, v1, v6

    const/4 v6, -0x5

    aput v6, v1, v5

    const/16 v5, -0x6ac1

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_5
    return-object v1

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :catch_2
    move-exception v1

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, -0x4c

    aput v30, v2, v29

    const/16 v29, -0x16

    aput v29, v2, v28

    const/16 v28, -0x61

    aput v28, v2, v27

    const/16 v27, -0x55

    aput v27, v2, v26

    const/16 v26, -0x2a

    aput v26, v2, v25

    const/16 v25, 0x4c

    aput v25, v2, v24

    const/16 v24, -0x7f97

    aput v24, v2, v23

    const/16 v23, -0x60

    aput v23, v2, v22

    const/16 v22, -0x27

    aput v22, v2, v21

    const/16 v21, -0x2eed

    aput v21, v2, v20

    const/16 v20, -0x48

    aput v20, v2, v19

    const/16 v19, -0x1e

    aput v19, v2, v18

    const/16 v18, -0x75

    aput v18, v2, v17

    const/16 v17, -0x7f

    aput v17, v2, v16

    const/16 v16, -0x39

    aput v16, v2, v15

    const/16 v15, -0x1e

    aput v15, v2, v14

    const/16 v14, -0xb

    aput v14, v2, v13

    const/16 v13, -0x4c

    aput v13, v2, v12

    const/16 v12, 0x7a55

    aput v12, v2, v11

    const/16 v11, -0x57f2

    aput v11, v2, v10

    const/16 v10, -0x3a

    aput v10, v2, v9

    const/16 v9, -0x36

    aput v9, v2, v8

    const/16 v8, 0x1e22

    aput v8, v2, v7

    const/16 v7, 0x6f79

    aput v7, v2, v6

    const/16 v6, 0x2a0e

    aput v6, v2, v5

    const/16 v5, -0x14a8

    aput v5, v2, v3

    const/16 v3, -0x73

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, -0x8

    aput v31, v1, v30

    const/16 v30, -0x5a

    aput v30, v1, v29

    const/16 v29, -0x36

    aput v29, v1, v28

    const/16 v28, -0x1b

    aput v28, v1, v27

    const/16 v27, -0xa

    aput v27, v1, v26

    const/16 v26, 0x3f

    aput v26, v1, v25

    const/16 v25, -0x8000

    aput v25, v1, v24

    const/16 v24, -0x80

    aput v24, v1, v23

    const/16 v23, -0x52

    aput v23, v1, v22

    const/16 v22, -0x2e8a

    aput v22, v1, v21

    const/16 v21, -0x2f

    aput v21, v1, v20

    const/16 v20, -0x4c

    aput v20, v1, v19

    const/16 v19, -0x1

    aput v19, v1, v18

    const/16 v18, -0x11

    aput v18, v1, v17

    const/16 v17, -0x5e

    aput v17, v1, v16

    const/16 v16, -0x6a

    aput v16, v1, v15

    const/16 v15, -0x65

    aput v15, v1, v14

    const/16 v14, -0x25

    aput v14, v1, v13

    const/16 v13, 0x7a16

    aput v13, v1, v12

    const/16 v12, -0x5786

    aput v12, v1, v11

    const/16 v11, -0x58

    aput v11, v1, v10

    const/16 v10, -0x51

    aput v10, v1, v9

    const/16 v9, 0x1e4f

    aput v9, v1, v8

    const/16 v8, 0x6f1e

    aput v8, v1, v7

    const/16 v7, 0x2a6f

    aput v7, v1, v6

    const/16 v6, -0x14d6

    aput v6, v1, v5

    const/16 v5, -0x15

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :catch_3
    move-exception v1

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    const/16 v1, 0x31

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x2e

    const/16 v50, 0x2f

    const/16 v51, 0x30

    const/16 v52, 0x3d40

    aput v52, v2, v51

    const/16 v51, 0x3b4d

    aput v51, v2, v50

    const/16 v50, 0x754e

    aput v50, v2, v49

    const/16 v49, -0x1be6

    aput v49, v2, v48

    const/16 v48, -0x6a

    aput v48, v2, v47

    const/16 v47, -0x33

    aput v47, v2, v46

    const/16 v46, -0x4b

    aput v46, v2, v45

    const/16 v45, 0x7d75

    aput v45, v2, v44

    const/16 v44, 0x4114

    aput v44, v2, v43

    const/16 v43, 0xd17

    aput v43, v2, v42

    const/16 v42, 0x5f2d

    aput v42, v2, v41

    const/16 v41, -0x59f5

    aput v41, v2, v40

    const/16 v40, -0x17

    aput v40, v2, v39

    const/16 v39, -0x37

    aput v39, v2, v38

    const/16 v38, 0x1f5b

    aput v38, v2, v37

    const/16 v37, 0x356c

    aput v37, v2, v36

    const/16 v36, -0x5ba4

    aput v36, v2, v35

    const/16 v35, -0x7c

    aput v35, v2, v34

    const/16 v34, -0x3a

    aput v34, v2, v33

    const/16 v33, -0x3a

    aput v33, v2, v32

    const/16 v32, -0x51

    aput v32, v2, v31

    const/16 v31, 0x7744

    aput v31, v2, v30

    const/16 v30, 0x4012

    aput v30, v2, v29

    const/16 v29, -0x32ce

    aput v29, v2, v28

    const/16 v28, -0x54

    aput v28, v2, v27

    const/16 v27, 0x1b01

    aput v27, v2, v26

    const/16 v26, -0x6c91

    aput v26, v2, v25

    const/16 v25, -0xa

    aput v25, v2, v24

    const/16 v24, -0x53

    aput v24, v2, v23

    const/16 v23, -0x22df

    aput v23, v2, v22

    const/16 v22, -0x56

    aput v22, v2, v21

    const/16 v21, -0xc

    aput v21, v2, v20

    const/16 v20, 0x7b1d

    aput v20, v2, v19

    const/16 v19, 0x262d

    aput v19, v2, v18

    const/16 v18, 0x7d52

    aput v18, v2, v17

    const/16 v17, 0x3513

    aput v17, v2, v16

    const/16 v16, 0x5a50

    aput v16, v2, v15

    const/16 v15, -0x10d2

    aput v15, v2, v14

    const/16 v14, -0x7f

    aput v14, v2, v13

    const/16 v13, -0xd

    aput v13, v2, v12

    const/16 v12, -0x37c3

    aput v12, v2, v11

    const/16 v11, -0x44

    aput v11, v2, v10

    const/16 v10, -0x67

    aput v10, v2, v9

    const/16 v9, -0x4dca

    aput v9, v2, v8

    const/16 v8, -0x21

    aput v8, v2, v7

    const/16 v7, -0x1c

    aput v7, v2, v6

    const/16 v6, -0x4ec8

    aput v6, v2, v5

    const/16 v5, -0x3d

    aput v5, v2, v3

    const/16 v3, -0x45

    aput v3, v2, v1

    const/16 v1, 0x31

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x2e

    const/16 v51, 0x2f

    const/16 v52, 0x30

    const/16 v53, 0x3d6e

    aput v53, v1, v52

    const/16 v52, 0x3b3d

    aput v52, v1, v51

    const/16 v51, 0x753b

    aput v51, v1, v50

    const/16 v50, -0x1b8b

    aput v50, v1, v49

    const/16 v49, -0x1c

    aput v49, v1, v48

    const/16 v48, -0x76

    aput v48, v1, v47

    const/16 v47, -0x3e

    aput v47, v1, v46

    const/16 v46, 0x7d10

    aput v46, v1, v45

    const/16 v45, 0x417d

    aput v45, v1, v44

    const/16 v44, 0xd41

    aput v44, v1, v43

    const/16 v43, 0x5f0d

    aput v43, v1, v42

    const/16 v42, -0x59a1

    aput v42, v1, v41

    const/16 v41, -0x5a

    aput v41, v1, v40

    const/16 v40, -0x79

    aput v40, v1, v39

    const/16 v39, 0x1f7b

    aput v39, v1, v38

    const/16 v38, 0x351f

    aput v38, v1, v37

    const/16 v37, -0x5bcb

    aput v37, v1, v36

    const/16 v36, -0x5c

    aput v36, v1, v35

    const/16 v35, -0x11

    aput v35, v1, v34

    const/16 v34, -0x12

    aput v34, v1, v33

    const/16 v33, -0x25

    aput v33, v1, v32

    const/16 v32, 0x772a

    aput v32, v1, v31

    const/16 v31, 0x4077

    aput v31, v1, v30

    const/16 v30, -0x32c0

    aput v30, v1, v29

    const/16 v29, -0x33

    aput v29, v1, v28

    const/16 v28, 0x1b51

    aput v28, v1, v27

    const/16 v27, -0x6ce5

    aput v27, v1, v26

    const/16 v26, -0x6d

    aput v26, v1, v25

    const/16 v25, -0x36

    aput v25, v1, v24

    const/16 v24, -0x22f1

    aput v24, v1, v23

    const/16 v23, -0x23

    aput v23, v1, v22

    const/16 v22, -0x6f

    aput v22, v1, v21

    const/16 v21, 0x7b74

    aput v21, v1, v20

    const/16 v20, 0x267b

    aput v20, v1, v19

    const/16 v19, 0x7d26

    aput v19, v1, v18

    const/16 v18, 0x357d

    aput v18, v1, v17

    const/16 v17, 0x5a35

    aput v17, v1, v16

    const/16 v16, -0x10a6

    aput v16, v1, v15

    const/16 v15, -0x11

    aput v15, v1, v14

    const/16 v14, -0x64

    aput v14, v1, v13

    const/16 v13, -0x3782

    aput v13, v1, v12

    const/16 v12, -0x38

    aput v12, v1, v11

    const/16 v11, -0x9

    aput v11, v1, v10

    const/16 v10, -0x4dad

    aput v10, v1, v9

    const/16 v9, -0x4e

    aput v9, v1, v8

    const/16 v8, -0x7d

    aput v8, v1, v7

    const/16 v7, -0x4ea7

    aput v7, v1, v6

    const/16 v6, -0x4f

    aput v6, v1, v5

    const/16 v5, -0x23

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto/16 :goto_5

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_9
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_a
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_b
    const/4 v4, 0x0

    aget-wide v3, v3, v4

    const-wide/16 v7, 0x0

    cmp-long v7, v3, v7

    if-eqz v7, :cond_c

    const-wide v7, -0x2f9a8ccba8139ec6L    # -1.9869938675542424E79

    xor-long/2addr v3, v7

    :cond_c
    const/16 v7, 0x20

    shl-long/2addr v3, v7

    const/16 v7, 0x20

    shr-long/2addr v3, v7

    long-to-int v3, v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mShareViewBmp:Landroid/graphics/Bitmap;

    new-instance v3, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mShareViewBmp:Landroid/graphics/Bitmap;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v6, v3}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    if-eqz v5, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget v4, Lcom/sec/android/app/shealth/framework/ui/R$id;->content_frame:I

    invoke-virtual {v2, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    instance-of v4, v2, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    if-eqz v4, :cond_d

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;->isNoDataGraph()Z

    move-result v2

    if-nez v2, :cond_d

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->general_view_container:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mShareViewBmp:Landroid/graphics/Bitmap;

    invoke-static {v4, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->prepareGraphScreenshot(Landroid/graphics/Bitmap;Landroid/widget/LinearLayout;)V

    :cond_d
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mShareViewBmp:Landroid/graphics/Bitmap;

    invoke-direct {v2, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/BitmapDrawable;->setAntiAlias(Z)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->tvShareViewHomeTitle:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->data_from_s_health:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_a
    :try_start_5
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_5

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-le v1, v2, :cond_e

    invoke-virtual {v3}, Landroid/graphics/Canvas;->release()V

    :cond_e
    :goto_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    goto/16 :goto_5

    :catch_4
    move-exception v1

    goto :goto_a

    :catch_5
    move-exception v1

    goto :goto_b
.end method

.method private disableMotionEventSplitting()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method private disableMotionEventSplittingForChildren(Landroid/view/View;)V
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v10, 0x1

    const-wide v8, -0x51f22a4abc5fe365L    # -7.498844548514877E-87

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v10

    instance-of v0, p1, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setMotionEventSplittingEnabled(Z)V

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_2

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    int-to-long v0, v3

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_3

    xor-long/2addr v0, v8

    :cond_3
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v8

    aput-wide v0, v2, v3

    :goto_0
    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_6

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v10, [I

    const/16 v0, -0x6bc

    aput v0, v1, v3

    new-array v0, v10, [I

    const/16 v2, -0x68c

    aput v2, v0, v3

    move v2, v3

    :goto_1
    array-length v5, v0

    if-lt v2, v5, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    :goto_2
    array-length v2, v0

    if-lt v3, v2, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_7

    xor-long/2addr v0, v8

    :cond_7
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_a

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v10, [I

    const/16 v0, -0x5e

    aput v0, v1, v3

    new-array v0, v10, [I

    const/16 v2, -0x6e

    aput v2, v0, v3

    move v2, v3

    :goto_3
    array-length v5, v0

    if-lt v2, v5, :cond_8

    array-length v0, v1

    new-array v0, v0, [C

    :goto_4
    array-length v2, v0

    if-lt v3, v2, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_9
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_a
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_b

    xor-long/2addr v0, v8

    :cond_b
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->disableMotionEventSplittingForChildren(Landroid/view/View;)V

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_c

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    aget-wide v0, v2, v3

    cmp-long v4, v0, v11

    if-eqz v4, :cond_d

    xor-long/2addr v0, v8

    :cond_d
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    aget-wide v4, v2, v1

    long-to-int v1, v4

    if-gtz v1, :cond_e

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    int-to-long v0, v0

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_f

    xor-long/2addr v0, v8

    :cond_f
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v8

    aput-wide v0, v2, v3

    goto/16 :goto_0
.end method

.method private getTitleBackgroundFromTheme()Landroid/graphics/drawable/Drawable;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    const/16 v3, 0x280

    const/16 v2, 0x1e0

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundPortraitFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundLandscapeFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/FolderUtils;->isFileExist(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundPortraitFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapByPath(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mTitleBmp:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mTitleBmp:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/GalleryUtils;->getDefaultHealthBoardBackgroundLandscapeFilePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapByPath(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mTitleBmp:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mTitleBmp:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private initView(Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->disableMotionEventSplitting()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private isDownloadablePluginApp()Z
    .locals 33

    const/4 v0, 0x2

    new-array v4, v0, [J

    const/4 v0, 0x1

    const-wide/16 v1, 0x2

    aput-wide v1, v4, v0

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/16 v0, 0x1b

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, -0xfc

    aput v31, v1, v30

    const/16 v30, -0x75

    aput v30, v1, v29

    const/16 v29, 0x7f27

    aput v29, v1, v28

    const/16 v28, -0x22e2

    aput v28, v1, v27

    const/16 v27, -0x48

    aput v27, v1, v26

    const/16 v26, -0x27

    aput v26, v1, v25

    const/16 v25, -0x3a

    aput v25, v1, v24

    const/16 v24, -0x6b

    aput v24, v1, v23

    const/16 v23, -0x42

    aput v23, v1, v22

    const/16 v22, -0x2fe0

    aput v22, v1, v21

    const/16 v21, -0x4f

    aput v21, v1, v20

    const/16 v20, 0x420b

    aput v20, v1, v19

    const/16 v19, -0x2eda

    aput v19, v1, v18

    const/16 v18, -0x48

    aput v18, v1, v17

    const/16 v17, -0x24cd

    aput v17, v1, v16

    const/16 v16, -0x57

    aput v16, v1, v15

    const/16 v15, 0x5009

    aput v15, v1, v14

    const/16 v14, -0x48c2

    aput v14, v1, v13

    const/16 v13, -0x2a

    aput v13, v1, v12

    const/16 v12, 0x6156

    aput v12, v1, v11

    const/16 v11, 0x4702

    aput v11, v1, v10

    const/16 v10, -0x52de

    aput v10, v1, v9

    const/16 v9, -0x22

    aput v9, v1, v8

    const/16 v8, -0x39

    aput v8, v1, v7

    const/16 v7, -0x76

    aput v7, v1, v6

    const/16 v6, -0x17

    aput v6, v1, v2

    const/16 v2, -0x36

    aput v2, v1, v0

    const/16 v0, 0x1b

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, -0x94

    aput v32, v0, v31

    const/16 v31, -0x1

    aput v31, v0, v30

    const/16 v30, 0x7f4b

    aput v30, v0, v29

    const/16 v29, -0x2281

    aput v29, v0, v28

    const/16 v28, -0x23

    aput v28, v0, v27

    const/16 v27, -0x4f

    aput v27, v0, v26

    const/16 v26, -0x4b

    aput v26, v0, v25

    const/16 v25, -0x45

    aput v25, v0, v24

    const/16 v24, -0x32

    aput v24, v0, v23

    const/16 v23, -0x2fb0

    aput v23, v0, v22

    const/16 v22, -0x30

    aput v22, v0, v21

    const/16 v21, 0x4225

    aput v21, v0, v20

    const/16 v20, -0x2ebe

    aput v20, v0, v19

    const/16 v19, -0x2f

    aput v19, v0, v18

    const/16 v18, -0x24a4

    aput v18, v0, v17

    const/16 v17, -0x25

    aput v17, v0, v16

    const/16 v16, 0x506d

    aput v16, v0, v15

    const/16 v15, -0x48b0

    aput v15, v0, v14

    const/16 v14, -0x49

    aput v14, v0, v13

    const/16 v13, 0x6178

    aput v13, v0, v12

    const/16 v12, 0x4761

    aput v12, v0, v11

    const/16 v11, -0x52b9

    aput v11, v0, v10

    const/16 v10, -0x53

    aput v10, v0, v9

    const/16 v9, -0x17

    aput v9, v0, v8

    const/16 v8, -0x19

    aput v8, v0, v7

    const/16 v7, -0x7a

    aput v7, v0, v6

    const/16 v6, -0x57

    aput v6, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v6, v0

    if-lt v2, v6, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v6, v0

    if-lt v2, v6, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_0
    aget v6, v0, v2

    aget v7, v1, v2

    xor-int/2addr v6, v7

    aput v6, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v6, v1, v2

    int-to-char v6, v6

    aput-char v6, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const/16 v0, 0x8

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x5d8f

    aput v12, v1, v11

    const/16 v11, -0x2e

    aput v11, v1, v10

    const/16 v10, -0x6eec

    aput v10, v1, v9

    const/16 v9, -0x1b

    aput v9, v1, v8

    const/16 v8, -0x67

    aput v8, v1, v7

    const/16 v7, -0x78

    aput v7, v1, v6

    const/16 v6, -0x6a

    aput v6, v1, v2

    const/16 v2, -0x3498

    aput v2, v1, v0

    const/16 v0, 0x8

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x5dec

    aput v13, v0, v12

    const/16 v12, -0x5e

    aput v12, v0, v11

    const/16 v11, -0x6e93

    aput v11, v0, v10

    const/16 v10, -0x6f

    aput v10, v0, v9

    const/16 v9, -0x3a

    aput v9, v0, v8

    const/4 v8, -0x8

    aput v8, v0, v7

    const/16 v7, -0x1a

    aput v7, v0, v6

    const/16 v6, -0x34f7

    aput v6, v0, v2

    const/4 v2, 0x0

    :goto_3
    array-length v6, v0

    if-lt v2, v6, :cond_3

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_4
    array-length v6, v0

    if-lt v2, v6, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-wide v5, v4, v2

    long-to-int v2, v5

    if-lt v1, v2, :cond_5

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move v0, v3

    goto/16 :goto_2

    :cond_3
    :try_start_1
    aget v6, v0, v2

    aget v7, v1, v2

    xor-int/2addr v6, v7

    aput v6, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    aget v6, v1, v2

    int-to-char v6, v6

    aput-char v6, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    int-to-long v0, v0

    const/16 v5, 0x20

    shl-long v5, v0, v5

    aget-wide v0, v4, v2

    const-wide/16 v7, 0x0

    cmp-long v7, v0, v7

    if-eqz v7, :cond_6

    const-wide v7, 0x17848b0fe3e5288aL    # 2.198574335970092E-195

    xor-long/2addr v0, v7

    :cond_6
    const/16 v7, 0x20

    shl-long/2addr v0, v7

    const/16 v7, 0x20

    ushr-long/2addr v0, v7

    xor-long/2addr v0, v5

    const-wide v5, 0x17848b0fe3e5288aL    # 2.198574335970092E-195

    xor-long/2addr v0, v5

    aput-wide v0, v4, v2

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v4, v0

    long-to-int v0, v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_9

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/16 v2, -0x44b5

    aput v2, v1, v0

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/16 v5, -0x4486

    aput v5, v0, v2

    const/4 v2, 0x0

    :goto_5
    array-length v5, v0

    if-lt v2, v5, :cond_7

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_6
    array-length v5, v0

    if-lt v2, v5, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_7
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_8
    aget v5, v1, v2

    int-to-char v5, v5

    aput-char v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_9
    const/4 v0, 0x0

    aget-wide v0, v4, v0
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_a

    const-wide v2, 0x17848b0fe3e5288aL    # 2.198574335970092E-195

    xor-long/2addr v0, v2

    :cond_a
    const/16 v2, 0x20

    shr-long/2addr v0, v2

    long-to-int v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    goto/16 :goto_2

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method private refreshFocusablesByForce()V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Lcom/sec/android/app/shealth/common/utils/FocusWorker;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FocusWorker;->refreshFocusables([Landroid/view/View;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static setRequestPassword(Z)V
    .locals 0

    sput-boolean p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    return-void
.end method


# virtual methods
.method protected addToScreenViewsList(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected clearScreensViewsList()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->screenViewsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected isTryActivity()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 23

    const/4 v2, 0x2

    new-array v5, v2, [J

    const/4 v2, 0x1

    const-wide/16 v3, 0x2

    aput-wide v3, v5, v2

    const/4 v2, 0x0

    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v5, v3

    long-to-int v3, v3

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p1

    int-to-long v2, v0

    const/16 v6, 0x20

    shl-long/2addr v2, v6

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    aget-wide v2, v5, v4

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_1

    const-wide v8, 0x18dad21cf3f2e3f5L    # 6.019684351259831E-189

    xor-long/2addr v2, v8

    :cond_1
    const/16 v8, 0x20

    ushr-long/2addr v2, v8

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    xor-long/2addr v2, v6

    const-wide v6, 0x18dad21cf3f2e3f5L    # 6.019684351259831E-189

    xor-long/2addr v2, v6

    aput-wide v2, v5, v4

    const/4 v2, 0x1

    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v5, v3

    long-to-int v3, v3

    if-lt v2, v3, :cond_2

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    const/4 v4, 0x0

    move/from16 v0, p2

    int-to-long v2, v0

    const/16 v6, 0x20

    shl-long v6, v2, v6

    aget-wide v2, v5, v4

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_3

    const-wide v8, 0x18dad21cf3f2e3f5L    # 6.019684351259831E-189

    xor-long/2addr v2, v8

    :cond_3
    const/16 v8, 0x20

    shl-long/2addr v2, v8

    const/16 v8, 0x20

    ushr-long/2addr v2, v8

    xor-long/2addr v2, v6

    const-wide v6, 0x18dad21cf3f2e3f5L    # 6.019684351259831E-189

    xor-long/2addr v2, v6

    aput-wide v2, v5, v4

    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    sget-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->sIsDownloadedPlugin:Z

    if-nez v2, :cond_4

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, -0x73

    aput v21, v3, v20

    const/16 v20, -0x15

    aput v20, v3, v19

    const/16 v19, -0x5b

    aput v19, v3, v18

    const/16 v18, 0x160b

    aput v18, v3, v17

    const/16 v17, -0x2c9b

    aput v17, v3, v16

    const/16 v16, -0x60

    aput v16, v3, v15

    const/16 v15, -0x5e

    aput v15, v3, v14

    const/16 v14, -0x57

    aput v14, v3, v13

    const/16 v13, 0x2709

    aput v13, v3, v12

    const/16 v12, -0x71ad

    aput v12, v3, v11

    const/4 v11, -0x3

    aput v11, v3, v10

    const/16 v10, -0x2d

    aput v10, v3, v9

    const/16 v9, -0x13b9

    aput v9, v3, v8

    const/16 v8, -0x63

    aput v8, v3, v7

    const/16 v7, 0x511e

    aput v7, v3, v4

    const/16 v4, -0x29dd

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, -0x17

    aput v22, v2, v21

    const/16 v21, -0x67

    aput v21, v2, v20

    const/16 v20, -0x36

    aput v20, v2, v19

    const/16 v19, 0x167c

    aput v19, v2, v18

    const/16 v18, -0x2cea

    aput v18, v2, v17

    const/16 v17, -0x2d

    aput v17, v2, v16

    const/16 v16, -0x3d

    aput v16, v2, v15

    const/16 v15, -0x27

    aput v15, v2, v14

    const/16 v14, 0x2756

    aput v14, v2, v13

    const/16 v13, -0x71d9

    aput v13, v2, v12

    const/16 v12, -0x72

    aput v12, v2, v11

    const/16 v11, -0x4a

    aput v11, v2, v10

    const/16 v10, -0x13ce

    aput v10, v2, v9

    const/16 v9, -0x14

    aput v9, v2, v8

    const/16 v8, 0x517b

    aput v8, v2, v7

    const/16 v7, -0x29af

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v7, v2

    if-lt v4, v7, :cond_5

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v7, v2

    if-lt v4, v7, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    move-object/from16 v0, p0

    invoke-virtual {v6, v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_4
    :goto_2
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->startedAnotherActivity:Z

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_9

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x69

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x59

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_3
    array-length v6, v2

    if-lt v4, v6, :cond_7

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_4
    array-length v6, v2

    if-lt v4, v6, :cond_8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_5
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_6
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_7
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_8
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_9
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-eqz v4, :cond_a

    const-wide v6, 0x18dad21cf3f2e3f5L    # 6.019684351259831E-189

    xor-long/2addr v2, v6

    :cond_a
    const/16 v4, 0x20

    shl-long/2addr v2, v4

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v4, v2

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_d

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x74c7

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x74f8

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_5
    array-length v6, v2

    if-lt v4, v6, :cond_b

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_6
    array-length v6, v2

    if-lt v4, v6, :cond_c

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_b
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_c
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_d
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-eqz v5, :cond_e

    const-wide v5, 0x18dad21cf3f2e3f5L    # 6.019684351259831E-189

    xor-long/2addr v2, v5

    :cond_e
    const/16 v5, 0x20

    shr-long/2addr v2, v5

    long-to-int v2, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-super {v0, v4, v2, v1}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    :catch_0
    move-exception v2

    goto/16 :goto_2
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onCacheBuildEnd()V
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mShareViewBmp:Landroid/graphics/Bitmap;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mShareViewBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mTitleBmp:Landroid/graphics/Bitmap;

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mTitleBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mScaledBitmap:Landroid/graphics/Bitmap;

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mScaledBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mFragmentRootLayout:Landroid/view/ViewGroup;

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mFragmentRootLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_5

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_6

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mOldFragmentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_3

    :catch_6
    move-exception v0

    goto :goto_3
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 40

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0xe

    aput v11, v2, v10

    const/16 v10, -0x42

    aput v10, v2, v9

    const/4 v9, -0x4

    aput v9, v2, v8

    const/16 v8, -0x9b2

    aput v8, v2, v7

    const/16 v7, -0x7c

    aput v7, v2, v6

    const/16 v6, -0x2a

    aput v6, v2, v5

    const/16 v5, 0x1079

    aput v5, v2, v3

    const/16 v3, 0x457f

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x69

    aput v12, v1, v11

    const/16 v11, -0x36

    aput v11, v1, v10

    const/16 v10, -0x63

    aput v10, v1, v9

    const/16 v9, -0x9d5

    aput v9, v1, v8

    const/16 v8, -0xa

    aput v8, v1, v7

    const/16 v7, -0x6b

    aput v7, v1, v6

    const/16 v6, 0x1017

    aput v6, v1, v5

    const/16 v5, 0x4510

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xa60

    aput v17, v2, v16

    const/16 v16, 0x1765

    aput v16, v2, v15

    const/16 v15, 0x1d5b

    aput v15, v2, v14

    const/16 v14, 0x7673

    aput v14, v2, v13

    const/16 v13, -0x23e7

    aput v13, v2, v12

    const/16 v12, -0x4b

    aput v12, v2, v11

    const/16 v11, -0x76

    aput v11, v2, v10

    const/16 v10, -0x7a

    aput v10, v2, v9

    const/16 v9, -0x22

    aput v9, v2, v8

    const/16 v8, -0x2e

    aput v8, v2, v7

    const/16 v7, -0x55d4

    aput v7, v2, v6

    const/16 v6, -0x3d

    aput v6, v2, v5

    const/16 v5, -0xc

    aput v5, v2, v4

    const/16 v4, -0x33

    aput v4, v2, v3

    const/16 v3, -0x4dac

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xa07

    aput v18, v1, v17

    const/16 v17, 0x170a

    aput v17, v1, v16

    const/16 v16, 0x1d17

    aput v16, v1, v15

    const/16 v15, 0x761d

    aput v15, v1, v14

    const/16 v14, -0x238a

    aput v14, v1, v13

    const/16 v13, -0x24

    aput v13, v1, v12

    const/4 v12, -0x2

    aput v12, v1, v11

    const/16 v11, -0x19

    aput v11, v1, v10

    const/16 v10, -0x43

    aput v10, v1, v9

    const/16 v9, -0x45

    aput v9, v1, v8

    const/16 v8, -0x55b6

    aput v8, v1, v7

    const/16 v7, -0x56

    aput v7, v1, v6

    const/16 v6, -0x7a

    aput v6, v1, v5

    const/16 v5, -0x58

    aput v5, v1, v4

    const/16 v4, -0x4dfe

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x32

    aput v11, v2, v10

    const/16 v10, -0x3c

    aput v10, v2, v9

    const/16 v9, 0x240c

    aput v9, v2, v8

    const/16 v8, -0x35bf

    aput v8, v2, v7

    const/16 v7, -0x48

    aput v7, v2, v6

    const/16 v6, -0x57

    aput v6, v2, v5

    const/16 v5, 0x242d

    aput v5, v2, v3

    const/16 v3, -0x26b5

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x55

    aput v12, v1, v11

    const/16 v11, -0x50

    aput v11, v1, v10

    const/16 v10, 0x246d

    aput v10, v1, v9

    const/16 v9, -0x35dc

    aput v9, v1, v8

    const/16 v8, -0x36

    aput v8, v1, v7

    const/16 v7, -0x16

    aput v7, v1, v6

    const/16 v6, 0x2443

    aput v6, v1, v5

    const/16 v5, -0x26dc

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->setRequestedOrientation(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x14

    if-gt v2, v3, :cond_7

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit16 v2, v2, 0x2000

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->isDownloadablePluginApp()Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->sIsDownloadedPlugin:Z

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/service/health/keyManager/KeyManager;->DEBUG:Z

    if-eqz v1, :cond_0

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x55

    aput v12, v2, v11

    const/16 v11, 0x3415

    aput v11, v2, v10

    const/16 v10, 0x2e53

    aput v10, v2, v9

    const/16 v9, -0x14b1

    aput v9, v2, v8

    const/16 v8, -0x7b

    aput v8, v2, v7

    const/16 v7, -0x5f

    aput v7, v2, v6

    const/16 v6, -0x4e

    aput v6, v2, v5

    const/16 v5, -0x55

    aput v5, v2, v4

    const/16 v4, 0x2132

    aput v4, v2, v3

    const/16 v3, -0x696

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x27

    aput v13, v1, v12

    const/16 v12, 0x3470

    aput v12, v1, v11

    const/16 v11, 0x2e34

    aput v11, v1, v10

    const/16 v10, -0x14d2

    aput v10, v1, v9

    const/16 v9, -0x15

    aput v9, v1, v8

    const/16 v8, -0x40

    aput v8, v1, v7

    const/4 v7, -0x1

    aput v7, v1, v6

    const/16 v6, -0x2e

    aput v6, v1, v5

    const/16 v5, 0x2157

    aput v5, v1, v4

    const/16 v4, -0x6df

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_7
    array-length v4, v1

    if-lt v3, v4, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_8
    array-length v4, v1

    if-lt v3, v4, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x22

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x3c4b

    aput v38, v2, v37

    const/16 v37, -0x77a7

    aput v37, v2, v36

    const/16 v36, -0x11

    aput v36, v2, v35

    const/16 v35, -0x68e1

    aput v35, v2, v34

    const/16 v34, -0x7

    aput v34, v2, v33

    const/16 v33, -0x63

    aput v33, v2, v32

    const/16 v32, -0x23

    aput v32, v2, v31

    const/16 v31, 0x4060

    aput v31, v2, v30

    const/16 v30, 0x6525

    aput v30, v2, v29

    const/16 v29, 0x712e

    aput v29, v2, v28

    const/16 v28, -0x20a1

    aput v28, v2, v27

    const/16 v27, -0x5a

    aput v27, v2, v26

    const/16 v26, -0x3c2

    aput v26, v2, v25

    const/16 v25, -0x6b

    aput v25, v2, v24

    const/16 v24, -0x5bf0

    aput v24, v2, v23

    const/16 v23, -0x33

    aput v23, v2, v22

    const/16 v22, 0x40

    aput v22, v2, v21

    const/16 v21, -0x689d

    aput v21, v2, v20

    const/16 v20, -0x2a

    aput v20, v2, v19

    const/16 v19, 0x240e

    aput v19, v2, v18

    const/16 v18, -0x2da9

    aput v18, v2, v17

    const/16 v17, -0x4d

    aput v17, v2, v16

    const/16 v16, -0x2c

    aput v16, v2, v15

    const/16 v15, -0x3b

    aput v15, v2, v14

    const/16 v14, -0x9bd

    aput v14, v2, v13

    const/16 v13, -0x6d

    aput v13, v2, v12

    const/16 v12, -0x7998

    aput v12, v2, v11

    const/16 v11, -0x19

    aput v11, v2, v10

    const/16 v10, -0x19

    aput v10, v2, v9

    const/16 v9, -0x5c

    aput v9, v2, v8

    const/16 v8, -0x7a

    aput v8, v2, v7

    const/16 v7, -0x6a

    aput v7, v2, v6

    const/16 v6, -0x38ea

    aput v6, v2, v3

    const/16 v3, -0x74

    aput v3, v2, v1

    const/16 v1, 0x22

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, 0x1d

    const/16 v35, 0x1e

    const/16 v36, 0x1f

    const/16 v37, 0x20

    const/16 v38, 0x21

    const/16 v39, 0x3c39

    aput v39, v1, v38

    const/16 v38, -0x77c4

    aput v38, v1, v37

    const/16 v37, -0x78

    aput v37, v1, v36

    const/16 v36, -0x6882

    aput v36, v1, v35

    const/16 v35, -0x69

    aput v35, v1, v34

    const/16 v34, -0x4

    aput v34, v1, v33

    const/16 v33, -0x70

    aput v33, v1, v32

    const/16 v32, 0x4019

    aput v32, v1, v31

    const/16 v31, 0x6540

    aput v31, v1, v30

    const/16 v30, 0x7165

    aput v30, v1, v29

    const/16 v29, -0x208f

    aput v29, v1, v28

    const/16 v28, -0x21

    aput v28, v1, v27

    const/16 v27, -0x3b6

    aput v27, v1, v26

    const/16 v26, -0x4

    aput v26, v1, v25

    const/16 v25, -0x5b9a

    aput v25, v1, v24

    const/16 v24, -0x5c

    aput v24, v1, v23

    const/16 v23, 0x34

    aput v23, v1, v22

    const/16 v22, -0x6900

    aput v22, v1, v21

    const/16 v21, -0x69

    aput v21, v1, v20

    const/16 v20, 0x246b

    aput v20, v1, v19

    const/16 v19, -0x2ddc

    aput v19, v1, v18

    const/16 v18, -0x2e

    aput v18, v1, v17

    const/16 v17, -0x6a

    aput v17, v1, v16

    const/16 v16, -0x1b

    aput v16, v1, v15

    const/16 v15, -0x9cf

    aput v15, v1, v14

    const/16 v14, -0xa

    aput v14, v1, v13

    const/16 v13, -0x79f1

    aput v13, v1, v12

    const/16 v12, -0x7a

    aput v12, v1, v11

    const/16 v11, -0x77

    aput v11, v1, v10

    const/16 v10, -0x3b

    aput v10, v1, v9

    const/16 v9, -0x35

    aput v9, v1, v8

    const/16 v8, -0x11

    aput v8, v1, v7

    const/16 v7, -0x388d

    aput v7, v1, v6

    const/16 v6, -0x39

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v6, v1

    if-lt v3, v6, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v6, v1

    if-lt v3, v6, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_7
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    goto/16 :goto_6

    :cond_8
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_9
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_a
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_b
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a
.end method

.method protected onDestroy()V
    .locals 14

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/16 v10, -0x6c

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v4

    const/16 v0, 0x9

    new-array v1, v0, [I

    const/4 v0, 0x4

    const/4 v2, 0x5

    const/4 v5, 0x6

    const/4 v6, 0x7

    const/16 v7, 0x8

    const/16 v8, 0x7363

    aput v8, v1, v7

    const/16 v7, -0xe4

    aput v7, v1, v6

    const/16 v6, -0x73

    aput v6, v1, v5

    const/16 v5, -0x47

    aput v5, v1, v2

    const/16 v2, -0x19

    aput v2, v1, v0

    const/16 v0, 0x368

    aput v0, v1, v13

    const/16 v0, -0x6bb9

    aput v0, v1, v12

    const/4 v0, -0x6

    aput v0, v1, v11

    const/16 v0, -0x10

    aput v0, v1, v3

    const/16 v0, 0x9

    new-array v0, v0, [I

    const/4 v2, 0x4

    const/4 v5, 0x5

    const/4 v6, 0x6

    const/4 v7, 0x7

    const/16 v8, 0x8

    const/16 v9, 0x731a

    aput v9, v0, v8

    const/16 v8, -0x8d

    aput v8, v0, v7

    const/4 v7, -0x1

    aput v7, v0, v6

    const/16 v6, -0x33

    aput v6, v0, v5

    aput v10, v0, v2

    const/16 v2, 0x30d

    aput v2, v0, v13

    const/16 v2, -0x6bfd

    aput v2, v0, v12

    aput v10, v0, v11

    const/16 v2, -0x61

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->clearScreensViewsList()V

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    return-void

    :cond_0
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$id;->sharevia:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->prepareShareView()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 32

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, -0x6f

    aput v30, v2, v29

    const/16 v29, -0x2f

    aput v29, v2, v28

    const/16 v28, -0x17f2

    aput v28, v2, v27

    const/16 v27, -0x77

    aput v27, v2, v26

    const/16 v26, -0x51

    aput v26, v2, v25

    const/16 v25, -0x66

    aput v25, v2, v24

    const/16 v24, -0x2f

    aput v24, v2, v23

    const/16 v23, 0x4876

    aput v23, v2, v22

    const/16 v22, 0x1f79

    aput v22, v2, v21

    const/16 v21, 0xc2e

    aput v21, v2, v20

    const/16 v20, -0xc8b

    aput v20, v2, v19

    const/16 v19, -0x79

    aput v19, v2, v18

    const/16 v18, -0x4

    aput v18, v2, v17

    const/16 v17, -0xdb6

    aput v17, v2, v16

    const/16 v16, -0x65

    aput v16, v2, v15

    const/16 v15, -0x30

    aput v15, v2, v14

    const/16 v14, -0x9

    aput v14, v2, v13

    const/16 v13, -0x31

    aput v13, v2, v12

    const/16 v12, -0x47

    aput v12, v2, v11

    const/16 v11, 0x1545

    aput v11, v2, v10

    const/16 v10, 0x5b74

    aput v10, v2, v9

    const/16 v9, 0x2519

    aput v9, v2, v8

    const/16 v8, -0x4caf

    aput v8, v2, v7

    const/16 v7, -0x24

    aput v7, v2, v6

    const/16 v6, -0x42d1

    aput v6, v2, v3

    const/16 v3, -0x11

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, -0xc

    aput v31, v1, v30

    const/16 v30, -0x5e

    aput v30, v1, v29

    const/16 v29, -0x1785

    aput v29, v1, v28

    const/16 v28, -0x18

    aput v28, v1, v27

    const/16 v27, -0x1

    aput v27, v1, v26

    const/16 v26, -0xc

    aput v26, v1, v25

    const/16 v25, -0x42

    aput v25, v1, v24

    const/16 v24, 0x4856

    aput v24, v1, v23

    const/16 v23, 0x1f48

    aput v23, v1, v22

    const/16 v22, 0xc1f

    aput v22, v1, v21

    const/16 v21, -0xcf4

    aput v21, v1, v20

    const/16 v20, -0xd

    aput v20, v1, v19

    const/16 v19, -0x6b

    aput v19, v1, v18

    const/16 v18, -0xdc4

    aput v18, v1, v17

    const/16 v17, -0xe

    aput v17, v1, v16

    const/16 v16, -0x5c

    aput v16, v1, v15

    const/16 v15, -0x6c

    aput v15, v1, v14

    const/16 v14, -0x72

    aput v14, v1, v13

    const/16 v13, -0x24

    aput v13, v1, v12

    const/16 v12, 0x1536

    aput v12, v1, v11

    const/16 v11, 0x5b15

    aput v11, v1, v10

    const/16 v10, 0x255b

    aput v10, v1, v9

    const/16 v9, -0x4cdb

    aput v9, v1, v8

    const/16 v8, -0x4d

    aput v8, v1, v7

    const/16 v7, -0x42c0

    aput v7, v1, v6

    const/16 v6, -0x43

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x7

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/16 v10, -0x43

    aput v10, v2, v9

    const/16 v9, -0x1f

    aput v9, v2, v8

    const/16 v8, 0x23d

    aput v8, v2, v7

    const/16 v7, 0x7263

    aput v7, v2, v6

    const/16 v6, 0x6122

    aput v6, v2, v5

    const/16 v5, -0xf1

    aput v5, v2, v3

    const/16 v3, -0x70

    aput v3, v2, v1

    const/4 v1, 0x7

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/16 v11, -0x28

    aput v11, v1, v10

    const/16 v10, -0x6e

    aput v10, v1, v9

    const/16 v9, 0x248

    aput v9, v1, v8

    const/16 v8, 0x7202

    aput v8, v1, v7

    const/16 v7, 0x6172

    aput v7, v1, v6

    const/16 v6, -0x9f

    aput v6, v1, v5

    const/4 v5, -0x1

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    sget-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->sIsDownloadedPlugin:Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x793c

    aput v19, v2, v18

    const/16 v18, 0xd0b

    aput v18, v2, v17

    const/16 v17, -0x3b9e

    aput v17, v2, v16

    const/16 v16, -0x4d

    aput v16, v2, v15

    const/16 v15, 0x6f52

    aput v15, v2, v14

    const/16 v14, -0x77e4

    aput v14, v2, v13

    const/16 v13, -0x17

    aput v13, v2, v12

    const/16 v12, -0x52

    aput v12, v2, v11

    const/16 v11, -0x5cfb

    aput v11, v2, v10

    const/16 v10, -0x29

    aput v10, v2, v9

    const/16 v9, -0x53

    aput v9, v2, v8

    const/16 v8, 0xa0a

    aput v8, v2, v7

    const/16 v7, 0x7b7f

    aput v7, v2, v6

    const/16 v6, -0x13f6

    aput v6, v2, v5

    const/16 v5, -0x77

    aput v5, v2, v3

    const/16 v3, 0x7e40

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x7958

    aput v20, v1, v19

    const/16 v19, 0xd79

    aput v19, v1, v18

    const/16 v18, -0x3bf3

    aput v18, v1, v17

    const/16 v17, -0x3c

    aput v17, v1, v16

    const/16 v16, 0x6f21

    aput v16, v1, v15

    const/16 v15, -0x7791

    aput v15, v1, v14

    const/16 v14, -0x78

    aput v14, v1, v13

    const/16 v13, -0x22

    aput v13, v1, v12

    const/16 v12, -0x5ca6

    aput v12, v1, v11

    const/16 v11, -0x5d

    aput v11, v1, v10

    const/16 v10, -0x22

    aput v10, v1, v9

    const/16 v9, 0xa6f

    aput v9, v1, v8

    const/16 v8, 0x7b0a

    aput v8, v1, v7

    const/16 v7, -0x1385

    aput v7, v1, v6

    const/16 v6, -0x14

    aput v6, v1, v5

    const/16 v5, 0x7e32

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_0
    :goto_6
    invoke-super/range {p0 .. p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    return-void

    :cond_1
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :catch_0
    move-exception v1

    goto :goto_6
.end method

.method protected onResume()V
    .locals 45

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->startedAnotherActivity:Z

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x6062

    aput v11, v2, v10

    const/16 v10, 0x760d

    aput v10, v2, v9

    const/16 v9, 0x7f03

    aput v9, v2, v8

    const/16 v8, -0x9f4

    aput v8, v2, v7

    const/16 v7, -0x6d

    aput v7, v2, v6

    const/16 v6, 0x4115

    aput v6, v2, v5

    const/16 v5, 0x752f

    aput v5, v2, v3

    const/16 v3, -0x1de6

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x6007

    aput v12, v1, v11

    const/16 v11, 0x7660

    aput v11, v1, v10

    const/16 v10, 0x7f76

    aput v10, v1, v9

    const/16 v9, -0x981

    aput v9, v1, v8

    const/16 v8, -0xa

    aput v8, v1, v7

    const/16 v7, 0x4147

    aput v7, v1, v6

    const/16 v6, 0x7541

    aput v6, v1, v5

    const/16 v5, -0x1d8b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_1

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0xf

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, -0x7e

    aput v17, v2, v16

    const/16 v16, -0x59c7

    aput v16, v2, v15

    const/16 v15, -0x16

    aput v15, v2, v14

    const/16 v14, -0x26ad

    aput v14, v2, v13

    const/16 v13, -0x4a

    aput v13, v2, v12

    const/16 v12, -0x1b

    aput v12, v2, v11

    const/4 v11, -0x3

    aput v11, v2, v10

    const/16 v10, -0x48

    aput v10, v2, v9

    const/16 v9, 0x1705

    aput v9, v2, v8

    const/16 v8, -0x2482

    aput v8, v2, v7

    const/16 v7, -0x43

    aput v7, v2, v6

    const/16 v6, -0x17

    aput v6, v2, v5

    const/16 v5, -0x14

    aput v5, v2, v4

    const/16 v4, -0xeef

    aput v4, v2, v3

    const/16 v3, -0x59

    aput v3, v2, v1

    const/16 v1, 0xf

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, -0x1b

    aput v18, v1, v17

    const/16 v17, -0x59aa

    aput v17, v1, v16

    const/16 v16, -0x5a

    aput v16, v1, v15

    const/16 v15, -0x26c3

    aput v15, v1, v14

    const/16 v14, -0x27

    aput v14, v1, v13

    const/16 v13, -0x74

    aput v13, v1, v12

    const/16 v12, -0x77

    aput v12, v1, v11

    const/16 v11, -0x27

    aput v11, v1, v10

    const/16 v10, 0x1766

    aput v10, v1, v9

    const/16 v9, -0x24e9

    aput v9, v1, v8

    const/16 v8, -0x25

    aput v8, v1, v7

    const/16 v7, -0x80

    aput v7, v1, v6

    const/16 v6, -0x62

    aput v6, v1, v5

    const/16 v5, -0xe8c

    aput v5, v1, v4

    const/16 v4, -0xf

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x7f

    aput v11, v2, v10

    const/16 v10, 0x527f

    aput v10, v2, v9

    const/16 v9, -0x20d9

    aput v9, v2, v8

    const/16 v8, -0x54

    aput v8, v2, v7

    const/16 v7, -0x2d

    aput v7, v2, v6

    const/16 v6, -0x6eae

    aput v6, v2, v5

    const/4 v5, -0x1

    aput v5, v2, v3

    const/16 v3, -0x7d

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x1c

    aput v12, v1, v11

    const/16 v11, 0x5212

    aput v11, v1, v10

    const/16 v10, -0x20ae

    aput v10, v1, v9

    const/16 v9, -0x21

    aput v9, v1, v8

    const/16 v8, -0x4a

    aput v8, v1, v7

    const/16 v7, -0x6f00

    aput v7, v1, v6

    const/16 v6, -0x6f

    aput v6, v1, v5

    const/16 v5, -0x14

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->getUserAccountInfo()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->getUserAccountInfo()Ljava/lang/String;

    move-result-object v1

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    :goto_7
    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0xe

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, -0x2d

    aput v20, v4, v19

    const/16 v19, 0x225d

    aput v19, v4, v18

    const/16 v18, 0x5856

    aput v18, v4, v17

    const/16 v17, -0x59ca

    aput v17, v4, v16

    const/16 v16, -0x2d

    aput v16, v4, v15

    const/16 v15, 0x2500

    aput v15, v4, v14

    const/16 v14, 0x246

    aput v14, v4, v13

    const/16 v13, 0x561

    aput v13, v4, v12

    const/16 v12, 0x1644

    aput v12, v4, v11

    const/16 v11, 0x2a60

    aput v11, v4, v10

    const/16 v10, 0x754f

    aput v10, v4, v9

    const/16 v9, -0x6af9

    aput v9, v4, v8

    const/16 v8, -0x3b

    aput v8, v4, v5

    const/4 v5, -0x8

    aput v5, v4, v3

    const/16 v3, 0xe

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, -0xd

    aput v21, v3, v20

    const/16 v20, 0x2267

    aput v20, v3, v19

    const/16 v19, 0x5822

    aput v19, v3, v18

    const/16 v18, -0x59a8

    aput v18, v3, v17

    const/16 v17, -0x5a

    aput v17, v3, v16

    const/16 v16, 0x256f

    aput v16, v3, v15

    const/16 v15, 0x225

    aput v15, v3, v14

    const/16 v14, 0x502

    aput v14, v3, v13

    const/16 v13, 0x1605

    aput v13, v3, v12

    const/16 v12, 0x2a16

    aput v12, v3, v11

    const/16 v11, 0x752a

    aput v11, v3, v10

    const/16 v10, -0x6a8b

    aput v10, v3, v9

    const/16 v9, -0x6b

    aput v9, v3, v8

    const/16 v8, -0x6b

    aput v8, v3, v5

    const/4 v5, 0x0

    :goto_8
    array-length v8, v3

    if-lt v5, v8, :cond_7

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_9
    array-length v8, v3

    if-lt v5, v8, :cond_8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v3, 0x11

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x4569

    aput v23, v4, v22

    const/16 v22, -0x781

    aput v22, v4, v21

    const/16 v21, -0x74

    aput v21, v4, v20

    const/16 v20, 0x2c52

    aput v20, v4, v19

    const/16 v19, -0x24a7

    aput v19, v4, v18

    const/16 v18, -0x4c

    aput v18, v4, v17

    const/16 v17, -0x74c7

    aput v17, v4, v16

    const/16 v16, -0x18

    aput v16, v4, v15

    const/16 v15, -0x1aa6

    aput v15, v4, v14

    const/16 v14, -0x6e

    aput v14, v4, v13

    const/16 v13, 0x4f30

    aput v13, v4, v12

    const/16 v12, 0x4901

    aput v12, v4, v11

    const/16 v11, -0x12dc

    aput v11, v4, v10

    const/16 v10, -0x33

    aput v10, v4, v9

    const/16 v9, -0x53

    aput v9, v4, v8

    const/16 v8, 0x5152

    aput v8, v4, v5

    const/16 v5, -0x3a8f

    aput v5, v4, v3

    const/16 v3, 0x11

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/4 v10, 0x3

    const/4 v11, 0x4

    const/4 v12, 0x5

    const/4 v13, 0x6

    const/4 v14, 0x7

    const/16 v15, 0x8

    const/16 v16, 0x9

    const/16 v17, 0xa

    const/16 v18, 0xb

    const/16 v19, 0xc

    const/16 v20, 0xd

    const/16 v21, 0xe

    const/16 v22, 0xf

    const/16 v23, 0x10

    const/16 v24, 0x4549

    aput v24, v3, v23

    const/16 v23, -0x7bb

    aput v23, v3, v22

    const/16 v22, -0x8

    aput v22, v3, v21

    const/16 v21, 0x2c3c

    aput v21, v3, v20

    const/16 v20, -0x24d4

    aput v20, v3, v19

    const/16 v19, -0x25

    aput v19, v3, v18

    const/16 v18, -0x74a6

    aput v18, v3, v17

    const/16 v17, -0x75

    aput v17, v3, v16

    const/16 v16, -0x1ae5

    aput v16, v3, v15

    const/16 v15, -0x1b

    aput v15, v3, v14

    const/16 v14, 0x4f5f

    aput v14, v3, v13

    const/16 v13, 0x494f

    aput v13, v3, v12

    const/16 v12, -0x12b7

    aput v12, v3, v11

    const/16 v11, -0x13

    aput v11, v3, v10

    const/16 v10, -0x6d

    aput v10, v3, v9

    const/16 v9, 0x516c

    aput v9, v3, v8

    const/16 v8, -0x3aaf

    aput v8, v3, v5

    const/4 v5, 0x0

    :goto_a
    array-length v8, v3

    if-lt v5, v8, :cond_9

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_b
    array-length v8, v3

    if-lt v5, v8, :cond_a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v3, 0x3

    new-array v4, v3, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x2

    const/16 v9, -0xe

    aput v9, v4, v8

    const/16 v8, -0x7c

    aput v8, v4, v5

    const/16 v5, -0x68

    aput v5, v4, v3

    const/4 v3, 0x3

    new-array v3, v3, [I

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const/16 v10, -0x32

    aput v10, v3, v9

    const/16 v9, -0x48

    aput v9, v3, v8

    const/16 v8, -0x48

    aput v8, v3, v5

    const/4 v5, 0x0

    :goto_c
    array-length v8, v3

    if-lt v5, v8, :cond_b

    array-length v3, v4

    new-array v3, v3, [C

    const/4 v5, 0x0

    :goto_d
    array-length v8, v3

    if-lt v5, v8, :cond_c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->isInitialSettingOngoing()Z

    move-result v3

    if-nez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setInitialSettingOngoing(Z)V

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setInputPincodeBeforeOOBE(Z)V

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const/16 v1, 0x28

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, -0x8

    aput v43, v2, v42

    const/16 v42, -0x54

    aput v42, v2, v41

    const/16 v41, -0x35

    aput v41, v2, v40

    const/16 v40, -0x33

    aput v40, v2, v39

    const/16 v39, -0x30

    aput v39, v2, v38

    const/16 v38, -0x9

    aput v38, v2, v37

    const/16 v37, -0x72

    aput v37, v2, v36

    const/16 v36, 0x3108

    aput v36, v2, v35

    const/16 v35, -0x5c90

    aput v35, v2, v34

    const/16 v34, -0x1c

    aput v34, v2, v33

    const/16 v33, -0x58b9

    aput v33, v2, v32

    const/16 v32, -0x15

    aput v32, v2, v31

    const/16 v31, 0x4d63

    aput v31, v2, v30

    const/16 v30, -0x1cdb

    aput v30, v2, v29

    const/16 v29, -0x69

    aput v29, v2, v28

    const/16 v28, 0x5728

    aput v28, v2, v27

    const/16 v27, 0x1536

    aput v27, v2, v26

    const/16 v26, 0x1470

    aput v26, v2, v25

    const/16 v25, 0x637c

    aput v25, v2, v24

    const/16 v24, -0x3df0

    aput v24, v2, v23

    const/16 v23, -0x14

    aput v23, v2, v22

    const/16 v22, 0x3374

    aput v22, v2, v21

    const/16 v21, -0x5cbd

    aput v21, v2, v20

    const/16 v20, -0x3e

    aput v20, v2, v19

    const/16 v19, -0x4f95

    aput v19, v2, v18

    const/16 v18, -0x2c

    aput v18, v2, v17

    const/16 v17, -0x77b4

    aput v17, v2, v16

    const/16 v16, -0x19

    aput v16, v2, v15

    const/16 v15, 0xa6a

    aput v15, v2, v14

    const/16 v14, -0x6b92

    aput v14, v2, v13

    const/4 v13, -0x6

    aput v13, v2, v12

    const/16 v12, -0x259e

    aput v12, v2, v11

    const/16 v11, -0xc

    aput v11, v2, v10

    const/16 v10, -0x1b

    aput v10, v2, v9

    const/16 v9, -0x29d4

    aput v9, v2, v8

    const/16 v8, -0x5b

    aput v8, v2, v7

    const/16 v7, -0x6bfc

    aput v7, v2, v6

    const/4 v6, -0x7

    aput v6, v2, v5

    const/16 v5, -0x1ce8

    aput v5, v2, v3

    const/16 v3, -0x80

    aput v3, v2, v1

    const/16 v1, 0x28

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, -0x43

    aput v44, v1, v43

    const/16 v43, -0x11

    aput v43, v1, v42

    const/16 v42, -0x7e

    aput v42, v1, v41

    const/16 v41, -0x67

    aput v41, v1, v40

    const/16 v40, -0x61

    aput v40, v1, v39

    const/16 v39, -0x47

    aput v39, v1, v38

    const/16 v38, -0x2f

    aput v38, v1, v37

    const/16 v37, 0x3144

    aput v37, v1, v36

    const/16 v36, -0x5ccf

    aput v36, v1, v35

    const/16 v35, -0x5d

    aput v35, v1, v34

    const/16 v34, -0x58fe

    aput v34, v1, v33

    const/16 v33, -0x59

    aput v33, v1, v32

    const/16 v32, 0x4d4d

    aput v32, v1, v31

    const/16 v31, -0x1cb3

    aput v31, v1, v30

    const/16 v30, -0x1d

    aput v30, v1, v29

    const/16 v29, 0x5744

    aput v29, v1, v28

    const/16 v28, 0x1557

    aput v28, v1, v27

    const/16 v27, 0x1415

    aput v27, v1, v26

    const/16 v26, 0x6314

    aput v26, v1, v25

    const/16 v25, -0x3d9d

    aput v25, v1, v24

    const/16 v24, -0x3e

    aput v24, v1, v23

    const/16 v23, 0x3304

    aput v23, v1, v22

    const/16 v22, -0x5ccd

    aput v22, v1, v21

    const/16 v21, -0x5d

    aput v21, v1, v20

    const/16 v20, -0x4fbb

    aput v20, v1, v19

    const/16 v19, -0x50

    aput v19, v1, v18

    const/16 v18, -0x77db

    aput v18, v1, v17

    const/16 v17, -0x78

    aput v17, v1, v16

    const/16 v16, 0xa18

    aput v16, v1, v15

    const/16 v15, -0x6bf6

    aput v15, v1, v14

    const/16 v14, -0x6c

    aput v14, v1, v13

    const/16 v13, -0x25fd

    aput v13, v1, v12

    const/16 v12, -0x26

    aput v12, v1, v11

    const/16 v11, -0x7a

    aput v11, v1, v10

    const/16 v10, -0x29b7

    aput v10, v1, v9

    const/16 v9, -0x2a

    aput v9, v1, v8

    const/16 v8, -0x6bd6

    aput v8, v1, v7

    const/16 v7, -0x6c

    aput v7, v1, v6

    const/16 v6, -0x1c89

    aput v6, v1, v5

    const/16 v5, -0x1d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v5, v1

    if-lt v3, v5, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v5, v1

    if-lt v3, v5, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v4, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_10
    invoke-super/range {p0 .. p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    return-void

    :cond_1
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_3
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_4
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :catch_0
    move-exception v1

    const-string v1, ""

    goto/16 :goto_6

    :catch_1
    move-exception v2

    const-string v2, ""

    goto/16 :goto_7

    :cond_7
    aget v8, v3, v5

    aget v9, v4, v5

    xor-int/2addr v8, v9

    aput v8, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_8

    :cond_8
    aget v8, v4, v5

    int-to-char v8, v8

    aput-char v8, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_9

    :cond_9
    aget v8, v3, v5

    aget v9, v4, v5

    xor-int/2addr v8, v9

    aput v8, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_a

    :cond_a
    aget v8, v4, v5

    int-to-char v8, v8

    aput-char v8, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_b

    :cond_b
    aget v8, v3, v5

    aget v9, v4, v5

    xor-int/2addr v8, v9

    aput v8, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_c

    :cond_c
    aget v8, v4, v5

    int-to-char v8, v8

    aput-char v8, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_d

    :cond_d
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_e
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_f
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/service/health/keyManager/KeyManager;->setInputPincodeBeforeOOBE(Z)V

    goto/16 :goto_10

    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setUserAccountInfo(Ljava/lang/String;)V

    goto/16 :goto_10
.end method

.method protected onResumeFragments()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResumeFragments()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->clearScreensViewsList()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->addToScreenViewsList(Landroid/view/View;)V

    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->refreshFocusables()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method protected onStart()V
    .locals 35

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x1a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x731c

    aput v30, v2, v29

    const/16 v29, 0x6c01

    aput v29, v2, v28

    const/16 v28, 0x690d

    aput v28, v2, v27

    const/16 v27, 0x91d

    aput v27, v2, v26

    const/16 v26, 0x695a

    aput v26, v2, v25

    const/16 v25, 0x5307

    aput v25, v2, v24

    const/16 v24, 0x3c1c

    aput v24, v2, v23

    const/16 v23, 0x2e1c

    aput v23, v2, v22

    const/16 v22, -0x2ce1

    aput v22, v2, v21

    const/16 v21, -0x1e

    aput v21, v2, v20

    const/16 v20, 0x301d

    aput v20, v2, v19

    const/16 v19, 0x3c44

    aput v19, v2, v18

    const/16 v18, -0x3aab

    aput v18, v2, v17

    const/16 v17, -0x4d

    aput v17, v2, v16

    const/16 v16, -0x4e

    aput v16, v2, v15

    const/16 v15, -0x499a

    aput v15, v2, v14

    const/16 v14, -0x2b

    aput v14, v2, v13

    const/16 v13, -0x26be

    aput v13, v2, v12

    const/16 v12, -0x44

    aput v12, v2, v11

    const/16 v11, 0x736f

    aput v11, v2, v10

    const/16 v10, -0x38ee

    aput v10, v2, v9

    const/16 v9, -0x7b

    aput v9, v2, v8

    const/16 v8, -0x5cea

    aput v8, v2, v7

    const/16 v7, -0x34

    aput v7, v2, v6

    const/16 v6, -0x4cb4

    aput v6, v2, v3

    const/16 v3, -0x1f

    aput v3, v2, v1

    const/16 v1, 0x1a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x7368

    aput v31, v1, v30

    const/16 v30, 0x6c73

    aput v30, v1, v29

    const/16 v29, 0x696c

    aput v29, v1, v28

    const/16 v28, 0x969

    aput v28, v1, v27

    const/16 v27, 0x6909

    aput v27, v1, v26

    const/16 v26, 0x5369

    aput v26, v1, v25

    const/16 v25, 0x3c53

    aput v25, v1, v24

    const/16 v24, 0x2e3c

    aput v24, v1, v23

    const/16 v23, -0x2cd2

    aput v23, v1, v22

    const/16 v22, -0x2d

    aput v22, v1, v21

    const/16 v21, 0x3064

    aput v21, v1, v20

    const/16 v20, 0x3c30

    aput v20, v1, v19

    const/16 v19, -0x3ac4

    aput v19, v1, v18

    const/16 v18, -0x3b

    aput v18, v1, v17

    const/16 v17, -0x25

    aput v17, v1, v16

    const/16 v16, -0x49ee

    aput v16, v1, v15

    const/16 v15, -0x4a

    aput v15, v1, v14

    const/16 v14, -0x26fd

    aput v14, v1, v13

    const/16 v13, -0x27

    aput v13, v1, v12

    const/16 v12, 0x731c

    aput v12, v1, v11

    const/16 v11, -0x388d

    aput v11, v1, v10

    const/16 v10, -0x39

    aput v10, v1, v9

    const/16 v9, -0x5c9e

    aput v9, v1, v8

    const/16 v8, -0x5d

    aput v8, v1, v7

    const/16 v7, -0x4cdd

    aput v7, v1, v6

    const/16 v6, -0x4d

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->startedAnotherActivity:Z

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, -0x30

    aput v25, v2, v24

    const/16 v24, -0x5192

    aput v24, v2, v23

    const/16 v23, -0x3f

    aput v23, v2, v22

    const/16 v22, -0xe

    aput v22, v2, v21

    const/16 v21, -0x68ff

    aput v21, v2, v20

    const/16 v20, -0x1c

    aput v20, v2, v19

    const/16 v19, 0x4834

    aput v19, v2, v18

    const/16 v18, 0x7838

    aput v18, v2, v17

    const/16 v17, 0x1227

    aput v17, v2, v16

    const/16 v16, -0x7886

    aput v16, v2, v15

    const/16 v15, -0x1c

    aput v15, v2, v14

    const/16 v14, -0x11

    aput v14, v2, v13

    const/16 v13, 0xd6c

    aput v13, v2, v12

    const/16 v12, -0x6094

    aput v12, v2, v11

    const/16 v11, -0xd

    aput v11, v2, v10

    const/16 v10, -0x6793

    aput v10, v2, v9

    const/16 v9, -0x14

    aput v9, v2, v8

    const/16 v8, -0x51

    aput v8, v2, v7

    const/16 v7, -0x78

    aput v7, v2, v6

    const/16 v6, -0x16

    aput v6, v2, v5

    const/16 v5, -0x45

    aput v5, v2, v3

    const/16 v3, -0x4f

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, -0x4c

    aput v26, v1, v25

    const/16 v25, -0x51e4

    aput v25, v1, v24

    const/16 v24, -0x52

    aput v24, v1, v23

    const/16 v23, -0x7b

    aput v23, v1, v22

    const/16 v22, -0x688e

    aput v22, v1, v21

    const/16 v21, -0x69

    aput v21, v1, v20

    const/16 v20, 0x4855

    aput v20, v1, v19

    const/16 v19, 0x7848

    aput v19, v1, v18

    const/16 v18, 0x1278

    aput v18, v1, v17

    const/16 v17, -0x78ee

    aput v17, v1, v16

    const/16 v16, -0x79

    aput v16, v1, v15

    const/16 v15, -0x7f

    aput v15, v1, v14

    const/16 v14, 0xd19

    aput v14, v1, v13

    const/16 v13, -0x60f3

    aput v13, v1, v12

    const/16 v12, -0x61

    aput v12, v1, v11

    const/16 v11, -0x67ce

    aput v11, v1, v10

    const/16 v10, -0x68

    aput v10, v1, v9

    const/16 v9, -0x36

    aput v9, v1, v8

    const/16 v8, -0x11

    aput v8, v1, v7

    const/16 v7, -0x72

    aput v7, v1, v6

    const/16 v6, -0x2e

    aput v6, v1, v5

    const/16 v5, -0x3a

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x3d3d

    aput v26, v2, v25

    const/16 v25, 0x674f

    aput v25, v2, v24

    const/16 v24, 0x2908

    aput v24, v2, v23

    const/16 v23, -0x47a2

    aput v23, v2, v22

    const/16 v22, -0x35

    aput v22, v2, v21

    const/16 v21, -0x69fd

    aput v21, v2, v20

    const/16 v20, -0x9

    aput v20, v2, v19

    const/16 v19, -0x74

    aput v19, v2, v18

    const/16 v18, -0x2bd7

    aput v18, v2, v17

    const/16 v17, -0x44

    aput v17, v2, v16

    const/16 v16, -0x62eb

    aput v16, v2, v15

    const/16 v15, -0xd

    aput v15, v2, v14

    const/16 v14, -0x4bc5

    aput v14, v2, v13

    const/16 v13, -0x2b

    aput v13, v2, v12

    const/16 v12, 0x2036

    aput v12, v2, v11

    const/16 v11, 0x67f

    aput v11, v2, v10

    const/16 v10, -0x108e

    aput v10, v2, v9

    const/16 v9, -0x76

    aput v9, v2, v8

    const/16 v8, 0x545

    aput v8, v2, v7

    const/16 v7, -0x519f

    aput v7, v2, v6

    const/16 v6, -0x39

    aput v6, v2, v3

    const/16 v3, -0x74d3

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x3d59

    aput v27, v1, v26

    const/16 v26, 0x673d

    aput v26, v1, v25

    const/16 v25, 0x2967

    aput v25, v1, v24

    const/16 v24, -0x47d7

    aput v24, v1, v23

    const/16 v23, -0x48

    aput v23, v1, v22

    const/16 v22, -0x6990

    aput v22, v1, v21

    const/16 v21, -0x6a

    aput v21, v1, v20

    const/16 v20, -0x4

    aput v20, v1, v19

    const/16 v19, -0x2b8a

    aput v19, v1, v18

    const/16 v18, -0x2c

    aput v18, v1, v17

    const/16 v17, -0x628a

    aput v17, v1, v16

    const/16 v16, -0x63

    aput v16, v1, v15

    const/16 v15, -0x4bb2

    aput v15, v1, v14

    const/16 v14, -0x4c

    aput v14, v1, v13

    const/16 v13, 0x205a

    aput v13, v1, v12

    const/16 v12, 0x620

    aput v12, v1, v11

    const/16 v11, -0x10fa

    aput v11, v1, v10

    const/16 v10, -0x11

    aput v10, v1, v9

    const/16 v9, 0x522

    aput v9, v1, v8

    const/16 v8, -0x51fb

    aput v8, v1, v7

    const/16 v7, -0x52

    aput v7, v1, v6

    const/16 v6, -0x74a6

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v6, v1

    if-lt v3, v6, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v6, v1

    if-lt v3, v6, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x16

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, -0x61

    aput v26, v2, v25

    const/16 v25, 0x1f7b

    aput v25, v2, v24

    const/16 v24, 0x7a70

    aput v24, v2, v23

    const/16 v23, 0x650d

    aput v23, v2, v22

    const/16 v22, 0xf16

    aput v22, v2, v21

    const/16 v21, 0x97c

    aput v21, v2, v20

    const/16 v20, 0x2168

    aput v20, v2, v19

    const/16 v19, 0x7051

    aput v19, v2, v18

    const/16 v18, -0x76d1

    aput v18, v2, v17

    const/16 v17, -0x1f

    aput v17, v2, v16

    const/16 v16, -0xea4

    aput v16, v2, v15

    const/16 v15, -0x61

    aput v15, v2, v14

    const/16 v14, -0x53

    aput v14, v2, v13

    const/16 v13, 0x655a

    aput v13, v2, v12

    const/16 v12, 0x4309

    aput v12, v2, v11

    const/16 v11, 0x51c

    aput v11, v2, v10

    const/16 v10, -0x4e8f

    aput v10, v2, v9

    const/16 v9, -0x2c

    aput v9, v2, v8

    const/16 v8, -0x78b5

    aput v8, v2, v7

    const/16 v7, -0x1d

    aput v7, v2, v6

    const/16 v6, -0x14e4

    aput v6, v2, v3

    const/16 v3, -0x64

    aput v3, v2, v1

    const/16 v1, 0x16

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, -0x5

    aput v27, v1, v26

    const/16 v26, 0x1f09

    aput v26, v1, v25

    const/16 v25, 0x7a1f

    aput v25, v1, v24

    const/16 v24, 0x657a

    aput v24, v1, v23

    const/16 v23, 0xf65

    aput v23, v1, v22

    const/16 v22, 0x90f

    aput v22, v1, v21

    const/16 v21, 0x2109

    aput v21, v1, v20

    const/16 v20, 0x7021

    aput v20, v1, v19

    const/16 v19, -0x7690

    aput v19, v1, v18

    const/16 v18, -0x77

    aput v18, v1, v17

    const/16 v17, -0xec1

    aput v17, v1, v16

    const/16 v16, -0xf

    aput v16, v1, v15

    const/16 v15, -0x28

    aput v15, v1, v14

    const/16 v14, 0x653b

    aput v14, v1, v13

    const/16 v13, 0x4365

    aput v13, v1, v12

    const/16 v12, 0x543

    aput v12, v1, v11

    const/16 v11, -0x4efb

    aput v11, v1, v10

    const/16 v10, -0x4f

    aput v10, v1, v9

    const/16 v9, -0x78d4

    aput v9, v1, v8

    const/16 v8, -0x79

    aput v8, v1, v7

    const/16 v7, -0x148b

    aput v7, v1, v6

    const/16 v6, -0x15

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v6, v1

    if-lt v3, v6, :cond_c

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v6, v1

    if-lt v3, v6, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v5, :cond_1

    sget-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->sIsDownloadedPlugin:Z

    if-nez v1, :cond_3

    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, -0x17

    aput v24, v2, v23

    const/16 v23, -0x55

    aput v23, v2, v22

    const/16 v22, -0x36

    aput v22, v2, v21

    const/16 v21, -0x6b

    aput v21, v2, v20

    const/16 v20, -0x1

    aput v20, v2, v19

    const/16 v19, -0x2a

    aput v19, v2, v18

    const/16 v18, -0x1e4

    aput v18, v2, v17

    const/16 v17, -0x5f

    aput v17, v2, v16

    const/16 v16, -0x23

    aput v16, v2, v15

    const/16 v15, 0x523e

    aput v15, v2, v14

    const/16 v14, -0x1ede

    aput v14, v2, v13

    const/16 v13, -0x42

    aput v13, v2, v12

    const/16 v12, -0x46

    aput v12, v2, v11

    const/4 v11, -0x1

    aput v11, v2, v10

    const/16 v10, -0x47

    aput v10, v2, v9

    const/16 v9, 0xe23

    aput v9, v2, v8

    const/16 v8, -0x1a85

    aput v8, v2, v7

    const/16 v7, -0x7a

    aput v7, v2, v6

    const/16 v6, -0x56ff

    aput v6, v2, v3

    const/16 v3, -0x26

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, -0x73

    aput v25, v1, v24

    const/16 v24, -0x32

    aput v24, v1, v23

    const/16 v23, -0x5a

    aput v23, v1, v22

    const/16 v22, -0x9

    aput v22, v1, v21

    const/16 v21, -0x62

    aput v21, v1, v20

    const/16 v20, -0x48

    aput v20, v1, v19

    const/16 v19, -0x187

    aput v19, v1, v18

    const/16 v18, -0x2

    aput v18, v1, v17

    const/16 v17, -0x4d

    aput v17, v1, v16

    const/16 v16, 0x5257

    aput v16, v1, v15

    const/16 v15, -0x1eae

    aput v15, v1, v14

    const/16 v14, -0x1f

    aput v14, v1, v13

    const/16 v13, -0x3d

    aput v13, v1, v12

    const/16 v12, -0x75

    aput v12, v1, v11

    const/16 v11, -0x30

    aput v11, v1, v10

    const/16 v10, 0xe51

    aput v10, v1, v9

    const/16 v9, -0x1af2

    aput v9, v1, v8

    const/16 v8, -0x1b

    aput v8, v1, v7

    const/16 v7, -0x569c

    aput v7, v1, v6

    const/16 v6, -0x57

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v6, v1

    if-lt v3, v6, :cond_e

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v6, v1

    if-lt v3, v6, :cond_f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x6a

    aput v20, v2, v19

    const/16 v19, -0x1b

    aput v19, v2, v18

    const/16 v18, -0xc

    aput v18, v2, v17

    const/16 v17, -0x14f2

    aput v17, v2, v16

    const/16 v16, -0x68

    aput v16, v2, v15

    const/16 v15, -0x4f85

    aput v15, v2, v14

    const/16 v14, -0x2f

    aput v14, v2, v13

    const/16 v13, -0x1b

    aput v13, v2, v12

    const/16 v12, -0x3a

    aput v12, v2, v11

    const/16 v11, -0x34

    aput v11, v2, v10

    const/16 v10, -0x6f

    aput v10, v2, v9

    const/16 v9, 0x5f49

    aput v9, v2, v8

    const/16 v8, 0x142a

    aput v8, v2, v7

    const/16 v7, 0x765

    aput v7, v2, v6

    const/16 v6, -0x1e9e

    aput v6, v2, v3

    const/16 v3, -0x6d

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, -0xe

    aput v21, v1, v20

    const/16 v20, -0x69

    aput v20, v1, v19

    const/16 v19, -0x65

    aput v19, v1, v18

    const/16 v18, -0x1487

    aput v18, v1, v17

    const/16 v17, -0x15

    aput v17, v1, v16

    const/16 v16, -0x4ff8

    aput v16, v1, v15

    const/16 v15, -0x50

    aput v15, v1, v14

    const/16 v14, -0x6b

    aput v14, v1, v13

    const/16 v13, -0x67

    aput v13, v1, v12

    const/16 v12, -0x48

    aput v12, v1, v11

    const/16 v11, -0x1e

    aput v11, v1, v10

    const/16 v10, 0x5f2c

    aput v10, v1, v9

    const/16 v9, 0x145f

    aput v9, v1, v8

    const/16 v8, 0x714

    aput v8, v1, v7

    const/16 v7, -0x1ef9

    aput v7, v1, v6

    const/16 v6, -0x1f

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v6, v1

    if-lt v3, v6, :cond_10

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v6, v1

    if-lt v3, v6, :cond_11

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->cancelPinRequest:I

    const/16 v2, 0x457

    if-eq v1, v2, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->isTryActivity()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/PincodeResetService;->isPasswordServiceReceived:Z

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x7dc4

    aput v19, v2, v18

    const/16 v18, -0xa

    aput v18, v2, v17

    const/16 v17, 0x3d4a

    aput v17, v2, v16

    const/16 v16, -0x1eb5

    aput v16, v2, v15

    const/16 v15, -0x78

    aput v15, v2, v14

    const/16 v14, -0x14

    aput v14, v2, v13

    const/16 v13, -0x17

    aput v13, v2, v12

    const/16 v12, 0x3573

    aput v12, v2, v11

    const/16 v11, 0x1750

    aput v11, v2, v10

    const/16 v10, 0x2564

    aput v10, v2, v9

    const/16 v9, -0x7bbc

    aput v9, v2, v8

    const/16 v8, -0x3a

    aput v8, v2, v7

    const/16 v7, 0x3978

    aput v7, v2, v6

    const/16 v6, -0xcaa

    aput v6, v2, v5

    const/16 v5, -0x64

    aput v5, v2, v3

    const/16 v3, -0x9

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x7dbb

    aput v20, v1, v19

    const/16 v19, -0x7e

    aput v19, v1, v18

    const/16 v18, 0x3d23

    aput v18, v1, v17

    const/16 v17, -0x1ec3

    aput v17, v1, v16

    const/16 v16, -0x1f

    aput v16, v1, v15

    const/16 v15, -0x68

    aput v15, v1, v14

    const/16 v14, -0x76

    aput v14, v1, v13

    const/16 v13, 0x3532

    aput v13, v1, v12

    const/16 v12, 0x1735

    aput v12, v1, v11

    const/16 v11, 0x2517

    aput v11, v1, v10

    const/16 v10, -0x7bdb

    aput v10, v1, v9

    const/16 v9, -0x7c

    aput v9, v1, v8

    const/16 v8, 0x390c

    aput v8, v1, v7

    const/16 v7, -0xcc7

    aput v7, v1, v6

    const/16 v6, -0xd

    aput v6, v1, v5

    const/16 v5, -0x5b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v5, v1

    if-lt v3, v5, :cond_12

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v5, v1

    if-lt v3, v5, :cond_13

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, -0x26

    aput v33, v2, v32

    const/16 v32, -0xf

    aput v32, v2, v31

    const/16 v31, -0x8

    aput v31, v2, v30

    const/16 v30, -0x72

    aput v30, v2, v29

    const/16 v29, -0x5a

    aput v29, v2, v28

    const/16 v28, -0x3f

    aput v28, v2, v27

    const/16 v27, -0x6d

    aput v27, v2, v26

    const/16 v26, -0x27

    aput v26, v2, v25

    const/16 v25, 0x5e77

    aput v25, v2, v24

    const/16 v24, 0x492e

    aput v24, v2, v23

    const/16 v23, -0xbd9

    aput v23, v2, v22

    const/16 v22, -0x43

    aput v22, v2, v21

    const/16 v21, 0x6331

    aput v21, v2, v20

    const/16 v20, 0x6b0b

    aput v20, v2, v19

    const/16 v19, 0xc08

    aput v19, v2, v18

    const/16 v18, 0x2162

    aput v18, v2, v17

    const/16 v17, 0x3254

    aput v17, v2, v16

    const/16 v16, -0x6dad

    aput v16, v2, v15

    const/4 v15, -0x2

    aput v15, v2, v14

    const/16 v14, -0x52e4

    aput v14, v2, v13

    const/16 v13, -0x6d

    aput v13, v2, v12

    const/16 v12, 0x1c44

    aput v12, v2, v11

    const/16 v11, -0x2698

    aput v11, v2, v10

    const/16 v10, -0x55

    aput v10, v2, v9

    const/16 v9, 0x21

    aput v9, v2, v8

    const/16 v8, 0x7c74

    aput v8, v2, v7

    const/16 v7, 0x562f

    aput v7, v2, v6

    const/16 v6, -0xc8

    aput v6, v2, v3

    const/16 v3, -0x70

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, 0x1c

    const/16 v34, -0x41

    aput v34, v1, v33

    const/16 v33, -0x6b

    aput v33, v1, v32

    const/16 v32, -0x69

    aput v32, v1, v31

    const/16 v31, -0x13

    aput v31, v1, v30

    const/16 v30, -0x38

    aput v30, v1, v29

    const/16 v29, -0x58

    aput v29, v1, v28

    const/16 v28, -0x3d

    aput v28, v1, v27

    const/16 v27, -0x53

    aput v27, v1, v26

    const/16 v26, 0x5e02

    aput v26, v1, v25

    const/16 v25, 0x495e

    aput v25, v1, v24

    const/16 v24, -0xbb7

    aput v24, v1, v23

    const/16 v23, -0xc

    aput v23, v1, v22

    const/16 v22, 0x6311

    aput v22, v1, v21

    const/16 v21, 0x6b63

    aput v21, v1, v20

    const/16 v20, 0xc6b

    aput v20, v1, v19

    const/16 v19, 0x210c

    aput v19, v1, v18

    const/16 v18, 0x3221

    aput v18, v1, v17

    const/16 v17, -0x6dce

    aput v17, v1, v16

    const/16 v16, -0x6e

    aput v16, v1, v15

    const/16 v15, -0x52c4

    aput v15, v1, v14

    const/16 v14, -0x53

    aput v14, v1, v13

    const/16 v13, 0x1c64

    aput v13, v1, v12

    const/16 v12, -0x26e4

    aput v12, v1, v11

    const/16 v11, -0x27

    aput v11, v1, v10

    const/16 v10, 0x40

    aput v10, v1, v9

    const/16 v9, 0x7c00

    aput v9, v1, v8

    const/16 v8, 0x567c

    aput v8, v1, v7

    const/16 v7, -0xaa

    aput v7, v1, v6

    const/4 v6, -0x1

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v6, v1

    if-lt v3, v6, :cond_14

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v6, v1

    if-lt v3, v6, :cond_15

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/sec/android/app/shealth/framework/ui/base/InputPincode;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/16 v1, 0x14

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x1101

    aput v24, v2, v23

    const/16 v23, 0x7a7e

    aput v23, v2, v22

    const/16 v22, 0x1c13

    aput v22, v2, v21

    const/16 v21, 0x6468

    aput v21, v2, v20

    const/16 v20, 0x6f07

    aput v20, v2, v19

    const/16 v19, -0x7ad2

    aput v19, v2, v18

    const/16 v18, -0x4

    aput v18, v2, v17

    const/16 v17, -0x3e

    aput v17, v2, v16

    const/16 v16, 0x4d79

    aput v16, v2, v15

    const/16 v15, -0x2fc5

    aput v15, v2, v14

    const/16 v14, -0x47

    aput v14, v2, v13

    const/16 v13, -0x5f

    aput v13, v2, v12

    const/16 v12, -0x4e

    aput v12, v2, v11

    const/16 v11, -0x2e

    aput v11, v2, v10

    const/16 v10, -0x6ea6

    aput v10, v2, v9

    const/16 v9, -0xc

    aput v9, v2, v8

    const/16 v8, -0x23

    aput v8, v2, v7

    const/16 v7, 0x2108

    aput v7, v2, v6

    const/16 v6, 0x5a48

    aput v6, v2, v3

    const/16 v3, 0x212d

    aput v3, v2, v1

    const/16 v1, 0x14

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x116f

    aput v25, v1, v24

    const/16 v24, 0x7a11

    aput v24, v1, v23

    const/16 v23, 0x1c7a

    aput v23, v1, v22

    const/16 v22, 0x641c

    aput v22, v1, v21

    const/16 v21, 0x6f64

    aput v21, v1, v20

    const/16 v20, -0x7a91

    aput v20, v1, v19

    const/16 v19, -0x7b

    aput v19, v1, v18

    const/16 v18, -0x4a

    aput v18, v1, v17

    const/16 v17, 0x4d10

    aput v17, v1, v16

    const/16 v16, -0x2fb3

    aput v16, v1, v15

    const/16 v15, -0x30

    aput v15, v1, v14

    const/16 v14, -0x2b

    aput v14, v1, v13

    const/16 v13, -0x2f

    aput v13, v1, v12

    const/16 v12, -0x6d

    aput v12, v1, v11

    const/16 v11, -0x6ed2

    aput v11, v1, v10

    const/16 v10, -0x6f

    aput v10, v1, v9

    const/16 v9, -0x46

    aput v9, v1, v8

    const/16 v8, 0x216c

    aput v8, v1, v7

    const/16 v7, 0x5a21

    aput v7, v1, v6

    const/16 v6, 0x215a

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v6, v1

    if-lt v3, v6, :cond_16

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v6, v1

    if-lt v3, v6, :cond_17

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    :cond_2
    const/high16 v1, 0x20000

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->startActivity(Landroid/content/Intent;)V

    :cond_3
    :goto_12
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    sget-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->sIsDownloadedPlugin:Z

    if-nez v1, :cond_4

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x26

    aput v19, v2, v18

    const/16 v18, -0x79

    aput v18, v2, v17

    const/16 v17, -0x3a

    aput v17, v2, v16

    const/16 v16, 0xc61

    aput v16, v2, v15

    const/16 v15, 0x537f

    aput v15, v2, v14

    const/16 v14, -0x45e0

    aput v14, v2, v13

    const/16 v13, -0x25

    aput v13, v2, v12

    const/16 v12, -0x189b

    aput v12, v2, v11

    const/16 v11, -0x48

    aput v11, v2, v10

    const/16 v10, -0x5d

    aput v10, v2, v9

    const/16 v9, -0x2b

    aput v9, v2, v8

    const/16 v8, 0x6218

    aput v8, v2, v7

    const/16 v7, -0x5ee9

    aput v7, v2, v6

    const/16 v6, -0x30

    aput v6, v2, v5

    const/16 v5, -0x29

    aput v5, v2, v3

    const/16 v3, -0x39

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x42

    aput v20, v1, v19

    const/16 v19, -0xb

    aput v19, v1, v18

    const/16 v18, -0x57

    aput v18, v1, v17

    const/16 v17, 0xc16

    aput v17, v1, v16

    const/16 v16, 0x530c

    aput v16, v1, v15

    const/16 v15, -0x45ad

    aput v15, v1, v14

    const/16 v14, -0x46

    aput v14, v1, v13

    const/16 v13, -0x18eb

    aput v13, v1, v12

    const/16 v12, -0x19

    aput v12, v1, v11

    const/16 v11, -0x29

    aput v11, v1, v10

    const/16 v10, -0x5a

    aput v10, v1, v9

    const/16 v9, 0x627d

    aput v9, v1, v8

    const/16 v8, -0x5e9e

    aput v8, v1, v7

    const/16 v7, -0x5f

    aput v7, v1, v6

    const/16 v6, -0x4e

    aput v6, v1, v5

    const/16 v5, -0x4b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_13
    array-length v5, v1

    if-lt v3, v5, :cond_18

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_14
    array-length v5, v1

    if-lt v3, v5, :cond_19

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_4
    :goto_15
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->cancelPinRequest:I

    const/16 v2, 0x457

    if-ne v1, v2, :cond_5

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->cancelPinRequest:I

    :cond_5
    invoke-super/range {p0 .. p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    return-void

    :cond_6
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_7
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_8
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_9
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_a
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_b
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_c
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_d
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_e
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_f
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_10
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_11
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b

    :cond_12
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_c

    :cond_13
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_d

    :cond_14
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_15
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_16
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_10

    :cond_17
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_11

    :cond_18
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_13

    :cond_19
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_14

    :catch_0
    move-exception v1

    goto/16 :goto_12

    :catch_1
    move-exception v1

    goto/16 :goto_15
.end method

.method protected onStop()V
    .locals 31

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x19

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, -0x3b

    aput v29, v2, v28

    const/16 v28, -0x76c1

    aput v28, v2, v27

    const/16 v27, -0x3

    aput v27, v2, v26

    const/16 v26, -0x65fa

    aput v26, v2, v25

    const/16 v25, -0xc

    aput v25, v2, v24

    const/16 v24, 0x6517

    aput v24, v2, v23

    const/16 v23, 0x7445

    aput v23, v2, v22

    const/16 v22, 0x4e45

    aput v22, v2, v21

    const/16 v21, -0x5381

    aput v21, v2, v20

    const/16 v20, -0x2b

    aput v20, v2, v19

    const/16 v19, -0x50ae

    aput v19, v2, v18

    const/16 v18, -0x3a

    aput v18, v2, v17

    const/16 v17, -0x14a3

    aput v17, v2, v16

    const/16 v16, -0x7e

    aput v16, v2, v15

    const/16 v15, 0x3177

    aput v15, v2, v14

    const/16 v14, 0x5b52

    aput v14, v2, v13

    const/16 v13, -0x5e6

    aput v13, v2, v12

    const/16 v12, -0x61

    aput v12, v2, v11

    const/16 v11, -0x12

    aput v11, v2, v10

    const/16 v10, 0x387e

    aput v10, v2, v9

    const/16 v9, -0x4686

    aput v9, v2, v8

    const/16 v8, -0x33

    aput v8, v2, v7

    const/16 v7, 0x6a1b

    aput v7, v2, v6

    const/16 v6, 0x7d05

    aput v6, v2, v3

    const/16 v3, 0x592f

    aput v3, v2, v1

    const/16 v1, 0x19

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, -0x4b

    aput v30, v1, v29

    const/16 v29, -0x76b0

    aput v29, v1, v28

    const/16 v28, -0x77

    aput v28, v1, v27

    const/16 v27, -0x65ab

    aput v27, v1, v26

    const/16 v26, -0x66

    aput v26, v1, v25

    const/16 v25, 0x6578

    aput v25, v1, v24

    const/16 v24, 0x7465

    aput v24, v1, v23

    const/16 v23, 0x4e74

    aput v23, v1, v22

    const/16 v22, -0x53b2

    aput v22, v1, v21

    const/16 v21, -0x54

    aput v21, v1, v20

    const/16 v20, -0x50da

    aput v20, v1, v19

    const/16 v19, -0x51

    aput v19, v1, v18

    const/16 v18, -0x14d5

    aput v18, v1, v17

    const/16 v17, -0x15

    aput v17, v1, v16

    const/16 v16, 0x3103

    aput v16, v1, v15

    const/16 v15, 0x5b31

    aput v15, v1, v14

    const/16 v14, -0x5a5

    aput v14, v1, v13

    const/4 v13, -0x6

    aput v13, v1, v12

    const/16 v12, -0x63

    aput v12, v1, v11

    const/16 v11, 0x381f

    aput v11, v1, v10

    const/16 v10, -0x46c8

    aput v10, v1, v9

    const/16 v9, -0x47

    aput v9, v1, v8

    const/16 v8, 0x6a74

    aput v8, v1, v7

    const/16 v7, 0x7d6a

    aput v7, v1, v6

    const/16 v6, 0x597d

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-lt v3, v6, :cond_3

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v6, v1

    if-lt v3, v6, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/16 v9, -0x59b2

    aput v9, v2, v8

    const/16 v8, -0x37

    aput v8, v2, v7

    const/16 v7, 0x2b7c

    aput v7, v2, v6

    const/16 v6, 0x1378

    aput v6, v2, v5

    const/16 v5, 0x617d

    aput v5, v2, v3

    const/16 v3, 0x690e

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x59c2

    aput v10, v1, v9

    const/16 v9, -0x5a

    aput v9, v1, v8

    const/16 v8, 0x2b08

    aput v8, v1, v7

    const/16 v7, 0x132b

    aput v7, v1, v6

    const/16 v6, 0x6113

    aput v6, v1, v5

    const/16 v5, 0x6961

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_5

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->getPinWallPaper()Z

    move-result v1

    sget-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->startedAnotherActivity:Z

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->isChangingConfigurations()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    :cond_0
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    :cond_1
    sget-boolean v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->sIsDownloadedPlugin:Z

    if-nez v1, :cond_2

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x5d

    aput v19, v2, v18

    const/16 v18, -0x5d

    aput v18, v2, v17

    const/16 v17, 0x1b11

    aput v17, v2, v16

    const/16 v16, 0x246c

    aput v16, v2, v15

    const/16 v15, -0x6a9

    aput v15, v2, v14

    const/16 v14, -0x76

    aput v14, v2, v13

    const/16 v13, 0x1046

    aput v13, v2, v12

    const/16 v12, -0x15a0

    aput v12, v2, v11

    const/16 v11, -0x4b

    aput v11, v2, v10

    const/16 v10, -0x69b8

    aput v10, v2, v9

    const/16 v9, -0x1b

    aput v9, v2, v8

    const/16 v8, -0x79ae

    aput v8, v2, v7

    const/16 v7, -0xd

    aput v7, v2, v6

    const/4 v6, 0x5

    aput v6, v2, v5

    const/16 v5, -0x319b

    aput v5, v2, v3

    const/16 v3, -0x44

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x39

    aput v20, v1, v19

    const/16 v19, -0x2f

    aput v19, v1, v18

    const/16 v18, 0x1b7e

    aput v18, v1, v17

    const/16 v17, 0x241b

    aput v17, v1, v16

    const/16 v16, -0x6dc

    aput v16, v1, v15

    const/4 v15, -0x7

    aput v15, v1, v14

    const/16 v14, 0x1027

    aput v14, v1, v13

    const/16 v13, -0x15f0

    aput v13, v1, v12

    const/16 v12, -0x16

    aput v12, v1, v11

    const/16 v11, -0x69c4

    aput v11, v1, v10

    const/16 v10, -0x6a

    aput v10, v1, v9

    const/16 v9, -0x79c9

    aput v9, v1, v8

    const/16 v8, -0x7a

    aput v8, v1, v7

    const/16 v7, 0x74

    aput v7, v1, v6

    const/16 v6, -0x3200

    aput v6, v1, v5

    const/16 v5, -0x32

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_7

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_2
    :goto_6
    invoke-super/range {p0 .. p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    return-void

    :cond_3
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_4
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_5
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_6
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_7
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :catch_0
    move-exception v1

    goto :goto_6
.end method

.method protected prepareShareView()V
    .locals 46

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-gt v1, v2, :cond_2

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    if-eqz v1, :cond_2

    sget-object v4, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->TAG:Ljava/lang/String;

    const/16 v1, 0x29

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, -0x42

    aput v44, v2, v43

    const/16 v43, -0xb

    aput v43, v2, v42

    const/16 v42, -0x18b4

    aput v42, v2, v41

    const/16 v41, -0x6b

    aput v41, v2, v40

    const/16 v40, -0x22

    aput v40, v2, v39

    const/16 v39, -0x76

    aput v39, v2, v38

    const/16 v38, -0x5a

    aput v38, v2, v37

    const/16 v37, 0x7d22

    aput v37, v2, v36

    const/16 v36, -0x34a3

    aput v36, v2, v35

    const/16 v35, -0x5b

    aput v35, v2, v34

    const/16 v34, 0x2e0d

    aput v34, v2, v33

    const/16 v33, -0x50f2

    aput v33, v2, v32

    const/16 v32, -0x24

    aput v32, v2, v31

    const/16 v31, -0x4

    aput v31, v2, v30

    const/16 v30, 0x4540

    aput v30, v2, v29

    const/16 v29, 0x2f31

    aput v29, v2, v28

    const/16 v28, 0x5f5c

    aput v28, v2, v27

    const/16 v27, 0x453a

    aput v27, v2, v26

    const/16 v26, 0x2d30

    aput v26, v2, v25

    const/16 v25, 0xb5c

    aput v25, v2, v24

    const/16 v24, -0x6c92

    aput v24, v2, v23

    const/16 v23, -0x1f

    aput v23, v2, v22

    const/16 v22, -0x79

    aput v22, v2, v21

    const/16 v21, -0x79

    aput v21, v2, v20

    const/16 v20, -0x53

    aput v20, v2, v19

    const/16 v19, -0x43e3

    aput v19, v2, v18

    const/16 v18, -0x64

    aput v18, v2, v17

    const/16 v17, -0x5c

    aput v17, v2, v16

    const/16 v16, -0x13

    aput v16, v2, v15

    const/16 v15, 0x167c

    aput v15, v2, v14

    const/16 v14, 0x617e

    aput v14, v2, v13

    const/16 v13, -0x6eee

    aput v13, v2, v12

    const/16 v12, -0x4f

    aput v12, v2, v11

    const/16 v11, -0x3288

    aput v11, v2, v10

    const/16 v10, -0x48

    aput v10, v2, v9

    const/16 v9, 0x3d0a

    aput v9, v2, v8

    const/16 v8, -0x30ac

    aput v8, v2, v7

    const/16 v7, -0x47

    aput v7, v2, v6

    const/16 v6, 0x5f0c

    aput v6, v2, v5

    const/16 v5, 0x742d

    aput v5, v2, v3

    const/16 v3, 0x5c24

    aput v3, v2, v1

    const/16 v1, 0x29

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, -0x33

    aput v45, v1, v44

    const/16 v44, -0x7a

    aput v44, v1, v43

    const/16 v43, -0x18d7

    aput v43, v1, v42

    const/16 v42, -0x19

    aput v42, v1, v41

    const/16 v41, -0x47

    aput v41, v1, v40

    const/16 v40, -0x1b

    aput v40, v1, v39

    const/16 v39, -0x2c

    aput v39, v1, v38

    const/16 v38, 0x7d52

    aput v38, v1, v37

    const/16 v37, -0x3483

    aput v37, v1, v36

    const/16 v36, -0x35

    aput v36, v1, v35

    const/16 v35, 0x2e64

    aput v35, v1, v34

    const/16 v34, -0x50d2

    aput v34, v1, v33

    const/16 v33, -0x51

    aput v33, v1, v32

    const/16 v32, -0x6b

    aput v32, v1, v31

    const/16 v31, 0x4560

    aput v31, v1, v30

    const/16 v30, 0x2f45

    aput v30, v1, v29

    const/16 v29, 0x5f2f

    aput v29, v1, v28

    const/16 v28, 0x455f

    aput v28, v1, v27

    const/16 v27, 0x2d45

    aput v27, v1, v26

    const/16 v26, 0xb2d

    aput v26, v1, v25

    const/16 v25, -0x6cf5

    aput v25, v1, v24

    const/16 v24, -0x6d

    aput v24, v1, v23

    const/16 v23, -0x59

    aput v23, v1, v22

    const/16 v22, -0x1a

    aput v22, v1, v21

    const/16 v21, -0x3c

    aput v21, v1, v20

    const/16 v20, -0x4395

    aput v20, v1, v19

    const/16 v19, -0x44

    aput v19, v1, v18

    const/16 v18, -0x3f

    aput v18, v1, v17

    const/16 v17, -0x61

    aput v17, v1, v16

    const/16 v16, 0x161d

    aput v16, v1, v15

    const/16 v15, 0x6116

    aput v15, v1, v14

    const/16 v14, -0x6e9f

    aput v14, v1, v13

    const/16 v13, -0x6f

    aput v13, v1, v12

    const/16 v12, -0x32f5

    aput v12, v1, v11

    const/16 v11, -0x33

    aput v11, v1, v10

    const/16 v10, 0x3d65

    aput v10, v1, v9

    const/16 v9, -0x30c3

    aput v9, v1, v8

    const/16 v8, -0x31

    aput v8, v1, v7

    const/16 v7, 0x5f69

    aput v7, v1, v6

    const/16 v6, 0x745f

    aput v6, v1, v5

    const/16 v5, 0x5c74

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->addBackgroundView()Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mBackGroundView:Landroid/view/View;

    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    :catch_0
    move-exception v1

    sget v1, Lcom/sec/android/app/shealth/framework/ui/R$string;->share_image_storage_error:I

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    goto :goto_2
.end method

.method protected refreshFocusables()V
    .locals 4

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public setContentView(I)V
    .locals 13

    const-wide/16 v11, 0x0

    const-wide v9, -0x5569d7ff2c9773d1L

    const/4 v8, 0x1

    const/16 v7, 0x20

    const/4 v3, 0x0

    const/4 v0, 0x2

    new-array v2, v0, [J

    const-wide/16 v0, 0x1

    aput-wide v0, v2, v8

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    int-to-long v0, p1

    shl-long/2addr v0, v7

    ushr-long v4, v0, v7

    aget-wide v0, v2, v3

    cmp-long v6, v0, v11

    if-eqz v6, :cond_1

    xor-long/2addr v0, v9

    :cond_1
    ushr-long/2addr v0, v7

    shl-long/2addr v0, v7

    xor-long/2addr v0, v4

    xor-long/2addr v0, v9

    aput-wide v0, v2, v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, v2, v0

    long-to-int v0, v0

    if-gtz v0, :cond_4

    new-instance v4, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-array v1, v8, [I

    const/16 v0, -0x78da

    aput v0, v1, v3

    new-array v0, v8, [I

    const/16 v2, -0x78ea

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v5, v0

    if-lt v2, v5, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget v5, v0, v2

    aget v6, v1, v2

    xor-int/2addr v5, v6

    aput v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    aget-wide v0, v2, v3

    cmp-long v2, v0, v11

    if-eqz v2, :cond_5

    xor-long/2addr v0, v9

    :cond_5
    shl-long/2addr v0, v7

    shr-long/2addr v0, v7

    long-to-int v0, v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->initView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mContent:Landroid/view/View;

    invoke-super {p0, v0}, Landroid/support/v4/app/FragmentActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->initView(Landroid/view/View;)V

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 30

    const/4 v2, 0x2

    new-array v5, v2, [J

    const/4 v2, 0x1

    const-wide/16 v3, 0x1

    aput-wide v3, v5, v2

    const/4 v2, 0x0

    array-length v3, v5

    add-int/lit8 v3, v3, -0x1

    aget-wide v3, v5, v3

    long-to-int v3, v3

    if-gtz v3, :cond_0

    new-instance v3, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    const/4 v4, 0x0

    move/from16 v0, p2

    int-to-long v2, v0

    const/16 v6, 0x20

    shl-long/2addr v2, v6

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    aget-wide v2, v5, v4

    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_1

    const-wide v8, 0x7d20f293b355ca7dL    # 5.411941733237619E294

    xor-long/2addr v2, v8

    :cond_1
    const/16 v8, 0x20

    ushr-long/2addr v2, v8

    const/16 v8, 0x20

    shl-long/2addr v2, v8

    xor-long/2addr v2, v6

    const-wide v6, 0x7d20f293b355ca7dL    # 5.411941733237619E294

    xor-long/2addr v2, v6

    aput-wide v2, v5, v4

    const/16 v2, 0x18

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, -0x24

    aput v28, v3, v27

    const/16 v27, -0xb

    aput v27, v3, v26

    const/16 v26, -0x41

    aput v26, v3, v25

    const/16 v25, -0x45d1

    aput v25, v3, v24

    const/16 v24, -0x22

    aput v24, v3, v23

    const/16 v23, -0x48

    aput v23, v3, v22

    const/16 v22, 0xb33

    aput v22, v3, v21

    const/16 v21, -0x3e87

    aput v21, v3, v20

    const/16 v20, -0x5d

    aput v20, v3, v19

    const/16 v19, -0x2b

    aput v19, v3, v18

    const/16 v18, -0x69

    aput v18, v3, v17

    const/16 v17, -0x3b86

    aput v17, v3, v16

    const/16 v16, -0x5d

    aput v16, v3, v15

    const/16 v15, -0x67c6

    aput v15, v3, v14

    const/16 v14, -0xf

    aput v14, v3, v13

    const/16 v13, -0x28

    aput v13, v3, v12

    const/16 v12, -0x69b4

    aput v12, v3, v11

    const/4 v11, -0x8

    aput v11, v3, v10

    const/16 v10, -0x61d7

    aput v10, v3, v9

    const/16 v9, -0x3f

    aput v9, v3, v8

    const/16 v8, 0x3d43

    aput v8, v3, v7

    const/16 v7, -0x6ab8

    aput v7, v3, v6

    const/4 v6, -0x7

    aput v6, v3, v4

    const/16 v4, -0x4e

    aput v4, v3, v2

    const/16 v2, 0x18

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, -0x58

    aput v29, v2, v28

    const/16 v28, -0x7a

    aput v28, v2, v27

    const/16 v27, -0x22

    aput v27, v2, v26

    const/16 v26, -0x45b4

    aput v26, v2, v25

    const/16 v25, -0x46

    aput v25, v2, v24

    const/16 v24, -0x27

    aput v24, v2, v23

    const/16 v23, 0xb5c

    aput v23, v2, v22

    const/16 v22, -0x3ef5

    aput v22, v2, v21

    const/16 v21, -0x3f

    aput v21, v2, v20

    const/16 v20, -0x76

    aput v20, v2, v19

    const/16 v19, -0x1d

    aput v19, v2, v18

    const/16 v18, -0x3be1

    aput v18, v2, v17

    const/16 v17, -0x3c

    aput v17, v2, v16

    const/16 v16, -0x67a2

    aput v16, v2, v15

    const/16 v15, -0x68

    aput v15, v2, v14

    const/16 v14, -0x51

    aput v14, v2, v13

    const/16 v13, -0x69ed

    aput v13, v2, v12

    const/16 v12, -0x6a

    aput v12, v2, v11

    const/16 v11, -0x61c0

    aput v11, v2, v10

    const/16 v10, -0x62

    aput v10, v2, v9

    const/16 v9, 0x3d24

    aput v9, v2, v8

    const/16 v8, -0x6ac3

    aput v8, v2, v7

    const/16 v7, -0x6b

    aput v7, v2, v6

    const/16 v6, -0x3e

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v6, v2

    if-lt v4, v6, :cond_3

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v6, v2

    if-lt v4, v6, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x18

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x4d09

    aput v28, v3, v27

    const/16 v27, 0x7f3e

    aput v27, v3, v26

    const/16 v26, 0x581e

    aput v26, v3, v25

    const/16 v25, 0x73b

    aput v25, v3, v24

    const/16 v24, -0x539d

    aput v24, v3, v23

    const/16 v23, -0x33

    aput v23, v3, v22

    const/16 v22, -0x7ca3

    aput v22, v3, v21

    const/16 v21, -0xf

    aput v21, v3, v20

    const/16 v20, -0x1ab

    aput v20, v3, v19

    const/16 v19, -0x5f

    aput v19, v3, v18

    const/16 v18, -0xa

    aput v18, v3, v17

    const/16 v17, -0x7

    aput v17, v3, v16

    const/16 v16, 0x5f1e

    aput v16, v3, v15

    const/16 v15, -0x39c5

    aput v15, v3, v14

    const/16 v14, -0x51

    aput v14, v3, v13

    const/16 v13, 0x675d

    aput v13, v3, v12

    const/16 v12, -0x22c8

    aput v12, v3, v11

    const/16 v11, -0x4d

    aput v11, v3, v10

    const/16 v10, -0xb

    aput v10, v3, v9

    const/16 v9, -0x6cbc

    aput v9, v3, v8

    const/16 v8, -0xc

    aput v8, v3, v7

    const/16 v7, -0x7e94

    aput v7, v3, v6

    const/16 v6, -0x13

    aput v6, v3, v4

    const/16 v4, 0x2b67

    aput v4, v3, v2

    const/16 v2, 0x18

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x4d7d

    aput v29, v2, v28

    const/16 v28, 0x7f4d

    aput v28, v2, v27

    const/16 v27, 0x587f

    aput v27, v2, v26

    const/16 v26, 0x758

    aput v26, v2, v25

    const/16 v25, -0x53f9

    aput v25, v2, v24

    const/16 v24, -0x54

    aput v24, v2, v23

    const/16 v23, -0x7cce

    aput v23, v2, v22

    const/16 v22, -0x7d

    aput v22, v2, v21

    const/16 v21, -0x1c9

    aput v21, v2, v20

    const/16 v20, -0x2

    aput v20, v2, v19

    const/16 v19, -0x7e

    aput v19, v2, v18

    const/16 v18, -0x64

    aput v18, v2, v17

    const/16 v17, 0x5f79

    aput v17, v2, v16

    const/16 v16, -0x39a1

    aput v16, v2, v15

    const/16 v15, -0x3a

    aput v15, v2, v14

    const/16 v14, 0x672a

    aput v14, v2, v13

    const/16 v13, -0x2299

    aput v13, v2, v12

    const/16 v12, -0x23

    aput v12, v2, v11

    const/16 v11, -0x64

    aput v11, v2, v10

    const/16 v10, -0x6ce5

    aput v10, v2, v9

    const/16 v9, -0x6d

    aput v9, v2, v8

    const/16 v8, -0x7ee7

    aput v8, v2, v7

    const/16 v7, -0x7f

    aput v7, v2, v6

    const/16 v6, 0x2b17

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_2
    array-length v6, v2

    if-lt v4, v6, :cond_5

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3
    array-length v6, v2

    if-lt v4, v6, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    :goto_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->mIsShareViaRunning:Z

    if-nez v2, :cond_2

    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->startedAnotherActivity:Z

    :cond_2
    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_a

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x74c3

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x74f3

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_5
    array-length v6, v2

    if-lt v4, v6, :cond_8

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_6
    array-length v6, v2

    if-lt v4, v6, :cond_9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_3
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_4
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_5
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_6
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    :cond_7
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    goto :goto_4

    :cond_8
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_9
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_a
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-eqz v4, :cond_b

    const-wide v6, 0x7d20f293b355ca7dL    # 5.411941733237619E294

    xor-long/2addr v2, v6

    :cond_b
    const/16 v4, 0x20

    shl-long/2addr v2, v4

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->cancelPinRequest:I

    sget-boolean v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->sIsDownloadedPlugin:Z

    if-nez v2, :cond_c

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v6, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->securitySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, -0x45bc

    aput v21, v3, v20

    const/16 v20, -0x38

    aput v20, v3, v19

    const/16 v19, -0x36

    aput v19, v3, v18

    const/16 v18, -0x5d7

    aput v18, v3, v17

    const/16 v17, -0x77

    aput v17, v3, v16

    const/16 v16, -0x5aeb

    aput v16, v3, v15

    const/16 v15, -0x3c

    aput v15, v3, v14

    const/16 v14, -0x1a

    aput v14, v3, v13

    const/16 v13, -0x38

    aput v13, v3, v12

    const/16 v12, 0x7177

    aput v12, v3, v11

    const/16 v11, -0x7afe

    aput v11, v3, v10

    const/16 v10, -0x20

    aput v10, v3, v9

    const/16 v9, -0x29

    aput v9, v3, v8

    const/16 v8, -0x76de

    aput v8, v3, v7

    const/16 v7, -0x14

    aput v7, v3, v4

    const/16 v4, -0x6d

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, -0x45e0

    aput v22, v2, v21

    const/16 v21, -0x46

    aput v21, v2, v20

    const/16 v20, -0x5b

    aput v20, v2, v19

    const/16 v19, -0x5a2

    aput v19, v2, v18

    const/16 v18, -0x6

    aput v18, v2, v17

    const/16 v17, -0x5a9a

    aput v17, v2, v16

    const/16 v16, -0x5b

    aput v16, v2, v15

    const/16 v15, -0x6a

    aput v15, v2, v14

    const/16 v14, -0x69

    aput v14, v2, v13

    const/16 v13, 0x7103

    aput v13, v2, v12

    const/16 v12, -0x7a8f

    aput v12, v2, v11

    const/16 v11, -0x7b

    aput v11, v2, v10

    const/16 v10, -0x5e

    aput v10, v2, v9

    const/16 v9, -0x76ad

    aput v9, v2, v8

    const/16 v8, -0x77

    aput v8, v2, v7

    const/16 v7, -0x1f

    aput v7, v2, v4

    const/4 v4, 0x0

    :goto_7
    array-length v7, v2

    if-lt v4, v7, :cond_d

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_8
    array-length v7, v2

    if-lt v4, v7, :cond_e

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Lcom/sec/android/app/shealth/framework/ui/base/RootBaseActivity;->requestPassword:Z

    move-object/from16 v0, p0

    invoke-virtual {v6, v0, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->saveBooleanValueForConfigKey(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_c
    :goto_9
    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x1e

    aput v12, v3, v11

    const/16 v11, -0x39

    aput v11, v3, v10

    const/16 v10, -0x16

    aput v10, v3, v9

    const/16 v9, -0x5c

    aput v9, v3, v8

    const/4 v8, -0x6

    aput v8, v3, v7

    const/16 v7, -0x2fcd

    aput v7, v3, v6

    const/16 v6, -0x41

    aput v6, v3, v4

    const/16 v4, -0x3c7

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x79

    aput v13, v2, v12

    const/16 v12, -0x4c

    aput v12, v2, v11

    const/16 v11, -0x75

    aput v11, v2, v10

    const/16 v10, -0x1a

    aput v10, v2, v9

    const/16 v9, -0x72

    aput v9, v2, v8

    const/16 v8, -0x2fa4

    aput v8, v2, v7

    const/16 v7, -0x30

    aput v7, v2, v6

    const/16 v6, -0x395

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_a
    array-length v6, v2

    if-lt v4, v6, :cond_f

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_b
    array-length v6, v2

    if-lt v4, v6, :cond_10

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_13

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x6ed6

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x6ee6

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_c
    array-length v6, v2

    if-lt v4, v6, :cond_11

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_d
    array-length v6, v2

    if-lt v4, v6, :cond_12

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_d
    aget v7, v2, v4

    aget v8, v3, v4

    xor-int/2addr v7, v8

    aput v7, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_7

    :cond_e
    aget v7, v3, v4

    int-to-char v7, v7

    aput-char v7, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_8

    :cond_f
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_a

    :cond_10
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_b

    :cond_11
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    :cond_12
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    :cond_13
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v7, 0x0

    cmp-long v7, v2, v7

    if-eqz v7, :cond_14

    const-wide v7, 0x7d20f293b355ca7dL    # 5.411941733237619E294

    xor-long/2addr v2, v7

    :cond_14
    const/16 v7, 0x20

    shl-long/2addr v2, v7

    const/16 v7, 0x20

    shr-long/2addr v2, v7

    long-to-int v2, v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    array-length v2, v5

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v5, v2

    long-to-int v2, v2

    if-gtz v2, :cond_17

    new-instance v5, Ljava/lang/ArrayIndexOutOfBoundsException;

    const/4 v2, 0x1

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/16 v4, -0x1492

    aput v4, v3, v2

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/16 v6, -0x14a2

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_e
    array-length v6, v2

    if-lt v4, v6, :cond_15

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_f
    array-length v6, v2

    if-lt v4, v6, :cond_16

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_15
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_e

    :cond_16
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    :cond_17
    const/4 v2, 0x0

    aget-wide v2, v5, v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_18

    const-wide v4, 0x7d20f293b355ca7dL    # 5.411941733237619E294

    xor-long/2addr v2, v4

    :cond_18
    const/16 v4, 0x20

    shl-long/2addr v2, v4

    const/16 v4, 0x20

    shr-long/2addr v2, v4

    long-to-int v2, v2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :catch_0
    move-exception v2

    goto/16 :goto_9
.end method
