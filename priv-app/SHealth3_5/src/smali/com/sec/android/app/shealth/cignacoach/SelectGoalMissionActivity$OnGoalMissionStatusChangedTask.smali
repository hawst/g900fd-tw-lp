.class Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;
.super Landroid/os/AsyncTask;
.source "SelectGoalMissionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OnGoalMissionStatusChangedTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mNewGoalDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mNewMissionDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNewTotalCount:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V
    .locals 1

    .prologue
    .line 531
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 532
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    .line 533
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 531
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 539
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalListData(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 541
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewTotalCount:I

    .line 542
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 543
    .local v1, "missionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewTotalCount:I

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewTotalCount:I

    goto :goto_0

    .line 547
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "missionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaBaseData;>;"
    :cond_0
    const/4 v2, 0x0

    return-object v2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 531
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 552
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1100(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1100(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 561
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mGoalDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1100(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewGoalDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 562
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mMissionDataList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->mNewMissionDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 564
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->setAdapter()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1300(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->mExpandableListAdapter:Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$200(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/CignaMainExpandableAdapter;->getCheckedItemCount()I

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->updateActionBar(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$300(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;I)V

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity$OnGoalMissionStatusChangedTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->allExpandGroup()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;->access$1400(Lcom/sec/android/app/shealth/cignacoach/SelectGoalMissionActivity;)V

    .line 569
    :cond_2
    return-void
.end method
