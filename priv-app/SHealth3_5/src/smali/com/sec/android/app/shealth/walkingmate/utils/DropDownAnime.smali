.class public Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;
.super Landroid/view/animation/Animation;
.source "DropDownAnime.java"


# instance fields
.field private mIsExpandAnimation:Z

.field private mTargetHeight:I

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;IZ)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "targetHeight"    # I
    .param p3, "isExpandAnimation"    # Z

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->mView:Landroid/view/View;

    .line 26
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->mTargetHeight:I

    .line 27
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->mIsExpandAnimation:Z

    .line 28
    return-void
.end method


# virtual methods
.method public afterChanges()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 33
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->mIsExpandAnimation:Z

    if-eqz v1, :cond_1

    .line 34
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->mTargetHeight:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 37
    .local v0, "newHeight":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 38
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->afterChanges()V

    .line 40
    cmpl-float v1, p1, v3

    if-nez v1, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->finished()V

    .line 43
    :cond_0
    return-void

    .line 36
    .end local v0    # "newHeight":I
    :cond_1
    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/DropDownAnime;->mTargetHeight:I

    int-to-float v1, v1

    sub-float v2, v3, p1

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .restart local v0    # "newHeight":I
    goto :goto_0
.end method

.method public finished()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public initialize(IIII)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "parentWidth"    # I
    .param p4, "parentHeight"    # I

    .prologue
    .line 47
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 48
    return-void
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method
