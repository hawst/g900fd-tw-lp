.class public Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "SelectFavoriteActivity.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;,
        Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SpinnerCustomAdapter;,
        Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;
    }
.end annotation


# static fields
.field private static final DELETE_DIALOG:Ljava/lang/String; = "DeleteDialog"

.field private static final DIALOG_FILTER:Ljava/lang/String; = "dialog_filter"

.field private static final FAVORITE_ARTICLES_HASHSET_KEY:Ljava/lang/String; = "FavoriteArticlesHashKey"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private favorite_articles_hashSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;",
            ">;"
        }
    .end annotation
.end field

.field private isDeleteDialogShown:Z

.field private mCategoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

.field private mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

.field private mFavoriteListView:Landroid/widget/ExpandableListView;

.field private mFavoritesFactoryDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation
.end field

.field private mFavoritesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

.field private mSelectDataAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectSpinner:Landroid/widget/Spinner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->isDeleteDialogShown:Z

    .line 71
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->favorite_articles_hashSet:Ljava/util/HashSet;

    .line 654
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->refreshFocusables()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;
    .param p1, "x1"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->updateActionBar(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->showDeleteDoneDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->sortByCategory()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->sortByTitle()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->performDelete()V

    return-void
.end method

.method private addSelectSpinner()V
    .locals 6

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-nez v0, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_2

    .line 172
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SpinnerCustomAdapter;

    const v3, 0x7f03021a

    const v4, 0x7f08080a

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SpinnerCustomAdapter;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x7f030219

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 177
    return-void
.end method

.method private expandAllCategory()V
    .locals 3

    .prologue
    .line 464
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 465
    .local v0, "categoryCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 466
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 465
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 468
    :cond_0
    return-void
.end method

.method private getListData()V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 271
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getFavoriteArticle()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    .line 272
    return-void
.end method

.method private getListEmptyView()Landroid/view/View;
    .locals 2

    .prologue
    .line 381
    const v1, 0x7f0801ca

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 383
    .local v0, "emptyView":Landroid/view/View;
    return-object v0
.end method

.method private performDelete()V
    .locals 7

    .prologue
    .line 548
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getGroupCount()I

    move-result v4

    .line 549
    .local v4, "listAdapterCount":I
    const/4 v3, 0x0

    .local v3, "group":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 551
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChildrenCount(I)I

    move-result v0

    .line 553
    .local v0, "articleCount":I
    const/4 v2, 0x0

    .local v2, "child":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 555
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v5, v3, v2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v1

    .line 557
    .local v1, "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 559
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->removeFavorite(I)Z

    .line 553
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 549
    .end local v1    # "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 564
    .end local v0    # "articleCount":I
    .end local v2    # "child":I
    :cond_2
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->isDeleteDialogShown:Z

    .line 565
    return-void
.end method

.method private setAdapter()V
    .locals 3

    .prologue
    .line 275
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;->DELETE:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->setType(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter$Type;)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->setOnCheckedCountListener(Lcom/sec/android/app/shealth/cignacoach/listener/CheckedCheckBoxCountListener;)V

    .line 284
    return-void
.end method

.method private setListSort()V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    if-ne v0, v1, :cond_1

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->sortByCategory()V

    .line 295
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->TITLE:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    if-ne v0, v1, :cond_0

    .line 293
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->sortByTitle()V

    goto :goto_0
.end method

.method private showDeleteDoneDialog()V
    .locals 5

    .prologue
    const v4, 0x7f090035

    const/4 v3, 0x1

    .line 527
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->isDeleteDialogShown:Z

    .line 529
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getCheckedItemCount()I

    move-result v1

    if-le v1, v3, :cond_0

    const v1, 0x7f090c7f

    :goto_0
    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$7;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 542
    .local v0, "deleteDoneBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 544
    return-void

    .line 529
    .end local v0    # "deleteDoneBuilder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    :cond_0
    const v1, 0x7f090c7e

    goto :goto_0
.end method

.method private sortByCategory()V
    .locals 7

    .prologue
    .line 387
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 388
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 390
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 391
    .local v1, "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getCategory()Ljava/lang/String;

    move-result-object v3

    .line 392
    .local v3, "groupString":Ljava/lang/CharSequence;
    if-eqz v3, :cond_0

    .line 393
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 394
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 397
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 400
    .local v2, "favoritesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 407
    .end local v1    # "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .end local v2    # "favoritesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    .end local v3    # "groupString":Ljava/lang/CharSequence;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 408
    .local v0, "articleDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->sortFavoriteListByTitle(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 411
    .end local v0    # "articleDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->notifyDataSetChanged()V

    .line 413
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->expandAllCategory()V

    .line 415
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    .line 416
    return-void
.end method

.method private sortByTitle()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 419
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 420
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 422
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 426
    .local v2, "favoriteArrayData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 427
    .local v1, "dataListSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 428
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 431
    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->sortFavoriteListByTitle(Ljava/util/ArrayList;)V

    .line 433
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 434
    .local v0, "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 437
    .end local v0    # "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 438
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    const v6, 0x7f090076

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->notifyDataSetChanged()V

    .line 443
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->expandAllCategory()V

    .line 445
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->TITLE:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    .line 446
    return-void
.end method

.method private sortFavoriteListByTitle(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p1, "favoriteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    .line 460
    .local v0, "favoriteComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 461
    return-void
.end method

.method private toggleActionButtonsVisibility(Z)V
    .locals 2
    .param p1, "visibility"    # Z

    .prologue
    .line 569
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 571
    return-void
.end method

.method private toggleActionButtonsVisibilityforSelectMode(Z)V
    .locals 2
    .param p1, "visibility"    # Z

    .prologue
    .line 575
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mActionBarHelper:Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 577
    return-void
.end method

.method private updateActionBar(I)V
    .locals 4
    .param p1, "checkedCount"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 222
    if-gtz p1, :cond_1

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v0, v1, :cond_0

    .line 224
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    .line 237
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->updateSelectedCount(I)V

    .line 238
    return-void

    .line 226
    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->toggleActionButtonsVisibility(Z)V

    goto :goto_0

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v0, v1, :cond_2

    .line 230
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    .line 234
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->refreshFocusables()V

    goto :goto_0

    .line 232
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->toggleActionButtonsVisibility(Z)V

    goto :goto_1
.end method

.method private updateList()V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getListData()V

    .line 265
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->setAdapter()V

    .line 266
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->setListSort()V

    .line 267
    return-void
.end method

.method private updateListSelectInfo()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, -0x1

    .line 241
    const/4 v2, -0x1

    .line 242
    .local v2, "expandGroupPosition":I
    const/4 v1, -0x1

    .line 244
    .local v1, "expandChildPosition":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 245
    .local v3, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_0

    .line 246
    const-string v4, "EXTRA_NAME_EXPAND_GROUP_POSITION"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 247
    const-string v4, "EXTRA_NAME_EXPAND_CHILD_POSITION"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 250
    :cond_0
    if-eq v2, v6, :cond_1

    if-eq v1, v6, :cond_1

    .line 252
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v4, v2, v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v0

    .line 253
    .local v0, "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isChecked()Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v5

    :goto_0
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setChecked(Z)V

    .line 254
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v4, v2, v1, v5}, Landroid/widget/ExpandableListView;->setSelectedChild(IIZ)Z

    .line 255
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->notifyDataSetChanged()V

    .line 258
    .end local v0    # "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :cond_1
    const-string v4, "EXTRA_NAME_EXPAND_GROUP_POSITION"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 259
    const-string v4, "EXTRA_NAME_EXPAND_CHILD_POSITION"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 261
    return-void

    .line 253
    .restart local v0    # "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private updateSelectSpinner(I)V
    .locals 7
    .param p1, "selectedCounts"    # I

    .prologue
    const v6, 0x7f090073

    const v5, 0x7f090071

    const/4 v4, 0x0

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_1

    .line 183
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->addSelectSpinner()V

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    if-nez v0, :cond_2

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    .line 189
    :cond_2
    if-ltz p1, :cond_6

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    const v1, 0x7f090074

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    if-nez p1, :cond_4

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectDataAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 218
    :goto_1
    return-void

    .line 199
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 203
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->disableActionBarButton(I)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1
.end method

.method private updateSelectedCount(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    const v5, 0x7f090074

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v5, v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080304

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v5, v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 377
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->updateSelectSpinner(I)V

    .line 378
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 10

    .prologue
    const v5, 0x7f090033

    const v9, 0x7f0207b0

    const/4 v8, 0x1

    const v7, 0x7f090035

    const/4 v6, 0x0

    .line 304
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 308
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->SELECT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v3, v4, :cond_1

    .line 309
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    .line 333
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    const v3, 0x7f0207cf

    invoke-direct {v1, v3, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 334
    .local v1, "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 335
    invoke-virtual {v1, v5}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 336
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setIconResDrawable(Landroid/graphics/drawable/Drawable;)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 342
    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 343
    invoke-virtual {v1, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 346
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->toggleActionButtonsVisibilityforSelectMode(Z)V

    .line 372
    .end local v0    # "actionBarButtonListener":Landroid/view/View$OnClickListener;
    .end local v1    # "actionBarButtonMoreBuilder":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    :cond_0
    :goto_0
    return-void

    .line 348
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    sget-object v4, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    if-ne v3, v4, :cond_0

    .line 349
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    .line 366
    .restart local v0    # "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    invoke-direct {v2, v9, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;-><init>(ILandroid/view/View$OnClickListener;)V

    .line 367
    .local v2, "deleteButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;
    const v3, 0x7f020022

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setBackgroundResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 368
    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setContentDescription(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 369
    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->setHoverTextResID(I)Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;

    .line 370
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton$Builder;->create()Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    goto :goto_0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 502
    const-string v0, "dialog_filter"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$6;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;)V

    .line 523
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 6
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    const/4 v3, 0x0

    .line 473
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v2, p3, p4}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v0

    .line 474
    .local v0, "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isChecked()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setChecked(Z)V

    .line 475
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->notifyDataSetChanged()V

    .line 476
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;

    invoke-direct {v1, p3, p4}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;-><init>(II)V

    .line 479
    .local v1, "favorite_articles":Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 481
    const-string v2, "Favorite Article selection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Favorite Article Selected"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->favorite_articles_hashSet:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 484
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    .line 486
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->favorite_articles_hashSet:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 488
    const-string v2, "Favorite Article Deselection"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Favorite Article Deselected"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->favorite_articles_hashSet:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 495
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getCheckedItemCount()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->updateActionBar(I)V

    .line 497
    return v3

    .end local v1    # "favorite_articles":Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;
    :cond_2
    move v2, v3

    .line 474
    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 78
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 81
    const-string v3, "EXTRA_NAME_DELETE_TYPE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mDeleteType:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->refreshFocusables()V

    .line 85
    const-string v3, "EXTRA_NAME_SORTING_TYPE"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 87
    .local v2, "sortType":Z
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 88
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    .line 93
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->setFilterCategory(Z)V

    .line 96
    .end local v2    # "sortType":Z
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    const v3, 0x7f03005a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->setContentView(I)V

    .line 100
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    .line 101
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCategoryList:Ljava/util/ArrayList;

    .line 102
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesList:Ljava/util/ArrayList;

    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getListEmptyView()Landroid/view/View;

    move-result-object v0

    .line 105
    .local v0, "emptyView":Landroid/view/View;
    const v3, 0x7f0801c9

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ExpandableListView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    .line 106
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v0}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 107
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->updateList()V

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->updateListSelectInfo()V

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getCheckedItemCount()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->updateActionBar(I)V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setTitleActionBarVisibility(Z)V

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    if-eqz v3, :cond_2

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    .line 126
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleTextVisibility(Z)V

    .line 128
    return-void

    .line 91
    .end local v0    # "emptyView":Landroid/view/View;
    .restart local v2    # "sortType":Z
    :cond_1
    sget-object v3, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;->TITLE:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$SortType;

    goto :goto_0

    .line 122
    .end local v2    # "sortType":Z
    .restart local v0    # "emptyView":Landroid/view/View;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->addSelectSpinner()V

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addSpinnerView(Landroid/widget/Spinner;)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 299
    const/4 v0, 0x0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 630
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 633
    new-instance v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;-><init>()V

    .line 635
    .local v1, "fav_articles":Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;
    const-string v4, "DeleteDialog"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 638
    const-string/jumbo v4, "onRestoreInstanceState"

    const-string v5, "Showing Dialog Again"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    const-string v4, "FavoriteArticlesHashKey"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    .line 640
    .local v3, "restored_articles":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;>;"
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 642
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 644
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "fav_articles":Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;
    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;

    .line 645
    .restart local v1    # "fav_articles":Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->getGroup()I

    move-result v5

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;->getChild()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v0

    .line 646
    .local v0, "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->setChecked(Z)V

    goto :goto_0

    .line 650
    .end local v0    # "articleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->showDeleteDoneDialog()V

    .line 652
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;>;"
    .end local v3    # "restored_articles":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity$LibraryFav;>;"
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 620
    const-string v0, "DeleteDialog"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->isDeleteDialogShown:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 621
    const-string/jumbo v0, "onSaveInstanceState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Was Dialog Shown Before "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->isDeleteDialogShown:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    const-string v0, "FavoriteArticlesHashKey"

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;->favorite_articles_hashSet:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 625
    return-void
.end method
