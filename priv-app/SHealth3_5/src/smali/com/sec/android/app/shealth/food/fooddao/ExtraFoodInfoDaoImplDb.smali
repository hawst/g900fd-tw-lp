.class public Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;
.source "ExtraFoodInfoDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;",
        ">;",
        "Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDao;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrient;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;-><init>(Landroid/net/Uri;Landroid/content/Context;)V

    .line 39
    return-void
.end method


# virtual methods
.method public deleteDataById(J)Z
    .locals 5
    .param p1, "itemId"    # J

    .prologue
    .line 75
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v1, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "="

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # J

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v0

    return-object v0
.end method

.method public getDataById(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 82
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v2, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "="

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v1

    .line 84
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 33
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)Landroid/content/ContentValues;
    .locals 5
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 90
    if-nez p1, :cond_0

    .line 91
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "item shouldn\'t be null!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 93
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)V

    .line 94
    .local v0, "extraFoodInfo":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;
    const-string v2, "calcium"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCalcium()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 95
    const-string/jumbo v2, "metric_serving_amount"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDefaultNumber()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 96
    const-string v2, "carbohydrate"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCarbohydrate()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 97
    const-string v2, "cholesterol"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getCholesterol()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 98
    const-string v2, "dietary_fiber"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getDietary()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 99
    const-string/jumbo v2, "total_fat"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFat()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 100
    const-string v2, "food_info__id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFoodInfoId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 101
    const-string v2, "iron"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getIron()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 102
    const-string/jumbo v2, "monosaturated_fat"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getMonosaturated()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 103
    const-string/jumbo v2, "polysaturated_fat"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getPolysaturated()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 104
    const-string/jumbo v2, "trans_fat"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getTransFat()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 105
    const-string/jumbo v2, "potassium"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getPotassium()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 106
    const-string/jumbo v2, "protein"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getProtein()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 107
    const-string/jumbo v2, "saturated_fat"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getSaturated()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 108
    const-string/jumbo v2, "sodium"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getSodium()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 109
    const-string/jumbo v2, "sugar"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getSugar()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 110
    const-string/jumbo v2, "metric_serving_unit"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getUnitName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string/jumbo v2, "serving_description"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getServingDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getGrammInKcal()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 113
    .local v1, "valueOrNull":Ljava/lang/Float;
    const-string v3, "kcal_in_unit"

    if-eqz v1, :cond_1

    const/high16 v2, -0x40800000    # -1.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 115
    const-string/jumbo v2, "vitamin_a"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getVitaminA()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 116
    const-string/jumbo v2, "vitamin_c"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getVitaminC()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 117
    const-string v2, "default_number_of_serving_unit"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getUnit()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 118
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->getContentValues()Landroid/content/ContentValues;

    move-result-object v2

    return-object v2

    .line 113
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    goto :goto_0
.end method

.method public getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .locals 7
    .param p1, "foodInfoId"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 43
    const-string v2, "food_info__id = ?"

    new-array v3, v6, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v1, v2, v3, v1}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->getAllDatas([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 51
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lt v2, v6, :cond_0

    .line 52
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .line 59
    :cond_0
    return-object v1
.end method

.method protected bridge synthetic getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v0

    return-object v0
.end method

.method protected getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    .locals 35
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 124
    new-instance v34, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;

    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;-><init>(Landroid/database/Cursor;)V

    .line 125
    .local v34, "getter":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    const-string v3, "_id"

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string v5, "food_info__id"

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    const-string v7, "default_number_of_serving_unit"

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string/jumbo v8, "total_fat"

    move-object/from16 v0, v34

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v8

    const-string/jumbo v9, "saturated_fat"

    move-object/from16 v0, v34

    invoke-virtual {v0, v9}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v9

    const-string/jumbo v10, "polysaturated_fat"

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v10

    const-string/jumbo v11, "monosaturated_fat"

    move-object/from16 v0, v34

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v11

    const-string/jumbo v12, "trans_fat"

    move-object/from16 v0, v34

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v12

    const-string v13, "carbohydrate"

    move-object/from16 v0, v34

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v13

    const-string v14, "dietary_fiber"

    move-object/from16 v0, v34

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v14

    const-string/jumbo v15, "sugar"

    move-object/from16 v0, v34

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v15

    const-string/jumbo v16, "protein"

    move-object/from16 v0, v34

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v16

    const-string/jumbo v17, "metric_serving_unit"

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string/jumbo v18, "serving_description"

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string/jumbo v19, "metric_serving_amount"

    move-object/from16 v0, v34

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v19

    const-string v20, "kcal_in_unit"

    move-object/from16 v0, v34

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v20

    const/high16 v21, -0x40800000    # -1.0f

    const-string v22, "cholesterol"

    move-object/from16 v0, v34

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v22

    const-string/jumbo v23, "sodium"

    move-object/from16 v0, v34

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v23

    const-string/jumbo v24, "potassium"

    move-object/from16 v0, v34

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v24

    const-string/jumbo v25, "vitamin_a"

    move-object/from16 v0, v34

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v25

    const-string/jumbo v26, "vitamin_c"

    move-object/from16 v0, v34

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v26

    const-string v27, "calcium"

    move-object/from16 v0, v34

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v27

    const-string v28, "iron"

    move-object/from16 v0, v34

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v28

    const-string v29, "create_time"

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v29

    const-string/jumbo v31, "update_time"

    move-object/from16 v0, v34

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v31

    const-string/jumbo v33, "time_zone"

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v33

    invoke-direct/range {v2 .. v33}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;-><init>(JJIFFFFFFFFFLjava/lang/String;Ljava/lang/String;FFFIIIIIIIJJI)V

    .line 154
    .local v2, "extraFoodInfo":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    return-object v2
.end method

.method public bridge synthetic updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 33
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)I

    move-result v0

    return v0
.end method

.method public updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;)I
    .locals 5
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFoodInfoId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    .line 65
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getFoodInfoId()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/food/fooddao/ExtraFoodInfoDaoImplDb;->getExtraByFoodId(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;

    move-result-object v0

    .line 66
    .local v0, "insertedExtraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->getId()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;->setId(J)V

    .line 70
    .end local v0    # "insertedExtraFoodInfoData":Lcom/sec/android/app/shealth/common/utils/hcp/data/ExtraFoodInfoData;
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/CommonDaoImplDb;->updateData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)I

    move-result v1

    return v1
.end method
