.class public final enum Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;
.super Ljava/lang/Enum;
.source "GradationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ControllerMovementDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

.field public static final enum DIRECTION_LEFT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

.field public static final enum DIRECTION_RIGHT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    const-string v1, "DIRECTION_RIGHT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->DIRECTION_RIGHT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    const-string v1, "DIRECTION_LEFT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->DIRECTION_LEFT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    .line 47
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->DIRECTION_RIGHT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->DIRECTION_LEFT:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->$VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    const-class v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->$VALUES:[Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/commonui/controller/GradationView$ControllerMovementDirection;

    return-object v0
.end method
