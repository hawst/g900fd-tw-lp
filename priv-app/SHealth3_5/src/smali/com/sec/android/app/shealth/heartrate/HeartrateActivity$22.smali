.class Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$22;
.super Ljava/lang/Object;
.source "HeartrateActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->showNoSensorInformationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)V
    .locals 0

    .prologue
    .line 1128
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$22;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOKButtonClick(Landroid/app/Activity;Landroid/view/View;Landroid/app/Dialog;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;
    .param p2, "content"    # Landroid/view/View;
    .param p3, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 1131
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$22;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1132
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "heartrate_warning_checked"

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity$22;->this$0:Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;->access$1100(Lcom/sec/android/app/shealth/heartrate/HeartrateActivity;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1133
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1134
    return-void
.end method
