.class Lcom/sec/android/app/shealth/home/HomeActivity$17;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/HomeActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeActivity;)V
    .locals 0

    .prologue
    .line 1934
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 3
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/4 v1, 0x1

    .line 1937
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_5

    .line 1938
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1300(Lcom/sec/android/app/shealth/home/HomeActivity;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1971
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1940
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1200(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1941
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setLaunchMobilePopupWasShown(Landroid/content/Context;Z)V

    .line 1943
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2500(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchWifiPopupWasShown(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1945
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    const/16 v1, 0x8

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1302(Lcom/sec/android/app/shealth/home/HomeActivity;I)I

    .line 1946
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->connectionWifiBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2600(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 1948
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->checkForUpdates()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2700(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    .line 1949
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->widgetIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2800(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 1953
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->doNotShowCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1200(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1954
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setLaunchWifiPopupWasShown(Landroid/content/Context;Z)V

    .line 1956
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2500(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2500(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;->isMobileDataEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isLaunchMobilePopupWasShown(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1958
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    const/4 v1, 0x6

    # setter for: Lcom/sec/android/app/shealth/home/HomeActivity;->popupMode:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$1302(Lcom/sec/android/app/shealth/home/HomeActivity;I)I

    .line 1959
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2900(Lcom/sec/android/app/shealth/home/HomeActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "data_connection_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1961
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # invokes: Lcom/sec/android/app/shealth/home/HomeActivity;->checkForUpdates()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2700(Lcom/sec/android/app/shealth/home/HomeActivity;)V

    .line 1962
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    # getter for: Lcom/sec/android/app/shealth/home/HomeActivity;->widgetIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->access$2800(Lcom/sec/android/app/shealth/home/HomeActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1968
    :cond_5
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->NEGATIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v0, :cond_0

    .line 1969
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/HomeActivity$17;->this$0:Lcom/sec/android/app/shealth/home/HomeActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/HomeActivity;->finish()V

    goto/16 :goto_0

    .line 1938
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
