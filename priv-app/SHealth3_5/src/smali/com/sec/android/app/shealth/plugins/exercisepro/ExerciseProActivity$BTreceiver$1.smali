.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver$1;
.super Ljava/lang/Object;
.source "ExerciseProActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->customizeActionBar()V

    .line 293
    sget-boolean v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->getActionBarTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 298
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$BTreceiver;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->getActionBarTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_0
.end method
