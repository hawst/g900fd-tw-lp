.class public Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;
.super Landroid/widget/FrameLayout;
.source "WeightIndicatorView.java"


# static fields
.field private static final INTERPOLATOR_OVERSHOOT_TENSION:F = 0.7f


# instance fields
.field private mAnimatorSet:Landroid/animation/AnimatorSet;

.field private mDialsView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;

.field private mDialsViewAnimator:Landroid/animation/ObjectAnimator;

.field private mGoalValue:F

.field private mGoalView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;

.field private mGoalViewAlphaAnimator:Landroid/animation/ObjectAnimator;

.field private mGoalViewAnimator:Landroid/animation/ObjectAnimator;

.field private mRangeView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

.field private mRangeViewAnimator:Landroid/animation/ObjectAnimator;

.field private mWeightTextView:Landroid/widget/TextView;

.field private mWeightViewAnimator:Landroid/animation/ValueAnimator;

.field private mWindowView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 57
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initialize()V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initialize()V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initialize()V

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->isGoalSet()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAlphaAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;
    .param p1, "x1"    # F

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setWeightTextValue(F)V

    return-void
.end method

.method private getInterpolator()Landroid/view/animation/Interpolator;
    .locals 2

    .prologue
    .line 238
    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    const v1, 0x3f333333    # 0.7f

    invoke-direct {v0, v1}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    return-object v0
.end method

.method private getNewObjectAnimator(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p1, "target"    # Ljava/lang/Object;
    .param p2, "propertyName"    # Ljava/lang/String;

    .prologue
    .line 143
    new-instance v0, Landroid/animation/ObjectAnimator;

    invoke-direct {v0}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 144
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 145
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 146
    return-object v0
.end method

.method private initDialViewAnimator()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mDialsView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;

    const-string/jumbo v1, "rotation"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getNewObjectAnimator(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mDialsViewAnimator:Landroid/animation/ObjectAnimator;

    .line 140
    return-void
.end method

.method private initGoalViewAlphaAnimator()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;

    const-string/jumbo v1, "paintAlpha"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getNewObjectAnimator(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAlphaAnimator:Landroid/animation/ObjectAnimator;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0051

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAlphaAnimator:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0053

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    aput v2, v1, v4

    const/4 v2, 0x1

    aput v4, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 117
    return-void
.end method

.method private initGoalViewAnimator()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;

    const-string v1, "arcAngle"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getNewObjectAnimator(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAnimator:Landroid/animation/ObjectAnimator;

    .line 132
    return-void
.end method

.method private initRangeViewAnimator()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

    const-string/jumbo v1, "rangeAngleDiff"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getNewObjectAnimator(Ljava/lang/Object;Ljava/lang/String;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeViewAnimator:Landroid/animation/ObjectAnimator;

    .line 136
    return-void
.end method

.method private initWeightViewAnimator()V
    .locals 2

    .prologue
    .line 120
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWeightViewAnimator:Landroid/animation/ValueAnimator;

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWeightViewAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$2;-><init>(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 128
    return-void
.end method

.method private initialize()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initializeLayout()V

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initializeUIComponents()V

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initializeAnimators()V

    .line 78
    return-void
.end method

.method private initializeAnimators()V
    .locals 4

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initDialViewAnimator()V

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initGoalViewAnimator()V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initRangeViewAnimator()V

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initWeightViewAnimator()V

    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->initGoalViewAlphaAnimator()V

    .line 87
    iget v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWeightTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToTenth(F)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    :cond_0
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    const/4 v1, 0x4

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mDialsViewAnimator:Landroid/animation/ObjectAnimator;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWeightViewAnimator:Landroid/animation/ValueAnimator;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAnimator:Landroid/animation/ObjectAnimator;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeViewAnimator:Landroid/animation/ObjectAnimator;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    new-instance v1, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView$1;-><init>(Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 107
    return-void
.end method

.method private initializeLayout()V
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0302be

    invoke-static {v0, v1, p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 151
    return-void
.end method

.method private initializeUIComponents()V
    .locals 1

    .prologue
    .line 154
    const v0, 0x7f080c77

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWeightTextView:Landroid/widget/TextView;

    .line 155
    const v0, 0x7f080c74

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;

    .line 156
    const v0, 0x7f080c73

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

    .line 157
    const v0, 0x7f080c72

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mDialsView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;

    .line 158
    const v0, 0x7f080c75

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWindowView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;

    .line 159
    return-void
.end method

.method private isGoalSet()Z
    .locals 2

    .prologue
    .line 110
    iget v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    const/high16 v1, -0x40800000    # -1.0f

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isEqual(FF)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setGoal(F)V
    .locals 0
    .param p1, "goal"    # F

    .prologue
    .line 247
    iput p1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    .line 249
    return-void
.end method

.method private setWeight(FZ)V
    .locals 9
    .param p1, "weight"    # F
    .param p2, "needAnimation"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mDialsView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->setWeightValue(F)V

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->setWeightValue(F)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->stopIndicatorAnimation()V

    .line 208
    invoke-static {p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getAngleForWeightValue(F)F

    move-result v0

    .line 210
    .local v0, "endAngle":F
    if-eqz p2, :cond_0

    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getStartAngle(FF)F

    move-result v1

    .line 211
    .local v1, "startAngle":F
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mDialsViewAnimator:Landroid/animation/ObjectAnimator;

    new-array v4, v8, [F

    aput v1, v4, v6

    aput v0, v4, v7

    invoke-virtual {v2, v4}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeViewAnimator:Landroid/animation/ObjectAnimator;

    new-array v4, v8, [F

    aput v1, v4, v6

    aput v0, v4, v7

    invoke-virtual {v2, v4}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 213
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWeightViewAnimator:Landroid/animation/ValueAnimator;

    new-array v5, v8, [F

    if-eqz p2, :cond_1

    iget v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    invoke-static {v2, p1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorHelper;->getStartValue(FF)F

    move-result v2

    :goto_1
    aput v2, v5, v6

    aput p1, v5, v7

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 214
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAnimator:Landroid/animation/ObjectAnimator;

    new-array v5, v8, [F

    aput v3, v5, v6

    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->isGoalSet()Z

    move-result v2

    if-eqz v2, :cond_2

    sub-float v2, v0, v1

    :goto_2
    aput v2, v5, v7

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 215
    return-void

    .end local v1    # "startAngle":F
    :cond_0
    move v1, v0

    .line 210
    goto :goto_0

    .restart local v1    # "startAngle":F
    :cond_1
    move v2, p1

    .line 213
    goto :goto_1

    :cond_2
    move v2, v3

    .line 214
    goto :goto_2
.end method

.method private setWeightTextValue(F)V
    .locals 7
    .param p1, "value"    # F

    .prologue
    const v5, 0x7f090c94

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 228
    const/4 v1, 0x0

    .line 229
    .local v1, "newText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "language":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "ar"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 231
    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToTenth(F)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 234
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWeightTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    return-void

    .line 233
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->roundToTenth(F)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 163
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getMeasuredWidth()I

    move-result v1

    .line 165
    .local v1, "maxWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getMeasuredHeight()I

    move-result v0

    .line 166
    .local v0, "maxHeight":I
    div-int/lit8 v2, v1, 0x2

    .line 167
    .local v2, "pivotX":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0b1d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 168
    .local v3, "pivotY":I
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 169
    .local v4, "radius":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mDialsView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->setRadius(I)V

    .line 170
    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->setRadius(I)V

    .line 171
    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorGoalView;->setRadius(I)V

    .line 172
    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWindowView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->setRadius(I)V

    .line 173
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setMeasuredDimension(II)V

    .line 174
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->release()V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWindowView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;->release()V

    .line 278
    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

    .line 279
    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mWindowView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorWindowView;

    .line 280
    return-void
.end method

.method public setNormalWeightRange(FF)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mRangeView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorRangeView;->setNormalWeightRange(FF)V

    .line 225
    return-void
.end method

.method public setWeightWithGoal(FFZ)V
    .locals 2
    .param p1, "weight"    # F
    .param p2, "goal"    # F
    .param p3, "needAnimation"    # Z

    .prologue
    .line 185
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setGoal(F)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mDialsView:Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;

    iget v1, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalValue:F

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorDialsView;->setGoalValue(F)V

    .line 187
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setWeight(FZ)V

    .line 188
    return-void
.end method

.method public setWeightWithoutGoal(FFZ)V
    .locals 0
    .param p1, "weight"    # F
    .param p2, "prevValue"    # F
    .param p3, "needAnimation"    # Z

    .prologue
    .line 200
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setGoal(F)V

    .line 201
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->setWeight(FZ)V

    .line 202
    return-void
.end method

.method public startIndicatorAnimation()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 261
    :cond_0
    return-void
.end method

.method public stopIndicatorAnimation()V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/summary/custom/WeightIndicatorView;->mGoalViewAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 269
    return-void
.end method
