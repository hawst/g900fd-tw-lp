.class public Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "DeleteUserDataActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# instance fields
.field private CONFIRM_DELETE:Ljava/lang/String;

.field public TAG:Ljava/lang/String;

.field private loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 42
    const-class v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    .line 44
    const-string v0, "confirm_delete"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->CONFIRM_DELETE:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->CONFIRM_DELETE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    return-object v0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090d48

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 223
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->CONFIRM_DELETE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    new-instance v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)V

    .line 241
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initLayout()V
    .locals 3

    .prologue
    .line 53
    const v0, 0x7f080329

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isRtlLanguage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const v0, 0x7f080327

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "\u200f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f090e6d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 103
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onActivityResult() requestCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resultCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const/16 v1, 0x1c6d

    if-ne p1, v1, :cond_0

    .line 106
    const/4 v1, -0x1

    if-ne v1, p2, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.android.app.shealth"

    const-string v3, "S011"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$2;

    invoke-direct {v1, p0, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->loadingScreenPopup:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    .line 122
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;-><init>(Landroid/content/Context;)V

    .line 123
    .local v0, "helper":Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;)V

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->erase(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    .line 216
    .end local v0    # "helper":Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v0, 0x7f0300a8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->setContentView(I)V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->initLayout()V

    .line 50
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const v6, 0x7f090e72

    const v5, 0x7f090e70

    const v4, 0x7f090e6f

    const v2, 0x7f080327

    const v3, 0x7f080328

    .line 71
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 72
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->finish()V

    .line 98
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "strLanguage":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "ko"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0905c3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "5. "

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    if-eqz v0, :cond_2

    const-string v1, "kk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 83
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 89
    :cond_2
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f090e6e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/settings/samsungAccount/DeleteUserDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
