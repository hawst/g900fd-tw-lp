.class Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$2;
.super Ljava/lang/Object;
.source "HomeLatestData.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    .prologue
    .line 233
    new-instance v13, Ljava/lang/ref/WeakReference;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$2;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-direct {v13, v14}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 234
    .local v13, "w":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;>;"
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->sendUpdateViewFromSync:Ljava/lang/Boolean;
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$000(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Ljava/lang/Boolean;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 235
    .local v5, "needToRefreshWhole":Z
    if-nez v5, :cond_a

    .line 237
    const/4 v4, -0x1

    .line 238
    .local v4, "defaultValue":I
    int-to-long v1, v4

    .line 239
    .local v1, "burntCaloriesTime":J
    int-to-long v11, v4

    .line 240
    .local v11, "viewData":J
    const/4 v3, 0x0

    .line 243
    .local v3, "burntItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v15, 0x0

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->findChildView(I)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$400(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;I)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;

    move-result-object v10

    .line 244
    .local v10, "peodItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    if-nez v10, :cond_0

    const/4 v5, 0x1

    .line 246
    :cond_0
    if-nez v5, :cond_2

    .line 248
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v14

    if-nez v14, :cond_1

    .line 249
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->getPedometerData()Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$500(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v15

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$302(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/data/HomePedoData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    .line 252
    :cond_1
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->changeDataItemNumber:I
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$600(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)I

    move-result v9

    .line 253
    .local v9, "nowPedoPosition":I
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v15

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->findPositionCurrentList(J)I
    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$700(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;J)I

    move-result v7

    .line 254
    .local v7, "newPedoPosition":I
    if-ne v9, v7, :cond_7

    .line 256
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/16 v15, 0x61

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->findChildView(I)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$400(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;I)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;

    move-result-object v3

    .line 257
    if-nez v3, :cond_2

    const/4 v5, 0x1

    .line 265
    .end local v7    # "newPedoPosition":I
    .end local v9    # "nowPedoPosition":I
    :cond_2
    :goto_0
    if-nez v5, :cond_4

    .line 267
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v14

    if-nez v14, :cond_3

    .line 268
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->getPedometerData()Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$500(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v15

    # setter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v14, v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$302(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/data/HomePedoData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    .line 270
    :cond_3
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->changeDataItemNumber:I
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$600(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)I

    move-result v8

    .line 272
    .local v8, "nowBurntPosition":I
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v15

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$800(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)J

    move-result-wide v17

    cmp-long v14, v15, v17

    if-ltz v14, :cond_8

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/home/data/HomePedoData;->getTime()J

    move-result-wide v1

    .line 273
    :goto_1
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->findPositionCurrentList(J)I
    invoke-static {v14, v1, v2}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$700(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;J)I

    move-result v14

    add-int/lit8 v6, v14, 0x1

    .line 275
    .local v6, "newBurntPosition":I
    if-eq v8, v6, :cond_9

    const/4 v5, 0x1

    .line 278
    .end local v6    # "newBurntPosition":I
    .end local v8    # "nowBurntPosition":I
    :cond_4
    :goto_2
    if-nez v5, :cond_a

    .line 280
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/16 v16, 0x0

    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->pedoData:Lcom/sec/android/app/shealth/home/data/HomePedoData;
    invoke-static {v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$300(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/data/HomePedoData;

    move-result-object v15

    move/from16 v0, v16

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setWalkingMateView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;ZLcom/sec/android/app/shealth/home/data/HomePedoData;)J
    invoke-static {v14, v10, v0, v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$900(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;ZLcom/sec/android/app/shealth/home/data/HomePedoData;)J

    move-result-wide v11

    .line 281
    int-to-long v14, v4

    cmp-long v14, v11, v14

    if-lez v14, :cond_5

    .line 282
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setItemOnClickListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V
    invoke-static {v14, v10, v11, v12}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1000(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V

    .line 284
    :cond_5
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v15, 0x0

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setBurntCaloriesDataView(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J
    invoke-static {v14, v3, v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1100(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;Z)J

    move-result-wide v11

    .line 285
    int-to-long v14, v4

    cmp-long v14, v11, v14

    if-lez v14, :cond_6

    .line 286
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # invokes: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setItemOnClickListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V
    invoke-static {v14, v3, v11, v12}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1000(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V

    .line 300
    .end local v1    # "burntCaloriesTime":J
    .end local v3    # "burntItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .end local v4    # "defaultValue":I
    .end local v10    # "peodItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .end local v11    # "viewData":J
    :cond_6
    :goto_3
    return-void

    .line 261
    .restart local v1    # "burntCaloriesTime":J
    .restart local v3    # "burntItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .restart local v4    # "defaultValue":I
    .restart local v7    # "newPedoPosition":I
    .restart local v9    # "nowPedoPosition":I
    .restart local v10    # "peodItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .restart local v11    # "viewData":J
    :cond_7
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 272
    .end local v7    # "newPedoPosition":I
    .end local v9    # "nowPedoPosition":I
    .restart local v8    # "nowBurntPosition":I
    :cond_8
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mExerciseTime:J
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$800(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)J

    move-result-wide v1

    goto :goto_1

    .line 275
    .restart local v6    # "newBurntPosition":I
    :cond_9
    const/4 v5, 0x0

    goto :goto_2

    .line 291
    .end local v1    # "burntCaloriesTime":J
    .end local v3    # "burntItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .end local v4    # "defaultValue":I
    .end local v6    # "newBurntPosition":I
    .end local v8    # "nowBurntPosition":I
    .end local v10    # "peodItemView":Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;
    .end local v11    # "viewData":J
    :cond_a
    const-string v14, "HomeLatestData"

    const-string/jumbo v15, "mUpdateViewRunnable updateView"

    invoke-static {v14, v15}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->updateView(Ljava/lang/Boolean;)V

    .line 296
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    move-result-object v14

    if-eqz v14, :cond_6

    .line 298
    invoke-virtual {v13}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;
    invoke-static {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    move-result-object v14

    invoke-interface {v14}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;->refresh()V

    goto :goto_3
.end method
