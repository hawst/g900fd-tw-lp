.class public Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImplGingerbread;,
        Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImplBase;,
        Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;
    }
.end annotation


# static fields
.field static final IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;


# instance fields
.field mScroller:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImplGingerbread;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImplGingerbread;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImplBase;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImplBase;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    goto :goto_0
.end method

.method constructor <init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->createScroller(Landroid/content/Context;Landroid/view/animation/Interpolator;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    return-void
.end method

.method public static create(Landroid/content/Context;Landroid/view/animation/Interpolator;)Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;
    .locals 1

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    return-object v0
.end method


# virtual methods
.method public abortAnimation()V
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->abortAnimation(Ljava/lang/Object;)V

    return-void
.end method

.method public computeScrollOffset()Z
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->computeScrollOffset(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getCurrX()I
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->getCurrX(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getCurrY()I
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->getCurrY(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getFinalX()I
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->getFinalX(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getFinalY()I
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->getFinalY(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isFinished()Z
    .locals 2

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->isFinished(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public startScroll(IIIII)V
    .locals 7

    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->IMPL:Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat;->mScroller:Ljava/lang/Object;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/app/shealth/framework/ui/view/ScrollerCompat$ScrollerCompatImpl;->startScroll(Ljava/lang/Object;IIIII)V

    return-void
.end method
