.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SuccessSearchDialogButtonController;
.super Ljava/lang/Object;
.source "FoodTrackerActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuccessSearchDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;)V
    .locals 0

    .prologue
    .line 578
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$1;

    .prologue
    .line 578
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SuccessSearchDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 1
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 581
    sget-object v0, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->foodTrackerBaseFragment:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->resetDailyData()V

    .line 586
    :cond_0
    return-void
.end method
