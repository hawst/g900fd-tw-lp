.class public Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;
.super Ljava/lang/Object;
.source "CircleProgressDrawer.java"


# static fields
.field public static final COLOR_GOOD:I = -0x1

.field public static final COLOR_GOOD_DIM:I = 0x33ffffff

.field private static final COLOR_OVER:I = -0x1

.field private static final COLOR_OVER_DIM:I = 0x66ffffff

.field private static sImages:[Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Bitmap;

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized clearImages()V
    .locals 4

    .prologue
    .line 145
    const-class v1, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    .line 146
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 148
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    .line 149
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 152
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x2

    aget-object v0, v0, v2

    if-eqz v0, :cond_2

    .line 153
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x2

    const/4 v3, 0x0

    aput-object v3, v0, v2

    .line 155
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    aget-object v0, v0, v2

    if-eqz v0, :cond_3

    .line 156
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    :cond_3
    monitor-exit v1

    return-void

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized getImages(Landroid/content/Context;)[Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    const-class v1, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020719

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v0, v2

    .line 132
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 133
    :cond_2
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020717

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v0, v2

    .line 135
    :cond_3
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x2

    aget-object v0, v0, v2

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x2

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 136
    :cond_4
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020720

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v0, v2

    .line 138
    :cond_5
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    aget-object v0, v0, v2

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 139
    :cond_6
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020727

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v0, v2

    .line 141
    :cond_7
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    aput-object v3, v0, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    aput-object v3, v0, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    aput-object v3, v0, v2

    const/4 v2, 0x3

    sget-object v3, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->sImages:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    aput-object v3, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getSmallProgressBitmapBurned(Landroid/content/Context;FZZ)Landroid/graphics/Bitmap;
    .locals 28
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "progress"    # F
    .param p2, "registered"    # Z
    .param p3, "isPlain"    # Z

    .prologue
    .line 41
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->getImages(Landroid/content/Context;)[Landroid/graphics/Bitmap;

    move-result-object v21

    .line 47
    .local v21, "images":[Landroid/graphics/Bitmap;
    if-eqz p3, :cond_2

    .line 48
    const/4 v3, 0x0

    aget-object v23, v21, v3

    .line 49
    .local v23, "mCircleMask":Landroid/graphics/Bitmap;
    const/4 v3, 0x1

    aget-object v22, v21, v3

    .line 57
    .local v22, "mBacgroundCircle":Landroid/graphics/Bitmap;
    :goto_0
    if-nez p0, :cond_0

    .line 58
    const-string v3, "CircleProgressDrawer"

    const-string v4, "context is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    if-nez v23, :cond_3

    .line 61
    const-string v3, "CircleProgressDrawer"

    const-string v4, "Mask image is null"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const/16 v26, 0x0

    .line 125
    :cond_1
    :goto_1
    return-object v26

    .line 51
    .end local v22    # "mBacgroundCircle":Landroid/graphics/Bitmap;
    .end local v23    # "mCircleMask":Landroid/graphics/Bitmap;
    :cond_2
    const/4 v3, 0x2

    aget-object v23, v21, v3

    .line 52
    .restart local v23    # "mCircleMask":Landroid/graphics/Bitmap;
    const/4 v3, 0x3

    aget-object v22, v21, v3

    .restart local v22    # "mBacgroundCircle":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 65
    :cond_3
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v26

    .line 66
    .local v26, "out":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    move-object/from16 v0, v26

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 70
    .local v2, "canvas":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 80
    .local v7, "maskPaint_def":Landroid/graphics/Paint;
    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 81
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 82
    const/high16 v3, 0x66000000

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    new-instance v25, Landroid/graphics/Matrix;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Matrix;-><init>()V

    .line 84
    .local v25, "matrix_def":Landroid/graphics/Matrix;
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 85
    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 86
    new-instance v3, Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    invoke-direct {v3, v4, v5, v6, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v4, 0x0

    const v5, 0x470ca000    # 36000.0f

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-float v5, v5

    const/4 v6, 0x1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 88
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 89
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v2, v0, v3, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 92
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 93
    .local v13, "maskPaint":Landroid/graphics/Paint;
    const/4 v3, 0x1

    invoke-virtual {v13, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 94
    if-nez p2, :cond_4

    .line 95
    const v3, 0x33ffffff

    invoke-virtual {v13, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    :goto_2
    new-instance v24, Landroid/graphics/Matrix;

    invoke-direct/range {v24 .. v24}, Landroid/graphics/Matrix;-><init>()V

    .line 100
    .local v24, "matrix":Landroid/graphics/Matrix;
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    move-object/from16 v0, v24

    invoke-virtual {v0, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 101
    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 102
    new-instance v9, Landroid/graphics/RectF;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v9, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v10, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float v3, v3, p1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v11, v3

    const/4 v12, 0x1

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 105
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v13, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 106
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v2, v0, v3, v4, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 107
    const/4 v3, 0x0

    cmpl-float v3, p1, v3

    if-lez v3, :cond_1

    .line 108
    new-instance v19, Landroid/graphics/Paint;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Paint;-><init>()V

    .line 109
    .local v19, "thickMaskPaint":Landroid/graphics/Paint;
    const/4 v3, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 110
    if-nez p2, :cond_5

    .line 111
    const v3, 0x66ffffff

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 116
    :goto_3
    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v27

    .line 117
    .local v27, "thickProgress":Landroid/graphics/Bitmap;
    new-instance v14, Landroid/graphics/Canvas;

    move-object/from16 v0, v27

    invoke-direct {v14, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 118
    .local v14, "canvas2":Landroid/graphics/Canvas;
    new-instance v15, Landroid/graphics/RectF;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v15, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/16 v16, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float v3, v3, p1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v0, v3

    move/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v14 .. v19}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 120
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 121
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v3, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 123
    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 72
    .end local v7    # "maskPaint_def":Landroid/graphics/Paint;
    .end local v13    # "maskPaint":Landroid/graphics/Paint;
    .end local v14    # "canvas2":Landroid/graphics/Canvas;
    .end local v19    # "thickMaskPaint":Landroid/graphics/Paint;
    .end local v24    # "matrix":Landroid/graphics/Matrix;
    .end local v25    # "matrix_def":Landroid/graphics/Matrix;
    .end local v27    # "thickProgress":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v20

    .line 74
    .local v20, "ex":Ljava/lang/NullPointerException;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 75
    const-string v3, "CircleProgressDrawer"

    const-string v4, "canvas.drawBitmap thrown nullpointer exception!!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 97
    .end local v20    # "ex":Ljava/lang/NullPointerException;
    .restart local v7    # "maskPaint_def":Landroid/graphics/Paint;
    .restart local v13    # "maskPaint":Landroid/graphics/Paint;
    .restart local v25    # "matrix_def":Landroid/graphics/Matrix;
    :cond_4
    const/4 v3, -0x1

    invoke-virtual {v13, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_2

    .line 113
    .restart local v19    # "thickMaskPaint":Landroid/graphics/Paint;
    .restart local v24    # "matrix":Landroid/graphics/Matrix;
    :cond_5
    const/4 v3, -0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_3
.end method
