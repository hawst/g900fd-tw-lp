.class Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$5;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Lcom/sec/android/app/shealth/walkingmate/widget/input/GradationView$OnControllerTapListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->registerListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onControllerTapped(Z)V
    .locals 1
    .param p1, "isStart"    # Z

    .prologue
    .line 341
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->moveFocusFromValueEditText()V

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->onTouchModeEditTextColor(Z)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->mRootFocusLayout:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;->access$000(Lcom/sec/android/app/shealth/walkingmate/widget/input/InputModule;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 347
    :cond_0
    return-void
.end method
