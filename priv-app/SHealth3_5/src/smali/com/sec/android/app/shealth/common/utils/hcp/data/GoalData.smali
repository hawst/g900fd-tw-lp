.class public Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
.super Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
.source "GoalData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mGoalSubType:I

.field private mGoalType:I

.field private mPeriod:I

.field private mSetTime:J

.field private mValue:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 233
    new-instance v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 23
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 24
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 34
    return-void
.end method

.method public constructor <init>(FI)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "goalType"    # I

    .prologue
    const/4 v1, -0x1

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 23
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 24
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 122
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 123
    iput p2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 124
    return-void
.end method

.method public constructor <init>(FIII)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "goalType"    # I
    .param p3, "goalSubType"    # I
    .param p4, "period"    # I

    .prologue
    const/4 v1, -0x1

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 23
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 24
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 113
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->initMainFields(FIII)V

    .line 114
    return-void
.end method

.method public constructor <init>(FIIIJJ)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "goalType"    # I
    .param p3, "goalSubType"    # I
    .param p4, "period"    # I
    .param p5, "setTime"    # J
    .param p7, "id"    # J

    .prologue
    const/4 v1, -0x1

    .line 98
    invoke-direct {p0, p7, p8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(J)V

    .line 23
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 24
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 99
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->initMainFields(FIII)V

    .line 100
    iput-wide p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 101
    return-void
.end method

.method public constructor <init>(FIIIJJJJII)V
    .locals 9
    .param p1, "value"    # F
    .param p2, "goalType"    # I
    .param p3, "goalSubType"    # I
    .param p4, "period"    # I
    .param p5, "setTime"    # J
    .param p7, "id"    # J
    .param p9, "createTime"    # J
    .param p11, "updateTime"    # J
    .param p13, "timeZone"    # I
    .param p14, "daylightSaving"    # I

    .prologue
    .line 77
    move-object v0, p0

    move-wide/from16 v1, p7

    move-wide/from16 v3, p9

    move-wide/from16 v5, p11

    move/from16 v7, p13

    move/from16 v8, p14

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>(JJJII)V

    .line 23
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 78
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->initMainFields(FIII)V

    .line 79
    iput-wide p5, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 80
    return-void
.end method

.method public constructor <init>(JIFI)V
    .locals 3
    .param p1, "createTime"    # J
    .param p3, "goalType"    # I
    .param p4, "value"    # F
    .param p5, "goalSubType"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 138
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;-><init>()V

    .line 23
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 24
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 25
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    .line 26
    iput v2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    .line 27
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 139
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->setCreateTime(J)V

    .line 140
    invoke-direct {p0, p4, p3, p5, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->initMainFields(FIII)V

    .line 141
    return-void
.end method

.method private initMainFields(FIII)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "goalType"    # I
    .param p3, "goalSubType"    # I
    .param p4, "period"    # I

    .prologue
    .line 48
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "value should not be < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 52
    iput p2, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 53
    iput p3, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    .line 54
    iput p4, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    .line 55
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x0

    return v0
.end method

.method public getGoalSubType()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    return v0
.end method

.method public getGoalType()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    return v0
.end method

.method public getPeriod()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    return v0
.end method

.method public getSetTime()J
    .locals 2

    .prologue
    .line 209
    iget-wide v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    return-wide v0
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    return v0
.end method

.method public setGoalSubType(I)V
    .locals 0
    .param p1, "goalSubType"    # I

    .prologue
    .line 188
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    .line 189
    return-void
.end method

.method public setGoalType(I)V
    .locals 0
    .param p1, "goalType"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    .line 171
    return-void
.end method

.method public setPeriod(I)V
    .locals 0
    .param p1, "period"    # I

    .prologue
    .line 154
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    .line 155
    return-void
.end method

.method public setSetTime(J)V
    .locals 0
    .param p1, "setTime"    # J

    .prologue
    .line 216
    iput-wide p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mSetTime:J

    .line 217
    return-void
.end method

.method public setValue(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 202
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    .line 203
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 221
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mValue:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 222
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 223
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mGoalSubType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 224
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->mPeriod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getSetTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getId()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getCreateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getUpdateTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getTimeZone()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getDaylightSaving()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 231
    return-void
.end method
