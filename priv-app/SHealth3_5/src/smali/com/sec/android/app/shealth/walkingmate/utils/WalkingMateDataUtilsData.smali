.class public Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;
.super Ljava/lang/Object;
.source "WalkingMateDataUtilsData.java"


# instance fields
.field mCalories:F

.field mDistance:F

.field mTotalSteps:I

.field mUpdateTime:J


# direct methods
.method public constructor <init>(IFFJ)V
    .locals 2
    .param p1, "totalSteps"    # I
    .param p2, "distance"    # F
    .param p3, "calories"    # F
    .param p4, "updateTime"    # J

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mTotalSteps:I

    .line 5
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mDistance:F

    .line 6
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mCalories:F

    .line 7
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mUpdateTime:J

    .line 10
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mTotalSteps:I

    .line 11
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mDistance:F

    .line 12
    iput p3, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mCalories:F

    .line 13
    iput-wide p4, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mUpdateTime:J

    .line 14
    return-void
.end method


# virtual methods
.method public getCalories()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mCalories:F

    return v0
.end method

.method public getDistance()F
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mDistance:F

    return v0
.end method

.method public getTotalSteps()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mTotalSteps:I

    return v0
.end method

.method public getUpdateTime()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mUpdateTime:J

    return-wide v0
.end method

.method public setCalories(F)V
    .locals 0
    .param p1, "calories"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mCalories:F

    .line 26
    return-void
.end method

.method public setDistance(F)V
    .locals 0
    .param p1, "distance"    # F

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mDistance:F

    .line 22
    return-void
.end method

.method public setTotalSteps(I)V
    .locals 0
    .param p1, "totalSteps"    # I

    .prologue
    .line 17
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mTotalSteps:I

    .line 18
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 0
    .param p1, "updateTime"    # J

    .prologue
    .line 29
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingMateDataUtilsData;->mUpdateTime:J

    .line 30
    return-void
.end method
