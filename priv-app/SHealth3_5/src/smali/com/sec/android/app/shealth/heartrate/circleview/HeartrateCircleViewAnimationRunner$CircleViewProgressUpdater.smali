.class Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;
.super Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;
.source "HeartrateCircleViewAnimationRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CircleViewProgressUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;II)V
    .locals 0
    .param p2, "delay"    # I
    .param p3, "messagesCount"    # I

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    .line 46
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;-><init>(II)V

    .line 47
    return-void
.end method


# virtual methods
.method public isRun()Z
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateProgressUpdater;->isRun()Z

    move-result v0

    return v0
.end method

.method protected onProgressUpdate(ZF)V
    .locals 2
    .param p1, "isEnd"    # Z
    .param p2, "progress"    # F

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->access$000(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->setStartAngle(F)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->access$000(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    move-result-object v0

    const/high16 v1, 0x43380000    # 184.0f

    add-float/2addr v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->setEndAngle(F)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;->access$000(Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleViewAnimationRunner;)Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/circleview/HeartrateCircleView;->invalidate()V

    .line 54
    return-void
.end method
