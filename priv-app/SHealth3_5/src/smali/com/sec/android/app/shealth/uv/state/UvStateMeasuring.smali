.class public Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;
.super Lcom/sec/android/app/shealth/uv/state/UvState;
.source "UvStateMeasuring.java"


# static fields
.field private static final CURRENT_STATE_TIMEOUT:J = 0x4fb0L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isInCurrentState:Z

.field private mCurrentStateTimer:Landroid/os/CountDownTimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V
    .locals 6
    .param p1, "summaryFragment"    # Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/uv/state/UvState;-><init>(Lcom/sec/android/app/shealth/uv/UvSummaryFragment;)V

    .line 58
    new-instance v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring$1;

    const-wide/16 v2, 0x4fb0

    const-wide/16 v4, 0xc8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring$1;-><init>(Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    .line 22
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;

    .prologue
    .line 11
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->isInCurrentState:Z

    return v0
.end method


# virtual methods
.method public clearForNextState()V
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->TAG:Ljava/lang/String;

    const-string v1, "clearForNextState()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->isInCurrentState:Z

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 56
    return-void
.end method

.method public reStartStates()V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateViewWithAnimation(Z)V

    .line 28
    return-void
.end method

.method public terminateState()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->stopSensor()V

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->clearForNextState()V

    .line 83
    return-void
.end method

.method public updateNextState(II)V
    .locals 2
    .param p1, "rate"    # I
    .param p2, "interval"    # I

    .prologue
    .line 34
    if-ltz p1, :cond_0

    if-lez p2, :cond_0

    .line 35
    sget-object v0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->TAG:Ljava/lang/String;

    const-string v1, "UvStateMeasuring  updateNextState() measuring"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setUVData(I)V

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateFinished()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->setState(Lcom/sec/android/app/shealth/uv/state/UvState;)V

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->getStateFinished()Lcom/sec/android/app/shealth/uv/state/UvState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/state/UvState;->updateViewWithAnimation(Z)V

    .line 40
    :cond_0
    return-void
.end method

.method public updateViewWithAnimation(Z)V
    .locals 2
    .param p1, "hasAnimation"    # Z

    .prologue
    const/4 v1, 0x1

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->isInCurrentState:Z

    .line 45
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 46
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 47
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :cond_0
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/state/UvStateMeasuring;->mSummaryFragment:Lcom/sec/android/app/shealth/uv/UvSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/uv/UvSummaryFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 49
    return-void
.end method
