.class public Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;
.super Landroid/app/DialogFragment;
.source "ProgressDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$OnBackPressListener;
    }
.end annotation


# static fields
.field public static final MESSAGE_TEXT_KEY:Ljava/lang/String; = "message_text"


# instance fields
.field private onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$OnBackPressListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 25
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->setRetainInstance(Z)V

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$OnBackPressListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$OnBackPressListener;

    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$OnBackPressListener;)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "messageText"    # Ljava/lang/String;
    .param p1, "onBackPressListener"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$OnBackPressListener;

    .prologue
    .line 35
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;-><init>()V

    .line 36
    .local v1, "progressDialogFragment":Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 37
    .local v0, "arguments":Landroid/os/Bundle;
    const-string/jumbo v2, "message_text"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 39
    iput-object p1, v1, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->onBackPressListener:Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$OnBackPressListener;

    .line 40
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->setCancelable(Z)V

    .line 47
    const/4 v0, 0x2

    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$style;->Dialog_NoTitleBar_And_Frame:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->setStyle(II)V

    .line 48
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$1;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->getTheme()I

    move-result v3

    invoke-direct {v0, p0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment$1;-><init>(Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;Landroid/content/Context;I)V

    .line 77
    .local v0, "dialog":Landroid/app/Dialog;
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 78
    .local v1, "lpWindow":Landroid/view/WindowManager$LayoutParams;
    const/4 v2, 0x2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 79
    const v2, 0x3e99999a    # 0.3f

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 80
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 81
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x1

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/common/commonui/R$layout;->loading_dialog:I

    const/4 v5, 0x0

    invoke-static {v1, v2, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 53
    .local v7, "content":Landroid/view/View;
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->loading_img:I

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 54
    .local v9, "receivingView":Landroid/view/View;
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x43b40000    # 360.0f

    move v5, v3

    move v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 55
    .local v0, "animation":Landroid/view/animation/Animation;
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 56
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 57
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 58
    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 59
    invoke-virtual {v9, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 60
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->tw_dialog_full_holo_light:I

    invoke-virtual {v7, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 61
    sget v1, Lcom/sec/android/app/shealth/common/commonui/R$id;->txt_receiving:I

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 62
    .local v8, "message":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ProgressDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "message_text"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    return-object v7
.end method
