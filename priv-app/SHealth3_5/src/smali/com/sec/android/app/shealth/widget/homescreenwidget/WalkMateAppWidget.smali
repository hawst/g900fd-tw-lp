.class public Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;
.super Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;
.source "WalkMateAppWidget.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Home."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Constructor called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    sget-object v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x2

    aput-object p0, v0, v1

    .line 43
    return-void
.end method


# virtual methods
.method public onDisabled(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->sWidgets:[Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;

    const/4 v1, 0x2

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 48
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->onDisabled(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method protected updateWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 53
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 55
    const-string v21, "AbstractWalkMateAppWidget"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "updateWidget() - appWidgetIds size = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v21

    if-nez v21, :cond_1

    const/4 v10, 0x1

    .line 57
    .local v10, "isDBPasswordLocked":Z
    :goto_0
    const-string v21, "AbstractWalkMateAppWidget"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "isDBPasswordLocked : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v21

    if-nez v21, :cond_0

    if-eqz v10, :cond_2

    .line 60
    :cond_0
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_17

    .line 61
    new-instance v19, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f030284

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 62
    .local v19, "views":Landroid/widget/RemoteViews;
    new-instance v8, Landroid/content/Intent;

    const-class v21, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 63
    .local v8, "intent":Landroid/content/Intent;
    const-string v21, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    const-string/jumbo v21, "widgetActivityAction"

    const-string v22, "com.sec.shealth.action.PEDOMETER"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string/jumbo v21, "widgetActivityPackage"

    const-string v22, "com.sec.android.app.shealth"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v21

    const/16 v22, 0x1ca8

    const/high16 v23, 0x8000000

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v8, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v14

    .line 67
    .local v14, "pendingIntent":Landroid/app/PendingIntent;
    const v21, 0x7f080b48

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1, v14}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 68
    aget v21, p3, v7

    move-object/from16 v0, p2

    move/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 60
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 56
    .end local v7    # "i":I
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v10    # "isDBPasswordLocked":Z
    .end local v14    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v19    # "views":Landroid/widget/RemoteViews;
    :cond_1
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 73
    .restart local v10    # "isDBPasswordLocked":Z
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getHRAvailabliltyFalseIfUnknown(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_5

    sget-object v21, Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;->HeartRate:Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/shealth/common/config/FeatureTable;->checkFeature(Lcom/sec/android/app/shealth/common/config/FeatureTable$FeatureType;)Lcom/sec/android/app/shealth/common/config/FeatureTable$Support;

    move-result-object v21

    sget-object v22, Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;->NOT_SUPPORTED:Lcom/sec/android/app/shealth/common/config/FeatureTable$HeartRate;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_5

    const/4 v6, 0x1

    .line 76
    .local v6, "hrAvailability":Z
    :goto_2
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v4

    .line 77
    .local v4, "connectedDevice":I
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v11

    .line 78
    .local v11, "lIsStartWalking":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getLastUpdatedTime()Ljava/lang/String;

    move-result-object v12

    .line 79
    .local v12, "lastUpdatedTime":Ljava/lang/String;
    const-string v21, "AbstractWalkMateAppWidget"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Last updated Time = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getCurrentMode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 82
    .local v5, "currentMode":Ljava/lang/String;
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 84
    .local v9, "isAccessoryConnected":Z
    const/16 v17, 0x0

    .line 85
    .local v17, "stepsValue":I
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-wide/from16 v0, v21

    long-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v15, v21, v22

    .line 86
    .local v15, "percent":F
    float-to-double v0, v15

    move-wide/from16 v21, v0

    const-wide v23, 0x3fb999999999999aL    # 0.1

    cmpl-double v21, v21, v23

    if-ltz v21, :cond_6

    move/from16 v21, v15

    :goto_3
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-static {v0, v1, v11, v2}, Lcom/sec/android/app/shealth/walkingmate/widget/homescreenwidget/CircleProgressDrawer;->getSmallProgressBitmapBurned(Landroid/content/Context;FZZ)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 88
    .local v16, "progressImage":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v13

    .line 90
    .local v13, "launchIntent":Landroid/app/PendingIntent;
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_4
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_17

    .line 92
    new-instance v19, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const v22, 0x7f030283

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 94
    .restart local v19    # "views":Landroid/widget/RemoteViews;
    const v21, 0x7f080b4d

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 95
    const v21, 0x7f080b4e

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 96
    const v21, 0x7f080b4f

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 97
    const v21, 0x7f080b50

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 98
    if-nez v11, :cond_8

    const-string/jumbo v21, "normal"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 99
    if-eqz v9, :cond_7

    const v17, 0x7f080b50

    .line 100
    :goto_5
    const v21, 0x7f080b53

    const v22, 0x7f090a17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 115
    :goto_6
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 117
    const v21, 0x7f080b54

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 118
    const v21, 0x7f080b55

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 120
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mWearableConnected:Z

    move/from16 v21, v0

    if-eqz v21, :cond_b

    .line 121
    const v21, 0x7f080520

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 122
    const v21, 0x7f080521

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 123
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSyncing()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 124
    const v21, 0x7f080520

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 125
    const v21, 0x7f080521

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 136
    :cond_3
    :goto_7
    const/16 v21, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 137
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x2710

    cmp-long v21, v21, v23

    if-ltz v21, :cond_d

    .line 138
    const/16 v21, 0x1

    const/high16 v22, 0x42100000    # 36.0f

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    .line 142
    :goto_8
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    const-wide/32 v23, 0x186a0

    cmp-long v21, v21, v23

    if-ltz v21, :cond_e

    .line 143
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-wide/from16 v0, v21

    long-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    .line 144
    .local v18, "value":Ljava/lang/String;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f090bb4

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move/from16 v1, v17

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 149
    .end local v18    # "value":Ljava/lang/String;
    :goto_9
    const v21, 0x7f080b51

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0909d9

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 159
    const-string v21, "gear"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 161
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 163
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_f

    .line 164
    const v21, 0x7f080b31

    const v22, 0x7f020723

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 186
    :goto_a
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-gez v21, :cond_14

    .line 187
    const-string/jumbo v21, "normal"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    if-nez v11, :cond_4

    .line 188
    const v21, 0x7f080b31

    const v22, 0x7f02072c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 190
    :cond_4
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 191
    const v21, 0x7f080b32

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 195
    :goto_b
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f070044

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getColor(I)I

    move-result v20

    .line 196
    .local v20, "white":I
    const v21, 0x7f080b53

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 197
    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 198
    const v21, 0x7f080b51

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 201
    const-string/jumbo v21, "normal"

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_15

    if-nez v11, :cond_15

    .line 202
    const/16 v21, 0x80

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    invoke-static/range {v21 .. v24}, Landroid/graphics/Color;->argb(IIII)I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 208
    :goto_c
    const v21, 0x7f080b49

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090019

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v23, v0

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f0900c6

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 210
    const v21, 0x7f080b49

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1, v13}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 211
    const v21, 0x7f080b53

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1, v13}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 218
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mWearableConnected:Z

    move/from16 v21, v0

    if-eqz v21, :cond_16

    .line 219
    const v21, 0x7f080b52

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getSyncPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 224
    :goto_d
    aget v21, p3, v7

    move-object/from16 v0, p2

    move/from16 v1, v21

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 90
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_4

    .line 73
    .end local v4    # "connectedDevice":I
    .end local v5    # "currentMode":Ljava/lang/String;
    .end local v6    # "hrAvailability":Z
    .end local v7    # "i":I
    .end local v9    # "isAccessoryConnected":Z
    .end local v11    # "lIsStartWalking":Z
    .end local v12    # "lastUpdatedTime":Ljava/lang/String;
    .end local v13    # "launchIntent":Landroid/app/PendingIntent;
    .end local v15    # "percent":F
    .end local v16    # "progressImage":Landroid/graphics/Bitmap;
    .end local v17    # "stepsValue":I
    .end local v19    # "views":Landroid/widget/RemoteViews;
    .end local v20    # "white":I
    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 86
    .restart local v4    # "connectedDevice":I
    .restart local v5    # "currentMode":Ljava/lang/String;
    .restart local v6    # "hrAvailability":Z
    .restart local v9    # "isAccessoryConnected":Z
    .restart local v11    # "lIsStartWalking":Z
    .restart local v12    # "lastUpdatedTime":Ljava/lang/String;
    .restart local v15    # "percent":F
    .restart local v17    # "stepsValue":I
    :cond_6
    const/16 v21, 0x0

    goto/16 :goto_3

    .line 99
    .restart local v7    # "i":I
    .restart local v13    # "launchIntent":Landroid/app/PendingIntent;
    .restart local v16    # "progressImage":Landroid/graphics/Bitmap;
    .restart local v19    # "views":Landroid/widget/RemoteViews;
    :cond_7
    const v17, 0x7f080b4e

    goto/16 :goto_5

    .line 102
    :cond_8
    if-eqz v9, :cond_9

    const v17, 0x7f080b4f

    .line 103
    :goto_e
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v21

    if-eqz v21, :cond_a

    .line 104
    const v21, 0x7f080b53

    sget-object v22, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090b68

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v26, v0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v22 .. v24}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 102
    :cond_9
    const v17, 0x7f080b4d

    goto :goto_e

    .line 106
    :cond_a
    const v21, 0x7f080b53

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f090b68

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 128
    :cond_b
    const v21, 0x7f080520

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 129
    const v21, 0x7f080521

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_7

    .line 132
    :cond_c
    const v21, 0x7f080b54

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_7

    .line 140
    :cond_d
    const/16 v21, 0x1

    const/high16 v22, 0x42200000    # 40.0f

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewTextSize(IIF)V

    goto/16 :goto_8

    .line 146
    :cond_e
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move/from16 v1, v17

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_9

    .line 166
    :cond_f
    const v21, 0x7f080b31

    const v22, 0x7f02071e

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_a

    .line 168
    :cond_10
    if-nez v4, :cond_11

    .line 169
    const v21, 0x7f080b31

    const v22, 0x7f020728

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 170
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_a

    .line 172
    :cond_11
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mTotalStep:J

    move-wide/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->mGoalStep:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v23, v0

    cmp-long v21, v21, v23

    if-ltz v21, :cond_12

    .line 173
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 174
    const v21, 0x7f080b31

    const v22, 0x7f02072f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_a

    .line 176
    :cond_12
    float-to-double v0, v15

    move-wide/from16 v21, v0

    const-wide v23, 0x3fb999999999999aL    # 0.1

    cmpl-double v21, v21, v23

    if-ltz v21, :cond_13

    .line 177
    const v21, 0x7f080b31

    const v22, 0x7f020727

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 178
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_a

    .line 180
    :cond_13
    const v21, 0x7f080b31

    const v22, 0x7f020728

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v11, v4}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getIconBitmap(Landroid/content/Context;IZI)Landroid/graphics/Bitmap;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 181
    const v21, 0x7f080b32

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto/16 :goto_a

    .line 193
    :cond_14
    const v21, 0x7f080b32

    const/16 v22, 0x8

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_b

    .line 205
    .restart local v20    # "white":I
    :cond_15
    const/16 v21, 0xff

    const/16 v22, 0xff

    const/16 v23, 0xff

    const/16 v24, 0xff

    invoke-static/range {v21 .. v24}, Landroid/graphics/Color;->argb(IIII)I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v17

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_c

    .line 221
    :cond_16
    const v21, 0x7f080b52

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;->getLaunchPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v22

    move-object/from16 v0, v19

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_d

    .line 226
    .end local v4    # "connectedDevice":I
    .end local v5    # "currentMode":Ljava/lang/String;
    .end local v6    # "hrAvailability":Z
    .end local v9    # "isAccessoryConnected":Z
    .end local v11    # "lIsStartWalking":Z
    .end local v12    # "lastUpdatedTime":Ljava/lang/String;
    .end local v13    # "launchIntent":Landroid/app/PendingIntent;
    .end local v15    # "percent":F
    .end local v16    # "progressImage":Landroid/graphics/Bitmap;
    .end local v17    # "stepsValue":I
    .end local v19    # "views":Landroid/widget/RemoteViews;
    .end local v20    # "white":I
    :cond_17
    return-void
.end method
