.class public Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "InitialProfileSamsungAccount.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$IRestoreCompleteListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;,
        Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$PagerAdapter;
    }
.end annotation


# static fields
.field protected static final ADD_SHEALTH_ACCOUNT:Ljava/lang/String; = "com.sec.android.service.health.cp.serversync.syncadapter.authenticator.SHEALTH_ACCOUNT"

.field public static PAGE_COUNT:I = 0x0

.field protected static final REQUEST_CODE:I = 0x3e7


# instance fields
.field private SAMSUNG_ACCOUNT_DISABLE:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field private mAppID:Ljava/lang/String;

.field private mBottomLayout:Landroid/widget/FrameLayout;

.field private mBottomRightButton:Landroid/widget/RelativeLayout;

.field private mContext:Landroid/content/Context;

.field private mPagerAdapter:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$PagerAdapter;

.field private mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;

.field private mSamsungAccountSignIn:Landroid/widget/Button;

.field private mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field rightButtomClickListener:Landroid/view/View$OnClickListener;

.field signInClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->PAGE_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 58
    const-class v0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->TAG:Ljava/lang/String;

    .line 63
    const-string/jumbo v0, "samsung_account_disable"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->SAMSUNG_ACCOUNT_DISABLE:Ljava/lang/String;

    .line 72
    const-string v0, "com.sec.android.app.shealth"

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mAppID:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mContext:Landroid/content/Context;

    .line 167
    new-instance v0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$2;-><init>(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->rightButtomClickListener:Landroid/view/View$OnClickListener;

    .line 196
    new-instance v0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$3;-><init>(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->signInClickListener:Landroid/view/View$OnClickListener;

    .line 320
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->setFooterButton(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mAppID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 51
    sput-object p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->sBirthday:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->sBirthday:Ljava/lang/String;

    return-object v0
.end method

.method private initLayout()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 106
    const v1, 0x7f080627

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mBottomLayout:Landroid/widget/FrameLayout;

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mBottomLayout:Landroid/widget/FrameLayout;

    const v2, 0x7f080613

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mBottomRightButton:Landroid/widget/RelativeLayout;

    .line 108
    const v1, 0x7f080622

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSamsungAccountSignIn:Landroid/widget/Button;

    .line 110
    invoke-static {p0}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->getSamsungAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSamsungAccountSignIn:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSamsungAccountSignIn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->signInClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mBottomRightButton:Landroid/widget/RelativeLayout;

    const v2, 0x7f08039d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090045

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeUtil;->getMigrationState(Landroid/content/Context;)I

    move-result v0

    .line 118
    .local v0, "migrationState":I
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v3, :cond_2

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mBottomLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mBottomRightButton:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->rightButtomClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mBottomRightButton:Landroid/widget/RelativeLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901f7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09020a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 129
    :cond_1
    :goto_0
    const v1, 0x7f080621

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 130
    new-instance v1, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$PagerAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$PagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mPagerAdapter:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$PagerAdapter;

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mPagerAdapter:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$PagerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$1;-><init>(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 148
    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->setFooterButton(I)V

    .line 149
    return-void

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mBottomLayout:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 124
    if-ne v0, v3, :cond_1

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->TAG:Ljava/lang/String;

    const-string v2, "MIGRATION - SamsungAccount not enabled! But MUST BE migrate from Fitness With Gear to new SHealth with SamsungAccount - server data restore"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private registerProfileFinishedReceiver()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;

    if-nez v0, :cond_0

    .line 339
    new-instance v0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;-><init>(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;

    .line 340
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.shealth.action.FINISHED_PROFILE_SETUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 345
    :cond_0
    return-void
.end method

.method private setFooterButton(I)V
    .locals 5
    .param p1, "mPosition"    # I

    .prologue
    const v4, 0x7f080625

    const v0, 0x7f080624

    const v3, 0x7f020296

    const v2, 0x7f020295

    .line 152
    packed-switch p1, :pswitch_data_0

    .line 165
    :goto_0
    return-void

    .line 154
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 155
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 158
    :pswitch_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 159
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 152
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private unregisterProfileFinishedReceiver()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 355
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mProfileFinishedReceiver:Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$ProfileFinishedReceiver;

    .line 357
    :cond_0
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f09086f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 96
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->SAMSUNG_ACCOUNT_DISABLE:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    new-instance v0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$5;-><init>(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)V

    .line 308
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 213
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Samsung Account Login MESSAGE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " resultCode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    const/16 v0, 0xd

    .line 218
    .local v0, "SAMSUNG_ACCOUNT_DISABLED":I
    packed-switch p1, :pswitch_data_0

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 221
    :pswitch_0
    const/4 v3, -0x1

    if-ne p2, v3, :cond_2

    .line 223
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->setInitialSettingOngoing(Z)V

    .line 224
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v3, p0, v4}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationEnable(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/SamsungAccount;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 226
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "1y90e30264"

    const-string v6, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v7, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$4;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount$4;-><init>(Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;)V

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    .line 248
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 249
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->startActivity(Landroid/content/Intent;)V

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->finish()V

    goto :goto_0

    .line 256
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_2
    if-eqz p2, :cond_0

    .line 260
    if-ne p2, v0, :cond_0

    .line 261
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 262
    .local v1, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const v3, 0x7f0900e1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 263
    const v3, 0x7f090870

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 264
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->SAMSUNG_ACCOUNT_DISABLE:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 218
    :pswitch_data_0
    .packed-switch 0x3e7
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    iput-object p0, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mContext:Landroid/content/Context;

    .line 79
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 80
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;->getInstance()Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->mSecuritySettingsHelper:Lcom/sec/android/app/shealth/framework/ui/base/SecuritySettingsHelper;

    .line 82
    const v1, 0x7f030169

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->setContentView(I)V

    .line 83
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isChinaModel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 88
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->initLayout()V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->registerProfileFinishedReceiver()V

    .line 90
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->unregisterProfileFinishedReceiver()V

    .line 101
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    .line 102
    return-void
.end method

.method public restoreCompleted(Z)V
    .locals 3
    .param p1, "restored"    # Z

    .prologue
    .line 315
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 316
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->startActivity(Landroid/content/Intent;)V

    .line 317
    return-void
.end method

.method protected startSHealthAccountSignInActivity()V
    .locals 3

    .prologue
    .line 205
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.service.health.cp.serversync.syncadapter.authenticator.SHEALTH_ACCOUNT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 206
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "packageName"

    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 208
    const/16 v1, 0x3e7

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/settings/initialprofile/InitialProfileSamsungAccount;->startActivityForResult(Landroid/content/Intent;I)V

    .line 209
    return-void
.end method
