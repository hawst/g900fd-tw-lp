.class Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;
.super Ljava/lang/Object;
.source "DataMigrationHelper.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;)V
    .locals 0

    .prologue
    .line 804
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPress(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    .line 808
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$502(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 809
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mCurrentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$402(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$Step;

    .line 810
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1900(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1900(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 812
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1900(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 813
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mMigrationProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$1902(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 815
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v0, v0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13$2;->this$1:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;

    iget-object v1, v1, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper$13;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->mAppID:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "MI05"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/DataMigrationHelper;->cancelMigration()V

    .line 817
    return-void
.end method
