.class public interface abstract Lcom/sec/android/app/shealth/plugins/exercisepro/constants/RangeConstants;
.super Ljava/lang/Object;
.source "RangeConstants.java"


# static fields
.field public static final BMI_NORMAL_MAX:F = 25.0f

.field public static final BMI_NORMAL_MIN:F = 18.5f

.field public static final BMI_RECOMMENDED:F = 23.5f

.field public static final BMR_LOWER_LIMIT:I = 0x618

.field public static final BMR_UPPER_LIMIT:I = 0x618

.field public static final CALORIES_CONSUMPTION_MAX:I = 0x2710

.field public static final CALORIES_CONSUMPTION_MIN:I = 0x64

.field public static final CALORIES_CONSUMPTION_RECOMMENDED:I = 0x9c4

.field public static final CALORIES_GOAL_MAX:I = 0x1869f

.field public static final CALORIES_GOAL_MIN:I = 0x0

.field public static final CALORIES_INTAKE_MAX:I = 0x2710

.field public static final CALORIES_INTAKE_MIN:I = 0x64

.field public static final CALORIES_INTAKE_RECOMMENDED:I = 0x9c4

.field public static final COMFORT_LEVEL_HUMIDITY_MAX:F = 99.0f

.field public static final COMFORT_LEVEL_HUMIDITY_MIN:F = 10.0f

.field public static final COMFORT_LEVEL_TEMPERATURE_MAX:F = 60.0f

.field public static final COMFORT_LEVEL_TEMPERATURE_MIN:F = -20.0f

.field public static final COMFORT_LEVEL_TEMPERATURE_SUMMER_MAX:F = 26.0f

.field public static final COMFORT_LEVEL_TEMPERATURE_SUMMER_MIN:F = 23.0f

.field public static final COMFORT_LEVEL_TEMPERATURE_WINTER_MAX:F = 24.0f

.field public static final COMFORT_LEVEL_TEMPERATURE_WINTER_MIN:F = 20.0f

.field public static final COMFORT_ZONE_HUMIDITY_BALANCE_VALUE_MAX:F = 70.0f

.field public static final COMFORT_ZONE_HUMIDITY_BALANCE_VALUE_MIN:F = 30.0f

.field public static final COMFORT_ZONE_TEMPERATURE_BALANCE_VALUE_MAX:F = 26.0f

.field public static final COMFORT_ZONE_TEMPERATURE_BALANCE_VALUE_MIN:F = 20.0f

.field public static final DAY_BACKGROUND_END_HOUR:I = 0x13

.field public static final DAY_BACKGROUND_START_HOUR:I = 0x7

.field public static final EXERCISE_CALORIES_MAX:I = 0x2710

.field public static final EXERCISE_CALORIES_MIN:I = 0x64

.field public static final EXERCISE_CALORIES_RECOMMENDED:I = 0x3e8

.field public static final EXERCISE_GOAL_MAX:I = 0xbb8

.field public static final EXERCISE_GOAL_MIN:I = 0x0

.field public static final EXERCISE_RECOMMENDED:I = 0xa28

.field public static final EXERCISE_ZERO_VALUE:I = 0x64

.field public static final FOOD_CALORIES_MAX:I = 0x1869f

.field public static final FOOD_CALORIES_MIN:I = 0x0

.field public static final FOOD_DIARY_RECOMMENDED:I = 0x960

.field public static final GLUCOSE_AFTER_MEAL_BALANCE_VALUE_MAX:F = 139.0f

.field public static final GLUCOSE_AFTER_MEAL_BALANCE_VALUE_MAX_KOREA:F = 140.0f

.field public static final GLUCOSE_AFTER_MEAL_BALANCE_VALUE_MIN:F = 0.0f

.field public static final GLUCOSE_AFTER_MEAL_BALANCE_VALUE_MIN_KOREA:F = 90.0f

.field public static final GLUCOSE_AFTER_MEAL_MAX:I = 0x258

.field public static final GLUCOSE_AFTER_MEAL_MIN:I = 0xa

.field public static final GLUCOSE_EMPTY_BALANCE_VALUE_MAX:F = 99.0f

.field public static final GLUCOSE_EMPTY_BALANCE_VALUE_MAX_KOREA:F = 100.0f

.field public static final GLUCOSE_EMPTY_BALANCE_VALUE_MIN:F = 0.0f

.field public static final GLUCOSE_EMPTY_BALANCE_VALUE_MIN_KOREA:F = 70.0f

.field public static final GLUCOSE_EMPTY_MAX:I = 0x258

.field public static final GLUCOSE_EMPTY_MIN:I = 0xa

.field public static final HEARTBEAT_GOAL_INTERVAL:I = 0x1e

.field public static final HEARTBEAT_HIGH_BALANCE_VALUE_MAX:F = 250.0f

.field public static final HEARTBEAT_HIGH_BALANCE_VALUE_MIN:F = 20.0f

.field public static final HEARTBEAT_VALUE_MAX:F = 250.0f

.field public static final HEARTBEAT_VALUE_MIN:F = 20.0f

.field public static final HEIGHT_MAX_IN_CM:I = 0x12c

.field public static final HEIGHT_MIN_IN_CM:I = 0x14

.field public static final MEDICATION_DEFAULT_HOURS:I = 0x8

.field public static final MEDICATION_DEFAULT_MINUTES:I = 0x1e

.field public static final PERIOD_MAX:I = 0x16d

.field public static final PERIOD_MIN:I = 0x0

.field public static final PRESSURE_DIASTOLIC_BALANCE_VALUE_MAX:F = 89.0f

.field public static final PRESSURE_DIASTOLIC_BALANCE_VALUE_MIN:F = 0.0f

.field public static final PRESSURE_DIASTOLIC_MAX:I = 0x96

.field public static final PRESSURE_DIASTOLIC_MIN:I = 0x28

.field public static final PRESSURE_HIGH_BALANCE_VALUE_MAX_KOREA:F = 119.0f

.field public static final PRESSURE_HIGH_BALANCE_VALUE_MIN_KOREA:F = 0.0f

.field public static final PRESSURE_LOW_BALANCE_VALUE_MAX_KOREA:F = 79.0f

.field public static final PRESSURE_LOW_BALANCE_VALUE_MAX_KR:F = 79.0f

.field public static final PRESSURE_LOW_BALANCE_VALUE_MIN_KOREA:F = 0.0f

.field public static final PRESSURE_LOW_GOAL_INTERVAL:I = 0x28

.field public static final PRESSURE_SYSTOLIC_BALANCE_VALUE_MAX:F = 139.0f

.field public static final PRESSURE_SYSTOLIC_BALANCE_VALUE_MIN:F = 0.0f

.field public static final PRESSURE_SYSTOLIC_MAX:I = 0xc8

.field public static final PRESSURE_SYSTOLIC_MIN:I = 0x46

.field public static final REALTIME_CALORIE_MAX:I = 0x2706

.field public static final REALTIME_CALORIE_MIN:I = 0xa

.field public static final REALTIME_DISTANCE_MAX:I = 0xf3e58

.field public static final REALTIME_DISTANCE_MAX_MILE:I = 0xf39a1

.field public static final REALTIME_DISTANCE_MIN:I = 0x64

.field public static final REALTIME_DISTANCE_MIN_MILE:I = 0xa1

.field public static final REALTIME_DURATION_MAX:I = 0x2d0

.field public static final REALTIME_DURATION_MAX_SEC:I = 0xa8c0

.field public static final REALTIME_DURATION_MAX_TE_GOAL:I = 0x78

.field public static final REALTIME_DURATION_MIN:I = 0xa

.field public static final REALTIME_HOUR_MAX:I = 0xc

.field public static final REALTIME_HOUR_MIN:I = 0x0

.field public static final REALTIME_MIN_MAX:I = 0x3b

.field public static final REALTIME_MIN_MIN:I = 0x0

.field public static final SLEEP_DEFAULT_TIME:I = 0x276

.field public static final SLEEP_DEFAULT_TIMEZONE:I = 0xa

.field public static final STEPS_RECOMMENDED_LOWER:I = 0x1f40

.field public static final STEPS_RECOMMENDED_MIDDLE:I = 0x2710

.field public static final STEPS_RECOMMENDED_UPPER:I = 0x2ee0

.field public static final WALK_FOR_LIFE_MAX:I = 0xc350

.field public static final WALK_FOR_LIFE_MIN:I = 0x3e8

.field public static final WALK_FOR_LIFE_RECOMMENDED:I = 0x2710

.field public static final WALK_FOR_LIFE_STEPS_TO_UPDATE_GRAPH:I = 0x0

.field public static final WALK_FOR_LIFE_ZERO_VALUE:I = 0x2710

.field public static final WEIGHT_BALANCE_VALUE_MAX:F = 65.0f

.field public static final WEIGHT_BALANCE_VALUE_MIN:F = 50.0f

.field public static final WEIGHT_MAX_IN_KG:I = 0x1f4

.field public static final WEIGHT_MAX_IN_LB:F = 1102.3f

.field public static final WEIGHT_MIN_IN_KG:I = 0x2
