.class Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;
.super Ljava/lang/Object;
.source "HomeLatestData.java"

# interfaces
.implements Lcom/sec/android/app/shealth/home/dashboard/SwipeTouchListener$DismissCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->setItemOnClickListener(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestDataItemView;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)V
    .locals 0

    .prologue
    .line 609
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public canDismiss(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 612
    const/4 v0, 0x1

    return v0
.end method

.method public onDismiss(Landroid/view/View;Ljava/lang/Object;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "viewData"    # Ljava/lang/Object;

    .prologue
    .line 617
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast p2, Ljava/lang/Long;

    .end local p2    # "viewData":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveDeleteDashboardItem(Ljava/lang/String;J)V

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1700(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1700(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1800(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->blankTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1800(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0907dd

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;->refresh()V

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->mHomeLatestInterface:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1200(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$5;->this$0:Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;

    # getter for: Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->listContainer:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;->access$1700(Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/home/dashboard/HomeLatestData$HomeLatestDataRefreshInterface;->itemsNumberChange(I)V

    .line 628
    :cond_1
    return-void
.end method
