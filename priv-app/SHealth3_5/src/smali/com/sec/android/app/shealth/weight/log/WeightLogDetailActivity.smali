.class public Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "WeightLogDetailActivity.java"


# instance fields
.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field private isfromaccesory:Z

.field private mAccessoryType:Landroid/widget/TextView;

.field private mCommentTitle:Landroid/widget/TextView;

.field protected mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

.field private mLogDetailMemoText:Landroid/widget/TextView;

.field private mTimeDate:Landroid/widget/TextView;

.field private mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

.field private mWeight:Landroid/widget/TextView;

.field private mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

.field private mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

.field private mWeightInputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

.field private mWeightStartValue:F

.field private mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

.field private mWeightdate:Landroid/widget/LinearLayout;

.field private mWeightvalue:Landroid/widget/LinearLayout;

.field private mlogDetailsMemoLayout:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 210
    new-instance v0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeight:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->isfromaccesory:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    return-object v0
.end method

.method private initView(Ljava/lang/String;)V
    .locals 12
    .param p1, "heartrateId"    # Ljava/lang/String;

    .prologue
    .line 106
    new-instance v8, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    .line 111
    const v8, 0x7f08052e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    .line 112
    const v8, 0x7f08052f

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    .line 113
    const v8, 0x7f080c3a

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeight:Landroid/widget/TextView;

    .line 115
    const v8, 0x7f080c38

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightdate:Landroid/widget/LinearLayout;

    .line 116
    const v8, 0x7f080c39

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightvalue:Landroid/widget/LinearLayout;

    .line 120
    new-instance v8, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-direct {v8, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    .line 121
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    .line 122
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    check-cast v8, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 123
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v8

    iput v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightStartValue:F

    .line 124
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeight:Landroid/widget/TextView;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget v10, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightStartValue:F

    invoke-static {v10}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {v10, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    const v8, 0x7f080c79

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout;

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mlogDetailsMemoLayout:Landroid/widget/FrameLayout;

    .line 126
    const v8, 0x7f0803e5

    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mLogDetailMemoText:Landroid/widget/TextView;

    .line 136
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v8

    if-eqz v8, :cond_2

    .line 137
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mlogDetailsMemoLayout:Landroid/widget/FrameLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 138
    new-instance v2, Landroid/text/SpannableString;

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 139
    .local v2, "s":Landroid/text/SpannableString;
    new-instance v8, Landroid/text/style/LeadingMarginSpan$Standard;

    const/16 v9, 0x67

    const/4 v10, 0x0

    invoke-direct {v8, v9, v10}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 140
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mLogDetailMemoText:Landroid/widget/TextView;

    invoke-virtual {v8, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    .end local v2    # "s":Landroid/text/SpannableString;
    :goto_0
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->isfromaccesory:Z

    .line 149
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 150
    .local v7, "weightFromAccessory":Ljava/lang/Boolean;
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getUserDeviceId()Ljava/lang/String;

    move-result-object v6

    .line 151
    .local v6, "userDeviceId":Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x5

    if-lt v8, v9, :cond_0

    .line 152
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->isfromaccesory:Z

    .line 153
    new-instance v4, Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 154
    .local v4, "userDeviceDao":Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;
    const-string/jumbo v3, "update_time DESC "

    .line 155
    .local v3, "sortOrder":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v8, "_id"

    const-string v9, "="

    invoke-direct {v1, v8, v6, v9}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 157
    .local v1, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    const/4 v8, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->getSQLCode()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v4, v8, v9, v10, v3}, Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;->getFirstItemFromQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;

    .line 158
    .local v5, "userDeviceData":Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;
    if-eqz v5, :cond_0

    .line 159
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;->getCustomName()Ljava/lang/String;

    move-result-object v0

    .line 160
    .local v0, "deviceName":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v8, ""

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 161
    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 162
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090a8c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    .end local v0    # "deviceName":Ljava/lang/String;
    .end local v1    # "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    .end local v3    # "sortOrder":Ljava/lang/String;
    .end local v4    # "userDeviceDao":Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;
    .end local v5    # "userDeviceData":Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;
    :cond_0
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_1

    .line 167
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mAccessoryType:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    :cond_1
    return-void

    .line 143
    .end local v6    # "userDeviceId":Ljava/lang/String;
    .end local v7    # "weightFromAccessory":Ljava/lang/Boolean;
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mlogDetailsMemoLayout:Landroid/widget/FrameLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private showDeletePopup()V
    .locals 3

    .prologue
    .line 244
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09078e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 254
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 202
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f09005b

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f020299

    const v6, 0x7f090040

    iget-object v7, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 206
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 207
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 259
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 260
    packed-switch p2, :pswitch_data_0

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 263
    :pswitch_0
    const-string v1, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 264
    const-string v1, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    iput-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .line 266
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mlogDetailsMemoLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 268
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 269
    .local v0, "s":Landroid/text/SpannableString;
    new-instance v1, Landroid/text/style/LeadingMarginSpan$Standard;

    const/16 v2, 0x67

    invoke-direct {v1, v2, v3}, Landroid/text/style/LeadingMarginSpan$Standard;-><init>(II)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mLogDetailMemoText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    .end local v0    # "s":Landroid/text/SpannableString;
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightStartValue:F

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeight:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightStartValue:F

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightUnit:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 273
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mlogDetailsMemoLayout:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x2711
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    const v0, 0x7f0302b6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->setContentView(I)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INPUT_ACTIVITY_DATA_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mCurrentData:Lcom/sec/android/app/shealth/common/utils/hcp/data/HealthCareData;

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "WEIGHT_ID_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->initView(Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100030

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 182
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 187
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 188
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 197
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 190
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.weight"

    const-string v2, "WT07"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->prepareShareView()V

    goto :goto_0

    .line 194
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->showDeletePopup()V

    goto :goto_0

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x7f080c8d
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 172
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mTimeDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/log/WeightLogDetailActivity;->mWeightData:Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/sec/android/app/shealth/heartrate/utils/DateFormatUtil;->getDateTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    return-void
.end method
