.class public interface abstract Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;
.super Ljava/lang/Object;
.source "TaskRunner.java"


# virtual methods
.method public abstract addNotificationListener(Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;)Z
.end method

.method public abstract addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
            ">(",
            "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
            "<TE;>;)V"
        }
    .end annotation
.end method

.method public abstract removeNotificationListener(Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;)Z
.end method

.method public abstract removeTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
            ">(",
            "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
            "<TE;>;)Z"
        }
    .end annotation
.end method

.method public abstract shutDown()V
.end method
