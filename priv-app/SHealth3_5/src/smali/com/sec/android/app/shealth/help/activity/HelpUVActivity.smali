.class public Lcom/sec/android/app/shealth/help/activity/HelpUVActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "HelpUVActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 25
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUVAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpUVActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f091115

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 30
    :goto_0
    return-void

    .line 28
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/help/activity/HelpUVActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f091055

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 13
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getUVAvailablilty(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14
    const v0, 0x7f030206

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpUVActivity;->setContentView(I)V

    .line 19
    :goto_0
    return-void

    .line 16
    :cond_0
    const v0, 0x7f030205

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/help/activity/HelpUVActivity;->setContentView(I)V

    goto :goto_0
.end method
