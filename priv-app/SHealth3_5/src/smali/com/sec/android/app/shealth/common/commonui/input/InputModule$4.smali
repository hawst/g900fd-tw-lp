.class Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->registerListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 336
    if-eqz p2, :cond_1

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mGradationView:Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$700(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/controller/GradationView;->stopInertia()V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->setCursorVisible(Z)V

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->hideKeyboard()V

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getFloatValue()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMinInputRange:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$900(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    iget-object v0, v0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mValueEditText:Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->getFloatValue()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->mMaxInputRange:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$800(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 346
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onValueOutOfRangeListener:Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$1000(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$4;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # getter for: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->onValueOutOfRangeListener:Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$1000(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$OnValueOutOfRangeListener;->onOutOfRanged()V

    goto :goto_0
.end method
