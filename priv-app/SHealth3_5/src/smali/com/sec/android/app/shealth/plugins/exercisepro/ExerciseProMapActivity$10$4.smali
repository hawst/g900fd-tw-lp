.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;
.super Ljava/lang/Object;
.source "ExerciseProMapActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->onNewLocation(Landroid/location/Location;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

.field final synthetic val$location:Landroid/location/Location;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 658
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;->val$location:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 662
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v0

    if-nez v0, :cond_0

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;-><init>()V

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 665
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;->val$location:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->latitude:D

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;

    iget-object v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->mLastLocation:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProMapActivity$10$4;->val$location:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;->longitude:D

    .line 667
    return-void
.end method
