.class Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;
.super Ljava/lang/Object;
.source "CircleProgressImage.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    .line 111
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_0

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mQueue:Ljava/util/concurrent/BlockingQueue;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$100(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/util/concurrent/BlockingQueue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->mExecutor:Ljava/util/concurrent/ExecutorService;
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->access$200(Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 116
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage$3;->this$0:Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/walkingmate/widget/CircleProgressImage;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 117
    const/4 v0, 0x1

    goto :goto_0
.end method
