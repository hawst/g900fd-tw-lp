.class public Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;
.super Landroid/view/View;
.source "ProfileFirstView.java"


# static fields
.field private static isBirthdateClicked:Z


# instance fields
.field private gender:I

.field private isFirstClick:Z

.field private isImage:Z

.field private mContext:Landroid/content/Context;

.field private mDatePicker:Landroid/widget/DatePicker;

.field private mDatePickerChildEditDay:Landroid/widget/EditText;

.field private mDatePickerChildEditMonth:Landroid/widget/EditText;

.field private mDatePickerChildEditYear:Landroid/widget/EditText;

.field private mEditNameWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

.field private mFemaleIcon:Landroid/widget/ImageView;

.field private mFemaleLayout:Landroid/widget/LinearLayout;

.field private mFemaleText:Landroid/widget/TextView;

.field private mMaleIcon:Landroid/widget/ImageView;

.field private mMaleLayout:Landroid/widget/LinearLayout;

.field private mMaleText:Landroid/widget/TextView;

.field private mNameEditText:Landroid/widget/EditText;

.field protected mProfileImage:Landroid/widget/ImageView;

.field private mProfileImageLayout:Landroid/widget/RelativeLayout;

.field private mProfileImageMan:Landroid/widget/ImageButton;

.field private mProfilePressView:Landroid/view/View;

.field private mRankingContainer:Landroid/widget/RelativeLayout;

.field private mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

.field private mUseRankingCheck:Landroid/widget/CheckBox;

.field private mView:Landroid/view/View;

.field oldText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isBirthdateClicked:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/content/ShealthProfile;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "shealthProfile"    # Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 102
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mContext:Landroid/content/Context;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    .line 78
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isImage:Z

    .line 79
    iput v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I

    .line 80
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->oldText:Ljava/lang/String;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfilePressView:Landroid/view/View;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageMan:Landroid/widget/ImageButton;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleIcon:Landroid/widget/ImageView;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleText:Landroid/widget/TextView;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mUseRankingCheck:Landroid/widget/CheckBox;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mRankingContainer:Landroid/widget/RelativeLayout;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 98
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isFirstClick:Z

    .line 172
    new-instance v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$4;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mEditNameWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

    .line 103
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mContext:Landroid/content/Context;

    .line 105
    const v0, 0x7f0301e5

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->initLayout()V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mEditNameWatcher:Lcom/sec/android/app/shealth/common/utils/MemoTextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$1;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$2;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$3;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 161
    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->deleteProfileBitmap()Ljava/lang/Boolean;

    .line 168
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->setDataProfileValue()V

    .line 169
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I

    return v0
.end method

.method static synthetic access$1002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 73
    sput-boolean p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isBirthdateClicked:Z

    return p0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mUseRankingCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private checkValidName(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 392
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 394
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character$UnicodeBlock;->of(C)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    sget-object v2, Ljava/lang/Character$UnicodeBlock;->HIGH_SURROGATES:Ljava/lang/Character$UnicodeBlock;

    if-ne v1, v2, :cond_0

    .line 396
    const/4 v1, 0x0

    .line 399
    :goto_1
    return v1

    .line 392
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 399
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private initLayout()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const v11, 0x7f09020b

    const/4 v10, 0x0

    .line 201
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080332

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageLayout:Landroid/widget/RelativeLayout;

    .line 202
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080846

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageMan:Landroid/widget/ImageButton;

    .line 203
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageMan:Landroid/widget/ImageButton;

    invoke-virtual {v5, v10}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 204
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080333

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    .line 205
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080847

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfilePressView:Landroid/view/View;

    .line 206
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080335

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    .line 207
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080849

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleIcon:Landroid/widget/ImageView;

    .line 208
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f08084c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleIcon:Landroid/widget/ImageView;

    .line 209
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f08084a

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleText:Landroid/widget/TextView;

    .line 210
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f08084d

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleText:Landroid/widget/TextView;

    .line 211
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080850

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mUseRankingCheck:Landroid/widget/CheckBox;

    .line 212
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f08084f

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mRankingContainer:Landroid/widget/RelativeLayout;

    .line 213
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080848

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleLayout:Landroid/widget/LinearLayout;

    .line 214
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f08084b

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleLayout:Landroid/widget/LinearLayout;

    .line 217
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080845

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090815

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901fd

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f08084e

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090816

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901fd

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09081f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0901f0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09020a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0901f2

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09081e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0901f0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09020a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0901f2

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mRankingContainer:Landroid/widget/RelativeLayout;

    new-instance v6, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$5;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)V

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    const v6, 0x7f080325

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/DatePicker;

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    .line 232
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->getInstance()Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;->SAMSUNG_ACCOUNT_NOTIFICATION:Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager;->isReminderNotificationEnable(Landroid/content/Context;Lcom/sec/android/app/shealth/settings/samsungAccount/ReminderNotificationManager$ReminderType;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 233
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 234
    const/4 v4, -0x1

    .line 235
    .local v4, "year":I
    const/4 v3, -0x1

    .line 236
    .local v3, "month":I
    const/4 v1, -0x1

    .line 238
    .local v1, "day":I
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->getSamsungAccountBirthday()Ljava/lang/String;

    move-result-object v0

    .line 239
    .local v0, "birthday":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 241
    const/4 v5, 0x0

    const/4 v6, 0x4

    :try_start_0
    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 242
    const/4 v5, 0x4

    const/4 v6, 0x6

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 243
    const/4 v5, 0x6

    const/16 v6, 0x8

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 250
    :cond_0
    :goto_0
    if-lez v4, :cond_2

    .line 251
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    add-int/lit8 v6, v3, -0x1

    invoke-virtual {v5, v4, v6, v1}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 261
    .end local v0    # "birthday":Ljava/lang/String;
    .end local v1    # "day":I
    .end local v3    # "month":I
    .end local v4    # "year":I
    :cond_1
    :goto_1
    invoke-direct {p0, v10}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->setDatepickerSelectOnFocus(Z)V

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->setDefaultImage()V

    .line 264
    return-void

    .line 244
    .restart local v0    # "birthday":Ljava/lang/String;
    .restart local v1    # "day":I
    .restart local v3    # "month":I
    .restart local v4    # "year":I
    :catch_0
    move-exception v2

    .line 245
    .local v2, "e":Ljava/lang/Exception;
    const/4 v4, -0x1

    .line 246
    sget-object v5, Lcom/sec/android/app/shealth/settings/userprofile/InitSetProfileCard;->TAG:Ljava/lang/String;

    const-string v6, "Exception in parsing Samsung Account Birthdate"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 253
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    const/16 v6, 0x7bc

    invoke-virtual {v5, v6, v10, v12}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 254
    iput-boolean v12, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isFirstClick:Z

    goto :goto_1

    .line 258
    .end local v0    # "birthday":Ljava/lang/String;
    .end local v1    # "day":I
    .end local v3    # "month":I
    .end local v4    # "year":I
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    const/16 v6, 0x7bc

    invoke-virtual {v5, v6, v10, v12}, Landroid/widget/DatePicker;->updateDate(III)V

    goto :goto_1
.end method

.method private setDataProfileValue()V
    .locals 9

    .prologue
    const v8, 0x7f07011b

    const v7, 0x2e636

    const v5, 0x2e635

    const/4 v6, 0x1

    .line 279
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 280
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 282
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 283
    iput v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205ee

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mMaleText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 287
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v3

    if-ne v3, v7, :cond_2

    .line 288
    iput v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I

    .line 289
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleIcon:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0205dd

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mFemaleText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 292
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getProfileDisclose()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 293
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mUseRankingCheck:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 295
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 297
    .local v0, "birthDate":J
    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-eqz v3, :cond_4

    .line 298
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 299
    .local v2, "cal":Ljava/util/Calendar;
    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 300
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/4 v6, 0x5

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 302
    .end local v2    # "cal":Ljava/util/Calendar;
    :cond_4
    return-void
.end method

.method private setDatepickerSelectOnFocus(Z)V
    .locals 15
    .param p1, "flag"    # Z

    .prologue
    .line 304
    iget-object v11, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/DatePicker;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    .line 305
    .local v10, "v":Landroid/view/ViewGroup;
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 306
    .local v0, "a":Landroid/view/ViewGroup;
    const/4 v11, 0x0

    const/16 v12, 0xa

    const/4 v13, 0x0

    const/16 v14, 0xa

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 307
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 308
    .local v1, "al":Landroid/view/ViewGroup$LayoutParams;
    const/4 v11, -0x1

    iput v11, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 309
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 311
    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 312
    .local v7, "numberPicker0":Landroid/view/ViewGroup;
    if-eqz v7, :cond_0

    .line 313
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 314
    .local v4, "nl0":Landroid/view/ViewGroup$LayoutParams;
    const/4 v11, -0x1

    iput v11, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 315
    invoke-virtual {v7, v4}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 318
    .end local v4    # "nl0":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 319
    .local v8, "numberPicker1":Landroid/view/ViewGroup;
    if-eqz v8, :cond_1

    .line 320
    invoke-virtual {v8}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 321
    .local v5, "nl1":Landroid/view/ViewGroup$LayoutParams;
    const/4 v11, -0x1

    iput v11, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 322
    invoke-virtual {v8, v5}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    .end local v5    # "nl1":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    .line 326
    .local v9, "numberPicker2":Landroid/view/ViewGroup;
    if-eqz v9, :cond_2

    .line 327
    invoke-virtual {v9}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 328
    .local v6, "nl2":Landroid/view/ViewGroup$LayoutParams;
    const/4 v11, -0x1

    iput v11, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 329
    invoke-virtual {v9, v6}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 332
    .end local v6    # "nl2":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    new-instance v2, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView$6;-><init>(Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;)V

    .line 341
    .local v2, "datePickerTouchListener":Landroid/view/View$OnTouchListener;
    if-eqz v7, :cond_5

    if-eqz v8, :cond_5

    if-eqz v9, :cond_5

    .line 342
    const/4 v11, 0x1

    invoke-virtual {v7, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditYear:Landroid/widget/EditText;

    .line 343
    const/4 v11, 0x1

    invoke-virtual {v8, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditMonth:Landroid/widget/EditText;

    .line 344
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditDay:Landroid/widget/EditText;

    .line 346
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v11, 0x3

    if-ge v3, v11, :cond_5

    .line 347
    invoke-virtual {v7, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-virtual {v8, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-virtual {v9, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    if-nez v11, :cond_4

    .line 346
    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 350
    :cond_4
    invoke-virtual {v7, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 351
    invoke-virtual {v8, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 352
    invoke-virtual {v9, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_1

    .line 356
    .end local v3    # "i":I
    :cond_5
    return-void
.end method

.method private showToast(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "isLightTheme"    # Z

    .prologue
    .line 526
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    .line 527
    .local v0, "t":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 528
    return-void
.end method


# virtual methods
.method public SaveBitmapToFileCache(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 531
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 532
    .local v1, "fileCacheItem":Ljava/io/File;
    const/4 v2, 0x0

    .line 534
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 535
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 536
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {p1, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 542
    if-eqz v3, :cond_0

    .line 543
    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 549
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    :cond_1
    :goto_0
    return-void

    .line 545
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 546
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 548
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_0

    .line 537
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 538
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 542
    if-eqz v2, :cond_1

    .line 543
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 545
    :catch_2
    move-exception v0

    .line 546
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 540
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 542
    :goto_2
    if-eqz v2, :cond_2

    .line 543
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 547
    :cond_2
    :goto_3
    throw v4

    .line 545
    :catch_3
    move-exception v0

    .line 546
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 540
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 537
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method public getImageDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mView:Landroid/view/View;

    return-object v0
.end method

.method public hideKeyboard()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 360
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mContext:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 361
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditYear:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditMonth:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditDay:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 363
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditYear:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditMonth:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePickerChildEditDay:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->clearFocus()V

    .line 368
    :cond_0
    return-void
.end method

.method public isDefaultBirthDateSet()Z
    .locals 1

    .prologue
    .line 552
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isFirstClick:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isBirthdateClicked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isImage()Z
    .locals 1

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isImage:Z

    return v0
.end method

.method public removeEditNameDafaultFocus()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 276
    :cond_0
    return-void
.end method

.method public resetFlag()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 556
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isFirstClick:Z

    .line 557
    sput-boolean v0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isBirthdateClicked:Z

    .line 558
    return-void
.end method

.method public savePhoto(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 380
    if-eqz p1, :cond_1

    .line 381
    sget-object v0, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveImageToInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    .line 382
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isNoMediaFile()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 383
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->onCreateNoMediaFile()V

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    if-nez p1, :cond_0

    .line 386
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->deleteProfileBitmap()Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public saveProfile()Z
    .locals 9

    .prologue
    const v8, 0x7f090837

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 404
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->clearFocus()V

    .line 406
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-nez v5, :cond_0

    .line 407
    new-instance v5, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 409
    :cond_0
    const/4 v2, 0x0

    .line 411
    .local v2, "name":Landroid/text/Editable;
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 412
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 414
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_3

    .line 415
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 417
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090838

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->showToast(Ljava/lang/String;Z)V

    .line 423
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->hideKeyboard()V

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->clearFocus()V

    .line 461
    :goto_1
    return v3

    .line 421
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->showToast(Ljava/lang/String;Z)V

    goto :goto_0

    .line 427
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->checkValidName(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090ae2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->showToast(Ljava/lang/String;Z)V

    .line 430
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->hideKeyboard()V

    .line 431
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->clearFocus()V

    goto :goto_1

    .line 437
    :cond_4
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_2
    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setName(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 444
    iget v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I

    const v6, 0x2e635

    if-eq v5, v6, :cond_6

    iget v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I

    const v6, 0x2e636

    if-eq v5, v6, :cond_6

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090839

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->showToast(Ljava/lang/String;Z)V

    .line 446
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->hideKeyboard()V

    goto :goto_1

    .line 437
    :cond_5
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f09081c

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto :goto_2

    .line 438
    :catch_0
    move-exception v1

    .line 439
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->showToast(Ljava/lang/String;Z)V

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->hideKeyboard()V

    goto/16 :goto_1

    .line 449
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->gender:I

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setGender(I)V

    .line 452
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 453
    .local v0, "cal":Ljava/util/Calendar;
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v5}, Landroid/widget/DatePicker;->getYear()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v6}, Landroid/widget/DatePicker;->getMonth()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mDatePicker:Landroid/widget/DatePicker;

    invoke-virtual {v7}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v7

    invoke-virtual {v0, v5, v6, v7}, Ljava/util/Calendar;->set(III)V

    .line 455
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Utils;->formatDateToProfileDob(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setBirthDate(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 460
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mShealthProfile:Lcom/samsung/android/sdk/health/content/ShealthProfile;

    iget-object v5, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mUseRankingCheck:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->setProfileDisclose(Z)V

    move v3, v4

    .line 461
    goto/16 :goto_1

    .line 456
    :catch_1
    move-exception v1

    .line 457
    .restart local v1    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090836

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->showToast(Ljava/lang/String;Z)V

    goto/16 :goto_1
.end method

.method public setDefaultImage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07011e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageMan:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageMan:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfilePressView:Landroid/view/View;

    const v1, 0x7f020280

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 485
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isImage:Z

    .line 486
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageMan:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0205f0

    invoke-static {p1, v1, v2}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getCroppedProfileIcon(Landroid/graphics/Bitmap;Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfilePressView:Landroid/view/View;

    const v1, 0x7f020284

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImage:Landroid/widget/ImageView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0901fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09020b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090200

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->isImage:Z

    .line 472
    return-void
.end method

.method public setImageClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 371
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/ProfileFirstView;->mProfileImageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 372
    return-void
.end method
