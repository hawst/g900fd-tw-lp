.class Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;
.super Ljava/lang/Object;
.source "SearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText$OnSearchKeyClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->initSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V
    .locals 0

    .prologue
    .line 654
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchClick()V
    .locals 2

    .prologue
    .line 660
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->performSearchFood()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1700(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    .line 669
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1800(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->FOOD_SEARCH_USING_SEARCH_BUTTON_ON_SIP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 670
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->cancelAutoCompleteSearch()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1900(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)V

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->mAutoCompletePopup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 674
    :cond_0
    return-void

    .line 666
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment$13;->this$0:Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/foodpick/search/SearchFragment;->searchEditText:Lcom/sec/android/app/shealth/food/foodpick/components/SearchEditText;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->showSoftInput(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0
.end method
