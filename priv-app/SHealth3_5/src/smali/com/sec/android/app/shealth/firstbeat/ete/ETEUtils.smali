.class public Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;
.super Ljava/lang/Object;
.source "ETEUtils.java"


# static fields
.field private static TARGET_TE_FREE:I

.field public static USE_CASE_1:I

.field public static USE_CASE_2a:I

.field public static USE_CASE_2b:I

.field private static m2aTragerAchieved:Z

.field private static mContext:Landroid/content/Context;

.field private static mETEc:Lfi/firstbeat/ete/ETEc;

.field private static mETEenergyExpenditureCumulative:I

.field private static mETEtimeToNextLevel:I

.field private static mETEtimeToTarget:I

.field private static mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

.field private static mLastETEAchievedPercent:I

.field private static mLastETEtrainingEffect:I

.field private static mLogger:Lcom/sec/android/app/shealth/firstbeat/ete/ETEDataLogger;

.field private static mMaxHR:I

.field private static mMaxLoadPeak:I

.field private static mMaxMET:I

.field static mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

.field private static mRealtimeTEUtils:Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;

.field private static mResult:Lfi/firstbeat/ete/ETEresults;

.field private static mRriRemainder:I

.field private static mTE50:Z

.field private static mTargetTE:I

.field private static mTargetTime:I

.field private static mUseCase:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x28

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->TARGET_TE_FREE:I

    .line 30
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    .line 31
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    .line 32
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    .line 36
    new-instance v0, Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-direct {v0}, Lfi/firstbeat/coach/eteintegration/CoachVars;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    .line 37
    new-instance v0, Lfi/firstbeat/ete/ETEresults;

    invoke-direct {v0}, Lfi/firstbeat/ete/ETEresults;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    .line 38
    new-instance v0, Lfi/firstbeat/ete/ETEc;

    invoke-direct {v0}, Lfi/firstbeat/ete/ETEc;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEc:Lfi/firstbeat/ete/ETEc;

    .line 41
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mContext:Landroid/content/Context;

    .line 55
    new-instance v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEUtils:Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method

.method public static getAudioGuideArg()I
    .locals 2

    .prologue
    .line 342
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-eq v0, v1, :cond_0

    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_1

    .line 343
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getTimeToTarget()I

    move-result v0

    .line 345
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getTimeToNextLevel()I

    move-result v0

    goto :goto_0
.end method

.method public static getAudioGuideId()I
    .locals 1

    .prologue
    .line 329
    sget-boolean v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTE50:Z

    if-eqz v0, :cond_0

    .line 330
    const/4 v0, 0x0

    .line 331
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->getAudioId()I

    move-result v0

    goto :goto_0
.end method

.method public static getAudioGuideString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    sget-boolean v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTE50:Z

    if-eqz v0, :cond_0

    .line 336
    const/4 v0, 0x0

    .line 337
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->getAudioString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getETEResult()I
    .locals 8

    .prologue
    const-wide/high16 v6, 0x40f0000000000000L    # 65536.0

    .line 242
    const-string v0, "Goal"

    const-string v1, " ==== anayze ===="

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEcorrectedHr["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEcorrectedHr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEminimalHr["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEminimalHr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEmaximalHr["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEmaximalHr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEepoc["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEepoc:I

    int-to-double v2, v2

    div-double/2addr v2, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEtrainingLoadPeak/65536.0["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEtrainingLoadPeak:I

    int-to-double v2, v2

    div-double/2addr v2, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEtrainingEffect/10.0["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEtrainingEffect:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEpercentAchieved["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEpercentAchieved:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEtimeToTarget["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEtimeToTarget:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEtimeToNextLevel["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEtimeToNextLevel:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEintensityControl["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEintensityControl:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEenergyExpenditure/65536.0["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEenergyExpenditure:I

    int-to-double v2, v2

    div-double/2addr v2, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEenergyExpenditureCumulative/65536.0["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEenergyExpenditureCumulative:I

    int-to-double v2, v2

    div-double/2addr v2, v6

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEmaximalMET["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMET:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEmaximalMETminutes["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMETminutes:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEresourceRecovery["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEresourceRecovery:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const-string v0, "Goal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " result.ETEphraseNumber["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v2, v2, Lfi/firstbeat/ete/ETEresults;->ETEphraseNumber:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v0, v0, Lfi/firstbeat/ete/ETEresults;->ETEtimeToTarget:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEtimeToTarget:I

    .line 269
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v0, v0, Lfi/firstbeat/ete/ETEresults;->ETEtimeToNextLevel:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEtimeToNextLevel:I

    .line 270
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v0, v0, Lfi/firstbeat/ete/ETEresults;->ETEtrainingEffect:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mLastETEtrainingEffect:I

    .line 271
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxHR:I

    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v1, v1, Lfi/firstbeat/ete/ETEresults;->ETEmaximalHr:I

    if-ge v0, v1, :cond_0

    .line 272
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v0, v0, Lfi/firstbeat/ete/ETEresults;->ETEmaximalHr:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxHR:I

    .line 274
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v0, v0, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMET:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxMET:I

    .line 275
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxLoadPeak:I

    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v1, v1, Lfi/firstbeat/ete/ETEresults;->ETEtrainingLoadPeak:I

    if-ge v0, v1, :cond_1

    .line 276
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v0, v0, Lfi/firstbeat/ete/ETEresults;->ETEtrainingLoadPeak:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxLoadPeak:I

    .line 278
    :cond_1
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v0, v0, Lfi/firstbeat/ete/ETEresults;->ETEenergyExpenditureCumulative:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEenergyExpenditureCumulative:I

    .line 279
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v0, v0, Lfi/firstbeat/ete/ETEresults;->ETEpercentAchieved:I

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mLastETEAchievedPercent:I

    .line 281
    const/4 v0, 0x0

    return v0
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEUtils:Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEUtils:Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;

    monitor-enter v1

    .line 69
    :try_start_0
    sput-object p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mContext:Landroid/content/Context;

    .line 70
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEUtils:Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;

    return-object v0

    .line 70
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getLastETEAchievedPercent()I
    .locals 1

    .prologue
    .line 289
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mLastETEAchievedPercent:I

    return v0
.end method

.method public static getLastETEtrainingEffect()I
    .locals 1

    .prologue
    .line 285
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mLastETEtrainingEffect:I

    return v0
.end method

.method public static getPaceGuideId()I
    .locals 1

    .prologue
    .line 355
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->getPaceId()I

    move-result v0

    return v0
.end method

.method private static getResourceRecovery()I
    .locals 14

    .prologue
    const/4 v9, 0x0

    .line 420
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 422
    .local v7, "millis":J
    const/4 v6, 0x0

    .line 424
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "end_time"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "resource_recovery"

    aput-object v1, v2, v0

    .line 425
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "end_time < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 426
    .local v3, "selectionClause":Ljava/lang/String;
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 427
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 428
    invoke-interface {v6}, Landroid/database/Cursor;->moveToLast()Z

    .line 429
    const-string v0, "end_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 430
    .local v10, "time":J
    const-string/jumbo v0, "resource_recovery"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 431
    .local v9, "resourceRecovery":I
    int-to-long v0, v9

    sub-long v4, v7, v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v4, v12

    const-wide/16 v12, 0x3c

    div-long/2addr v4, v12

    sub-long/2addr v0, v4

    long-to-int v9, v0

    .line 432
    if-gez v9, :cond_0

    .line 433
    const/4 v9, 0x0

    .line 435
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    if-eqz v6, :cond_1

    .line 440
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 443
    .end local v9    # "resourceRecovery":I
    .end local v10    # "time":J
    :cond_1
    :goto_0
    return v9

    .line 439
    :cond_2
    if-eqz v6, :cond_1

    .line 440
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 439
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selectionClause":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 440
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getSecToTarget()I
    .locals 2

    .prologue
    .line 370
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    if-ne v0, v1, :cond_0

    .line 371
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEtimeToTarget:I

    if-lez v0, :cond_0

    .line 372
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEtimeToTarget:I

    mul-int/lit8 v0, v0, 0x3c

    .line 375
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static getTimeToNextLevel()I
    .locals 1

    .prologue
    .line 363
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEtimeToNextLevel:I

    if-lez v0, :cond_0

    .line 364
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEtimeToNextLevel:I

    .line 366
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getTimeToTarget()I
    .locals 1

    .prologue
    .line 359
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEtimeToTarget:I

    if-lez v0, :cond_0

    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEtimeToTarget:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getTrainingLoadPeakTotal()I
    .locals 19

    .prologue
    .line 379
    const-wide/32 v8, 0x5265c00

    .line 380
    .local v8, "ONE_DAY":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 381
    .local v12, "millis":J
    const/4 v10, 0x0

    .line 385
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    const-wide v6, 0x8b0bb400L

    sub-long v14, v2, v6

    .line 386
    .local v14, "startTime":J
    const-string v2, "Goal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "startTime ["

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "]"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " PeriodUtils.getStartOfDay()["

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v12, v13}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "]"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "end_time"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "training_load_peak"

    aput-object v3, v4, v2

    .line 392
    .local v4, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "end_time > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "end_time"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 393
    .local v5, "selectionClause":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 394
    const-string v2, "Goal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadPeakList size["

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "]"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    const/4 v11, 0x0

    .line 396
    .local v11, "loadPeakTotal":I
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_2

    .line 397
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 398
    const-string v2, "end_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 399
    .local v16, "time":J
    const-string/jumbo v2, "training_load_peak"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 400
    .local v18, "trainingLoadPeak":I
    const-string v2, "Goal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadPeak time["

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "]"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " value["

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "]"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    cmp-long v2, v16, v14

    if-gez v2, :cond_1

    .line 403
    const-string v2, "Goal"

    const-string v3, "delete old data"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 412
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v5    # "selectionClause":Ljava/lang/String;
    .end local v11    # "loadPeakTotal":I
    .end local v14    # "startTime":J
    .end local v16    # "time":J
    .end local v18    # "trainingLoadPeak":I
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_0

    .line 413
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v2

    .line 407
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v5    # "selectionClause":Ljava/lang/String;
    .restart local v11    # "loadPeakTotal":I
    .restart local v14    # "startTime":J
    .restart local v16    # "time":J
    .restart local v18    # "trainingLoadPeak":I
    :cond_1
    add-int v11, v11, v18

    goto :goto_1

    .line 412
    .end local v16    # "time":J
    .end local v18    # "trainingLoadPeak":I
    :cond_2
    if-eqz v10, :cond_3

    .line 413
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 415
    :cond_3
    const-string v2, "Goal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "loadPeakTotal ["

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "]"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    const/high16 v2, 0x10000

    div-int v2, v11, v2

    return v2
.end method

.method public static initEngine(II)I
    .locals 11
    .param p0, "targetTE"    # I
    .param p1, "targetTime"    # I

    .prologue
    const/16 v10, 0xfa

    const/16 v9, 0x64

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 104
    new-instance v3, Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-direct {v3}, Lfi/firstbeat/coach/eteintegration/CoachVars;-><init>()V

    sput-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    .line 105
    new-instance v3, Lfi/firstbeat/ete/ETEresults;

    invoke-direct {v3}, Lfi/firstbeat/ete/ETEresults;-><init>()V

    sput-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    .line 106
    new-instance v3, Lfi/firstbeat/ete/ETEc;

    invoke-direct {v3}, Lfi/firstbeat/ete/ETEc;-><init>()V

    sput-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEc:Lfi/firstbeat/ete/ETEc;

    .line 109
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    const/16 v5, 0x32

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    .line 110
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 111
    .local v1, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 112
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 113
    .local v0, "birthDate":Ljava/util/Date;
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-virtual {v0}, Ljava/util/Date;->getDay()I

    move-result v5

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v6

    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v7

    add-int/lit16 v7, v7, 0x76c

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/firstbeat/utils/Utils;->getAgeValue(III)I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    .line 114
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getHeight()F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    .line 115
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getWeight()F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    .line 116
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "db age "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " height "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " weight "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    sget-object v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    const/16 v6, 0x8

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    .line 118
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    sget-object v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    const/16 v6, 0x6e

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    .line 119
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    sget-object v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    invoke-static {v5, v9}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    .line 120
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    sget-object v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    invoke-static {v5, v10}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    .line 121
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    sget-object v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    const/16 v6, 0x23

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    .line 122
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    sget-object v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v5, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    invoke-static {v5, v10}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    .line 123
    sget-object v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getGender()I

    move-result v3

    const v6, 0x2e636

    if-ne v3, v6, :cond_4

    move v3, v4

    :goto_0
    iput v3, v5, Lfi/firstbeat/coach/eteintegration/CoachVars;->gender:I

    .line 124
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iput v8, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->minHr:I

    .line 125
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRealTimeMaxHRAutoUpdate()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 126
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->getRealTimeMaxHR()I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    .line 131
    :goto_1
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v3, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    if-lt v3, v9, :cond_0

    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v3, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    const/16 v5, 0xf0

    if-le v3, v5, :cond_1

    .line 132
    :cond_0
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iput v8, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    .line 133
    :cond_1
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->getRealTimeMaxMET()I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    .line 134
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v3, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    const/high16 v5, 0x60000

    if-lt v3, v5, :cond_2

    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v3, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    const v5, 0x16db6e

    if-le v3, v5, :cond_3

    .line 135
    :cond_2
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iput v8, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    .line 136
    :cond_3
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getResourceRecovery()I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->resourceRecovery:I

    .line 137
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->getTrainingLoadPeakTotal()I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->monthlyLoad:I

    .line 143
    const-string v3, "Goal"

    const-string v5, " ==== initEngine ===="

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "targetTE["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "targetTime["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.AC["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->AC:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.age["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->age:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.height["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->height:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.weight["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->weight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.gender["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->gender:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.minHr["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->minHr:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.maxHr["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.maxMET["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.resourceRecovery["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->resourceRecovery:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const-string v3, "Goal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mFBTvars.monthlyLoad["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    iget v6, v6, Lfi/firstbeat/coach/eteintegration/CoachVars;->monthlyLoad:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEc:Lfi/firstbeat/ete/ETEc;

    sget-object v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-virtual {v3, v5, v8}, Lfi/firstbeat/ete/ETEc;->SetParameters(Lfi/firstbeat/ete/FBTvars;I)I

    move-result v2

    .line 164
    .local v2, "rc":I
    if-ne v2, v4, :cond_6

    .line 165
    const-string v3, "Goal"

    const-string v4, "Parameters OK"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :goto_2
    sput p0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTargetTE:I

    .line 174
    sput p1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTargetTime:I

    .line 175
    const/16 v3, 0xa

    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mLastETEtrainingEffect:I

    .line 176
    sput v8, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxLoadPeak:I

    sput v8, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxMET:I

    sput v8, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxHR:I

    sput v8, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mLastETEAchievedPercent:I

    .line 177
    sput v8, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRriRemainder:I

    .line 178
    sput v8, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEenergyExpenditureCumulative:I

    .line 179
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEc:Lfi/firstbeat/ete/ETEc;

    invoke-virtual {v3, p0, p1}, Lfi/firstbeat/ete/ETEc;->SetTargets(II)I

    .line 186
    sput-boolean v8, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->m2aTragerAchieved:Z

    .line 187
    sput-boolean v8, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTE50:Z

    .line 190
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->getInstance()Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    .line 191
    sget v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTargetTE:I

    sget v4, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->TARGET_TE_FREE:I

    if-ne v3, v4, :cond_7

    sget v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTargetTime:I

    if-nez v3, :cond_7

    .line 192
    sget v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    .line 198
    :goto_3
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->getInstance()Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->setUseCase(I)V

    .line 201
    return v2

    .line 123
    .end local v2    # "rc":I
    :cond_4
    const/4 v3, 0x2

    goto/16 :goto_0

    .line 129
    :cond_5
    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealTimeManualyMaxHR()I

    move-result v5

    iput v5, v3, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    goto/16 :goto_1

    .line 167
    .restart local v2    # "rc":I
    :cond_6
    const-string v3, "Goal"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ete.SetParameters error["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 193
    :cond_7
    sget v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTargetTime:I

    if-eqz v3, :cond_8

    .line 194
    sget v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    goto :goto_3

    .line 196
    :cond_8
    sget v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    sput v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    goto :goto_3
.end method

.method public static inputBpmTrainingData(IFF)I
    .locals 8
    .param p0, "bpm"    # I
    .param p1, "speed"    # F
    .param p2, "altitude"    # F

    .prologue
    const/4 v4, 0x0

    .line 206
    const-string v5, "Goal"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " input bpm["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " speed["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " altitude["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    if-nez p0, :cond_1

    .line 212
    invoke-static {v4, p1, p2}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->inputRriTrainingData(IFF)I

    move-result v4

    .line 222
    :cond_0
    return v4

    .line 215
    :cond_1
    const v5, 0xea60

    div-int v1, v5, p0

    .line 216
    .local v1, "rri":I
    sget v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRriRemainder:I

    add-int/lit16 v3, v5, 0x1388

    .line 217
    .local v3, "totalDuration":I
    div-int v2, v3, v1

    .line 218
    .local v2, "rriReportCount":I
    rem-int v5, v3, v1

    sput v5, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRriRemainder:I

    .line 219
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 220
    invoke-static {v1, p1, p2}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->inputRriTrainingData(IFF)I

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static inputRriTrainingData(IFF)I
    .locals 3
    .param p0, "rri"    # I
    .param p1, "speed"    # F
    .param p2, "altitude"    # F

    .prologue
    const/high16 v0, 0x47800000    # 65536.0f

    .line 227
    mul-float/2addr p1, v0

    .line 228
    mul-float/2addr p2, v0

    .line 230
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEc:Lfi/firstbeat/ete/ETEc;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, p0, v1, v2}, Lfi/firstbeat/ete/ETEc;->Analyzer(III)I

    .line 231
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEc:Lfi/firstbeat/ete/ETEc;

    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    invoke-virtual {v0, v1}, Lfi/firstbeat/ete/ETEc;->GetResult(Lfi/firstbeat/ete/ETEresults;)I

    .line 238
    const/4 v0, 0x0

    return v0
.end method

.method public static logUserGuide(Ljava/lang/String;I)V
    .locals 0
    .param p0, "audioGuide"    # Ljava/lang/String;
    .param p1, "paceGuide"    # I

    .prologue
    .line 326
    return-void
.end method

.method public static makeTargetTEValue(II)I
    .locals 2
    .param p0, "goalType"    # I
    .param p1, "teLevel"    # I

    .prologue
    const/4 v1, 0x1

    .line 75
    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 77
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->TARGET_TE_FREE:I

    .line 83
    sget v0, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->TARGET_TE_FREE:I

    .line 93
    :goto_0
    return v0

    .line 86
    :cond_0
    if-ge p1, v1, :cond_1

    .line 87
    const/16 v0, 0x13

    goto :goto_0

    .line 88
    :cond_1
    if-ne p1, v1, :cond_2

    .line 89
    const/16 v0, 0x19

    goto :goto_0

    .line 90
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    .line 91
    const/16 v0, 0x23

    goto :goto_0

    .line 93
    :cond_3
    const/16 v0, 0x2d

    goto :goto_0
.end method

.method public static makeTargetTime(II)I
    .locals 1
    .param p0, "goalType"    # I
    .param p1, "value"    # I

    .prologue
    .line 97
    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 98
    const/4 p1, 0x0

    .line 100
    .end local p1    # "value":I
    :cond_0
    return p1
.end method

.method public static processResult()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 293
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mRealtimeTEResultProcess:Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->process(Lfi/firstbeat/ete/ETEresults;)I

    move-result v0

    .line 294
    .local v0, "rc":I
    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    if-ne v1, v2, :cond_1

    .line 295
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v1, v1, Lfi/firstbeat/ete/ETEresults;->ETEtrainingEffect:I

    const/16 v2, 0x32

    if-ne v1, v2, :cond_0

    .line 297
    sput-boolean v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTE50:Z

    .line 318
    :cond_0
    :goto_0
    return v0

    .line 300
    :cond_1
    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2a:I

    if-ne v1, v2, :cond_0

    sget-boolean v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->m2aTragerAchieved:Z

    if-nez v1, :cond_0

    .line 301
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v1, v1, Lfi/firstbeat/ete/ETEresults;->ETEintensityControl:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 303
    const-string v1, "Goal"

    const-string v2, "change USE_CASE_2b"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    sput-boolean v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->m2aTragerAchieved:Z

    .line 305
    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_2b:I

    sput v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    .line 306
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->getInstance()Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->setUseCase(I)V

    .line 307
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEc:Lfi/firstbeat/ete/ETEc;

    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mTargetTE:I

    invoke-virtual {v1, v2, v4}, Lfi/firstbeat/ete/ETEc;->SetTargets(II)I

    goto :goto_0

    .line 309
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v1, v1, Lfi/firstbeat/ete/ETEresults;->ETEintensityControl:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_0

    .line 311
    const-string v1, "Goal"

    const-string v2, "change USE_CASE_1"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    sput-boolean v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->m2aTragerAchieved:Z

    .line 313
    sget v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->USE_CASE_1:I

    sput v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    .line 314
    invoke-static {}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->getInstance()Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mUseCase:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/firstbeat/ete/ETEResultProcess;->setUseCase(I)V

    .line 315
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mETEc:Lfi/firstbeat/ete/ETEc;

    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->TARGET_TE_FREE:I

    invoke-virtual {v1, v2, v4}, Lfi/firstbeat/ete/ETEc;->SetTargets(II)I

    goto :goto_0
.end method

.method public static saveValuesToPersist(JD)V
    .locals 5
    .param p0, "mExerciseId"    # J
    .param p2, "distance"    # D

    .prologue
    .line 447
    const-string v2, "Goal"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "saveValuesToPersist mMaxHR["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxHR:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "mMaxMET["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxMET:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxHR:I

    if-lez v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isRealTimeMaxHRAutoUpdate()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 450
    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxHR:I

    invoke-static {v2}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->setRealTimeMaxHR(I)V

    .line 451
    :cond_0
    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxMET:I

    if-lez v2, :cond_1

    .line 452
    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxMET:I

    invoke-static {v2}, Lcom/sec/android/app/shealth/firstbeat/db/FirstbeatDBHelper;->setRealTimeMaxMET(I)V

    .line 454
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    sget v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxHR:I

    iput v3, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxHr:I

    .line 455
    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mFBTvars:Lfi/firstbeat/coach/eteintegration/CoachVars;

    sget v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxMET:I

    iput v3, v2, Lfi/firstbeat/coach/eteintegration/CoachVars;->maxMET:I

    .line 458
    sget v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mMaxLoadPeak:I

    if-lez v2, :cond_2

    .line 459
    const/4 v0, 0x0

    .line 464
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 465
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "exercise__id"

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 466
    const-string v2, "end_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 467
    const-string v2, "distance"

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 468
    const-string/jumbo v2, "training_load_peak"

    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v3, v3, Lfi/firstbeat/ete/ETEresults;->ETEtrainingLoadPeak:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 469
    const-string/jumbo v2, "maximal_met"

    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v3, v3, Lfi/firstbeat/ete/ETEresults;->ETEmaximalMET:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 470
    const-string/jumbo v2, "resource_recovery"

    sget-object v3, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mResult:Lfi/firstbeat/ete/ETEresults;

    iget v3, v3, Lfi/firstbeat/ete/ETEresults;->ETEresourceRecovery:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    sget-object v2, Lcom/sec/android/app/shealth/firstbeat/ete/ETEUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResult;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    if-eqz v0, :cond_2

    .line 482
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 490
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_2
    return-void

    .line 481
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    .line 482
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method
