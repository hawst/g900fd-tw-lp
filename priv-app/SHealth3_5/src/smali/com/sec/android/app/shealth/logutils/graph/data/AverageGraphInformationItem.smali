.class public Lcom/sec/android/app/shealth/logutils/graph/data/AverageGraphInformationItem;
.super Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;
.source "AverageGraphInformationItem.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "seriesId"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/logutils/graph/GraphInformationItem;-><init>(Landroid/content/Context;I)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/AverageGraphInformationItem;->hideTitle()V

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/logutils/graph/data/AverageGraphInformationItem;->showAverageLabel()V

    .line 36
    return-void
.end method


# virtual methods
.method public hideAverageLabel()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method
