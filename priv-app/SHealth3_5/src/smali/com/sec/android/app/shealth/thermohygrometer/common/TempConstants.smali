.class public final Lcom/sec/android/app/shealth/thermohygrometer/common/TempConstants;
.super Ljava/lang/Object;
.source "TempConstants.java"


# static fields
.field public static final APRIL:I = 0x3

.field public static final AUGUST:I = 0x7

.field public static final AVG_TEMP:Ljava/lang/String; = "AvgTemp"

.field public static final DATE_FORMAT_ONLY_KOREA:Ljava/lang/String; = "yyyy-MM-dd"

.field public static final DECEMBER:I = 0xb

.field public static final DEFAULT_OF_VALUE:I = 0x0

.field public static final FEBRUARY:I = 0x1

.field public static final FIRST_OF_MONTH:I = 0x1

.field public static final FRI:I = 0x6

.field public static final JANUARY:I = 0x0

.field public static final JULY:I = 0x6

.field public static final JUNE:I = 0x5

.field public static final LAST_HOUR_IN_DAY:I = 0x17

.field public static final LAST_MILLI_IN_SECOND:I = 0x3e7

.field public static final LAST_MINUTE_IN_HOUR:I = 0x3b

.field public static final LAST_SECOND_IN_MINUTE:I = 0x3b

.field public static final MARCH:I = 0x2

.field public static final MAY:I = 0x4

.field public static final MON:I = 0x2

.field public static final NOVEMBER:I = 0xa

.field public static final OCTOBER:I = 0x9

.field public static final SAT:I = 0x7

.field public static final SEPTEMBER:I = 0x8

.field public static final SUN:I = 0x1

.field public static final TGH_LOGGING_ADJUST_COMFORT_ZONE:Ljava/lang/String; = "TH01"

.field public static final TGH_LOGGING_APP_ID:Ljava/lang/String; = "com.sec.android.app.shealth.thermohygrometer"

.field public static final TGH_LOGGING_HELP:Ljava/lang/String; = "TH02"

.field public static final TGH_LOGGING_SHARE:Ljava/lang/String; = "TH03"

.field public static final THU:I = 0x5

.field public static final TUE:I = 0x3

.field public static final WED:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
