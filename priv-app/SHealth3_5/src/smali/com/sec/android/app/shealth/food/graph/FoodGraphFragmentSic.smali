.class public Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;
.super Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.source "FoodGraphFragmentSic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$3;,
        Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;
    }
.end annotation


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final HOURS_IN_DAY:B = 0x18t

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MINUTE:I = 0xea60

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MILLIS_IN_WEEK:I = 0x240c8400

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final MONTH_IN_YEAR:I = 0xc

.field private static final PERIOD_TYPE_BUTTON_TO_USER_LOG_ACTION_LINK:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Lcom/sec/android/app/shealth/food/app/UserActionLog;",
            ">;"
        }
    .end annotation
.end field

.field private static final ROBOTO_LIGHT_FONT_ASSET_PATH:Ljava/lang/String; = "font/Roboto-Light.ttf"

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static mGoalDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# instance fields
.field density:F

.field private isFromHomeDashBoard:Z

.field private mDataSetMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;",
            ">;"
        }
    .end annotation
.end field

.field private mDataSetThread:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;

.field private mFoodDao:Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

.field private mFoodSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

.field private mHandlerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mInfoView:Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 111
    new-instance v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->PERIOD_TYPE_BUTTON_TO_USER_LOG_ACTION_LINK:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->isFromHomeDashBoard:Z

    .line 541
    return-void
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mGoalDataMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200()V
    .locals 0

    .prologue
    .line 75
    invoke-static {}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->createGoalMap()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->prepareDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    return-void
.end method

.method private static declared-synchronized createGoalMap()V
    .locals 13

    .prologue
    .line 654
    const-class v10, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    monitor-enter v10

    :try_start_0
    sget-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mGoalDataMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 684
    .local v2, "projection":[Ljava/lang/String;
    .local v3, "selectionClause":Ljava/lang/String;
    .local v4, "mSelectionArgs":[Ljava/lang/String;
    .local v6, "cursor":Landroid/database/Cursor;
    .local v9, "mGoalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;>;"
    :cond_0
    :goto_0
    monitor-exit v10

    return-void

    .line 658
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selectionClause":Ljava/lang/String;
    .end local v4    # "mSelectionArgs":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v9    # "mGoalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;>;"
    :cond_1
    const/4 v0, 0x7

    :try_start_1
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "period"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "create_time"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string/jumbo v1, "value"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "goal_type"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string v1, "goal_subtype"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string/jumbo v1, "update_time"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string/jumbo v1, "time_zone"

    aput-object v1, v2, v0

    .line 660
    .restart local v2    # "projection":[Ljava/lang/String;
    const-string v3, "goal_type=? "

    .line 661
    .restart local v3    # "selectionClause":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const v1, 0x9c43

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 662
    .restart local v4    # "mSelectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 666
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 667
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 668
    .restart local v9    # "mGoalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;>;"
    if-eqz v6, :cond_3

    .line 669
    invoke-static {v6}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getGoalList(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v9

    .line 670
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    .line 672
    .local v8, "mGoalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    sget-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mGoalDataMap:Ljava/util/Map;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getUpdateTime()J

    move-result-wide v11

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 679
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "mGoalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .end local v9    # "mGoalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;>;"
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 680
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 654
    .end local v3    # "selectionClause":Ljava/lang/String;
    .end local v4    # "mSelectionArgs":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v0

    monitor-exit v10

    throw v0

    .line 679
    .restart local v3    # "selectionClause":Ljava/lang/String;
    .restart local v4    # "mSelectionArgs":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "mGoalList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;>;"
    :cond_3
    if-eqz v6, :cond_0

    .line 680
    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0
.end method

.method private declared-synchronized prepareDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 31
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 557
    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->isRemoving()Z

    move-result v25

    if-nez v25, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->isDetached()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v25

    if-eqz v25, :cond_1

    .line 650
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 562
    :cond_1
    :try_start_1
    const-string v25, "FoodGraphFragmentSIC"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "prepareDataSet start "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetMap:Ljava/util/Map;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_0

    .line 569
    new-instance v10, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    invoke-direct {v10}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;-><init>()V

    .line 570
    .local v10, "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    new-instance v16, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;

    new-instance v25, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;-><init>(Lcom/sec/android/app/shealth/food/fooddao/MealDao;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 572
    .local v16, "foodGraphDataHolder":Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->isMealCursorListContainNull()Z

    move-result v25

    if-eqz v25, :cond_2

    .line 573
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetMap:Ljava/util/Map;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 575
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->moveAllCursorsToFirst()V

    .line 576
    new-instance v15, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v15}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    .line 577
    .local v15, "foodBarSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    new-instance v7, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;

    invoke-direct {v7}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;-><init>()V

    .line 578
    .local v7, "cumulativeFoodLineSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    const/16 v23, 0x0

    .line 579
    .local v23, "sumCalories":F
    const-wide/16 v8, 0x0

    .line 580
    .local v8, "currentTime":J
    const-wide/16 v12, 0x0

    .line 584
    .local v12, "endOfViewTime":J
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->isFromHomeDashBoard:Z

    move/from16 v25, v0

    if-eqz v25, :cond_8

    .line 586
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 594
    :goto_1
    const-wide/16 v17, 0x0

    .line 595
    .local v17, "handlerTime":J
    const-wide/16 v5, 0x0

    .line 596
    .local v5, "avgTotalCalorie":D
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->areAllCursorsAfterLast()Z

    move-result v25

    if-nez v25, :cond_a

    .line 597
    new-instance v24, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    invoke-direct/range {v24 .. v24}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>()V

    .line 598
    .local v24, "timeData":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getMealTimeList()Ljava/util/List;

    move-result-object v20

    .line 599
    .local v20, "mealTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static/range {v20 .. v20}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Long;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v21

    .line 600
    .local v21, "minTime":J
    cmp-long v25, v12, v21

    if-gtz v25, :cond_3

    const-wide/16 v25, 0x0

    cmp-long v25, v17, v25

    if-nez v25, :cond_4

    .line 601
    :cond_3
    move-wide/from16 v17, v21

    .line 603
    :cond_4
    move-object/from16 v0, v24

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setTime(J)V

    .line 604
    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getFoodCaloriesList(Ljava/util/List;J)Ljava/util/List;

    move-result-object v19

    .line 605
    .local v19, "mealCaloriesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getFilteredValueCaloriesForPeriodList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v14

    .line 608
    .local v14, "filteredValueCaloriesForPeriodList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    sget-object v25, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_5

    .line 609
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getTotalCalorie()D

    move-result-wide v25

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfMonth(J)J

    move-result-wide v27

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfMonth(J)J

    move-result-wide v29

    invoke-static/range {v27 .. v30}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getDataPresentCountInPeriod(JJ)I

    move-result v27

    move/from16 v0, v27

    int-to-double v0, v0

    move-wide/from16 v27, v0

    div-double v5, v25, v27

    .line 611
    new-instance v25, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    move-object/from16 v0, v25

    move-wide/from16 v1, v21

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>(JD)V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 616
    :cond_5
    invoke-static {v14}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 617
    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;->setData(Ljava/util/List;)V

    .line 623
    sget-object v25, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    if-ne v0, v1, :cond_7

    .line 624
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->getListSum(Ljava/util/List;)Ljava/lang/Float;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 626
    .local v11, "daySum":F
    const-wide/16 v25, 0x0

    cmp-long v25, v8, v25

    if-eqz v25, :cond_6

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v25

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v27

    cmp-long v25, v25, v27

    if-eqz v25, :cond_9

    .line 627
    :cond_6
    move-wide/from16 v8, v21

    .line 628
    move/from16 v23, v11

    .line 632
    :goto_3
    new-instance v25, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v26, v0

    move-object/from16 v0, v25

    move-wide/from16 v1, v21

    move-wide/from16 v3, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;-><init>(JD)V

    move-object/from16 v0, v25

    invoke-virtual {v7, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V

    .line 634
    .end local v11    # "daySum":F
    :cond_7
    move-object/from16 v0, v24

    invoke-virtual {v15, v0}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;->add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 557
    .end local v5    # "avgTotalCalorie":D
    .end local v7    # "cumulativeFoodLineSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    .end local v8    # "currentTime":J
    .end local v10    # "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .end local v12    # "endOfViewTime":J
    .end local v14    # "filteredValueCaloriesForPeriodList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .end local v15    # "foodBarSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    .end local v16    # "foodGraphDataHolder":Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;
    .end local v17    # "handlerTime":J
    .end local v19    # "mealCaloriesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .end local v20    # "mealTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v21    # "minTime":J
    .end local v23    # "sumCalories":F
    .end local v24    # "timeData":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    :catchall_0
    move-exception v25

    monitor-exit p0

    throw v25

    .line 590
    .restart local v7    # "cumulativeFoodLineSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    .restart local v8    # "currentTime":J
    .restart local v10    # "dataSet":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .restart local v12    # "endOfViewTime":J
    .restart local v15    # "foodBarSeries":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartXyTimeSeries;
    .restart local v16    # "foodGraphDataHolder":Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;
    .restart local v23    # "sumCalories":F
    :cond_8
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getArguments()Landroid/os/Bundle;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v25

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v12

    goto/16 :goto_1

    .line 630
    .restart local v5    # "avgTotalCalorie":D
    .restart local v11    # "daySum":F
    .restart local v14    # "filteredValueCaloriesForPeriodList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .restart local v17    # "handlerTime":J
    .restart local v19    # "mealCaloriesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .restart local v20    # "mealTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v21    # "minTime":J
    .restart local v24    # "timeData":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    :cond_9
    add-float v23, v23, v11

    goto :goto_3

    .line 636
    .end local v11    # "daySum":F
    .end local v14    # "filteredValueCaloriesForPeriodList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .end local v19    # "mealCaloriesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    .end local v20    # "mealTimeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v21    # "minTime":J
    .end local v24    # "timeData":Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedSchartTimeData;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mHandlerMap:Ljava/util/Map;

    move-object/from16 v25, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 637
    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/app/shealth/food/graph/FoodGraphDataHolder;->closeAllCursors()V

    .line 638
    invoke-virtual {v10, v15}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 639
    sget-object v25, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_b

    .line 641
    invoke-virtual {v10, v7}, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 644
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetMap:Ljava/util/Map;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 646
    const-string v25, "FoodGraphFragmentSIC"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "prepareDataSet end "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method protected createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .locals 5
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 465
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mHandlerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getHandlerUpdateDataManager()Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->updateHandlerTime(JJLcom/sec/android/app/shealth/logutils/graph/GraphDataUtils$IHandlerUpdateDataManager;)V

    .line 471
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    return-object v0
.end method

.method protected customizeChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 6
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 440
    const/high16 v1, 0x41800000    # 16.0f

    iget v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {p1, v4, v4, v4, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 441
    invoke-virtual {p1, v5, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMaxRounding(II)V

    .line 442
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMaxRoundDigit(II)V

    .line 443
    const v1, 0x3f8ccccd    # 1.1f

    invoke-virtual {p1, v1, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMaxRoundMagnification(FI)V

    .line 445
    invoke-virtual {p1, v3, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisVisible(ZI)V

    .line 446
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    .line 447
    .local v0, "sepTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAlpha(I)V

    .line 448
    const/high16 v1, 0x41400000    # 12.0f

    iget v2, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 449
    return-void
.end method

.method protected getContentUriList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 481
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/Uri;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;

    return-object v0
.end method

.method protected getLegendButtonBackgroundResourceId()I
    .locals 1

    .prologue
    .line 511
    const v0, 0x7f02078c

    return v0
.end method

.method protected getLegendMarks()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 497
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    const/4 v1, 0x0

    const v2, 0x7f07001c

    const v3, 0x7f090920

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->createColoredLegendMark(II)Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f07001d

    const v3, 0x7f090921

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->createColoredLegendMark(II)Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f07001e

    const v3, 0x7f090922

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->createColoredLegendMark(II)Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f07001f

    const v3, 0x7f090923

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->createColoredLegendMark(II)Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getNoDataIcon()I
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->deleteShareViaButton()V

    .line 184
    :cond_0
    const v0, 0x7f0205b7

    return v0
.end method

.method protected getYAxisLabelTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f0900b9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 162
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initDateBar()V

    .line 163
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 164
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090213

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 166
    return-void
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 3
    .param p1, "periodStart"    # J
    .param p3, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v2, 0x1

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mFoodSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    invoke-virtual {p3}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->putFoodGraphLastView(I)V

    .line 253
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataWasChanged:Z

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 256
    new-instance v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;-><init>(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetThread:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetThread:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;->start()V

    .line 259
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->PERIOD_TYPE_BUTTON_TO_USER_LOG_ACTION_LINK:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 260
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v0, v1, :cond_1

    .line 263
    const/4 v0, 0x2

    invoke-super {p0, v2, v0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->setXYAxisMarkCount(II)V

    .line 269
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 267
    :cond_1
    invoke-super {p0, v2, v2}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->setXYAxisMarkCount(II)V

    goto :goto_0
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 2
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 486
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->recyclePieChart()V

    .line 490
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;-><init>(Landroid/content/Context;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;->setDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mInfoView:Lcom/sec/android/app/shealth/food/graph/FoodGraphInformationAreaView;

    return-object v0
.end method

.method protected initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 16
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    .prologue
    .line 274
    new-instance v1, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;

    invoke-direct {v1}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;-><init>()V

    .line 275
    .local v1, "barSeriesStyle":Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;
    new-instance v3, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 276
    .local v3, "cumulativeSeriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    const/4 v11, 0x1

    invoke-virtual {v3, v11}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setEnableDisconnectByTime(Z)V

    .line 279
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    const v12, 0x9c43

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/shealth/common/utils/hcp/GoalUtils;->getLatestGoalData(Landroid/content/Context;Ljava/lang/Integer;)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v4

    .line 280
    .local v4, "goalData":Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v12, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v11, v12, :cond_6

    .line 281
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineVisible(Z)V

    .line 282
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f070211

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineColor(I)V

    .line 283
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a084e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextOffsetY(F)V

    .line 284
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a084f

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextOffsetX(F)V

    .line 285
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a0850

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineThickness(F)V

    .line 286
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const v12, 0x7f09006b

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextPrefix(Ljava/lang/String;)V

    .line 287
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a084d

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    const-string/jumbo v12, "sec-roboto-light"

    const/4 v13, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f070211

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    const/4 v15, 0x0

    invoke-static {v11, v12, v13, v14, v15}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextPrefixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a084c

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    const-string/jumbo v12, "sec-roboto-light"

    const/4 v13, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f070211

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    const/4 v15, 0x0

    invoke-static {v11, v12, v13, v14, v15}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 295
    if-eqz v4, :cond_5

    .line 296
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;->getValue()F

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineValue(F)V

    .line 311
    :goto_0
    const v11, 0x7f090028

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 312
    const v11, 0x7f0203ba

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 313
    .local v6, "handlerOverBitmap":Landroid/graphics/Bitmap;
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setValueMarkingVisible(Z)V

    .line 314
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f07001b

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setValueMarkingNormalColor(I)V

    .line 316
    invoke-virtual {v1, v6}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 317
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f07001f

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 318
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f07001e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 319
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f07001d

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 320
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f07001c

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->addItemColor(I)V

    .line 321
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v12, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v11, v12, :cond_0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v12, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v11, v12, :cond_1

    .line 322
    :cond_0
    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 325
    :cond_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v12, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v11, v12, :cond_2

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v12, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v11, v12, :cond_3

    .line 328
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 331
    :cond_3
    sget-object v11, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v11, v12, :cond_4

    .line 334
    new-instance v5, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$2;-><init>(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;)V

    .line 418
    .local v5, "goalListener":Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 420
    .local v8, "mEventicons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f020605

    invoke-static {v11, v12}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 421
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 422
    .local v7, "height":I
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconBitmapList(Ljava/util/ArrayList;)V

    .line 425
    const/4 v11, 0x1

    const/high16 v12, 0x40400000    # 3.0f

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v13

    invoke-static {v11, v12, v13}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v10

    .line 427
    .local v10, "px":F
    const/high16 v11, -0x40800000    # -1.0f

    int-to-float v12, v7

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    add-float/2addr v12, v10

    mul-float v9, v11, v12

    .line 429
    .local v9, "margin":F
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconVisible(Z)V

    .line 430
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconScalable(Z)V

    .line 431
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconOffsetX(F)V

    .line 432
    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconOffsetY(F)V

    .line 433
    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setEventIconListener(Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;)V

    .line 436
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "goalListener":Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;
    .end local v7    # "height":I
    .end local v8    # "mEventicons":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .end local v9    # "margin":F
    .end local v10    # "px":F
    :cond_4
    return-void

    .line 298
    .end local v6    # "handlerOverBitmap":Landroid/graphics/Bitmap;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getDefaultIntakeCalorieGoal()I

    move-result v11

    int-to-float v11, v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineValue(F)V

    goto/16 :goto_0

    .line 302
    :cond_6
    const v11, 0x44a28000    # 1300.0f

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineValue(F)V

    .line 303
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineVisible(Z)V

    .line 304
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0700fa

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineColor(I)V

    .line 305
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a084c

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    const-string/jumbo v12, "sec-roboto-light"

    const/4 v13, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0700fa

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    const/4 v15, 0x0

    invoke-static {v11, v12, v13, v14, v15}, Lcom/sec/android/app/shealth/logutils/graph/GraphFragmentUtils;->getTextStyle(FLjava/lang/String;ZII)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    goto/16 :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 130
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onAttach(Landroid/app/Activity;)V

    .line 131
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLoggerHolder;->getUserActionLogger()Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "show_graph_fragment"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "show_graph_fragment"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->isFromHomeDashBoard:Z

    .line 134
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mFoodDao:Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    .line 135
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->registerContentObserver(Landroid/net/Uri;)V

    .line 136
    return-void

    :cond_0
    move v0, v1

    .line 132
    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 143
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onCreateView"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    new-instance v1, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mFoodSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->density:F

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mFoodSharedPrefsHelper:Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/FoodSharedPrefsHelper;->getFoodGraphLastView()I

    move-result v0

    .line 147
    .local v0, "mLastViewOrdinal":I
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 149
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->values()[Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->setPeroidType(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 151
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetMap:Ljava/util/Map;

    .line 152
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mGoalDataMap:Ljava/util/Map;

    .line 153
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mHandlerMap:Ljava/util/Map;

    .line 154
    new-instance v1, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;-><init>(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$1;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetThread:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetThread:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;->start()V

    .line 156
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 535
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mDataSetThread:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;

    .line 537
    invoke-static {}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->resetSelectedTime()V

    .line 538
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDestroy()V

    .line 539
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 516
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->unregisterContentObserver()V

    .line 517
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onDetach()V

    .line 518
    return-void
.end method

.method public onHiddenChanged(Z)V
    .locals 0
    .param p1, "hidden"    # Z

    .prologue
    .line 523
    if-eqz p1, :cond_0

    .line 526
    invoke-static {}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->resetSelectedTime()V

    .line 528
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onHiddenChanged(Z)V

    .line 529
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 170
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onResume()V

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ROTATE_TO_THE_LANDSCAPE_MODE_IN_CHART_VIEW:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 174
    :cond_0
    return-void
.end method

.method protected setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
    .locals 11
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .prologue
    const-wide/16 v1, 0x0

    const/4 v5, 0x0

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerActivity;->addShareViaButton()V

    .line 195
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;->getSelectedDateInChart()J

    move-result-wide v9

    .line 196
    .local v9, "selectedTime":J
    :goto_0
    const-wide/16 v7, 0x0

    .line 199
    .local v7, "dataToShow":J
    sget-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eqz v0, :cond_1

    cmp-long v0, v9, v1

    if-lez v0, :cond_1

    .line 201
    sget-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$3;->$SwitchMap$com$sec$android$app$shealth$framework$ui$graph$PeriodH:[I

    sget-object v3, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 210
    const-string v0, "%Y-%m"

    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 215
    :cond_1
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 217
    .local v6, "calendar":Ljava/util/Calendar;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0, v3}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getSummaryViewTime(Landroid/os/Bundle;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v3

    cmp-long v0, v7, v3

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-eq v0, v3, :cond_2

    .line 219
    invoke-virtual {v6, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 220
    const/16 v0, 0xb

    invoke-virtual {v6, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 221
    const/16 v0, 0xc

    invoke-virtual {v6, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 222
    const/16 v0, 0xd

    invoke-virtual {v6, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 223
    const/16 v0, 0xe

    invoke-virtual {v6, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 224
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sput-object v0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPreviousType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 229
    const/4 v0, 0x4

    invoke-virtual {p1, v5, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setGraphType(II)V

    .line 233
    cmp-long v0, v7, v1

    if-lez v0, :cond_3

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v0

    iget v1, v0, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->timeDepthLevel:I

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v7, v8, v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphDataUtils;->getGraphStartTime(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)J

    move-result-wide v2

    long-to-double v2, v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getHorizontalMarksInterval()I

    move-result v4

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getHorizontalMarksCount()I

    move-result v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setStartVisual(IDII)V

    .line 238
    long-to-double v0, v7

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;->setHandlerStartDate(D)V

    .line 241
    :cond_3
    return-void

    .end local v6    # "calendar":Ljava/util/Calendar;
    .end local v7    # "dataToShow":J
    .end local v9    # "selectedTime":J
    :cond_4
    move-wide v9, v1

    .line 195
    goto/16 :goto_0

    .line 204
    .restart local v7    # "dataToShow":J
    .restart local v9    # "selectedTime":J
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v3, :cond_5

    const-string v0, "%Y-%m-%d"

    :goto_2
    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 205
    goto :goto_1

    .line 204
    :cond_5
    const-string v0, "%Y-%m"

    goto :goto_2

    .line 207
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v3, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v3, :cond_6

    const-string v0, "%Y-%m-%d"

    :goto_3
    invoke-static {v0, v9, v10}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->getLatestDataTimestamp(Ljava/lang/String;J)J

    move-result-wide v7

    .line 208
    goto/16 :goto_1

    .line 207
    :cond_6
    const-string v0, "%Y-%m"

    goto :goto_3

    .line 201
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 6
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 453
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->mFoodDao:Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;

    const-string/jumbo v3, "type"

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v4, 0x186a1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x1

    const v4, 0x186a2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x2

    const v4, 0x186a3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x3

    const v4, 0x186a4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;->MAX_SAMPLE_TIME:Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;Ljava/lang/String;Ljava/util/List;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/DaoContract;)V

    .line 459
    return-void
.end method
