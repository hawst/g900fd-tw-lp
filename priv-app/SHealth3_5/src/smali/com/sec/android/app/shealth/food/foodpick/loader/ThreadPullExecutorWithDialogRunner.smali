.class public Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
.super Ljava/lang/Object;
.source "ThreadPullExecutorWithDialogRunner.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/loader/TaskRunner;


# static fields
.field private static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MAX_THREADS_FOR_CALORIES_UPDATE:I = 0x5


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mDialogShowsCount:I

.field private mLoadingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

.field private mTaskNotificationListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;",
            ">;"
        }
    .end annotation
.end field

.field private mTaskWithDialogs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/loader/Task;",
            ">;"
        }
    .end annotation
.end field

.field private mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;

.field private mUiHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x5

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque;

    invoke-direct {v6}, Lcom/sec/android/app/shealth/food/foodpick/loader/LifoBlockingDeque;-><init>()V

    move v2, v1

    invoke-direct/range {v0 .. v6}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mActivity:Landroid/app/Activity;

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mUiHandler:Landroid/os/Handler;

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mDialogShowsCount:I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskNotificationListeners:Ljava/util/List;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskWithDialogs:Ljava/util/List;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyTaskAdded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    .param p2, "x2"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->getRunnable(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskWithDialogs:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Ljava/util/List;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->removeTasks(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->hideDialog(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Ljava/lang/Runnable;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->hideDialogIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyWithErrorFinished(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    .param p2, "x2"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyFinishedIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyTaskCanceled(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyTaskFinished(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->showDialog(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    return-void
.end method

.method private getRunnable(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)Ljava/lang/Runnable;
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    .param p2, "result"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$3;-><init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V

    return-object v0
.end method

.method private hideDialog(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mDialogShowsCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mDialogShowsCount:I

    .line 216
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mDialogShowsCount:I

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mLoadingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->hide()V

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskWithDialogs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 220
    return-void
.end method

.method private hideDialogIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 203
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->accompanyWithLoadingDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$8;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$8;-><init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 212
    :cond_0
    return-void
.end method

.method private notifyAllFinished()V
    .locals 3

    .prologue
    .line 255
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskNotificationListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;

    .line 256
    .local v1, "taskListener":Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;
    invoke-interface {v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;->onAllTasksFinished()V

    goto :goto_0

    .line 258
    .end local v1    # "taskListener":Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;
    :cond_0
    return-void
.end method

.method private notifyFinishedIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
            ">(",
            "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
            "<TE;>;",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "task":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    .local p2, "result":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    invoke-virtual {p2}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;->getResultContents()Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->onTaskCompleted(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;)V

    .line 225
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyTaskFinished(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->getQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->notifyAllFinished()V

    .line 230
    :cond_0
    return-void
.end method

.method private notifyTaskAdded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 3
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 237
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskNotificationListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;

    .line 238
    .local v1, "taskListener":Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;
    invoke-interface {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;->onTaskAdded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    goto :goto_0

    .line 240
    .end local v1    # "taskListener":Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;
    :cond_0
    return-void
.end method

.method private notifyTaskCanceled(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 3
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 243
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskNotificationListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;

    .line 244
    .local v1, "taskListener":Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;
    invoke-interface {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;->onTaskCanceled(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    goto :goto_0

    .line 246
    .end local v1    # "taskListener":Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;
    :cond_0
    return-void
.end method

.method private notifyTaskFinished(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 3
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 249
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskNotificationListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;

    .line 250
    .local v1, "taskListener":Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;
    invoke-interface {v1, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;->onTaskFinished(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    goto :goto_0

    .line 252
    .end local v1    # "taskListener":Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;
    :cond_0
    return-void
.end method

.method private notifyWithErrorFinished(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$5;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$5;-><init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 157
    :cond_0
    return-void
.end method

.method private removeTasks(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/loader/Task;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 124
    .local p1, "tasks":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/food/foodpick/loader/Task;>;"
    const/4 v1, 0x1

    .line 125
    .local v1, "result":Z
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .line 126
    .local v2, "task":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->removeTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Z

    move-result v3

    and-int/2addr v1, v3

    .line 127
    goto :goto_0

    .line 128
    .end local v2    # "task":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;
    :cond_0
    return v1
.end method

.method private runOnUiThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 234
    return-void
.end method

.method private showDialog(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 2
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mDialogShowsCount:I

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$7;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$7;-><init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mLoadingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mLoadingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->show()V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskWithDialogs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mDialogShowsCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mDialogShowsCount:I

    .line 200
    return-void
.end method

.method private showDialogIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 1
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 160
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->accompanyWithLoadingDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$6;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$6;-><init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 171
    :cond_0
    return-void
.end method


# virtual methods
.method public addNotificationListener(Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;)Z
    .locals 1
    .param p1, "taskNotificationListener"    # Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskNotificationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;",
            ">(",
            "Lcom/sec/android/app/shealth/food/foodpick/loader/Task",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "task":Lcom/sec/android/app/shealth/food/foodpick/loader/Task;, "Lcom/sec/android/app/shealth/food/foodpick/loader/Task<TE;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->showDialogIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 63
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$1;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$1;-><init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 71
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$2;-><init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 79
    .local v1, "taskRunnable":Ljava/lang/Runnable;
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 80
    .local v0, "taskFuture":Ljava/util/concurrent/Future;
    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->setFuture(Ljava/util/concurrent/Future;)V

    .line 81
    return-void
.end method

.method public removeNotificationListener(Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;)Z
    .locals 1
    .param p1, "taskNotificationListener"    # Lcom/sec/android/app/shealth/food/foodpick/loader/TaskNotificationListener;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mTaskNotificationListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeTask(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)Z
    .locals 2
    .param p1, "task"    # Lcom/sec/android/app/shealth/food/foodpick/loader/Task;

    .prologue
    .line 107
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->getFuture()Ljava/util/concurrent/Future;

    move-result-object v1

    if-nez v1, :cond_0

    .line 108
    const/4 v1, 0x0

    .line 120
    :goto_0
    return v1

    .line 110
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->hideDialogIfNeeded(Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    .line 111
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/Task;->clearFuture()Ljava/util/concurrent/Future;

    move-result-object v0

    .line 112
    .local v0, "future":Ljava/util/concurrent/Future;
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner$4;-><init>(Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;Lcom/sec/android/app/shealth/food/foodpick/loader/Task;)V

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 120
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move-result v1

    goto :goto_0
.end method

.method public shutDown()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/loader/ThreadPullExecutorWithDialogRunner;->mThreadPool:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    .line 134
    return-void
.end method
