.class public Lcom/sec/android/app/shealth/award/AwardActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "AwardActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissControllerProvider;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;
.implements Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/award/AwardActivity$DismissDialogController;
    }
.end annotation


# static fields
.field private static final CROP_FROM_ALBUM:I = 0x3

.field private static final CROP_FROM_CAMERA:I = 0x2

.field private static final DELETE_PHOTO_ACTION:I = 0x2

.field private static final PICK_FROM_ALBUM:I = 0x1

.field private static final PICK_FROM_CAMERA:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final TAKE_ALBUM_ACTION:I = 0x0

.field private static final TAKE_PHOTO_ACTION:I = 0x1

.field private static final TEMP_IMAGE_PREFIX:Ljava/lang/String; = "temp_"

.field private static final TEMP_IMAGE_TIME_FORMAT:Ljava/lang/String; = "HH_mm_ss_SSS"


# instance fields
.field private IMAGE_CAPTURE_DATA:Ljava/lang/String;

.field private IMAGE_CAPTURE_URI:Ljava/lang/String;

.field awardSummuryFragment:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

.field private camera_flag:Z

.field private imageClickListener:Landroid/view/View$OnClickListener;

.field private isImage:Z

.field private listItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBStateLossOnsaveInstance:Z

.field private final mDismissControlMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;",
            ">;"
        }
    .end annotation
.end field

.field private mImageCaptureUri:Landroid/net/Uri;

.field private mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

.field private original_file:Ljava/io/File;

.field private photo:Landroid/graphics/Bitmap;

.field private popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/sec/android/app/shealth/award/AwardActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/award/AwardActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 81
    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 83
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mBStateLossOnsaveInstance:Z

    .line 84
    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 86
    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    .line 87
    const-string v0, "imageCaptureUri"

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    .line 88
    const-string/jumbo v0, "photo_data"

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->IMAGE_CAPTURE_DATA:Ljava/lang/String;

    .line 90
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->isImage:Z

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->camera_flag:Z

    .line 105
    new-instance v0, Lcom/sec/android/app/shealth/award/AwardActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/award/AwardActivity$1;-><init>(Lcom/sec/android/app/shealth/award/AwardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mDismissControlMap:Ljava/util/Map;

    .line 153
    new-instance v0, Lcom/sec/android/app/shealth/award/AwardActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/award/AwardActivity$2;-><init>(Lcom/sec/android/app/shealth/award/AwardActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->imageClickListener:Landroid/view/View$OnClickListener;

    .line 620
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/award/AwardActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/AwardActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/award/AwardActivity;Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/AwardActivity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/award/AwardActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/AwardActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/award/AwardActivity;->createSetPicturePopup(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/award/AwardActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/AwardActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mDrawerMenuList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/award/AwardActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/award/AwardActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mDrawerMenuList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/app/shealth/award/AwardActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 4
    .param p0, "srcFile"    # Ljava/io/File;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    .line 545
    const/4 v2, 0x0

    .line 548
    .local v2, "result":Z
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 551
    .local v1, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/award/AwardActivity;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 555
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 562
    .end local v1    # "in":Ljava/io/InputStream;
    :goto_0
    return v2

    .line 555
    .restart local v1    # "in":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 558
    .end local v1    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 560
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "destFile"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x0

    .line 569
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 572
    .local v3, "out":Ljava/io/OutputStream;
    const/16 v5, 0x1000

    :try_start_1
    new-array v0, v5, [B

    .line 574
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    if-ltz v1, :cond_0

    .line 576
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 581
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    throw v5

    .line 585
    .end local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v2

    .line 587
    :goto_1
    return v4

    .line 581
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v3    # "out":Ljava/io/OutputStream;
    :cond_0
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 583
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private createSaveCropFile()Landroid/net/Uri;
    .locals 9

    .prologue
    .line 595
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 596
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 597
    .local v3, "time":J
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v7, "HH_mm_ss_SSS"

    invoke-direct {v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 598
    .local v2, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "temp_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/sql/Date;

    invoke-direct {v8, v3, v4}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 599
    .local v6, "url":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "Camera"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 600
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 601
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 602
    .local v5, "uri":Landroid/net/Uri;
    return-object v5
.end method

.method private createSetPicturePopup(Z)V
    .locals 4
    .param p1, "picture"    # Z

    .prologue
    .line 136
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    const v1, 0x7f0907a7

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;-><init>(II)V

    .line 138
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->listItem:Ljava/util/ArrayList;

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->listItem:Ljava/util/ArrayList;

    const v2, 0x7f0907a9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/award/AwardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->listItem:Ljava/util/ArrayList;

    const v2, 0x7f0907a8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/award/AwardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    if-eqz p1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->listItem:Ljava/util/ArrayList;

    const v2, 0x7f09003a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/award/AwardActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->listItem:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->setItems(Ljava/util/List;)Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;

    .line 146
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    .line 147
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mBStateLossOnsaveInstance:Z

    if-nez v1, :cond_1

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->popup:Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "SetPicture"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/ListChooseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 151
    :cond_1
    return-void
.end method

.method private deleteTempFile()V
    .locals 2

    .prologue
    .line 607
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 609
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 611
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 616
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private onSaveImageView()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 471
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    .line 472
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 473
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 474
    .local v1, "full_path":Ljava/lang/String;
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    .line 475
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 477
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->awardSummuryFragment:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->setImage(Landroid/graphics/Bitmap;)V

    .line 480
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v2, :cond_2

    .line 482
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 484
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 486
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 489
    .end local v0    # "f":Ljava/io/File;
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->camera_flag:Z

    .line 490
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    .line 491
    return-void
.end method

.method private saveImage()V
    .locals 4

    .prologue
    .line 498
    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;-><init>(Landroid/content/Context;)V

    .line 499
    .local v1, "scm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    const/16 v2, 0x190

    new-instance v3, Lcom/sec/android/app/shealth/award/AwardActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/award/AwardActivity$4;-><init>(Lcom/sec/android/app/shealth/award/AwardActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->startBackup(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 528
    .end local v1    # "scm":Lcom/samsung/android/sdk/health/content/ShealthContentManager;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->camera_flag:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 529
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/sec/android/app/shealth/common/utils/constants/FilePathsConstants;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveImageToInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    .line 530
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->isNoMediaFile()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 531
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->onCreateNoMediaFile()V

    .line 533
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 541
    :cond_1
    :goto_1
    return-void

    .line 523
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 534
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 535
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->deleteProfileBitmap()Ljava/lang/Boolean;

    goto :goto_1
.end method

.method private setActionBarTitle()V
    .locals 3

    .prologue
    .line 181
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 182
    .local v0, "shealthProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getName()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 183
    return-void

    .line 182
    :cond_0
    const-string v1, "Kate Lee"

    goto :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 3

    .prologue
    .line 173
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 174
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->setActionBarTitle()V

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0207a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 177
    return-void
.end method

.method public doDeletePhotoAction()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 323
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->deleteTempFile()V

    .line 324
    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->photo:Landroid/graphics/Bitmap;

    .line 325
    iput-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->awardSummuryFragment:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->setDefaultImage()V

    .line 329
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->clearProfileImageCache()V

    .line 330
    return-void
.end method

.method public doTakeAlbumAction()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 347
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 348
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "set-as-image"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 349
    const-string/jumbo v1, "vnd.android.cursor.dir/image"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    invoke-virtual {p0, v0, v2}, Lcom/sec/android/app/shealth/award/AwardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 351
    return-void
.end method

.method public doTakePhotoAction()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 335
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->deleteTempFile()V

    .line 336
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 338
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 339
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 340
    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 341
    const-string/jumbo v1, "return-data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 342
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shealth/award/AwardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 343
    return-void
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 268
    const-string v0, "SET_PHOTO"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    new-instance v0, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/award/mypagephoto/AwardSetPhotoContentInitializationListener;-><init>()V

    .line 271
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;

    move-result-object v0

    goto :goto_0
.end method

.method public getDismissController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mDismissControlMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDismissController;

    return-object v0
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 289
    const-string v0, "SetPicture"

    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    new-instance v0, Lcom/sec/android/app/shealth/award/AwardActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/award/AwardActivity$3;-><init>(Lcom/sec/android/app/shealth/award/AwardActivity;)V

    .line 317
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 11
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 355
    sget-object v8, Lcom/sec/android/app/shealth/award/AwardActivity;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "onActivityResult : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v8, 0x3

    if-ne p1, v8, :cond_0

    if-nez p2, :cond_0

    .line 359
    :cond_0
    const/4 v8, -0x1

    if-eq p2, v8, :cond_2

    .line 467
    :cond_1
    :goto_0
    return-void

    .line 364
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 398
    :pswitch_0
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_5

    .line 400
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 401
    .local v5, "mediaScanIntent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v5, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 402
    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/award/AwardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 404
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.android.camera.action.CROP"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 405
    .local v3, "intent":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    const-string v9, "image/*"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 407
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 408
    const-string/jumbo v8, "output"

    iget-object v9, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 409
    const-string v8, "aspectX"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 410
    const-string v8, "aspectY"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 411
    const/4 v8, 0x2

    invoke-virtual {p0, v3, v8}, Lcom/sec/android/app/shealth/award/AwardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 412
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto :goto_0

    .line 369
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "mediaScanIntent":Landroid/content/Intent;
    :pswitch_1
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    if-nez v8, :cond_4

    .line 371
    :cond_3
    sget-object v8, Lcom/sec/android/app/shealth/award/AwardActivity;->TAG:Ljava/lang/String;

    const-string v9, "OnactivityResult: Data or data.getdata() is NULL"

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 375
    :cond_4
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getImageFile(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->original_file:Ljava/io/File;

    .line 376
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->original_file:Ljava/io/File;

    if-eqz v8, :cond_1

    .line 381
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->deleteTempFile()V

    .line 382
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 383
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 384
    .local v0, "copy_file":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->original_file:Ljava/io/File;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 386
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.android.camera.action.CROP"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 387
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    const-string v9, "image/*"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 388
    const-string/jumbo v8, "output"

    iget-object v9, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 389
    const-string v8, "aspectX"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 390
    const-string v8, "aspectY"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 391
    const-string/jumbo v8, "set-as-image"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 392
    const/4 v8, 0x3

    invoke-virtual {p0, v3, v8}, Lcom/sec/android/app/shealth/award/AwardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 393
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;->putPinWallPaper(Z)V

    goto/16 :goto_0

    .line 423
    .end local v0    # "copy_file":Ljava/io/File;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_5
    :pswitch_2
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-nez v8, :cond_6

    .line 425
    if-eqz p3, :cond_6

    .line 427
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getImageFile(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/io/File;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->original_file:Ljava/io/File;

    .line 428
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 429
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 430
    .restart local v0    # "copy_file":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->original_file:Ljava/io/File;

    invoke-static {v8, v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    .line 433
    .end local v0    # "copy_file":Ljava/io/File;
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_a

    .line 435
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 436
    .local v2, "full_path":Ljava/lang/String;
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 437
    .local v6, "tempPhotoBitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    .line 438
    .local v4, "isSaved":Z
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->createSaveCropFile()Landroid/net/Uri;

    move-result-object v7

    .line 439
    .local v7, "tempUri":Landroid/net/Uri;
    if-eqz v6, :cond_7

    if-eqz v7, :cond_7

    .line 441
    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveImageToInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    move-result v4

    .line 444
    :cond_7
    if-eqz v4, :cond_9

    .line 447
    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v8, :cond_8

    .line 449
    new-instance v1, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 451
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 453
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 456
    .end local v1    # "f":Ljava/io/File;
    :cond_8
    iput-object v7, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 458
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->onSaveImageView()V

    .line 459
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->saveImage()V

    .line 463
    .end local v2    # "full_path":Ljava/lang/String;
    .end local v4    # "isSaved":Z
    .end local v6    # "tempPhotoBitmap":Landroid/graphics/Bitmap;
    .end local v7    # "tempUri":Landroid/net/Uri;
    :cond_a
    invoke-static {}, Lcom/sec/android/app/shealth/framework/ui/drawermenu/DrawerMenuAdapter;->clearProfileImageCache()V

    goto/16 :goto_0

    .line 364
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    invoke-virtual {p0, p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->setDrawerMenuListener(Lcom/sec/android/app/shealth/framework/ui/base/DrawerMenuActionListener;)V

    .line 119
    new-instance v1, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->awardSummuryFragment:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    .line 120
    const-string v1, "AwardRuleManager"

    const-string v2, "AwardActivity  onCreate()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mSharedPrefsHelper:Lcom/sec/android/app/shealth/framework/ui/utils/UIPrefsHelper;

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->awardSummuryFragment:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/award/AwardActivity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 123
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 125
    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "uriString":Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    .line 129
    .end local v0    # "uriString":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v0, 0x0

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f100000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 222
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->isDrawerMenuShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    :goto_0
    return v0

    .line 225
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 226
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 200
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mBStateLossOnsaveInstance:Z

    .line 202
    return-void
.end method

.method public onDrawerClosed()V
    .locals 0

    .prologue
    .line 280
    return-void
.end method

.method public onDrawerOpened()V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method protected onLogSelected()V
    .locals 0

    .prologue
    .line 216
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 237
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f080c8a

    if-ne v3, v4, :cond_0

    .line 238
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    .line 262
    :goto_0
    return v3

    .line 240
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 262
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 242
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 243
    .local v1, "intentEditProfile":Landroid/content/Intent;
    const-string v3, "TYPE"

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 244
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/award/AwardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 247
    .end local v1    # "intentEditProfile":Landroid/content/Intent;
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/settings/userprofile/ProfileInformationActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 248
    .local v0, "informationIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth"

    const-string v5, "S007"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/award/AwardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 253
    .end local v0    # "informationIntent":Landroid/content/Intent;
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->prepareShareView()V

    goto :goto_1

    .line 256
    :pswitch_3
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 257
    .local v2, "intentSetting":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.shealth"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    const-string v3, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/award/AwardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x7f080c86
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 232
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mBStateLossOnsaveInstance:Z

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->awardSummuryFragment:Lcom/sec/android/app/shealth/award/AwardSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->imageClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/award/AwardSummaryFragment;->setImageClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mDrawerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;->isDrawerOpen(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    invoke-direct {p0}, Lcom/sec/android/app/shealth/award/AwardActivity;->setActionBarTitle()V

    .line 194
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 195
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mBStateLossOnsaveInstance:Z

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->IMAGE_CAPTURE_URI:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/shealth/award/AwardActivity;->mImageCaptureUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 212
    return-void
.end method

.method public onSlide(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 284
    return-void
.end method
