.class final Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;
.super Ljava/lang/Object;
.source "ShareUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createShareViewForSNS(Landroid/app/Activity;Landroid/graphics/Bitmap;IJILjava/util/List;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$extraImageList:Ljava/util/List;

.field final synthetic val$mainContent:Landroid/graphics/Bitmap;

.field final synthetic val$mainView:Landroid/view/View;

.field final synthetic val$type:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/graphics/Bitmap;Landroid/app/Activity;Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$mainView:Landroid/view/View;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$mainContent:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$activity:Landroid/app/Activity;

    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$type:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    iput-object p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$extraImageList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const v12, 0x7f090033

    const/4 v11, 0x0

    .line 256
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$mainView:Landroid/view/View;

    const v9, 0x7f080798

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 257
    .local v7, "titleView":Landroid/view/View;
    invoke-static {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->getScreenshot(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 259
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$mainContent:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    add-int/2addr v9, v10

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 262
    .local v5, "resultBitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 263
    .local v3, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v3, v2, v11, v11, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 264
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$mainContent:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v3, v8, v11, v9, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 266
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$activity:Landroid/app/Activity;

    invoke-static {v8, v5}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->saveShareImageToSdCard(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/io/File;

    move-result-object v4

    .line 268
    .local v4, "file":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$type:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->Twitter:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    if-ne v8, v9, :cond_1

    .line 269
    new-instance v6, Landroid/content/Intent;

    const-string v8, "android.intent.action.SEND"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 270
    .local v6, "sendingIntent":Landroid/content/Intent;
    const-string v8, "image/jpeg"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    const-string v8, "android.intent.extra.STREAM"

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 273
    const-string/jumbo v0, "theme"

    .line 274
    .local v0, "THEME_CHOOSER":Ljava/lang/String;
    const/4 v1, 0x2

    .line 276
    .local v1, "THEME_DEVICE_DEFAULT_LIGHT":I
    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 277
    const-string v8, "com.twitter.android"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$activity:Landroid/app/Activity;

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 295
    .end local v0    # "THEME_CHOOSER":Ljava/lang/String;
    .end local v1    # "THEME_DEVICE_DEFAULT_LIGHT":I
    .end local v6    # "sendingIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 280
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$type:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;->FaceBook:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$SnsType;

    if-ne v8, v9, :cond_0

    .line 281
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$extraImageList:Ljava/util/List;

    const/4 v9, 0x0

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v10

    invoke-interface {v8, v9, v10}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 283
    new-instance v6, Landroid/content/Intent;

    const-string v8, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 284
    .restart local v6    # "sendingIntent":Landroid/content/Intent;
    const-string v8, "image/jpeg"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    const-string v9, "android.intent.extra.STREAM"

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$extraImageList:Ljava/util/List;

    check-cast v8, Ljava/util/ArrayList;

    invoke-virtual {v6, v9, v8}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 287
    const-string/jumbo v0, "theme"

    .line 288
    .restart local v0    # "THEME_CHOOSER":Ljava/lang/String;
    const/4 v1, 0x2

    .line 290
    .restart local v1    # "THEME_DEVICE_DEFAULT_LIGHT":I
    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 291
    const-string v8, "com.facebook.katana"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$activity:Landroid/app/Activity;

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
