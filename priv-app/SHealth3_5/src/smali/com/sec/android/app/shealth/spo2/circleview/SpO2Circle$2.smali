.class Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;
.super Ljava/lang/Object;
.source "SpO2Circle.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->stopAnimation(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;->this$0:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;->this$0:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    # getter for: Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->access$100(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;->this$0:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    # getter for: Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mMeasuringAniLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->access$100(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;->this$0:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    # getter for: Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mSecondMeasuringProgressView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->access$200(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;->this$0:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    # getter for: Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->access$000(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;->this$0:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    # getter for: Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v0}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->access$000(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;)Landroid/view/animation/AnimationSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle$2;->this$0:Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->mCircleAnimationSet:Landroid/view/animation/AnimationSet;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;->access$002(Lcom/sec/android/app/shealth/spo2/circleview/SpO2Circle;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;

    .line 130
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 119
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 115
    return-void
.end method
