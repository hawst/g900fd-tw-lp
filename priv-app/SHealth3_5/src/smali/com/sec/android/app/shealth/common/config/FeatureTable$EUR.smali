.class final enum Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;
.super Ljava/lang/Enum;
.source "FeatureTable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/config/FeatureTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "EUR"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

.field public static final enum FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

.field public static final enum TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    const-string v1, "TRUE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    .line 30
    new-instance v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    const-string v1, "FALSE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->TRUE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->FALSE:Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->$VALUES:[Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/common/config/FeatureTable$EUR;

    return-object v0
.end method
