.class public Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;
.super Ljava/lang/Object;
.source "SelectedPanelHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_AMOUNT:F = 1.0f

.field public static final NO_ID:I = -0x1


# instance fields
.field private mCategory:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

.field private mId:Ljava/lang/Long;

.field private mMealItemDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mUniqueId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 220
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$3;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$3;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p4, "mealItemDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_DATA:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    invoke-direct {p0, p3, v0, v1, p4}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Ljava/lang/String;Ljava/lang/Long;Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;Ljava/util/List;)V

    .line 91
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    .line 195
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mName:Ljava/lang/String;

    .line 196
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mId:Ljava/lang/Long;

    .line 197
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mUniqueId:Ljava/lang/String;

    .line 198
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->values()[Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mCategory:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    if-nez v0, :cond_0

    .line 201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    const-class v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 204
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$1;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;)V
    .locals 1
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;

    .prologue
    .line 79
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;F)V

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;F)V
    .locals 4
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "amount"    # F

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$1;

    invoke-direct {v3, p1, p2}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$1;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;F)V

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Ljava/lang/String;Ljava/lang/Long;Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;Ljava/util/List;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;FI)V
    .locals 4
    .param p1, "foodInfoData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;
    .param p2, "amount"    # F
    .param p3, "unit"    # I

    .prologue
    .line 63
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;->getId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_ITEM:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    new-instance v3, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;

    invoke-direct {v3, p1, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$2;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/FoodInfoData;FI)V

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;-><init>(Ljava/lang/String;Ljava/lang/Long;Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;Ljava/util/List;)V

    .line 71
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/Long;Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;Ljava/util/List;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/Long;
    .param p3, "category"    # Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p4, "mealItemDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    .line 102
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mName:Ljava/lang/String;

    .line 103
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mId:Ljava/lang/Long;

    .line 104
    iput-object p3, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mCategory:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mUniqueId:Ljava/lang/String;

    .line 106
    iput-object p4, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    .line 107
    return-void
.end method


# virtual methods
.method public addMealItemData(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;)V
    .locals 1
    .param p1, "mealItemData"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    return-void
.end method

.method public addMealItemDatas(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, "mealItemDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 192
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public getCategory()Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mCategory:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getMealItemData()Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->isMyMeal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Use getMealItemDatas() to get My Meal data."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->getMealItemDatas()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;

    return-object v0
.end method

.method public getMealItemDatas()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealItemData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mUniqueId:Ljava/lang/String;

    return-object v0
.end method

.method protected isMyMeal()Z
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mCategory:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->MEAL_DATA:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mUniqueId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mCategory:Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder$Category;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/components/SelectedPanelHolder;->mMealItemDatas:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 218
    return-void
.end method
