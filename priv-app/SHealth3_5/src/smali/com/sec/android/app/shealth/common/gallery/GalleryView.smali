.class public Lcom/sec/android/app/shealth/common/gallery/GalleryView;
.super Landroid/widget/HorizontalScrollView;
.source "GalleryView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;,
        Lcom/sec/android/app/shealth/common/gallery/GalleryView$OnItemSelectedListener;,
        Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;
    }
.end annotation


# instance fields
.field private mCompleteItemAddNotifier:Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;

.field private mContainer:Landroid/view/ViewGroup;

.field private mImagePathList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mItemSize:I

.field private mLastSelected:Landroid/view/View;

.field private mOnItemSelectedListener:Lcom/sec/android/app/shealth/common/gallery/GalleryView$OnItemSelectedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0787

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mItemSize:I

    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->init()V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0787

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mItemSize:I

    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->init()V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0787

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mItemSize:I

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->init()V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mLastSelected:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/common/gallery/GalleryView;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->scrollAccordingCenter(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mItemSize:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/common/gallery/GalleryView;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->selectItem(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Lcom/sec/android/app/shealth/common/gallery/GalleryView$OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mOnItemSelectedListener:Lcom/sec/android/app/shealth/common/gallery/GalleryView$OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mImagePathList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mCompleteItemAddNotifier:Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;

    return-object v0
.end method

.method private init()V
    .locals 4

    .prologue
    .line 70
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->addView(Landroid/view/View;)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/common/gallery/GalleryView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView$1;-><init>(Lcom/sec/android/app/shealth/common/gallery/GalleryView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 83
    return-void
.end method

.method private scrollAccordingCenter(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string/jumbo v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 224
    .local v0, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 225
    .local v2, "size":Landroid/graphics/Point;
    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 226
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    iget v4, v2, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    add-int v1, v3, v4

    .line 227
    .local v1, "scrollX":I
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->smoothScrollTo(II)V

    .line 228
    return-void
.end method

.method private selectItem(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 205
    if-eqz p1, :cond_1

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mLastSelected:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mLastSelected:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 213
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 215
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->scrollAccordingCenter(Landroid/view/View;)V

    .line 217
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mLastSelected:Landroid/view/View;

    .line 220
    :cond_1
    return-void
.end method


# virtual methods
.method public addItem(Ljava/lang/String;)V
    .locals 5
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 107
    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 108
    .local v0, "thumbnail":Landroid/widget/ImageButton;
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v2, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mItemSize:I

    iget v3, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mItemSize:I

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 110
    const v1, 0x7f02019b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 112
    new-instance v1, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;

    invoke-direct {v1, p0, v0, p1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;-><init>(Lcom/sec/android/app/shealth/common/gallery/GalleryView;Landroid/widget/ImageButton;Ljava/lang/String;)V

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/gallery/GalleryView$ThumbNailImageTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 115
    return-void
.end method

.method public addItems(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "imagePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mImagePathList:Ljava/util/List;

    .line 92
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 93
    .local v1, "path":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->addItem(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public getItemsCount()I
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getSelectedPosition()I
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mLastSelected:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public removeAllViews()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 100
    return-void
.end method

.method public selectItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->selectItem(Landroid/view/View;)V

    .line 191
    return-void
.end method

.method public setCompleteItemAddNotifier(Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;)V
    .locals 0
    .param p1, "completeItemAddNotifier"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mCompleteItemAddNotifier:Lcom/sec/android/app/shealth/common/gallery/GalleryView$CompleteItemAddNotifier;

    .line 259
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->callOnClick()Z

    .line 199
    return-void
.end method

.method public setOnItemSelectedListener(Lcom/sec/android/app/shealth/common/gallery/GalleryView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "onItemSelectedListener"    # Lcom/sec/android/app/shealth/common/gallery/GalleryView$OnItemSelectedListener;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/gallery/GalleryView;->mOnItemSelectedListener:Lcom/sec/android/app/shealth/common/gallery/GalleryView$OnItemSelectedListener;

    .line 182
    return-void
.end method
