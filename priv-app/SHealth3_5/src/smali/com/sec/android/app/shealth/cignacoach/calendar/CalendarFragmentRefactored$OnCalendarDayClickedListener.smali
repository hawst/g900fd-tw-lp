.class Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;
.super Ljava/lang/Object;
.source "CalendarFragmentRefactored.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnCalendarDayClickedListener"
.end annotation


# instance fields
.field private final containsMeasurements:Z

.field private final dayWithMeasure:Ljava/lang/Long;

.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;Ljava/lang/Long;Z)V
    .locals 0
    .param p2, "dayWithMeasure"    # Ljava/lang/Long;
    .param p3, "containsMeasurements"    # Z

    .prologue
    .line 817
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 818
    iput-object p2, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;->dayWithMeasure:Ljava/lang/Long;

    .line 819
    iput-boolean p3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;->containsMeasurements:Z

    .line 820
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 824
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;->this$0:Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;->dayWithMeasure:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-boolean v3, p0, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored$OnCalendarDayClickedListener;->containsMeasurements:Z

    # invokes: Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->onCalendarDayClicked(JZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;->access$100(Lcom/sec/android/app/shealth/cignacoach/calendar/CalendarFragmentRefactored;JZ)V

    .line 826
    return-void
.end method
