.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;
.super Ljava/lang/Object;
.source "ExerciseProStatusFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 333
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onCreateView()-Runnable: Fragment resumed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->isResumed()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",Visible:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->isVisible()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;

    .line 336
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->loadConfigure()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V

    .line 337
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->setBigTextSize()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V

    .line 341
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getSwitchButton()Landroid/view/View;

    move-result-object v0

    .line 342
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 343
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 355
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 356
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->setMapEnable(Z)V

    .line 359
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->getCurrentMode()I

    move-result v2

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->setWorkoutModeState(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;I)V

    .line 360
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$1;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeMapZoomLevel()F

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mZoomLevel:F
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;F)F

    .line 361
    return-void
.end method
