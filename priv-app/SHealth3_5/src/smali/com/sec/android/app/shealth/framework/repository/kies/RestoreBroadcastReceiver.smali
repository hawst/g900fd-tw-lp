.class public Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RestoreBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$1;,
        Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$OnRestoreEndHandler;
    }
.end annotation


# static fields
.field public static final ACTION_ID_KEY:Ljava/lang/String; = "ACTION"

.field public static final CANCEL_ACTION:I = 0x2

.field public static final IS_KIES_RESTORE_MODE:Ljava/lang/String; = "IS_KIES_RESTORE_MODE"

.field public static final KIES2_RESTORE:Ljava/lang/String; = "android.intent.action.REQUEST_RESTORE_SHEALTH"

.field public static final KIES2_RESTORE_COMPLETE_INTENT:Ljava/lang/String; = "android.intent.action.RESPONSE_RESTORE_SHEALTH"

.field public static final KIES_PATH:Ljava/lang/String; = "SAVE_PATH"

.field public static final KIES_RESTORE:Ljava/lang/String; = "com.sec.android.intent.action.REQUEST_RESTORE_SHEALTH"

.field public static final KIES_RESTORE_COMPLETE_INTENT:Ljava/lang/String; = "com.sec.android.intent.action.RESPONSE_RESTORE_SHEALTH"

.field public static KIES_RESTORE_SHARED_PREF_FILE:Ljava/lang/String; = null

.field public static KIES_SHARED_PREF_FILE:Ljava/lang/String; = null

.field public static final KIES_SOURCE:Ljava/lang/String; = "SOURCE"

.field private static final NEW_VERSION:I = 0x6

.field public static final POPUP_ACTION_IS_BACKUP_OR_RESTORE_RUNNING:I = -0xd

.field public static final POPUP_ACTION_IS_SHEALTH_RUNNING:I = -0xb

.field public static final POPUP_ACTION_IS_WORKOUT_RUNNING:I = -0xc

.field public static final POPUP_ACTION_NAME:Ljava/lang/String; = "POPUP_ACTION_NAME"

.field public static final SDCARD_PATH:Ljava/lang/String;

.field public static SHARED_PREF_FOLDER_NAME:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static kies_intent_source:Ljava/lang/String;

.field public static mContext:Landroid/content/Context;


# instance fields
.field private KIES_BACKGROUND_PORTRAIT__IMAGE:Ljava/lang/String;

.field private KIES_IMAGES_FOLDER:Ljava/lang/String;

.field private KIES_MEAL_IMAGES_FOLDER:Ljava/lang/String;

.field private KIES_PROFILE_IMAGE:Ljava/lang/String;

.field private final SHEALTH2_NORMAL_DB_PATH:Ljava/lang/String;

.field private final SHEALTH2_SECURE_DB_PATH:Ljava/lang/String;

.field private final SHEALTH2_SECURE_PROVIDER_DB_PATH:Ljava/lang/String;

.field private final SHEALTH_BACKGROUND_IMAGE_PATH:Ljava/lang/String;

.field private final SHEALTH_BASE_DB_PATH:Ljava/lang/String;

.field private final SHEALTH_KIES_SHAREDPREF_PATH:Ljava/lang/String;

.field private final SHEALTH_UPGRADE_DB_PATH:Ljava/lang/String;

.field public final USER_BG_FILE_FULL_PATH:Ljava/lang/String;

.field public final USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

.field private final USER_PHOTO_FILE_PATH:Ljava/lang/String;

.field private final WAKE_LOCK_TIME_OUT:J

.field private final backupSync:Ljava/lang/Object;

.field private restoreThread:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    const-class v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 92
    const-string/jumbo v0, "prefs.bin"

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_SHARED_PREF_FILE:Ljava/lang/String;

    .line 95
    const-string/jumbo v0, "shared_prefs"

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHARED_PREF_FOLDER_NAME:Ljava/lang/String;

    .line 98
    const-string v0, "kies_prefs.bin"

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_RESTORE_SHARED_PREF_FILE:Ljava/lang/String;

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Android/data/com.sec.android.app.shealth/cache/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SDCARD_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->restoreThread:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->backupSync:Ljava/lang/Object;

    .line 80
    const-string v0, "images"

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_IMAGES_FOLDER:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_IMAGES_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SHealth2/cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_MEAL_IMAGES_FOLDER:Ljava/lang/String;

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_MEAL_IMAGES_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "img"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "user_photo.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_PROFILE_IMAGE:Ljava/lang/String;

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_MEAL_IMAGES_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "img"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "background_portrait_image.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_BACKGROUND_PORTRAIT__IMAGE:Ljava/lang/String;

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/data/data/com.sec.android.app.shealth/databases/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "shealth2.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH2_NORMAL_DB_PATH:Ljava/lang/String;

    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/data/data/com.sec.android.app.shealth/databases/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "secure_sec_health.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH2_SECURE_PROVIDER_DB_PATH:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/data/data/com.sec.android.app.shealth/databases/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "secure_shealth2.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH2_SECURE_DB_PATH:Ljava/lang/String;

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/data/data/com.sec.android.app.shealth/databases/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "base.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH_BASE_DB_PATH:Ljava/lang/String;

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "background_images"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "background_portrait_image.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH_BACKGROUND_IMAGE_PATH:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/database/DBManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHARED_PREF_FOLDER_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_SHARED_PREF_FILE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH_KIES_SHAREDPREF_PATH:Ljava/lang/String;

    .line 134
    const-string v0, "SHealth3/cache/img/user_photo.jpg"

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->USER_PHOTO_FILE_PATH:Ljava/lang/String;

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SHealth3/cache/img/user_photo.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SHealth3/cache/img/background_portrait_image.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->USER_BG_FILE_FULL_PATH:Ljava/lang/String;

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/data/data/com.sec.android.app.shealth/databases/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "shealth_to_upgrade.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH_UPGRADE_DB_PATH:Ljava/lang/String;

    .line 142
    const-wide/16 v0, 0xdac

    iput-wide v0, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->WAKE_LOCK_TIME_OUT:J

    .line 268
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->upgradeApplication()V

    return-void
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->kies_intent_source:Ljava/lang/String;

    return-object v0
.end method

.method private static getErrorIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "intentName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 231
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "RESULT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 233
    const-string v1, "ERR_CODE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 234
    const-string v1, "REQ_SIZE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 235
    const-string v1, "SOURCE"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    return-object v0
.end method

.method public static final isShealthRunning(Landroid/content/Context;)Z
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 246
    const-string v3, "activity"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 247
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/16 v3, 0x64

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 248
    .local v2, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v2, :cond_1

    .line 250
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 252
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "base activity: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 256
    const/4 v3, 0x1

    .line 260
    .end local v1    # "i":I
    :goto_1
    return v3

    .line 250
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    .end local v1    # "i":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static renameFiletoOLD(Ljava/lang/String;)V
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 356
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_old"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 357
    .local v1, "old_file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 358
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 359
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 360
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 361
    return-void
.end method

.method private upgradeApplication()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 327
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "upgradeApplication"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH2_SECURE_DB_PATH:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 329
    .local v3, "secure_db":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH2_NORMAL_DB_PATH:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 330
    .local v2, "normal_db":Ljava/io/File;
    const/4 v1, 0x0

    .line 331
    .local v1, "isRestoreFromJ":Z
    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH_UPGRADE_DB_PATH:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->renameFiletoOLD(Ljava/lang/String;)V

    .line 332
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 334
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH_UPGRADE_DB_PATH:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 343
    :cond_0
    :goto_0
    new-instance v0, Landroid/content/Intent;

    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->mContext:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/shealth/framework/repository/MigrationService;

    invoke-direct {v0, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 344
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "IS_KIES_RESTORE_MODE"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 345
    if-nez v1, :cond_1

    .line 347
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Adding pasword"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    const-string v4, "isSecure"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 350
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 351
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "upgradeApplication Finished"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    return-void

    .line 336
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 338
    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v5, "Converting J db to secure DB"

    invoke-static {v4, v5}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    const/4 v1, 0x1

    .line 340
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH_UPGRADE_DB_PATH:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_0
.end method


# virtual methods
.method public getRestoreFileCopyPathList(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "kiesPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 299
    .local v0, "copyPathList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;>;"
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "secure_shealth2.db"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 300
    .local v1, "securedb":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 303
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v3, "KiesBackuper getRestoreFileCopyPathList : JMR/H"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "secure_shealth2.db"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH2_SECURE_DB_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 305
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_IMAGES_FOLDER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SDCARD_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 307
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_PROFILE_IMAGE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    :cond_0
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_BACKGROUND_PORTRAIT__IMAGE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->USER_BG_FILE_FULL_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 321
    :goto_0
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_SHARED_PREF_FILE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH_KIES_SHAREDPREF_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    return-object v0

    .line 313
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v3, "KiesBackuper getRestoreFileCopyPathList : J"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "shealth2.db"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH2_NORMAL_DB_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "secure_sec_health.db"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SHEALTH2_SECURE_PROVIDER_DB_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 317
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "user_photo.jpg"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->USER_PHOTO_FILE_FULL_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    :cond_2
    new-instance v2, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->KIES_IMAGES_FOLDER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->SDCARD_PATH:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/sec/android/app/shealth/framework/repository/kies/FileCopyUtils$FileCopyPath;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 151
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " Receive intent action = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    sput-object p1, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->mContext:Landroid/content/Context;

    .line 153
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 154
    .local v8, "kies_intent_action":Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "isBlocked":Z
    const/4 v7, 0x0

    .line 155
    .local v7, "isSHealthRunning":Z
    const-string v13, "SOURCE"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->kies_intent_source:Ljava/lang/String;

    .line 158
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->isShealthRunning(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 160
    if-eqz v6, :cond_1

    const/16 v4, -0xc

    .line 161
    .local v4, "error_id":I
    :goto_0
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->kies_intent_source:Ljava/lang/String;

    const-string v14, "com.sec.android.intent.action.RESPONSE_RESTORE_SHEALTH"

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->getErrorIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 162
    .local v3, "errorIntent":Landroid/content/Intent;
    const-string v13, "POPUP_ACTION_NAME"

    invoke-virtual {v3, v13, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 163
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 164
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Cannot backup or restore error id = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    new-instance v5, Landroid/content/Intent;

    const-class v13, Lcom/sec/android/app/shealth/framework/ui/base/ShowAlertActivity;

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 166
    .local v5, "i":Landroid/content/Intent;
    const/high16 v13, 0x10000000

    invoke-virtual {v5, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 167
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 227
    .end local v3    # "errorIntent":Landroid/content/Intent;
    .end local v4    # "error_id":I
    .end local v5    # "i":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-void

    .line 160
    :cond_1
    const/16 v4, -0xb

    goto :goto_0

    .line 171
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->isRestoreBlocked()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 174
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v14, " Shealth upgrade is not done yet. Please launch shealth to complete upgrade process before restore"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->kies_intent_source:Ljava/lang/String;

    const-string v14, "com.sec.android.intent.action.RESPONSE_RESTORE_SHEALTH"

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->getErrorIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 176
    .restart local v3    # "errorIntent":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 177
    const-string/jumbo v13, "power"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/PowerManager;

    .line 178
    .local v9, "powerManager":Landroid/os/PowerManager;
    const/4 v12, 0x0

    .line 179
    .local v12, "wakeLock":Landroid/os/PowerManager$WakeLock;
    const v13, 0x1000000a

    sget-object v14, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    invoke-virtual {v9, v13, v14}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v12

    .line 180
    const-wide/16 v13, 0xdac

    invoke-virtual {v12, v13, v14}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 181
    sget v13, Lcom/sec/android/app/shealth/framework/repository/R$string;->kies_restoration_failed:I

    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 185
    .end local v3    # "errorIntent":Landroid/content/Intent;
    .end local v9    # "powerManager":Landroid/os/PowerManager;
    .end local v12    # "wakeLock":Landroid/os/PowerManager$WakeLock;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->isAlreadyRestored(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 187
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v14, " Shealth isAlreadyRestored can not be restored more than once"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->kies_intent_source:Ljava/lang/String;

    const-string v14, "com.sec.android.intent.action.RESPONSE_RESTORE_SHEALTH"

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->getErrorIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 189
    .restart local v3    # "errorIntent":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 191
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 192
    .local v10, "result":Landroid/content/Intent;
    const-string v13, "com.sec.android.intent.action.RESPONSE_RESTORE_SHEALTH"

    invoke-virtual {v10, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    const-string v13, "error"

    const/4 v14, 0x1

    invoke-virtual {v10, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 194
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 198
    .end local v3    # "errorIntent":Landroid/content/Intent;
    .end local v10    # "result":Landroid/content/Intent;
    :cond_4
    sget-boolean v13, Lcom/sec/android/app/shealth/framework/repository/MigrationService;->isRestoreInProgress:Z

    if-eqz v13, :cond_5

    .line 200
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string v14, " Shealth Restore is in progress"

    invoke-static {v13, v14}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->kies_intent_source:Ljava/lang/String;

    const-string v14, "com.sec.android.intent.action.RESPONSE_RESTORE_SHEALTH"

    invoke-static {v13, v14}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->getErrorIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 202
    .restart local v3    # "errorIntent":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 203
    sget v13, Lcom/sec/android/app/shealth/framework/repository/R$string;->shealth_restore_inprogress:I

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 207
    .end local v3    # "errorIntent":Landroid/content/Intent;
    :cond_5
    const-string v13, "SAVE_PATH"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 208
    .local v11, "savePath":Ljava/lang/String;
    if-nez v11, :cond_6

    .line 210
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->TAG:Ljava/lang/String;

    const-string/jumbo v14, "savePath is null"

    invoke-static {v13, v14}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 215
    :cond_6
    const-string v13, "com.sec.android.intent.action.REQUEST_RESTORE_SHEALTH"

    invoke-virtual {v8, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 217
    sget-object v13, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;->Kies:Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;

    invoke-static {v13}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier;->restoreStarted(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreNotifier$RestoreType;)V

    .line 218
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->backupSync:Ljava/lang/Object;

    monitor-enter v14

    .line 220
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->restoreThread:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->restoreThread:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;

    invoke-virtual {v13}, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v13

    sget-object v15, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v13, v15, :cond_8

    .line 222
    :cond_7
    new-instance v13, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;

    const-string v15, "com.sec.android.intent.action.RESPONSE_RESTORE_SHEALTH"

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->getRestoreFileCopyPathList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    new-instance v17, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$OnRestoreEndHandler;

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$OnRestoreEndHandler;-><init>(Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver$1;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, v17

    invoke-direct {v13, v0, v15, v1, v2}, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer$RestoreCompleteListener;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->restoreThread:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;

    .line 223
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/shealth/framework/repository/kies/RestoreBroadcastReceiver;->restoreThread:Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;

    sget-object v16, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v13, 0x0

    check-cast v13, [Ljava/lang/Void;

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v13}, Lcom/sec/android/app/shealth/framework/repository/kies/KiesRestorer;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 225
    :cond_8
    monitor-exit v14

    goto/16 :goto_1

    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v13
.end method
