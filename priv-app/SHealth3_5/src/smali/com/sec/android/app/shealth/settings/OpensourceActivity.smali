.class public Lcom/sec/android/app/shealth/settings/OpensourceActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "OpensourceActivity.java"


# instance fields
.field private opensource:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->opensource:Landroid/widget/TextView;

    return-void
.end method

.method private returnLicenseInformation()Ljava/lang/String;
    .locals 7

    .prologue
    .line 44
    const/4 v4, 0x0

    .line 46
    .local v4, "strLicense":Ljava/lang/String;
    const/4 v2, 0x0

    .line 48
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    const-string/jumbo v6, "opensourcelicense.txt"

    invoke-virtual {v5, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 50
    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v3

    .line 52
    .local v3, "size":I
    new-array v0, v3, [B

    .line 53
    .local v0, "buffer":[B
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    .line 55
    new-instance v4, Ljava/lang/String;

    .end local v4    # "strLicense":Ljava/lang/String;
    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    .restart local v4    # "strLicense":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 61
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 67
    :cond_0
    :goto_0
    return-object v4

    .line 62
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 56
    .end local v0    # "buffer":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "size":I
    .end local v4    # "strLicense":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 57
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_2
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 59
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 60
    if-eqz v2, :cond_1

    .line 61
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 64
    :cond_1
    :goto_1
    throw v5

    .line 62
    :catch_2
    move-exception v1

    .line 63
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f0907ef

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v0

    const-string v1, "#73b90f"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 78
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v0, 0x7f030184

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->setContentView(I)V

    .line 40
    const v0, 0x7f080678

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->opensource:Landroid/widget/TextView;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->opensource:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/OpensourceActivity;->returnLicenseInformation()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    return-void
.end method
