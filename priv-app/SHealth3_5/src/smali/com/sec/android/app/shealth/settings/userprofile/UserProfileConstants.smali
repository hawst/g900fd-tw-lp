.class public Lcom/sec/android/app/shealth/settings/userprofile/UserProfileConstants;
.super Ljava/lang/Object;
.source "UserProfileConstants.java"


# static fields
.field public static final ACTIVITY_LEV1:I = 0x1

.field public static final ACTIVITY_LEV2:I = 0x2

.field public static final ACTIVITY_LEV3:I = 0x3

.field public static final ACTIVITY_LEV4:I = 0x4

.field public static final ACTIVITY_LEV5:I = 0x5

.field public static final DEFAULT_WEIGHT:F = 50.0f

.field public static final EDIT_PROFILE:I = 0x2

.field public static final FIRST_CARD:I = 0x1

.field public static final FOURTH_CARD:I = 0x4

.field public static final HEAVY_ACTIVITY:B = 0x4t

.field public static final INIT_PROFILE:I = 0x1

.field public static final LIGHT_ACTIVITY:B = 0x2t

.field public static final MAX_CHARACTER_COUNT:I = 0x32

.field public static final MAX_WEIGHT:F = 500.0f

.field public static final MIN_WEIGHT:F = 2.0f

.field public static final MODERATE_ACTIVITY:B = 0x3t

.field public static final MOVE_DISTANCE:F = 0.1f

.field public static final NO_ACTIVITY:B = 0x1t

.field public static final SECOND_CARD:I = 0x2

.field public static final THIRD_CARD:I = 0x3

.field public static final TYPE:Ljava/lang/String; = "TYPE"

.field public static final VERY_HEAVY_ACTIVITY:B = 0x5t


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
