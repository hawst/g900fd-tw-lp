.class Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;
.super Ljava/lang/Object;
.source "HomeExerciseData.java"

# interfaces
.implements Landroid/text/Html$ImageGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/data/HomeExerciseData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/data/HomeExerciseData;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/data/HomeExerciseData;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;->this$0:Lcom/sec/android/app/shealth/home/data/HomeExerciseData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 240
    const/4 v0, 0x0

    .line 241
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    const-string v1, "durationIcon"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;->this$0:Lcom/sec/android/app/shealth/home/data/HomeExerciseData;

    # getter for: Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->access$000(Lcom/sec/android/app/shealth/home/data/HomeExerciseData;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0204c4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 258
    :cond_0
    :goto_0
    return-object v0

    .line 244
    :cond_1
    const-string v1, "distanceIcon"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;->this$0:Lcom/sec/android/app/shealth/home/data/HomeExerciseData;

    # getter for: Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->access$000(Lcom/sec/android/app/shealth/home/data/HomeExerciseData;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0204c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    .line 247
    :cond_2
    const-string v1, "elevation"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;->this$0:Lcom/sec/android/app/shealth/home/data/HomeExerciseData;

    # getter for: Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->access$000(Lcom/sec/android/app/shealth/home/data/HomeExerciseData;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0204c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 249
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    .line 250
    :cond_3
    const-string v1, "calory"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;->this$0:Lcom/sec/android/app/shealth/home/data/HomeExerciseData;

    # getter for: Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->access$000(Lcom/sec/android/app/shealth/home/data/HomeExerciseData;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0204c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 252
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto :goto_0

    .line 253
    :cond_4
    const-string v1, "heart"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/data/HomeExerciseData$1;->this$0:Lcom/sec/android/app/shealth/home/data/HomeExerciseData;

    # getter for: Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/data/HomeExerciseData;->access$000(Lcom/sec/android/app/shealth/home/data/HomeExerciseData;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0204c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 255
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    goto/16 :goto_0
.end method
