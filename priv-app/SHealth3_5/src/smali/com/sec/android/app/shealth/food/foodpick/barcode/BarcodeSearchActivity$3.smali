.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;
.super Ljava/lang/Object;
.source "BarcodeSearchActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$ResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBarcodeTaken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "contents"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->stopCameraFocusing()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$300(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;)V

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mSearchFoodInfoFromBarCodeAsyncTask:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$402(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mSearchFoodInfoFromBarCodeAsyncTask:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$400(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SearchFoodInfoFromBarCodeAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mBarcode:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$602(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 165
    return-void
.end method

.method public onTimeOut()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mProgressDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$700(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/LoadingScreenPopup;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$3;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->pauseHandler:Lcom/sec/android/app/shealth/food/utils/PauseHandler;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/utils/PauseHandler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/food/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 172
    :cond_0
    return-void
.end method
