.class public Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;
.super Lcom/sec/android/app/shealth/stress/state/StressState;
.source "StressStateMeasureWarning.java"


# static fields
.field private static final CURRENT_STATE_TIMEOUT:J = 0xbb8L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isInCurrentState:Z

.field private mCurrentStateTimer:Landroid/os/CountDownTimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V
    .locals 6
    .param p1, "summaryFragment"    # Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/stress/state/StressState;-><init>(Lcom/sec/android/app/shealth/stress/StressSummaryFragment;)V

    .line 55
    new-instance v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0x64

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning$1;-><init>(Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;JJ)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    .line 18
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;

    .prologue
    .line 10
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->isInCurrentState:Z

    return v0
.end method


# virtual methods
.method public clearForNextState()V
    .locals 2

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->TAG:Ljava/lang/String;

    const-string v1, "clearForNextState()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->isInCurrentState:Z

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 53
    return-void
.end method

.method public reStartStates()V
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateReady()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    .line 24
    return-void
.end method

.method public terminateState()V
    .locals 2

    .prologue
    .line 77
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "terminateState()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->clearForNextState()V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->stopSensor()V

    .line 81
    return-void
.end method

.method public updateNextState(II)V
    .locals 3
    .param p1, "rate"    # I
    .param p2, "interval"    # I

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "updateNextState() rate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 30
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->clearForNextState()V

    .line 31
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->setState(Lcom/sec/android/app/shealth/stress/state/StressState;)V

    .line 32
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->getStateMeasuring()Lcom/sec/android/app/shealth/stress/state/StressState;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/state/StressState;->updateViewWithAnimation(Z)V

    .line 36
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    if-lez p1, :cond_0

    goto :goto_0
.end method

.method public updateViewWithAnimation(Z)V
    .locals 3
    .param p1, "hasAnimation"    # Z

    .prologue
    const/4 v1, 0x1

    .line 40
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->isInCurrentState:Z

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mCurrentStateTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v2}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 42
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 43
    .local v0, "msg":Landroid/os/Message;
    const/4 v2, 0x2

    iput v2, v0, Landroid/os/Message;->what:I

    .line 44
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :cond_0
    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/state/StressStateMeasureWarning;->mSummaryFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 46
    return-void
.end method
