.class public Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;
.source "WeightDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;",
        "Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;",
        ">;",
        "Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;"
    }
.end annotation


# static fields
.field private static final CONTRACT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/weight/weightdao/WeightDao$WeightContract;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->CONTRACT_MAP:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v3, "sample_time"

    const-string v4, "_id"

    const-string/jumbo v5, "weight"

    const-string/jumbo v6, "weight"

    sget-object v7, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->CONTRACT_MAP:Ljava/util/Map;

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;-><init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    iget-object v1, p0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    .line 73
    return-void
.end method


# virtual methods
.method public addGoal(F)J
    .locals 3
    .param p1, "goalValue"    # F

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    const v2, 0x9c44

    invoke-direct {v1, p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FI)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    move-result-wide v0

    return-wide v0
.end method

.method public deleteAllGoals()I
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    const v1, 0x9c44

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->deleteAllByGoalType(I)I

    move-result v0

    return v0
.end method

.method public getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 2
    .param p1, "beforeTime"    # J

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    const v1, 0x9c44

    invoke-interface {v0, v1, p1, p2}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->getActiveGoal(IJ)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 50
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;)Landroid/content/ContentValues;
    .locals 4
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    .prologue
    .line 132
    if-nez p1, :cond_0

    .line 133
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "WeightData cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :cond_0
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;

    invoke-direct {v0, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)V

    .line 136
    .local v0, "weight":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;
    const-string/jumbo v1, "user_device__id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getUserDeviceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v1, "body_fat"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyFat()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 138
    const-string v1, "height"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getHeight()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 139
    const-string/jumbo v1, "weight"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 140
    const-string v1, "body_mass_index"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyMassIndex()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 141
    const-string v1, "basal_metabolic_rate"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBasalMetabolicRate()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 142
    const-string v1, "activity_metabolic_rate"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getActivityMetabolicRate()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 143
    const-string v1, "body_age"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyYear()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 144
    const-string v1, "body_water"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBodyWater()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 145
    const-string/jumbo v1, "visceral_fat"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getViscFat()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 146
    const-string/jumbo v1, "muscle_mass"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getMuscleMass()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 147
    const-string/jumbo v1, "skeletal_muscle"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSkeletalMuscle()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 148
    const-string v1, "bone_mass"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getBoneMass()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 149
    const-string v1, "comment"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getComment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string/jumbo v1, "sample_time"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getSampleTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 151
    const-string v1, "input_source_type"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;->getInputSourceType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->getContentValues()Landroid/content/ContentValues;

    move-result-object v1

    return-object v1
.end method

.method protected bridge synthetic getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    move-result-object v0

    return-object v0
.end method

.method protected getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 28
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 157
    new-instance v27, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;-><init>(Landroid/database/Cursor;)V

    .line 158
    .local v27, "getter":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    const-string v3, "_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string/jumbo v5, "user_device__id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "comment"

    move-object/from16 v0, v27

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "input_source_type"

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string/jumbo v8, "sample_time"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    const-string v10, "create_time"

    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    const-string/jumbo v12, "update_time"

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    const-string/jumbo v14, "time_zone"

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v14

    const-string v15, "body_fat"

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v15

    const-string v16, "height"

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v16

    const-string/jumbo v17, "weight"

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v17

    const-string v18, "body_mass_index"

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v18

    const-string v19, "basal_metabolic_rate"

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v19

    const-string v20, "activity_metabolic_rate"

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v20

    const-string v21, "body_age"

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v21

    const-string v22, "body_water"

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v22

    const-string/jumbo v23, "visceral_fat"

    move-object/from16 v0, v27

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v23

    const-string/jumbo v24, "muscle_mass"

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v24

    const-string/jumbo v25, "skeletal_muscle"

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v25

    const-string v26, "bone_mass"

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v26

    invoke-direct/range {v2 .. v26}, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;-><init>(JLjava/lang/String;Ljava/lang/String;IJJJIFFFFFFFFFFFF)V

    .line 180
    .local v2, "weight":Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    return-object v2
.end method

.method public getLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 92
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v2, "sample_time"

    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "<"

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    .line 94
    .local v0, "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    return-object v1
.end method

.method public getLastUpdatedUserDeviceForWeight()Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 111
    const-string/jumbo v1, "update_time DESC "

    .line 113
    .local v1, "sortOrder":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v3, "device_type"

    const/16 v4, 0x2712

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v5, "="

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    .local v0, "filter":Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;
    new-instance v2, Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;

    iget-object v3, p0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;-><init>(Landroid/content/Context;)V

    .line 118
    .local v2, "userDeviceDao":Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;->getSQLCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v6, v3, v6, v1}, Lcom/sec/android/app/shealth/common/commondao/userdevicedao/UserDeviceDaoImplDb;->getFirstItemFromQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/common/utils/hcp/data/UserDeviceData;

    return-object v3
.end method

.method public getLatestGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    const v1, 0x9c44

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->getLatestGoal(I)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    return-object v0
.end method

.method public getSecondLastData(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 101
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v2, "sample_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v4, "<"

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    .line 105
    .local v0, "filterQueryBuilder":Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;->getLastDataWithFilter(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/WeightData;

    return-object v1
.end method
