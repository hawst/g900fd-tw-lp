.class public Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;
.super Landroid/widget/RelativeLayout;
.source "ComfortLevelAreaView.java"


# static fields
.field private static final HEIGHT_ALL:F = 540.0f

.field private static HEIGHT_ALL_IN_DP:F = 0.0f

.field private static HUM_TV_HEIGHT_IN_DP:F = 0.0f

.field private static MARGIN_LEFT_ALL_IN_DP:F = 0.0f

.field private static final MARGIN_LEFT_CENTER_LINE:F = 375.0f

.field private static final MARGIN_LEFT_LINE:F = 143.0f

.field private static final MARGIN_LOW_LINE:F = 397.0f

.field private static final MARGIN_RIGHT_CENTER_LINE:F = 474.0f

.field private static final MARGIN_RIGHT_LINE:F = 702.0f

.field private static MARGIN_TOP_ALL_IN_DP:F = 0.0f

.field private static final MARGIN_TOP_LINE:F = 133.0f

.field private static TEMP_TV_WIDTH_IN_DP:F = 0.0f

.field private static final WIDTH_ALL:F = 867.0f

.field private static WIDTH_ALL_IN_DP:F


# instance fields
.field private humidityTV:[Landroid/widget/TextView;

.field private leftTemp:F

.field private leftTempPrev:F

.field private lowHum:F

.field private lowHumPrev:F

.field private mAppContext:Landroid/content/Context;

.field private mContext:Landroid/content/Context;

.field private rightTemp:F

.field private rightTempPrev:F

.field private stroke:Landroid/view/View;

.field private strokeNoAnime:Landroid/view/View;

.field private summer:Landroid/view/View;

.field private temperatureTV:[Landroid/widget/TextView;

.field private topHum:F

.field private topHumPrev:F

.field private winter:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    .line 24
    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_TOP_ALL_IN_DP:F

    .line 28
    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    .line 29
    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_LEFT_ALL_IN_DP:F

    .line 34
    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HUM_TV_HEIGHT_IN_DP:F

    .line 35
    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->TEMP_TV_WIDTH_IN_DP:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v3, 0x42b40000    # 90.0f

    const/high16 v2, 0x42700000    # 60.0f

    const/high16 v1, 0x41800000    # 16.0f

    const/high16 v0, -0x3e600000    # -20.0f

    .line 102
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 42
    iput v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    .line 44
    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    .line 45
    iput v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    .line 46
    iput v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    .line 47
    iput v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    .line 48
    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    .line 49
    iput v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    .line 103
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->init(Landroid/content/Context;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v3, 0x42b40000    # 90.0f

    const/high16 v2, 0x42700000    # 60.0f

    const/high16 v1, 0x41800000    # 16.0f

    const/high16 v0, -0x3e600000    # -20.0f

    .line 107
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    iput v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    .line 44
    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    .line 45
    iput v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    .line 46
    iput v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    .line 47
    iput v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    .line 48
    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    .line 49
    iput v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->init(Landroid/content/Context;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v3, 0x42b40000    # 90.0f

    const/high16 v2, 0x42700000    # 60.0f

    const/high16 v1, 0x41800000    # 16.0f

    const/high16 v0, -0x3e600000    # -20.0f

    .line 112
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    iput v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    .line 43
    iput v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    .line 44
    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    .line 45
    iput v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    .line 46
    iput v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    .line 47
    iput v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    .line 48
    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    .line 49
    iput v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    .line 113
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->init(Landroid/content/Context;)V

    .line 114
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->strokeNoAnime:Landroid/view/View;

    return-object v0
.end method

.method private convertHumToPX(F)F
    .locals 5
    .param p1, "hum"    # F

    .prologue
    const/high16 v4, 0x428c0000    # 70.0f

    const/high16 v2, 0x41f00000    # 30.0f

    const/high16 v1, 0x41800000    # 16.0f

    const/high16 v3, 0x44070000    # 540.0f

    .line 86
    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    .line 87
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    .line 98
    :goto_0
    return v0

    .line 89
    :cond_0
    const/high16 v0, 0x42b40000    # 90.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 90
    const/4 v0, 0x0

    goto :goto_0

    .line 92
    :cond_1
    cmpg-float v0, p1, v2

    if-gez v0, :cond_2

    .line 93
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    div-float/2addr v0, v3

    sub-float v1, p1, v1

    const/high16 v2, -0x3cf10000    # -143.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x41600000    # 14.0f

    div-float/2addr v1, v2

    add-float/2addr v1, v3

    mul-float/2addr v0, v1

    goto :goto_0

    .line 95
    :cond_2
    cmpg-float v0, p1, v4

    if-gez v0, :cond_3

    .line 96
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    div-float/2addr v0, v3

    sub-float v1, p1, v2

    const v2, -0x3f2ccccd    # -6.6f

    mul-float/2addr v1, v2

    const v2, 0x43c68000    # 397.0f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    goto :goto_0

    .line 98
    :cond_3
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    div-float/2addr v0, v3

    sub-float v1, p1, v4

    const v2, -0x3f2b3333    # -6.65f

    mul-float/2addr v1, v2

    const/high16 v2, 0x43050000    # 133.0f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method private convertTempToPX(F)F
    .locals 6
    .param p1, "temp"    # F

    .prologue
    const/high16 v5, 0x430f0000    # 143.0f

    const v1, 0x4458c000    # 867.0f

    .line 55
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureMin()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 56
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    .line 58
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureMax()F

    move-result v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 59
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    goto :goto_0

    .line 61
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureWinterMin()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    .line 62
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    div-float/2addr v0, v1

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureMin()F

    move-result v1

    sub-float v1, p1, v1

    mul-float/2addr v0, v1

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureWinterMin()F

    move-result v1

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureMin()F

    move-result v2

    sub-float/2addr v1, v2

    div-float v1, v5, v1

    mul-float/2addr v0, v1

    goto :goto_0

    .line 65
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureSummerMin()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    .line 66
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    div-float/2addr v0, v1

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureWinterMin()F

    move-result v1

    sub-float v1, p1, v1

    const/high16 v2, 0x43680000    # 232.0f

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureSummerMin()F

    move-result v3

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureWinterMin()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v1, v5

    mul-float/2addr v0, v1

    goto :goto_0

    .line 70
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureWinterMax()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_4

    .line 71
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    div-float/2addr v0, v1

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureSummerMin()F

    move-result v1

    sub-float v1, p1, v1

    const/high16 v2, 0x42c60000    # 99.0f

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureWinterMax()F

    move-result v3

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureSummerMin()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    const v2, 0x43bb8000    # 375.0f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    goto :goto_0

    .line 75
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureSummerMax()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_5

    .line 76
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    div-float/2addr v0, v1

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureWinterMax()F

    move-result v1

    sub-float v1, p1, v1

    const/high16 v2, 0x43640000    # 228.0f

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureSummerMax()F

    move-result v3

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureWinterMax()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    const/high16 v2, 0x43ed0000    # 474.0f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    goto/16 :goto_0

    .line 80
    :cond_5
    sget v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    div-float/2addr v0, v1

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureSummerMax()F

    move-result v1

    sub-float v1, p1, v1

    const/high16 v2, 0x43250000    # 165.0f

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureMax()F

    move-result v3

    invoke-static {}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getComfortLevelTemperatureSummerMax()F

    move-result v4

    sub-float/2addr v3, v4

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    const v2, 0x442f8000    # 702.0f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    goto/16 :goto_0
.end method

.method public static getComfortLevelTemperatureMax()F
    .locals 3

    .prologue
    const/high16 v0, 0x42700000    # 60.0f

    .line 342
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 343
    const-string v1, "C"

    const-string v2, "F"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 346
    :cond_0
    return v0
.end method

.method public static getComfortLevelTemperatureMin()F
    .locals 3

    .prologue
    const/high16 v0, -0x3e600000    # -20.0f

    .line 302
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 303
    const-string v1, "C"

    const-string v2, "F"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 306
    :cond_0
    return v0
.end method

.method public static getComfortLevelTemperatureSummerMax()F
    .locals 3

    .prologue
    const/high16 v0, 0x41d00000    # 26.0f

    .line 334
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 335
    const-string v1, "C"

    const-string v2, "F"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 338
    :cond_0
    return v0
.end method

.method public static getComfortLevelTemperatureSummerMin()F
    .locals 3

    .prologue
    const/high16 v0, 0x41b80000    # 23.0f

    .line 326
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    const-string v1, "C"

    const-string v2, "F"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 330
    :cond_0
    return v0
.end method

.method public static getComfortLevelTemperatureWinterMax()F
    .locals 3

    .prologue
    const/high16 v0, 0x41c00000    # 24.0f

    .line 318
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    const-string v1, "C"

    const-string v2, "F"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 322
    :cond_0
    return v0
.end method

.method public static getComfortLevelTemperatureWinterMin()F
    .locals 3

    .prologue
    const/high16 v0, 0x41a00000    # 20.0f

    .line 310
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 311
    const-string v1, "C"

    const-string v2, "F"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    .line 314
    :cond_0
    return v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mContext:Landroid/content/Context;

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030094

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 120
    const v1, 0x7f0802c4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 121
    .local v0, "mainGraph":Landroid/view/View;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->initValues()V

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->initHumTV()V

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->initTempTV()V

    .line 124
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->initMainGraph()V

    .line 125
    return-void
.end method

.method private initHumTV()V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 191
    const/4 v1, 0x3

    new-array v1, v1, [Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->humidityTV:[Landroid/widget/TextView;

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->humidityTV:[Landroid/widget/TextView;

    const/4 v3, 0x0

    const v1, 0x7f0802bc

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    .line 193
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->humidityTV:[Landroid/widget/TextView;

    const v1, 0x7f0802bd

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v4

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->humidityTV:[Landroid/widget/TextView;

    aget-object v1, v1, v4

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 195
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_TOP_ALL_IN_DP:F

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    const v3, 0x3e7c3519

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HUM_TV_HEIGHT_IN_DP:F

    div-float/2addr v2, v6

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->humidityTV:[Landroid/widget/TextView;

    aget-object v1, v1, v4

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->humidityTV:[Landroid/widget/TextView;

    const v1, 0x7f0802be

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v5

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->humidityTV:[Landroid/widget/TextView;

    aget-object v1, v1, v5

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 199
    .restart local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_TOP_ALL_IN_DP:F

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    const v3, 0x3f3c3519

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HUM_TV_HEIGHT_IN_DP:F

    div-float/2addr v2, v6

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->humidityTV:[Landroid/widget/TextView;

    aget-object v1, v1, v5

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 201
    return-void
.end method

.method private initMainGraph()V
    .locals 5

    .prologue
    const v4, 0x3efa4fa5

    const v3, 0x3e7c3519

    .line 137
    const v1, 0x7f0802c5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->summer:Landroid/view/View;

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->summer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 139
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 140
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    const v2, 0x3e28e521

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 141
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 142
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    const v2, 0x3ec3782d

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->summer:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    const v1, 0x7f0802c6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->winter:Landroid/view/View;

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->winter:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 146
    .restart local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 147
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    const v2, 0x3edd740b

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 148
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 149
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    const v2, 0x3ec11b76

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->winter:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 151
    const v1, 0x7f0802c7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->stroke:Landroid/view/View;

    .line 152
    const v1, 0x7f0802c8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->strokeNoAnime:Landroid/view/View;

    .line 153
    return-void
.end method

.method private initTempTV()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 156
    const/4 v1, 0x5

    new-array v1, v1, [Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    const v1, 0x7f0802bf

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v5

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v5

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 159
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_LEFT_ALL_IN_DP:F

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    const v3, 0x3e28e521

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->TEMP_TV_WIDTH_IN_DP:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v5

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    const v1, 0x7f0802c0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v6

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v6

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 163
    .restart local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_LEFT_ALL_IN_DP:F

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    const v3, 0x3edd740b

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->TEMP_TV_WIDTH_IN_DP:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v6

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    const v1, 0x7f0802c1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v7

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v7

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 167
    .restart local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_LEFT_ALL_IN_DP:F

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    const v3, 0x3f0bf55f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->TEMP_TV_WIDTH_IN_DP:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v7

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    const v1, 0x7f0802c2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v8

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v8

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 171
    .restart local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    sget v1, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_LEFT_ALL_IN_DP:F

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    const v3, 0x3f4f47c0

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    sget v2, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->TEMP_TV_WIDTH_IN_DP:F

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    const/4 v3, 0x4

    const v1, 0x7f0802c3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    .line 174
    sget-object v1, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHCache;->infoTemperatureUnit:Ljava/lang/String;

    const-string v2, "F"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v5

    const/high16 v2, 0x41a00000    # 20.0f

    const-string v3, "C"

    const-string v4, "F"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v6

    const/high16 v2, 0x41b80000    # 23.0f

    const-string v3, "C"

    const-string v4, "F"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v7

    const/high16 v2, 0x41c00000    # 24.0f

    const-string v3, "C"

    const-string v4, "F"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    aget-object v1, v1, v8

    const/high16 v2, 0x41d00000    # 26.0f

    const-string v3, "C"

    const-string v4, "F"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/thermohygrometer/utils/TGHUnitUtils;->convertTemperatureData(FLjava/lang/String;Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900ce

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->temperatureTV:[Landroid/widget/TextView;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900cd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private initValues()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a0e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a12

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_TOP_ALL_IN_DP:F

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a10

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a11

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->MARGIN_LEFT_ALL_IN_DP:F

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a13

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HUM_TV_HEIGHT_IN_DP:F

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a14

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->TEMP_TV_WIDTH_IN_DP:F

    .line 134
    return-void
.end method

.method private startAnimation()V
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->startAnimation(Z)V

    .line 257
    return-void
.end method

.method private startAnimation(Z)V
    .locals 20
    .param p1, "first"    # Z

    .prologue
    .line 260
    new-instance v11, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x1

    invoke-direct {v11, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 261
    .local v11, "animationSet":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->convertHumToPX(F)F

    move-result v10

    .line 262
    .local v10, "topHumPX":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->convertHumToPX(F)F

    move-result v12

    .line 263
    .local v12, "lowHumPX":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->convertTempToPX(F)F

    move-result v6

    .line 264
    .local v6, "leftTempPX":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->convertTempToPX(F)F

    move-result v15

    .line 265
    .local v15, "rightTempPX":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->convertHumToPX(F)F

    move-result v8

    .line 266
    .local v8, "topHumPrevPX":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->convertHumToPX(F)F

    move-result v13

    .line 267
    .local v13, "lowHumPrevPX":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->convertTempToPX(F)F

    move-result v4

    .line 268
    .local v4, "leftTempPrevPX":F
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->convertTempToPX(F)F

    move-result v16

    .line 269
    .local v16, "rightTempPrevPX":F
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->strokeNoAnime:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/RelativeLayout$LayoutParams;

    .line 270
    .local v14, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    float-to-int v3, v10

    iput v3, v14, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 271
    float-to-int v3, v6

    iput v3, v14, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 272
    sub-float v3, v12, v10

    float-to-int v3, v3

    iput v3, v14, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 273
    sub-float v3, v15, v6

    float-to-int v3, v3

    iput v3, v14, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 274
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->strokeNoAnime:Landroid/view/View;

    invoke-virtual {v3, v14}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 275
    new-instance v17, Landroid/view/animation/ScaleAnimation;

    sub-float v3, v16, v4

    sget v5, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    div-float/2addr v3, v5

    sub-float v5, v15, v6

    sget v7, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->WIDTH_ALL_IN_DP:F

    div-float/2addr v5, v7

    sub-float v7, v13, v8

    sget v9, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    div-float/2addr v7, v9

    sub-float v9, v12, v10

    sget v18, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->HEIGHT_ALL_IN_DP:F

    div-float v9, v9, v18

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v5, v7, v9}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    .line 277
    .local v17, "scaleAnimation":Landroid/view/animation/ScaleAnimation;
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 279
    .local v2, "translateAnimation":Landroid/view/animation/TranslateAnimation;
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 280
    invoke-virtual {v11, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 281
    const/4 v3, 0x1

    invoke-virtual {v11, v3}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 282
    if-eqz p1, :cond_0

    const-wide/16 v18, 0x1f4

    :goto_0
    move-wide/from16 v0, v18

    invoke-virtual {v11, v0, v1}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 283
    new-instance v3, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;)V

    invoke-virtual {v11, v3}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->stroke:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 299
    return-void

    .line 282
    :cond_0
    const-wide/16 v18, 0xc8

    goto :goto_0
.end method


# virtual methods
.method public setData(FFFF)V
    .locals 1
    .param p1, "topHum"    # F
    .param p2, "lowHum"    # F
    .param p3, "leftTemp"    # F
    .param p4, "rightTemp"    # F

    .prologue
    .line 206
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    .line 207
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    .line 208
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    .line 209
    iput p2, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    .line 210
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    .line 211
    iput p3, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    .line 212
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    .line 213
    iput p4, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    .line 214
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->startAnimation(Z)V

    .line 215
    return-void
.end method

.method public setLeftTemp(F)V
    .locals 1
    .param p1, "leftTemp"    # F

    .prologue
    .line 237
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    .line 238
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    .line 239
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    .line 240
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    .line 241
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    .line 242
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->startAnimation()V

    .line 243
    return-void
.end method

.method public setLowHum(F)V
    .locals 1
    .param p1, "lowHum"    # F

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    .line 228
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    .line 229
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    .line 230
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    .line 231
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    .line 232
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->startAnimation()V

    .line 233
    return-void
.end method

.method public setRightTemp(F)V
    .locals 1
    .param p1, "rightTemp"    # F

    .prologue
    .line 247
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    .line 248
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    .line 249
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    .line 250
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    .line 251
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    .line 252
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->startAnimation()V

    .line 253
    return-void
.end method

.method public setTopHum(F)V
    .locals 1
    .param p1, "topHum"    # F

    .prologue
    .line 218
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHumPrev:F

    .line 219
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHum:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->lowHumPrev:F

    .line 220
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->leftTempPrev:F

    .line 221
    iget v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTemp:F

    iput v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->rightTempPrev:F

    .line 222
    iput p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->topHum:F

    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/shealth/thermohygrometer/ComfortLevelAreaView;->startAnimation()V

    .line 224
    return-void
.end method
