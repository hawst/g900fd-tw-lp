.class Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;
.super Landroid/view/View;
.source "ControllerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GradationView"
.end annotation


# static fields
.field public static final NONE_CHANGE:I = -0x2710

.field public static final NUMBER_FORMAT_STRING:Ljava/lang/String; = "###"


# instance fields
.field private beforeX:F

.field private beforeY:F

.field private isEnabledNormalRange:Z

.field private isTouchMoving:Z

.field private mBetweenDistanceGradationBar:I

.field private mBigGradationHeight:I

.field private mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

.field private mGradationArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;",
            ">;"
        }
    .end annotation
.end field

.field private mInterval:I

.field private mMaxInputRange:I

.field private mMaxNormalRange:I

.field private mMaxNormalRangeIndex:I

.field private mMinInputRange:I

.field private mMinNormalRange:I

.field private mMinNormalRangeIndex:I

.field private mNumberDecimalFormat:Ljava/text/DecimalFormat;

.field private mPaint:Landroid/graphics/Paint;

.field private mSmallGradationHeight:I

.field private mTextHalfHeight:I

.field private mTextSize:I

.field private mThickGradationWeight:I

.field private mThinGradationWeight:I

.field private minChangedMovement:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 153
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    .line 154
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 140
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    .line 147
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isEnabledNormalRange:Z

    .line 151
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    .line 155
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->initView()V

    .line 156
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 158
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    .line 159
    invoke-direct {p0, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 140
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    .line 147
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isEnabledNormalRange:Z

    .line 151
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->initView()V

    .line 161
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 163
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    .line 164
    invoke-direct {p0, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 140
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    .line 147
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isEnabledNormalRange:Z

    .line 151
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    .line 165
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->initView()V

    .line 166
    return-void
.end method

.method private dispatchTouchEventHorizontalLayout(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x3

    .line 501
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 527
    :goto_0
    :pswitch_0
    return-void

    .line 503
    :pswitch_1
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    .line 504
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->beforeX:F

    .line 505
    const/16 v6, 0x2710

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->minChangedMovement:I

    goto :goto_0

    .line 508
    :pswitch_2
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    .line 509
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 511
    .local v0, "afterX":F
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->beforeX:F

    sub-float v6, v0, v6

    float-to-int v5, v6

    .line 512
    .local v5, "offsetMovement":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getValue()I

    move-result v2

    .line 513
    .local v2, "getValue":I
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinInputRange:I

    if-gt v2, v6, :cond_1

    if-ltz v5, :cond_1

    .line 514
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->setAction(I)V

    .line 523
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->invalidate()V

    .line 524
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->beforeX:F

    goto :goto_0

    .line 515
    :cond_1
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxInputRange:I

    if-lt v2, v6, :cond_2

    if-gtz v5, :cond_2

    .line 516
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_1

    .line 518
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 519
    .local v4, "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v6

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->beforeX:F

    sub-float v7, v0, v7

    float-to-int v7, v7

    add-int v1, v6, v7

    .line 520
    .local v1, "changedMovement":I
    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->setOffsetMovement(I)V

    goto :goto_2

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private dispatchTouchEventVerticalLayout(Landroid/view/MotionEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x3

    .line 530
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 556
    :goto_0
    :pswitch_0
    return-void

    .line 532
    :pswitch_1
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    .line 533
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->beforeY:F

    .line 534
    const/16 v6, 0x2710

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->minChangedMovement:I

    goto :goto_0

    .line 537
    :pswitch_2
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    .line 538
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    .line 540
    .local v0, "afterY":F
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->beforeY:F

    sub-float v6, v0, v6

    float-to-int v5, v6

    .line 541
    .local v5, "offsetMovement":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getValue()I

    move-result v2

    .line 542
    .local v2, "getValue":I
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinInputRange:I

    if-gt v2, v6, :cond_1

    if-ltz v5, :cond_1

    .line 543
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->setAction(I)V

    .line 552
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->invalidate()V

    .line 553
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iput v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->beforeY:F

    goto :goto_0

    .line 544
    :cond_1
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxInputRange:I

    if-lt v2, v6, :cond_2

    if-gtz v5, :cond_2

    .line 545
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_1

    .line 547
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 548
    .local v4, "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v6

    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->beforeY:F

    sub-float v7, v0, v7

    float-to-int v7, v7

    add-int v1, v6, v7

    .line 549
    .local v1, "changedMovement":I
    invoke-virtual {v4, v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->setOffsetMovement(I)V

    goto :goto_2

    .line 530
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getViewCenterXPosition()I
    .locals 1

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private getViewCenterYPosition()I
    .locals 1

    .prologue
    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private initView()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a3e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mSmallGradationHeight:I

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0a3f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mBigGradationHeight:I

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThinGradationWeight:I

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThickGradationWeight:I

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0106

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mTextSize:I

    .line 176
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mTextHalfHeight:I

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0208df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeY(Landroid/graphics/Shader$TileMode;)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    sget-object v1, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeX(Landroid/graphics/Shader$TileMode;)V

    .line 188
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinInputRange:I

    .line 189
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxInputRange:I

    .line 190
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mInterval:I

    .line 191
    const/16 v0, 0x1f4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setValue(I)V

    .line 193
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setOrientation(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;)V

    .line 194
    return-void
.end method

.method private onDrawHorizontal(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 218
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isEnabledNormalRange:Z

    if-eqz v0, :cond_3

    .line 219
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    if-ltz v0, :cond_1

    .line 220
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterXPosition()I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v0

    add-int/2addr v0, v2

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mBetweenDistanceGradationBar:I

    add-int/2addr v2, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isThick()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThickGradationWeight:I

    :goto_0
    add-int v10, v2, v0

    .line 222
    .local v10, "normalRangeRightPos":I
    new-instance v12, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredWidth()I

    move-result v0

    if-lt v10, v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredWidth()I

    move-result v10

    .end local v10    # "normalRangeRightPos":I
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v0

    invoke-direct {v12, v1, v1, v10, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 228
    .local v12, "rect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v12}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 232
    .end local v12    # "rect":Landroid/graphics/Rect;
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    if-ltz v0, :cond_3

    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterXPosition()I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v0

    add-int v9, v2, v0

    .line 234
    .local v9, "normalRangeLeftPos":I
    new-instance v12, Landroid/graphics/Rect;

    if-gtz v9, :cond_2

    move v9, v1

    .end local v9    # "normalRangeLeftPos":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v2

    invoke-direct {v12, v9, v1, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 240
    .restart local v12    # "rect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v12}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 245
    .end local v12    # "rect":Landroid/graphics/Rect;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 248
    .local v11, "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isBig()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isThick()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 249
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setThickGradationBar()V

    .line 250
    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mBigGradationHeight:I

    .line 252
    .local v7, "heightOffset":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$100(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)I

    move-result v1

    sub-int/2addr v0, v1

    sub-int/2addr v0, v7

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$100(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v1, v7

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mTextHalfHeight:I

    sub-int v6, v0, v1

    .line 253
    .local v6, "Y":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterXPosition()I

    move-result v1

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v2, v6

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 262
    .end local v6    # "Y":I
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterXPosition()I

    move-result v0

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$100(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v2, v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterXPosition()I

    move-result v0

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v3

    add-int/2addr v0, v3

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v0, v7

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$100(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 220
    .end local v7    # "heightOffset":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v11    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_4
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThinGradationWeight:I

    goto/16 :goto_0

    .line 254
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v11    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_5
    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isBig()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isThick()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setThickGradationBar()V

    .line 256
    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mSmallGradationHeight:I

    .restart local v7    # "heightOffset":I
    goto :goto_2

    .line 258
    .end local v7    # "heightOffset":I
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setThinGradationBar()V

    .line 259
    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mSmallGradationHeight:I

    .restart local v7    # "heightOffset":I
    goto :goto_2

    .line 270
    .end local v7    # "heightOffset":I
    .end local v11    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_7
    return-void
.end method

.method private onDrawVertical(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 274
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isEnabledNormalRange:Z

    if-eqz v0, :cond_3

    .line 275
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    if-ltz v0, :cond_1

    .line 276
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterYPosition()I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v0

    add-int/2addr v0, v2

    iget v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mBetweenDistanceGradationBar:I

    add-int/2addr v2, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isThick()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThickGradationWeight:I

    :goto_0
    add-int v9, v2, v0

    .line 278
    .local v9, "normalRangeBottomPos":I
    new-instance v12, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v2

    if-lt v9, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v9

    .end local v9    # "normalRangeBottomPos":I
    :cond_0
    invoke-direct {v12, v1, v1, v0, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 284
    .local v12, "rect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v12}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 288
    .end local v12    # "rect":Landroid/graphics/Rect;
    :cond_1
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    if-ltz v0, :cond_3

    .line 289
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterYPosition()I

    move-result v2

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v0

    add-int v10, v2, v0

    .line 290
    .local v10, "normalRangeTopPos":I
    new-instance v12, Landroid/graphics/Rect;

    if-gtz v10, :cond_2

    move v10, v1

    .end local v10    # "normalRangeTopPos":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredHeight()I

    move-result v2

    invoke-direct {v12, v1, v10, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 296
    .restart local v12    # "rect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v12}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mDrawableOrange:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 301
    .end local v12    # "rect":Landroid/graphics/Rect;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 304
    .local v11, "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isBig()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isThick()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 305
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setThickGradationBar()V

    .line 306
    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mBigGradationHeight:I

    .line 308
    .local v7, "heightOffset":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$100(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)I

    move-result v1

    sub-int/2addr v0, v1

    sub-int/2addr v0, v7

    div-int/lit8 v0, v0, 0x2

    add-int v6, v7, v0

    .line 309
    .local v6, "X":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mNumberDecimalFormat:Ljava/text/DecimalFormat;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v6

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterYPosition()I

    move-result v2

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mTextHalfHeight:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 318
    .end local v6    # "X":I
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$100(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)I

    move-result v0

    int-to-float v1, v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterYPosition()I

    move-result v0

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutHalfMargin:I
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$100(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)I

    move-result v0

    add-int/2addr v0, v7

    int-to-float v3, v0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getViewCenterYPosition()I

    move-result v0

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 276
    .end local v7    # "heightOffset":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v11    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_4
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThinGradationWeight:I

    goto/16 :goto_0

    .line 310
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v11    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_5
    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isBig()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->isThick()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 311
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setThickGradationBar()V

    .line 312
    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mSmallGradationHeight:I

    .restart local v7    # "heightOffset":I
    goto :goto_2

    .line 314
    .end local v7    # "heightOffset":I
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setThinGradationBar()V

    .line 315
    iget v7, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mSmallGradationHeight:I

    .restart local v7    # "heightOffset":I
    goto :goto_2

    .line 326
    .end local v7    # "heightOffset":I
    .end local v11    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_7
    return-void
.end method

.method private setThickGradationBar()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThickGradationWeight:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 334
    return-void
.end method

.method private setThinGradationBar()V
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThinGradationWeight:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 330
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 445
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 446
    .local v0, "GradationViewBounds":Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getHitRect(Landroid/graphics/Rect;)V

    .line 448
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isEnabled()Z

    move-result v8

    if-nez v8, :cond_0

    .line 497
    :goto_0
    return v6

    .line 451
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-nez v8, :cond_1

    .line 452
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->setAction(I)V

    .line 455
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;
    invoke-static {v8}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$000(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 456
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->dispatchTouchEventHorizontalLayout(Landroid/view/MotionEvent;)V

    .line 461
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 480
    :goto_2
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$200(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 481
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getValue()I

    move-result v3

    .line 483
    .local v3, "getValue":I
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinInputRange:I

    if-gt v3, v6, :cond_8

    .line 484
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinInputRange:I

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setValue(I)V

    .line 485
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinInputRange:I

    .line 491
    :cond_2
    :goto_3
    const/16 v6, -0x2710

    if-eq v3, v6, :cond_3

    .line 492
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mOnValueChangedListener:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$200(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;

    move-result-object v6

    int-to-float v8, v3

    const/high16 v9, 0x41200000    # 10.0f

    div-float/2addr v8, v9

    invoke-interface {v6, v8}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$OnValueChangedListener;->onValueChanged(F)V

    .line 496
    .end local v3    # "getValue":I
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->requestDisallowInterceptTouchEvent(Z)V

    move v6, v7

    .line 497
    goto :goto_0

    .line 458
    :cond_4
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->dispatchTouchEventVerticalLayout(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 464
    :pswitch_1
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    .line 465
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 466
    .local v5, "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v2

    .line 467
    .local v2, "changedMovement":I
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->minChangedMovement:I

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 468
    .local v1, "absMinMovement":I
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-ne v1, v6, :cond_5

    .line 469
    iput v2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->minChangedMovement:I

    goto :goto_4

    .line 473
    .end local v1    # "absMinMovement":I
    .end local v2    # "changedMovement":I
    .end local v5    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 474
    .restart local v5    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v6

    iget v8, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->minChangedMovement:I

    sub-int/2addr v6, v8

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->setOffsetMovement(I)V

    goto :goto_5

    .line 476
    .end local v5    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->invalidate()V

    goto/16 :goto_2

    .line 486
    .end local v4    # "i$":Ljava/util/Iterator;
    .restart local v3    # "getValue":I
    :cond_8
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxInputRange:I

    if-lt v3, v6, :cond_2

    .line 487
    iget v6, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxInputRange:I

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setValue(I)V

    .line 488
    iget v3, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxInputRange:I

    goto :goto_3

    .line 461
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getInterval()I
    .locals 1

    .prologue
    .line 422
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mInterval:I

    return v0
.end method

.method public getValue()I
    .locals 7

    .prologue
    .line 345
    const/16 v4, -0x2710

    .line 347
    .local v4, "result":I
    const/16 v5, 0x2710

    iput v5, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->minChangedMovement:I

    .line 348
    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 349
    .local v3, "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v1

    .line 350
    .local v1, "changedMovement":I
    iget v5, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->minChangedMovement:I

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 351
    .local v0, "absMinMovement":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-ne v0, v5, :cond_0

    .line 352
    iput v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->minChangedMovement:I

    .line 353
    invoke-virtual {v3}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getValue()F

    move-result v5

    const/high16 v6, 0x41200000    # 10.0f

    mul-float/2addr v5, v6

    float-to-int v4, v5

    goto :goto_0

    .line 357
    .end local v0    # "absMinMovement":I
    .end local v1    # "changedMovement":I
    .end local v3    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_1
    return v4
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v0, 0xff

    .line 208
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->mLayoutType:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;
    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;->access$000(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;)Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->onDrawHorizontal(Landroid/graphics/Canvas;)V

    .line 214
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->onDrawVertical(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public setInputRange(II)V
    .locals 1
    .param p1, "minInputRange"    # I
    .param p2, "maxInputRange"    # I

    .prologue
    .line 426
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinInputRange:I

    .line 427
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxInputRange:I

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setValue(I)V

    .line 429
    return-void
.end method

.method public setInterval(I)V
    .locals 1
    .param p1, "interval"    # I

    .prologue
    .line 417
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mInterval:I

    .line 418
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setValue(I)V

    .line 419
    return-void
.end method

.method public setNormalRange(II)V
    .locals 1
    .param p1, "minNormalRange"    # I
    .param p2, "maxNormalRange"    # I

    .prologue
    .line 432
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isEnabledNormalRange:Z

    .line 433
    iput p1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRange:I

    .line 434
    iput p2, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRange:I

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->setValue(I)V

    .line 436
    return-void
.end method

.method public setOrientation(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;)V
    .locals 2
    .param p1, "layoutType"    # Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    .prologue
    .line 197
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$LayoutType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mBetweenDistanceGradationBar:I

    .line 203
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->invalidate()V

    .line 204
    return-void

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mBetweenDistanceGradationBar:I

    goto :goto_0
.end method

.method public setValue(I)V
    .locals 14
    .param p1, "value"    # I

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->isTouchMoving:Z

    if-eqz v0, :cond_0

    .line 414
    :goto_0
    return-void

    .line 362
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    .line 363
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    .line 365
    const/4 v9, 0x0

    .line 367
    .local v9, "nonBigandThickLineValue":I
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mInterval:I

    mul-int/lit8 v0, v0, 0xa

    rem-int v0, p1, v0

    if-eqz v0, :cond_1

    .line 368
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mInterval:I

    mul-int/lit8 v0, v0, 0xa

    rem-int v9, p1, v0

    .line 369
    sub-int/2addr p1, v9

    .line 372
    :cond_1
    const/4 v4, 0x0

    .line 373
    .local v4, "isBig":Z
    const/4 v5, 0x0

    .line 374
    .local v5, "isThick":Z
    const/4 v3, 0x0

    .line 375
    .local v3, "distanceMovement":I
    const/4 v11, -0x5

    .local v11, "tensPlace":I
    :goto_1
    const/4 v0, 0x5

    if-ge v11, v0, :cond_8

    .line 376
    const/4 v12, 0x0

    .local v12, "unitsPlace":I
    :goto_2
    const/16 v0, 0xa

    if-ge v12, v0, :cond_7

    .line 377
    if-nez v12, :cond_3

    .line 378
    const/4 v4, 0x1

    .line 379
    const/4 v5, 0x1

    .line 388
    :goto_3
    int-to-float v0, p1

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    int-to-float v1, v11

    const/high16 v13, 0x41200000    # 10.0f

    mul-float/2addr v1, v13

    int-to-float v13, v12

    add-float/2addr v1, v13

    iget v13, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mInterval:I

    int-to-float v13, v13

    mul-float/2addr v1, v13

    const/high16 v13, 0x41200000    # 10.0f

    div-float/2addr v1, v13

    add-float v2, v0, v1

    .line 389
    .local v2, "gradationValue":F
    iget-object v13, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->this$0:Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;-><init>(Lcom/sec/android/app/shealth/walkingmate/input/ControllerView;FIZZ)V

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRange:I

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    cmpg-float v0, v2, v0

    if-gez v0, :cond_5

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMinNormalRangeIndex:I

    .line 397
    :cond_2
    :goto_4
    if-eqz v5, :cond_6

    .line 398
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThickGradationWeight:I

    add-int/2addr v3, v0

    .line 403
    :goto_5
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mBetweenDistanceGradationBar:I

    add-int/2addr v3, v0

    .line 376
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 380
    .end local v2    # "gradationValue":F
    :cond_3
    const/4 v0, 0x5

    if-ne v12, v0, :cond_4

    .line 381
    const/4 v4, 0x0

    .line 382
    const/4 v5, 0x1

    goto :goto_3

    .line 384
    :cond_4
    const/4 v4, 0x0

    .line 385
    const/4 v5, 0x0

    goto :goto_3

    .line 393
    .restart local v2    # "gradationValue":F
    :cond_5
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRange:I

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    cmpl-float v0, v2, v0

    if-lez v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mMaxNormalRangeIndex:I

    goto :goto_4

    .line 400
    :cond_6
    iget v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mThinGradationWeight:I

    add-int/2addr v3, v0

    goto :goto_5

    .line 375
    .end local v2    # "gradationValue":F
    :cond_7
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 407
    .end local v12    # "unitsPlace":I
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget v13, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mInterval:I

    div-int v13, v9, v13

    add-int/2addr v1, v13

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 408
    .local v7, "middleObject":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v7}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v8

    .line 409
    .local v8, "middleObjectMovementPosition":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->mGradationArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;

    .line 410
    .local v10, "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    invoke-virtual {v10}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->getOffsetMovement()I

    move-result v0

    sub-int/2addr v0, v8

    invoke-virtual {v10, v0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;->setOffsetMovement(I)V

    goto :goto_6

    .line 413
    .end local v10    # "object":Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationObject;
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/input/ControllerView$GradationView;->invalidate()V

    goto/16 :goto_0
.end method
