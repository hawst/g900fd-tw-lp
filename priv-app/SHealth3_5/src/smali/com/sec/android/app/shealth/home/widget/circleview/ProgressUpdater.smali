.class public abstract Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;
.super Ljava/lang/Object;
.source "ProgressUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;
    }
.end annotation


# instance fields
.field private counter:I

.field private final delay:I

.field private final messagesCount:I


# direct methods
.method protected constructor <init>(II)V
    .locals 1
    .param p1, "delay"    # I
    .param p2, "messagesCount"    # I

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->counter:I

    .line 30
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->delay:I

    .line 31
    iput p2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->messagesCount:I

    .line 32
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->counter:I

    return v0
.end method

.method static synthetic access$108(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->counter:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->counter:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->messagesCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;->delay:I

    return v0
.end method


# virtual methods
.method protected abstract onProgressUpdate(F)V
.end method

.method public start()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$TimerHandler;-><init>(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;)V

    .line 36
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater$1;-><init>(Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 50
    return-void
.end method
