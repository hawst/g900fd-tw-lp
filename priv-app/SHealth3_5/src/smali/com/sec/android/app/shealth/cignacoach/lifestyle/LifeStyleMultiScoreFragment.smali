.class public Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;
.super Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;
.source "LifeStyleMultiScoreFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final HOURS_IN_DAY:B = 0x18t

.field private static final MARKING_COUNT:I = 0xc

.field private static final MARKING_INTERVAL:I = 0x1

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MILLIS_IN_YEAR:J = 0x77bbdb000L

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private datas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field

.field private mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

.field private mDensity:F

.field private mGraphType:I

.field private mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

.field private mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

.field private mLifeStyleSchartTimeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

.field private mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

.field private mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

.field private mXAxisDateFormat:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;-><init>()V

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->datas:Ljava/util/List;

    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mXAxisDateFormat:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 97
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 478
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;)Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    return v0
.end method

.method private getGraphBottomPadding()F
    .locals 3

    .prologue
    .line 682
    const/4 v0, 0x0

    .line 694
    .local v0, "bottomPadding":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 695
    return v0
.end method

.method private getGraphViewHeight()I
    .locals 3

    .prologue
    .line 663
    const/4 v0, 0x0

    .line 665
    .local v0, "chartHeight":I
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getOrientation()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 666
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a01f2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 677
    :goto_0
    return v0

    .line 668
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0215

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto :goto_0
.end method

.method private getHandlerItemTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 468
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 469
    .local v0, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v1, 0x41900000    # 18.0f

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 470
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 471
    const/16 v1, 0xff

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 472
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 473
    const-string v1, "font/Roboto-Light.ttf"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 475
    return-object v0
.end method

.method private getInteraction()Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    .line 237
    new-instance v0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 238
    .local v0, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 239
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 240
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 242
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 243
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 245
    return-object v0
.end method

.method private getOrientation()I
    .locals 2

    .prologue
    .line 657
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 658
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    return v1
.end method

.method private getPopupTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 4

    .prologue
    const/16 v3, 0xff

    .line 458
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 459
    .local v0, "pointTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v1, 0x41700000    # 15.0f

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 460
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 461
    invoke-virtual {v0, v3, v3, v3, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 462
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 464
    return-object v0
.end method

.method private initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 16
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    .line 327
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getXTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v13

    .line 329
    .local v13, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getYTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v14

    .line 331
    .local v14, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/4 v11, 0x0

    .line 332
    .local v11, "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v10, 0x0

    .line 335
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getGraphBottomPadding()F

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 339
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 340
    check-cast v11, Landroid/graphics/drawable/BitmapDrawable;

    .end local v11    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v11}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    .line 341
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 342
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setChartBackgroundColor(I)V

    .line 345
    const/high16 v1, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mDensity:F

    mul-float/2addr v1, v2

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 346
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 347
    const v1, -0xcbb1ec

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 348
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 349
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0216

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 350
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_0

    .line 351
    const-string/jumbo v1, "yyyy"

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    .line 357
    :goto_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 358
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 359
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 360
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 361
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 362
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0216

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextSpace(FI)V

    .line 363
    const/4 v1, 0x4

    new-array v15, v1, [I

    .line 364
    .local v15, "yVisibleidx":[I
    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v15, v1

    .line 365
    const/4 v1, 0x1

    const/16 v2, 0x1e

    aput v2, v15, v1

    .line 366
    const/4 v1, 0x2

    const/16 v2, 0x46

    aput v2, v15, v1

    .line 367
    const/4 v1, 0x3

    const/16 v2, 0x64

    aput v2, v15, v1

    .line 368
    const/4 v1, 0x1

    const/16 v2, 0x65

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v15, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisUseManualMarking(ZI[II)V

    .line 372
    const v1, -0x7f8000

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupStrokeColor(I)V

    .line 373
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getPopupTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 376
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200c8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 377
    .restart local v11    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v11, Landroid/graphics/drawable/BitmapDrawable;

    .end local v11    # "bitmapDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v11}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    .line 378
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupImage(Landroid/graphics/Bitmap;)V

    .line 381
    const/4 v12, 0x0

    .line 382
    .local v12, "bitmapDrawable2":Landroid/graphics/drawable/Drawable;
    const/4 v6, 0x0

    .line 383
    .local v6, "handleItemWidth":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_1

    .line 384
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 385
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0219

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    .line 391
    :goto_1
    check-cast v12, Landroid/graphics/drawable/BitmapDrawable;

    .end local v12    # "bitmapDrawable2":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v12}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 392
    .local v3, "handlerBmp":Landroid/graphics/Bitmap;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v4, v1

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getHandlerItemTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0218

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v7, v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a021a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a021b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v9}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->setHandlerItem(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Landroid/graphics/Bitmap;FLcom/samsung/android/sdk/chart/style/SchartTextStyle;FFFF)V

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mHandlerListener:Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 398
    const-wide/16 v1, 0x1388

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTimeOutDelay(J)V

    .line 399
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v2, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v1, v2, :cond_2

    .line 400
    const-string/jumbo v1, "yyyy"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    .line 406
    :goto_2
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupVisible(Z)V

    .line 407
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupEnable(Z)V

    .line 408
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 409
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 410
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mXAxisDateFormat:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getSeparatorDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorDateFormat(Ljava/lang/String;)V

    .line 411
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 412
    const v1, 0x7f0203c4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->createHandlerLine(I)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 413
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0217

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 414
    return-void

    .line 353
    .end local v3    # "handlerBmp":Landroid/graphics/Bitmap;
    .end local v6    # "handleItemWidth":F
    .end local v15    # "yVisibleidx":[I
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mXAxisDateFormat:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->dateFormatPattern:Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDateFormat(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 387
    .restart local v6    # "handleItemWidth":F
    .restart local v12    # "bitmapDrawable2":Landroid/graphics/drawable/Drawable;
    .restart local v15    # "yVisibleidx":[I
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0203c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 388
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    goto/16 :goto_1

    .line 402
    .end local v12    # "bitmapDrawable2":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "handlerBmp":Landroid/graphics/Bitmap;
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mXAxisDateFormat:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    invoke-static {v1}, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->getGraphParams(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/GraphParams;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/shealth/logutils/graph/GraphParams;->dateFormatPattern:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemDateFormat(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V
    .locals 5
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 282
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;-><init>()V

    .line 284
    .local v0, "seriesStyle":Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;
    const/4 v1, 0x0

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 285
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingVisible(Z)V

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090029

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 287
    const v1, 0x7f0203c0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 288
    const v1, 0x7f0203c1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getCachedBitmapFromResource(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 289
    const v1, -0xa15ace

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineColor(I)V

    .line 290
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setLineThickness(F)V

    .line 292
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setNormalRangeVisible(Z)V

    .line 299
    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/chart/style/SchartLineSeriesStyle;->setGoalLineVisible(Z)V

    .line 322
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 323
    return-void
.end method

.method private setSeriesData()V
    .locals 10

    .prologue
    .line 417
    new-instance v4, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-direct {v4}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .line 418
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->datas:Ljava/util/List;

    if-eqz v4, :cond_0

    .line 419
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->datas:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 421
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->datas:Ljava/util/List;

    .line 423
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getLifeStyleScores()Ljava/util/List;

    move-result-object v1

    .line 425
    .local v1, "lifeStyleScores":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 426
    .local v3, "scoreSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 427
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 428
    .local v2, "lsScore":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->datas:Ljava/util/List;

    new-instance v5, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v8

    int-to-double v8, v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>(JD)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 431
    .end local v2    # "lsScore":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->datas:Ljava/util/List;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->setAllData(Ljava/util/List;)V

    .line 432
    return-void
.end method

.method private updateGraphLayout()V
    .locals 4

    .prologue
    .line 636
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f08036d

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 637
    .local v0, "gerneralView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 638
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getGraphViewHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 640
    :cond_0
    return-void
.end method

.method private updateInfoAreaLayout()V
    .locals 4

    .prologue
    .line 643
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f08036c

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 644
    .local v0, "graphInfo":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 645
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 647
    :cond_0
    return-void
.end method

.method private updateLegendLayout()V
    .locals 5

    .prologue
    .line 650
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f080371

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 651
    .local v0, "legendContainer":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 652
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0214

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 654
    :cond_0
    return-void
.end method

.method private updatePeriodTab()V
    .locals 0

    .prologue
    .line 633
    return-void
.end method


# virtual methods
.method protected createDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;
    .locals 1
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 221
    const/4 v0, 0x0

    return-object v0
.end method

.method protected createHandlerLine(I)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "lineDrawable"    # I

    .prologue
    const/4 v6, 0x0

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0015

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 529
    .local v2, "handlerLineBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 530
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 531
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    invoke-virtual {v1, v6, v6, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 532
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 533
    return-object v2
.end method

.method protected getContentUriList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 754
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getInfoView()Lcom/sec/android/app/shealth/logutils/graph/InformationAreaViewInterLayer;
    .locals 1

    .prologue
    .line 722
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getLegendButtonBackgroundResourceId()I
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x0

    return v0
.end method

.method protected getLegendMarks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/logutils/graph/LegendMark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/sec/android/app/shealth/logutils/graph/LegendMark;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getNoDataIcon()I
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x0

    return v0
.end method

.method public getPeridType()Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    return-object v0
.end method

.method protected getSeparatorDateFormat(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Ljava/lang/String;
    .locals 7
    .param p1, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 726
    const-string v2, "-"

    .line 728
    .local v2, "REGEX_ONE_OR_MORE":Ljava/lang/String;
    const-string v0, "d"

    .line 729
    .local v0, "DAY_CHAR":Ljava/lang/String;
    const-string v1, "M"

    .line 730
    .local v1, "MONTH_CHAR":Ljava/lang/String;
    const-string/jumbo v3, "yyyy"

    .line 732
    .local v3, "YEAR_STRING":Ljava/lang/String;
    const-string v4, ""

    .line 733
    .local v4, "dateFormat":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "date_format"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 734
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v5, :cond_0

    .line 735
    const-string v5, "d"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 736
    const-string v5, "M"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 737
    const-string v5, "--"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 745
    :goto_0
    return-object v4

    .line 738
    :cond_0
    sget-object v5, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne p1, v5, :cond_1

    .line 739
    const-string v5, "dd-"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 740
    const-string v5, "-dd"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 742
    :cond_1
    const-string/jumbo v5, "yyyy-"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 743
    const-string v5, "-yyyy"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getXTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 435
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 436
    .local v0, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v1, 0x41700000    # 15.0f

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 437
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 438
    const/16 v1, 0xff

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 439
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 441
    const-string v1, "font/Roboto-Light.ttf"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 443
    return-object v0
.end method

.method protected getYAxisLabelTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    return-object v0
.end method

.method public getYTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 447
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 448
    .local v0, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v1, 0x41700000    # 15.0f

    iget v2, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 449
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 450
    const/16 v1, 0xff

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 451
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 452
    const-string v1, "font/Roboto-Light.ttf"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->createTypeFaceFromName(Ljava/lang/String;)V

    .line 454
    return-object v0
.end method

.method protected initDateBar()V
    .locals 11

    .prologue
    const v10, 0x7f09020b

    const v9, 0x7f090066

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 117
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->initDateBar()V

    .line 122
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090212

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fb0

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 126
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;

    const v1, 0x7f090068

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setText(I)V

    .line 131
    :cond_0
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;

    if-eqz v0, :cond_1

    .line 134
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->capitalizeFirstLetter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/TabTextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 138
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getTabButtonView(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 139
    :cond_2
    return-void
.end method

.method protected initGraphLegendArea()Landroid/view/View;
    .locals 4

    .prologue
    .line 576
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->updateLegendLayout()V

    .line 578
    const/4 v0, 0x0

    .line 580
    .local v0, "legendView":Landroid/view/View;
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getOrientation()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 581
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f030065

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 582
    const v1, 0x7f0801e4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    .line 584
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 585
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->isTimeToReassess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 586
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 589
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 592
    :cond_1
    return-object v0
.end method

.method protected initGraphView(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 10
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    const/4 v8, 0x5

    const/4 v4, 0x1

    .line 143
    iput-object p3, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_2

    .line 146
    iput v8, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    .line 147
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mXAxisDateFormat:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;->MONTHLY:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getScoreData(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V

    .line 158
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->setSeriesData()V

    .line 160
    new-instance v6, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    invoke-direct {v6, v4, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    .line 161
    .local v6, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 162
    invoke-direct {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->initChartStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;)V

    .line 164
    new-instance v7, Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;-><init>()V

    .line 165
    .local v7, "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->addSeries(Lcom/samsung/android/sdk/chart/series/SchartSeries;)V

    .line 167
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, v6, v7}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mLifeStyleSchartTimeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mLifeStyleSchartTimeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getInteraction()Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 170
    const-wide/16 v2, 0x0

    .line 171
    .local v2, "startTimes":D
    const/16 v5, 0xc

    .line 172
    .local v5, "markingCount":I
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    long-to-double v2, v0

    .line 175
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    if-ne v0, v4, :cond_3

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    const-wide/32 v8, 0x1b77400

    sub-long/2addr v0, v8

    long-to-double v2, v0

    .line 177
    const/16 v5, 0xc

    .line 192
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mLifeStyleSchartTimeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;->setStartVisual(IDII)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mLifeStyleSchartTimeLineChartView:Lcom/samsung/android/sdk/chart/view/SchartTimeLineChartView;

    return-object v0

    .line 151
    .end local v2    # "startTimes":D
    .end local v5    # "markingCount":I
    .end local v6    # "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .end local v7    # "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mPeriodType:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    if-ne v0, v1, :cond_0

    .line 152
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    .line 153
    sget-object v0, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mXAxisDateFormat:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;->YEARLY:Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getScoreData(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V

    goto/16 :goto_0

    .line 179
    .restart local v2    # "startTimes":D
    .restart local v5    # "markingCount":I
    .restart local v6    # "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .restart local v7    # "dataSet":Lcom/samsung/android/sdk/chart/series/SchartDataSet;
    :cond_3
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    const-wide/32 v8, 0x14997000

    sub-long/2addr v0, v8

    long-to-double v2, v0

    .line 181
    const/4 v5, 0x7

    goto :goto_1

    .line 183
    :cond_4
    iget v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    if-ne v0, v8, :cond_5

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    const-wide v8, 0x4fd292000L

    sub-long/2addr v0, v8

    long-to-double v2, v0

    .line 185
    const/16 v5, 0xc

    goto :goto_1

    .line 187
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mSeries:Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    const-wide v8, 0x1673391000L

    sub-long/2addr v0, v8

    long-to-double v2, v0

    .line 188
    const/4 v5, 0x6

    goto/16 :goto_1
.end method

.method protected initInformationArea(JLcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)Landroid/view/View;
    .locals 8
    .param p1, "periodStart"    # J
    .param p3, "type"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 549
    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    .line 551
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getOrientation()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 552
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f08036c

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 553
    .local v0, "informationLayout":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 554
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 566
    .end local v0    # "informationLayout":Landroid/view/View;
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->updatePeriodTab()V

    .line 567
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->updateGraphLayout()V

    .line 568
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->updateInfoAreaLayout()V

    .line 570
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    return-object v3

    .line 558
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getLifeStyleScores()Ljava/util/List;

    move-result-object v2

    .line 559
    .local v2, "lifeStyleScores":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 561
    .local v1, "lifeStyleScore":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getScoreCoachDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->setCoachMessageTxt(Ljava/lang/String;)V

    .line 562
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v6

    iget v7, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mGraphType:I

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->setCurrentScoreInfo(JII)V

    .line 563
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mInfomationAreaView:Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getVarianceMessageMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->getLastScoreDifference()I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/CignaChartInformartionAreaView;->updateComparisonTxt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected initSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;)V
    .locals 0
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "dataSet"    # Lcom/sec/android/app/shealth/logutils/graph/data/ExtendedDataSet;

    .prologue
    .line 209
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 250
    const/4 v0, 0x0

    .line 252
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 278
    :goto_0
    return-void

    .line 254
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getCurrentGoalCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 255
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/reassessment/LifeStyleReassessmentActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 259
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 261
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 262
    .local v1, "remainCategory":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;>;"
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->FOOD:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->EXERCISE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->SLEEP:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->STRESS:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;->WEIGHT:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 269
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/cignacoach/assessment/AssessmentActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 270
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v2, "intent_lifestyle_categorys"

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 252
    nop

    :pswitch_data_0
    .packed-switch 0x7f0801e4
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onCreate(Landroid/os/Bundle;)V

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mDensity:F

    .line 112
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 539
    invoke-super {p0}, Lcom/sec/android/app/shealth/logutils/graph/CustomGraphFragment;->onResume()V

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->initGeneralView()V

    .line 542
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->initInformationArea()V

    .line 543
    return-void
.end method

.method protected setAdditionalChartViewArguments(Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;)V
    .locals 0
    .param p1, "chartView"    # Lcom/samsung/android/sdk/chart/view/SchartTimeMultiChartView;

    .prologue
    .line 606
    return-void
.end method

.method protected setHandlerItem(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Landroid/graphics/Bitmap;FLcom/samsung/android/sdk/chart/style/SchartTextStyle;FFFF)V
    .locals 0
    .param p1, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p2, "handlerImage"    # Landroid/graphics/Bitmap;
    .param p3, "strokeWidth"    # F
    .param p4, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p5, "itemWidth"    # F
    .param p6, "itemHeight"    # F
    .param p7, "itemOffSet"    # F
    .param p8, "itemTextOffset"    # F

    .prologue
    .line 516
    invoke-virtual {p1, p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 517
    invoke-virtual {p1, p3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 518
    invoke-virtual {p1, p4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 519
    invoke-virtual {p1, p5}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 520
    invoke-virtual {p1, p6}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 521
    invoke-virtual {p1, p7}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 522
    invoke-virtual {p1, p8}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextOffset(F)V

    .line 523
    return-void
.end method

.method protected updateLatestMeasureInHourTimeMap(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    .locals 0
    .param p1, "periodType"    # Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    .prologue
    .line 215
    return-void
.end method

.method public updateMultiScoreContent()V
    .locals 2

    .prologue
    .line 699
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getOrientation()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 701
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "updateMultiScoreContent"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 705
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleScoreActivity;->isTimeToReassess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 706
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 709
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/lifestyle/LifeStyleMultiScoreFragment;->mCignaScoreReassessBtnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 712
    :cond_1
    return-void
.end method
