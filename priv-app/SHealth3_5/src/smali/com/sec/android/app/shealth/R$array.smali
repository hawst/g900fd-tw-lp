.class public final Lcom/sec/android/app/shealth/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final age_groups:I = 0x7f0e0028

.field public static final app_actions:I = 0x7f0e0011

.field public static final app_actions_for_gear:I = 0x7f0e0015

.field public static final app_favorite:I = 0x7f0e0012

.field public static final app_favorite_for_gear:I = 0x7f0e0016

.field public static final app_icons:I = 0x7f0e0010

.field public static final app_icons_for_gear:I = 0x7f0e0014

.field public static final app_names:I = 0x7f0e000f

.field public static final app_names_for_gear:I = 0x7f0e0013

.field public static final chart_y_axis_marking:I = 0x7f0e0006

.field public static final cigna_library_favorite_list_filter:I = 0x7f0e0019

.field public static final cigna_spinner_frequency:I = 0x7f0e0021

.field public static final days_of_week:I = 0x7f0e0002

.field public static final days_of_week_short:I = 0x7f0e0003

.field public static final days_of_week_short_one_char:I = 0x7f0e001a

.field public static final font_package:I = 0x7f0e0022

.field public static final font_path:I = 0x7f0e0023

.field public static final food_days_of_week_long_three_char:I = 0x7f0e0026

.field public static final food_gallery_mode_items:I = 0x7f0e0024

.field public static final food_meal_types:I = 0x7f0e0025

.field public static final help_guide_action:I = 0x7f0e0020

.field public static final help_guide_title:I = 0x7f0e001f

.field public static final inactive_time_period:I = 0x7f0e0007

.field public static final months:I = 0x7f0e0001

.field public static final months_short:I = 0x7f0e0000

.field public static final primary_display_item:I = 0x7f0e001b

.field public static final syncFrequency:I = 0x7f0e0017

.field public static final syncFrequencyValues:I = 0x7f0e0018

.field public static final timezone_labels:I = 0x7f0e0004

.field public static final timezone_values:I = 0x7f0e0005

.field public static final unit_blood_glucose:I = 0x7f0e000d

.field public static final unit_distance:I = 0x7f0e000c

.field public static final unit_height:I = 0x7f0e0008

.field public static final unit_temperature:I = 0x7f0e000a

.field public static final unit_water:I = 0x7f0e000e

.field public static final unit_weight:I = 0x7f0e0009

.field public static final week_format_type:I = 0x7f0e000b

.field public static final weight_unit:I = 0x7f0e0027

.field public static final workout_days_of_week_short_one_char:I = 0x7f0e001c

.field public static final workout_primary_display_item:I = 0x7f0e001d

.field public static final workout_primary_display_mi_item:I = 0x7f0e001e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
