.class Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;
.super Ljava/lang/Object;
.source "UvSkinTypeDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0804ee

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinScroll:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$100(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/ScrollView;

    move-result-object v1

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->changeState(Landroid/view/ViewGroup;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$200(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;Landroid/view/ViewGroup;Z)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # invokes: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clearSelection()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$300(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->clearSkinSelection()V

    .line 89
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->mSkinSettings:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$000(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->updateButtonStatus(Z)V

    .line 92
    :cond_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    iget-object v1, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    # getter for: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->skinScroll:Landroid/widget/ScrollView;
    invoke-static {v1}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$100(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;)Landroid/widget/ScrollView;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->changeState(Landroid/view/ViewGroup;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->access$200(Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;Landroid/view/ViewGroup;Z)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog$1;->this$0:Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/uv/UvSkinTypeDialog;->applyOptionsIfSelected()V

    goto :goto_0
.end method
