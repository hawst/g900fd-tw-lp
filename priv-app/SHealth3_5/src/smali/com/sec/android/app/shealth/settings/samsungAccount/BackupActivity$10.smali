.class Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;
.super Ljava/lang/Object;
.source "BackupActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

.field final synthetic val$dialogTag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    iput-object p2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->val$dialogTag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 4
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->val$dialogTag:Ljava/lang/String;

    const-string v1, "backup_progress_popup"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 523
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$11;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 555
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 525
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Cancel is clicked for all work"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->stopBackup()V

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const v1, 0x7f080089

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setClickable(Z)V

    goto :goto_0

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->val$dialogTag:Ljava/lang/String;

    const-string v1, "backup_failed_network"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->mIsNetworkErrorOccured:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$1002(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Z)Z

    .line 539
    sget-object v0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$11;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 541
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;->START:Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;

    # invokes: Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->showBackupPopup(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;->access$100(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity;Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$BackupStatus;)V

    .line 542
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10$1;-><init>(Lcom/sec/android/app/shealth/settings/samsungAccount/BackupActivity$10;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 523
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch

    .line 539
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
