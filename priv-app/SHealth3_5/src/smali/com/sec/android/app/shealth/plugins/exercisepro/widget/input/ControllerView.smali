.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;
.super Landroid/widget/FrameLayout;
.source "ControllerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;
    }
.end annotation


# static fields
.field public static final VALUE_TO_GRADATION_VALUE_RATIO:F = 10.0f


# instance fields
.field private mBackgroundImageView:Landroid/widget/ImageView;

.field private mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method private addBackgroundImage(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 3
    .param p1, "layoutType"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;
    .param p2, "controllerLayoutParams"    # Landroid/widget/FrameLayout$LayoutParams;

    .prologue
    .line 161
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;

    invoke-virtual {p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201ec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 170
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->addView(Landroid/view/View;)V

    .line 171
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mBackgroundImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0208ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private addGradationView(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;Landroid/widget/FrameLayout$LayoutParams;)V
    .locals 2
    .param p1, "layoutType"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;
    .param p2, "controllerLayoutParams"    # Landroid/widget/FrameLayout$LayoutParams;

    .prologue
    .line 146
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/HorizontalGradationView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->addView(Landroid/view/View;)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setOnBackgroundChangedListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnBackgroundChangedListener;)V

    .line 158
    return-void
.end method

.method private getInterval()F
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getInterval()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public getGradationView()Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    return-object v0
.end method

.method public getValue()F
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->getValue()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 109
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setEnabled(Z)V

    .line 111
    return-void
.end method

.method public setInputRange(FF)V
    .locals 3
    .param p1, "minValue"    # F
    .param p2, "maxValue"    # F

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    mul-float v1, p1, v2

    float-to-int v1, v1

    mul-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setInputRange(II)V

    .line 81
    return-void
.end method

.method public setInterval(F)V
    .locals 2
    .param p1, "interval"    # F

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setInterval(I)V

    .line 118
    return-void
.end method

.method public setNormalRange(FF)V
    .locals 3
    .param p1, "lowBound"    # F
    .param p2, "highBound"    # F

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    mul-float v1, p1, v2

    float-to-int v1, v1

    mul-float/2addr v2, p2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setNormalRange(II)V

    .line 91
    return-void
.end method

.method public setOnControllerTapListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setOnControllerTapListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnControllerTapListener;)V

    .line 187
    return-void
.end method

.method public setOnValueChangedListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setOnValueChangedListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView$OnValueChangedListener;)V

    .line 179
    return-void
.end method

.method public setOrientation(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;)V
    .locals 4
    .param p1, "layoutType"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;

    .prologue
    .line 132
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;->HORIZONTAL:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;

    invoke-virtual {p1, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00f0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 141
    .local v0, "controllerLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->addGradationView(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;Landroid/widget/FrameLayout$LayoutParams;)V

    .line 142
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->addBackgroundImage(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView$LayoutType;Landroid/widget/FrameLayout$LayoutParams;)V

    .line 143
    return-void

    .line 137
    .end local v0    # "controllerLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .restart local v0    # "controllerLayoutParams":Landroid/widget/FrameLayout$LayoutParams;
    goto :goto_0
.end method

.method public setValue(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/ControllerView;->mGradationView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/input/GradationView;->setValue(I)V

    .line 60
    return-void
.end method
