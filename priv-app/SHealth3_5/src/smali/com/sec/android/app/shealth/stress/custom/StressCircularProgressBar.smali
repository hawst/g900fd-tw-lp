.class public Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;
.super Landroid/view/View;
.source "StressCircularProgressBar.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ResourceAsColor"
    }
.end annotation


# instance fields
.field private final mCircleBounds:Landroid/graphics/RectF;

.field private mCircleStrokeWidth:I

.field private mEndRange:F

.field private final mGravity:I

.field mHandler:Landroid/os/Handler;

.field private mHorizontalInset:I

.field private mIsInitializing:Z

.field private mOverrdraw:Z

.field private mProgress:F

.field private mProgressColor:I

.field private mProgressColorPaint:Landroid/graphics/Paint;

.field private mRadius:F

.field mRunnable:Ljava/lang/Runnable;

.field private mStartRange:F

.field private mThumbColorPaint:Landroid/graphics/Paint;

.field private mThumbRadius:I

.field private mTranslationOffsetX:F

.field private mTranslationOffsetY:F

.field private mVerticalInset:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 82
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mIsInitializing:Z

    .line 39
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleStrokeWidth:I

    .line 44
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mThumbRadius:I

    .line 47
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleBounds:Landroid/graphics/RectF;

    .line 57
    iput v2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgress:F

    .line 58
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mThumbColorPaint:Landroid/graphics/Paint;

    .line 60
    iput v3, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHorizontalInset:I

    .line 61
    iput v3, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mVerticalInset:I

    .line 63
    iput v2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mStartRange:F

    .line 64
    iput v2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mEndRange:F

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRunnable:Ljava/lang/Runnable;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHandler:Landroid/os/Handler;

    .line 70
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mOverrdraw:Z

    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgressColor(I)V

    .line 86
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setWheelSize(I)V

    .line 88
    const/16 v0, 0x11

    iput v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mGravity:I

    .line 90
    iget v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleStrokeWidth:I

    iget v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleStrokeWidth:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mThumbRadius:I

    .line 91
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgress(F)V

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->updateProgressColor()V

    .line 96
    iput-boolean v3, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mIsInitializing:Z

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHandler:Landroid/os/Handler;

    .line 101
    new-instance v0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar$1;-><init>(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRunnable:Ljava/lang/Runnable;

    .line 126
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mEndRange:F

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mStartRange:F

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;
    .param p1, "x1"    # F

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mStartRange:F

    return p1
.end method

.method private computeInsets(II)V
    .locals 5
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 173
    iget v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mGravity:I

    .line 174
    .local v0, "absoluteGravity":I
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getLayoutDirection()I

    move-result v1

    .line 176
    .local v1, "layoutDirection":I
    iget v2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mGravity:I

    invoke-static {v2, v1}, Landroid/view/Gravity;->getAbsoluteGravity(II)I

    move-result v0

    .line 179
    .end local v1    # "layoutDirection":I
    :cond_0
    and-int/lit8 v2, v0, 0x7

    packed-switch v2, :pswitch_data_0

    .line 188
    :pswitch_0
    div-int/lit8 v2, p1, 0x2

    iput v2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHorizontalInset:I

    .line 191
    :goto_0
    and-int/lit8 v2, v0, 0x70

    sparse-switch v2, :sswitch_data_0

    .line 200
    div-int/lit8 v2, p2, 0x2

    iput v2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mVerticalInset:I

    .line 203
    :goto_1
    return-void

    .line 181
    :pswitch_1
    iput v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHorizontalInset:I

    goto :goto_0

    .line 184
    :pswitch_2
    iput p1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHorizontalInset:I

    goto :goto_0

    .line 193
    :sswitch_0
    iput v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mVerticalInset:I

    goto :goto_1

    .line 196
    :sswitch_1
    iput p2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mVerticalInset:I

    goto :goto_1

    .line 179
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 191
    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method private getCurrentRotation()F
    .locals 2

    .prologue
    .line 207
    const/high16 v0, 0x43b40000    # 360.0f

    iget v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgress:F

    mul-float/2addr v0, v1

    return v0
.end method

.method private setWheelSize(I)V
    .locals 0
    .param p1, "dimension"    # I

    .prologue
    .line 211
    iput p1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleStrokeWidth:I

    .line 212
    return-void
.end method

.method private updateProgressColor()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 217
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgressColorPaint:Landroid/graphics/Paint;

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgressColorPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgressColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgressColorPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgressColorPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleStrokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 222
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mThumbColorPaint:Landroid/graphics/Paint;

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mThumbColorPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgressColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mThumbColorPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mThumbColorPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleStrokeWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->invalidate()V

    .line 228
    return-void
.end method


# virtual methods
.method public getProgress()F
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgress:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mTranslationOffsetX:F

    iget v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mTranslationOffsetY:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getCurrentRotation()F

    move-result v6

    .line 142
    .local v6, "progressRotation":F
    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleBounds:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mOverrdraw:Z

    if-eqz v0, :cond_0

    const/high16 v3, 0x43b40000    # 360.0f

    :goto_0
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgressColorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 146
    return-void

    :cond_0
    move v3, v6

    .line 142
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-static {v4, p2}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getDefaultSize(II)I

    move-result v1

    .line 152
    .local v1, "height":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getSuggestedMinimumWidth()I

    move-result v4

    invoke-static {v4, p1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getDefaultSize(II)I

    move-result v3

    .line 153
    .local v3, "width":I
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 154
    .local v2, "min":I
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setMeasuredDimension(II)V

    .line 156
    int-to-float v4, v2

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float v0, v4, v5

    .line 157
    .local v0, "halfWidth":F
    iget v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mThumbRadius:I

    int-to-float v4, v4

    sub-float v4, v0, v4

    const/high16 v5, 0x41400000    # 12.0f

    add-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRadius:F

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mCircleBounds:Landroid/graphics/RectF;

    iget v5, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRadius:F

    neg-float v5, v5

    iget v6, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRadius:F

    neg-float v6, v6

    iget v7, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRadius:F

    iget v8, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRadius:F

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 160
    sub-int v4, v3, v2

    sub-int v5, v1, v2

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->computeInsets(II)V

    .line 162
    iget v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHorizontalInset:I

    int-to-float v4, v4

    add-float/2addr v4, v0

    iput v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mTranslationOffsetX:F

    .line 163
    iget v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mVerticalInset:I

    int-to-float v4, v4

    add-float/2addr v4, v0

    iput v4, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mTranslationOffsetY:F

    .line 165
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 292
    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHandler:Landroid/os/Handler;

    .line 293
    iput-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRunnable:Ljava/lang/Runnable;

    .line 294
    return-void
.end method

.method public resetProgress()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 237
    iput v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgress:F

    .line 238
    iput v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mEndRange:F

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->getProgress()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mStartRange:F

    .line 240
    iget v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgress:F

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->setProgress(F)V

    .line 241
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->invalidate()V

    .line 242
    return-void
.end method

.method public setProgress(F)V
    .locals 3
    .param p1, "progress"    # F

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 256
    iget v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgress:F

    invoke-static {p1, v0}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-nez v0, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-nez v0, :cond_2

    .line 261
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mOverrdraw:Z

    .line 262
    iput v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgress:F

    .line 274
    :goto_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mIsInitializing:Z

    if-nez v0, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->invalidate()V

    goto :goto_0

    .line 265
    :cond_2
    invoke-static {p1, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->compare(FF)I

    move-result v0

    if-ltz v0, :cond_3

    .line 266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mOverrdraw:Z

    .line 271
    :goto_2
    rem-float v0, p1, v1

    iput v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgress:F

    goto :goto_1

    .line 268
    :cond_3
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mOverrdraw:Z

    goto :goto_2
.end method

.method public setProgressColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 284
    iput p1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mProgressColor:I

    .line 286
    invoke-direct {p0}, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->updateProgressColor()V

    .line 287
    return-void
.end method

.method public setProgressRange(F)V
    .locals 2
    .param p1, "end"    # F

    .prologue
    .line 249
    iput p1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mEndRange:F

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/shealth/stress/custom/StressCircularProgressBar;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 252
    return-void
.end method
