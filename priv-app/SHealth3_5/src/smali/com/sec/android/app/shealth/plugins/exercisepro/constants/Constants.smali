.class public Lcom/sec/android/app/shealth/plugins/exercisepro/constants/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ACCESSARY_ADD_REQ_CODE:I = 0x7b

.field public static final ACTION_BAR_BUTTON_CANCEL:I = 0x0

.field public static final ACTION_BAR_BUTTON_DONE:I = 0x1

.field public static final ACTION_FILTER_LUNCH_EXERCISE_MATE:Ljava/lang/String; = "com.sec.android.app.shealth.exercisemate.command.exercisemate"

.field public static final ACTION_FILTER_MEDIA_CHECK:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.checkplaystatus"

.field public static final ACTION_FILTER_MEDIA_INFO:Ljava/lang/String; = "com.sec.android.music.musicservicecommnad.mediainfo"

.field public static final ACTION_FILTER_MEDIA_NEXT:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.next"

.field public static final ACTION_FILTER_MEDIA_PALY:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.play"

.field public static final ACTION_FILTER_MEDIA_PALY_TOGGLE:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.togglepause"

.field public static final ACTION_FILTER_MEDIA_PAUSE:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.pause"

.field public static final ACTION_FILTER_MEDIA_PREVIOUS:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.prev"

.field public static final ACTION_TAB:Ljava/lang/String; = "action_tab"

.field public static final ANCHOR_CENTER:I = 0x1

.field public static final ANCHOR_LEFT:I = 0x2

.field public static final ANCHOR_MASK:I = 0xf

.field public static final ANCHOR_RIGHT:I = 0x4

.field public static final AT_ABOVE:I = 0x10000

.field public static final AT_BELOW:I = 0x20000

.field public static final AT_MASK:I = 0xf0000

.field public static final BSAC_CONNECT_ERROR:I = 0x3f7

.field public static final BSAC_DEVICE_CONNECTED:I = 0x3f6

.field public static final BSAC_DEVICE_CONNECTING:I = 0x3f5

.field public static final BSAC_DEVICE_DISCONNECTED:I = 0x3f4

.field public static final CAMERA_REQ_CODE:I = 0x82

.field public static final CHINA_LOCATION_DIALOG_TAG:Ljava/lang/String; = "location_dialog"

.field public static final CURRENT_FRAGMENT:Ljava/lang/String; = "exercise_pro_details_fragment"

.field public static final DEFAULT_BACKGROUND_COLOR:I = -0xa0a0b

.field public static final DEFAULT_CALORIE_TO_BURN_PER:I = 0x294

.field public static final DEFAULT_ROW_ID:I = -0x1

.field public static final DELETE_DIALOG:Ljava/lang/String; = "delete_dialog"

.field public static final DELETE_RESULT_CODE:I = 0x5dc

.field public static final DISCARD_DIALOG:Ljava/lang/String; = "discard_dialog"

.field public static final EDIT_POPUP_REQUEST_CODE:I = 0x99

.field public static final EXERCISE_CHARTTAB:Ljava/lang/String; = "EXERCISE_CHARTTAB"

.field public static final EXERCISE_FREQUENCY_0_5KM:I = 0x1

.field public static final EXERCISE_FREQUENCY_10MIN:I = 0x5

.field public static final EXERCISE_FREQUENCY_1HR:I = 0x7

.field public static final EXERCISE_FREQUENCY_1KM:I = 0x2

.field public static final EXERCISE_FREQUENCY_2KM:I = 0x3

.field public static final EXERCISE_FREQUENCY_30MIN:I = 0x6

.field public static final EXERCISE_FREQUENCY_5MIN:I = 0x4

.field public static final EXERCISE_FREQUENCY_AFTER_RUN:I = 0x8

.field public static final EXERCISE_FREQUENCY_NEVER:I = 0x0

.field public static final EXERICSE_PRIMARY_DISPLAY:Ljava/lang/String; = "exercisePrimaryDisplay"

.field public static final FEEDBACK_TYPE_AVERAGE_PACE:J = 0x20L

.field public static final FEEDBACK_TYPE_AVERAGE_SPEED:J = 0x8L

.field public static final FEEDBACK_TYPE_CADANCE:J = 0x200L

.field public static final FEEDBACK_TYPE_CALORIES:J = 0x40L

.field public static final FEEDBACK_TYPE_CURRENT_PACE:J = 0x10L

.field public static final FEEDBACK_TYPE_CURRENT_SPEED:J = 0x4L

.field public static final FEEDBACK_TYPE_DISTANCE:J = 0x2L

.field public static final FEEDBACK_TYPE_DURATION:J = 0x1L

.field public static final FEEDBACK_TYPE_ELEVATION:J = 0x400L

.field public static final FEEDBACK_TYPE_HEARTRATE:J = 0x100L

.field public static final FEEDBACK_TYPE_REMAINING_TO_REACH_GOAL:J = 0x80L

.field public static final FE_DEVICE_CONNECTED:I = 0x3f0

.field public static final FE_DEVICE_CONNECTING:I = 0x3ef

.field public static final FE_DEVICE_DISCONNECTED:I = 0x3ee

.field public static final FE_NOTI_DISCONNECTED:I = 0x3f1

.field public static final FE_NOTI_USER_CANCEL:I = 0x3f3

.field public static final FE_RESTART:I = 0x3f2

.field public static final FITNESS_EQUIPMENT_TYPE_BIKE:I = 0x15

.field public static final FITNESS_EQUIPMENT_TYPE_CLIMBER:I = 0x17

.field public static final FITNESS_EQUIPMENT_TYPE_ELLIPTICAL:I = 0x14

.field public static final FITNESS_EQUIPMENT_TYPE_GENERAL:I = 0x10

.field public static final FITNESS_EQUIPMENT_TYPE_NORDICSKIER:I = 0x18

.field public static final FITNESS_EQUIPMENT_TYPE_ROWER:I = 0x16

.field public static final FITNESS_EQUIPMENT_TYPE_TREADMILL:I = 0x13

.field public static final FITNESS_GRAPH:I = 0x3ed

.field public static final GALLERY_REQ_CODE:I = 0x83

.field public static final GEAR2_STRING_CODE:Ljava/lang/String; = "GEAR2"

.field public static final GEARFIT_STRING_CODE:Ljava/lang/String; = "WINGTIP"

.field public static final GOAL_ADD_REQ_CODE:I = 0x7c

.field public static final GPS_CONNECTED:I = 0xbbf

.field public static final GPS_DATA_RECEIVED:I = 0xbbe

.field public static final GPS_DEVICE_CONNECTED:I = 0xbbd

.field public static final GPS_DEVICE_DISCONNECTED:I = 0xbbc

.field public static final GPS_DISCONNECTED:I = 0xbc0

.field public static final GPS_SIGNAL_FAIR:I = 0xbba

.field public static final GPS_SIGNAL_LOCATING:I = 0xbb8

.field public static final GPS_SIGNAL_STRONG:I = 0xbbb

.field public static final GPS_SIGNAL_WEAK:I = 0xbb9

.field public static final GRAPH_REFLESH_FORM_LOG:I = 0x84

.field public static final GYM_MODE_GUIDE_CHECK_DIALOG:Ljava/lang/String; = "gym_mode_check_guide"

.field public static final GYM_MODE_GUIDE_DIALOG:Ljava/lang/String; = "gym_mode_guide"

.field public static final GYM_MODE_IN_PROGRESS_DIALOG:Ljava/lang/String; = "gym_mode_in_progress"

.field public static final HEALTHDIARY_APPLICATION_NAME:Ljava/lang/String; = "SHealth2"

.field public static final HRM_CONNECT_ERROR:I = 0x3ed

.field public static final HRM_DEVICE_CONNECTED:I = 0x3ea

.field public static final HRM_DEVICE_CONNECTING:I = 0x3e9

.field public static final HRM_DEVICE_DISCONNECTED:I = 0x3e8

.field public static final HRM_MEASURE_EXIT:I = 0x5

.field public static final HRM_MEASURE_EXIT_ON_RESUME:I = 0x6

.field public static final HRM_MEASURE_OPEN:I = 0x1

.field public static final IMAGE_MINE_TYPE:Ljava/lang/String; = "image/*"

.field public static final IS_PLAY_SERVICE_POPUP_DISPLAYED:Ljava/lang/String; = "is_play_service_popup_displayed"

.field public static final KCAL_GRAPH:I = 0x3ec

.field public static final KEY_AUTO_MODE:Ljava/lang/String; = "key_auto_mode"

.field public static final KEY_FOR_DISP_DATA:Ljava/lang/String; = "key_for_disp_data"

.field public static final KEY_MAX_VALUE:Ljava/lang/String; = "key_max_value"

.field public static final MAP_MODE:Ljava/lang/String; = "map_mode"

.field public static final MAX_DISPLAYED_IMAGES_COUNT:I = 0x4

.field public static final MAX_HR_UPDATE_REQ_CODE:I = 0x83

.field public static final MAX_RANGE_HR_VALUE:I = 0xf0

.field public static final MEMO_KEY:Ljava/lang/String; = "memo"

.field public static final MEMO_MAX_LINES:I = 0x3

.field public static final MIN_RANGE_HR_VALUE:I = 0x64

.field public static final MULTI_PICK_EXTRA_TAG:Ljava/lang/String; = "multi-pick"

.field public static final MUSIC_PLAYER_REQ_CODE:I = 0x3039

.field public static final NORMALZONE:I = 0x7532

.field public static final OVERHRZONE:I = 0x7530

.field public static final PHOTO_MAX_COUNT:I = 0x9

.field public static final PICK_MAX_ITEM_EXTRA_TAG:Ljava/lang/String; = "pick-max-item"

.field public static final POPUP_GPS_LOST:I = 0x8

.field public static final POPUP_GPS_ON:I = 0x2

.field public static final POPUP_GYM_CHK_GUIDE:I = 0x5

.field public static final POPUP_GYM_CONNECTING:I = 0x6

.field public static final POPUP_GYM_DISCONNECTING:I = 0x7

.field public static final POPUP_GYM_GUIDE:I = 0x4

.field public static final POPUP_HRM_CONNECT:I = 0x1

.field public static final POPUP_WFL_PAUSE:I = 0x3

.field public static final REALTIME_GRAPH:I = 0x3eb

.field public static final REALTIME_MAP:I = 0x3ea

.field public static final REALTIME_STATUS:I = 0x3e9

.field public static final REALTIME_TEST:I = 0x3e8

.field public static final REQUEST_CAMERA:I = 0x4e20

.field public static final REQUEST_EXERCISE:I = 0x1

.field public static final REQUEST_GALLERY:I = 0x4e21

.field public static final REQUEST_SNS_RESULT:I = 0x64

.field public static final RESULT_SAVED:I = 0xc8

.field public static final RT_EXERCISE_START:Ljava/lang/String; = "com.sec.android.app.shealth.measure.realtime.EXERCISE_START"

.field public static final RT_EXERCISE_STOP:Ljava/lang/String; = "com.sec.android.app.shealth.measure.realtime.EXERCISE_STOP"

.field public static final SAVEWORKOUT_ADD_REQ_CODE:I = 0x7e

.field public static final SAVEWORKOUT_REQ_CODE:I = 0x7e

.field public static final SAVE_FROM_MAP_REQ_CODE:I = 0x7f

.field public static final SELECTED_ITEMS_EXTRA_DATAS:Ljava/lang/String; = "selectedItems"

.field public static final SELECTED_TAB:Ljava/lang/String; = "selected_tab"

.field public static final SQLITE_EXERICSE_GROUPBY_DAY:Ljava/lang/String; = "strftime(\"%d-%m-%Y\",([Exercise].[start_time]/1000),\'unixepoch\',\'localtime\')"

.field public static final SQLITE_EXERICSE_GROUPBY_HOUR:Ljava/lang/String; = "strftime(\"%d-%m-%Y %H\",([Exercise].[start_time]/1000),\'unixepoch\',\'localtime\')"

.field public static final SQLITE_EXERICSE_GROUPBY_MONTH:Ljava/lang/String; = "strftime(\"%m-%Y\",([Exercise].[start_time]/1000),\'unixepoch\',\'localtime\')"

.field public static final STARTWORKOUT_GPSON_REQ_CODE:I = 0x7d

.field public static final START_HRM:I = 0xfa4

.field public static final START_SCAN:I = 0x3e8

.field public static final STRING_ACTIVITY_TRACKER_CODE:Ljava/lang/String; = "Samsung EI-AN900A"

.field public static final SUPPORTED_FILE_PREFIXES:[Ljava/lang/String;

.field public static final UNDERHRZONE:I = 0x7531

.field public static final UNLOCK_DELAY_TIME:I = 0x514

.field public static final WORKOUT_LESS_THAN_2MIN_TAG:Ljava/lang/String; = "less_than_2min_tag"

.field public static final WORKOUT_STATE_PAUSE:I = 0x7d2

.field public static final WORKOUT_STATE_RUNNING:I = 0x7d1

.field public static final WORKOUT_STATE_SLEEP:I = 0x7d0


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 132
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "file://"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/Constants;->SUPPORTED_FILE_PREFIXES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
