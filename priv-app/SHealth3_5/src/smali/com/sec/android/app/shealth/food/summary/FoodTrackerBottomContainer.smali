.class public Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;
.super Landroid/widget/LinearLayout;
.source "FoodTrackerBottomContainer.java"


# instance fields
.field private mChartButton:Landroid/view/View;

.field private mFavoriteButton:Landroid/view/View;

.field private mQuickInputButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->init()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->init()V

    .line 46
    return-void
.end method

.method private init()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->initViews()V

    .line 50
    return-void
.end method

.method private initViews()V
    .locals 6

    .prologue
    const v5, 0x7f09020a

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 54
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03011b

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 55
    const v1, 0x7f0804a9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->mQuickInputButton:Landroid/view/View;

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->mQuickInputButton:Landroid/view/View;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09023b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 59
    const v1, 0x7f0804aa

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->mFavoriteButton:Landroid/view/View;

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->mFavoriteButton:Landroid/view/View;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0901df

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 62
    const v1, 0x7f0804ab

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->mChartButton:Landroid/view/View;

    .line 63
    return-void
.end method


# virtual methods
.method public getChartButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->mChartButton:Landroid/view/View;

    return-object v0
.end method

.method public getFavouriteButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->mFavoriteButton:Landroid/view/View;

    return-object v0
.end method

.method public getQuickInputButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBottomContainer;->mQuickInputButton:Landroid/view/View;

    return-object v0
.end method
