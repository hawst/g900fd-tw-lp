.class Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;
.super Ljava/lang/Object;
.source "PrivacyPolicy.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isCheck"    # Z

    .prologue
    const/4 v1, 0x0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$000(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->mAgreeCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$000(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # setter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isAgreeCheck:Z
    invoke-static {v0, p2}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$102(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;Z)Z

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # getter for: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->isAgreeCheck:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$100(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->enableNextBtn(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$200(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;Z)V

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy$3;->this$0:Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;

    # invokes: Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->enableNextBtn(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;->access$200(Lcom/sec/android/app/shealth/settings/policy/PrivacyPolicy;Z)V

    goto :goto_0
.end method
