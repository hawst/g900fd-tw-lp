.class Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;
.super Ljava/lang/Object;
.source "SleepMonitorSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->update()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 29

    .prologue
    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->updateConnectionStatus()V
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->updateLastSyncTime()V
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$700(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Ljava/util/Date;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/util/Date;->getTime()J

    move-result-wide v24

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getSelectData(J)Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;
    invoke-static/range {v23 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;J)Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    move-result-object v7

    .line 283
    .local v7, "data":Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayEfficiency:F

    .line 284
    if-nez v7, :cond_0

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1000(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RatingBar;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RatingBar;->setRating(F)V

    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1100(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RatingBar;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RatingBar;->setRating(F)V

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1000(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RatingBar;

    move-result-object v23

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1100(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RatingBar;

    move-result-object v23

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceOne:Landroid/widget/RelativeLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1200(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v23

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceTwo:Landroid/widget/RelativeLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1300(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RelativeLayout;

    move-result-object v23

    const/16 v24, 0x4

    invoke-virtual/range {v23 .. v24}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f090d88

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f090d88

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayProgress:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const v24, -0x777778

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayProgress:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, 0x2

    const/16 v25, 0x2

    const/16 v26, 0x2

    const/16 v27, 0x2

    invoke-virtual/range {v23 .. v27}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 301
    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v23, -0x1

    const/16 v24, -0x1

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v13, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 302
    .local v13, "param":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayTotalSleepBarPortion:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1700(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, -0x1

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLessBarPortion:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayTotalSleepBarPortion:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1700(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLess:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f090d64

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const-string v28, "--"

    aput-object v28, v26, v27

    const/16 v27, 0x1

    const-string v28, "--"

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setMonthData(Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;)V

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepData:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2000(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepNoData:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2100(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setNextAndPrevDates()V

    .line 476
    .end local v13    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-void

    .line 313
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2300(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2200(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Ljava/util/Date;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setNextAndPrevDates()V

    .line 315
    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2400()Ljava/lang/String;

    move-result-object v23

    const-string/jumbo v24, "update"

    invoke-static/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepData:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2000(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepNoData:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2100(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Ljava/util/Date;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/util/Date;->getTime()J

    move-result-wide v24

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalTimeForDay(J)Ljava/lang/String;
    invoke-static/range {v23 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;J)Ljava/lang/String;

    move-result-object v23

    const-string v24, "_"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 325
    .local v4, "args":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v24, v4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v24

    # setter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalDaySleepTime:J
    invoke-static/range {v23 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2702(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;J)J

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalDaySleepTime:J
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2700(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)J

    move-result-wide v23

    const-wide/32 v25, 0xea60

    div-long v19, v23, v25

    .line 328
    .local v19, "totalMinute":J
    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v23, v0

    div-int/lit8 v21, v23, 0x3c

    .line 329
    .local v21, "totalSleepHour":I
    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v23, v0

    rem-int/lit8 v22, v23, 0x3c

    .line 330
    .local v22, "totalSleepMinute":I
    const-string v15, ""

    .line 331
    .local v15, "talkBackHour":Ljava/lang/String;
    const-string v16, ""

    .line 332
    .local v16, "talkBackMin":Ljava/lang/String;
    const-string v17, ""

    .line 333
    .local v17, "talkBackMotionlessHour":Ljava/lang/String;
    const-string v18, ""

    .line 335
    .local v18, "talkBackMotionlessMin":Ljava/lang/String;
    if-nez v21, :cond_3

    .line 337
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_1

    .line 339
    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    const v25, 0x7f090f6d

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v23 .. v25}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 343
    .local v14, "str":Ljava/lang/String;
    :goto_1
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v14}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 344
    .local v5, "builder":Landroid/text/SpannableStringBuilder;
    new-instance v23, Landroid/text/style/AbsoluteSizeSpan;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f0d005f

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v24

    const/16 v25, 0x1

    invoke-direct/range {v23 .. v25}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/16 v24, 0x0

    const/16 v25, 0x2

    const/16 v26, 0x21

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_2

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc2

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 412
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v24

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v21, :cond_a

    const-string v23, ""

    :goto_3
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    if-nez v22, :cond_b

    const-string v23, ""

    :goto_4
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 413
    const/16 v23, 0x2

    aget-object v23, v4, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->round(F)I

    move-result v23

    const v24, 0xea60

    div-int v23, v23, v24

    move/from16 v0, v23

    int-to-long v9, v0

    .line 414
    .local v9, "minute":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    aget-object v24, v4, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayEfficiency:F

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayProgress:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayProgress:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v23 .. v27}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayTotalSleepBarPortion:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1700(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f070221

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLessBarPortion:Landroid/widget/LinearLayout;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f070222

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getColor(I)I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 419
    long-to-int v0, v9

    move/from16 v23, v0

    div-int/lit8 v11, v23, 0x3c

    .line 420
    .local v11, "motionlessHour":I
    long-to-int v0, v9

    move/from16 v23, v0

    rem-int/lit8 v12, v23, 0x3c

    .line 423
    .local v12, "motionlessMinute":I
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v11, v0, :cond_f

    .line 424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc1

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 425
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v12, v0, :cond_d

    .line 426
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_c

    .line 427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090bfc

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 431
    .local v8, "label":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc2

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 466
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLess:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v24

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x7f090d8e

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    if-nez v11, :cond_15

    const-string v23, ""

    :goto_7
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    if-nez v12, :cond_16

    const-string v23, ""

    :goto_8
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, " "

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayEfficiency:F

    move/from16 v25, v0

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v25, "%"

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 467
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "("

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "%d"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 468
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_17

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLess:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v25, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayEfficiency:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v8, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "%)"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 472
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLess:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setVisibility(I)V

    .line 473
    const/16 v23, 0x3

    aget-object v23, v4, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 474
    .local v6, "count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;
    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Ljava/util/Date;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/util/Date;->getTime()J

    move-result-wide v24

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    # invokes: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setTimeOfSleep(JI)V
    invoke-static {v0, v1, v2, v6}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$2900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;JI)V

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setMonthData(Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;)V

    goto/16 :goto_0

    .line 341
    .end local v5    # "builder":Landroid/text/SpannableStringBuilder;
    .end local v6    # "count":I
    .end local v8    # "label":Ljava/lang/String;
    .end local v9    # "minute":J
    .end local v11    # "motionlessHour":I
    .end local v12    # "motionlessMinute":I
    .end local v14    # "str":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090f6d

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "str":Ljava/lang/String;
    goto/16 :goto_1

    .line 356
    .restart local v5    # "builder":Landroid/text/SpannableStringBuilder;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc4

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    .line 365
    .end local v5    # "builder":Landroid/text/SpannableStringBuilder;
    .end local v14    # "str":Ljava/lang/String;
    :cond_3
    const/16 v23, 0x1

    move/from16 v0, v21

    move/from16 v1, v23

    if-ne v0, v1, :cond_5

    .line 366
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_4

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090bff

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 371
    .restart local v14    # "str":Ljava/lang/String;
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc1

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 382
    :goto_b
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 384
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v14}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 386
    .restart local v5    # "builder":Landroid/text/SpannableStringBuilder;
    new-instance v23, Landroid/text/style/AbsoluteSizeSpan;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f0d005f

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v24

    const/16 v25, 0x1

    invoke-direct/range {v23 .. v25}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/16 v24, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    const/16 v26, 0x21

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    const-string v24, ""

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 390
    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_8

    .line 391
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_7

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090c00

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 396
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc2

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 407
    :goto_d
    new-instance v5, Landroid/text/SpannableStringBuilder;

    .end local v5    # "builder":Landroid/text/SpannableStringBuilder;
    invoke-direct {v5, v14}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 409
    .restart local v5    # "builder":Landroid/text/SpannableStringBuilder;
    new-instance v23, Landroid/text/style/AbsoluteSizeSpan;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f0d005f

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v24

    const/16 v25, 0x1

    invoke-direct/range {v23 .. v25}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/16 v24, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v25

    const/16 v26, 0x21

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 369
    .end local v5    # "builder":Landroid/text/SpannableStringBuilder;
    .end local v14    # "str":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090d71

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "str":Ljava/lang/String;
    goto/16 :goto_a

    .line 374
    .end local v14    # "str":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_6

    .line 375
    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    const v25, 0x7f090d72

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v23 .. v25}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 379
    .restart local v14    # "str":Ljava/lang/String;
    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc3

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_b

    .line 377
    .end local v14    # "str":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090d72

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "str":Ljava/lang/String;
    goto :goto_e

    .line 394
    .restart local v5    # "builder":Landroid/text/SpannableStringBuilder;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090d73

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_c

    .line 399
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_9

    .line 400
    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    const v25, 0x7f090f6d

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v23 .. v25}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 404
    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc4

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_d

    .line 402
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090f6d

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    goto :goto_f

    .line 412
    :cond_a
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    goto/16 :goto_3

    :cond_b
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    goto/16 :goto_4

    .line 429
    .restart local v9    # "minute":J
    .restart local v11    # "motionlessHour":I
    .restart local v12    # "motionlessMinute":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090d62

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "label":Ljava/lang/String;
    goto/16 :goto_5

    .line 433
    .end local v8    # "label":Ljava/lang/String;
    :cond_d
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_e

    .line 434
    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    const v25, 0x7f090bfb

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v23 .. v25}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 438
    .restart local v8    # "label":Ljava/lang/String;
    :goto_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc4

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_6

    .line 436
    .end local v8    # "label":Ljava/lang/String;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090d61

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "label":Ljava/lang/String;
    goto :goto_10

    .line 441
    .end local v8    # "label":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc3

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 442
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v12, v0, :cond_11

    .line 443
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_10

    .line 444
    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    const v25, 0x7f090bfd

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v23 .. v25}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 448
    .restart local v8    # "label":Ljava/lang/String;
    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc2

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_6

    .line 446
    .end local v8    # "label":Ljava/lang/String;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090d63

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "label":Ljava/lang/String;
    goto :goto_11

    .line 449
    .end local v8    # "label":Ljava/lang/String;
    :cond_11
    if-nez v11, :cond_13

    .line 450
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_12

    .line 451
    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    const v25, 0x7f090d6f

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v23 .. v25}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 455
    .restart local v8    # "label":Ljava/lang/String;
    :goto_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc4

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 456
    const-string v17, ""

    goto/16 :goto_6

    .line 453
    .end local v8    # "label":Ljava/lang/String;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090d6f

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "label":Ljava/lang/String;
    goto :goto_12

    .line 458
    .end local v8    # "label":Ljava/lang/String;
    :cond_13
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v23

    if-eqz v23, :cond_14

    .line 459
    sget-object v23, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v24, v0

    const v25, 0x7f090d60

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v23 .. v25}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 463
    .restart local v8    # "label":Ljava/lang/String;
    :goto_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f090dc4

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_6

    .line 461
    .end local v8    # "label":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    const v24, 0x7f090d60

    invoke-virtual/range {v23 .. v24}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v23 .. v24}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "label":Ljava/lang/String;
    goto :goto_13

    .line 466
    :cond_15
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    goto/16 :goto_7

    :cond_16
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    goto/16 :goto_8

    .line 471
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLess:Landroid/widget/TextView;
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->access$1900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;

    move-result-object v23

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;->this$0:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayEfficiency:F

    move/from16 v27, v0

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    move-object/from16 v0, v25

    invoke-static {v8, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "%)"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9
.end method
