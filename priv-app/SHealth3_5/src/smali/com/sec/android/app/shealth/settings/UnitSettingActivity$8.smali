.class Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;
.super Landroid/content/BroadcastReceiver;
.source "UnitSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/UnitSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V
    .locals 0

    .prologue
    .line 733
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 737
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.health.sensor.action.PROFILE_UPDATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 738
    const-string v0, "UnitSettingActivity"

    const-string/jumbo v1, "mSyncedReceiver onRecieve : HEALTH_PROFILE_UPDATED"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$900(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$900(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;->clear()V

    .line 741
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->initUnitValue()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$1000(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 743
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    new-instance v1, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->resetDataListItem:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$1200(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v1, v2, p1, v3, v4}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;-><init>(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Landroid/content/Context;ILjava/util/List;)V

    # setter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$902(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    .line 744
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    const v2, 0x7f080a8d

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    # setter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->measurablesList:Landroid/widget/ListView;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$1302(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;Landroid/widget/ListView;)Landroid/widget/ListView;

    .line 745
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->measurablesList:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$1300(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->unitsListAdapter:Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$900(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)Lcom/sec/android/app/shealth/settings/UnitSettingActivity$UnitsListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 746
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/UnitSettingActivity$8;->this$0:Lcom/sec/android/app/shealth/settings/UnitSettingActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->initUnitValue()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/UnitSettingActivity;->access$1000(Lcom/sec/android/app/shealth/settings/UnitSettingActivity;)V

    goto :goto_0
.end method
