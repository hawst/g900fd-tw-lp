.class public Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;
.super Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EditTextLimitHelperThatShowsToast"
.end annotation


# instance fields
.field private final contextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final toast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textMessageResId"    # I

    .prologue
    const/16 v4, 0x12

    const/16 v5, 0xa

    const/4 v6, 0x0

    .line 320
    invoke-direct {p0}, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelper;-><init>()V

    .line 321
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->contextRef:Ljava/lang/ref/WeakReference;

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->contextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v2, p2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->toast:Landroid/widget/Toast;

    .line 323
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x14

    if-ge v2, v3, :cond_1

    .line 325
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->toast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    .line 327
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 328
    sget v2, Lcom/sec/android/app/shealth/common/utils/R$drawable;->tw_toast_frame_holo_light:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 329
    const v2, 0x102000b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 332
    .local v0, "tv":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 334
    invoke-static {p1, v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v2

    float-to-int v2, v2

    invoke-static {p1, v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v3

    float-to-int v3, v3

    invoke-static {p1, v4}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p1, v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 335
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 336
    const-string/jumbo v2, "sans-serif-light"

    invoke-static {v2, v6}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->contextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-virtual {v2, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->toast:Landroid/widget/Toast;

    invoke-virtual {v2, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 345
    .end local v0    # "tv":Landroid/widget/TextView;
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method


# virtual methods
.method public onLimitExceeded()V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->toast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/utils/Utils$EditTextLimitHelperThatShowsToast;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 351
    :cond_0
    return-void
.end method
