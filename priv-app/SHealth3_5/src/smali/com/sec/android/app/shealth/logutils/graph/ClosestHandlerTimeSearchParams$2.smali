.class final enum Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams$2;
.super Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;
.source "ClosestHandlerTimeSearchParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;J)V
    .locals 7
    .param p3, "x0"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .param p4, "x1"    # J

    .prologue
    .line 30
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams;-><init>(Ljava/lang/String;ILcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;JLcom/sec/android/app/shealth/logutils/graph/ClosestHandlerTimeSearchParams$1;)V

    return-void
.end method


# virtual methods
.method public getOptimalTime(JJ)J
    .locals 2
    .param p1, "time1"    # J
    .param p3, "time2"    # J

    .prologue
    .line 33
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method
