.class final enum Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
.super Ljava/lang/Enum;
.source "BarcodeScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FocusState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

.field public static final enum FOCUSED:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

.field public static final enum FOCUSING:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

.field public static final enum IDLE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

.field public static final enum READING_BARCODE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->IDLE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 101
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    const-string v1, "FOCUSING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->FOCUSING:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 107
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    const-string v1, "FOCUSED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->FOCUSED:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 110
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    const-string v1, "READING_BARCODE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->READING_BARCODE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    .line 93
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->IDLE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->FOCUSING:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->FOCUSED:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->READING_BARCODE:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 93
    const-class v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->$VALUES:[Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeScanner$FocusState;

    return-object v0
.end method
