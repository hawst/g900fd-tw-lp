.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;
.super Ljava/lang/Object;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$100(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->ENTER_FAVORITE_BY_ICON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    const-class v2, Lcom/sec/android/app/shealth/food/favorites/FavoritesActivity;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->createIntentForQuickInput(Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 235
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$1;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->startActivity(Landroid/content/Intent;)V

    .line 236
    return-void
.end method
