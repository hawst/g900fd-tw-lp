.class Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;
.super Ljava/lang/Object;
.source "RestoreHelper2.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->showServerCheckingPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)V
    .locals 0

    .prologue
    .line 1966
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPress(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 1970
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->NONE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->popupStatus:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1702(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    .line 1972
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1973
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismissAllowingStateLoss()V

    .line 1974
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mServerCheckingDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1977
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_PROFILE_FOR_RESTORE:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    if-ne v0, v1, :cond_2

    .line 1979
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1981
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mConnectionManagerInstance:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3500(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->cancelRequest(Ljava/lang/Object;)V

    .line 1982
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1984
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3600(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1985
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->wl:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3602(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Landroid/os/PowerManager$WakeLock;)Landroid/os/PowerManager$WakeLock;

    .line 1994
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData2:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1302(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z

    move-result v1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->noData1:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$1202(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;Z)Z

    .line 1995
    return-void

    .line 1989
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->currentStep:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3400(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;->CHECK_DATA_TO_IMPORT:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$Step;

    if-ne v0, v1, :cond_1

    .line 1991
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2$28;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->mRestoreManager:Lcom/sec/android/app/shealth/backup/server/RestoreManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;->access$3700(Lcom/sec/android/app/shealth/settings/samsungAccount/RestoreHelper2;)Lcom/sec/android/app/shealth/backup/server/RestoreManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/backup/server/RestoreManager;->cancel()V

    goto :goto_0
.end method
