.class Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;
.super Landroid/os/Handler;
.source "HeartrateSummaryFragmentNew.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeviceStateHandler"
.end annotation


# instance fields
.field private final mReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V
    .locals 1
    .param p1, "reference"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .prologue
    .line 233
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 234
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->mReference:Ljava/lang/ref/WeakReference;

    .line 235
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$1;

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;-><init>(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 239
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->mReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew$DeviceStateHandler;->mReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;

    .line 241
    .local v0, "obj":Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    iget v1, p1, Landroid/os/Message;->what:I

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mPrevError:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$002(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)I

    .line 242
    sget-object v1, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 268
    .end local v0    # "obj":Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    :cond_0
    :goto_0
    return-void

    .line 245
    .restart local v0    # "obj":Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;
    :pswitch_0
    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateState:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$102(I)I

    .line 246
    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setReadyUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$200(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    goto :goto_0

    .line 249
    :pswitch_1
    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateState:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$102(I)I

    .line 250
    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$300(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    goto :goto_0

    .line 253
    :pswitch_2
    const/4 v1, 0x2

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateState:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$102(I)I

    .line 254
    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringFailUI(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$400(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;I)V

    goto :goto_0

    .line 257
    :pswitch_3
    const/4 v1, 0x3

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateState:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$102(I)I

    .line 258
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setMeasuringEndUI()V

    .line 259
    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->refreshFragmentFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$500(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    goto :goto_0

    .line 262
    :pswitch_4
    const/4 v1, 0x4

    # setter for: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->mHeartrateState:I
    invoke-static {v1}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$102(I)I

    .line 263
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->setNonSensorUI()V

    .line 264
    # invokes: Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->refreshFragmentFocusables()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;->access$600(Lcom/sec/android/app/shealth/heartrate/HeartrateSummaryFragmentNew;)V

    goto :goto_0

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
