.class public Lcom/sec/android/app/shealth/common/commonui/ToastView;
.super Ljava/lang/Object;
.source "ToastView.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static makeCustomView(Landroid/content/Context;II)Landroid/widget/Toast;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "length"    # I

    .prologue
    const/16 v6, 0x12

    const/16 v7, 0x9

    const/4 v8, 0x0

    .line 83
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "message":Ljava/lang/String;
    invoke-static {p0, v0, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 86
    .local v1, "toast":Landroid/widget/Toast;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x14

    if-ge v4, v5, :cond_0

    .line 88
    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v3

    .line 89
    .local v3, "view":Landroid/view/View;
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->tw_toast_frame_holo_light:I

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 90
    const v4, 0x102000b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 91
    .local v2, "tv":Landroid/widget/TextView;
    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v5

    float-to-int v5, v5

    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v6

    float-to-int v6, v6

    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 92
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/common/commonui/R$color;->dialog_item_text:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 93
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/common/commonui/R$color;->dialog_item_text:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v8, v8, v8, v4}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 94
    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 95
    const/4 v4, 0x2

    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 96
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 102
    .end local v2    # "tv":Landroid/widget/TextView;
    .end local v3    # "view":Landroid/view/View;
    :cond_0
    return-object v1
.end method

.method public static makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "length"    # I

    .prologue
    const/16 v5, 0x12

    const/16 v6, 0x9

    const/4 v7, 0x0

    .line 25
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 26
    .local v0, "toast":Landroid/widget/Toast;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x14

    if-ge v3, v4, :cond_0

    .line 29
    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    .line 30
    .local v2, "view":Landroid/view/View;
    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->tw_toast_frame_holo_light:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 31
    const v3, 0x102000b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 32
    .local v1, "tv":Landroid/widget/TextView;
    invoke-static {p0, v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v3

    float-to-int v3, v3

    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p0, v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v5

    float-to-int v5, v5

    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 33
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$color;->dialog_item_text:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 34
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$color;->dialog_item_text:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v7, v7, v7, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 35
    const-string/jumbo v3, "sec-roboto-light"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 36
    const/4 v3, 0x2

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 37
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 43
    .end local v1    # "tv":Landroid/widget/TextView;
    .end local v2    # "view":Landroid/view/View;
    :cond_0
    return-object v0
.end method

.method public static makeCustomViewWithTalkback(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)Landroid/widget/Toast;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "length"    # I
    .param p3, "talkbackContent"    # Ljava/lang/String;
    .param p4, "talkbackSuffixString"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x12

    const/16 v7, 0xa

    const/4 v8, 0x0

    .line 115
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "message":Ljava/lang/String;
    invoke-static {p0, v0, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 118
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v3

    .line 119
    .local v3, "view":Landroid/view/View;
    const v4, 0x102000b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 120
    .local v2, "tv":Landroid/widget/TextView;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x14

    if-ge v4, v5, :cond_0

    .line 122
    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->tw_toast_frame_holo_light:I

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 123
    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v5

    float-to-int v5, v5

    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v6

    float-to-int v6, v6

    invoke-static {p0, v7}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 124
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/common/commonui/R$color;->dialog_item_text:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 125
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/sec/android/app/shealth/common/commonui/R$color;->dialog_item_text:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v8, v8, v8, v4}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 126
    const-string/jumbo v4, "sec-roboto-light"

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 127
    const/4 v4, 0x2

    const/high16 v5, 0x41700000    # 15.0f

    invoke-virtual {v2, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 128
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 132
    :cond_0
    invoke-static {v2, p3, p4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-object v1
.end method

.method public static makeCustomViewWithTalkback(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Landroid/widget/Toast;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "length"    # I
    .param p3, "talkbackContent"    # Ljava/lang/String;
    .param p4, "talkbackSuffixString"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x12

    const/16 v6, 0xa

    const/4 v7, 0x0

    .line 55
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 56
    .local v0, "toast":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    .line 57
    .local v2, "view":Landroid/view/View;
    const v3, 0x102000b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 58
    .local v1, "tv":Landroid/widget/TextView;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x14

    if-ge v3, v4, :cond_0

    .line 60
    sget v3, Lcom/sec/android/app/shealth/common/commonui/R$drawable;->tw_toast_frame_holo_light:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 61
    invoke-static {p0, v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v3

    float-to-int v3, v3

    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v4

    float-to-int v4, v4

    invoke-static {p0, v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v5

    float-to-int v5, v5

    invoke-static {p0, v6}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDpToPx(Landroid/content/Context;I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 62
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$color;->dialog_item_text:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 63
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/sec/android/app/shealth/common/commonui/R$color;->dialog_item_text:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v7, v7, v7, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 64
    const-string/jumbo v3, "sec-roboto-light"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 65
    const/4 v3, 0x2

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 66
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 70
    :cond_0
    invoke-static {v1, p3, p4}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->setContentDescription(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-object v0
.end method
