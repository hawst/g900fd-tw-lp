.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;
.super Ljava/lang/Object;
.source "ExerciseProActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0

    .prologue
    .line 922
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 926
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    instance-of v8, v8, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShareViaRunning:Z
    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 928
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v8, v8, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mCurrentFragment:Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    check-cast v8, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/plugins/exercisemate/ExerciseMateGraphFragmentSIC;->getChartReadyToShown()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 931
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->isShareViaRunning:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$1002(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Z)Z

    .line 932
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a015f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 933
    .local v7, "topMargin":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    const v9, 0x1020002

    invoke-virtual {v8, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 934
    .local v6, "shareView":Landroid/view/View;
    const v8, 0x7f08036d

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 935
    .local v3, "generalView":Landroid/widget/LinearLayout;
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v3, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    instance-of v8, v8, Landroid/view/TextureView;

    if-nez v8, :cond_1

    .line 957
    .end local v3    # "generalView":Landroid/widget/LinearLayout;
    .end local v6    # "shareView":Landroid/view/View;
    .end local v7    # "topMargin":I
    :cond_0
    :goto_0
    return-void

    .line 939
    .restart local v3    # "generalView":Landroid/widget/LinearLayout;
    .restart local v6    # "shareView":Landroid/view/View;
    .restart local v7    # "topMargin":I
    :cond_1
    invoke-virtual {v3, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/TextureView;

    .line 940
    .local v2, "chartView":Landroid/view/TextureView;
    invoke-virtual {v2}, Landroid/view/TextureView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 942
    .local v1, "chartBitmap":Landroid/graphics/Bitmap;
    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->getScreenshot(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 944
    .local v5, "parentBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    sub-int/2addr v9, v7

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 946
    .local v4, "overlay":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 948
    .local v0, "canvas":Landroid/graphics/Canvas;
    new-instance v8, Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-direct {v8, v12, v7, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v9, Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    sub-int/2addr v11, v7

    invoke-direct {v9, v12, v12, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v5, v8, v9, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 950
    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLeft()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getTop()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {v0, v1, v8, v9, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 952
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$8;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-static {v8, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createChartShareView(Landroid/app/Activity;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
