.class Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$16;
.super Ljava/lang/Object;
.source "ExerciseProTimeGoalActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 590
    move-object v0, p1

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 592
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 604
    :goto_0
    return-void

    .line 594
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 595
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 596
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->hourEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 598
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->checkChangesAndExit()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)V

    goto :goto_0

    .line 601
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity$16;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->checkDataAndSave()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;->access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/goalactivity/ExerciseProTimeGoalActivity;)V

    goto :goto_0

    .line 592
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
