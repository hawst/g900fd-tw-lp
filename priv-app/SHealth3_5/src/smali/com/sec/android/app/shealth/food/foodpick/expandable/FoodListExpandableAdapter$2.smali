.class Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;
.super Ljava/lang/Object;
.source "FoodListExpandableAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->initFavoriteButtonListener(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

.field final synthetic val$categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

.field final synthetic val$listItem:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    iput-object p3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->val$categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "favoriteWrapper"    # Landroid/view/View;

    .prologue
    const v6, 0x7f0803af

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 365
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 366
    .local v2, "isChecked":Z
    if-nez v2, :cond_0

    .line 368
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    iget-object v3, v3, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->isFavoriteItemsLimitExceed(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 369
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    iget-object v3, v3, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    iget-object v6, v6, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mContext:Landroid/content/Context;

    const v7, 0x7f090926

    new-array v4, v4, [Ljava/lang/Object;

    const/16 v8, 0x1e

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-virtual {v6, v7, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 384
    :goto_0
    return-void

    .line 375
    :cond_0
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    if-nez v2, :cond_1

    :goto_1
    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 376
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->val$categoryHolder:Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->performClickOnFavoriteButton(Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;Lcom/sec/android/app/shealth/food/foodpick/expandable/CategoryDataHolder;)V

    .line 378
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnFavoriteClickListeners:Ljava/util/Set;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->access$000(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;)Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 379
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->mOnFavoriteClickListeners:Ljava/util/Set;
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->access$000(Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;

    .line 380
    .local v0, "favoriteClickListener":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->val$listItem:Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;

    invoke-interface {v0, v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;->onFavoriteClick(Lcom/sec/android/app/shealth/food/foodpick/expandable/FavoritableListItem;)V

    goto :goto_2

    .end local v0    # "favoriteClickListener":Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener;, "Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$OnFavoriteClickListener<Lcom/sec/android/app/shealth/food/foodpick/expandable/ListItem;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    move v4, v5

    .line 375
    goto :goto_1

    .line 383
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter$2;->this$0:Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/foodpick/expandable/FoodListExpandableAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
