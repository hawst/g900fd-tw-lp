.class public Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;
.super Ljava/lang/Object;
.source "HomeFavOrderPrefHelper.java"


# static fields
.field private static final FAVORITE_COUNT_KEY:Ljava/lang/String; = "FavoriteCount"

.field private static final INITIALISATION_STATUS:Ljava/lang/String; = "INITIALISATION"

.field private static final PREF_FILE_NAME:Ljava/lang/String; = "home_fav_order"

.field private static editor:Landroid/content/SharedPreferences$Editor;

.field private static mPref:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "home_fav_order"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->mPref:Landroid/content/SharedPreferences;

    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clear()V
    .locals 1

    .prologue
    .line 170
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 171
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 172
    return-void
.end method

.method public static containsKey(Ljava/lang/String;)Z
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getOrder(Ljava/lang/String;)I
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->readInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getTotalFavoriteCount()I
    .locals 2

    .prologue
    .line 110
    const-string v0, "FavoriteCount"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->readInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static isInitialised()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 89
    const-string v1, "INITIALISATION"

    invoke-static {v1}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    :goto_0
    return v0

    :cond_0
    const-string v1, "INITIALISATION"

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public static logPrefFile()V
    .locals 9

    .prologue
    .line 147
    sget-object v6, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v5

    .line 148
    .local v5, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 149
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 152
    .local v4, "key":Ljava/lang/String;
    const-string v6, "EditFavorites"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "KEY : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const-string v6, "INITIALISATION"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 155
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 156
    .local v0, "a":Z
    const-string v6, "EditFavorites"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "VALUE : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 160
    .end local v0    # "a":Z
    :cond_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 161
    .local v1, "b":I
    const-string v6, "EditFavorites"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "VALUE : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 164
    .end local v1    # "b":I
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v4    # "key":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private static readBoolean(Ljava/lang/String;Z)Z
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 53
    sget-object v1, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1, p0, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 54
    .local v0, "value":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .end local v0    # "value":Ljava/lang/Boolean;
    .end local p1    # "defaultValue":Z
    :goto_0
    return p1

    .restart local v0    # "value":Ljava/lang/Boolean;
    .restart local p1    # "defaultValue":Z
    :cond_0
    check-cast v0, Ljava/lang/Boolean;

    .end local v0    # "value":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0
.end method

.method private static readInt(Ljava/lang/String;I)I
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 68
    sget-object v1, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v1, p0, p1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 70
    .local v0, "value":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .end local v0    # "value":Ljava/lang/Integer;
    .end local p1    # "defaultValue":I
    :goto_0
    return p1

    .restart local v0    # "value":Ljava/lang/Integer;
    .restart local p1    # "defaultValue":I
    :cond_0
    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "value":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0
.end method

.method public static remove(Ljava/lang/String;)V
    .locals 0
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 139
    invoke-static {p0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->removeKey(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method private static removeKey(Ljava/lang/String;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 41
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 42
    return-void
.end method

.method public static setIsInitialised(Z)V
    .locals 1
    .param p0, "isIntialised"    # Z

    .prologue
    .line 80
    const-string v0, "INITIALISATION"

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->writeBoolean(Ljava/lang/String;Z)V

    .line 81
    return-void
.end method

.method public static setOrder(Ljava/lang/String;I)V
    .locals 0
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "order"    # I

    .prologue
    .line 120
    invoke-static {p0, p1}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->writeInt(Ljava/lang/String;I)V

    .line 121
    return-void
.end method

.method public static setTotalFavoriteCount(I)V
    .locals 1
    .param p0, "count"    # I

    .prologue
    .line 101
    const-string v0, "FavoriteCount"

    invoke-static {v0, p0}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->writeInt(Ljava/lang/String;I)V

    .line 102
    return-void
.end method

.method public static updateOrderOnDelete(I)V
    .locals 8
    .param p0, "vacantPosition"    # I

    .prologue
    .line 180
    sget-object v5, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->mPref:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    .line 181
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 182
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 184
    .local v2, "key":Ljava/lang/String;
    const-string v5, "EditFavorites"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "KEY : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    const-string v5, "INITIALISATION"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "FavoriteCount"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 187
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 188
    .local v4, "position":I
    if-le v4, p0, :cond_0

    .line 190
    add-int/lit8 v5, v4, -0x1

    invoke-static {v2, v5}, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->writeInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 196
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "position":I
    :cond_1
    return-void
.end method

.method private static writeBoolean(Ljava/lang/String;Z)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Z

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 48
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 49
    return-void
.end method

.method private static writeInt(Ljava/lang/String;I)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # I

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 62
    sget-object v0, Lcom/sec/android/app/shealth/home/data/HomeFavOrderPrefHelper;->editor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 63
    return-void
.end method
