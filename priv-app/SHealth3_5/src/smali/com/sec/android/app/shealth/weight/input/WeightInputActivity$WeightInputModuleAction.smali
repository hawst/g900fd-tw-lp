.class Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;
.super Ljava/lang/Object;
.source "WeightInputActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$InputModuleAlertAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WeightInputModuleAction"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private inputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;Landroid/content/Context;Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "input"    # Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439
    iput-object p2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->context:Landroid/content/Context;

    .line 440
    iput-object p3, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->inputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    .line 441
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    .prologue
    .line 467
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->inputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-static {}, Lcom/sec/android/app/shealth/weight/common/RangeConstants;->getInputRangeMin()F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {v2}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$400(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->setValue(F)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->inputModule:Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/HorizontalInputModule;->getFloatEditTextView()Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/input/FloatEditText;->selectAll()V

    .line 469
    return-void
.end method

.method public getAlertMessage()Ljava/lang/String;
    .locals 13

    .prologue
    const v12, 0x7f090c96

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 446
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    invoke-virtual {v5, v12}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {}, Lcom/sec/android/app/shealth/weight/common/RangeConstants;->getInputRangeMin()F

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {v8}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$400(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 447
    .local v2, "strMin":Ljava/lang/String;
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    invoke-virtual {v5, v12}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {}, Lcom/sec/android/app/shealth/weight/common/RangeConstants;->getInputRangeMax()F

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {v8}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$400(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 448
    .local v1, "strMax":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$400(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->context:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->getUnitName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 450
    .local v3, "strUnit":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    const v6, 0x7f090846

    invoke-virtual {v5, v6}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 452
    .local v0, "message":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$400(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->KG_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 453
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    const v6, 0x7f090841

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    float-to-int v8, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 457
    :cond_0
    :goto_0
    return-object v0

    .line 454
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    # getter for: Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;
    invoke-static {v4}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->access$400(Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;)Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->LB_UNIT:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 455
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->this$0:Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;

    const v6, 0x7f090844

    new-array v7, v11, [Ljava/lang/Object;

    aput-object v2, v7, v9

    aput-object v1, v7, v10

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getAlertTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/input/WeightInputActivity$WeightInputModuleAction;->context:Landroid/content/Context;

    const v1, 0x7f090ae2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
