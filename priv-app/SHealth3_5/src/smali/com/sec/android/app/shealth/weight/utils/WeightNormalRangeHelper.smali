.class public final Lcom/sec/android/app/shealth/weight/utils/WeightNormalRangeHelper;
.super Ljava/lang/Object;
.source "WeightNormalRangeHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNormalRangeValues(Landroid/content/Context;F)Landroid/util/Pair;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "heightInCm"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "F)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    new-instance v4, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v0

    .line 56
    .local v0, "currentUnit":Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->convertCmToMeter(F)F

    move-result v1

    .line 57
    .local v1, "heightInMeter":F
    const/high16 v4, 0x41940000    # 18.5f

    mul-float/2addr v4, v1

    mul-float v3, v4, v1

    .line 58
    .local v3, "minNormalWeightRangeValue":F
    const/high16 v4, 0x41b80000    # 23.0f

    mul-float/2addr v4, v1

    mul-float v2, v4, v1

    .line 59
    .local v2, "maxNormalWeightRangeValue":F
    new-instance v4, Landroid/util/Pair;

    invoke-static {v3, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-static {v2, v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->convertKgToUnit(FLcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4
.end method

.method public static isInNormalRange(Landroid/content/Context;FF)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "weightValue"    # F
    .param p2, "heightInCm"    # F

    .prologue
    .line 44
    invoke-static {p0, p2}, Lcom/sec/android/app/shealth/weight/utils/WeightNormalRangeHelper;->getNormalRangeValues(Landroid/content/Context;F)Landroid/util/Pair;

    move-result-object v0

    .line 45
    .local v0, "normalRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Float;Ljava/lang/Float;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {p1, v2, v1}, Lcom/sec/android/app/shealth/common/utils/FloatUtils;->isInRange(FFF)Z

    move-result v1

    return v1
.end method
