.class Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$3;
.super Ljava/lang/Object;
.source "AMapExerciseProStatusFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->initLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)V
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 527
    invoke-virtual {p1, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 529
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->mWorkoutController:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseProWorkoutController;->setMusicUpdate(Z)V

    .line 530
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->updateMusicPlayerText(Ljava/lang/String;)V

    .line 531
    const-string v3, "com.sec.android.app.music"

    .line 532
    .local v3, "pkgname":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 533
    .local v2, "pkgmanager":Landroid/content/pm/PackageManager;
    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 534
    .local v1, "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment$3;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    const/16 v5, 0x3039

    invoke-virtual {v4, v1, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 538
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pkgmanager":Landroid/content/pm/PackageManager;
    .end local v3    # "pkgname":Ljava/lang/String;
    :goto_0
    return-void

    .line 535
    :catch_0
    move-exception v0

    .line 536
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
