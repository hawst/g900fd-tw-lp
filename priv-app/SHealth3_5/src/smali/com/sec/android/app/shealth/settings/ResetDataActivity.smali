.class public Lcom/sec/android/app/shealth/settings/ResetDataActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ResetDataActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/ResetDataActivity$9;,
        Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataButtonController;,
        Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItemHolder;,
        Lcom/sec/android/app/shealth/settings/ResetDataActivity$ItemViewHolder;,
        Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;,
        Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;
    }
.end annotation


# static fields
.field private static final PRELOADED_PLUGINS:I = 0x0

.field private static final RESET_DATA_POPUP:Ljava/lang/String; = "reset_data_popup"

.field private static final TAG:Ljava/lang/String; = "ResetDataActivity"


# instance fields
.field private actionButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

.field private checkedDbResettableAppListItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;",
            ">;"
        }
    .end annotation
.end field

.field private emptyView:Landroid/widget/TextView;

.field private headerPositions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private listregistry:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;",
            ">;"
        }
    .end annotation
.end field

.field private final mDialogControllerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;",
            ">;"
        }
    .end annotation
.end field

.field private mcheckedItem:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private resetDataListItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;",
            ">;"
        }
    .end annotation
.end field

.field private resetableDbAppList:Landroid/widget/ListView;

.field private resettableDbAdapter:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetDataListItem:Ljava/util/ArrayList;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->headerPositions:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->mcheckedItem:Ljava/util/Map;

    .line 95
    new-instance v0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->mDialogControllerMap:Ljava/util/Map;

    .line 525
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetData()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->mcheckedItem:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ResetDataActivity;

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->refreshFocusables()V

    return-void
.end method

.method private initializeDbResetableAppList()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v7, v4}, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryDbManagerWithProvider;->getAllAppRegistryDatas(ZLandroid/content/Context;)Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    iput-object v4, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->listregistry:Ljava/util/ArrayList;

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09002a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 126
    .local v1, "moreapps":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090242

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 128
    .local v3, "tgh":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->listregistry:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 130
    .local v2, "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    iget v4, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appType:I

    if-nez v4, :cond_0

    .line 132
    iget-object v4, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    const v5, 0x7f090029

    invoke-virtual {p0, v5}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 133
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/FullVersionCountryTable;->isUseCigna()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 134
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetDataListItem:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;

    iget-object v6, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->extra:Ljava/lang/String;

    invoke-direct {v5, v2, v6, v7}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;-><init>(Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;Ljava/lang/String;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 137
    :cond_1
    iget-object v4, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->appName:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 140
    iget-object v4, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetDataListItem:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;

    iget-object v6, v2, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->extra:Ljava/lang/String;

    invoke-direct {v5, v2, v6, v7}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;-><init>(Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;Ljava/lang/String;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    .end local v2    # "temp":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :cond_2
    return-void
.end method

.method private resetData()V
    .locals 3

    .prologue
    .line 552
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 564
    :goto_0
    return-void

    .line 557
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 560
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->checkedDbResettableAppListItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;

    iget-object v0, v2, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetDataListItem;->mAppregistryData:Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;

    .line 561
    .local v0, "appRegistryData":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;->actions:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetPreloadedPluginData(Ljava/lang/String;)V

    .line 557
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 563
    .end local v0    # "appRegistryData":Lcom/sec/android/app/shealth/framework/ui/data/AppRegistryData;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->finish()V

    goto :goto_0
.end method

.method private resetPreloadedPluginData(Ljava/lang/String;)V
    .locals 5
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 214
    const-string v3, "com.sec.shealth.action.PEDOMETER"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 216
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/shealth/settings/ResetDataActivity$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 327
    :goto_0
    return-void

    .line 241
    :cond_0
    const-string v3, "com.sec.shealth.action.EXERCISE"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 243
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/shealth/settings/ResetDataActivity$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 263
    :cond_1
    const-string v3, "com.sec.shealth.action.HEART_RATE"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 264
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/shealth/settings/ResetDataActivity$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 271
    :cond_2
    const-string v3, "com.sec.shealth.action.FOOD"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 273
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/shealth/settings/ResetDataActivity$6;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$6;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 283
    :cond_3
    const-string v3, "com.sec.shealth.action.COACH"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 286
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.shealth.action.RESET_COACH_DATA"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 287
    .local v0, "mCoachResetIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 289
    .end local v0    # "mCoachResetIntent":Landroid/content/Intent;
    :cond_4
    const-string v3, "com.sec.shealth.action.WEIGHT"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 291
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/shealth/settings/ResetDataActivity$7;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$7;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 299
    :cond_5
    const-string v3, "com.sec.shealth.action.SLEEP"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 301
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/shealth/settings/ResetDataActivity$8;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$8;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 310
    :cond_6
    const-string v3, "com.sec.shealth.action.STRESS"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 312
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.shealth.action.RESET_STRESS_DATA"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 313
    .local v2, "mStressResetIntent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 316
    .end local v2    # "mStressResetIntent":Landroid/content/Intent;
    :cond_7
    const-string v3, "com.sec.shealth.action.UV"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/uv/common/UvDatabaseHelper;->deleteAllData()Z

    goto/16 :goto_0

    .line 319
    :cond_8
    const-string v3, "com.sec.shealth.action.SPO2"

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 321
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.shealth.action.RESET_SPO2_DATA"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 322
    .local v1, "mSpO2ResetIntent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 325
    .end local v1    # "mSpO2ResetIntent":Landroid/content/Intent;
    :cond_9
    const-string v3, "ResetDataActivity"

    const-string v4, "No Preloaded plugin is selected"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 148
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f0907e8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 151
    new-instance v0, Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;)V

    .line 207
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f090044

    invoke-direct {v1, v4, v2, v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->actionButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->actionButton:Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarButtonVisiblity(ZI)V

    .line 210
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 546
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->mDialogControllerMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const v1, 0x7f0301f4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->setContentView(I)V

    .line 107
    if-eqz p1, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "reset_data_popup"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 109
    .local v0, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 112
    .end local v0    # "dialogFragment":Landroid/support/v4/app/DialogFragment;
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetDataListItem:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->headerPositions:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2, v3}, Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;-><init>(Lcom/sec/android/app/shealth/settings/ResetDataActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resettableDbAdapter:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->initializeDbResetableAppList()V

    .line 116
    const v1, 0x7f0808ab

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetableDbAppList:Landroid/widget/ListView;

    .line 117
    const v1, 0x7f080009

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->emptyView:Landroid/widget/TextView;

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetableDbAppList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->emptyView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resetableDbAppList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ResetDataActivity;->resettableDbAdapter:Lcom/sec/android/app/shealth/settings/ResetDataActivity$ResetableDbAppsAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 120
    return-void
.end method
