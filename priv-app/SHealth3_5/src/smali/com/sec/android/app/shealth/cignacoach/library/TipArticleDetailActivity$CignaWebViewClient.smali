.class Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$CignaWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "TipArticleDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CignaWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$CignaWebViewClient;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 227
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$CignaWebViewClient;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    const v1, 0x7f0801db

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 230
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "failingUrl"    # Ljava/lang/String;

    .prologue
    .line 235
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$CignaWebViewClient;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    const v1, 0x7f0801db

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 238
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 2
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "handler"    # Landroid/webkit/SslErrorHandler;
    .param p3, "error"    # Landroid/net/http/SslError;

    .prologue
    .line 243
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity$CignaWebViewClient;->this$0:Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    const v1, 0x7f0801db

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 246
    return-void
.end method
