.class Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;
.super Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;
.source "MealItemsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/OnSingleClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickAction(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 213
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/common/utils/SoftInputUtils;->hideSoftInput(Landroid/app/Activity;)V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # invokes: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->handleMemoFocus()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$100(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)V

    .line 215
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/food/mealinput/nutrition/NutritionInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 216
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "NUTRITION_DATA"

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    iget-object v1, v1, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mMealItems:Ljava/util/List;

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->startActivity(Landroid/content/Intent;)V

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;->access$200(Lcom/sec/android/app/shealth/food/mealinput/mealitemsfragment/MealItemsFragment;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/shealth/food/app/UserActionLog;->VIEW_VIEW_NUTRITION_INFO_BY_BUTTON:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v1, v2}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    .line 219
    return-void
.end method
