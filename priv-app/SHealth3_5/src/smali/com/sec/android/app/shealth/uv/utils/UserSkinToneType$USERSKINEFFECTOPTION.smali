.class public final enum Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;
.super Ljava/lang/Enum;
.source "UserSkinToneType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "USERSKINEFFECTOPTION"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

.field public static final enum INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

.field public static final enum SKIN_EFFECT_1:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

.field public static final enum SKIN_EFFECT_2:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

.field public static final enum SKIN_EFFECT_3:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

.field public static final enum SKIN_EFFECT_4:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

.field public static final enum SKIN_EFFECT_5:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

.field public static final enum SKIN_EFFECT_6:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 11
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    const-string v1, "INVALID"

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 12
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    const-string v1, "SKIN_EFFECT_1"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_1:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 13
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    const-string v1, "SKIN_EFFECT_2"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_2:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 14
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    const-string v1, "SKIN_EFFECT_3"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_3:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 15
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    const-string v1, "SKIN_EFFECT_4"

    invoke-direct {v0, v1, v7, v7}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_4:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 16
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    const-string v1, "SKIN_EFFECT_5"

    invoke-direct {v0, v1, v8, v8}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_5:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 17
    new-instance v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    const-string v1, "SKIN_EFFECT_6"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_6:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    .line 10
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->INVALID:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    aput-object v2, v0, v1

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_1:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_2:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_3:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_4:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_5:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->SKIN_EFFECT_6:Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->$VALUES:[Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->value:I

    .line 23
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->$VALUES:[Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/shealth/uv/utils/UserSkinToneType$USERSKINEFFECTOPTION;->value:I

    return v0
.end method
