.class public interface abstract Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;
.super Ljava/lang/Object;
.source "IMealActivityTopFragmentAction.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# static fields
.field public static final ALL_DIALOGS_TAGS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/IMealActivityTopFragmentAction;->ALL_DIALOGS_TAGS:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public abstract getMealType()I
.end method

.method public abstract getTimeInMillis()J
.end method

.method public abstract setEnableMealTypeChoice(Z)V
.end method

.method public abstract setMealType(I)V
.end method

.method public abstract setTimeInMillis(J)V
.end method
