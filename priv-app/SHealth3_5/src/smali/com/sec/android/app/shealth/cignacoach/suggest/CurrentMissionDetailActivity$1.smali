.class Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$1;
.super Ljava/lang/Object;
.source "CurrentMissionDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->updateHeaderView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoItAgainClick()V
    .locals 3

    .prologue
    .line 168
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/suggest/SuggestedMissionsDetailActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 169
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_GOAL_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 170
    const-string v1, "EXTRA_NAME_MISSION_ID"

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 172
    return-void
.end method
