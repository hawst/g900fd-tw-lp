.class public Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "ExerciseProDetailsMapFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# static fields
.field private static final MIN_LOCATION_VALUE:F = 1.0E-5f

.field private static final POLYLINE_DEFAULT_COLOR:I

.field private static POLYLINE_DEFAULT_WIDTH:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ExerciseProDetailsMapFragment"

.field private static mBackupDataType:I

.field private static mBackupIntent:Landroid/content/Intent;

.field private static mBackupRowId:J

.field public static mMapMode:I


# instance fields
.field private final FIVE_THOUSAND_METERS:I

.field private SPACE:Ljava/lang/String;

.field private final TEN_THOUSAND_METERS:I

.field bpm:I

.field private currentZoomLevel:F

.field private delay:Z

.field hrBpmText:Landroid/widget/TextView;

.field private hrBpmTextNonMedical:Landroid/widget/TextView;

.field hrGuideText:Landroid/widget/TextView;

.field hrmPopUpLayout:Landroid/widget/RelativeLayout;

.field hrmText:Landroid/widget/TextView;

.field private layoutMedical:Landroid/widget/LinearLayout;

.field private layoutNonMedical:Landroid/widget/LinearLayout;

.field private mActivityType:I

.field private mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mAvgHrm:F

.field private mAvgSpeed:F

.field private mBundle:Landroid/os/Bundle;

.field private mCalories:F

.field private mContentView:Landroid/view/View;

.field private mDataType:I

.field private mDistance:F

.field private mDuration:F

.field private mElevation:F

.field private mEndMode:Z

.field private mExerciseId:J

.field private mFastSpeed:Landroid/widget/TextView;

.field private mIntent:Landroid/content/Intent;

.field private mIsMiniMode:Z

.field public mMap:Lcom/google/android/gms/maps/GoogleMap;

.field private mMapClickListener:Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

.field private mMapHRMList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;"
        }
    .end annotation
.end field

.field private mMapMarkerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/maps/model/Marker;",
            ">;"
        }
    .end annotation
.end field

.field mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

.field private mMapView:Lcom/google/android/gms/maps/MapView;

.field mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

.field private mMaxHrm:F

.field private mMaxSpeed:F

.field private mMinSpeed:F

.field private mNoDataNotiText:Landroid/widget/TextView;

.field private mPhotoDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation
.end field

.field mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

.field mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

.field private mPlaceMode:I

.field mPolygonHeight:I

.field mPolygonWidth:I

.field private mRowId:J

.field private mSlowSpeed:Landroid/widget/TextView;

.field private mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

.field private mSync_time:J

.field private mTryit_map:Z

.field private mUiSettings:Lcom/google/android/gms/maps/UiSettings;

.field maxBpmText:Landroid/widget/TextView;

.field maxHRzone:I

.field minBpmText:Landroid/widget/TextView;

.field minHRzone:I

.field rangeText:Landroid/widget/TextView;

.field private viewModeHandler:Landroid/os/Handler;

.field private viewModeRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 113
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_WIDTH:I

    .line 114
    const/16 v0, 0xff

    const/4 v1, 0x0

    const/16 v2, 0xb7

    const/16 v3, 0xc7

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_COLOR:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 183
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 117
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapMarkerList:Ljava/util/ArrayList;

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    .line 129
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIsMiniMode:Z

    .line 142
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->currentZoomLevel:F

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    .line 154
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->delay:Z

    .line 155
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->viewModeHandler:Landroid/os/Handler;

    .line 156
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    .line 166
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->TEN_THOUSAND_METERS:I

    .line 167
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->FIVE_THOUSAND_METERS:I

    .line 175
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->viewModeRunnable:Ljava/lang/Runnable;

    .line 791
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .line 808
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    .line 185
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupDataType:I

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDataType:I

    .line 186
    sget-wide v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupRowId:J

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    .line 187
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupIntent:Landroid/content/Intent;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    .line 189
    return-void
.end method

.method public constructor <init>(IJLandroid/content/Intent;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "rowId"    # J
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 192
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 117
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapMarkerList:Ljava/util/ArrayList;

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    .line 129
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIsMiniMode:Z

    .line 142
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->currentZoomLevel:F

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    .line 154
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->delay:Z

    .line 155
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->viewModeHandler:Landroid/os/Handler;

    .line 156
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    .line 166
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->TEN_THOUSAND_METERS:I

    .line 167
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->FIVE_THOUSAND_METERS:I

    .line 175
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->viewModeRunnable:Ljava/lang/Runnable;

    .line 791
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .line 808
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    .line 193
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDataType:I

    .line 194
    iput-wide p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    .line 195
    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    .line 197
    sput p1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupDataType:I

    .line 198
    sput-wide p2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupRowId:J

    .line 199
    sput-object p4, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupIntent:Landroid/content/Intent;

    .line 200
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    .line 201
    return-void
.end method

.method public constructor <init>(ZJLandroid/content/Intent;)V
    .locals 2
    .param p1, "isMiniMode"    # Z
    .param p2, "rowId"    # J
    .param p4, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 117
    const-string v0, " "

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapMarkerList:Ljava/util/ArrayList;

    .line 128
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    .line 129
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIsMiniMode:Z

    .line 142
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->currentZoomLevel:F

    .line 148
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    .line 154
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->delay:Z

    .line 155
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->viewModeHandler:Landroid/os/Handler;

    .line 156
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    .line 166
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->TEN_THOUSAND_METERS:I

    .line 167
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->FIVE_THOUSAND_METERS:I

    .line 175
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->viewModeRunnable:Ljava/lang/Runnable;

    .line 791
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$6;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    .line 808
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$7;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    .line 205
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIsMiniMode:Z

    .line 206
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDataType:I

    .line 207
    iput-wide p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    .line 208
    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    .line 210
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDataType:I

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupDataType:I

    .line 211
    sput-wide p2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupRowId:J

    .line 212
    sput-object p4, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBackupIntent:Landroid/content/Intent;

    .line 213
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    .line 214
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->delay:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->delay:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapClickListener:Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
    .param p1, "x1"    # I

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setHRMStateBar(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->currentZoomLevel:F

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
    .param p1, "x1"    # F

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->currentZoomLevel:F

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->viewModeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->viewModeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapMarkerList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutMedical:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutMedical:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutNonMedical:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->layoutNonMedical:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->hrBpmTextNonMedical:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->hrBpmTextNonMedical:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    return-object p1
.end method

.method private addListenerViewModeButton()V
    .locals 3

    .prologue
    .line 740
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mContentView:Landroid/view/View;

    const v2, 0x7f080717

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 741
    .local v0, "mapModeSetting":Landroid/widget/ImageButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09021d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 742
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 753
    return-void
.end method

.method private convertDptoPx(I)I
    .locals 5
    .param p1, "dp"    # I

    .prologue
    .line 1381
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1382
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1383
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    int-to-float v3, p1

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, v3, v4

    .line 1384
    .local v1, "px":F
    float-to-int v3, v1

    return v3
.end method

.method private drawMapRoute(Ljava/util/List;Ljava/util/List;)V
    .locals 35
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 577
    .local p1, "locationItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    .local p2, "speedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x2

    if-ge v5, v6, :cond_1

    .line 674
    :cond_0
    return-void

    .line 580
    :cond_1
    sget v20, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_COLOR:I

    .line 581
    .local v20, "lineColor":I
    sget v21, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_WIDTH:I

    .line 583
    .local v21, "lineWidth":I
    const-string v5, "ExerciseProDetailsMapFragment"

    const-string v6, "drawMapRoute()"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    const/16 v32, 0x0

    .line 587
    .local v32, "polyline":Lcom/google/android/gms/maps/model/Polyline;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v34

    .line 588
    .local v34, "speedItemsSize":I
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v23

    .line 590
    .local v23, "locationItemCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getExerciseData(Landroid/content/Context;J)Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/ExerciseData;->getDistance()F

    move-result v26

    .line 592
    .local v26, "mapPathLength":F
    const v5, 0x461c4000    # 10000.0f

    cmpl-float v5, v26, v5

    if-ltz v5, :cond_8

    .line 593
    const v5, 0x461c4000    # 10000.0f

    sub-float v5, v26, v5

    const v6, 0x459c4000    # 5000.0f

    div-float/2addr v5, v6

    float-to-int v15, v5

    .line 594
    .local v15, "factor":I
    mul-int/lit8 v5, v15, 0x5

    add-int/lit8 v5, v5, 0xa

    mul-int/lit8 v5, v5, 0x2

    int-to-float v0, v5

    move/from16 v27, v0

    .line 596
    .local v27, "minRequiredGap":F
    const/16 v16, 0x0

    .local v16, "i":I
    :cond_2
    :goto_0
    add-int/lit8 v5, v23, -0x1

    move/from16 v0, v16

    if-ge v0, v5, :cond_0

    .line 597
    const-wide/16 v13, 0x0

    .line 598
    .local v13, "distance":D
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 599
    .local v30, "path1":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    new-instance v28, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v5

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v7

    move-object/from16 v0, v28

    invoke-direct {v0, v5, v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 601
    .local v28, "p1":Lcom/google/android/gms/maps/model/LatLng;
    add-int/lit8 v17, v16, 0x1

    .local v17, "j":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_2

    .line 602
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 603
    .local v31, "path2":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    new-instance v29, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v5

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v7

    move-object/from16 v0, v29

    invoke-direct {v0, v5, v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 605
    .local v29, "p2":Lcom/google/android/gms/maps/model/LatLng;
    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v5

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v7

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v9

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v11

    invoke-static/range {v5 .. v12}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->getDistanceFromLatLngInMeter(DDDD)D

    move-result-wide v5

    add-double/2addr v13, v5

    .line 607
    move/from16 v0, v27

    float-to-double v5, v0

    cmpl-double v5, v13, v5

    if-gez v5, :cond_3

    add-int/lit8 v5, v23, -0x1

    move/from16 v0, v17

    if-ne v0, v5, :cond_7

    .line 609
    :cond_3
    const/16 v33, 0x0

    .line 610
    .local v33, "speed":F
    if-lez v34, :cond_4

    .line 611
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v33

    .line 616
    :goto_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    move/from16 v0, v33

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getLineColor(FFF)I

    move-result v20

    .line 618
    if-nez v16, :cond_5

    .line 619
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v6, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v6}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/android/gms/maps/model/LatLng;

    const/4 v8, 0x0

    aput-object v28, v7, v8

    const/4 v8, 0x1

    aput-object v29, v7, v8

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->add([Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    move/from16 v0, v21

    int-to-float v7, v0

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->zIndex(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    move-result-object v32

    .line 627
    :goto_3
    move/from16 v16, v17

    .line 628
    goto/16 :goto_0

    .line 613
    :cond_4
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    add-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float v33, v5, v6

    goto :goto_2

    .line 620
    :cond_5
    invoke-virtual/range {v32 .. v32}, Lcom/google/android/gms/maps/model/Polyline;->getColor()I

    move-result v5

    move/from16 v0, v20

    if-ne v5, v0, :cond_6

    .line 621
    invoke-virtual/range {v32 .. v32}, Lcom/google/android/gms/maps/model/Polyline;->getPoints()Ljava/util/List;

    move-result-object v22

    .line 622
    .local v22, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 623
    move-object/from16 v0, v32

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Polyline;->setPoints(Ljava/util/List;)V

    goto :goto_3

    .line 625
    .end local v22    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v6, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v6}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/android/gms/maps/model/LatLng;

    const/4 v8, 0x0

    aput-object v28, v7, v8

    const/4 v8, 0x1

    aput-object v29, v7, v8

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->add([Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    move/from16 v0, v21

    int-to-float v7, v0

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->zIndex(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    move-result-object v32

    goto :goto_3

    .line 630
    .end local v33    # "speed":F
    :cond_7
    move-object/from16 v30, v31

    .line 601
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_1

    .line 635
    .end local v13    # "distance":D
    .end local v15    # "factor":I
    .end local v16    # "i":I
    .end local v17    # "j":I
    .end local v27    # "minRequiredGap":F
    .end local v28    # "p1":Lcom/google/android/gms/maps/model/LatLng;
    .end local v29    # "p2":Lcom/google/android/gms/maps/model/LatLng;
    .end local v30    # "path1":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    .end local v31    # "path2":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    :cond_8
    const/16 v16, 0x1

    .restart local v16    # "i":I
    :goto_4
    move/from16 v0, v16

    move/from16 v1, v23

    if-ge v0, v1, :cond_0

    .line 637
    const/16 v33, 0x0

    .line 638
    .restart local v33    # "speed":F
    if-lez v34, :cond_a

    .line 639
    add-int/lit8 v5, v34, -0x1

    move/from16 v0, v16

    if-gt v5, v0, :cond_9

    .line 640
    add-int/lit8 v5, v34, -0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v33

    .line 648
    :goto_5
    add-int/lit8 v5, v16, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 649
    .restart local v30    # "path1":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 651
    .restart local v31    # "path2":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v18

    .line 652
    .local v18, "lat":D
    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v24

    .line 653
    .local v24, "lon":D
    new-instance v28, Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v0, v28

    move-wide/from16 v1, v18

    move-wide/from16 v3, v24

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 655
    .restart local v28    # "p1":Lcom/google/android/gms/maps/model/LatLng;
    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v18

    .line 656
    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v24

    .line 657
    new-instance v29, Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v0, v29

    move-wide/from16 v1, v18

    move-wide/from16 v3, v24

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 659
    .restart local v29    # "p2":Lcom/google/android/gms/maps/model/LatLng;
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    move/from16 v0, v33

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getLineColor(FFF)I

    move-result v20

    .line 661
    const/4 v5, 0x1

    move/from16 v0, v16

    if-ne v0, v5, :cond_b

    .line 662
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v6, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v6}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/android/gms/maps/model/LatLng;

    const/4 v8, 0x0

    aput-object v28, v7, v8

    const/4 v8, 0x1

    aput-object v29, v7, v8

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->add([Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    move/from16 v0, v21

    int-to-float v7, v0

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->zIndex(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    move-result-object v32

    .line 635
    :goto_6
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_4

    .line 642
    .end local v18    # "lat":D
    .end local v24    # "lon":D
    .end local v28    # "p1":Lcom/google/android/gms/maps/model/LatLng;
    .end local v29    # "p2":Lcom/google/android/gms/maps/model/LatLng;
    .end local v30    # "path1":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    .end local v31    # "path2":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    :cond_9
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v33

    goto/16 :goto_5

    .line 645
    :cond_a
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    add-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float v33, v5, v6

    goto/16 :goto_5

    .line 664
    .restart local v18    # "lat":D
    .restart local v24    # "lon":D
    .restart local v28    # "p1":Lcom/google/android/gms/maps/model/LatLng;
    .restart local v29    # "p2":Lcom/google/android/gms/maps/model/LatLng;
    .restart local v30    # "path1":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    .restart local v31    # "path2":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    :cond_b
    invoke-virtual/range {v32 .. v32}, Lcom/google/android/gms/maps/model/Polyline;->getColor()I

    move-result v5

    move/from16 v0, v20

    if-ne v5, v0, :cond_c

    .line 665
    invoke-virtual/range {v32 .. v32}, Lcom/google/android/gms/maps/model/Polyline;->getPoints()Ljava/util/List;

    move-result-object v22

    .line 666
    .restart local v22    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 667
    move-object/from16 v0, v32

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/Polyline;->setPoints(Ljava/util/List;)V

    goto :goto_6

    .line 669
    .end local v22    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v6, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v6}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/android/gms/maps/model/LatLng;

    const/4 v8, 0x0

    aput-object v28, v7, v8

    const/4 v8, 0x1

    aput-object v29, v7, v8

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->add([Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    move/from16 v0, v21

    int-to-float v7, v0

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/PolylineOptions;->zIndex(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    move-result-object v32

    goto :goto_6
.end method

.method private drawNoSpeedDBMapRoute(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 677
    .local p1, "locationItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_COLOR:I

    .line 678
    .local v1, "lineColor":I
    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->POLYLINE_DEFAULT_WIDTH:I

    .line 679
    .local v3, "lineWidth":I
    iget v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    iput v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    .line 680
    const-string v4, "ExerciseProDetailsMapFragment"

    const-string v5, "drawMapRoute()"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    const-string v4, "ExerciseProDetailsMapFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ">>> locationItems="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",  <<<"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 693
    :goto_0
    return-void

    .line 686
    :cond_0
    new-instance v2, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    .line 687
    .local v2, "lineOpt":Lcom/google/android/gms/maps/model/PolylineOptions;
    int-to-float v4, v3

    invoke-virtual {v2, v4}, Lcom/google/android/gms/maps/model/PolylineOptions;->width(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/gms/maps/model/PolylineOptions;->color(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    .line 689
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 690
    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v6

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v5}, Lcom/google/android/gms/maps/model/PolylineOptions;->add(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/PolylineOptions;

    .line 689
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 692
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/maps/GoogleMap;->addPolyline(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/Polyline;

    goto :goto_0
.end method

.method private drawRoute(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 569
    .local p1, "speedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;>;"
    .local p2, "historyLocation":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    .local p3, "historySpeed":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 570
    :cond_0
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->drawNoSpeedDBMapRoute(Ljava/util/List;)V

    .line 574
    :goto_0
    return-void

    .line 572
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->drawMapRoute(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method private getDeviceGroupType(Ljava/lang/String;)J
    .locals 12
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, 0x0

    .line 1021
    if-nez p1, :cond_1

    move-wide v0, v10

    .line 1043
    :cond_0
    :goto_0
    return-wide v0

    .line 1023
    :cond_1
    const-string v0, "_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 1024
    .local v9, "tempDeviceIDArray":[Ljava/lang/String;
    move-object v8, p1

    .line 1025
    .local v8, "tempDeviceID":Ljava/lang/String;
    const/4 v0, 0x0

    aget-object v0, v9, v0

    const-string v1, ""

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1026
    .local v6, "deviceID":Ljava/lang/String;
    const-string v0, "_"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1028
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select device_group_type, device_id from user_device where device_id == \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1030
    .local v3, "tempSelection":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1032
    .local v7, "tempCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1033
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 1035
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1036
    const-string v0, "device_group_type"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1040
    if-eqz v7, :cond_0

    .line 1041
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1040
    :cond_3
    if-eqz v7, :cond_4

    .line 1041
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    move-wide v0, v10

    .line 1043
    goto :goto_0

    .line 1040
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_5

    .line 1041
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private getExtras()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 272
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_workout_end_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_during_try_it"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPlaceMode:I

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_time"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mSync_time:J

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_exercise_info_db_id"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mExerciseId:J

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_activity_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mActivityType:I

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_distance"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDistance:F

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_duration"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDuration:F

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_calories"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mCalories:F

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_max_hrm"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxHrm:F

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_max_speed"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_avg_hrm"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAvgHrm:F

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_avg_speed"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAvgSpeed:F

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_elevation"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mElevation:F

    goto/16 :goto_0
.end method

.method private getHeartRateData()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 945
    const/4 v12, 0x0

    .line 946
    .local v12, "hrmValue":I
    const-wide/16 v9, 0x0

    .local v9, "heartrateSampleTime":J
    const-wide/16 v7, 0x0

    .line 947
    .local v7, "deviceGroupType":J
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v13

    const-string v0, "heart_rate_per_min"

    aput-object v0, v2, v14

    const/4 v0, 0x2

    const-string/jumbo v1, "sample_time"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v2, v0

    .line 951
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "exercise__id =?  AND user_device__id LIKE \'10008%\'"

    .line 953
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v14, [Ljava/lang/String;

    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v13

    .line 956
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 957
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v6, 0x0

    .line 960
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 962
    if-eqz v6, :cond_2

    .line 963
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 964
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 965
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 966
    const-string v0, "heart_rate_per_min"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 967
    const-string/jumbo v0, "sample_time"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 968
    const-string/jumbo v0, "user_device__id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getDeviceGroupType(Ljava/lang/String;)J

    move-result-wide v7

    .line 969
    const-wide/32 v0, 0x57e41

    cmp-long v0, v7, v0

    if-nez v0, :cond_0

    .line 970
    new-instance v11, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    invoke-direct {v11}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;-><init>()V

    .line 971
    .local v11, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    int-to-float v0, v12

    invoke-virtual {v11, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setHeartRateVal(F)V

    .line 972
    invoke-virtual {v11, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setSampleTime(J)V

    .line 973
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 975
    .end local v11    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 982
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 983
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 982
    :cond_2
    if-eqz v6, :cond_3

    .line 983
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 986
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 987
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setLocationtoHRMData()V

    .line 988
    :cond_4
    return-void
.end method

.method private getKmFromMeter(J)Ljava/lang/String;
    .locals 3
    .param p1, "metervalue"    # J

    .prologue
    .line 728
    const-wide/16 v1, 0x64

    div-long v1, p1, v1

    long-to-int v1, v1

    mul-int/lit8 v1, v1, 0x64

    int-to-float v0, v1

    .line 729
    .local v0, "maxSpeed":F
    float-to-long v1, v0

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/UnitUtils$UnitConverter;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getMaxHR(I)I
    .locals 5
    .param p1, "age"    # I

    .prologue
    const/16 v4, 0x32

    const/16 v3, 0x2d

    const/16 v2, 0x28

    const/16 v1, 0x23

    const/16 v0, 0x1e

    .line 1408
    if-lez p1, :cond_0

    if-ge p1, v0, :cond_0

    .line 1409
    const/16 v0, 0xc8

    .line 1428
    :goto_0
    return v0

    .line 1410
    :cond_0
    if-lt p1, v0, :cond_1

    if-ge p1, v1, :cond_1

    .line 1411
    const/16 v0, 0xbe

    goto :goto_0

    .line 1412
    :cond_1
    if-lt p1, v1, :cond_2

    if-ge p1, v2, :cond_2

    .line 1413
    const/16 v0, 0xb9

    goto :goto_0

    .line 1414
    :cond_2
    if-lt p1, v2, :cond_3

    if-ge p1, v3, :cond_3

    .line 1415
    const/16 v0, 0xb4

    goto :goto_0

    .line 1416
    :cond_3
    if-lt p1, v3, :cond_4

    if-ge p1, v4, :cond_4

    .line 1417
    const/16 v0, 0xaf

    goto :goto_0

    .line 1418
    :cond_4
    if-lt p1, v4, :cond_5

    const/16 v0, 0x37

    if-ge p1, v0, :cond_5

    .line 1419
    const/16 v0, 0xaa

    goto :goto_0

    .line 1420
    :cond_5
    const/16 v0, 0x37

    if-lt p1, v0, :cond_6

    const/16 v0, 0x3c

    if-ge p1, v0, :cond_6

    .line 1421
    const/16 v0, 0xa5

    goto :goto_0

    .line 1422
    :cond_6
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_7

    const/16 v0, 0x41

    if-ge p1, v0, :cond_7

    .line 1423
    const/16 v0, 0xa0

    goto :goto_0

    .line 1424
    :cond_7
    const/16 v0, 0x41

    if-lt p1, v0, :cond_8

    const/16 v0, 0x46

    if-ge p1, v0, :cond_8

    .line 1425
    const/16 v0, 0x9b

    goto :goto_0

    .line 1426
    :cond_8
    const/16 v0, 0x46

    if-lt p1, v0, :cond_9

    .line 1427
    const/16 v0, 0x96

    goto :goto_0

    .line 1428
    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMilesFromMeters(J)Ljava/lang/String;
    .locals 8
    .param p1, "metervalue"    # J

    .prologue
    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    .line 734
    long-to-double v2, p1

    const-wide v4, 0x3f445c6dea1359a4L    # 6.2137E-4

    mul-double v0, v2, v4

    .line 735
    .local v0, "mi":D
    mul-double v2, v0, v6

    double-to-int v2, v2

    int-to-double v2, v2

    div-double v0, v2, v6

    .line 736
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "0.0"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getSeedDataFormDB()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 885
    const-string v6, "AVG_SPEED"

    .line 886
    .local v6, "avgColumn":Ljava/lang/String;
    const-string v9, "MIN_SPEED"

    .line 887
    .local v9, "minColumn":Ljava/lang/String;
    const-string v8, "MAX_SPEED"

    .line 888
    .local v8, "maxColumn":Ljava/lang/String;
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v11

    const-string/jumbo v0, "speed_per_hour"

    aput-object v0, v2, v12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MAX(speed_per_hour) AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v13

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MIN(speed_per_hour) AS "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "AVG(speed_per_hour) AS "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    .line 894
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "exercise__id =?  AND update_time>? "

    .line 896
    .local v3, "selectionClause":Ljava/lang/String;
    new-array v4, v13, [Ljava/lang/String;

    iget-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v11

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v12

    .line 899
    .local v4, "selectionArg":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 900
    .local v5, "sortOrder":Ljava/lang/String;
    const/4 v7, 0x0

    .line 903
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 905
    if-eqz v7, :cond_0

    .line 906
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 907
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 908
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    .line 909
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    long-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAvgSpeed:F

    .line 910
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 914
    :cond_0
    if-eqz v7, :cond_1

    .line 915
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 917
    :cond_1
    return-void

    .line 914
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 915
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getUserAge()I
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x1

    .line 1388
    new-instance v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5}, Lcom/samsung/android/sdk/health/content/ShealthProfile;-><init>(Landroid/content/Context;)V

    .line 1390
    .local v3, "profile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1391
    .local v1, "birth":Ljava/util/Calendar;
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1393
    .local v4, "today":Ljava/util/Calendar;
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getBirthDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/common/utils/Utils;->convertDateStringToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 1395
    .local v0, "bDate":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1396
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1398
    const/4 v2, 0x0

    .line 1399
    .local v2, "factor":I
    invoke-virtual {v4, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 1400
    const/4 v2, -0x1

    .line 1403
    :cond_0
    invoke-virtual {v4, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v5, v2

    return v5
.end method

.method private isLogEntryFromWearable(I)Z
    .locals 1
    .param p1, "dataType"    # I

    .prologue
    .line 422
    const/16 v0, 0x2724

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2728

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2726

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2723

    if-eq p1, v0, :cond_0

    const/16 v0, 0x272e

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2727

    if-ne p1, v0, :cond_1

    .line 428
    :cond_0
    const/4 v0, 0x1

    .line 430
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeHRMarker(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "hrData"    # Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "markedHRData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;>;"
    const v5, 0x3727c5ac    # 1.0E-5f

    const/high16 v4, -0x40800000    # -1.0f

    .line 1065
    if-nez p1, :cond_1

    .line 1090
    :cond_0
    :goto_0
    return-void

    .line 1067
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v3

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v3

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_0

    .line 1071
    :cond_2
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_3

    .line 1072
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1075
    :cond_3
    const/4 v2, 0x0

    .line 1076
    .local v2, "skip":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 1077
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 1079
    .local v0, "data2":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_5

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v3

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_5

    .line 1081
    const-string v3, "ExerciseProDetailsMapFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Skipping: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1082
    const/4 v2, 0x1

    .line 1086
    .end local v0    # "data2":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_4
    if-nez v2, :cond_0

    .line 1087
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1076
    .restart local v0    # "data2":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private makeHRMarker(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1056
    .local p1, "hrDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1057
    .local v2, "markedHRData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 1058
    .local v0, "hrData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->makeHRMarker(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1060
    .end local v0    # "hrData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1061
    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    .line 1062
    return-void
.end method

.method private playMapFromDB()V
    .locals 34

    .prologue
    .line 434
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 435
    .local v23, "locationItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    new-instance v31, Ljava/util/ArrayList;

    invoke-direct/range {v31 .. v31}, Ljava/util/ArrayList;-><init>()V

    .line 437
    .local v31, "speedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;>;"
    const-string v6, "ExerciseProDetailsMapFragment"

    const-string/jumbo v7, "playMapFromDB()"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/RealtimeUtils;->getLastLocation(Landroid/content/Context;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v21

    .line 439
    .local v21, "lat":Lcom/google/android/gms/maps/model/LatLng;
    if-eqz v21, :cond_0

    .line 440
    new-instance v12, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v12}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    .line 441
    .local v12, "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    .line 442
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v12}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v7

    const/16 v8, 0x168

    const/16 v9, 0x220

    const/16 v32, 0x3

    move/from16 v0, v32

    invoke-static {v7, v8, v9, v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 445
    .end local v12    # "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getSelectionClause(ZJ)Ljava/lang/String;

    move-result-object v4

    .line 446
    .local v4, "selectionClause":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "data_type"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " = 300001"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 449
    .local v26, "selectionClause2":Ljava/lang/String;
    const-string v5, "create_time ASC"

    .line 451
    .local v5, "sortOder":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    invoke-static/range {v4 .. v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getLocationItems(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;ZJ)Ljava/util/ArrayList;

    move-result-object v23

    .line 453
    const-string v4, ""

    .line 454
    const-string v6, "ExerciseProDetailsMapFragment"

    const-string v7, "Read cache db."

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getSelectionClause(ZJ)Ljava/lang/String;

    move-result-object v4

    .line 458
    const-string v5, "create_time ASC"

    .line 459
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v0, v26

    invoke-static {v0, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getSpeedItems(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/util/ArrayList;

    move-result-object v31

    .line 461
    if-nez v23, :cond_1

    .line 462
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mNoDataNotiText:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 566
    :goto_0
    return-void

    .line 464
    :cond_1
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_2

    .line 465
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mNoDataNotiText:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 469
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    sget v7, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapMode:I

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/GoogleMap;->setMapType(I)V

    .line 471
    new-instance v11, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    invoke-direct {v11}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;-><init>()V

    .line 473
    .local v11, "boundsBuilder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    const/16 v25, 0x0

    .line 474
    .local v25, "maxSpeedIndex":I
    const/16 v24, 0x0

    .line 475
    .local v24, "mapPathIndex":I
    const/16 v30, 0x0

    .line 476
    .local v30, "speedIndex":I
    const/16 v28, 0x1

    .line 477
    .local v28, "spaceNumber":I
    const/4 v13, 0x0

    .line 478
    .local v13, "deltaSpeed":F
    const/16 v27, 0x0

    .line 479
    .local v27, "setSpeed":F
    if-eqz v31, :cond_3

    .line 480
    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v25, v6, -0x1

    .line 483
    :cond_3
    const-string v6, "ExerciseProDetailsMapFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No. of speeditems : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  No. of locationItems: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 486
    .local v16, "historyLocation":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 487
    .local v17, "historySpeed":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDataType:I

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->isLogEntryFromWearable(I)Z

    move-result v19

    .line 488
    .local v19, "isWearableLogEntry":Z
    const/16 v29, 0x1

    .line 490
    .local v29, "speedDistributed":Z
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;

    .line 491
    .local v22, "location":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 492
    if-eqz v31, :cond_4

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_7

    .line 493
    :cond_4
    const-string v6, "ExerciseProDetailsMapFragment"

    const-string v7, "Speed DB Empty"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    :cond_5
    :goto_2
    new-instance v15, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLatitude()D

    move-result-wide v6

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getLongitude()D

    move-result-wide v8

    invoke-direct {v15, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 542
    .local v15, "historyLatLng":Lcom/google/android/gms/maps/model/LatLng;
    if-nez v24, :cond_10

    .line 544
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v7, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v7}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v7, v15}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v7

    const/high16 v8, 0x3f000000    # 0.5f

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8, v9}, Lcom/google/android/gms/maps/model/MarkerOptions;->anchor(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v6

    const v7, 0x7f0203fc

    invoke-static {v7}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/Marker;->setIcon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)V

    .line 555
    :cond_6
    :goto_3
    invoke-virtual {v11, v15}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    .line 556
    add-int/lit8 v24, v24, 0x1

    .line 557
    goto :goto_1

    .line 494
    .end local v15    # "historyLatLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_7
    if-eqz v19, :cond_a

    .line 495
    move-object/from16 v0, v31

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getCreateTime()J

    move-result-wide v6

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_8

    .line 496
    add-int/lit8 v28, v28, 0x1

    goto :goto_2

    .line 498
    :cond_8
    move-object/from16 v0, v31

    move/from16 v1, v30

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getDeltaSpeed(Ljava/util/ArrayList;II)F

    move-result v13

    .line 500
    const/16 v20, 0x0

    .local v20, "j":I
    :goto_4
    move/from16 v0, v20

    move/from16 v1, v28

    if-ge v0, v1, :cond_9

    .line 501
    add-float v27, v27, v13

    .line 502
    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    .line 505
    :cond_9
    const/16 v28, 0x1

    .line 506
    add-int/lit8 v30, v30, 0x1

    .line 507
    move/from16 v0, v30

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    .line 508
    move/from16 v30, v25

    goto :goto_2

    .line 512
    .end local v20    # "j":I
    :cond_a
    if-eqz v29, :cond_c

    .line 513
    :goto_5
    add-int/lit8 v6, v25, -0x1

    move/from16 v0, v30

    if-gt v0, v6, :cond_b

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v7

    move-object/from16 v0, v31

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getCreateTime()J

    move-result-wide v32

    cmp-long v6, v7, v32

    if-lez v6, :cond_b

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v7

    add-int/lit8 v6, v30, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getCreateTime()J

    move-result-wide v32

    cmp-long v6, v7, v32

    if-lez v6, :cond_b

    .line 514
    add-int/lit8 v30, v30, 0x1

    goto :goto_5

    .line 516
    :cond_b
    const/16 v29, 0x0

    .line 519
    :cond_c
    move-object/from16 v0, v31

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/RealtimeData;->getCreateTime()J

    move-result-wide v6

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;->getCreateTime()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_d

    .line 520
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_2

    .line 522
    :cond_d
    move-object/from16 v0, v31

    move/from16 v1, v30

    move/from16 v2, v28

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getDeltaSpeed(Ljava/util/ArrayList;IIF)F

    move-result v13

    .line 523
    const/4 v6, 0x1

    move/from16 v0, v28

    if-le v0, v6, :cond_e

    .line 524
    const/16 v29, 0x1

    .line 527
    :cond_e
    const/16 v20, 0x0

    .restart local v20    # "j":I
    :goto_6
    move/from16 v0, v20

    move/from16 v1, v28

    if-ge v0, v1, :cond_f

    .line 528
    add-float v27, v27, v13

    .line 529
    invoke-static/range {v27 .. v27}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527
    add-int/lit8 v20, v20, 0x1

    goto :goto_6

    .line 532
    :cond_f
    const/16 v28, 0x1

    .line 533
    add-int/lit8 v30, v30, 0x1

    .line 534
    move/from16 v0, v30

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    .line 535
    move/from16 v30, v25

    goto/16 :goto_2

    .line 546
    .end local v20    # "j":I
    .restart local v15    # "historyLatLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_10
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move/from16 v0, v24

    if-ne v0, v6, :cond_6

    .line 548
    const-string v6, "ExerciseProDetailsMapFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "playMapFromDB() 7 mapPathIndex"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v7, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v7}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v7, v15}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v7

    const/high16 v8, 0x3f000000    # 0.5f

    const/high16 v9, 0x3f000000    # 0.5f

    invoke-virtual {v7, v8, v9}, Lcom/google/android/gms/maps/model/MarkerOptions;->anchor(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v6

    const v7, 0x7f0203f1

    invoke-static {v7}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/gms/maps/model/Marker;->setIcon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)V

    goto/16 :goto_3

    .line 560
    .end local v15    # "historyLatLng":Lcom/google/android/gms/maps/model/LatLng;
    .end local v22    # "location":Lcom/sec/android/app/shealth/common/utils/hcp/data/MapPathData;
    :cond_11
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->drawRoute(Ljava/util/ArrayList;Ljava/util/List;Ljava/util/List;)V

    .line 563
    invoke-virtual {v11}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v10

    .line 564
    .local v10, "bounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v14

    .line 565
    .local v14, "display":Landroid/util/DisplayMetrics;
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIsMiniMode:Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    const/4 v8, 0x0

    invoke-static {v10, v14, v6, v7, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->panAllRouteOnMap(Ljava/lang/Object;Landroid/util/DisplayMetrics;ZLcom/google/android/gms/maps/GoogleMap;Lcom/amap/api/maps2d/AMap;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/maps/GoogleMap;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    goto/16 :goto_0
.end method

.method private setHRMList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1048
    .local p1, "hrDatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;>;"
    if-nez p1, :cond_1

    .line 1053
    :cond_0
    :goto_0
    return-void

    .line 1050
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1052
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->makeHRMarker(Ljava/util/List;)V

    goto :goto_0
.end method

.method private setHRMStateBar(I)V
    .locals 11
    .param p1, "bpm"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x0

    .line 1307
    new-array v0, v8, [I

    fill-array-data v0, :array_0

    .line 1308
    .local v0, "avgRange":[I
    new-array v6, v8, [I

    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minHRzone:I

    aput v8, v6, v10

    const/4 v8, 0x1

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxHRzone:I

    aput v9, v6, v8

    .line 1309
    .local v6, "percentile":[I
    const/16 v3, 0xc8

    .line 1310
    .local v3, "maxHR":I
    const/4 v1, 0x0

    .line 1311
    .local v1, "greenBarMargin":I
    const/4 v2, 0x0

    .line 1312
    .local v2, "greenBarWidth":I
    const/16 v8, 0x10c

    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->convertDptoPx(I)I

    move-result v7

    .line 1313
    .local v7, "width":I
    const/4 v4, 0x1

    .line 1315
    .local v4, "n":I
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1317
    .local v5, "number":I
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxHRzone:I

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minHRzone:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v7

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1318
    iget v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minHRzone:I

    int-to-float v8, v8

    int-to-float v9, v3

    div-float/2addr v8, v9

    int-to-float v9, v7

    mul-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 1320
    :goto_0
    div-int/lit8 v5, v5, 0xa

    if-eqz v5, :cond_0

    .line 1321
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1324
    :cond_0
    const/4 v8, 0x3

    if-lt v4, v8, :cond_1

    .line 1325
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    add-int/lit8 v9, v1, 0x10

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1328
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxBpmText:Landroid/widget/TextView;

    add-int v9, v1, v2

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1330
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mStateBar:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;

    invoke-virtual {v8, p1, v0, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ExerciseStateSummaryBar;->moveToPolygon(I[I[I)V

    .line 1331
    return-void

    .line 1327
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minBpmText:Landroid/widget/TextView;

    add-int/lit8 v9, v1, 0x17

    invoke-virtual {v8, v9, v10, v10, v10}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_1

    .line 1307
    :array_0
    .array-data 4
        0x0
        0xc8
    .end array-data
.end method

.method private setHeartRateMarkerOnMap()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1094
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1095
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-direct {p0, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setHRMList(Ljava/util/List;)V

    .line 1096
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 1097
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 1098
    .local v1, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v4

    cmpl-float v4, v4, v8

    if-eqz v4, :cond_0

    .line 1099
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f02031c

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->drawTextToBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1101
    .local v0, "bm":Landroid/graphics/Bitmap;
    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLatitude()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getLongitude()F

    move-result v6

    float-to-double v6, v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 1102
    .local v3, "pos":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v5, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v5}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HRM_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getHeartRateVal()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->snippet(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v5

    const/high16 v6, 0x3f000000    # 0.5f

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/maps/model/MarkerOptions;->anchor(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/Marker;->setIcon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)V

    .line 1104
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v5, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$9;

    invoke-direct {v5, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$9;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    .line 1096
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "pos":Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1124
    .end local v1    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_1
    return-void
.end method

.method private setSpeedValue()V
    .locals 9

    .prologue
    const v8, 0x7f090810

    const v7, 0x7f09080f

    .line 696
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getDistanceUnitFromSharedPreferences()Ljava/lang/String;

    move-result-object v3

    const-string v4, "km"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 701
    .local v0, "kmUnit":Z
    const-string v3, "ExerciseProDetailsMapFragment"

    const-string/jumbo v4, "setSpeedValue()"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    if-eqz v0, :cond_0

    .line 703
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    float-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v2

    .line 704
    .local v2, "value":Ljava/lang/String;
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 710
    .local v1, "unit":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mSlowSpeed:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 711
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mSlowSpeed:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 713
    if-eqz v0, :cond_1

    .line 714
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    float-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getKmFromMeter(J)Ljava/lang/String;

    move-result-object v2

    .line 715
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 722
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mFastSpeed:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getEnglishFromArabic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 723
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mFastSpeed:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->SPACE:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/TalkbackUtils;->convertToProperUnitsText(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 724
    return-void

    .line 706
    .end local v1    # "unit":Ljava/lang/String;
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMinSpeed:F

    float-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v2

    .line 707
    .restart local v2    # "value":Ljava/lang/String;
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "unit":Ljava/lang/String;
    goto/16 :goto_0

    .line 718
    :cond_1
    iget v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    float-to-long v3, v3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getMilesFromMeters(J)Ljava/lang/String;

    move-result-object v2

    .line 719
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private setUpMap()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 393
    const-string v0, "ExerciseProDetailsMapFragment"

    const-string/jumbo v1, "setUpMap()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/GoogleMap;->setMapType(I)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getMinZoomLevel()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->currentZoomLevel:F

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraChangeListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/UiSettings;->setZoomControlsEnabled(Z)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mUiSettings:Lcom/google/android/gms/maps/UiSettings;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/UiSettings;->setCompassEnabled(Z)V

    .line 414
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->playMapFromDB()V

    .line 415
    return-void
.end method

.method private setUpMapIfNeeded(Landroid/view/View;)V
    .locals 5
    .param p1, "contentView"    # Landroid/view/View;

    .prologue
    .line 353
    const-string v2, "ExerciseProDetailsMapFragment"

    const-string/jumbo v3, "setUpMapIfNeeded()"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 356
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v2, :cond_0

    .line 357
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/MapView;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 360
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v2, :cond_3

    .line 361
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIsMiniMode:Z

    if-eqz v2, :cond_1

    .line 362
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;)V

    .line 375
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v3}, Lcom/google/android/gms/maps/GoogleMap;->getMapType()I

    move-result v3

    const-string v4, "ExerciseProDetailsMapFragment"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getMapMode(Landroid/app/Activity;ILjava/lang/String;)I

    move-result v2

    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapMode:I

    .line 376
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v1

    .line 378
    .local v1, "status":I
    if-nez v1, :cond_2

    .line 379
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setUpMap()V

    .line 380
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIsMiniMode:Z

    if-nez v2, :cond_3

    .line 381
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setUpMarkIcon()V

    .line 382
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->updatePhotoMarkIcon()V

    .line 383
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setHeartRateMarkerOnMap()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    .end local v1    # "status":I
    :cond_3
    :goto_0
    return-void

    .line 386
    .restart local v1    # "status":I
    :catch_0
    move-exception v0

    .line 387
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ExerciseProDetailsMapFragment"

    const-string/jumbo v3, "setUpMap() exception handling"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setUpMarkIcon()V
    .locals 2

    .prologue
    .line 756
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMarkListener:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->setOnUpdatePhotoListListener(Lcom/sec/android/app/shealth/plugins/exercisepro/utils/PhotoListUtil$OnUpdatePhotoListListener;)V

    .line 758
    return-void
.end method

.method private updatePhotoMarkIcon()V
    .locals 4

    .prologue
    .line 761
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getPhotoDatas(ZLandroid/app/Activity;J)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    .line 763
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 789
    :cond_0
    :goto_0
    return-void

    .line 766
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapPhotoMarker:Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapPhotoList;->SetPhotoList(Ljava/util/List;)V

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$5;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public drawTextToBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "resourceId"    # I
    .param p3, "mText"    # Ljava/lang/String;

    .prologue
    .line 1128
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1129
    .local v6, "resources":Landroid/content/res/Resources;
    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    iget v7, v10, Landroid/util/DisplayMetrics;->density:F

    .line 1130
    .local v7, "scale":F
    move/from16 v0, p2

    invoke-static {v6, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1132
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    .line 1134
    .local v2, "bitmapConfig":Landroid/graphics/Bitmap$Config;
    if-nez v2, :cond_0

    .line 1135
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1139
    :cond_0
    const/4 v10, 0x1

    invoke-virtual {v1, v2, v10}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1141
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1143
    .local v4, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    const/4 v10, 0x1

    invoke-direct {v5, v10}, Landroid/graphics/Paint;-><init>(I)V

    .line 1145
    .local v5, "paint":Landroid/graphics/Paint;
    const/16 v10, 0x6e

    const/16 v11, 0x6e

    const/16 v12, 0x6e

    invoke-static {v10, v11, v12}, Landroid/graphics/Color;->rgb(III)I

    move-result v10

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 1147
    const/high16 v10, 0x41400000    # 12.0f

    mul-float/2addr v10, v7

    float-to-int v10, v10

    int-to-float v10, v10

    invoke-virtual {v5, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1149
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const v13, -0xbbbbbc

    invoke-virtual {v5, v10, v11, v12, v13}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 1152
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 1153
    .local v3, "bounds":Landroid/graphics/Rect;
    const/4 v10, 0x0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v11

    move-object/from16 v0, p3

    invoke-virtual {v5, v0, v10, v11, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1156
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    div-int/lit8 v8, v10, 0x2

    .line 1157
    .local v8, "x":I
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v11

    add-int/2addr v10, v11

    div-int/lit8 v9, v10, 0x2

    .line 1158
    .local v9, "y":I
    int-to-float v10, v8

    int-to-float v11, v9

    move-object/from16 v0, p3

    invoke-virtual {v4, v0, v10, v11, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1166
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "bitmapConfig":Landroid/graphics/Bitmap$Config;
    .end local v3    # "bounds":Landroid/graphics/Rect;
    .end local v4    # "canvas":Landroid/graphics/Canvas;
    .end local v5    # "paint":Landroid/graphics/Paint;
    .end local v6    # "resources":Landroid/content/res/Resources;
    .end local v7    # "scale":F
    .end local v8    # "x":I
    .end local v9    # "y":I
    :goto_0
    return-object v1

    .line 1161
    :catch_0
    move-exception v10

    .line 1166
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 938
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/utils/HealthServiceUtil;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 939
    .local v0, "android_id":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "10008_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 940
    .local v1, "deviceId":Ljava/lang/String;
    return-object v1
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 859
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$8;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    return-object v0
.end method

.method public isAbleFinished()Z
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->onBackPress()V

    .line 851
    const/4 v0, 0x1

    .line 854
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 841
    const/16 v0, 0x99

    if-ne p1, v0, :cond_0

    .line 842
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mContentView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mContentView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setUpMapIfNeeded(Landroid/view/View;)V

    .line 846
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 218
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 219
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBundle:Landroid/os/Bundle;

    .line 220
    if-eqz p1, :cond_0

    .line 221
    const-string/jumbo v0, "realtime_workout_end_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    .line 222
    const-string/jumbo v0, "realtime_during_try_it"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    .line 223
    const-string/jumbo v0, "realtime_sync_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPlaceMode:I

    .line 224
    const-string/jumbo v0, "realtime_sync_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mSync_time:J

    .line 225
    const-string/jumbo v0, "realtime_sync_exercise_info_db_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mExerciseId:J

    .line 226
    const-string/jumbo v0, "realtime_sync_activity_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mActivityType:I

    .line 227
    const-string/jumbo v0, "realtime_sync_distance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDistance:F

    .line 228
    const-string/jumbo v0, "realtime_sync_duration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDuration:F

    .line 229
    const-string/jumbo v0, "realtime_sync_calories"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mCalories:F

    .line 230
    const-string/jumbo v0, "realtime_sync_max_hrm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxHrm:F

    .line 231
    const-string/jumbo v0, "realtime_sync_max_speed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    .line 232
    const-string/jumbo v0, "realtime_sync_avg_hrm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAvgHrm:F

    .line 233
    const-string/jumbo v0, "realtime_sync_avg_speed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAvgSpeed:F

    .line 234
    const-string/jumbo v0, "realtime_sync_elevation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mElevation:F

    .line 238
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getExtras()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f080700

    const v9, 0x7f080717

    const v8, 0x7f080711

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 298
    const v3, 0x7f0301ab

    invoke-virtual {p1, v3, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 300
    .local v0, "contentView":Landroid/view/View;
    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mContentView:Landroid/view/View;

    .line 301
    if-eqz p3, :cond_0

    .line 302
    const-string v3, "mEndMode"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    .line 304
    :cond_0
    const v3, 0x7f08070f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mNoDataNotiText:Landroid/widget/TextView;

    .line 305
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mNoDataNotiText:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 307
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getSeedDataFormDB()V

    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getHeartRateData()V

    .line 310
    const-string v3, "ExerciseProDetailsMapFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "onCreateView() mTryit_map : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    if-nez v3, :cond_3

    .line 314
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/maps/MapsInitializer;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    :goto_0
    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/maps/MapView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/MapView;->onCreate(Landroid/os/Bundle;)V

    .line 320
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setUpMapIfNeeded(Landroid/view/View;)V

    .line 326
    :goto_1
    const v3, 0x7f080719

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    .line 327
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoHorizontalScrollClickListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->setOnTumbnailClickListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView$OnTumbnailClickListener;)V

    .line 328
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPhotoScrollView:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PhotoHorizontalScrollView;->setVisibility(I)V

    .line 330
    const v3, 0x7f080716

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mSlowSpeed:Landroid/widget/TextView;

    .line 331
    const v3, 0x7f080715

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mFastSpeed:Landroid/widget/TextView;

    .line 332
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->setSpeedValue()V

    .line 334
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mIsMiniMode:Z

    if-eqz v3, :cond_1

    .line 335
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 336
    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 339
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v2

    .line 340
    .local v2, "status":I
    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    const/16 v3, 0x9

    if-eq v2, v3, :cond_2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 342
    :cond_2
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 343
    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 348
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->addListenerViewModeButton()V

    .line 349
    return-object v0

    .line 315
    .end local v2    # "status":I
    :catch_0
    move-exception v1

    .line 316
    .local v1, "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    const-string v3, "ExerciseProDetailsMapFragment"

    const-string v4, "GooglePlayServicesNotAvailable Exception in onCreateView()"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 323
    .end local v1    # "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    :cond_3
    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 324
    const v3, 0x7f0806f5

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f020798

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    .line 345
    .restart local v2    # "status":I
    :cond_4
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 346
    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 283
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onDestroy()V

    .line 286
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onDestroy()V

    .line 287
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 291
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onLowMemory()V

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onLowMemory()V

    .line 294
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onPause()V

    .line 278
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onPause()V

    .line 279
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 835
    if-eqz p1, :cond_0

    .line 836
    const-string v0, "mEndMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    .line 838
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 242
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onResume()V

    .line 243
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onResume()V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v0, :cond_1

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/GoogleMap;->getMapType()I

    move-result v1

    const-string v2, "ExerciseProDetailsMapFragment"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/Utilspro;->getMapMode(Landroid/app/Activity;ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapMode:I

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapMode:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setMapType(I)V

    .line 251
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 817
    const-string/jumbo v0, "realtime_workout_end_mode"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mEndMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 818
    const-string/jumbo v0, "realtime_during_try_it"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mTryit_map:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 819
    const-string/jumbo v0, "realtime_sync_mode"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mPlaceMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 820
    const-string/jumbo v0, "realtime_sync_time"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mSync_time:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 821
    const-string/jumbo v0, "realtime_sync_exercise_info_db_id"

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mExerciseId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 822
    const-string/jumbo v0, "realtime_sync_activity_type"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mActivityType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 823
    const-string/jumbo v0, "realtime_sync_distance"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDistance:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 824
    const-string/jumbo v0, "realtime_sync_duration"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mDuration:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 825
    const-string/jumbo v0, "realtime_sync_calories"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mCalories:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 826
    const-string/jumbo v0, "realtime_sync_max_hrm"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxHrm:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 827
    const-string/jumbo v0, "realtime_sync_max_speed"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMaxSpeed:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 828
    const-string/jumbo v0, "realtime_sync_avg_hrm"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAvgHrm:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 829
    const-string/jumbo v0, "realtime_sync_avg_speed"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAvgSpeed:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 830
    const-string/jumbo v0, "realtime_sync_elevation"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mElevation:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 831
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/MapView;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 832
    return-void
.end method

.method public setLocationtoHRMData()V
    .locals 11

    .prologue
    .line 995
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_3

    .line 996
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapHRMList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;

    .line 997
    .local v7, "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select location_data.[latitude],location_data.[longitude] from location_data where location_data.[exercise__id]= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mRowId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and location_data.[sample_time]<= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->getSampleTime()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " order by location_data.[sample_time] desc limit 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 998
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1001
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1002
    if-eqz v6, :cond_0

    .line 1003
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1004
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1005
    const-string v0, "latitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v9

    .line 1006
    .local v9, "latitude":F
    const-string v0, "longitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v10

    .line 1007
    .local v10, "longitude":F
    invoke-virtual {v7, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setLatitude(F)V

    .line 1008
    invoke-virtual {v7, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;->setLongitude(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1013
    .end local v9    # "latitude":F
    .end local v10    # "longitude":F
    :cond_0
    if-eqz v6, :cond_1

    .line 1014
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 995
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1013
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1014
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 1018
    .end local v3    # "selectionClause":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "hrmData":Lcom/sec/android/app/shealth/plugins/exercisepro/utils/MapHRMData;
    :cond_3
    return-void
.end method

.method public setOnMapClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

    .prologue
    .line 418
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mMapClickListener:Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;

    .line 419
    return-void
.end method

.method public showHRdialog(I)V
    .locals 5
    .param p1, "hrValue"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x42c80000    # 100.0f

    .line 1171
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->bpm:I

    .line 1172
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getUserAge()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getMaxHR(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    const/high16 v2, 0x42480000    # 50.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->minHRzone:I

    .line 1173
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getUserAge()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getMaxHR(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    const/high16 v2, 0x42aa0000    # 85.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->maxHRzone:I

    .line 1175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 1177
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1180
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1181
    const v1, 0x7f090a4c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1182
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1184
    const v1, 0x7f0301a8

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$10;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1261
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$11;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1276
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$12;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1285
    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$13;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment$13;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 1297
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1298
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    .line 1299
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1301
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->mAlertBuild_HRM_Result:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProDetailsMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1304
    :cond_1
    return-void
.end method
