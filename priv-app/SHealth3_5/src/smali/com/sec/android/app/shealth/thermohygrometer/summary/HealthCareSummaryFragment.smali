.class public abstract Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "HealthCareSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment$ResetDialogButtonController;
    }
.end annotation


# static fields
.field public static final RESET_DIALOG_TAG:Ljava/lang/String; = "RESET_DIALOG_TAG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 254
    return-void
.end method

.method private setNextDate(JJ)V
    .locals 0
    .param p1, "startOfCurrentDay"    # J
    .param p3, "startOfSelectedDate"    # J

    .prologue
    .line 211
    return-void
.end method

.method private setPrevDate(JJ)V
    .locals 0
    .param p1, "startOfCurrentDay"    # J
    .param p3, "startOfSelectedDate"    # J

    .prologue
    .line 196
    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 236
    return-void
.end method


# virtual methods
.method protected abstract clearMainView()V
.end method

.method protected deleteAllDataForDay()V
    .locals 0

    .prologue
    .line 243
    return-void
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 247
    const-string v0, "RESET_DIALOG_TAG"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    new-instance v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment$ResetDialogButtonController;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment$ResetDialogButtonController;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment$1;)V

    .line 250
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 144
    const/16 v0, 0x2711

    if-ne p1, v0, :cond_0

    .line 145
    if-eqz p3, :cond_0

    const-string v0, "TIME"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    new-instance v0, Ljava/util/Date;

    const-string v1, "TIME"

    iget-object v2, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p3, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->onDateChanged(Ljava/util/Date;)V

    .line 149
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 150
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->setNextAndPrevDates()V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->updateMainView()V

    .line 122
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    if-eqz p1, :cond_0

    const-string v0, "TIME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "TIME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 103
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->setHasOptionsMenu(Z)V

    .line 104
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 127
    const v0, 0x7f100027

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 128
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 129
    return-void
.end method

.method public onDateChanged(Ljava/util/Date;)V
    .locals 0
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 169
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDateChanged(Ljava/util/Date;)V

    .line 170
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->setNextAndPrevDates()V

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->updateMainView()V

    .line 173
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 133
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 134
    .local v0, "itemId":I
    const v1, 0x7f0802db

    if-ne v0, v1, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->showResetDialog()V

    .line 136
    const/4 v1, 0x1

    .line 138
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 108
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->setNextAndPrevDates()V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->updateMainView()V

    .line 115
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    const-string v0, "TIME"

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getSelectedDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 94
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 95
    return-void
.end method

.method protected onSytemDateChanged()V
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 155
    new-instance v0, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->updateMainView()V

    .line 159
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->setNextAndPrevDates()V

    .line 160
    return-void
.end method

.method protected setNextAndPrevDates()V
    .locals 6

    .prologue
    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v0

    .line 178
    .local v0, "startOfCurrentDay":J
    iget-object v4, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v2

    .line 179
    .local v2, "startOfSelectedDate":J
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->setPrevDate(JJ)V

    .line 180
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->setNextDate(JJ)V

    .line 181
    return-void
.end method

.method protected showResetDialog()V
    .locals 4

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getRequestedOrientation()I

    move-result v0

    .line 218
    .local v0, "orientation":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 219
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090062

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "RESET_DIALOG_TAG"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method protected abstract updateMainView()V
.end method
