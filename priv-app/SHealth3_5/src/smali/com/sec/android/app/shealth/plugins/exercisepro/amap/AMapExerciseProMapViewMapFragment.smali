.class public Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;
.super Lcom/amap/api/maps2d/SupportMapFragment;
.source "AMapExerciseProMapViewMapFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;
    }
.end annotation


# instance fields
.field mOriginalView:Landroid/view/View;

.field mapWrapper:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/amap/api/maps2d/SupportMapFragment;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;->mOriginalView:Landroid/view/View;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/amap/api/maps2d/SupportMapFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;->mOriginalView:Landroid/view/View;

    .line 22
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;->mapWrapper:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;

    .line 23
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;->mapWrapper:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;->mOriginalView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;->addView(Landroid/view/View;)V

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment;->mapWrapper:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProMapViewMapFragment$MapWrapper;

    return-object v0
.end method
