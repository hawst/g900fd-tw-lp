.class Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;
.super Ljava/lang/Object;
.source "InputModule.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/input/InputModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0x42

    const/16 v3, 0x17

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 527
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->isInputWithEmptyListener()Z

    move-result v2

    if-nez v2, :cond_0

    .line 529
    iget-object v2, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->hideKeyboard()V

    .line 532
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    .line 534
    if-eq p2, v4, :cond_1

    if-ne p2, v3, :cond_4

    .line 535
    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setPressed(Z)V

    .line 536
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    # invokes: Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->incrementDecrementValue(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->access$1100(Lcom/sec/android/app/shealth/common/commonui/input/InputModule;Landroid/view/View;)V

    .line 547
    :goto_0
    return v0

    .line 539
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_4

    .line 540
    invoke-virtual {p1, v1}, Landroid/view/View;->setPressed(Z)V

    .line 541
    if-eq p2, v4, :cond_3

    if-ne p2, v3, :cond_4

    .line 542
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/common/commonui/input/InputModule$8;->this$0:Lcom/sec/android/app/shealth/common/commonui/input/InputModule;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/input/InputModule;->stopInertia()V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 547
    goto :goto_0
.end method
