.class public Lcom/sec/android/app/shealth/data/DataMonitorUtils;
.super Ljava/lang/Object;
.source "DataMonitorUtils.java"


# static fields
.field private static LOG_TAG:Ljava/lang/String;

.field private static mDataCP:Lcom/sec/android/app/shealth/data/DataMonitorUtils;


# instance fields
.field private DEFAULT_PEDOMETER_STEPS_GOAL:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/sec/android/app/shealth/data/DataMonitorUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->mDataCP:Lcom/sec/android/app/shealth/data/DataMonitorUtils;

    .line 21
    const-class v0, Lcom/sec/android/app/shealth/data/DataMonitorUtils;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->DEFAULT_PEDOMETER_STEPS_GOAL:I

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/shealth/data/DataMonitorUtils;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->mDataCP:Lcom/sec/android/app/shealth/data/DataMonitorUtils;

    return-object v0
.end method


# virtual methods
.method public getCurrentFoodKcalValue([Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p1, "selectionArg"    # [Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "selectionCaluse"    # Ljava/lang/String;
    .param p4, "dataId"    # Ljava/lang/String;

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 44
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 47
    .local v2, "kcal":I
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getCursor([Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_2

    .line 50
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 51
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_1

    .line 52
    const-string/jumbo v3, "total_kilo_calorie"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 53
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 63
    if-eqz v0, :cond_0

    .line 65
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 69
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    :goto_1
    return v2

    .line 55
    :cond_1
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 63
    :cond_2
    if-eqz v0, :cond_0

    .line 65
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 63
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_3

    .line 65
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3
.end method

.method public getCursor([Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "selectionArg"    # [Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "selectionCaluse"    # Ljava/lang/String;
    .param p4, "dataId"    # Ljava/lang/String;

    .prologue
    .line 34
    const/4 v6, 0x0

    .line 36
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " DESC "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p2

    move-object v3, p3

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 37
    sget-object v1, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->LOG_TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cursor.getCount() : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-object v6

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProgressGoalValue(I)Lcom/sec/android/app/shealth/data/ProgressGoalData;
    .locals 2
    .param p1, "goalType"    # I

    .prologue
    .line 73
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->getProgressGoalValue(IJ)Lcom/sec/android/app/shealth/data/ProgressGoalData;

    move-result-object v0

    return-object v0
.end method

.method public getProgressGoalValue(IJ)Lcom/sec/android/app/shealth/data/ProgressGoalData;
    .locals 16
    .param p1, "goalType"    # I
    .param p2, "time"    # J

    .prologue
    .line 77
    const/4 v4, 0x0

    .line 78
    .local v4, "selectionClause":Ljava/lang/String;
    const/4 v3, 0x0

    .line 79
    .local v3, "projection":[Ljava/lang/String;
    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    .line 80
    .local v15, "nowTime":Ljava/lang/Long;
    const/4 v12, 0x0

    .line 82
    .local v12, "cursorGoalExercise":Landroid/database/Cursor;
    new-instance v14, Lcom/sec/android/app/shealth/data/ProgressGoalData;

    invoke-direct {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;-><init>()V

    .line 84
    .local v14, "goalData":Lcom/sec/android/app/shealth/data/ProgressGoalData;
    const v1, 0x9c41

    move/from16 v0, p1

    if-eq v0, v1, :cond_1

    .line 85
    const-string v4, "goal_type = ?  AND set_time<? "

    .line 86
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    .end local v3    # "projection":[Ljava/lang/String;
    const/4 v1, 0x0

    const-string/jumbo v2, "value"

    aput-object v2, v3, v1

    .line 87
    .restart local v3    # "projection":[Ljava/lang/String;
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v5, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v5, v1

    .line 88
    .local v5, "mGoalSelectionArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 89
    const/4 v1, 0x1

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 91
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Goal;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v6, "set_time DESC LIMIT 1"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 98
    .end local v5    # "mGoalSelectionArgs":[Ljava/lang/String;
    :goto_0
    if-eqz v12, :cond_0

    .line 100
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 101
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 102
    sparse-switch p1, :sswitch_data_0

    .line 119
    const-wide/16 v1, 0x0

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    .line 145
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 147
    :cond_0
    return-object v14

    .line 93
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT G.[value] as goal_value FROM (goal left join user_device on goal.user_device__id == user_device._id) as G  WHERE G.sync_status != 170004  AND goal_type = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "set_time"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND ifnull(device_type, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2719

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " order by G.[update_time] DESC LIMIT 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 95
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v9, v4

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    goto/16 :goto_0

    .line 104
    :sswitch_0
    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    goto/16 :goto_1

    .line 107
    :sswitch_1
    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    goto/16 :goto_1

    .line 110
    :sswitch_2
    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    goto/16 :goto_1

    .line 113
    :sswitch_3
    const/4 v1, 0x0

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    .line 114
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v1

    long-to-float v1, v1

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v6, v8

    double-to-float v2, v6

    add-float/2addr v1, v2

    invoke-virtual {v14, v1}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setMaxValue(F)V

    .line 115
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v1

    long-to-float v1, v1

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v6, v8

    double-to-float v2, v6

    sub-float/2addr v1, v2

    invoke-virtual {v14, v1}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setMinValue(F)V

    goto/16 :goto_1

    .line 123
    :cond_2
    sparse-switch p1, :sswitch_data_1

    .line 141
    const-wide/16 v1, 0x0

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    goto/16 :goto_1

    .line 125
    :sswitch_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/shealth/data/DataMonitorUtils;->DEFAULT_PEDOMETER_STEPS_GOAL:I

    int-to-long v1, v1

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    goto/16 :goto_1

    .line 129
    :sswitch_5
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setCalorieGoal(Landroid/content/Context;)V

    .line 130
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v1

    int-to-long v1, v1

    long-to-double v1, v1

    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getBMR()D

    move-result-wide v6

    sub-double/2addr v1, v6

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v13, v1

    .line 131
    .local v13, "defaultValue":I
    int-to-long v1, v13

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    goto/16 :goto_1

    .line 134
    .end local v13    # "defaultValue":I
    :sswitch_6
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setCalorieGoal(Landroid/content/Context;)V

    .line 135
    invoke-static {}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->netCalorieValue()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v14, v1, v2}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setGoalValue(J)V

    .line 136
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v1

    long-to-float v1, v1

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v6, v8

    double-to-float v2, v6

    add-float/2addr v1, v2

    invoke-virtual {v14, v1}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setMaxValue(F)V

    .line 137
    invoke-virtual {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v1

    long-to-float v1, v1

    invoke-virtual {v14}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->getGoalValue()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    mul-double/2addr v6, v8

    double-to-float v2, v6

    sub-float/2addr v1, v2

    invoke-virtual {v14, v1}, Lcom/sec/android/app/shealth/data/ProgressGoalData;->setMinValue(F)V

    goto/16 :goto_1

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x9c41 -> :sswitch_0
        0x9c42 -> :sswitch_2
        0x9c43 -> :sswitch_3
        0x9c4b -> :sswitch_1
    .end sparse-switch

    .line 123
    :sswitch_data_1
    .sparse-switch
        0x9c41 -> :sswitch_4
        0x9c42 -> :sswitch_5
        0x9c43 -> :sswitch_6
        0x9c4b -> :sswitch_5
    .end sparse-switch
.end method
