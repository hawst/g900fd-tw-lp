.class Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DropDownListButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/widget/DropDownListButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DropDownListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/widget/DropDownListButton;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 205
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    .line 211
    if-nez p2, :cond_0

    .line 212
    new-instance v2, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$400(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 213
    .local v2, "textView":Landroid/widget/TextView;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v4, -0x1

    iget-object v5, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListHeight:I
    invoke-static {v5}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$500(Lcom/sec/android/app/shealth/widget/DropDownListButton;)I

    move-result v5

    invoke-direct {v1, v4, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 214
    .local v1, "lp":Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 215
    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getPaddingTop()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getPaddingRight()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getPaddingBottom()I

    move-result v7

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 216
    const/16 v4, 0x10

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 217
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 219
    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextSize:F
    invoke-static {v4}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$600(Lcom/sec/android/app/shealth/widget/DropDownListButton;)F

    move-result v4

    invoke-virtual {v2, v8, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 220
    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextColor:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$700(Lcom/sec/android/app/shealth/widget/DropDownListButton;)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 222
    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownButtonTextFont:Ljava/lang/CharSequence;
    invoke-static {v5}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$800(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/FontUtil;->createFont(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 223
    .local v3, "typeface":Landroid/graphics/Typeface;
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 227
    .end local v1    # "lp":Landroid/widget/AbsListView$LayoutParams;
    .end local v3    # "typeface":Landroid/graphics/Typeface;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    # getter for: Lcom/sec/android/app/shealth/widget/DropDownListButton;->mDropDownListItems:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->access$000(Lcom/sec/android/app/shealth/widget/DropDownListButton;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 228
    .local v0, "fontInfo":[Ljava/lang/String;
    const/4 v4, 0x1

    aget-object v4, v0, v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v4, p0, Lcom/sec/android/app/shealth/widget/DropDownListButton$DropDownListAdapter;->this$0:Lcom/sec/android/app/shealth/widget/DropDownListButton;

    aget-object v5, v0, v8

    const/4 v6, 0x2

    aget-object v6, v0, v6

    invoke-virtual {v4, v2, v5, v6}, Lcom/sec/android/app/shealth/widget/DropDownListButton;->setTypeface(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    return-object v2

    .end local v0    # "fontInfo":[Ljava/lang/String;
    .end local v2    # "textView":Landroid/widget/TextView;
    :cond_0
    move-object v2, p2

    .line 225
    check-cast v2, Landroid/widget/TextView;

    .restart local v2    # "textView":Landroid/widget/TextView;
    goto :goto_0
.end method
