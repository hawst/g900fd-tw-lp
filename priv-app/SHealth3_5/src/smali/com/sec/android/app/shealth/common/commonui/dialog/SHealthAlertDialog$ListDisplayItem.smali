.class public Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;
.super Ljava/lang/Object;
.source "SHealthAlertDialog.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListDisplayItem"
.end annotation


# instance fields
.field public primaryText:Ljava/lang/String;

.field public secondaryText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1340
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1345
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;->primaryText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1346
    iget-object v0, p0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$ListDisplayItem;->secondaryText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1348
    return-void
.end method
