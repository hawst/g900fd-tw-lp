.class public Lcom/sec/android/app/shealth/settings/ManualActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;
.source "ManualActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/settings/ManualActivity$mWebViewClient;
    }
.end annotation


# static fields
.field private static final DATA_CONNECTION_DIALOG:Ljava/lang/String; = "data_connection_dialog"

.field private static final POPUP_DATALIMIT:I = 0x4

.field private static final POPUP_DISABLE_AIRPLANEMODE:I = 0x1

.field private static final POPUP_ENABLE_MOBILE:I = 0x2

.field private static final POPUP_ENABLE_ROAMING:I = 0x3

.field private static final POPUP_MOBILE:I = 0x6

.field private static final POPUP_OUTOFSERVICE:I = 0x5

.field private static final POPUP_ROAMING:I = 0x7

.field private static final SELECTED_KEY:Ljava/lang/String; = "selected"


# instance fields
.field backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

.field cancelButtonClickListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

.field private checkDataConnectionPopup:Ljava/lang/Runnable;

.field private checkedforDisableAirplane:Z

.field private checkedforEnableMobile:Z

.field private checkedforEnableRoaming:Z

.field private checkedforMobile:Z

.field private checkedforRoaming:Z

.field connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field connectionFailBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field dataLimitBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private extraText:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field mWebView:Landroid/webkit/WebView;

.field private networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

.field okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

.field onCheckBoxClickListener:Landroid/view/View$OnClickListener;

.field outOfServiceBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

.field private popupMode:I

.field private userManualUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    .line 78
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;

    .line 199
    new-instance v0, Lcom/sec/android/app/shealth/settings/ManualActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ManualActivity$2;-><init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;

    .line 245
    new-instance v0, Lcom/sec/android/app/shealth/settings/ManualActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ManualActivity$3;-><init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->onCheckBoxClickListener:Landroid/view/View$OnClickListener;

    .line 273
    new-instance v0, Lcom/sec/android/app/shealth/settings/ManualActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ManualActivity$4;-><init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    .line 325
    new-instance v0, Lcom/sec/android/app/shealth/settings/ManualActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ManualActivity$5;-><init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->cancelButtonClickListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    .line 332
    new-instance v0, Lcom/sec/android/app/shealth/settings/ManualActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ManualActivity$6;-><init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    .line 363
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->extraText:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/shealth/settings/ManualActivity;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->extraText:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/settings/ManualActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/settings/ManualActivity;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/settings/ManualActivity;->procesCancel(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/settings/ManualActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforMobile:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforMobile:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforRoaming:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforRoaming:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/settings/ManualActivity;)Lcom/sec/android/app/shealth/common/utils/NetworkUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforDisableAirplane:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforDisableAirplane:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableMobile:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableMobile:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/settings/ManualActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableRoaming:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/shealth/settings/ManualActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforEnableRoaming:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/settings/ManualActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/settings/ManualActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/settings/ManualActivity;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private procesCancel(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 339
    iget v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->popupMode:I

    packed-switch v0, :pswitch_data_0

    .line 359
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setResult(I)V

    .line 360
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 361
    :goto_0
    return-void

    .line 345
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforMobile:Z

    if-eqz v0, :cond_0

    .line 346
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setManualMobilePopupWasShown(Landroid/content/Context;Z)V

    .line 348
    :cond_0
    invoke-virtual {p1, v1}, Landroid/app/Activity;->setResult(I)V

    .line 349
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 352
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkedforRoaming:Z

    if-eqz v0, :cond_1

    .line 353
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setManualRoamingPopupWasShown(Landroid/content/Context;Z)V

    .line 355
    :cond_1
    invoke-virtual {p1, v1}, Landroid/app/Activity;->setResult(I)V

    .line 356
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 426
    invoke-static {p1}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isExternalKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    const/4 v0, 0x1

    .line 429
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 2
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 83
    const-string v1, "data_connection_dialog"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/settings/ManualActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/settings/ManualActivity$1;-><init>(Lcom/sec/android/app/shealth/settings/ManualActivity;)V

    .line 159
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 373
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 374
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/common/utils/NetworkUtils;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->networkUtils:Lcom/sec/android/app/shealth/common/utils/NetworkUtils;

    .line 375
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 376
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->checkDataConnectionPopup:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 377
    const v2, 0x7f03017e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->setContentView(I)V

    .line 378
    const v2, 0x7f08066f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/ManualActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mWebView:Landroid/webkit/WebView;

    .line 379
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 381
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 382
    .local v1, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 383
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 384
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 385
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 386
    invoke-virtual {v1, v5}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 387
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/android/app/shealth/settings/ManualActivity$mWebViewClient;

    invoke-direct {v3}, Lcom/sec/android/app/shealth/settings/ManualActivity$mWebViewClient;-><init>()V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 389
    if-eqz p1, :cond_1

    .line 390
    const-string/jumbo v2, "user_manual_url"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->userManualUrl:Ljava/lang/String;

    .line 396
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->userManualUrl:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 397
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mWebView:Landroid/webkit/WebView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->userManualUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 400
    :cond_0
    return-void

    .line 392
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 393
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "user_manual_url"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->userManualUrl:Ljava/lang/String;

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 411
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 413
    const/4 v0, 0x1

    .line 415
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPostCreate()V
    .locals 5

    .prologue
    const v4, 0x7f09078f

    const v3, 0x7f0300a4

    const/4 v2, 0x2

    .line 171
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setNeedRedraw(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->connectionFailBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 176
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0907a0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->cancelButtonClickListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->dataLimitBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 182
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09004b

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09004c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->cancelButtonClickListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->outOfServiceBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 189
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f0907a1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0300a2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09009f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->okButtonClickListener:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->backPressListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->cancelButtonClickListenernew:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->connectionBuilder:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 197
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 164
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ManualActivity;->onPostCreate()V

    .line 166
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 404
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->userManualUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 406
    const-string/jumbo v0, "user_manual_url"

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ManualActivity;->userManualUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_0
    return-void
.end method

.method public startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;
    .param p2, "arg1"    # Landroid/os/UserHandle;

    .prologue
    .line 420
    const/4 v0, 0x0

    return-object v0
.end method
