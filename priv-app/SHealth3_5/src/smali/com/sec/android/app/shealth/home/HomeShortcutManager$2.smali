.class Lcom/sec/android/app/shealth/home/HomeShortcutManager$2;
.super Ljava/lang/Object;
.source "HomeShortcutManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/HomeShortcutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$2;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 143
    const v1, 0x7f080501

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 157
    .local v0, "iconContainer":Landroid/widget/FrameLayout;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 176
    :goto_0
    :pswitch_0
    const/4 v1, 0x0

    return v1

    .line 161
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$2;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    # getter for: Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->access$000(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)Lcom/sec/android/app/shealth/home/HomeActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 167
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/HomeShortcutManager$2;->this$0:Lcom/sec/android/app/shealth/home/HomeShortcutManager;

    # getter for: Lcom/sec/android/app/shealth/home/HomeShortcutManager;->mActivity:Lcom/sec/android/app/shealth/home/HomeActivity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/HomeShortcutManager;->access$000(Lcom/sec/android/app/shealth/home/HomeShortcutManager;)Lcom/sec/android/app/shealth/home/HomeActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700e0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
