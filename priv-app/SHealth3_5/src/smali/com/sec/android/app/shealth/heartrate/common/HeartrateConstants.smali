.class public final Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;
.super Ljava/lang/Object;
.source "HeartrateConstants.java"


# static fields
.field public static final ACCESSORY_NAME:Ljava/lang/String; = "custom_name"

.field public static final ACCESSORY_TYPE_SENSOR:Ljava/lang/String; = "10008"

.field public static final ACTIONBAR_ACC:I = 0x1

.field public static final ACTIONBAR_EDIT:I = 0x0

.field public static final ACTIONBAR_LOG:I = 0x0

.field public static final ACTIONBAR_SHARE:I = 0x2

.field public static final ALL:Ljava/lang/String; = "*"

.field public static final AND:Ljava/lang/String; = " AND "

.field public static ARE_DEFAULT_CUSTOM_TAGS_ADDED:Ljava/lang/String; = null

.field public static final ASC:Ljava/lang/String; = " ASC"

.field public static final ASC_LIMIT_1:Ljava/lang/String; = " ASC LIMIT 1"

.field public static final AVG_HEART_RATE:Ljava/lang/String; = "AvgHeartRate"

.field public static final AVG_MONTH:Ljava/lang/String; = "AvgMonth"

.field public static final CHOOSE_MORE_TAGS:I = 0x94

.field public static final CHOOSE_TAG:I = 0x93

.field public static final COMMENT_KEY:Ljava/lang/String; = "COMMENT_KEY"

.field public static final COUNT:I = 0x168

.field public static final CUSTOM_TAG_START_INDEX:I = 0x7530

.field public static final DATE_KEY:Ljava/lang/String; = "Date"

.field public static final DAYS_IN_MONTH:Ljava/lang/String; = "\"%d-%m-%Y\", "

.field public static final DAYS_IN_MONTH_GRAPH:Ljava/lang/String; = "STRFTIME(\"%d-%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

.field public static final DAYS_IN_WEEK:B = 0x7t

.field public static final DAY_IN_MONTH:B = 0x1ft

.field public static final DEFAULT_OF_VALUE:I = 0x0

.field public static final DEFAULT_TAG:I = 0x0

.field public static final DEFAULT_TAGS_NUMBER:I = 0x4

.field public static final DELAY:I = 0x9

.field public static final DESC:Ljava/lang/String; = " DESC"

.field public static final DESC_LIMIT_1:Ljava/lang/String; = " DESC LIMIT 1"

.field public static final DESC_LIMIT_2:Ljava/lang/String; = " DESC LIMIT 2"

.field public static final DIALOG_NON_SENSOR_INFO:I = 0x2

.field public static final EDIT:Ljava/lang/String; = "Edit"

.field public static final EQUAL_THAN:Ljava/lang/String; = "=?"

.field public static final FILTER_ALL:Ljava/lang/String; = "0==0"

.field public static final FIRST_OF_MONTH:I = 0x1

.field public static final FIRST_RECENT_TAG_ID:Ljava/lang/String; = "first_r_tag"

.field public static final FIRST_RECENT_TAG_USERDEFINED_TYPE:Ljava/lang/String; = "first_r_tag_type"

.field public static final FRI:I = 0x6

.field public static final FROM_MORE_TAG:Ljava/lang/String; = "from_more_tag"

.field public static final HANDLER_MESSAGE_MEASURING:I = 0x1

.field public static final HANDLER_MESSAGE_MEASURING_END:I = 0x3

.field public static final HANDLER_MESSAGE_MEASURING_FAIL:I = 0x2

.field public static final HANDLER_MESSAGE_NON_SENSOR_UI:I = 0x4

.field public static final HANDLER_MESSAGE_READY:I = 0x0

.field public static final HANDLER_NOT_SHOW_ANIMATION:I = 0x1

.field public static final HANDLER_SHOW_ANIMATION:I = 0x0

.field public static final HAS_DEFAULT_TAG:Ljava/lang/String; = "have_default_tag"

.field public static final HAS_MORE_DEFAULT_TAG:Ljava/lang/String; = "has_more_default_tag_pref"

.field public static final HEARTRATE_CHARTTAB:Ljava/lang/String; = "HEARTRATE_CHARTTAB"

.field public static final HEARTRATE_LOGGING_ADDINGTAG:Ljava/lang/String; = "HR12"

.field public static final HEARTRATE_LOGGING_APP_ID:Ljava/lang/String; = "com.sec.android.app.shealth.heartrate"

.field public static final HEARTRATE_LOGGING_CHART:Ljava/lang/String; = "HR04"

.field public static final HEARTRATE_LOGGING_DELETINGTAG:Ljava/lang/String; = "HR14"

.field public static final HEARTRATE_LOGGING_EDITINGTAG:Ljava/lang/String; = "HR13"

.field public static final HEARTRATE_LOGGING_FAIL:Ljava/lang/String; = "HR05"

.field public static final HEARTRATE_LOGGING_FINISHED:Ljava/lang/String; = "HR02"

.field public static final HEARTRATE_LOGGING_HELP:Ljava/lang/String; = "HR07"

.field public static final HEARTRATE_LOGGING_HISTORY:Ljava/lang/String; = "HR03"

.field public static final HEARTRATE_LOGGING_SELECTTAG:Ljava/lang/String; = "HR15"

.field public static final HEARTRATE_LOGGING_SHARVIA_1:Ljava/lang/String; = "HR08"

.field public static final HEARTRATE_LOGGING_SHARVIA_2:Ljava/lang/String; = "HR09"

.field public static final HEARTRATE_LOGGING_SHARVIA_3:Ljava/lang/String; = "HR10"

.field public static final HEARTRATE_LOGGING_SHARVIA_4:Ljava/lang/String; = "HR11"

.field public static final HEARTRATE_LOGGING_START:Ljava/lang/String; = "HR01"

.field public static final HEARTRATE_LOGGING_WIDGET:Ljava/lang/String; = "HR06"

.field public static final HEARTRATE_WIDGET:Ljava/lang/String; = "HEARTRATE_WIDGET_CLICKED"

.field public static final HEART_RATE_ID_KEY:Ljava/lang/String; = "HEART_RATE_ID_KEY"

.field public static final HEART_RATE_KEY:Ljava/lang/String; = "HEART_RATE_KEY"

.field public static final HOURS_IN_DAY_BYTE:B = 0x18t

.field public static final HRM_NONE_ERROR:I = 0x0

.field public static final HR_EFFEET_END:I = 0x4

.field public static final HR_EFFEET_ERROR:I = 0x3

.field public static final HR_EFFEET_MEASURE:I = 0x1

.field public static final HR_EFFEET_START:I = 0x0

.field public static final HR_EFFEET_STOP:I = 0x2

.field public static final HR_ERROR_MAX_VALUE:I = -0x9

.field public static final HR_ERROR_TIMEOUT:I = -0x6

.field public static final HR_FINGER_DETECTED:I = -0x1

.field public static final INVALID:I = -0x1

.field public static final IS:Ljava/lang/String; = " IS "

.field public static final ISADD_TAG:Ljava/lang/String; = "isaddtag"

.field public static IS_CUSTOM_MODIFIED:Ljava/lang/String; = null

.field public static final LAST_HOUR_IN_DAY:I = 0x17

.field public static final LAST_MILLI_IN_SECOND:I = 0x3e7

.field public static final LAST_MINUTE_IN_HOUR:I = 0x3b

.field public static final LAST_SECOND_IN_MINUTE:I = 0x3b

.field public static final LATEST_CUSTOM_ID:Ljava/lang/String; = "latest_custom_created_id"

.field public static final LESS_THAN:Ljava/lang/String; = "<?"

.field public static final MAX_CUSTOM_TAG_COUNT:I = 0x14

.field public static final MAX_HEART_RATE:Ljava/lang/String; = "MaxHeartRate"

.field public static final MAX_MONTH:Ljava/lang/String; = "MaxMonth"

.field public static final MILLIS_IN_DAY:I = 0x5265c00

.field public static final MILLIS_IN_HOUR:I = 0x36ee80

.field public static final MILLIS_IN_MONTH:J = 0x9fa52400L

.field public static final MILLIS_IN_SECOND:I = 0x3e8

.field public static final MINUTES_IN_DAY_GRAPH:Ljava/lang/String; = "(C.[sample_time]/(1000 * 60 *5))"

.field public static final MINUTES_IN_HOUR:B = 0x3ct

.field public static final MIN_HEART_RATE:Ljava/lang/String; = "MinHeartRate"

.field public static final MIN_MONTH:Ljava/lang/String; = "MinMonth"

.field public static final MODE_KEY:Ljava/lang/String; = "MODE_KEY"

.field public static final MON:I = 0x2

.field public static final MONTHS_IN_YEAR:Ljava/lang/String; = "\"%m-%Y\", "

.field public static final MONTHS_IN_YEAR_GRAPH:Ljava/lang/String; = "STRFTIME(\"%m-%Y\", C.[sample_time]/1000, \'unixepoch\', \'localtime\')"

.field public static final MORE_TAG_ICON_ID:Ljava/lang/String; = "more_tag_icon_id"

.field public static final MORE_TAG_ID:Ljava/lang/String; = "more_tag_id"

.field public static final MORE_TAG_NAME:Ljava/lang/String; = "more_tag_name"

.field public static final MORE_THAN:Ljava/lang/String; = ">?"

.field public static final NULL:Ljava/lang/String; = " NULL "

.field public static final REQUEST_FOR_DETAIL:I = 0x45b

.field public static final REQUEST_FOR_INFORMATION:I = 0x45a

.field public static final REQUEST_FOR_TAG:I = 0x45c

.field public static final RESULT_TAG_ICON_ID:Ljava/lang/String; = "result_tag_icon_id"

.field public static final RESULT_TAG_ID:Ljava/lang/String; = "result_tag_id"

.field public static final RESULT_TAG_NAME:Ljava/lang/String; = "result_tag_name"

.field public static final SCOVER_WINDOW_HEIGHT:I = 0x121

.field public static final SCOVER_WINDOW_WIDTH:I = 0x145

.field public static final SECONDS_IN_MINUTE:B = 0x3ct

.field public static final SECOND_RECENT_TAG_ID:Ljava/lang/String; = "second_r_tag"

.field public static SELECTED_FILTER_INDEX:I = 0x0

.field public static final SUN:I = 0x1

.field public static final TAG_ICON_ID_KEY:Ljava/lang/String; = "TAG_ICON_ID"

.field public static final TAG_INDEX_KEY:Ljava/lang/String; = "TAG_INDEX"

.field public static final TAG_TEXT_ID_KEY:Ljava/lang/String; = "TAG_TEXT_ID"

.field public static final TAG_TEXT_KEY:Ljava/lang/String; = "TAG_TEXT"

.field public static final TAG_TYPE_PREDEFINED:I = 0x2

.field public static final THIRD_RECENT_TAG_ID:Ljava/lang/String; = "third_r_tag"

.field public static final THU:I = 0x5

.field public static final TIME_DATE_KEY:Ljava/lang/String; = "TIME_DATE_KEY"

.field public static final TIME_ZONE:Ljava/lang/String; = " \'unixepoch\', \'localtime\')"

.field public static final TUE:I = 0x3

.field public static final WARNING_CHECKED:Ljava/lang/String; = "heartrate_warning_checked"

.field public static final WED:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "iscustomModified"

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;->IS_CUSTOM_MODIFIED:Ljava/lang/String;

    .line 49
    const-string v0, "areDefaultCustomTagsAdded"

    sput-object v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;->ARE_DEFAULT_CUSTOM_TAGS_ADDED:Ljava/lang/String;

    .line 174
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/heartrate/common/HeartrateConstants;->SELECTED_FILTER_INDEX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
