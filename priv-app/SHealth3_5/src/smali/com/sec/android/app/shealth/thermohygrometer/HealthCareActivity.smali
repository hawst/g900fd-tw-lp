.class public abstract Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "HealthCareActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;
.implements Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;


# instance fields
.field private mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->onLogSelected()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 94
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getActionBarTitleId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    .line 97
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207c0

    const v3, 0x7f09005a

    new-instance v4, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;)V

    invoke-direct {v1, v2, v5, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 104
    .local v1, "logButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 106
    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v2, 0x7f0207bc

    const v3, 0x7f090180

    new-instance v4, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity$2;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;)V

    invoke-direct {v0, v2, v5, v3, v4}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    .line 113
    .local v0, "connectionsButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    new-array v3, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v0, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 116
    return-void
.end method

.method protected abstract getActionBarTitleId()Ljava/lang/String;
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    move-result-object v0

    return-object v0
.end method

.method protected getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    .line 131
    .local v1, "fragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;, "TT;"
    if-nez v1, :cond_0

    .line 133
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "fragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;, "TT;"
    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 140
    .restart local v1    # "fragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;, "TT;"
    :cond_0
    return-object v1

    .line 134
    .end local v1    # "fragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;, "TT;"
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/InstantiationException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 136
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/IllegalAccessException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected abstract getGraphFragmentClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract getSummaryFragmentClass()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;",
            ">;"
        }
    .end annotation
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    .line 122
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    if-nez p1, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f080091

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 83
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->isDrawerMenuShown()Z

    move-result v0

    .line 88
    .local v0, "isDrawerMenuShown":Z
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;

    if-nez v0, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/thermohygrometer/summary/HealthCareSummaryFragment;->setHasOptionsMenu(Z)V

    .line 89
    if-nez v0, :cond_1

    :goto_1
    return v3

    :cond_0
    move v2, v4

    .line 88
    goto :goto_0

    :cond_1
    move v3, v4

    .line 89
    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 179
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f080c8a

    if-ne v3, v4, :cond_1

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.thermohygrometer"

    const-string v5, "TH02"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const/4 v1, 0x0

    .line 182
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getHelpItem()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 183
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string v3, "com.sec.shealth.action.HELP"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    .restart local v1    # "intent":Landroid/content/Intent;
    :goto_0
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return v2

    .line 185
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getHelpItem()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/lang/Exception;
    const-string/jumbo v3, "onOptionsItemSelected"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 195
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f080c9c

    if-ne v3, v4, :cond_2

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->launchSettingsActivity()V

    goto :goto_1

    .line 199
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f080c98

    if-ne v3, v4, :cond_3

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.android.app.shealth.thermohygrometer"

    const-string v5, "TH03"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->prepareShareView()V

    goto :goto_1

    .line 205
    :cond_3
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_1
.end method

.method protected abstract startConnectivityActivity()V
.end method

.method public switchFragmentToGraph()V
    .locals 5

    .prologue
    .line 156
    iget-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    if-nez v3, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getGraphFragmentClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v0

    .line 162
    .local v0, "graphFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    :goto_0
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 172
    :goto_1
    return-void

    .line 159
    .end local v0    # "graphFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    .line 160
    .restart local v0    # "graphFragment":Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    goto :goto_0

    .line 165
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;

    .line 166
    .local v1, "summaryFragment":Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/logutils/graph/GraphLogUtils;->addBundleToGraphFragment(Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 168
    .local v2, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v3, 0x7f080091

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v0, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 169
    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 170
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 171
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    goto :goto_1
.end method

.method public switchFragmentToSummary()V
    .locals 3

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getGraphFragmentClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    iput-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 147
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getSummaryFragmentClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getFragment(Ljava/lang/Class;)Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->mGraphFragment:Lcom/sec/android/app/shealth/framework/ui/base/GraphFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 149
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/HealthCareActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 151
    return-void
.end method
