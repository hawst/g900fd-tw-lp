.class Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;
.super Ljava/lang/Object;
.source "AutoBackupActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PeriodItemsCheckListener"
.end annotation


# instance fields
.field mPosition:I

.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;I)V
    .locals 0
    .param p2, "position"    # I

    .prologue
    .line 504
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 506
    iput p2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->mPosition:I

    .line 507
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 511
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mClickState:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$1602(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;Z)Z

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mIsBottomViewEnabled:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$700(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mPeriodicSyncManager:Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$200(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->interval:[J
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$400()[J

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->mPosition:I

    aget-wide v2, v2, v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/shealth/settings/samsungAccount/PeriodicSyncManager;->setPeroidicSync(Landroid/content/Context;J)V

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->interval:[J
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$400()[J

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->mPosition:I

    aget-wide v1, v1, v2

    # setter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mSyncInterval:J
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$102(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;J)J

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->interval:[J
    invoke-static {}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$400()[J

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->mPosition:I

    aget-wide v1, v1, v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->setAutoBackupInterval(Landroid/content/Context;J)V

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$PeriodItemsCheckListener;->this$0:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;

    # getter for: Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->mAdapter:Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;->access$300(Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity;)Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/settings/samsungAccount/AutoBackupActivity$IntervalItemAdapter;->notifyDataSetChanged()V

    .line 520
    return-void
.end method
