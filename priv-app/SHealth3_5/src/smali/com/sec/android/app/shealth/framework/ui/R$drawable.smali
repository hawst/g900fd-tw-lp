.class public final Lcom/sec/android/app/shealth/framework/ui/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/framework/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_switch_bg_disabled_holo_light:I = 0x7f020000

.field public static final ab_switch_bg_focused_holo_light:I = 0x7f020001

.field public static final ab_switch_bg_holo_light:I = 0x7f020002

.field public static final accessory_product_samsung_activitytracker:I = 0x7f020011

.field public static final accessory_settings_button_selector:I = 0x7f02001b

.field public static final accessory_temperature_bg:I = 0x7f02001c

.field public static final accessory_temperature_bp:I = 0x7f02001d

.field public static final accessory_temperature_hr:I = 0x7f02001e

.field public static final accessory_temperature_weight:I = 0x7f02001f

.field public static final action_bar_btn_overflow:I = 0x7f020020

.field public static final action_bar_spinner_selecter:I = 0x7f020021

.field public static final action_item_background_select_mode:I = 0x7f020022

.field public static final action_item_background_selector:I = 0x7f020023

.field public static final action_item_cab_background_selector:I = 0x7f020024

.field public static final actionbar_item_background_selector:I = 0x7f020025

.field public static final airview_popup_picker_bg_light:I = 0x7f020026

.field public static final apptheme_btn_radio_holo_light:I = 0x7f020027

.field public static final apptheme_edit_text_holo_light:I = 0x7f020028

.field public static final bottom_button_selector:I = 0x7f02002d

.field public static final cal_day_select:I = 0x7f020032

.field public static final cal_month_data_etc:I = 0x7f020037

.field public static final cal_month_popup_title_back:I = 0x7f020038

.field public static final cal_month_popup_title_back_dim:I = 0x7f020039

.field public static final cal_month_popup_title_back_dimfocus:I = 0x7f02003a

.field public static final cal_month_popup_title_back_focus:I = 0x7f02003b

.field public static final cal_month_popup_title_back_nor:I = 0x7f02003c

.field public static final cal_month_popup_title_back_press:I = 0x7f02003d

.field public static final cal_month_popup_title_next:I = 0x7f02003e

.field public static final cal_month_popup_title_next_dim:I = 0x7f02003f

.field public static final cal_month_popup_title_next_dimfocus:I = 0x7f020040

.field public static final cal_month_popup_title_next_focus:I = 0x7f020041

.field public static final cal_month_popup_title_next_nor:I = 0x7f020042

.field public static final cal_month_popup_title_next_press:I = 0x7f020043

.field public static final cal_month_popup_title_today:I = 0x7f020044

.field public static final cal_month_select:I = 0x7f020045

.field public static final cal_month_title_back:I = 0x7f020046

.field public static final cal_month_title_back_focus:I = 0x7f020047

.field public static final cal_month_title_back_press:I = 0x7f020048

.field public static final cal_month_title_next:I = 0x7f020049

.field public static final cal_month_title_next_focus:I = 0x7f02004a

.field public static final cal_month_title_next_press:I = 0x7f02004b

.field public static final cal_month_today_list:I = 0x7f02004c

.field public static final calendar_horizontall_divider:I = 0x7f02004d

.field public static final calendar_month_year_button_bottom_selector:I = 0x7f02004e

.field public static final calendar_month_year_button_top_selector:I = 0x7f02004f

.field public static final calendar_next_selector:I = 0x7f020050

.field public static final calendar_prev_selector:I = 0x7f020051

.field public static final calendar_textview_selector:I = 0x7f020052

.field public static final calendar_vertical_divider:I = 0x7f020053

.field public static final checkbox_selector:I = 0x7f020056

.field public static final checkbox_selector_for_textview:I = 0x7f020057

.field public static final common_btn_bg:I = 0x7f020118

.field public static final common_cbx_bg:I = 0x7f020119

.field public static final common_et_bg:I = 0x7f02011a

.field public static final common_rb_bg:I = 0x7f02011b

.field public static final common_spn_bg:I = 0x7f020134

.field public static final common_thumb_switch_selector:I = 0x7f020135

.field public static final common_track_switch_selector:I = 0x7f020136

.field public static final contacts_default_caller_id:I = 0x7f020138

.field public static final custom_cursor:I = 0x7f02013d

.field public static final custom_popup_background_material:I = 0x7f02013e

.field public static final date_bar_selector:I = 0x7f020140

.field public static final dateselector_next_selector:I = 0x7f020141

.field public static final dateselector_prev_selector:I = 0x7f020142

.field public static final day_button_selector:I = 0x7f020143

.field public static final default_button_selector:I = 0x7f020144

.field public static final default_checkbox_selector:I = 0x7f020145

.field public static final default_dropdown_list_item_selector:I = 0x7f020146

.field public static final default_dropdown_list_selector:I = 0x7f020147

.field public static final drag:I = 0x7f020148

.field public static final drawer_icon_blood_glucose:I = 0x7f020149

.field public static final drawer_icon_blood_pressure:I = 0x7f02014a

.field public static final drawer_icon_coach:I = 0x7f02014b

.field public static final drawer_icon_exerise:I = 0x7f02014c

.field public static final drawer_icon_food:I = 0x7f02014d

.field public static final drawer_icon_heartrate:I = 0x7f02014e

.field public static final drawer_icon_home:I = 0x7f02014f

.field public static final drawer_icon_hr:I = 0x7f020150

.field public static final drawer_icon_hygrometer:I = 0x7f020151

.field public static final drawer_icon_more_apps:I = 0x7f020152

.field public static final drawer_icon_mychallenge:I = 0x7f020153

.field public static final drawer_icon_pedometer:I = 0x7f020154

.field public static final drawer_icon_sleep:I = 0x7f020155

.field public static final drawer_icon_spo2:I = 0x7f020156

.field public static final drawer_icon_stress:I = 0x7f020157

.field public static final drawer_icon_uv:I = 0x7f020158

.field public static final drawer_icon_weight:I = 0x7f020159

.field public static final drawer_list_photo_bg_01:I = 0x7f02015a

.field public static final exercise_pro_action_item_background_focus:I = 0x7f020175

.field public static final exercise_pro_action_item_background_press:I = 0x7f020176

.field public static final exercise_pro_action_item_background_select:I = 0x7f020177

.field public static final f_airview_infopreview_thumb_bg_light:I = 0x7f02018d

.field public static final fairview_internet_folder_hover_bg_dark:I = 0x7f02018e

.field public static final fliper_date_selector:I = 0x7f02018f

.field public static final food_tw_drawerlist_selected_holo_light:I = 0x7f0201be

.field public static final frag_scanning:I = 0x7f0201c0

.field public static final horizontal_input_module_back_btn_selector:I = 0x7f0201eb

.field public static final horizontal_input_module_controller_view_background_green_pin:I = 0x7f0201ec

.field public static final horizontal_input_module_controller_view_background_orange_pin:I = 0x7f0201ed

.field public static final horizontal_input_module_next_btn_selector:I = 0x7f0201ee

.field public static final ic_launcher:I = 0x7f02020b

.field public static final ic_menu_moreoverflow_disable_holo_light:I = 0x7f02020c

.field public static final ic_menu_moreoverflow_normal_holo_light:I = 0x7f02020d

.field public static final icon_foreground:I = 0x7f020212

.field public static final input_common_edit_text_background_selector:I = 0x7f02021a

.field public static final input_edit_text_background_selector:I = 0x7f02021b

.field public static final list_selector:I = 0x7f020222

.field public static final log_list_selectall_selector:I = 0x7f020228

.field public static final menu_icon_delete:I = 0x7f020230

.field public static final menu_icon_filter:I = 0x7f020231

.field public static final menu_icon_guide:I = 0x7f020232

.field public static final menu_icon_print:I = 0x7f020233

.field public static final menu_icon_set_goal:I = 0x7f020234

.field public static final menu_icon_share:I = 0x7f020235

.field public static final menu_selector:I = 0x7f020236

.field public static final navigation_selector:I = 0x7f020243

.field public static final no_item_bg_light_v:I = 0x7f020246

.field public static final options_menu_item_selector:I = 0x7f02024c

.field public static final popup_bottom_button_selector_bg:I = 0x7f02024d

.field public static final popup_button_selector_bg:I = 0x7f02024e

.field public static final profile_edit_text_background_selector:I = 0x7f020281

.field public static final s_health_accessory_big_galaxy_gear:I = 0x7f020288

.field public static final s_health_action_bar_back_nor:I = 0x7f020297

.field public static final s_health_action_bar_focus:I = 0x7f020298

.field public static final s_health_action_bar_icon_edit_nor:I = 0x7f020299

.field public static final s_health_action_bar_icon_loglist_nor:I = 0x7f02029a

.field public static final s_health_action_bar_icon_loglist_nor12:I = 0x7f02029b

.field public static final s_health_action_bar_icon_nor:I = 0x7f02029c

.field public static final s_health_action_bar_icon_paired_dim:I = 0x7f02029d

.field public static final s_health_action_bar_icon_paired_nor:I = 0x7f02029e

.field public static final s_health_action_bar_main_icon11:I = 0x7f0202a0

.field public static final s_health_actionbar_info:I = 0x7f0202a1

.field public static final s_health_blood_glucose_list_dot:I = 0x7f0202a2

.field public static final s_health_blood_glucose_mask:I = 0x7f0202a3

.field public static final s_health_blood_glucose_orange:I = 0x7f0202a4

.field public static final s_health_blood_glucose_setgoal:I = 0x7f0202a5

.field public static final s_health_blood_glucose_setgoal_pin_green:I = 0x7f0202a6

.field public static final s_health_blood_glucose_setgoal_pin_orange:I = 0x7f0202a7

.field public static final s_health_bottom_btn_icon:I = 0x7f0202a8

.field public static final s_health_calendar_bar:I = 0x7f0202a9

.field public static final s_health_calendar_h_sub_title_btn_normal:I = 0x7f0202aa

.field public static final s_health_calendar_h_sub_title_btn_press:I = 0x7f0202ab

.field public static final s_health_calendar_today:I = 0x7f0202ac

.field public static final s_health_drawer_setting:I = 0x7f0202cb

.field public static final s_health_favorites_check_off_disabled:I = 0x7f020397

.field public static final s_health_favorites_check_off_disabledfocused:I = 0x7f020398

.field public static final s_health_favorites_check_off_focus:I = 0x7f020399

.field public static final s_health_favorites_check_off_normal:I = 0x7f02039a

.field public static final s_health_favorites_check_off_press:I = 0x7f02039b

.field public static final s_health_favorites_check_on_disabled:I = 0x7f02039c

.field public static final s_health_favorites_check_on_disabledfocused:I = 0x7f02039d

.field public static final s_health_favorites_check_on_focus:I = 0x7f02039e

.field public static final s_health_favorites_check_on_normal:I = 0x7f02039f

.field public static final s_health_favorites_check_on_press:I = 0x7f0203a0

.field public static final s_health_favorites_checkbox_press:I = 0x7f0203a1

.field public static final s_health_glucose_noitem:I = 0x7f0203b1

.field public static final s_health_graph_default_bg:I = 0x7f0203b2

.field public static final s_health_graph_no_data:I = 0x7f0203c8

.field public static final s_health_h_common_icon_accessory:I = 0x7f0203e5

.field public static final s_health_h_exercise_detected:I = 0x7f0203e6

.field public static final s_health_h_list_icon_setting:I = 0x7f020407

.field public static final s_health_h_list_icon_sync:I = 0x7f020408

.field public static final s_health_h_walking_earth:I = 0x7f02040e

.field public static final s_health_hr_list_icon_bg:I = 0x7f020506

.field public static final s_health_icon_notification:I = 0x7f02055f

.field public static final s_health_input_divider:I = 0x7f020562

.field public static final s_health_input_exercise_icon_back_dim:I = 0x7f020563

.field public static final s_health_input_exercise_icon_back_dimfocus:I = 0x7f020564

.field public static final s_health_input_exercise_icon_back_focus:I = 0x7f020565

.field public static final s_health_input_exercise_icon_back_nor:I = 0x7f020566

.field public static final s_health_input_exercise_icon_back_press:I = 0x7f020567

.field public static final s_health_input_exercise_icon_down_dim:I = 0x7f020568

.field public static final s_health_input_exercise_icon_down_dimfocus:I = 0x7f020569

.field public static final s_health_input_exercise_icon_down_focus:I = 0x7f02056a

.field public static final s_health_input_exercise_icon_down_nor:I = 0x7f02056b

.field public static final s_health_input_exercise_icon_down_press:I = 0x7f02056c

.field public static final s_health_input_exercise_icon_next_dim:I = 0x7f02056d

.field public static final s_health_input_exercise_icon_next_dimfocus:I = 0x7f02056e

.field public static final s_health_input_exercise_icon_next_focus:I = 0x7f02056f

.field public static final s_health_input_exercise_icon_next_nor:I = 0x7f020570

.field public static final s_health_input_exercise_icon_next_press:I = 0x7f020571

.field public static final s_health_input_exercise_icon_up_dim:I = 0x7f020572

.field public static final s_health_input_exercise_icon_up_dimpress:I = 0x7f020573

.field public static final s_health_input_exercise_icon_up_focus:I = 0x7f020574

.field public static final s_health_input_exercise_icon_up_nor:I = 0x7f020575

.field public static final s_health_input_exercise_icon_up_press:I = 0x7f020576

.field public static final s_health_list_section_divider_holo_light:I = 0x7f0205a1

.field public static final s_health_medication_setgoal:I = 0x7f0205a8

.field public static final s_health_medication_setgoal_pin_green:I = 0x7f0205a9

.field public static final s_health_medication_setgoal_pin_orange:I = 0x7f0205aa

.field public static final s_health_noitem:I = 0x7f0205b7

.field public static final s_health_page_turn_summary_p:I = 0x7f0205b8

.field public static final s_health_page_turn_summary_p123:I = 0x7f0205b9

.field public static final s_health_pedometer_sub_action_bar_bg_default:I = 0x7f0205c4

.field public static final s_health_pedometer_sub_action_bar_bg_disabled:I = 0x7f0205c5

.field public static final s_health_pedometer_sub_action_bar_bg_focused:I = 0x7f0205c6

.field public static final s_health_pedometer_sub_action_bar_bg_pressed:I = 0x7f0205c7

.field public static final s_health_pedometer_sub_action_bar_bg_rtl_default:I = 0x7f0205c8

.field public static final s_health_pedometer_sub_action_bar_bg_rtl_disabled:I = 0x7f0205c9

.field public static final s_health_pedometer_sub_action_bar_bg_rtl_focused:I = 0x7f0205ca

.field public static final s_health_pedometer_sub_action_bar_bg_rtl_pressed:I = 0x7f0205cb

.field public static final s_health_pressure_noitem:I = 0x7f0205cf

.field public static final s_health_quickpanel_icon_02:I = 0x7f020605

.field public static final s_health_quickpanel_icon_app:I = 0x7f020608

.field public static final s_health_quickpanel_sub_icon:I = 0x7f020611

.field public static final s_health_quickpanel_sub_icon_gear:I = 0x7f020612

.field public static final s_health_quickpanel_sub_icon_gearfit:I = 0x7f020613

.field public static final s_health_sband_icon_sync:I = 0x7f020617

.field public static final s_health_scanning_animation:I = 0x7f020618

.field public static final s_health_splash:I = 0x7f020625

.field public static final s_health_summary_accessary:I = 0x7f020654

.field public static final s_health_summary_arrow_next_default:I = 0x7f020655

.field public static final s_health_summary_arrow_next_dim:I = 0x7f020656

.field public static final s_health_summary_arrow_next_disablefocused:I = 0x7f020657

.field public static final s_health_summary_arrow_next_focused:I = 0x7f020658

.field public static final s_health_summary_arrow_next_pressed:I = 0x7f020659

.field public static final s_health_summary_arrow_prev_default:I = 0x7f02065a

.field public static final s_health_summary_arrow_prev_dim:I = 0x7f02065b

.field public static final s_health_summary_arrow_prev_disablefocused:I = 0x7f02065c

.field public static final s_health_summary_arrow_prev_focused:I = 0x7f02065d

.field public static final s_health_summary_arrow_prev_pressed:I = 0x7f02065e

.field public static final s_health_summary_button_dim:I = 0x7f020664

.field public static final s_health_summary_button_disabled:I = 0x7f020665

.field public static final s_health_summary_button_discard:I = 0x7f020666

.field public static final s_health_summary_button_focus:I = 0x7f020667

.field public static final s_health_summary_button_normal:I = 0x7f020669

.field public static final s_health_summary_button_pause_dim:I = 0x7f02066b

.field public static final s_health_summary_button_plus:I = 0x7f02066c

.field public static final s_health_summary_button_plus_dim:I = 0x7f02066d

.field public static final s_health_summary_button_press:I = 0x7f02066e

.field public static final s_health_summary_button_pressselect:I = 0x7f02066f

.field public static final s_health_summary_button_refresh:I = 0x7f020672

.field public static final s_health_summary_button_select:I = 0x7f020673

.field public static final s_health_summary_pageturn:I = 0x7f02069c

.field public static final s_health_summary_plus:I = 0x7f0206d9

.field public static final s_health_summary_s_accessary:I = 0x7f0206dd

.field public static final s_health_temperature_acc_b2:I = 0x7f0206f0

.field public static final s_health_temperature_acc_sband:I = 0x7f0206f1

.field public static final s_health_temperature_samsung_gear:I = 0x7f0206f2

.field public static final s_health_temperature_samsung_gear2:I = 0x7f0206f3

.field public static final s_health_temperature_samsung_gearfit:I = 0x7f0206f4

.field public static final s_health_temperature_samsung_neogear:I = 0x7f0206f5

.field public static final s_health_title_wheel_bg_01:I = 0x7f020700

.field public static final s_health_weight_noitem:I = 0x7f020709

.field public static final scan_button_selector:I = 0x7f020737

.field public static final scanning_progress_circle:I = 0x7f020738

.field public static final select_all_bg:I = 0x7f02073b

.field public static final settings_drawable:I = 0x7f02073d

.field public static final settings_list_selector:I = 0x7f02073e

.field public static final share_item_bg_color:I = 0x7f02073f

.field public static final share_popup_item_selector:I = 0x7f020740

.field public static final spinner_divider:I = 0x7f020756

.field public static final sub_tab_background_selector:I = 0x7f020786

.field public static final summary_button_left_drawable_selector:I = 0x7f020787

.field public static final summary_button_selector:I = 0x7f020788

.field public static final summary_button_text_color_selector:I = 0x7f020789

.field public static final transparent_button_selector:I = 0x7f020797

.field public static final tw_ab_bottom_div_holo_light:I = 0x7f020799

.field public static final tw_ab_bottom_transparent_holo_light:I = 0x7f02079a

.field public static final tw_ab_bottom_transparent_mtrl:I = 0x7f02079c

.field public static final tw_ab_bottom_transparent_shadow_holo_light:I = 0x7f02079d

.field public static final tw_ab_solid_shadow_holo_light:I = 0x7f02079e

.field public static final tw_ab_spinner_list_focused_holo_light:I = 0x7f02079f

.field public static final tw_ab_spinner_list_pressed_holo_light:I = 0x7f0207a0

.field public static final tw_ab_transparent_dark_holo:I = 0x7f0207a1

.field public static final tw_ab_transparent_holo_light:I = 0x7f0207a2

.field public static final tw_ab_transparent_light_holo:I = 0x7f0207a3

.field public static final tw_action_bar_icon_add:I = 0x7f0207a4

.field public static final tw_action_bar_icon_add_disabled:I = 0x7f0207a5

.field public static final tw_action_bar_icon_camera:I = 0x7f0207a6

.field public static final tw_action_bar_icon_camera_disabled:I = 0x7f0207a7

.field public static final tw_action_bar_icon_cancel_02_disabled_holo_light:I = 0x7f0207a8

.field public static final tw_action_bar_icon_cancel_02_holo_light:I = 0x7f0207a9

.field public static final tw_action_bar_icon_check_disabled_holo_light:I = 0x7f0207aa

.field public static final tw_action_bar_icon_check_holo_light:I = 0x7f0207ab

.field public static final tw_action_bar_icon_clip:I = 0x7f0207ac

.field public static final tw_action_bar_icon_clip_disabled:I = 0x7f0207ad

.field public static final tw_action_bar_icon_connect:I = 0x7f0207ae

.field public static final tw_action_bar_icon_connect_disabled:I = 0x7f0207af

.field public static final tw_action_bar_icon_delete:I = 0x7f0207b0

.field public static final tw_action_bar_icon_delete_disabled:I = 0x7f0207b1

.field public static final tw_action_bar_icon_disconnect:I = 0x7f0207b2

.field public static final tw_action_bar_icon_disconnect_disabled:I = 0x7f0207b3

.field public static final tw_action_bar_icon_edit_nor:I = 0x7f0207b4

.field public static final tw_action_bar_icon_edit_nor_disabled:I = 0x7f0207b5

.field public static final tw_action_bar_icon_favorites:I = 0x7f0207b6

.field public static final tw_action_bar_icon_favorites_disabled:I = 0x7f0207b7

.field public static final tw_action_bar_icon_filter_disabled_holo_light:I = 0x7f0207b8

.field public static final tw_action_bar_icon_filter_holo_light:I = 0x7f0207b9

.field public static final tw_action_bar_icon_guide_disabled_holo_light:I = 0x7f0207ba

.field public static final tw_action_bar_icon_guide_holo_light:I = 0x7f0207bb

.field public static final tw_action_bar_icon_link:I = 0x7f0207bc

.field public static final tw_action_bar_icon_link_disabled:I = 0x7f0207bd

.field public static final tw_action_bar_icon_listoffavorites:I = 0x7f0207be

.field public static final tw_action_bar_icon_listoffavorites_disabled:I = 0x7f0207bf

.field public static final tw_action_bar_icon_loglist:I = 0x7f0207c0

.field public static final tw_action_bar_icon_loglist_disabled:I = 0x7f0207c1

.field public static final tw_action_bar_icon_map:I = 0x7f0207c2

.field public static final tw_action_bar_icon_map_disabled:I = 0x7f0207c3

.field public static final tw_action_bar_icon_print:I = 0x7f0207c4

.field public static final tw_action_bar_icon_print_disabled:I = 0x7f0207c5

.field public static final tw_action_bar_icon_print_disabled_holo_light:I = 0x7f0207c6

.field public static final tw_action_bar_icon_print_holo_light:I = 0x7f0207c7

.field public static final tw_action_bar_icon_profile:I = 0x7f0207c8

.field public static final tw_action_bar_icon_profile_disabled:I = 0x7f0207c9

.field public static final tw_action_bar_icon_reset_holo_light:I = 0x7f0207ca

.field public static final tw_action_bar_icon_search:I = 0x7f0207cb

.field public static final tw_action_bar_icon_search_disabled:I = 0x7f0207cc

.field public static final tw_action_bar_icon_set_goal_disabled_holo_light:I = 0x7f0207cd

.field public static final tw_action_bar_icon_set_goal_holo_light:I = 0x7f0207ce

.field public static final tw_action_bar_icon_share:I = 0x7f0207cf

.field public static final tw_action_bar_icon_share_disabled:I = 0x7f0207d0

.field public static final tw_action_bar_icon_sortby:I = 0x7f0207d1

.field public static final tw_action_bar_icon_sortby_disabled:I = 0x7f0207d2

.field public static final tw_action_bar_icon_total:I = 0x7f0207d3

.field public static final tw_action_bar_icon_total_disabled:I = 0x7f0207d4

.field public static final tw_action_bar_icon_viewby:I = 0x7f0207d5

.field public static final tw_action_bar_icon_viewby_disabled:I = 0x7f0207d6

.field public static final tw_action_bar_icon_workout:I = 0x7f0207d7

.field public static final tw_action_bar_icon_workout_disabled:I = 0x7f0207d8

.field public static final tw_action_bar_sub_tab_bg_01_holo_light:I = 0x7f0207d9

.field public static final tw_action_bar_sub_tab_bg_holo_light:I = 0x7f0207da

.field public static final tw_action_bar_sub_tab_selected_bg_holo_light:I = 0x7f0207db

.field public static final tw_action_bar_tab_bg_holo_light:I = 0x7f0207dc

.field public static final tw_action_bar_tab_selected_bg_holo_light:I = 0x7f0207de

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f0207e0

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f0207e1

.field public static final tw_action_item_background_selected_holo_light:I = 0x7f0207e2

.field public static final tw_app_default_tab_bg_holo_light:I = 0x7f0207e3

.field public static final tw_app_default_tab_selected_bg_holo_light:I = 0x7f0207e4

.field public static final tw_background_dark:I = 0x7f0207e5

.field public static final tw_btn_back_depth_disable_focus_holo_light:I = 0x7f0207e6

.field public static final tw_btn_back_depth_disabled_holo_light:I = 0x7f0207e7

.field public static final tw_btn_back_depth_focus_holo_light:I = 0x7f0207e8

.field public static final tw_btn_back_depth_holo_light:I = 0x7f0207e9

.field public static final tw_btn_back_depth_pressed_holo_light:I = 0x7f0207ea

.field public static final tw_btn_cab_done_focused_holo_light:I = 0x7f0207eb

.field public static final tw_btn_cab_done_pressed_holo_light:I = 0x7f0207ec

.field public static final tw_btn_check_off_disabled_focused_holo_light:I = 0x7f0207ed

.field public static final tw_btn_check_off_disabled_holo_light:I = 0x7f0207ee

.field public static final tw_btn_check_off_focused_holo_light:I = 0x7f0207ef

.field public static final tw_btn_check_off_holo_light:I = 0x7f0207f0

.field public static final tw_btn_check_off_pressed_holo_light:I = 0x7f0207f1

.field public static final tw_btn_check_on_disabled_focused_holo_light:I = 0x7f0207f2

.field public static final tw_btn_check_on_disabled_holo_light:I = 0x7f0207f3

.field public static final tw_btn_check_on_focused_holo_light:I = 0x7f0207f4

.field public static final tw_btn_check_on_holo_light:I = 0x7f0207f5

.field public static final tw_btn_check_on_pressed_holo_light:I = 0x7f0207f6

.field public static final tw_btn_default_disabled_focused_holo_light:I = 0x7f0207f7

.field public static final tw_btn_default_disabled_holo_light:I = 0x7f0207f8

.field public static final tw_btn_default_focused_holo_light:I = 0x7f0207f9

.field public static final tw_btn_default_normal_holo_light:I = 0x7f0207fa

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f0207fb

.field public static final tw_btn_default_selected_holo_light:I = 0x7f0207fc

.field public static final tw_btn_next_depth_disabled_focused_holo_light:I = 0x7f020804

.field public static final tw_btn_next_depth_disabled_holo_light:I = 0x7f020805

.field public static final tw_btn_next_depth_focused_holo_light:I = 0x7f020806

.field public static final tw_btn_next_depth_holo_light:I = 0x7f020807

.field public static final tw_btn_next_depth_pressed_holo_light:I = 0x7f020808

.field public static final tw_btn_radio_off_disabled_focused_holo_light:I = 0x7f02080a

.field public static final tw_btn_radio_off_disabled_holo_light:I = 0x7f02080b

.field public static final tw_btn_radio_off_focused_holo_light:I = 0x7f02080c

.field public static final tw_btn_radio_off_holo_light:I = 0x7f02080d

.field public static final tw_btn_radio_off_pressed_holo_light:I = 0x7f02080e

.field public static final tw_btn_radio_on_disabled_focused_holo_light:I = 0x7f02080f

.field public static final tw_btn_radio_on_disabled_holo_light:I = 0x7f020810

.field public static final tw_btn_radio_on_focused_holo_light:I = 0x7f020811

.field public static final tw_btn_radio_on_holo_light:I = 0x7f020812

.field public static final tw_btn_radio_on_pressed_holo_light:I = 0x7f020813

.field public static final tw_btn_radio_selector:I = 0x7f020814

.field public static final tw_btn_switch_ab_off_bg:I = 0x7f020815

.field public static final tw_btn_switch_ab_on_bg:I = 0x7f020816

.field public static final tw_btn_switch_ab_on_default:I = 0x7f020817

.field public static final tw_btn_switch_ab_on_focused:I = 0x7f020818

.field public static final tw_btn_switch_ab_on_pressed:I = 0x7f020819

.field public static final tw_btn_switch_off_default:I = 0x7f02081a

.field public static final tw_btn_switch_off_focused:I = 0x7f02081b

.field public static final tw_btn_switch_off_pressed:I = 0x7f02081c

.field public static final tw_buttonbarbutton_selector_default_holo_light:I = 0x7f02081d

.field public static final tw_buttonbarbutton_selector_disabled_focused_holo_light:I = 0x7f020820

.field public static final tw_buttonbarbutton_selector_disabled_holo_light:I = 0x7f020823

.field public static final tw_buttonbarbutton_selector_focused_holo_light:I = 0x7f020826

.field public static final tw_buttonbarbutton_selector_pressed_holo_light:I = 0x7f020829

.field public static final tw_buttonbarbutton_selector_selected_holo_light:I = 0x7f02082c

.field public static final tw_cab_background_top_holo_light:I = 0x7f02082f

.field public static final tw_dialog_bottom_holo_light:I = 0x7f020830

.field public static final tw_dialog_bottom_medium_holo_light:I = 0x7f020831

.field public static final tw_dialog_full_holo_light:I = 0x7f020832

.field public static final tw_dialog_middle_holo_light:I = 0x7f020833

.field public static final tw_dialog_middle_list_line:I = 0x7f020834

.field public static final tw_dialog_title_holo_light:I = 0x7f020835

.field public static final tw_dialog_top_holo_light:I = 0x7f020836

.field public static final tw_dialog_top_medium_holo_light:I = 0x7f020837

.field public static final tw_divider_ab_holo_light:I = 0x7f020838

.field public static final tw_divider_option_popup_holo_light:I = 0x7f020839

.field public static final tw_divider_popup_vertical_holo_light:I = 0x7f02083a

.field public static final tw_drawer_bg_holo_dark:I = 0x7f02083c

.field public static final tw_drawer_bg_holo_light:I = 0x7f02083d

.field public static final tw_drawer_list_line_holo_light:I = 0x7f02083e

.field public static final tw_drawerlist_disabled_focused_holo_light:I = 0x7f02083f

.field public static final tw_drawerlist_disabled_holo_light:I = 0x7f020840

.field public static final tw_drawerlist_focused_holo_light:I = 0x7f020841

.field public static final tw_drawerlist_pressed_holo_light:I = 0x7f020842

.field public static final tw_drawerlist_section_divider_holo_light2:I = 0x7f020843

.field public static final tw_drawerlist_selected_holo_light:I = 0x7f020844

.field public static final tw_ic_ab_back_holo_light:I = 0x7f020851

.field public static final tw_ic_ab_drawer_holo_light:I = 0x7f020852

.field public static final tw_ic_ab_share_holo_light:I = 0x7f020853

.field public static final tw_ic_cab_done_holo_light:I = 0x7f020854

.field public static final tw_list_disabled_focused_holo_light:I = 0x7f02085a

.field public static final tw_list_disabled_holo_light:I = 0x7f02085b

.field public static final tw_list_divider_holo_light:I = 0x7f02085c

.field public static final tw_list_focused_holo_light:I = 0x7f02085d

.field public static final tw_list_longpressed_holo_light:I = 0x7f020864

.field public static final tw_list_pressed_holo_light:I = 0x7f020865

.field public static final tw_list_section_divider_holo_light:I = 0x7f020867

.field public static final tw_list_selected_holo_light:I = 0x7f02086c

.field public static final tw_list_selector_disabled_holo_light:I = 0x7f02086d

.field public static final tw_menu_ab_dropdown_panel_holo_light:I = 0x7f02086e

.field public static final tw_menu_dropdown_panel_holo_light:I = 0x7f02086f

.field public static final tw_menu_hardkey_panel_holo_light:I = 0x7f020870

.field public static final tw_menu_popup_panel_holo_light:I = 0x7f020871

.field public static final tw_no_item_popup_bg_holo_light:I = 0x7f020872

.field public static final tw_select_all_bg_holo_light:I = 0x7f020879

.field public static final tw_spinner_ab_default_holo_light_am:I = 0x7f02087a

.field public static final tw_spinner_ab_disabled_holo_light_am:I = 0x7f02087b

.field public static final tw_spinner_ab_focused_holo_light:I = 0x7f02087c

.field public static final tw_spinner_ab_focused_holo_light_am:I = 0x7f02087d

.field public static final tw_spinner_ab_pressed_holo_light:I = 0x7f02087e

.field public static final tw_spinner_ab_pressed_holo_light_am:I = 0x7f02087f

.field public static final tw_spinner_default_holo_light:I = 0x7f020880

.field public static final tw_spinner_default_holo_light_am:I = 0x7f020881

.field public static final tw_spinner_disabled_holo_light:I = 0x7f020882

.field public static final tw_spinner_disabled_holo_light_am:I = 0x7f020883

.field public static final tw_spinner_focused_holo_light:I = 0x7f020884

.field public static final tw_spinner_focused_holo_light_am:I = 0x7f020885

.field public static final tw_spinner_list_focused_holo_light:I = 0x7f020886

.field public static final tw_spinner_list_pressed_holo_light:I = 0x7f020887

.field public static final tw_spinner_pressed_holo_light:I = 0x7f020888

.field public static final tw_spinner_pressed_holo_light_am:I = 0x7f020889

.field public static final tw_sub_tab_divider_holo_light:I = 0x7f02088b

.field public static final tw_switch_ab_thumb_activated_holo_light:I = 0x7f02088c

.field public static final tw_switch_activation_holo_light:I = 0x7f02088d

.field public static final tw_switch_activation_holo_light_dim:I = 0x7f02088e

.field public static final tw_switch_activation_holo_light_pressed:I = 0x7f02088f

.field public static final tw_switch_bg_disabled_holo_light:I = 0x7f020890

.field public static final tw_switch_bg_focused_holo_light:I = 0x7f020891

.field public static final tw_switch_bg_holo_light:I = 0x7f020892

.field public static final tw_switch_disabled_holo_light:I = 0x7f020893

.field public static final tw_switch_disabled_holo_light_dim:I = 0x7f020894

.field public static final tw_switch_disabled_holo_light_pressed:I = 0x7f020895

.field public static final tw_switch_thumb_activated_holo_light:I = 0x7f020896

.field public static final tw_switch_thumb_activation_disabled_holo_light:I = 0x7f020897

.field public static final tw_switch_thumb_activation_pressed_holo_light:I = 0x7f020898

.field public static final tw_switch_thumb_disabled_holo_light:I = 0x7f020899

.field public static final tw_switch_thumb_holo_light:I = 0x7f02089a

.field public static final tw_switch_thumb_pressed_holo_light:I = 0x7f02089b

.field public static final tw_tab_divider_holo_light:I = 0x7f02089c

.field public static final tw_tab_selected_focused_holo_light:I = 0x7f02089d

.field public static final tw_tab_selected_focused_pressed_holo_light:I = 0x7f02089e

.field public static final tw_tab_selected_pressed_holo_light:I = 0x7f02089f

.field public static final tw_textfield_activated_holo_light:I = 0x7f0208a0

.field public static final tw_textfield_default_holo_light:I = 0x7f0208a1

.field public static final tw_textfield_disabled_focused_holo_light:I = 0x7f0208a2

.field public static final tw_textfield_disabled_holo_light:I = 0x7f0208a3

.field public static final tw_textfield_focused_holo_light:I = 0x7f0208a4

.field public static final tw_textfield_multiline_activated_holo_light:I = 0x7f0208a5

.field public static final tw_textfield_multiline_default_holo_light:I = 0x7f0208a6

.field public static final tw_textfield_multiline_disabled_focused_holo_light:I = 0x7f0208a7

.field public static final tw_textfield_multiline_disabled_holo_light:I = 0x7f0208a8

.field public static final tw_textfield_multiline_focused_holo_light:I = 0x7f0208a9

.field public static final tw_textfield_multiline_pressed_holo_light:I = 0x7f0208aa

.field public static final tw_textfield_multiline_selected_holo_light:I = 0x7f0208ab

.field public static final tw_textfield_pressed_holo_light:I = 0x7f0208ac

.field public static final tw_textfield_selected_holo_light:I = 0x7f0208b1

.field public static final tw_toast_frame_holo_light:I = 0x7f0208b2

.field public static final tw_widget_progressbar:I = 0x7f0208b3

.field public static final tw_widget_progressbar_holo_light:I = 0x7f0208b4

.field public static final tw_widget_progressbar_holo_light_02:I = 0x7f0208b5

.field public static final vertical_input_module_controller_view_background_green_pin:I = 0x7f0208ce

.field public static final vertical_input_module_controller_view_background_orange_pin:I = 0x7f0208cf

.field public static final vertical_input_module_down_btn_selector:I = 0x7f0208d0

.field public static final vertical_input_module_up_btn_selector:I = 0x7f0208d1

.field public static final welcomepage_s_health:I = 0x7f0208f6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
