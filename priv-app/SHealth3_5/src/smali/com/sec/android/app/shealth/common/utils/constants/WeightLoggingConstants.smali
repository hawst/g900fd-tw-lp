.class public Lcom/sec/android/app/shealth/common/utils/constants/WeightLoggingConstants;
.super Ljava/lang/Object;
.source "WeightLoggingConstants.java"


# static fields
.field public static final LOGGING_CHART_VIEW:Ljava/lang/String; = "WT05"

.field public static final LOGGING_CHART_VIEW_SHARE_VIA:Ljava/lang/String; = "WT08"

.field public static final LOGGING_CONNECT_ACCESSORIES:Ljava/lang/String; = "WT06"

.field public static final LOGGING_HELP:Ljava/lang/String; = "WT03"

.field public static final LOGGING_LOG:Ljava/lang/String; = "WT04"

.field public static final LOGGING_LOG_DETAIL_SHARE_VIA:Ljava/lang/String; = "WT07"

.field public static final LOGGING_LOG_SHARE_VIA:Ljava/lang/String; = "WT02"

.field public static final LOGGING_SUMMARY_VIEW_SHARE_VIA:Ljava/lang/String; = "WT01"

.field public static final SET_GOAL_ADD:Ljava/lang/String; = "3001"

.field public static final SUMMARY_UPDATE_DONE:Ljava/lang/String; = "2001"

.field public static final WEIGHT_APP_ID:Ljava/lang/String; = "com.sec.android.app.shealth.weight"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    return-void
.end method
