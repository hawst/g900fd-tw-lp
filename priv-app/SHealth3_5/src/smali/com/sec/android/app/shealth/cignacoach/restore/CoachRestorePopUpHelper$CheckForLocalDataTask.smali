.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;
.super Landroid/os/AsyncTask;
.source "CoachRestorePopUpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckForLocalDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 209
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "doInBackground"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isLocalDataPresent()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 206
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isLocalDataPresent"    # Ljava/lang/Boolean;

    .prologue
    .line 215
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onCancelled"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 206
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->onCancelled(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isLocalDataPresent"    # Ljava/lang/Boolean;

    .prologue
    .line 220
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onPostExecute"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->activity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    # invokes: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->showRestoreTriggerPopup(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$700(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Z)V

    .line 223
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 206
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$CheckForLocalDataTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
