.class final Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
.super Ljava/lang/Object;
.source "FatSecretFoodParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UnitHolder"
.end annotation


# static fields
.field private static final DEFAULT_VALUE:F = -1.0f


# instance fields
.field final unitNames:[Ljava/lang/String;

.field private value:F


# direct methods
.method private varargs constructor <init>([Ljava/lang/String;)V
    .locals 1
    .param p1, "unitNames"    # [Ljava/lang/String;

    .prologue
    .line 517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F

    .line 518
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->unitNames:[Ljava/lang/String;

    .line 519
    return-void
.end method

.method synthetic constructor <init>([Ljava/lang/String;Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$1;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/String;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$1;

    .prologue
    .line 512
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;-><init>([Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    .prologue
    .line 512
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
    .param p1, "x1"    # F

    .prologue
    .line 512
    iput p1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F

    return p1
.end method

.method static synthetic access$140(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;
    .param p1, "x1"    # F

    .prologue
    .line 512
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F

    div-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    .prologue
    .line 512
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->valueChanged()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;

    .prologue
    .line 512
    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->resetValue()V

    return-void
.end method

.method private resetValue()V
    .locals 1

    .prologue
    .line 526
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F

    .line 527
    return-void
.end method

.method private valueChanged()Z
    .locals 2

    .prologue
    .line 522
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretFoodParser$UnitHolder;->value:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
