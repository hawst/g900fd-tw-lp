.class final Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$3;
.super Ljava/lang/Object;
.source "ShareUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->createChartShareView(Landroid/app/Activity;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$mainView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$3;->val$mainView:Landroid/view/View;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$3;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 341
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$3;->val$mainView:Landroid/view/View;

    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil;->getScreenshot(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 342
    .local v1, "resultBitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ShareUtil$3;->val$activity:Landroid/app/Activity;

    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/ShareViaDialog;->showShareViaDialog(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    .end local v1    # "resultBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 343
    :catch_0
    move-exception v0

    .line 344
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method
