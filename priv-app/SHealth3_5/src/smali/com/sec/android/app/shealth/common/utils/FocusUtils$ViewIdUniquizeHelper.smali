.class public Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
.super Ljava/lang/Object;
.source "FocusUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/common/utils/FocusUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewIdUniquizeHelper"
.end annotation


# instance fields
.field currentMaxViewId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1090
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;->currentMaxViewId:I

    return-void
.end method


# virtual methods
.method public getCurrentMaxViewId()I
    .locals 1

    .prologue
    .line 1097
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;->currentMaxViewId:I

    return v0
.end method

.method public setCurrentMaxViewId(I)Z
    .locals 1
    .param p1, "currentMaxViewId"    # I

    .prologue
    .line 1106
    iget v0, p0, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;->currentMaxViewId:I

    if-ge p1, v0, :cond_0

    .line 1107
    const/4 v0, 0x0

    .line 1109
    :goto_0
    return v0

    .line 1108
    :cond_0
    iput p1, p0, Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;->currentMaxViewId:I

    .line 1109
    const/4 v0, 0x1

    goto :goto_0
.end method
