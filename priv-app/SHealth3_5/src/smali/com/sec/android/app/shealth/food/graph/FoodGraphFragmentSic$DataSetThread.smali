.class Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;
.super Ljava/lang/Thread;
.source "FoodGraphFragmentSic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataSetThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;->this$0:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$1;

    .prologue
    .line 541
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;-><init>(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;->this$0:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->HOURS_IN_DAY:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    # invokes: Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->prepareDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->access$300(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;->this$0:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->DAYS_IN_MONTH:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    # invokes: Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->prepareDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->access$300(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic$DataSetThread;->this$0:Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;

    sget-object v1, Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;->MONTHS_IN_YEAR:Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;

    # invokes: Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->prepareDataSet(Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->access$300(Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;Lcom/sec/android/app/shealth/framework/ui/graph/PeriodH;)V

    .line 550
    # invokes: Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->createGoalMap()V
    invoke-static {}, Lcom/sec/android/app/shealth/food/graph/FoodGraphFragmentSic;->access$200()V

    .line 551
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 552
    return-void
.end method
