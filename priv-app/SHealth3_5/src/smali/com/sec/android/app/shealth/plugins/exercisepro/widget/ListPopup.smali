.class public abstract Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
.super Landroid/widget/PopupWindow;
.source "ListPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private additionalItems:[Ljava/lang/String;

.field private additionalItemsPosition:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;

.field private alignment:I

.field private areTextPaddingsDefault:Z

.field private caller:Landroid/view/View;

.field protected context:Landroid/content/Context;

.field private descriptions:[Ljava/lang/String;

.field private hasContentDescription:Z

.field private isAdditionalTextVisible:Z

.field private items:[Ljava/lang/String;

.field private listView:Landroid/widget/ListView;

.field private listeners:[Landroid/view/View$OnClickListener;

.field private mainView:Landroid/view/View;

.field private onItemClickedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;

.field private selectedItemIndex:I

.field private textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

.field private textSelection:Ljava/lang/String;

.field private tittleText:Ljava/lang/String;

.field private uniquizeViewIdHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;ILcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .prologue
    .line 137
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;ILcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V

    .line 138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;ILcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    .param p6, "focusable"    # Z

    .prologue
    .line 142
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V

    .line 143
    if-eqz p4, :cond_0

    .line 144
    iput p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    .line 145
    invoke-virtual {p0, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setWidth(I)V

    .line 147
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;I[Ljava/lang/String;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "width"    # I
    .param p5, "additionalItems"    # [Ljava/lang/String;
    .param p6, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .prologue
    .line 151
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;ILcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V

    .line 152
    iput-object p5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->additionalItems:[Ljava/lang/String;

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->isAdditionalTextVisible:Z

    .line 154
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .prologue
    .line 121
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;-><init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Landroid/view/View;Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # [Ljava/lang/String;
    .param p3, "caller"    # Landroid/view/View;
    .param p4, "uniquizeViewIdHelper"    # Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    .param p5, "focusable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 126
    invoke-direct {p0}, Landroid/widget/PopupWindow;-><init>()V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->selectedItemIndex:I

    .line 50
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->hasContentDescription:Z

    .line 55
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;->NONE:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->additionalItemsPosition:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->areTextPaddingsDefault:Z

    .line 61
    const v0, 0x20001

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->alignment:I

    .line 127
    iput v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    .line 128
    iput-object p3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->caller:Landroid/view/View;

    .line 129
    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    .line 130
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    .line 131
    array-length v0, p2

    new-array v0, v0, [Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listeners:[Landroid/view/View$OnClickListener;

    .line 132
    iput-object p4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->uniquizeViewIdHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    .line 133
    invoke-direct {p0, p5}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->init(Z)V

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->onItemClickedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->hasContentDescription:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->descriptions:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->selectedItemIndex:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textSelection:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->uniquizeViewIdHelper:Lcom/sec/android/app/shealth/common/utils/FocusUtils$ViewIdUniquizeHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;Landroid/widget/TextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->applyPaddinsToTextView(Landroid/widget/TextView;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->isAdditionalTextVisible:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->tittleText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listeners:[Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->additionalItemsPosition:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$AdditionalItemsPosition;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->additionalItems:[Ljava/lang/String;

    return-object v0
.end method

.method private adjustPopupWidth()V
    .locals 4

    .prologue
    .line 392
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->areTextPaddingsDefault:Z

    if-nez v0, :cond_0

    .line 393
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    iget v1, v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;->paddingLeft:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a08e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;->paddingRight:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a08e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    .line 397
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setWidth(I)V

    .line 398
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->areTextPaddingsDefault:Z

    .line 400
    :cond_0
    return-void
.end method

.method private applyPaddinsToTextView(Landroid/widget/TextView;)V
    .locals 4
    .param p1, "text"    # Landroid/widget/TextView;

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    iget v0, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;->paddingLeft:I

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    iget v2, v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;->paddingRight:I

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 373
    return-void
.end method

.method private checkFocus()V
    .locals 2

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->caller:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 424
    :cond_0
    return-void
.end method

.method private init(Z)V
    .locals 12
    .param p1, "focusable"    # Z

    .prologue
    .line 204
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 205
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f0301cf

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->mainView:Landroid/view/View;

    .line 206
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->mainView:Landroid/view/View;

    const v10, 0x7f080801

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ListView;

    iput-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    .line 207
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getBackgroundResource()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setBackgroundResource(I)V

    .line 208
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getDividerDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 209
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getDividerHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 210
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    invoke-direct {v0, p0, v9, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;Landroid/content/Context;[Ljava/lang/String;)V

    .line 211
    .local v0, "adapter":Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$PopupListAdapter;
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setClickable(Z)V

    .line 212
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    invoke-virtual {v9, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 232
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    if-nez v9, :cond_2

    .line 233
    const/4 v6, 0x0

    .line 234
    .local v6, "maxWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getLayoutId()I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 235
    .local v8, "view":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getTextViewId()I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 236
    .local v7, "text":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v4, v1, v2

    .line 237
    .local v4, "item":Ljava/lang/String;
    invoke-virtual {v7, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/widget/LinearLayout;->measure(II)V

    .line 239
    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    .line 240
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    if-le v9, v6, :cond_0

    .line 241
    iget v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    .line 236
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 245
    .end local v4    # "item":Ljava/lang/String;
    :cond_1
    iput v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    .line 247
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "maxWidth":I
    .end local v7    # "text":Landroid/widget/TextView;
    .end local v8    # "view":Landroid/widget/LinearLayout;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->caller:Landroid/view/View;

    if-eqz v9, :cond_3

    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->caller:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v10

    if-ge v9, v10, :cond_3

    .line 248
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->caller:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    .line 251
    :cond_3
    iget v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->width:I

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setWidth(I)V

    .line 252
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->isHeightSetToWrapContent()Z

    move-result v9

    if-eqz v9, :cond_4

    const/4 v9, -0x2

    :goto_1
    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setHeight(I)V

    .line 254
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->mainView:Landroid/view/View;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setContentView(Landroid/view/View;)V

    .line 255
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/EventKeyUtils;->isExternalKeyBoardDisabled()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 256
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 257
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 262
    :goto_2
    new-instance v10, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const/4 v9, 0x0

    check-cast v9, Landroid/graphics/Bitmap;

    invoke-direct {v10, v11, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v10}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 263
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->initOnItemClickListeners()V

    .line 264
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->setFocusable(Z)V

    .line 265
    return-void

    .line 252
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getListPopupItemHeight()I

    move-result v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    array-length v10, v10

    mul-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getDividerHeight()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    array-length v11, v11

    mul-int/2addr v10, v11

    add-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getBackgroundEmptySpaceHeight()I

    move-result v10

    add-int/2addr v9, v10

    goto :goto_1

    .line 259
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 260
    iget-object v9, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    goto :goto_2
.end method

.method private initOnItemClickListeners()V
    .locals 4

    .prologue
    .line 285
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listeners:[Landroid/view/View$OnClickListener;

    new-instance v2, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$InternalOnItemClickedListener;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;ILcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$1;)V

    aput-object v2, v1, v0

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract getAdditionalUnitViewId()I
.end method

.method protected abstract getBackgroundEmptySpaceHeight()I
.end method

.method protected abstract getBackgroundResource()I
.end method

.method protected abstract getBelowTextViewId()I
.end method

.method protected abstract getBottomItemDrawableId()I
.end method

.method protected abstract getClickableViewId()I
.end method

.method protected abstract getDefaultItemColorId()I
.end method

.method protected abstract getDividerDrawable()Landroid/graphics/drawable/Drawable;
.end method

.method protected abstract getDividerHeight()I
.end method

.method protected abstract getGreyItemColorId()I
.end method

.method protected abstract getLayoutId()I
.end method

.method protected abstract getListPopupItemHeight()I
.end method

.method protected abstract getMiddleItemDrawableId()I
.end method

.method protected abstract getRecetTextViewId()I
.end method

.method protected abstract getSelectedItemColorId()I
.end method

.method protected abstract getTextViewId()I
.end method

.method protected abstract getTopItemDrawableId()I
.end method

.method protected isHeightSetToWrapContent()Z
    .locals 1

    .prologue
    .line 281
    const/4 v0, 0x1

    return v0
.end method

.method public setAlignment(I)V
    .locals 0
    .param p1, "align"    # I

    .prologue
    .line 448
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->alignment:I

    .line 449
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 0
    .param p1, "itemIndex"    # I

    .prologue
    .line 427
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->selectedItemIndex:I

    .line 428
    return-void
.end method

.method public setListViewHeight(I)V
    .locals 7
    .param p1, "bottomPixel"    # I

    .prologue
    .line 268
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 270
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v4

    .line 272
    .local v1, "heightPixels":F
    int-to-float v4, p1

    sub-float v2, v1, v4

    .line 273
    .local v2, "layoutHeight":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getListPopupItemHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    array-length v5, v5

    mul-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getDividerHeight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    array-length v6, v6

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getBackgroundEmptySpaceHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-lez v4, :cond_0

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getListPopupItemHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    array-length v5, v5

    mul-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getDividerHeight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    array-length v6, v6

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->getBackgroundEmptySpaceHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v2, v4, v5

    .line 276
    :cond_0
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    float-to-int v5, v2

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 277
    .local v3, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v3}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 278
    return-void
.end method

.method public setOnItemClickedListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;)V
    .locals 0
    .param p1, "onItemClickedListener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->onItemClickedListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$OnItemClickedListener;

    .line 182
    return-void
.end method

.method public setTextPaddings(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;)V
    .locals 1
    .param p1, "textPaddings"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->areTextPaddingsDefault:Z

    .line 105
    return-void
.end method

.method public setTextPaddings(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;Z)V
    .locals 0
    .param p1, "textPaddings"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;
    .param p2, "isDefaultPadding"    # Z

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->textPaddings:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup$TextPaddings;

    .line 109
    iput-boolean p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->areTextPaddingsDefault:Z

    .line 110
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 376
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->show(II)V

    .line 377
    return-void
.end method

.method public show(II)V
    .locals 1
    .param p1, "xOffset"    # I
    .param p2, "yOffset"    # I

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->items:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 381
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->adjustPopupWidth()V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->caller:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->caller:Landroid/view/View;

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->showAsDropDown(Landroid/view/View;II)V

    .line 384
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->checkFocus()V

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ListPopup;->dismiss()V

    goto :goto_0
.end method
