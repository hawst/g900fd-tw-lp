.class public Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;
.super Landroid/widget/FrameLayout;
.source "ImagePager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ImageCountCallback;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$DataHolder;,
        Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$OnDeleteButtonListener;
    }
.end annotation


# static fields
.field public static final IMAGES:Ljava/lang/String; = "exercise_images"

.field public static final IMAGES_DELETE_BUTTON_VISIBILITY:Ljava/lang/String; = "images_delete_button_visibility"

.field public static final IS_DELETE_BUTTON_FORCE_GONE:Ljava/lang/String; = "is_delete_button_force_gone"

.field public static final SELECTED_IMAGE:Ljava/lang/String; = "selected_image"


# instance fields
.field private adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

.field private cashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private deleteButton:Landroid/view/View;

.field private deleteButtonVisibility:I

.field private imageCountCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ImageCountCallback;

.field private isDelBtnForceGone:Z

.field private mDeleteListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$OnDeleteButtonListener;

.field private mHeight:I

.field private mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mSynObject:Ljava/lang/Object;

.field private mWidth:I

.field private pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

.field private scrollCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;

.field updateHandler:Landroid/os/Handler;

.field private viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->isDelBtnForceGone:Z

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->cashMap:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mSynObject:Ljava/lang/Object;

    .line 261
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->updateHandler:Landroid/os/Handler;

    .line 58
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->init()V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->isDelBtnForceGone:Z

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->cashMap:Ljava/util/HashMap;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mSynObject:Ljava/lang/Object;

    .line 261
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$4;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->updateHandler:Landroid/os/Handler;

    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->init()V

    .line 65
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->cashMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->checkVisibility()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->notifyImagesCountChanged()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->isDelBtnForceGone:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->scrollCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$OnDeleteButtonListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mDeleteListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$OnDeleteButtonListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mWidth:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mHeight:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mSynObject:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    return-object v0
.end method

.method private checkVisibility()V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->setVisibility(I)V

    .line 311
    return-void

    .line 310
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0955

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mWidth:I

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0956

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mHeight:I

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030194

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 96
    const v0, 0x7f0803ce

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setFocusable(Z)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setFocusableInTouchMode(Z)V

    .line 99
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setSwipingEnabled(Z)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->cashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$1;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 142
    const v0, 0x7f0803c1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButton:Landroid/view/View;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButton:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$2;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    const v0, 0x7f0803cf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    iput-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    .line 153
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->checkVisibility()V

    .line 154
    return-void
.end method

.method private notifyImagesCountChanged()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->imageCountCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ImageCountCallback;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->imageCountCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ImageCountCallback;

    invoke-interface {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ImageCountCallback;->onImageCountChanged()V

    .line 235
    :cond_0
    return-void
.end method

.method private setPhotoBitmap(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 238
    .local p1, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$3;-><init>(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 250
    return-void
.end method


# virtual methods
.method public addImage(Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;)V
    .locals 3
    .param p1, "image"    # Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    .prologue
    .line 200
    if-nez p1, :cond_0

    .line 209
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->addPage()V

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->notifyDataSetChanged()V

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 206
    .local v0, "lastItem":I
    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setCurrentItem(IZ)V

    .line 207
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->checkVisibility()V

    .line 208
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->notifyImagesCountChanged()V

    goto :goto_0
.end method

.method public addImage(Ljava/lang/String;)V
    .locals 4
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 212
    if-nez p1, :cond_0

    .line 223
    :goto_0
    return-void

    .line 214
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;-><init>()V

    .line 215
    .local v1, "photodata":Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    invoke-virtual {v1, p1}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->setFilePath(Ljava/lang/String;)V

    .line 216
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->addPage()V

    .line 217
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->notifyDataSetChanged()V

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .line 220
    .local v0, "lastItem":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setCurrentItem(IZ)V

    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->checkVisibility()V

    .line 222
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->notifyImagesCountChanged()V

    goto :goto_0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->removeAllViews()V

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$502(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;Ljava/util/List;)Ljava/util/List;

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->notifyDataSetChanged()V

    .line 229
    return-void
.end method

.method public getCurrentImagePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getImagesCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 189
    const/4 v0, 0x0

    .line 191
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getCurrentItemIndex()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;->getFilePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentItem()Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getCurrentItemIndex()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;

    return-object v0
.end method

.method public getCurrentItemIndex()I
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getImagesCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 174
    const/4 v0, 0x0

    .line 176
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->getCurrentItem()I

    move-result v0

    goto :goto_0
.end method

.method public getExercisePhotos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getImagesCount()I
    .locals 1

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->getExercisePhotos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public hasImages()Z
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$500(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->notifyDataSetChanged()V

    .line 76
    const-string/jumbo v2, "selected_image"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 77
    .local v1, "selected":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->getCount()I

    move-result v0

    .line 78
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 79
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->fill(II)V

    .line 81
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    invoke-virtual {v2, v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setCurrentItem(IZ)V

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButton:Landroid/view/View;

    const-string v3, "images_delete_button_visibility"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 83
    const-string v2, "is_delete_button_force_gone"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->isDelBtnForceGone:Z

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->checkVisibility()V

    .line 85
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 68
    const-string/jumbo v0, "selected_image"

    iget-object v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    const-string v0, "images_delete_button_visibility"

    iget v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButtonVisibility:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    const-string v0, "is_delete_button_force_gone"

    iget-boolean v1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->isDelBtnForceGone:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 71
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->viewPager:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ViewPager;->setCurrentItem(IZ)V

    .line 181
    return-void
.end method

.method public setDeleteButtonVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 165
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButtonVisibility:I

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->deleteButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 167
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->isDelBtnForceGone:Z

    .line 170
    :cond_0
    return-void
.end method

.method public setExercisePhotoList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "photodatas":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/plugins/common/data/ExercisePhotoData;>;"
    if-nez p1, :cond_0

    .line 307
    :goto_0
    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->cashMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 299
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->fill(II)V

    .line 301
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->setPhotoBitmap(Ljava/util/List;)V

    goto :goto_0

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->photoDatas:Ljava/util/List;
    invoke-static {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->access$502(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;Ljava/util/List;)Ljava/util/List;

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->adapter:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ExercisePhotoAdapter;->notifyDataSetChanged()V

    .line 305
    invoke-direct {p0}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->checkVisibility()V

    goto :goto_0
.end method

.method public setImageCountCallback(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ImageCountCallback;)V
    .locals 0
    .param p1, "imageCountCallback"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ImageCountCallback;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->imageCountCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ImageCountCallback;

    .line 319
    return-void
.end method

.method public setOnDeleteButtonListener(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$OnDeleteButtonListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$OnDeleteButtonListener;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mDeleteListener:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$OnDeleteButtonListener;

    .line 49
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 53
    return-void
.end method

.method public setPageSelectorVisibility(I)V
    .locals 1
    .param p1, "visiblity"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->pageSelector:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/PageSelector;->setVisibility(I)V

    .line 158
    return-void
.end method

.method public setScrollCallback(Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;)V
    .locals 0
    .param p1, "scrollCallback"    # Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;

    .prologue
    .line 314
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->scrollCallback:Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager$ScrollCallback;

    .line 315
    return-void
.end method

.method public setSize(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mWidth:I

    .line 89
    iput p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/widget/ImagePager;->mHeight:I

    .line 90
    return-void
.end method
