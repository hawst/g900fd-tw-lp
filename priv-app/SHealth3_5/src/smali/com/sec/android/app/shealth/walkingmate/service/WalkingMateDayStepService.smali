.class public Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
.super Landroid/app/Service;
.source "WalkingMateDayStepService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;,
        Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$SyncWearableRunner;,
        Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$BluetoothBroadcastReceiver;
    }
.end annotation


# static fields
.field public static final ACTION_DELETE_TOTAL_STEP:Ljava/lang/String; = "com.sec.android.app.shealth.command.deletetotalstep"

.field public static final ACTION_GOAL_REFRESH:Ljava/lang/String; = "com.sec.android.app.shealth.command.pedometer.goal.refresh"

.field public static final ACTION_HOME_RESUME:Ljava/lang/String; = "com.sec.android.intent.action.HOME_RESUME"

.field public static final ACTION_RESET_TOTAL_STEP:Ljava/lang/String; = "com.sec.android.app.shealth.command.resettotalstep"

.field public static final ACTION_RESTORE_END:Ljava/lang/String; = "com.sec.android.app.shealth.RESTORE_END"

.field public static final ACTION_RESTORE_START:Ljava/lang/String; = "com.sec.android.app.shealth.RESTORE_START"

.field public static final ACTION_VIEW_STEP_COUNT:Ljava/lang/String; = "com.sec.android.app.shealth.pedometer.viewstepcount"

.field private static AUTO_STARTED:Z = false

.field private static final COCKTAIL_WIDGET_INITIALIZE:Ljava/lang/String; = "com.sec.android.app.shealth.walkingmate.cocktailwidget.INITIALIZE"

.field private static final ERROR_NONE:I = -0x1

.field private static final NOTIFICATION_ID:I = 0x42c

.field private static PEDOMETER_START:Z = false

.field private static final TAG:Ljava/lang/String; = "WalkingMateDayStepService"

.field private static dontAllowLockWidget:Z

.field private static isUpdateWidget:Z

.field private static lock:Ljava/lang/Object;

.field private static mCocktailWidgetIntentFilter:Landroid/content/IntentFilter;

.field public static mCurrentDevice:I

.field private static mHealthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

.field private static mInactiveTime:J

.field private static mIsKeyManagerSetAdaptiveEvent:Z

.field private static mIsRun:Z

.field private static mKeyguardManager:Landroid/app/KeyguardManager;

.field private static mSyncfailed:Z

.field private static mSyncing:Z

.field private static mUpdateCurrentStatus:Landroid/content/ServiceConnection;

.field private static mWalkingMode:Ljava/lang/String;

.field private static pedoNotiType:I

.field private static sIntentFilter:Landroid/content/IntentFilter;


# instance fields
.field private final NOTI_CASE_BEST_RECORD:I

.field private final NOTI_CASE_GOAL_ACHIEVED:I

.field private final NOTI_CASE_HALF_ACHIEVED:I

.field private final NOTI_CASE_INACTIVE:I

.field public final NOTI_CASE_NOT_USED:I

.field private currentEndofDate:J

.field private final mBinder:Landroid/os/IBinder;

.field private mBluetoothBroadcastReceiver:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$BluetoothBroadcastReceiver;

.field private mBluetoothHandler:Landroid/os/Handler;

.field private final mCocktailWidgetRequestReceiver:Landroid/content/BroadcastReceiver;

.field private mDistanceUnit:Ljava/lang/String;

.field public mHandler:Landroid/os/Handler;

.field private volatile mIsLoadLocked:Z

.field private mLoadLock:Ljava/lang/Object;

.field private mNotification:Landroid/app/Notification;

.field private mSyncTimeoutRunnable:Ljava/lang/Runnable;

.field private final mSystemMonitor:Landroid/content/BroadcastReceiver;

.field public mToastRunnable:Ljava/lang/Runnable;

.field private mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

.field private mUpdatePlainWidgetRunnable:Ljava/lang/Runnable;

.field private mUpdateRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 111
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsRun:Z

    .line 112
    sput-boolean v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->AUTO_STARTED:Z

    .line 113
    sput-boolean v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->PEDOMETER_START:Z

    .line 115
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsKeyManagerSetAdaptiveEvent:Z

    .line 119
    const/4 v1, -0x1

    sput v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->pedoNotiType:I

    .line 121
    const-wide/16 v4, 0x0

    sput-wide v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mInactiveTime:J

    .line 136
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isUpdateWidget:Z

    .line 141
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    sput-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCocktailWidgetIntentFilter:Landroid/content/IntentFilter;

    .line 142
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCocktailWidgetIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.app.shealth.walkingmate.cocktailwidget.INITIALIZE"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 143
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCocktailWidgetIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.shealth.UPDATE_PROFILE"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    const-string/jumbo v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "productName":Ljava/lang/String;
    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move v1, v3

    :goto_0
    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->dontAllowLockWidget:Z

    .line 147
    sget-boolean v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->dontAllowLockWidget:Z

    if-eqz v1, :cond_1

    .line 149
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-gt v1, v4, :cond_3

    move v1, v3

    :goto_1
    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->dontAllowLockWidget:Z

    .line 157
    :cond_1
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    sput-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    .line 158
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 162
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.app.shealth.command.resettotalstep"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 163
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.app.shealth.command.deletetotalstep"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 164
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.app.shealth.command.pedometer.goal.refresh"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 165
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 166
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.app.shealth.pedometer.viewstepcount"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 167
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.app.shealth.RESTORE_START"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 168
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.app.shealth.RESTORE_END"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 169
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.TIME_TICK"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 170
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.WEARABLE_DEVICE_SYNC"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 171
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.sdk.health.sensor.action.DATA_UPDATED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 250
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$2;

    invoke-direct {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$2;-><init>()V

    sput-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdateCurrentStatus:Landroid/content/ServiceConnection;

    .line 696
    const-string/jumbo v1, "normal"

    sput-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mWalkingMode:Ljava/lang/String;

    .line 698
    sput v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    .line 699
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z

    .line 700
    sput-boolean v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncfailed:Z

    .line 702
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->lock:Ljava/lang/Object;

    return-void

    :cond_2
    move v1, v2

    .line 146
    goto/16 :goto_0

    :cond_3
    move v1, v2

    .line 149
    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mNotification:Landroid/app/Notification;

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->NOTI_CASE_NOT_USED:I

    .line 106
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->NOTI_CASE_HALF_ACHIEVED:I

    .line 107
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->NOTI_CASE_GOAL_ACHIEVED:I

    .line 108
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->NOTI_CASE_BEST_RECORD:I

    .line 109
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->NOTI_CASE_INACTIVE:I

    .line 178
    iput-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mBluetoothBroadcastReceiver:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$BluetoothBroadcastReceiver;

    .line 179
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mBluetoothHandler:Landroid/os/Handler;

    .line 685
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$WalkingMateDayStepBinder;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mBinder:Landroid/os/IBinder;

    .line 697
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J

    .line 704
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mLoadLock:Ljava/lang/Object;

    .line 1151
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$7;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSystemMonitor:Landroid/content/BroadcastReceiver;

    .line 1433
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$10;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCocktailWidgetRequestReceiver:Landroid/content/BroadcastReceiver;

    .line 1664
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    .line 1666
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$11;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mToastRunnable:Ljava/lang/Runnable;

    .line 1676
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$12;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdateRunnable:Ljava/lang/Runnable;

    .line 1683
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$13;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdatePlainWidgetRunnable:Ljava/lang/Runnable;

    .line 1690
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$14;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public static StartAfterProfileSetUp()V
    .locals 3

    .prologue
    .line 1758
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "call service start"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1760
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1761
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1762
    .local v0, "iService":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1766
    .end local v0    # "iService":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1764
    :cond_0
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "DayStep service already started"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateHomeWidget()V

    return-void
.end method

.method static synthetic access$1002(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 101
    sput-wide p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mInactiveTime:J

    return-wide p0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .param p1, "x1"    # I

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setNotification(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->dateChanged()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdateRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithNotification()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J

    return-wide v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .param p1, "x1"    # J

    .prologue
    .line 101
    iput-wide p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncTimeoutRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1700()Z
    .locals 1

    .prologue
    .line 101
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z

    return v0
.end method

.method static synthetic access$1702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 101
    sput-boolean p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z

    return p0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData()V

    return-void
.end method

.method static synthetic access$1900(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateLockScreenWidget(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 101
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sendCurrentStatusToCockTailWidget(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$2000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->lock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateWidget()V

    return-void
.end method

.method static synthetic access$2202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 101
    sput-boolean p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncfailed:Z

    return p0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdatePlainWidgetRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->initialize()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mBluetoothHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400()Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdateCurrentStatus:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->prepareWidgetInCaseOfPasswordLocked()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;
    .param p1, "x1"    # Z

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->prepareWidgetInCaseOfNoProfile(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->initializeService()I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithoutNotification()V

    return-void
.end method

.method private callLoadLatestData()V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData(Z)V

    .line 202
    return-void
.end method

.method private callLoadLatestData(Z)V
    .locals 4
    .param p1, "isStepCounting"    # Z

    .prologue
    .line 207
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getLastUpdateType()I

    move-result v0

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 209
    const-string v0, "OPTIMIZE-DAYSTEP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device type is changed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setLastUpdateTime(J)V

    .line 215
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getLastUpdateTime()J

    move-result-wide v0

    const-wide/16 v2, 0x10

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 216
    const-string v0, "WalkingMateDayStepService"

    const-string/jumbo v1, "skip...."

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setLastUpdateTime(J)V

    .line 223
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;->run()V

    goto :goto_0
.end method

.method public static cancelNotification()V
    .locals 3

    .prologue
    .line 835
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 836
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    const/16 v1, 0x42c

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 837
    return-void
.end method

.method private dateChanged()V
    .locals 6

    .prologue
    .line 1110
    iget-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 1127
    :goto_0
    return-void

    .line 1114
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1115
    .local v0, "inTime":J
    const-string v2, "WalkingMateDayStepService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "currentEndofDate = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J

    .line 1117
    const-string v2, "WalkingMateDayStepService"

    const-string/jumbo v3, "notUsedNotify change false 4"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1120
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V

    .line 1121
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithNotification()V

    .line 1125
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1126
    const-string v2, "WalkingMateDayStepService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DATE_CHANGED duration = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1123
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->resetWithNotification()V

    goto :goto_1
.end method

.method private getNotificationIntent(I)Landroid/app/PendingIntent;
    .locals 7
    .param p1, "noti_kind"    # I

    .prologue
    const/16 v6, 0x1ca8

    .line 1471
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1472
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1473
    .local v0, "context":Landroid/content/Context;
    const-string/jumbo v3, "widgetActivityAction"

    const-string v4, "com.sec.android.app.shealth.pedometer.Launch"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1474
    const-string/jumbo v3, "widgetActivityPackage"

    const-string v4, "com.sec.android.app.shealth"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1475
    if-eqz p1, :cond_0

    .line 1476
    const-string v3, "SelectedDate"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1478
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->isHomeActivityRunning(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1479
    const-string v3, "WalkingMateDayStepService"

    const-string v4, "getNotificationIntent"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    const-class v3, Lcom/sec/android/app/shealth/receiver/LaunchWidgetReceiver;

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1481
    const-string v3, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1482
    const/high16 v3, 0x8000000

    invoke-static {v0, v6, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1486
    :goto_0
    return-object v2

    :cond_1
    invoke-static {v0, v1, v6}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->launchExistingHomeActivity(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    goto :goto_0
.end method

.method private static getSoundUri(I)Landroid/net/Uri;
    .locals 5
    .param p0, "id"    # I

    .prologue
    .line 1701
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1702
    .local v0, "localContext":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1703
    .local v1, "resources":Landroid/content/res/Resources;
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    .line 1704
    .local v2, "uriBuilder":Landroid/net/Uri$Builder;
    const-string v3, "android.resource"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1705
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1706
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1707
    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1708
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    return-object v3
.end method

.method private static getWidgetsCount()I
    .locals 5

    .prologue
    .line 1018
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 1019
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/widget/WalkMateAppWidget;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    array-length v1, v2

    .line 1020
    .local v1, "length":I
    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/widget/WalkMatePlainAppWidget;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    array-length v2, v2

    add-int/2addr v1, v2

    .line 1021
    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/widget/homescreenwidget/WalkMateAppWidget;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    array-length v2, v2

    add-int/2addr v1, v2

    .line 1022
    new-instance v2, Landroid/content/ComponentName;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    array-length v2, v2

    add-int/2addr v1, v2

    .line 1023
    return v1
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 708
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v0

    sput v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    .line 709
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshBestRecordInfo()V

    .line 710
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshFirstDate()V

    .line 711
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData()V

    .line 712
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->currentEndofDate:J

    .line 713
    return-void
.end method

.method private initializeService()I
    .locals 9

    .prologue
    const/4 v5, -0x1

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 450
    sget-boolean v4, Lcom/sec/android/app/shealth/SHealthApplication;->unSupported:Z

    if-eqz v4, :cond_0

    .line 451
    const-string v4, "WalkingMateDayStepService"

    const-string v6, "SHealthApplication.unSupported"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    .line 575
    :goto_0
    return v4

    .line 463
    :cond_0
    sget-boolean v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsRun:Z

    if-eqz v4, :cond_1

    .line 464
    const-string v4, "WalkingMateDayStepService"

    const-string/jumbo v5, "service is already running!!"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v6

    .line 465
    goto :goto_0

    .line 468
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->isInitializationNeeded(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 469
    const-string v4, "WalkingMateDayStepService"

    const-string/jumbo v6, "walking mate cannot start"

    invoke-static {v4, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-direct {p0, v8}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->prepareWidgetInCaseOfNoProfile(Z)V

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopSelf()V

    move v4, v5

    .line 472
    goto :goto_0

    .line 474
    :cond_2
    new-instance v4, Ljava/lang/Thread;

    new-instance v7, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$5;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$5;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    invoke-direct {v4, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 481
    sget-object v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v4, :cond_3

    .line 482
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v7, "keyguard"

    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/KeyguardManager;

    sput-object v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 486
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateLockScreenWidget(Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 490
    :goto_1
    const-string v4, "WalkingMateDayStepService"

    const-string v7, "Start Service"

    invoke-static {v4, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isMobilePedometerDisabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 497
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopSelf()V

    move v4, v5

    .line 498
    goto :goto_0

    .line 487
    :catch_0
    move-exception v0

    .line 488
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 507
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v3

    .line 508
    .local v3, "viewStepCount":I
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->allowDeviceConnectionSwitch(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->hasWearableManager(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 509
    const/16 v4, 0x2719

    if-eq v3, v4, :cond_5

    .line 510
    const-string v4, "WalkingMateDayStepService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[FIRSTCONNECTION]initializeService - viewStepCount:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->saveViewStepCount(I)V

    .line 512
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v4

    sput v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    .line 516
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSystemMonitor:Landroid/content/BroadcastReceiver;

    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 517
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCocktailWidgetRequestReceiver:Landroid/content/BroadcastReceiver;

    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCocktailWidgetIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 519
    const-string v4, "WalkingMateDayStepService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "STEP_AUTO_STARTED="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v7, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->AUTO_STARTED:Z

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    sget-boolean v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->AUTO_STARTED:Z

    if-eqz v4, :cond_6

    sget-boolean v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->PEDOMETER_START:Z

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isWalkingStarted()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 522
    const-string v4, "WalkingMateDayStepService"

    const-string v5, "[**] Pedo Auto Start"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setStartWalking(Z)V

    .line 524
    invoke-static {v8}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setNotificationONOFF(Z)V

    .line 527
    :cond_6
    sput-boolean v6, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->AUTO_STARTED:Z

    .line 528
    sput-boolean v6, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->PEDOMETER_START:Z

    .line 530
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    if-nez v4, :cond_7

    .line 531
    new-instance v4, Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    .line 534
    :cond_7
    const-string v4, "PEDOSTART"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mIsRun ="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v7, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsRun:Z

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->initialize()V

    .line 538
    sput-boolean v8, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsRun:Z

    .line 540
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isFirstStart()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 541
    const-string v4, "PEDOSTART"

    const-string v5, "initial processing after profile setting"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V

    .line 543
    invoke-static {v6}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->setStartWalking(Z)V

    .line 544
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->StartProcessAfterServiceStart()V

    .line 550
    :cond_8
    :goto_2
    const-string v4, "PEDOSTART"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mIsRun ="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v7, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsRun:Z

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.RESTART_WIDGET"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 553
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sendBroadcast(Landroid/content/Intent;)V

    .line 555
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUnitSettingHelper:Lcom/sec/android/app/shealth/common/utils/UnitHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/UnitHelper;->getDistanceUnit()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mDistanceUnit:Ljava/lang/String;

    .line 556
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mDistanceUnit:Ljava/lang/String;

    if-nez v4, :cond_9

    .line 557
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900c7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mDistanceUnit:Ljava/lang/String;

    .line 560
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->checkNotiStatus()V

    .line 563
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->startClearCoverView(Landroid/content/Context;)V

    .line 564
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->refreshGearGoal(Landroid/content/Context;)V

    .line 565
    new-instance v4, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$BluetoothBroadcastReceiver;

    invoke-direct {v4, p0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$BluetoothBroadcastReceiver;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mBluetoothBroadcastReceiver:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$BluetoothBroadcastReceiver;

    .line 566
    new-instance v2, Landroid/content/IntentFilter;

    const-string v4, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 567
    .local v2, "lFilter":Landroid/content/IntentFilter;
    const-string v4, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 568
    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mBluetoothBroadcastReceiver:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$BluetoothBroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 570
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->walkingMateServiceStart()V

    .line 571
    const-string v4, "PEDOSTART"

    const-string/jumbo v5, "service onCreate Finished"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$SyncWearableRunner;

    const/4 v7, 0x0

    invoke-direct {v5, v7}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$SyncWearableRunner;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$1;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    move v4, v6

    .line 575
    goto/16 :goto_0

    .line 545
    .end local v1    # "i":Landroid/content/Intent;
    .end local v2    # "lFilter":Landroid/content/IntentFilter;
    :cond_a
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 546
    const-string v4, "PEDOSTART"

    const-string v5, "Starting device pedometer."

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStart()V

    goto/16 :goto_2
.end method

.method private isAvalableInactiveTime()Z
    .locals 20

    .prologue
    .line 724
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeFrom()Ljava/lang/String;

    move-result-object v15

    .line 725
    .local v15, "timeFrom":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeTo()Ljava/lang/String;

    move-result-object v16

    .line 727
    .local v16, "timeTo":Ljava/lang/String;
    const-string v10, ":"

    .line 729
    .local v10, "sep":Ljava/lang/String;
    invoke-virtual {v15, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 730
    .local v11, "startHour":I
    invoke-virtual {v15, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x1

    aget-object v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 731
    .local v12, "startMin":I
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 732
    .local v6, "endHour":I
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x1

    aget-object v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 734
    .local v7, "endMin":I
    const-wide/16 v13, 0x0

    .line 735
    .local v13, "startTime":J
    const-wide/16 v8, 0x0

    .line 737
    .local v8, "endTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 739
    .local v4, "currentTime":J
    const-string v17, "checkInactiveTime"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "timeFrom = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    const-string v17, "checkInactiveTime"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "timeTo = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 744
    .local v2, "cal":Ljava/util/Calendar;
    const/16 v17, 0xb

    move/from16 v0, v17

    invoke-virtual {v2, v0, v11}, Ljava/util/Calendar;->set(II)V

    .line 745
    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v2, v0, v12}, Ljava/util/Calendar;->set(II)V

    .line 746
    const/16 v17, 0xd

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 748
    const-string v17, "UTC"

    invoke-static/range {v17 .. v17}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v17

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v3

    .line 749
    .local v3, "converter":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 750
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    .line 752
    const-string v17, "checkInactiveTime"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "UTC start time = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Ljava/util/Calendar;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 755
    const/16 v17, 0xb

    move/from16 v0, v17

    invoke-virtual {v2, v0, v6}, Ljava/util/Calendar;->set(II)V

    .line 756
    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v2, v0, v7}, Ljava/util/Calendar;->set(II)V

    .line 757
    const/16 v17, 0xd

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 759
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    move-wide/from16 v0, v17

    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 760
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 762
    cmp-long v17, v4, v13

    if-ltz v17, :cond_0

    cmp-long v17, v4, v8

    if-gtz v17, :cond_0

    .line 763
    const/16 v17, 0x1

    .line 765
    :goto_0
    return v17

    :cond_0
    const/16 v17, 0x0

    goto :goto_0
.end method

.method private static isLockScreenPedometerChecked()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1626
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "lock_additional_steps"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 1630
    .local v1, "value":I
    if-nez v1, :cond_0

    .line 1631
    const-string v3, "WalkingMateDayStepService"

    const-string v4, "Lock screen pedometer is unchecked"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1638
    :goto_0
    return v2

    .line 1634
    :catch_0
    move-exception v0

    .line 1635
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    move v2, v3

    .line 1638
    goto :goto_0
.end method

.method public static isRun()Z
    .locals 1

    .prologue
    .line 175
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsRun:Z

    return v0
.end method

.method private static isSecureStorageSupported()Z
    .locals 2

    .prologue
    .line 777
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isSecureStorageSupported(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static isSensorHubSupported()Z
    .locals 5

    .prologue
    .line 587
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 588
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    const-string v2, "com.sec.feature.sensorhub"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.sec.feature.scontext_lite"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 590
    .local v0, "isSupported":Z
    :goto_0
    const-string v2, "WalkingMateDayStepService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sensor hub supported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    return v0

    .line 588
    .end local v0    # "isSupported":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSyncing()Z
    .locals 3

    .prologue
    .line 1745
    const-string v0, "WalkingMateDayStepService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSyncStatus() mSyncing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1746
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z

    return v0
.end method

.method private static launchExistingHomeActivity(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "launcherintent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    const/4 v3, 0x1

    .line 1459
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "launchExistingHomeActivity - getPendingForProcessing"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1460
    const-string v1, "com.sec.shealth.HomeActivity"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1461
    const-string v1, "com.sec.android.app.shealth"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1462
    const-string v1, "launchWidget"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1463
    const-string v1, "exercisenotify"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1464
    const/high16 v1, 0x10010000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1465
    const/high16 v1, 0x8000000

    invoke-static {p0, p2, p1, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1467
    .local v0, "pendingIntent":Landroid/app/PendingIntent;
    return-object v0
.end method

.method private prepareWidgetInCaseOfNoProfile(Z)V
    .locals 6
    .param p1, "apply"    # Z

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 273
    if-nez p1, :cond_1

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030281

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 277
    .local v0, "clearCover":Landroid/widget/RemoteViews;
    const v2, 0x7f080525

    invoke-virtual {v0, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 278
    const v2, 0x7f080b5a

    invoke-virtual {v0, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 279
    const v2, 0x7f080b5b

    invoke-virtual {v0, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 280
    const v2, 0x7f080b5c

    invoke-virtual {v0, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 281
    const v2, 0x7f080b5d

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 282
    invoke-static {p0, v0, v5}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->updateClearCoverAppWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 283
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v2, :cond_2

    .line 284
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    sput-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 287
    :cond_2
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v2, :cond_0

    .line 289
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->removeLockScreenWidget()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 290
    :catch_0
    move-exception v1

    .line 291
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "WalkingMateDayStepService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private prepareWidgetInCaseOfPasswordLocked()V
    .locals 8

    .prologue
    const v7, 0x7f080bc8

    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 422
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateWidget()V

    .line 423
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030281

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 424
    .local v0, "clearCover":Landroid/widget/RemoteViews;
    const v2, 0x7f080525

    invoke-virtual {v0, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 425
    const v2, 0x7f080b5a

    invoke-virtual {v0, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 426
    const v2, 0x7f080b5b

    invoke-virtual {v0, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 427
    const v2, 0x7f080b5c

    invoke-virtual {v0, v2, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 428
    const v2, 0x7f080b5d

    const-string v3, "--"

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 429
    const/4 v2, 0x1

    invoke-static {p0, v0, v2}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->updateClearCoverAppWidget(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 431
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v2, :cond_0

    .line 432
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    sput-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 434
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v2, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_1

    .line 435
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f03029c

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 436
    .local v1, "lockView":Landroid/widget/RemoteViews;
    const v2, 0x7f080bc1

    invoke-virtual {v1, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 437
    const v2, 0x7f080bc2

    invoke-virtual {v1, v2, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 438
    const v2, 0x7f080bc7

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 439
    const v2, 0x7f080bc4

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 440
    const v2, 0x7f080bc5

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 441
    const v2, 0x7f080bc6

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 442
    const/4 v2, -0x1

    invoke-virtual {v1, v7, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 443
    const-string v2, "--"

    invoke-virtual {v1, v7, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 444
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    const-string v3, "com.sec.android.app.shealth.walkingmate.service.WalkingMateDayStepService"

    invoke-virtual {v2, v3, v1, v1}, Landroid/app/KeyguardManager;->setAdaptiveEvent(Ljava/lang/String;Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    .line 446
    .end local v1    # "lockView":Landroid/widget/RemoteViews;
    :cond_1
    return-void
.end method

.method private static declared-synchronized removeLockScreenWidget()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1642
    const-class v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    monitor-enter v2

    :try_start_0
    const-string v1, "WalkingMateDayStepService"

    const-string/jumbo v3, "removeAdativeEvent = com.sec.android.app.shealth.walkingmate.service.WalkingMateDayStepService"

    invoke-static {v1, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1643
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v3, 0x12

    if-lt v1, v3, :cond_0

    .line 1645
    :try_start_1
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v1, :cond_0

    .line 1646
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    const-string v3, "com.sec.android.app.shealth.walkingmate.service.WalkingMateDayStepService"

    invoke-virtual {v1, v3}, Landroid/app/KeyguardManager;->removeAdaptiveEvent(Ljava/lang/String;)V

    .line 1648
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsKeyManagerSetAdaptiveEvent:Z
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1654
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 1650
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :catch_0
    move-exception v0

    .line 1651
    .restart local v0    # "e":Ljava/lang/NoSuchMethodError;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1642
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private resetWithNotification()V
    .locals 2

    .prologue
    .line 1141
    const-string v0, "WalkingMateDayStepService"

    const-string/jumbo v1, "refresh with noti"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1142
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshBestRecordInfo()V

    .line 1143
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshFirstDate()V

    .line 1144
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->resetNotificationStatus()V

    .line 1145
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 1146
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData()V

    .line 1147
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 1148
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->checkNotiStatus()V

    .line 1149
    return-void
.end method

.method private resetWithoutNotification()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1130
    const-string v0, "WalkingMateDayStepService"

    const-string/jumbo v1, "refresh without noti"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1131
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshBestRecordInfo()V

    .line 1132
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->refreshFirstDate()V

    .line 1133
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 1134
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->dateChanged()V

    .line 1135
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->checkNotiStatus()V

    .line 1136
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->callLoadLatestData(Z)V

    .line 1137
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setRefreshingNotiState(Z)V

    .line 1138
    return-void
.end method

.method private static sendCurrentStatusToCockTailWidget(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1447
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.cocktailbar"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 1448
    .local v0, "isSupported":Z
    if-eqz v0, :cond_0

    .line 1449
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/cocktailwidget/WalkingMateCocktailWidgetReceiver;->updateCocktailWidget(Landroid/content/Context;)V

    .line 1451
    :cond_0
    return-void
.end method

.method public static sendNotification()V
    .locals 21

    .prologue
    .line 781
    const-string v18, "WalkingMateDayStepService"

    const-string/jumbo v19, "sendNotification() is called"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSecureStorageSupported()Z

    move-result v18

    if-nez v18, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSensorHubSupported()Z

    move-result v18

    if-nez v18, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/walkingmate/utils/PedometerUtils;->isDeviceConnected(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 784
    :cond_0
    const-string v18, "WalkingMateDayStepService"

    const-string/jumbo v19, "returning as secure storage supported or (sensor hub not supported and no devices connected)."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    .local v8, "notiManager":Landroid/app/NotificationManager;
    :goto_0
    return-void

    .line 788
    .end local v8    # "notiManager":Landroid/app/NotificationManager;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    const-string/jumbo v19, "notification"

    invoke-virtual/range {v18 .. v19}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/NotificationManager;

    .line 789
    .restart local v8    # "notiManager":Landroid/app/NotificationManager;
    if-nez v8, :cond_2

    .line 790
    const-string v18, "WalkingMateDayStepService"

    const-string v19, "NotificationManager is null"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 793
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 794
    .local v12, "resources":Landroid/content/res/Resources;
    const v18, 0x7f06002b

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSoundUri(I)Landroid/net/Uri;

    move-result-object v14

    .line 795
    .local v14, "soundUri":Landroid/net/Uri;
    const v11, 0x7f02060f

    .line 796
    .local v11, "resId":I
    const v13, 0x7f020560

    .line 797
    .local v13, "smallIcon":I
    invoke-static {v12, v11}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 798
    .local v7, "largeIcon":Landroid/graphics/Bitmap;
    const v18, 0x7f090260

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 799
    .local v16, "ticker":Ljava/lang/String;
    const v18, 0x7f090260

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 800
    .local v17, "title":Ljava/lang/String;
    const-string v15, ""

    .line 801
    .local v15, "subText":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isSensorHubSupported()Z

    move-result v18

    if-nez v18, :cond_3

    .line 802
    const v18, 0x7f090262

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 806
    :goto_1
    new-instance v6, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    const-class v19, Lcom/sec/android/app/shealth/walkingmate/receiver/WalkingMateReceiver;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 807
    .local v6, "intentSyncStart":Landroid/content/Intent;
    const-string v18, "com.sec.android.intent.action.WEARABLE_DEVICE_SYNC_AND_START"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 808
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x1ca8

    const/high16 v20, 0x8000000

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v6, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 810
    .local v10, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v4, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 811
    .local v4, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    new-instance v3, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v3}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    .line 812
    .local v3, "bigTextStyle":Landroid/support/v4/app/NotificationCompat$BigTextStyle;
    invoke-virtual {v3, v15}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    .line 813
    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    .line 814
    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->build()Landroid/app/Notification;

    .line 816
    invoke-virtual {v4, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Landroid/support/v4/app/NotificationCompat$Builder;->setSound(Landroid/net/Uri;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 827
    const/high16 v5, 0x20000000

    .line 828
    .local v5, "flags":I
    const/4 v9, 0x0

    .line 829
    .local v9, "notification":Landroid/app/Notification;
    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v9

    .line 830
    iget v0, v9, Landroid/app/Notification;->flags:I

    move/from16 v18, v0

    or-int v18, v18, v5

    move/from16 v0, v18

    iput v0, v9, Landroid/app/Notification;->flags:I

    .line 831
    const/16 v18, 0x42c

    move/from16 v0, v18

    invoke-virtual {v8, v0, v9}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 804
    .end local v3    # "bigTextStyle":Landroid/support/v4/app/NotificationCompat$BigTextStyle;
    .end local v4    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v5    # "flags":I
    .end local v6    # "intentSyncStart":Landroid/content/Intent;
    .end local v9    # "notification":Landroid/app/Notification;
    .end local v10    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_3
    const v18, 0x7f090261

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_1
.end method

.method private setNotification(I)V
    .locals 1
    .param p1, "noti_kind"    # I

    .prologue
    .line 720
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setNotification(IZ)V

    .line 721
    return-void
.end method

.method private setNotification(IZ)V
    .locals 26
    .param p1, "noti_kind"    # I
    .param p2, "update"    # Z

    .prologue
    .line 840
    const-string v20, "WalkingMateDayStepService"

    const-string v21, "Call setNotification"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    sput p1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->pedoNotiType:I

    .line 843
    const-string/jumbo v20, "notification"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/NotificationManager;

    .line 844
    .local v10, "notiManager":Landroid/app/NotificationManager;
    if-nez v10, :cond_1

    .line 845
    const-string v20, "WalkingMateDayStepService"

    const-string v21, "NotificationManager is null"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 998
    :cond_0
    :goto_0
    return-void

    .line 849
    :cond_1
    const/16 v11, 0x42c

    .line 850
    .local v11, "notificationId":I
    const/16 v20, 0x4

    move/from16 v0, p1

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    .line 852
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInactiveTimeTracking()Z

    move-result v20

    if-eqz v20, :cond_0

    .line 868
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopNotification()V

    .line 870
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 871
    .local v14, "resources":Landroid/content/res/Resources;
    const v20, 0x7f090b92

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 872
    .local v18, "ticker":Ljava/lang/String;
    const v20, 0x7f090b92

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 873
    .local v19, "title":Ljava/lang/String;
    const v20, 0x7f090b99

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 874
    .local v17, "subText":Ljava/lang/String;
    const/16 v16, 0x0

    .line 875
    .local v16, "soundUri":Landroid/net/Uri;
    const v13, 0x7f02060f

    .line 877
    .local v13, "resId":I
    packed-switch p1, :pswitch_data_0

    .line 970
    const/16 v20, 0x2

    invoke-static/range {v20 .. v20}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v16

    .line 973
    :goto_1
    invoke-static {v14, v13}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 974
    .local v7, "largeIcon":Landroid/graphics/Bitmap;
    const v15, 0x7f020560

    .line 975
    .local v15, "smallIcon":I
    const/16 v20, 0x2

    move/from16 v0, p1

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 976
    const v15, 0x7f020561

    .line 978
    :cond_3
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getNotificationIntent(I)Landroid/app/PendingIntent;

    move-result-object v12

    .line 981
    .local v12, "pendingIntent":Landroid/app/PendingIntent;
    const/high16 v3, 0x20000000

    .line 983
    .local v3, "flags":I
    new-instance v2, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 984
    .local v2, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 987
    if-nez p2, :cond_4

    .line 988
    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setSound(Landroid/net/Uri;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 990
    :cond_4
    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mNotification:Landroid/app/Notification;

    .line 992
    sget v20, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v21, 0x15

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_5

    .line 993
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mNotification:Landroid/app/Notification;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Landroid/app/Notification;->visibility:I

    .line 995
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mNotification:Landroid/app/Notification;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/app/Notification;->flags:I

    move/from16 v21, v0

    or-int v21, v21, v3

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Landroid/app/Notification;->flags:I

    .line 997
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mNotification:Landroid/app/Notification;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v10, v11, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 857
    .end local v2    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v3    # "flags":I
    .end local v7    # "largeIcon":Landroid/graphics/Bitmap;
    .end local v12    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v13    # "resId":I
    .end local v14    # "resources":Landroid/content/res/Resources;
    .end local v15    # "smallIcon":I
    .end local v16    # "soundUri":Landroid/net/Uri;
    .end local v17    # "subText":Ljava/lang/String;
    .end local v18    # "ticker":Ljava/lang/String;
    .end local v19    # "title":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isPedometerNotificationOn()Z

    move-result v20

    if-eqz v20, :cond_0

    .line 862
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getNotificationValue(Landroid/content/Context;)Z

    move-result v20

    if-nez v20, :cond_2

    goto/16 :goto_0

    .line 879
    .restart local v13    # "resId":I
    .restart local v14    # "resources":Landroid/content/res/Resources;
    .restart local v16    # "soundUri":Landroid/net/Uri;
    .restart local v17    # "subText":Ljava/lang/String;
    .restart local v18    # "ticker":Ljava/lang/String;
    .restart local v19    # "title":Ljava/lang/String;
    :pswitch_0
    const v20, 0x7f090b92

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 880
    const v20, 0x7f090b92

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 881
    const v20, 0x7f090b99

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 882
    const v13, 0x7f02060f

    .line 883
    const v20, 0x7f06002b

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSoundUri(I)Landroid/net/Uri;

    move-result-object v16

    .line 884
    goto/16 :goto_1

    .line 886
    :pswitch_1
    const v20, 0x7f090b93

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x32

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v14, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 887
    const v20, 0x7f090b93

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x32

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    move/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v14, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 888
    const v20, 0x7f090b94

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 889
    const v13, 0x7f02060f

    .line 890
    const v20, 0x7f06002b

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSoundUri(I)Landroid/net/Uri;

    move-result-object v16

    .line 891
    goto/16 :goto_1

    .line 893
    :pswitch_2
    const v20, 0x7f090b95

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 894
    const v20, 0x7f090b95

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 895
    const v20, 0x7f090b96

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 896
    const v13, 0x7f02060e

    .line 897
    const v20, 0x7f06002e

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSoundUri(I)Landroid/net/Uri;

    move-result-object v16

    .line 898
    goto/16 :goto_1

    .line 900
    :pswitch_3
    const v20, 0x7f090b97

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 901
    const v20, 0x7f090b97

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 902
    const v20, 0x7f090b98

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getBestValue()I

    move-result v20

    const/16 v24, 0x2710

    move/from16 v0, v20

    move/from16 v1, v24

    if-ge v0, v1, :cond_7

    const/16 v20, 0x2710

    :goto_2
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 905
    const v13, 0x7f02060f

    .line 906
    const v20, 0x7f06002b

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSoundUri(I)Landroid/net/Uri;

    move-result-object v16

    .line 907
    goto/16 :goto_1

    .line 902
    :cond_7
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getBestValue()I

    move-result v20

    goto :goto_2

    .line 909
    :pswitch_4
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInactiveTimeTracking()Z

    move-result v20

    if-nez v20, :cond_8

    .line 910
    const-string v20, "WalkingMateDayStepService"

    const-string v21, "Inactive notification is off."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 913
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getInactiveTimeTrackingAvailableTimeAlways()Z

    move-result v20

    if-nez v20, :cond_9

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isAvalableInactiveTime()Z

    move-result v20

    if-eqz v20, :cond_14

    .line 915
    :cond_9
    sget-object v20, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v21, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mInactiveTime:J

    invoke-virtual/range {v20 .. v22}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v5

    .line 916
    .local v5, "hours":J
    sget-object v20, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v21, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mInactiveTime:J

    invoke-virtual/range {v20 .. v22}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v20

    sget-object v22, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    sget-object v23, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-wide v24, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mInactiveTime:J

    invoke-virtual/range {v23 .. v25}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v23

    invoke-virtual/range {v22 .. v24}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v22

    sub-long v8, v20, v22

    .line 918
    .local v8, "mins":J
    const-string v4, ""

    .line 919
    .local v4, "formattedTitle":Ljava/lang/String;
    const-wide/16 v20, 0x0

    cmp-long v20, v5, v20

    if-nez v20, :cond_a

    .line 920
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v20

    if-eqz v20, :cond_d

    .line 921
    sget-object v20, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v21, 0x7f090bf2

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v20 .. v22}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 926
    :cond_a
    :goto_3
    const-wide/16 v20, 0x1

    cmp-long v20, v5, v20

    if-nez v20, :cond_b

    .line 927
    const-wide/16 v20, 0x0

    cmp-long v20, v8, v20

    if-nez v20, :cond_f

    .line 928
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v20

    if-eqz v20, :cond_e

    .line 929
    const v20, 0x7f090bf9

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 941
    :cond_b
    :goto_4
    const-wide/16 v20, 0x1

    cmp-long v20, v5, v20

    if-lez v20, :cond_c

    .line 942
    const-wide/16 v20, 0x0

    cmp-long v20, v8, v20

    if-nez v20, :cond_12

    .line 943
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v20

    if-eqz v20, :cond_11

    .line 944
    sget-object v20, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v21, 0x7f090bf1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v20 .. v22}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 958
    :cond_c
    :goto_5
    move-object/from16 v18, v4

    .line 959
    move-object/from16 v19, v4

    .line 960
    const v20, 0x7f090be6

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 961
    const v13, 0x7f02060d

    .line 962
    const v20, 0x7f06002b

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSoundUri(I)Landroid/net/Uri;

    move-result-object v16

    .line 967
    const-string v20, "WalkingMateDayStepService"

    const-string v21, "inactive time notification called"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 923
    :cond_d
    const v20, 0x7f090bf2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 931
    :cond_e
    const v20, 0x7f090bf0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 934
    :cond_f
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v20

    if-eqz v20, :cond_10

    .line 935
    sget-object v20, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v21, 0x7f090bee

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v20 .. v22}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 937
    :cond_10
    const v20, 0x7f090bee

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4

    .line 946
    :cond_11
    const v20, 0x7f090bf1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 949
    :cond_12
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v20

    if-eqz v20, :cond_13

    .line 950
    sget-object v20, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v21, 0x7f090bef

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v20 .. v22}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 953
    :cond_13
    const v20, 0x7f090bef

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    .line 964
    .end local v4    # "formattedTitle":Ljava/lang/String;
    .end local v5    # "hours":J
    .end local v8    # "mins":J
    :cond_14
    const-string v20, "WalkingMateDayStepService"

    const-string v21, "It is not notification period."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 877
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static setSyncing(Z)Z
    .locals 3
    .param p0, "status"    # Z

    .prologue
    .line 1750
    const-string v0, "WalkingMateDayStepService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setSyncStatus() mSyncing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1751
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z

    .line 1752
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncing:Z

    return v0
.end method

.method public static startClearCoverView(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1712
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.shealth.COVER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1713
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v1, "type"

    const-string v2, "WALKMATE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1714
    const-string/jumbo v1, "sent_by_shealth"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1715
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1716
    return-void
.end method

.method public static updateCurrentStatus()V
    .locals 4

    .prologue
    .line 243
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isRun()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mUpdateCurrentStatus:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 247
    :cond_0
    return-void
.end method

.method private updateHomeWidget()V
    .locals 1

    .prologue
    .line 1454
    sget-boolean v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncfailed:Z

    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/widget/AbstractWalkMateAppWidget;->updateHomeWidgets(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V

    .line 1455
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSyncfailed:Z

    .line 1456
    return-void
.end method

.method private static updateLockScreenWidget(Landroid/content/Context;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const v11, 0x7f080bc9

    const v10, 0x7f080bc8

    const/16 v9, 0x8

    const/4 v8, 0x0

    const/4 v7, 0x4

    .line 1491
    const-string v5, "WalkingMateDayStepService"

    const-string v6, "Update Lock Screen Widget"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1493
    sget-boolean v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->dontAllowLockWidget:Z

    if-nez v5, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isLockScreenPedometerChecked()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1494
    :cond_0
    const-string v5, "WalkingMateDayStepService"

    const-string v6, "Removing the lock screen widget!!"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1495
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->removeLockScreenWidget()V

    .line 1622
    :goto_0
    return-void

    .line 1498
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v5

    sput v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    .line 1499
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getGoal()I

    move-result v1

    .line 1501
    .local v1, "goal":I
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f03029c

    invoke-direct {v2, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1504
    .local v2, "lockView":Landroid/widget/RemoteViews;
    sget v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    const/16 v6, 0x2719

    if-eq v5, v6, :cond_2

    .line 1507
    const v5, 0x7f080bc1

    invoke-virtual {v2, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1508
    const-string v5, "WalkingMateDayStepService"

    const-string v6, "Lock screen widget is gone"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1589
    :goto_1
    sget-object v6, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->lock:Ljava/lang/Object;

    monitor-enter v6

    .line 1590
    :try_start_0
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v7, 0x12

    if-lt v5, v7, :cond_c

    .line 1593
    :try_start_1
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-nez v5, :cond_b

    .line 1594
    const-string v5, "WalkingMateDayStepService"

    const-string/jumbo v7, "mKeyguardManager is null"

    invoke-static {v5, v7}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1595
    :try_start_2
    monitor-exit v6

    goto :goto_0

    .line 1621
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 1511
    :cond_2
    const v5, 0x7f080bc1

    invoke-virtual {v2, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1512
    const v5, 0x7f080bca

    invoke-virtual {v2, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1513
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isHealthyStep()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1514
    const v5, 0x7f080bc2

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1515
    const v5, 0x7f080bc7

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1516
    const v5, 0x7f080bc4

    invoke-virtual {v2, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1517
    const v5, 0x7f080bc5

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1518
    const v5, 0x7f080bc6

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1549
    :goto_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v5

    const v6, 0x186a0

    if-lt v5, v6, :cond_8

    .line 1550
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v5

    div-int/lit16 v5, v5, 0x3e8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 1552
    .local v4, "value":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1553
    invoke-virtual {v2, v11, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1554
    const v5, 0x7f080bcb

    invoke-virtual {v2, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1555
    invoke-virtual {v2, v10, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1556
    const v5, 0x7f080bca

    invoke-virtual {v2, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1557
    invoke-virtual {v2, v10, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1579
    .end local v4    # "value":Ljava/lang/String;
    :goto_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_a

    const v3, 0x7f090baa

    .line 1581
    .local v3, "step_id":I
    :goto_4
    const v5, 0x7f080bc1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090019

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1519
    .end local v3    # "step_id":I
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->isInactivetime()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1520
    const v5, 0x7f080bc2

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1521
    const v5, 0x7f080bc7

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1522
    const v5, 0x7f080bc4

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1523
    const v5, 0x7f080bc5

    invoke-virtual {v2, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1524
    const v5, 0x7f080bc6

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2

    .line 1526
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v5

    if-lt v5, v1, :cond_5

    .line 1527
    const v5, 0x7f080bc2

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1528
    const v5, 0x7f080bc7

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1529
    const v5, 0x7f080bc4

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1530
    const v5, 0x7f080bc5

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1531
    const v5, 0x7f080bc6

    invoke-virtual {v2, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2

    .line 1533
    :cond_5
    const v5, 0x7f080bc7

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1534
    const v5, 0x7f080bc4

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1535
    const v5, 0x7f080bc5

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1536
    const v5, 0x7f080bc6

    invoke-virtual {v2, v5, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1537
    const/4 v5, -0x1

    invoke-virtual {v2, v10, v5}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 1538
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1539
    const v5, 0x7f080bc2

    invoke-virtual {v2, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1540
    const v5, 0x7f080bc3

    invoke-virtual {v2, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2

    .line 1542
    :cond_6
    const v5, 0x7f080bc2

    invoke-virtual {v2, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1543
    const v5, 0x7f080bc3

    invoke-virtual {v2, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2

    .line 1559
    .restart local v4    # "value":Ljava/lang/String;
    :cond_7
    invoke-virtual {v2, v10, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1560
    const v5, 0x7f080bca

    invoke-virtual {v2, v5, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1561
    invoke-virtual {v2, v11, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1562
    const v5, 0x7f080bcb

    invoke-virtual {v2, v5, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1563
    invoke-virtual {v2, v11, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1566
    .end local v4    # "value":Ljava/lang/String;
    :cond_8
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1567
    invoke-virtual {v2, v11, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1568
    invoke-virtual {v2, v10, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1569
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v10, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1572
    :cond_9
    invoke-virtual {v2, v10, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1573
    invoke-virtual {v2, v11, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1574
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v11, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 1579
    :cond_a
    const v3, 0x7f0900c6

    goto/16 :goto_4

    .line 1598
    :cond_b
    :try_start_3
    const-string v5, "WalkingMateDayStepService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Keguard locked:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v8}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1599
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v5}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v5

    if-nez v5, :cond_e

    .line 1600
    sget-boolean v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsKeyManagerSetAdaptiveEvent:Z

    const/4 v7, 0x1

    if-ne v5, v7, :cond_d

    .line 1601
    const-string v5, "WalkingMateDayStepService"

    const-string/jumbo v7, "remove keyguard, keyguard is unlocked"

    invoke-static {v5, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1602
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    const-string v7, "com.sec.android.app.shealth.walkingmate.service.WalkingMateDayStepService"

    invoke-virtual {v5, v7}, Landroid/app/KeyguardManager;->removeAdaptiveEvent(Ljava/lang/String;)V

    .line 1604
    const/4 v5, 0x0

    sput-boolean v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsKeyManagerSetAdaptiveEvent:Z
    :try_end_3
    .catch Ljava/lang/NoSuchMethodError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1621
    :cond_c
    :goto_5
    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1606
    :cond_d
    :try_start_5
    const-string v5, "WalkingMateDayStepService"

    const-string/jumbo v7, "mIsKeyManagerSetAdaptiveEvent is false"

    invoke-static {v5, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NoSuchMethodError; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_5

    .line 1617
    :catch_0
    move-exception v0

    .line 1618
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    :try_start_6
    const-string v5, "WalkingMateDayStepService"

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 1609
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :cond_e
    :try_start_7
    const-string v5, "WalkingMateDayStepService"

    const-string/jumbo v7, "update keyguard"

    invoke-static {v5, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1610
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v5, :cond_c

    .line 1611
    sget-object v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mKeyguardManager:Landroid/app/KeyguardManager;

    const-string v7, "com.sec.android.app.shealth.walkingmate.service.WalkingMateDayStepService"

    invoke-virtual {v5, v7, v2, v2}, Landroid/app/KeyguardManager;->setAdaptiveEvent(Ljava/lang/String;Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    .line 1614
    const/4 v5, 0x1

    sput-boolean v5, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsKeyManagerSetAdaptiveEvent:Z
    :try_end_7
    .catch Ljava/lang/NoSuchMethodError; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_5
.end method

.method private updateWidget()V
    .locals 2

    .prologue
    .line 1657
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getWidgetsCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1658
    const-string v0, "WalkingMateDayStepService"

    const-string v1, "getWidgetsCount is not Empty"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1659
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 1660
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateHomeWidget()V

    .line 1662
    :cond_0
    return-void
.end method

.method private walkingMateServiceStart()V
    .locals 2

    .prologue
    .line 596
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    const-string v0, "in Pedo on, healthyStep, ActiveTime, inactiveTime start"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 598
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setInactiveMonitor(I)V

    .line 599
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setActiveTime()V

    .line 600
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->setHealthyStep()V

    .line 606
    :goto_0
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$6;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHealthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    .line 648
    invoke-static {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHealthyCallBack:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor;->addCallBacks(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateActiveTimeMonitor$CallBacks;)V

    .line 649
    return-void

    .line 602
    :cond_0
    const-string v0, "in Pedo off, healthyStep, ActiveTime do not start"

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public StartProcessAfterServiceStart()V
    .locals 2

    .prologue
    .line 1769
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$15;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1806
    return-void
.end method

.method public checkNotification()V
    .locals 2

    .prologue
    .line 1719
    sget v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    const/16 v1, 0x2719

    if-eq v0, v1, :cond_1

    .line 1720
    const-string v0, "WalkingMateDayStepService"

    const-string v1, "Current device is not phone"

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1742
    :cond_0
    :goto_0
    return-void

    .line 1724
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isNotUsedNotify()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1725
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopNotification()V

    .line 1726
    const-string v0, "WalkingMateDayStepService"

    const-string v1, "NOTI_CASE_NOT_USED ~1"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1727
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setNotification(I)V

    goto :goto_0

    .line 1728
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isNewRecordNotify()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1729
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setNotification(I)V

    goto :goto_0

    .line 1730
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isAcheivedNotify()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1731
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setNotification(I)V

    goto :goto_0

    .line 1735
    :cond_4
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->isHalfNotify()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1736
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->setNotification(I)V

    goto :goto_0
.end method

.method public getExerciseId()J
    .locals 12

    .prologue
    .line 1029
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "select exercise._id, ifnull(device_type, 10009) as device_type         from exercise         left join                 (select * from exercise_device_info                 join                         user_device                 on exercise_device_info.user_device__id == user_device._id) as A         on exercise._id == A.exercise__id         where device_type == 10009  and start_time >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getStartOfDay(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "start_time"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/common/utils/calendar/PeriodUtils;->getEndOfDay(J)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1038
    .local v3, "selectionClause":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1040
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1042
    if-eqz v6, :cond_2

    .line 1043
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 1044
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1045
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 1046
    .local v9, "returnValue":J
    if-eqz v6, :cond_0

    .line 1047
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1053
    :cond_0
    if-eqz v6, :cond_1

    .line 1054
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1077
    .end local v9    # "returnValue":J
    :cond_1
    :goto_0
    return-wide v9

    .line 1053
    :cond_2
    if-eqz v6, :cond_3

    .line 1054
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1058
    :cond_3
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1059
    .local v7, "cv":Landroid/content/ContentValues;
    const-string v0, "exercise_info__id"

    const/16 v1, 0x4651

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1060
    const-string v0, "exercise_type"

    const/16 v1, 0x4e23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1061
    const-string/jumbo v0, "start_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1062
    const-string v0, "end_time"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1063
    const-string/jumbo v0, "update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1064
    const-string v0, "comment"

    const-string v1, ""

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    const-string v0, "input_source_type"

    const v1, 0x3f7a2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1067
    const/4 v11, 0x0

    .line 1069
    .local v11, "rowId":Landroid/net/Uri;
    :try_start_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v11

    .line 1074
    :goto_1
    if-eqz v11, :cond_5

    .line 1075
    invoke-virtual {v11}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    goto :goto_0

    .line 1053
    .end local v7    # "cv":Landroid/content/ContentValues;
    .end local v11    # "rowId":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 1054
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1070
    .restart local v7    # "cv":Landroid/content/ContentValues;
    .restart local v11    # "rowId":Landroid/net/Uri;
    :catch_0
    move-exception v8

    .line 1071
    .local v8, "ie":Ljava/lang/IllegalArgumentException;
    const-string v0, "WalkingMateDayStepService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "insert exception-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1077
    .end local v8    # "ie":Ljava/lang/IllegalArgumentException;
    :cond_5
    const-wide/16 v9, -0x1

    goto/16 :goto_0
.end method

.method public isWalkingStarted()Z
    .locals 4

    .prologue
    .line 670
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isInitializationNeeded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 671
    const/4 v0, 0x0

    .line 676
    :goto_0
    return v0

    .line 674
    :cond_0
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v0

    .line 675
    .local v0, "startWalking":Z
    const-string v1, "WalkingMateDayStepService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isStarted = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadLatestData(ZZ)V
    .locals 5
    .param p1, "loadFromDB"    # Z
    .param p2, "isWearable"    # Z

    .prologue
    .line 1082
    iget-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsLoadLocked:Z

    if-eqz v1, :cond_0

    .line 1083
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "loadLatestData is getting processed, returning ..."

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1107
    :goto_0
    return-void

    .line 1087
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mLoadLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1088
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsLoadLocked:Z

    .line 1089
    const-string/jumbo v0, "normal"

    .line 1090
    .local v0, "walkingMode":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 1091
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->updateFromDB()V

    .line 1093
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->getViewStepCount()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->sendResponse(I)V

    .line 1094
    sget v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I

    const/16 v3, 0x2719

    if-eq v1, v3, :cond_2

    if-eqz p2, :cond_3

    .line 1095
    :cond_2
    sget-object v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mWalkingMode:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1096
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->sendModeChange(Ljava/lang/String;)V

    .line 1097
    sput-object v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mWalkingMode:Ljava/lang/String;

    .line 1101
    :cond_3
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateWidgets(Landroid/content/Context;)V

    .line 1102
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->checkCoverView()V

    .line 1104
    const-string v1, "WalkingMateDayStepService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "mTotalStep = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getTotalStep()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsLoadLocked:Z

    .line 1106
    monitor-exit v2

    goto :goto_0

    .end local v0    # "walkingMode":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 681
    const-string v0, "PEDOSTART"

    const-string v1, "DayStepService onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 298
    const-string v0, "WalkingMateDayStepService"

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 301
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->setDayStepService(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    .line 310
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isSHealthUpgradeNeeded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    const-string v0, "WalkingMateDayStepService"

    const-string v1, "isUpgradeNeeded = true"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopSelf()V

    .line 367
    :goto_0
    return-void

    .line 317
    :cond_0
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    const-string v0, "WalkingMateDayStepService"

    const-string v1, "isHealthServiceOld = true"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopSelf()V

    goto :goto_0

    .line 324
    :cond_1
    new-instance v0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$3;->start()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 653
    const-string v1, "WalkingMateDayStepService"

    const-string/jumbo v2, "onDestroy()"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    sput-boolean v3, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mIsRun:Z

    .line 655
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->clearListener()V

    .line 656
    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->onStop(Z)V

    .line 659
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCocktailWidgetRequestReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 660
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mSystemMonitor:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 661
    iget-object v1, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mBluetoothBroadcastReceiver:Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$BluetoothBroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 666
    :goto_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 667
    return-void

    .line 662
    :catch_0
    move-exception v0

    .line 663
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "It is failed to unregister, because it is not register."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onLocaleChanged()V
    .locals 2

    .prologue
    .line 716
    const-string v0, "WalkingMateDayStepService"

    const-string v1, "This feature is not supported"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$8;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1399
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v0, 0x2

    .line 371
    const-string v1, "WalkingMateDayStepService"

    const-string/jumbo v2, "onStartCommand"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->I(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;)V

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$4;->start()V

    .line 404
    invoke-static {}, Lcom/sec/android/service/health/keyManager/KeyManager;->getInstance()Lcom/sec/android/service/health/keyManager/KeyManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/service/health/keyManager/KeyManager;->isContentProviderAccessible()Z

    move-result v1

    if-nez v1, :cond_0

    .line 405
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "CP is not accessible"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "Stop Self with NOT STICKY"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopSelf()V

    .line 418
    :goto_0
    return v0

    .line 411
    :cond_0
    invoke-static {p0}, Lcom/sec/android/service/health/upgrade/SHealthUpgradeHelper;->isHealthServiceUpgradeNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 412
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "isHealthServiceOld = true"

    invoke-static {v1, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->stopSelf()V

    goto :goto_0

    .line 418
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onStop(Z)V
    .locals 2
    .param p1, "reStart"    # Z

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService$9;-><init>(Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1431
    return-void
.end method

.method public resetData(Z)V
    .locals 2
    .param p1, "stopNoti"    # Z

    .prologue
    .line 1353
    const-string v0, "WalkingMateDayStepService"

    const-string/jumbo v1, "resetData !~~~~~"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1354
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->resetTodayData()V

    .line 1356
    if-eqz p1, :cond_0

    .line 1357
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/walkingmate/data/WalkingMateTodayData;->resetNotificationStatus()V

    .line 1359
    :cond_0
    return-void
.end method

.method public stopNotification()V
    .locals 3

    .prologue
    .line 1001
    const-string v1, "WalkingMateDayStepService"

    const-string/jumbo v2, "notification cancel called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    const-string/jumbo v1, "notification"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1003
    .local v0, "notiManager":Landroid/app/NotificationManager;
    if-nez v0, :cond_0

    .line 1004
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "NotificationManager is null"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    :goto_0
    return-void

    .line 1008
    :cond_0
    sget v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->pedoNotiType:I

    if-nez v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper$WALKING_STATISTICS;->isStartWalking()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1011
    const-string v1, "WalkingMateDayStepService"

    const-string v2, "Pedo paused state notification should not be canceled"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1013
    :cond_1
    const/16 v1, 0x42c

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public updateWidgets(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1317
    sget-boolean v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isUpdateWidget:Z

    if-eqz v1, :cond_0

    .line 1318
    const-string v1, "WalkingMateDayStepService"

    const-string/jumbo v2, "skip update widgets"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1350
    :goto_0
    return-void

    .line 1322
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->lock:Ljava/lang/Object;

    monitor-enter v2

    .line 1323
    :try_start_0
    const-string v1, "WalkingMateDayStepService"

    const-string/jumbo v3, "update widgets"

    invoke-static {v1, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isUpdateWidget:Z

    .line 1326
    invoke-static {}, Lcom/sec/android/app/shealth/widget/ClearCoverBroadcastReciever;->checkCoverView()V

    .line 1327
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->sendCurrentStatusToCockTailWidget(Landroid/content/Context;)V

    .line 1329
    sget v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->mCurrentDevice:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v3, 0x2719

    if-ne v1, v3, :cond_2

    .line 1331
    :try_start_1
    invoke-static {p1}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateLockScreenWidget(Landroid/content/Context;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1343
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->getWidgetsCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 1344
    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->updateHomeWidget()V

    .line 1345
    invoke-static {}, Lcom/sec/android/app/shealth/widget/WalkMateAppEasyWidget;->updateWidgets()V

    .line 1348
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->isUpdateWidget:Z

    .line 1349
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 1332
    :catch_0
    move-exception v0

    .line 1333
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1337
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/WalkingMateDayStepService;->removeLockScreenWidget()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1338
    :catch_1
    move-exception v0

    .line 1339
    .restart local v0    # "e":Landroid/os/RemoteException;
    :try_start_5
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method
