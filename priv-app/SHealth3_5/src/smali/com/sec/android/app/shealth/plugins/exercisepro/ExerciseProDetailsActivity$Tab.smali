.class public final enum Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
.super Ljava/lang/Enum;
.source "ExerciseProDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "Tab"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

.field public static final enum CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

.field public static final enum DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

.field public static final enum MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    const-string v1, "DETAIL"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    const-string v1, "MAP"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    new-instance v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    const-string v1, "CHART"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    .line 94
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->DETAIL:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->MAP:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->CHART:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->$VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 94
    const-class v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->$VALUES:[Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    invoke-virtual {v0}, [Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$Tab;

    return-object v0
.end method
