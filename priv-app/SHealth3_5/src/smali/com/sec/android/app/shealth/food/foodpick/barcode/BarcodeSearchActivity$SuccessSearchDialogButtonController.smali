.class Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;
.super Ljava/lang/Object;
.source "BarcodeSearchActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SuccessSearchDialogButtonController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;
    .param p2, "x1"    # Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$1;

    .prologue
    .line 367
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;-><init>(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 2
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 370
    sget-object v0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$8;->$SwitchMap$com$sec$android$app$shealth$common$commonui$dialog$DialogButtonType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 387
    :goto_0
    return-void

    .line 372
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mExtraFoodInfoHolder:Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1500(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;

    move-result-object v1

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->sendResultAndFinish(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1600(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Lcom/sec/android/app/shealth/food/foodpick/search/api/base/ResultHolder;)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mIsPopupShown:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1702(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;Z)Z

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->OK_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 378
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->finish()V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->CANCEL_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 383
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # invokes: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->tryNewSearch()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1800(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity$SuccessSearchDialogButtonController;->this$0:Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;

    # getter for: Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->mUserActionLogger:Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;
    invoke-static {v0}, Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;->access$1100(Lcom/sec/android/app/shealth/food/foodpick/barcode/BarcodeSearchActivity;)Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/shealth/food/app/UserActionLog;->TRY_AGAIN_AFTER_BARCODE_SEARCH_BY_COMPLETE_POPUP:Lcom/sec/android/app/shealth/food/app/UserActionLog;

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/food/app/IFoodUserActionLogger;->logUserAction(Lcom/sec/android/app/shealth/food/app/UserActionLog;)V

    goto :goto_0

    .line 370
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
