.class public Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "WebViewFragment.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SetJavaScriptEnabled"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/moreapps/WebViewFragment$CustomWebView;,
        Lcom/sec/android/app/shealth/moreapps/WebViewFragment$WebInterface;,
        Lcom/sec/android/app/shealth/moreapps/WebViewFragment$WebViewBridge;
    }
.end annotation


# static fields
.field private static WEBVIEW_URL_CN:Ljava/lang/String;

.field private static WEBVIEW_URL_OTHER:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;

.field private static mJsonData:Ljava/lang/String;

.field public static webServer:Ljava/lang/String;


# instance fields
.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-string v0, "http://img.samsungshealth.com/Moreapps/index3_5.html"

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->WEBVIEW_URL_OTHER:Ljava/lang/String;

    .line 55
    const-string v0, "http://cn-img.samsungshealth.com/Moreapps/index3_5.html"

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->WEBVIEW_URL_CN:Ljava/lang/String;

    .line 58
    sget-object v0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->WEBVIEW_URL_OTHER:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->webServer:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 156
    return-void
.end method

.method static synthetic access$300()Landroid/content/Context;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mJsonData:Ljava/lang/String;

    return-object v0
.end method

.method public static newInstance(ILjava/lang/String;Landroid/content/Context;)Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
    .locals 1
    .param p0, "tab"    # I
    .param p1, "json"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    sput-object p1, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mJsonData:Ljava/lang/String;

    .line 73
    sput-object p2, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mContext:Landroid/content/Context;

    .line 75
    new-instance v0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;-><init>()V

    .line 77
    .local v0, "wvFragment":Lcom/sec/android/app/shealth/moreapps/WebViewFragment;
    return-object v0
.end method


# virtual methods
.method public getWebView()Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 84
    const v2, 0x7f03017f

    invoke-virtual {p1, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 86
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f080670

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$CustomWebView;

    invoke-direct {v3, p0, v6}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$CustomWebView;-><init>(Lcom/sec/android/app/shealth/moreapps/WebViewFragment;Lcom/sec/android/app/shealth/moreapps/WebViewFragment$1;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 92
    .local v0, "settings":Landroid/webkit/WebSettings;
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 95
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_0

    .line 97
    sget-object v2, Landroid/webkit/WebSettings$LayoutAlgorithm;->TEXT_AUTOSIZING:Landroid/webkit/WebSettings$LayoutAlgorithm;

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLayoutAlgorithm(Landroid/webkit/WebSettings$LayoutAlgorithm;)V

    .line 100
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 101
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 103
    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$WebViewBridge;

    invoke-direct {v3, p0, v6}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$WebViewBridge;-><init>(Lcom/sec/android/app/shealth/moreapps/WebViewFragment;Lcom/sec/android/app/shealth/moreapps/WebViewFragment$1;)V

    const-string v4, "clickEvent"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$WebInterface;

    invoke-direct {v3, p0, v6}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$WebInterface;-><init>(Lcom/sec/android/app/shealth/moreapps/WebViewFragment;Lcom/sec/android/app/shealth/moreapps/WebViewFragment$1;)V

    const-string/jumbo v4, "toapp"

    invoke-virtual {v2, v3, v4}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    sget-object v2, Lcom/sec/android/app/shealth/moreapps/service/MoreAppsServiceManager;->regionGroup:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    sget-object v3, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->CN:Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    if-ne v2, v3, :cond_1

    .line 110
    sget-object v2, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->WEBVIEW_URL_CN:Ljava/lang/String;

    sput-object v2, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->webServer:Ljava/lang/String;

    .line 117
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/moreapps/WebViewFragment$1;-><init>(Lcom/sec/android/app/shealth/moreapps/WebViewFragment;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 123
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->setLongClickable(Z)V

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2, v5}, Landroid/webkit/WebView;->setHapticFeedbackEnabled(Z)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->mWebView:Landroid/webkit/WebView;

    sget-object v3, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->webServer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 128
    return-object v1

    .line 113
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->WEBVIEW_URL_OTHER:Ljava/lang/String;

    sput-object v2, Lcom/sec/android/app/shealth/moreapps/WebViewFragment;->webServer:Ljava/lang/String;

    goto :goto_0
.end method
