.class Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;
.super Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;
.source "CircleViewAnimationRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CircleViewProgressUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;II)V
    .locals 0
    .param p2, "delay"    # I
    .param p3, "messagesCount"    # I

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    .line 44
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/shealth/home/widget/circleview/ProgressUpdater;-><init>(II)V

    .line 45
    return-void
.end method


# virtual methods
.method protected onProgressUpdate(F)V
    .locals 4
    .param p1, "progress"    # F

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$100(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->from:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$000(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)F

    move-result v0

    const/high16 v2, 0x43b40000    # 360.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setStartAngle(F)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$100(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->from:F
    invoke-static {v1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$000(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->to:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$200(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->from:F
    invoke-static {v3}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$000(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->getInterpolation(F)F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setEndAngle(F)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->circleView:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$100(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->invalidate()V

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->animationEndListener:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$300(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;

    move-result-object v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->animationEndListener:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$300(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$AnimationEndListener;->onAnimationEnd()V

    .line 55
    :cond_0
    return-void

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner$CircleViewProgressUpdater;->this$0:Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;

    # getter for: Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->from:F
    invoke-static {v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;->access$000(Lcom/sec/android/app/shealth/home/widget/circleview/CircleViewAnimationRunner;)F

    move-result v0

    goto :goto_0
.end method
