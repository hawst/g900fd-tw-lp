.class public Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;
.super Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;
.source "MealDaoImplDb.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/fooddao/MealDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
        "Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;",
        ">;",
        "Lcom/sec/android/app/shealth/food/fooddao/MealDao;"
    }
.end annotation


# static fields
.field private static final CONTRACT_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/app/shealth/food/fooddao/MealDao$MealContract;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mFilterQueryBuilder:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

.field private mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->CONTRACT_MAP:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    const-string/jumbo v3, "sample_time"

    const-string v4, "_id"

    const-string/jumbo v5, "total_kilo_calorie"

    const-string/jumbo v6, "meal"

    sget-object v7, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->CONTRACT_MAP:Ljava/util/Map;

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;-><init>(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 61
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    .line 62
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v2, "category"

    sget-object v3, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->PLANNED_EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->getConstantRepresentation()Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "="

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;-><init>(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)V

    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string v2, "category"

    sget-object v3, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->getConstantRepresentation()Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "="

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;->OR(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->mFilterQueryBuilder:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    .line 79
    return-void
.end method


# virtual methods
.method public addGoal(F)J
    .locals 3
    .param p1, "goalValue"    # F

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    new-instance v1, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    const v2, 0x9c43

    invoke-direct {v1, p1, v2}, Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;-><init>(FI)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->insertData(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 2
    .param p1, "beforTime"    # J

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    const v1, 0x9c43

    invoke-interface {v0, v1, p1, p2}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->getActiveGoal(IJ)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    return-object v0
.end method

.method protected getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->mFilterQueryBuilder:Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    return-object v0
.end method

.method public bridge synthetic getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)Landroid/content/ContentValues;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;

    .prologue
    .line 50
    check-cast p1, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .end local p1    # "x0":Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public getDataContentValue(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;)Landroid/content/ContentValues;
    .locals 5
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    .line 91
    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;

    invoke-direct {v1, p1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;-><init>(Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;)V

    .line 92
    .local v1, "values":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;
    const-string v2, "comment"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getComment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string/jumbo v2, "total_kilo_calorie"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getKcal()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 94
    const-string/jumbo v2, "sample_time"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 95
    const-string/jumbo v2, "type"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 96
    const-string v2, "category"

    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->getDefaultCategory(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;)Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->getConstantRepresentation()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 97
    const-string/jumbo v2, "meal_plan__id"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealPlanId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 98
    const-string v2, "favorite"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->isFavourite()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 100
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/ContentValuesWrapperWithValidation;->getContentValues()Landroid/content/ContentValues;

    move-result-object v0

    .line 101
    .local v0, "cv":Landroid/content/ContentValues;
    const-string/jumbo v3, "name"

    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, ""

    :goto_1
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method protected getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;
    .locals 14
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .param p5, "filter"    # Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;
    .param p6, "sortOrder"    # Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;",
            "Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 159
    .local v8, "tempCal":Ljava/util/Calendar;
    move-wide v0, p1

    invoke-virtual {v8, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 160
    const/4 v9, 0x5

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v7

    .line 161
    .local v7, "startDate":I
    move-wide/from16 v0, p3

    invoke-virtual {v8, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 162
    const/4 v9, 0x5

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 163
    .local v2, "endDate":I
    if-ne v7, v2, :cond_4

    .line 165
    invoke-super/range {p0 .. p6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;

    move-result-object v5

    .line 166
    .local v5, "mResultantList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    if-nez v5, :cond_0

    .line 168
    const/4 v9, 0x0

    .line 188
    .end local v5    # "mResultantList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    :goto_0
    return-object v9

    .line 170
    .restart local v5    # "mResultantList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    :cond_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 171
    .local v6, "mResultantMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .line 173
    .local v4, "mMealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 175
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v10

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    invoke-virtual {v9}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealTime()J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-lez v9, :cond_1

    .line 177
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v6, v9, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 182
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->getMealType()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v6, v9, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 185
    .end local v4    # "mMealData":Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    :cond_3
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 188
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "mResultantList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    .end local v6    # "mResultantMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;>;"
    :cond_4
    invoke-super/range {p0 .. p6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;

    move-result-object v9

    goto :goto_0
.end method

.method public getDataForPeriodExclusive(JJ)Ljava/util/List;
    .locals 7
    .param p1, "periodStart"    # J
    .param p3, "periodEnd"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->getCommonMethodsFilter()Lcom/sec/android/app/shealth/common/commondao/Filters/FilterQueryBuilder;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-super/range {v0 .. v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/specific/CommonDaoForLogListAndChartsWithFilter;->getDataForPeriod(JJLcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;Lcom/sec/android/app/shealth/common/commondao/daointerfaces/IMeasureSpecificDbOperations$SortOrder;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getDefaultCategory(Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;)Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    .prologue
    .line 146
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;->isPlanned()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->PLANNED_EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;->EATEN:Lcom/sec/android/app/shealth/food/fooddao/utils/MealCategory;

    goto :goto_0
.end method

.method protected bridge synthetic getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/BaseEntryData;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    move-result-object v0

    return-object v0
.end method

.method protected getItemFromRow(Landroid/database/Cursor;)Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;
    .locals 20
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 108
    new-instance v19, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;-><init>(Landroid/database/Cursor;)V

    .line 109
    .local v19, "getter":Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;
    new-instance v2, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;

    const-string v3, "_id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string/jumbo v5, "name"

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "total_kilo_calorie"

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getFloat(Ljava/lang/String;)F

    move-result v6

    const-string/jumbo v7, "type"

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string/jumbo v8, "sample_time"

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    const-string v10, "comment"

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "meal_plan__id"

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    const-string v13, "create_time"

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v13

    const-string/jumbo v15, "update_time"

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getLong(Ljava/lang/String;)J

    move-result-wide v15

    const-string/jumbo v17, "time_zone"

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getInt(Ljava/lang/String;)I

    move-result v17

    const-string v18, "favorite"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/commondaoimpl/tools/CursorWrapperWithValidation;->getBoolean(Ljava/lang/String;)Z

    move-result v18

    invoke-direct/range {v2 .. v18}, Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;-><init>(JLjava/lang/String;FIJLjava/lang/String;JJJIZ)V

    return-object v2
.end method

.method public getLatestGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->mGoalDao:Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;

    const v1, 0x9c43

    invoke-interface {v0, v1}, Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;->getLatestGoal(I)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    return-object v0
.end method

.method public getMealsByName(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/common/utils/hcp/data/MealData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;

    const-string/jumbo v1, "name"

    const-string v2, "="

    invoke-direct {v0, v1, p1, v2}, Lcom/sec/android/app/shealth/common/commondao/Filters/RangeFilter;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/food/fooddao/MealDaoImplDb;->getAllDatas(Lcom/sec/android/app/shealth/common/commondao/Filters/FilterContainable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
