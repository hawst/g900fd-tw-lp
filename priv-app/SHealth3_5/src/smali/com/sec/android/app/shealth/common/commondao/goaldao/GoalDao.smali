.class public interface abstract Lcom/sec/android/app/shealth/common/commondao/goaldao/GoalDao;
.super Ljava/lang/Object;
.source "GoalDao.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commondao/CommonDao;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/app/shealth/common/commondao/CommonDao",
        "<",
        "Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract deleteAllByGoalType(I)I
.end method

.method public abstract getActiveGoal(IJ)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
.end method

.method public abstract getLatestGoal(I)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
