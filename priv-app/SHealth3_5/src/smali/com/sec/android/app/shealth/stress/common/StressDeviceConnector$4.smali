.class Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$4;
.super Landroid/os/CountDownTimer;
.source "StressDeviceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 390
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 400
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onFinish!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mStressSensorListener:Lcom/sec/android/app/shealth/stress/StressSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$500(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/sec/android/app/shealth/stress/StressSensorListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/shealth/stress/StressSensorListener;->onTimeout()V

    .line 402
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 394
    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CountDownTimer onTick: "

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector$4;->this$0:Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;

    # getter for: Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->mStressSensorListener:Lcom/sec/android/app/shealth/stress/StressSensorListener;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;->access$500(Lcom/sec/android/app/shealth/stress/common/StressDeviceConnector;)Lcom/sec/android/app/shealth/stress/StressSensorListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/shealth/stress/StressSensorListener;->onTick(J)V

    .line 396
    return-void
.end method
