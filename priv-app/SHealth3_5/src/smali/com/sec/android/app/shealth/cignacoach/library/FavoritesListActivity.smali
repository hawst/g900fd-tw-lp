.class public Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "FavoritesListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Landroid/widget/ExpandableListView$OnChildClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ISaveChosenItemListenerGetter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;
    }
.end annotation


# static fields
.field private static final DIALOG_FILTER:Ljava/lang/String; = "dialog_filter"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCategoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

.field private mFavoriteListView:Landroid/widget/ExpandableListView;

.field private mFavoritesFactoryDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;"
        }
    .end annotation
.end field

.field private mFavoritesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 43
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortByCategory()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortByTitle()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method private expandAllCategory()V
    .locals 3

    .prologue
    .line 217
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 218
    .local v0, "categoryCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 218
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 221
    :cond_0
    return-void
.end method

.method private getListData()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 75
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryDataManager;->getFavoriteArticle()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    .line 76
    return-void
.end method

.method private getListEmptyView()Landroid/view/View;
    .locals 2

    .prologue
    .line 136
    const v1, 0x7f0801ca

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 137
    .local v0, "emptyView":Landroid/view/View;
    return-object v0
.end method

.method private setAdapter()V
    .locals 3

    .prologue
    .line 79
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 81
    return-void
.end method

.method private setListSort()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    if-ne v0, v1, :cond_1

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortByCategory()V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;->TITLE:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    if-ne v0, v1, :cond_0

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortByTitle()V

    goto :goto_0
.end method

.method private sortByCategory()V
    .locals 7

    .prologue
    .line 141
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 142
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 144
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 145
    .local v1, "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getCategory()Ljava/lang/String;

    move-result-object v3

    .line 146
    .local v3, "groupString":Ljava/lang/CharSequence;
    if-eqz v3, :cond_0

    .line 147
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 148
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 151
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 154
    .local v2, "favoritesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    .end local v1    # "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    .end local v2    # "favoritesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    .end local v3    # "groupString":Ljava/lang/CharSequence;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 162
    .local v0, "articleDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortFavoriteListByTitle(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 165
    .end local v0    # "articleDataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->notifyDataSetChanged()V

    .line 167
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->expandAllCategory()V

    .line 169
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    .line 170
    return-void
.end method

.method private sortByTitle()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 173
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 174
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 176
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v2, "favoriteArrayData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 180
    .local v1, "dataListSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 181
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 184
    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->sortFavoriteListByTitle(Ljava/util/ArrayList;)V

    .line 186
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    .line 187
    .local v0, "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 190
    .end local v0    # "data":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 191
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    const v6, 0x7f090076

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->notifyDataSetChanged()V

    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->expandAllCategory()V

    .line 198
    sget-object v5, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;->TITLE:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    iput-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    .line 199
    return-void
.end method

.method private sortFavoriteListByTitle(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 206
    .local p1, "favoriteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)V

    .line 213
    .local v0, "favoriteComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;>;"
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 214
    return-void
.end method

.method private updateList()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->getListData()V

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->setAdapter()V

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->setListSort()V

    .line 71
    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090c55

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 99
    return-void
.end method

.method public getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
    .locals 1
    .param p1, "dialogTag"    # Ljava/lang/String;

    .prologue
    .line 242
    const-string v0, "dialog_filter"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;)V

    .line 263
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 292
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 294
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    .line 295
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 296
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->updateList()V

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->invalidateOptionsMenu()V

    .line 300
    :cond_0
    return-void
.end method

.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 4
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 231
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesListAdapter:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;

    invoke-virtual {v2, p3, p4}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListAdapter;->getChild(II)Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;

    move-result-object v0

    .line 233
    .local v0, "cignaLibraryArticleData":Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/shealth/cignacoach/library/TipArticleDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 235
    const-string v2, "extra_name_article_id"

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/cignacoach/data/CignaLibraryArticleData;->getId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 236
    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 237
    const/4 v2, 0x0

    return v2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v1, 0x7f03005b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->setContentView(I)V

    .line 50
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCategoryList:Ljava/util/ArrayList;

    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesList:Ljava/util/ArrayList;

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->getListEmptyView()Landroid/view/View;

    move-result-object v0

    .line 55
    .local v0, "emptyView":Landroid/view/View;
    const v1, 0x7f0801c9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ExpandableListView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->setEmptyView(Landroid/view/View;)V

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoriteListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, p0}, Landroid/widget/ExpandableListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    sget-object v2, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    if-ne v1, v2, :cond_0

    .line 62
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->setFilterCategory(Z)V

    .line 64
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->updateList()V

    .line 65
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100018

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 1
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "id"    # J

    .prologue
    .line 225
    const/4 v0, 0x1

    return v0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 7
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "av":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x1

    .line 269
    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionType(J)I

    move-result v5

    if-ne v5, v4, :cond_1

    .line 270
    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionGroup(J)I

    move-result v1

    .line 271
    .local v1, "groupPosition":I
    invoke-static {p4, p5}, Landroid/widget/ExpandableListView;->getPackedPositionChild(J)I

    move-result v0

    .line 273
    .local v0, "childPosition":I
    iget-object v5, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mCurrentSortType:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;->CATEGORY:Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity$SortType;

    if-ne v5, v6, :cond_0

    .line 274
    const/4 v3, 0x1

    .line 278
    .local v3, "sortType":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/app/shealth/cignacoach/library/SelectFavoriteActivity;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "EXTRA_NAME_SORTING_TYPE"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 280
    const-string v5, "EXTRA_NAME_EXPAND_GROUP_POSITION"

    invoke-virtual {v2, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 281
    const-string v5, "EXTRA_NAME_EXPAND_CHILD_POSITION"

    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 282
    const-string v5, "EXTRA_NAME_DELETE_TYPE"

    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;->DELETE:Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 283
    const/16 v5, 0x64

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 287
    .end local v0    # "childPosition":I
    .end local v1    # "groupPosition":I
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "sortType":Z
    :goto_1
    return v4

    .line 276
    .restart local v0    # "childPosition":I
    .restart local v1    # "groupPosition":I
    :cond_0
    const/4 v3, 0x0

    .restart local v3    # "sortType":Z
    goto :goto_0

    .line 287
    .end local v0    # "childPosition":I
    .end local v1    # "groupPosition":I
    .end local v3    # "sortType":Z
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 130
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/app/shealth/cignacoach/util/CignaLibraryMoreUtils;->showMoreForContextMenu(Landroid/content/Context;Landroid/view/MenuItem;)V

    .line 132
    const/4 v0, 0x1

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f080ca6

    const/4 v0, 0x1

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/library/FavoritesListActivity;->mFavoritesFactoryDataList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 113
    const v1, 0x7f080c8d

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 114
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 116
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isChinaModel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isHKTWModel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 124
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    .line 120
    :cond_2
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    goto :goto_0
.end method
