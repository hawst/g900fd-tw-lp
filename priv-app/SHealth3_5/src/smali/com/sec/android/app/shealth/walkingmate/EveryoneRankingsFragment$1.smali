.class Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;
.super Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
.source "EveryoneRankingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-direct {p0}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method protected processMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x2

    const/4 v8, 0x3

    const/4 v4, 0x0

    .line 139
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 188
    :goto_0
    return-void

    .line 141
    :pswitch_0
    const-string v2, "REGISTER_NEW_SAMSUNG_TOKEN_MESSAGE"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->sendRequestForNewUserToken()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    goto :goto_0

    .line 145
    :pswitch_1
    const-string v2, "BROADCAST_MESSAGE"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 146
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->sendRequestForUserToken()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$100(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    goto :goto_0

    .line 149
    :pswitch_2
    const-string v2, "LIST_UPDATE_MESSAGE"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "LIST_UPDATE_MESSAGE "

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-static {v4}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->saveLastUpdatedTime(Z)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->checkMyRanking()Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$400(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Z

    move-result v0

    .line 154
    .local v0, "hasMyRanking":Z
    if-eqz v0, :cond_0

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTimeFilter:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTimeFilter:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$600(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mActivity:Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$700(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/WalkingMateRankingActivity;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->displayLastUpdatedTime(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "str":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateTextView:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    .end local v1    # "str":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    new-instance v3, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

    iget-object v4, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->topWalkersList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->myValuesHolder:Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;
    invoke-static {v6}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mDefaultListProfileIcon:[Landroid/graphics/drawable/BitmapDrawable;
    invoke-static {v7}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1100(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)[Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/android/app/shealth/walkingmate/listview/TopWalkersHolder;[Landroid/graphics/drawable/BitmapDrawable;)V

    # setter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mAdapter:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$902(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;)Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mAdapter:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$900(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    iget-object v3, v3, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mItemClickListener:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;->setOnItemClickListener(Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter$OnItemClickListener;)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mTopWalkersListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$600(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Landroid/widget/ListView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mAdapter:Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;
    invoke-static {v3}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$900(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/widget/EveryoneRankingsAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 166
    .end local v0    # "hasMyRanking":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->visibleZeroRankingLayout()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1300(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->sendEmptyMessage(I)Z

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->refreshFragmentFocusables()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1400(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    goto/16 :goto_0

    .line 174
    :pswitch_3
    const-string v2, "UPDATE_MESSAGE_EPIC_FAIL"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->removeMessages(I)V

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->dismissLoadingDialog()V

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->showUpdateFail()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1500(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    goto/16 :goto_0

    .line 181
    :pswitch_4
    const-string v2, "DISMISSDIALOG"

    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/utils/WalkingLog;->D(Ljava/lang/String;)V

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->removeMessages(I)V

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->mUpdateHandler:Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/walkingmate/utils/PauseHandler;->removeMessages(I)V

    .line 184
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->dismissLoadingDialog()V

    .line 185
    iget-object v2, p0, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment$1;->this$0:Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->refreshFragmentFocusables()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;->access$1600(Lcom/sec/android/app/shealth/walkingmate/EveryoneRankingsFragment;)V

    goto/16 :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method protected storeMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 134
    const/4 v0, 0x1

    return v0
.end method
