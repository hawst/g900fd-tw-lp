.class Lcom/sec/android/app/shealth/stress/StressActivity$7;
.super Ljava/lang/Object;
.source "StressActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/stress/StressActivity;->showDeleteDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/stress/StressActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/stress/StressActivity;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcom/sec/android/app/shealth/stress/StressActivity$7;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelButtonClick(Landroid/app/Activity;)V
    .locals 2
    .param p1, "parentActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$7;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/shealth/stress/StressActivity;->isMeasureCompleted:Z

    if-nez v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$7;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # getter for: Lcom/sec/android/app/shealth/stress/StressActivity;->mSummarayFragment:Lcom/sec/android/app/shealth/stress/StressSummaryFragment;
    invoke-static {v0}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$700(Lcom/sec/android/app/shealth/stress/StressActivity;)Lcom/sec/android/app/shealth/stress/StressSummaryFragment;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/stress/StressSummaryFragment;->restartCurrentState(I)V

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/stress/StressActivity$7;->this$0:Lcom/sec/android/app/shealth/stress/StressActivity;

    # setter for: Lcom/sec/android/app/shealth/stress/StressActivity;->isShownPopup:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/stress/StressActivity;->access$802(Lcom/sec/android/app/shealth/stress/StressActivity;Z)Z

    .line 341
    return-void
.end method
