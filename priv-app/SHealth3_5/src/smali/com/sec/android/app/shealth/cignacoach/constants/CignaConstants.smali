.class public Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants;
.super Ljava/lang/Object;
.source "CignaConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DateformatType;,
        Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$DeleteType;,
        Lcom/sec/android/app/shealth/cignacoach/constants/CignaConstants$LIFE_STYLE;
    }
.end annotation


# static fields
.field public static final ACTION_NAME:Ljava/lang/String; = "ACTION_NAME"

.field public static final ACTION_NAME_APPWIDGET_UPDATE:Ljava/lang/String; = "android.appwidget.action.APPWIDGET_UPDATE"

.field public static final ACTION_NAME_CIGNA_EVENT_GOAL_CANCELLED:Ljava/lang/String; = "com.cigna.mobile.coach.GOAL_CANCELLED"

.field public static final ACTION_NAME_CIGNA_EVENT_GOAL_COMPLETE:Ljava/lang/String; = "com.cigna.mobile.coach.GOAL_COMPLETED"

.field public static final ACTION_NAME_CIGNA_EVENT_MISSION_COMPLETED:Ljava/lang/String; = "com.cigna.mobile.coach.MISSION_COMPLETED"

.field public static final ACTION_NAME_CIGNA_EVENT_MISSION_FAILED:Ljava/lang/String; = "com.cigna.mobile.coach.MISSION_FAILED"

.field public static final ACTION_NAME_CIGNA_EVENT_SCORE_CHANGED:Ljava/lang/String; = "com.cigna.mobile.coach.SCORE_CHANGED"

.field public static final ACTION_NAME_COACH_WIDGET_INTENT_DISMISS:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_WIDGET_INTENT_DISMISS"

.field public static final ACTION_NAME_COACH_WIDGET_INVALIDATE:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_WIDGET_INVALIDATE"

.field public static final ACTION_NAME_EVENT_GOAL_COMPLETE:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.GOAL_COMPLETE"

.field public static final ACTION_NAME_EVENT_MISSION_ADD:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.MISSION_ADD"

.field public static final ACTION_NAME_EVENT_MISSION_COMPLETE:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.MISSION_COMPLETE"

.field public static final ACTION_NAME_EVENT_MISSION_PROGRESSED:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.MISSION_PROGRESSED"

.field public static final ACTION_NAME_EVENT_RESET_DATA:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.RESET_DATA"

.field public static final ACTION_NAME_EVENT_RESTORE_SUCCESS:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.RESTORE_SUCCESS"

.field public static final ACTION_NAME_LOCALE_CHANGED:Ljava/lang/String; = "android.intent.action.LOCALE_CHANGED"

.field public static final ACTION_NAME_MODE_CHANGE:Ljava/lang/String; = "com.sec.android.app.shealth.intent.action.MODE_CHANGE"

.field public static final ACTION_NAME_PREVENT_CRASH:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.CIGNA_COACH"

.field public static final ACTION_NAME_RESTART_WIDGET:Ljava/lang/String; = "android.intent.action.RESTART_WIDGET"

.field public static final ACTION_NAME_RESTORE_COMPLETED:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_RESTORE_COMPLETED"

.field public static final ACTION_NAME_RULE_DESTINATION_ASSESSMENT:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.DESTINATION_ASSESSMENT"

.field public static final ACTION_NAME_RULE_DESTINATION_CURRENT_MSSN:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL"

.field public static final ACTION_NAME_RULE_DESTINATION_LIFESTYLE_SCORE:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.DESTINATION_LIFESTYLE_SCORE"

.field public static final ACTION_NAME_RULE_DESTINATION_SUGGEST_GOAL:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.DESTINATION_SUGGEST_GOAL"

.field public static final ACTION_NAME_UPDATE_WIDGET:Ljava/lang/String; = "com.sec.android.app.shealth.intent.action.UPDATE_WIDGET"

.field public static final APRIL:I = 0x3

.field public static final AUGUST:I = 0x7

.field public static final CALENDAR_DATE:I = 0x5

.field public static final CALENDAR_DAY_OF_WEEK:I = 0x7

.field public static final CALENDAR_FRIDAY:I = 0x6

.field public static final CALENDAR_HOUR_OF_DAY:I = 0xb

.field public static final CALENDAR_MILLISECOND:I = 0xe

.field public static final CALENDAR_MINUTE:I = 0xc

.field public static final CALENDAR_MONDAY:I = 0x2

.field public static final CALENDAR_SECOND:I = 0xd

.field public static final CALENDAR_SUNDAY:I = 0x1

.field public static final CALENDAR_THURSDAY:I = 0x5

.field public static final CALENDAR_TUESDAY:I = 0x3

.field public static final CALENDAR_WEDNESDAY:I = 0x4

.field public static final CIGNA_BADGE_ICON_ACTIONHERO:Ljava/lang/String; = "cigna_badge_icon_actionhero"

.field public static final CIGNA_BADGE_ICON_ATHLETE:Ljava/lang/String; = "cigna_badge_icon_athlete"

.field public static final CIGNA_BADGE_ICON_CHAMP:Ljava/lang/String; = "cigna_badge_icon_champ"

.field public static final CIGNA_BADGE_ICON_COACH:Ljava/lang/String; = "cigna_badge_icon_coach"

.field public static final CIGNA_BADGE_ICON_DREAMCATCHER:Ljava/lang/String; = "cigna_badge_icon_dreamcatcher"

.field public static final CIGNA_BADGE_ICON_FITNESSGURU:Ljava/lang/String; = "cigna_badge_icon_fitnessguru"

.field public static final CIGNA_BADGE_ICON_FOODIE:Ljava/lang/String; = "cigna_badge_icon_foodie"

.field public static final CIGNA_BADGE_ICON_FOURSQUARE:Ljava/lang/String; = "cigna_badge_icon_foursquare"

.field public static final CIGNA_BADGE_ICON_GEAREDUP:Ljava/lang/String; = "cigna_badge_icon_gearedup"

.field public static final CIGNA_BADGE_ICON_GOGETTER:Ljava/lang/String; = "cigna_badge_icon_gogetter"

.field public static final CIGNA_BADGE_ICON_HATTRICK:Ljava/lang/String; = "cigna_badge_icon_hattrick"

.field public static final CIGNA_BADGE_ICON_HEALTHNUT:Ljava/lang/String; = "cigna_badge_icon_healthnut"

.field public static final CIGNA_BADGE_ICON_HEAVYWEIGHT:Ljava/lang/String; = "cigna_badge_icon_heavyweight"

.field public static final CIGNA_BADGE_ICON_HIGHFIVE:Ljava/lang/String; = "cigna_badge_icon_highfive"

.field public static final CIGNA_BADGE_ICON_KNOWTHSELF:Ljava/lang/String; = "cigna_badge_icon_knowthyself"

.field public static final CIGNA_BADGE_ICON_KNOWTHYSELF:Ljava/lang/String; = "cigna_badge_icon_knowthyself"

.field public static final CIGNA_BADGE_ICON_LAYER_2296:Ljava/lang/String; = "cigna_badge_icon_layer_2296"

.field public static final CIGNA_BADGE_ICON_LIGHTWEIGHT:Ljava/lang/String; = "cigna_badge_icon_lightweight"

.field public static final CIGNA_BADGE_ICON_LUCKYSEVEN:Ljava/lang/String; = "cigna_badge_icon_luckyseven"

.field public static final CIGNA_BADGE_ICON_MIRROMIRRO:Ljava/lang/String; = "cigna_badge_icon_mirromirro"

.field public static final CIGNA_BADGE_ICON_MIRROR:Ljava/lang/String; = "cigna_badge_icon_mirror"

.field public static final CIGNA_BADGE_ICON_MOVINGTHENEEDLE:Ljava/lang/String; = "cigna_badge_icon_movingtheneedle"

.field public static final CIGNA_BADGE_ICON_OUTOFTHEGATE:Ljava/lang/String; = "cigna_badge_icon_outofthegate"

.field public static final CIGNA_BADGE_ICON_PLATEBALANCER:Ljava/lang/String; = "cigna_badge_icon_platebalancer"

.field public static final CIGNA_BADGE_ICON_RELAXMASTER:Ljava/lang/String; = "cigna_badge_icon_relaxmaster"

.field public static final CIGNA_BADGE_ICON_ROCKSTAR:Ljava/lang/String; = "cigna_badge_icon_rockstar"

.field public static final CIGNA_BADGE_ICON_SIXPACK:Ljava/lang/String; = "cigna_badge_icon_sixpack"

.field public static final CIGNA_BADGE_ICON_SLEEPINGBEAUTY:Ljava/lang/String; = "cigna_badge_icon_sleepingbeauty"

.field public static final CIGNA_BADGE_ICON_SLIMNTRIM:Ljava/lang/String; = "cigna_badge_icon_slimntrim"

.field public static final CIGNA_BADGE_ICON_STICKWITHIT:Ljava/lang/String; = "cigna_badge_icon_stickwithit"

.field public static final CIGNA_BADGE_ICON_SUPERHERO:Ljava/lang/String; = "cigna_badge_icon_polygon"

.field public static final CIGNA_BADGE_ICON_WELLROUNDED:Ljava/lang/String; = "cigna_badge_icon_wellrounded"

.field public static final CIGNA_BADGE_ICON_ZENMASTER:Ljava/lang/String; = "cigna_badge_icon_zenmaster"

.field public static final CIGNA_COACH_MISSION_FIRST_ITEMS:I = 0x5

.field public static final CIGNA_WEB_URL:Ljava/lang/String; = "http://www.cigna.com/cignacoach"

.field public static final CIGNA_WIDGET_REQUEST_CODE_DEFAULT_INTENT:I = 0x98e

.field public static final CIGNA_WIDGET_REQUEST_CODE_MESSAGE_INTENT:I = 0x98e

.field public static final CIGNA_WIDGET_REQUEST_CODE_TITLE_INTENT:I = 0x98e

.field public static final CIGNA_WIDGET_REQUEST_CODE_WEB_INTENT:I = 0x98e

.field public static final COACH_WIDGET:Ljava/lang/String; = "COACH_WIDGET"

.field public static final DATE_FORMAT_KOR:Ljava/lang/String; = "yyyy-MM-dd"

.field public static final EXTRA_NAME_ASSESSMENT_CATEGORY_NOTI:Ljava/lang/String; = "EXTRA_NAME_ASSESSMENT_CATEGORY_NOTI"

.field public static final EXTRA_NAME_BADGE_IDS:Ljava/lang/String; = "EXTRA_NAME_BADGE_IDS"

.field public static final EXTRA_NAME_BADGE_VIEW_MODE:Ljava/lang/String; = "EXTRA_NAME_BADGE_VIEW_MODE"

.field public static final EXTRA_NAME_CANCEL_GOAL_UNALIGNED_DESCRIPTION:Ljava/lang/String; = "intent_cancel_goal_unaligned_description"

.field public static final EXTRA_NAME_CANCEL_GOAL_UNALIGNED_MESSAGE:Ljava/lang/String; = "intent_cancel_goal_unaligned_message"

.field public static final EXTRA_NAME_CATEGORY_TYPE:Ljava/lang/String; = "intent_category_type"

.field public static final EXTRA_NAME_CHANGE_LANGUAGE:Ljava/lang/String; = "EXTRA_NAME_CHANGE_LANGUAGE"

.field public static final EXTRA_NAME_COMPLETE_INFO_DATA:Ljava/lang/String; = "intent_complete_info_data"

.field public static final EXTRA_NAME_DELETE_ALL:Ljava/lang/String; = "intent_delete_all"

.field public static final EXTRA_NAME_DELETE_TYPE:Ljava/lang/String; = "EXTRA_NAME_DELETE_TYPE"

.field public static final EXTRA_NAME_DESTINATION:Ljava/lang/String; = "EXTRA_NAME_DESTINATION"

.field public static final EXTRA_NAME_EXPAND_CHILD_POSITION:Ljava/lang/String; = "EXTRA_NAME_EXPAND_CHILD_POSITION"

.field public static final EXTRA_NAME_EXPAND_GOAL_ID:Ljava/lang/String; = "EXTRA_NAME_EXPAND_GOAL_ID"

.field public static final EXTRA_NAME_EXPAND_GROUP_POSITION:Ljava/lang/String; = "EXTRA_NAME_EXPAND_GROUP_POSITION"

.field public static final EXTRA_NAME_FIRST_ASSESSMENT_COMPLETE_CATEGORY:Ljava/lang/String; = "EXTRA_NAME_FIRST_ASSESSMENT_COMPLETE_CATEGORY"

.field public static final EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN:Ljava/lang/String; = "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

.field public static final EXTRA_NAME_FROM_COACH_MESSAGE:Ljava/lang/String; = "EXTRA_NAME_FROM_COACH_MESSAGE"

.field public static final EXTRA_NAME_FROM_HISTORY:Ljava/lang/String; = "EXTRA_NAME_FROM_HISTORY"

.field public static final EXTRA_NAME_GOAL_DATA:Ljava/lang/String; = "intent_data"

.field public static final EXTRA_NAME_GOAL_ID:Ljava/lang/String; = "EXTRA_NAME_GOAL_ID"

.field public static final EXTRA_NAME_GOAL_LIST_INDEX:Ljava/lang/String; = "intent_extra_index"

.field public static final EXTRA_NAME_LIFESTYLE_CATEGORY:Ljava/lang/String; = "intent_lifestyle_category"

.field public static final EXTRA_NAME_LIFESTYLE_CATEGORYS:Ljava/lang/String; = "intent_lifestyle_categorys"

.field public static final EXTRA_NAME_MESSAGE_ID:Ljava/lang/String; = "EXTRA_NAME_MESSAGE_ID"

.field public static final EXTRA_NAME_MISSIONS_SELECTED_INDEX:Ljava/lang/String; = "intent_extra_index"

.field public static final EXTRA_NAME_MISSION_FROM_WHERE:Ljava/lang/String; = "EXTRA_NAME_MISSION_FROM_WHERE"

.field public static final EXTRA_NAME_MISSION_ID:Ljava/lang/String; = "EXTRA_NAME_MISSION_ID"

.field public static final EXTRA_NAME_MISSION_SEQUENCE_ID:Ljava/lang/String; = "EXTRA_NAME_MISSION_SEQUENCE_ID"

.field public static final EXTRA_NAME_NEED_CIGNA_MAIN_UPDATE:Ljava/lang/String; = "intent_need_cigna_main_update"

.field public static final EXTRA_NAME_NEED_UPATE_MAIN_SCORE_ANIMATION:Ljava/lang/String; = "EXTRA_NAME_NEED_UPATE_MAIN_SCORE_ANIMATION"

.field public static final EXTRA_NAME_REASSESSMENT_EXIST_CURRENT_GOAL:Ljava/lang/String; = "EXTRA_NAME_REASSESSMENT_EXIST_CURRENT_GOAL"

.field public static final EXTRA_NAME_REASSESS_STATE:Ljava/lang/String; = "intent_reassess_state"

.field public static final EXTRA_NAME_SCORE:Ljava/lang/String; = "EXTRA_NAME_SCORE"

.field public static final EXTRA_NAME_SELECTED_MISSION_DATA:Ljava/lang/String; = "EXTRA_NAME_SELECTED_MISSION_DATA"

.field public static final EXTRA_NAME_SORTING_TYPE:Ljava/lang/String; = "EXTRA_NAME_SORTING_TYPE"

.field public static final EXTRA_NAME_SUGGESTED_GOAL_CATEGORY:Ljava/lang/String; = "EXTRA_NAME_SUGGESTED_GOAL_CATEGORY"

.field public static final EXTRA_NAME_TERMS_OF_USE_VERSION:Ljava/lang/String; = "EXTRA_NAME_TERMS_OF_USE_VERSION"

.field public static final FEBRUARY:I = 0x1

.field public static final GRAPH_FONT:Ljava/lang/String; = "font/Roboto-Light.ttf"

.field public static final JANUARY:I = 0x0

.field public static final JULY:I = 0x6

.field public static final JUNE:I = 0x5

.field public static final LAUNCHED_FROM_WIDGET:Ljava/lang/String; = "LAUNCHED_FROM_WIDGET"

.field public static final MARCH:I = 0x2

.field public static final MAX_SUGGESTED_COACHS_LIST_COUNT:I = 0x3

.field public static final MAY:I = 0x4

.field public static final NOVEMBER:I = 0xa

.field public static final OCTOBER:I = 0x9

.field public static final PACKAGE_NAME_COACH:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach"

.field public static final PATTERN_YEAR:Ljava/lang/String; = "yyyy"

.field public static final REASSESSMENT_COMPLETED:I = 0x3

.field public static final REASSESSMENT_GOAL_WARNING:I = 0x1

.field public static final REASSESSMENT_LESS_THAN6:I = 0x0

.field public static final REASSESSMENT_NOW:I = 0x2

.field public static final REQUEST_BADGE_ASSESS_MODE_REWARD:I = 0x115c

.field public static final REQUEST_BADGE_GOAL_MODE_REWARD:I = 0x8ae

.field public static final REQUEST_BADGE_MISSION_MODE_REWARD:I = 0x457

.field public static final REQUEST_BADGE_VIEW_MODE_INFO:I = 0xd05

.field public static final REQUEST_CODE_COACH_MESSAGE:I = 0x320

.field public static final REQUEST_CODE_DELETE_GOAL:I = 0x258

.field public static final REQUEST_CODE_DELETE_MISSION:I = 0x2bc

.field public static final REQUEST_CODE_EXERCISE:I = 0x64

.field public static final REQUEST_CODE_FIRST_TIME_NO_SCORE:I = 0x71

.field public static final REQUEST_CODE_FOOD:I = 0xc8

.field public static final REQUEST_CODE_SLEEP:I = 0x12c

.field public static final REQUEST_CODE_STRESS:I = 0x190

.field public static final REQUEST_CODE_WEIGHT:I = 0x1f4

.field public static final REQUEST_GOAL_DETAIL:I = 0x6e

.field public static final REQUEST_MISSION_DETAIL:I = 0xdc

.field public static final REQUEST_MSSN_FROM_OTHER:I = 0x16

.field public static final REQUEST_MSSN_FROM_SUGGEST_GOAL:I = 0xb

.field public static final REQUEST_NEW_SCORE_SCREEN_AFTER_BADGE_SCREEN:I = 0x2b67

.field public static final REQUEST_SET_RESULT_AFTER_BADGE_SCREEN:I = 0x56ce

.field public static final RESULT_CODE_GOAL_REMOVE_ITEM:I = 0x6f

.field public static final RESULT_CODE_GOAL_UPDATE_LIST:I = 0x70

.field public static final RESULT_CODE_MISSION_REMOVE_ITEM:I = 0xdd

.field public static final RESULT_CODE_MISSION_UPDATE_LIST:I = 0xde

.field public static final REWARD_FINISH_DELAY_TIME:I = 0xfa0

.field public static final SEPTEMBER:I = 0x8

.field public static final SHEALTH_ERASE_STARTED:Ljava/lang/String; = "action_deletion_started"

.field public static final SHEALTH_WIDGET:Ljava/lang/String; = "SHEALTH_WIDGET"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    return-void
.end method
