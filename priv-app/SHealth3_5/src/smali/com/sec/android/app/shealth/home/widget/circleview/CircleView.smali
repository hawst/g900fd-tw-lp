.class public Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;
.super Landroid/view/View;
.source "CircleView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$1;,
        Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;
    }
.end annotation


# instance fields
.field private color:I

.field private endAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

.field private externalRadius:F

.field private innerRadius:F

.field private mPaint:Landroid/graphics/Paint;

.field private mPath:Landroid/graphics/Path;

.field private startAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 33
    sget-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->ZERO:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->startAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    .line 34
    sget-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->ZERO:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->endAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPaint:Landroid/graphics/Paint;

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->initAttributes()V

    .line 42
    return-void
.end method

.method private drawArc(Landroid/graphics/Path;FLcom/sec/android/app/shealth/home/widget/circleview/Angle;Lcom/sec/android/app/shealth/home/widget/circleview/Angle;Z)V
    .locals 4
    .param p1, "path"    # Landroid/graphics/Path;
    .param p2, "radius"    # F
    .param p3, "from"    # Lcom/sec/android/app/shealth/home/widget/circleview/Angle;
    .param p4, "to"    # Lcom/sec/android/app/shealth/home/widget/circleview/Angle;
    .param p5, "forceMoveTo"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p2}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->getRectForCircle(F)Landroid/graphics/RectF;

    move-result-object v0

    iget v1, p3, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->degreeValue:F

    const/high16 v2, 0x42b40000    # 90.0f

    sub-float/2addr v1, v2

    iget v2, p4, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->degreeValue:F

    iget v3, p3, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->degreeValue:F

    sub-float/2addr v2, v3

    invoke-virtual {p1, v0, v1, v2, p5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    .line 74
    return-void
.end method

.method private drawLine(Landroid/graphics/Path;Lcom/sec/android/app/shealth/home/widget/circleview/Angle;F)V
    .locals 4
    .param p1, "path"    # Landroid/graphics/Path;
    .param p2, "angle"    # Lcom/sec/android/app/shealth/home/widget/circleview/Angle;
    .param p3, "radius"    # F

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    iget v1, p2, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->radianValue:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    iget v2, p2, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->radianValue:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v2, p3

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 86
    return-void
.end method

.method private getRectForCircle(F)Landroid/graphics/RectF;
    .locals 4
    .param p1, "radius"    # F

    .prologue
    .line 77
    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    sub-float v0, v2, p1

    .line 78
    .local v0, "marginLT":F
    const/high16 v2, 0x40000000    # 2.0f

    iget v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    mul-float/2addr v2, v3

    sub-float v1, v2, v0

    .line 79
    .local v1, "marginRB":F
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v0, v0, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2
.end method

.method private initAttributes()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    .line 46
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPaint:Landroid/graphics/Paint;

    .line 47
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v5, 0x1

    .line 51
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->endAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    iget v0, v0, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->degreeValue:F

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->startAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    iget v1, v1, Lcom/sec/android/app/shealth/home/widget/circleview/Angle;->degreeValue:F

    sub-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->innerRadius:F

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->startAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->endAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->drawArc(Landroid/graphics/Path;FLcom/sec/android/app/shealth/home/widget/circleview/Angle;Lcom/sec/android/app/shealth/home/widget/circleview/Angle;Z)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->endAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->drawLine(Landroid/graphics/Path;Lcom/sec/android/app/shealth/home/widget/circleview/Angle;F)V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    iget-object v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->endAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    iget-object v4, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->startAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->drawArc(Landroid/graphics/Path;FLcom/sec/android/app/shealth/home/widget/circleview/Angle;Lcom/sec/android/app/shealth/home/widget/circleview/Angle;Z)V

    .line 66
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 67
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    iget v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->innerRadius:F

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->mPath:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    iget v3, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    sget-object v4, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->color:I

    .line 114
    return-void
.end method

.method public setEndAngle(F)V
    .locals 1
    .param p1, "endAngle"    # F

    .prologue
    .line 93
    sget-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->DEGREES:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setEndAngle(FLcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;)V

    .line 94
    return-void
.end method

.method public setEndAngle(FLcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;)V
    .locals 1
    .param p1, "endAngle"    # F
    .param p2, "angleUnits"    # Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    .prologue
    .line 101
    invoke-virtual {p2, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->getAngle(F)Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->endAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    .line 102
    return-void
.end method

.method public setExternalRadius(F)V
    .locals 0
    .param p1, "externalRadius"    # F

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->externalRadius:F

    .line 106
    return-void
.end method

.method public setInnerRadius(F)V
    .locals 0
    .param p1, "innerRadius"    # F

    .prologue
    .line 109
    iput p1, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->innerRadius:F

    .line 110
    return-void
.end method

.method public setStartAngle(F)V
    .locals 1
    .param p1, "startAngle"    # F

    .prologue
    .line 89
    sget-object v0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->DEGREES:Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->setStartAngle(FLcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;)V

    .line 90
    return-void
.end method

.method public setStartAngle(FLcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;)V
    .locals 1
    .param p1, "startAngle"    # F
    .param p2, "angleUnits"    # Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;

    .prologue
    .line 97
    invoke-virtual {p2, p1}, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView$AngleUnits;->getAngle(F)Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/widget/circleview/CircleView;->startAngle:Lcom/sec/android/app/shealth/home/widget/circleview/Angle;

    .line 98
    return-void
.end method
