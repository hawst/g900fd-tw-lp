.class Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$4;
.super Ljava/lang/Object;
.source "CignaSummaryCategoryScoreView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V
    .locals 0

    .prologue
    .line 542
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 560
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 553
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreAnimationLayout:Landroid/widget/FrameLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->access$400(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->mScoreLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->access$500(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView$4;->this$0:Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;

    # invokes: Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->updateScoreView()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;->access$600(Lcom/sec/android/app/shealth/cignacoach/widget/CignaSummaryCategoryScoreView;)V

    .line 556
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 550
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 546
    return-void
.end method
