.class Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$2;
.super Ljava/lang/Object;
.source "GeneralMealActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)V
    .locals 0

    .prologue
    .line 547
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 552
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 554
    check-cast v0, Landroid/widget/TextView;

    .line 556
    .local v0, "textView":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity$2;->this$0:Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->mMaxAcceptableCaloriesValue:F
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;->access$500(Lcom/sec/android/app/shealth/food/mealinput/GeneralMealActivity;)F

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/food/utils/FoodPluginMealUtils;->prepareTextForOutOfRangePouup(Landroid/content/Context;F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 561
    .end local v0    # "textView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method
