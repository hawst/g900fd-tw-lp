.class public abstract Lcom/sec/android/app/shealth/common/HealthCarePluginsApp;
.super Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
.source "HealthCarePluginsApp.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;-><init>()V

    return-void
.end method


# virtual methods
.method public getCurrentMeasureType()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v0, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/HealthCarePluginsApp;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    return-object v0
.end method

.method protected abstract getTag()Ljava/lang/String;
.end method

.method protected abstract getType()I
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/common/HealthCarePluginsApp;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-super {p0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->onCreate()V

    .line 56
    return-void
.end method

.method public onDataStopped(Ljava/lang/String;II)V
    .locals 0
    .param p1, "mSensorDeviceId"    # Ljava/lang/String;
    .param p2, "dataType"    # I
    .param p3, "error"    # I

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/common/HealthCarePluginsApp;->resumeDataReceiving(Ljava/lang/String;)Z

    .line 86
    return-void
.end method

.method public onDeviceJoined(Ljava/lang/String;I)V
    .locals 0
    .param p1, "uSensorDeviceId"    # Ljava/lang/String;
    .param p2, "error"    # I

    .prologue
    .line 91
    return-void
.end method

.method public onDeviceLeft(Ljava/lang/String;I)V
    .locals 0
    .param p1, "uSensorDeviceId"    # Ljava/lang/String;
    .param p2, "error"    # I

    .prologue
    .line 96
    return-void
.end method
