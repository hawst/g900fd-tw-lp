.class Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$9;
.super Ljava/lang/Object;
.source "EditProfileActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->getOnSaveChosenItemListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/OnSaveChosenItemListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V
    .locals 0

    .prologue
    .line 1498
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSave(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1502
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1504
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->doTakePhotoAction()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$2300(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    .line 1506
    :cond_0
    if-nez p1, :cond_1

    .line 1508
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->doTakeAlbumAction()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$2400(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    .line 1510
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1512
    iget-object v0, p0, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity$9;->this$0:Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;

    # invokes: Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->doDeletePhotoAction()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;->access$2500(Lcom/sec/android/app/shealth/settings/userprofile/EditProfileActivity;)V

    .line 1514
    :cond_2
    return-void
.end method
