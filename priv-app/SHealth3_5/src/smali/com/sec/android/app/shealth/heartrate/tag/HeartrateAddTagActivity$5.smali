.class Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;
.super Ljava/lang/Object;
.source "HeartrateAddTagActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 308
    .local v0, "mgr":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 309
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1200(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1300(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->clearFocus()V

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1400(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setCursorVisible(Z)V

    .line 313
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    # setter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I
    invoke-static {v3, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1502(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;I)I

    .line 314
    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getmSelectedIconPos()I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconNameId(I)I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 315
    .local v1, "notSelectedTag":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getmSelectedIconPos()I
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1600(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    const v5, 0x7f09110a

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # invokes: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->setmSelectedIconPos(I)V
    invoke-static {v2, p3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1700(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;I)V

    .line 317
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Landroid/widget/ImageView;

    move-result-object v2

    const v3, 0x7f0204cb

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 318
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconResourceId(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 319
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mSelectedTagIcon:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1800(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconNameId(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 320
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    iget-object v5, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I
    invoke-static {v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v5

    invoke-static {v5}, Lcom/sec/android/app/shealth/heartrate/utils/HeartrateTagUtils;->getIconNameId(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    const v5, 0x7f091109

    invoke-virtual {v4, v5}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mMemoView:Lcom/sec/android/app/shealth/framework/ui/common/MemoView;
    invoke-static {v2}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1900(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)Lcom/sec/android/app/shealth/framework/ui/common/MemoView;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/framework/ui/common/MemoView;->setCursorVisible(Z)V

    .line 322
    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$2000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Icon("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity$5;->this$0:Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;

    # getter for: Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->mTagIconId:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;->access$1500(Lcom/sec/android/app/shealth/heartrate/tag/HeartrateAddTagActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") is selected."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    return-void
.end method
