.class Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;
.super Ljava/lang/Object;
.source "SpO2Activity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showInfomationDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0

    .prologue
    .line 583
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
    .locals 3
    .param p1, "content"    # Landroid/view/View;
    .param p2, "parentActivity"    # Landroid/app/Activity;
    .param p3, "dialog"    # Landroid/app/Dialog;
    .param p4, "savedInstanceState"    # Landroid/os/Bundle;
    .param p5, "okButtonHandler"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;

    .prologue
    .line 589
    const/4 v0, 0x0

    .line 590
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/widget/CheckBox;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 591
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 593
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    const v1, 0x7f080998

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->slidingimage:Landroid/widget/ImageView;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$1002(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 594
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    const v1, 0x7f0804ef

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$902(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;

    .line 595
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 596
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgain:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$900(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/widget/CheckBox;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 597
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    const v1, 0x7f0804ee

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    # setter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgainCheckLayout:Landroid/widget/LinearLayout;
    invoke-static {v2, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$1102(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 598
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # getter for: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgainCheckLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$1100(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    iget-object v2, v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;->this$0:Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    # invokes: Lcom/sec/android/app/shealth/spo2/SpO2Activity;->initInfoSlideShow()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->access$1200(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    .line 601
    return-void
.end method
