.class public abstract Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;
.super Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;
.source "TGHSummaryView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    }
.end annotation


# static fields
.field private static final BALANCE_STEP_FLOAT:F = 0.1f

.field private static final BALANCE_STEP_INT:I = 0x1


# instance fields
.field protected contentView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;

.field protected headerView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

.field private mHeaderBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/common/commonui/summary/SummaryContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;)Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->mHeaderBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    return-object v0
.end method


# virtual methods
.method protected abstract createContentView()Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;
.end method

.method protected abstract createHeaderView()Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;
.end method

.method protected getBackgroundColor()I
    .locals 2

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070057

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method protected initializeContentView()V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->setBackgroundColor(I)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->createHeaderView()Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->headerView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->headerView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->addView(Landroid/view/View;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->createContentView()Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->contentView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->contentView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;

    new-instance v1, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$1;-><init>(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->setAnimatorListener(Lcom/sec/android/app/shealth/common/commonui/summary/StubAnimatorListener;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->contentView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->addView(Landroid/view/View;)V

    .line 66
    return-void
.end method

.method protected updateHeaderViewBalanceState(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;Z)V
    .locals 2
    .param p1, "balanceState"    # Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;
    .param p2, "update"    # Z

    .prologue
    .line 75
    if-eqz p2, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->headerView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->stopBalanceAnim()V

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->mHeaderBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    .line 82
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->headerView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->mHeaderBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->refreshBalanceState(Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;)V

    .line 83
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->headerView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHHeaderView;->startBalanceAnim()V

    .line 80
    sget-object v0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;->CHECKING_TEMPERATURE_HUMIDITY:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    iput-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->mHeaderBalanceState:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView$BalanceState;

    goto :goto_0
.end method

.method protected updateHealthCareContentView(FFZZ)V
    .locals 1
    .param p1, "valueLeft"    # F
    .param p2, "valueRight"    # F
    .param p3, "updateLeft"    # Z
    .param p4, "updateRight"    # Z

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->contentView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->setValues(FFZZ)V

    .line 94
    return-void
.end method

.method protected updateHealthCareContentView(IIZZ)V
    .locals 1
    .param p1, "valueLeft"    # I
    .param p2, "valueRight"    # I
    .param p3, "updateLeft"    # Z
    .param p4, "updateRight"    # Z

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHSummaryView;->contentView:Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/shealth/thermohygrometer/summary/views/TGHContentView;->setValues(IIZZ)V

    .line 105
    return-void
.end method
