.class public Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;
.super Ljava/lang/Object;
.source "MealDataHolder.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;",
            ">;"
        }
    .end annotation
.end field

.field public static final EMPTY_FIELD:J = -0x8000000000000000L

.field private static final LIST_POSSIBLE_MEAL_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final MINIMAL_MEAL_TIME:J


# instance fields
.field private mMealId:J

.field private mMealTime:J

.field private mMealType:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    const/16 v0, 0x76c

    invoke-static {v0, v1, v1}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance(III)Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->MINIMAL_MEAL_TIME:J

    .line 37
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->LIST_POSSIBLE_MEAL_TYPES:Ljava/util/List;

    .line 148
    new-instance v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder$2;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder$2;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/high16 v0, -0x8000000000000000L

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealId:J

    .line 49
    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealType:J

    .line 50
    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealTime:J

    .line 51
    return-void
.end method

.method public constructor <init>(JJJ)V
    .locals 0
    .param p1, "mealId"    # J
    .param p3, "mealType"    # J
    .param p5, "mealTime"    # J

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealId(J)V

    .line 59
    invoke-virtual {p0, p3, p4}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealType(J)V

    .line 60
    invoke-virtual {p0, p5, p6}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->setMealTime(J)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->readFromParcel(Landroid/os/Parcel;)V

    .line 163
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealId:J

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealType:J

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealTime:J

    .line 147
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public getMealId()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealId:J

    return-wide v0
.end method

.method public getMealTime()J
    .locals 2

    .prologue
    .line 111
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealTime:J

    return-wide v0
.end method

.method public getMealType()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealType:J

    return-wide v0
.end method

.method public setMealId(J)V
    .locals 3
    .param p1, "mealId"    # J

    .prologue
    .line 78
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mealId must be positive or EMPTY_FIELD, while actual is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealId:J

    .line 83
    return-void
.end method

.method public setMealTime(J)V
    .locals 3
    .param p1, "mealTime"    # J

    .prologue
    .line 121
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    sget-wide v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->MINIMAL_MEAL_TIME:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mealTime must be positive or EMPTY_FIELD, while actual is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealTime:J

    .line 126
    return-void
.end method

.method public setMealType(J)V
    .locals 3
    .param p1, "mealType"    # J

    .prologue
    .line 99
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->LIST_POSSIBLE_MEAL_TYPES:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "mealType must be one of constants from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealType;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", or EMPTY_FIELD, while actual is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_0
    iput-wide p1, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealType:J

    .line 105
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 136
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealType:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 137
    iget-wide v0, p0, Lcom/sec/android/app/shealth/food/mealinput/MealDataHolder;->mMealTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 138
    return-void
.end method
