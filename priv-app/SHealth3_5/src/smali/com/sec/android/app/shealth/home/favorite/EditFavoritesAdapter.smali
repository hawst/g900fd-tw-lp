.class public Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;
.super Landroid/widget/BaseAdapter;
.source "EditFavoritesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/home/data/IconData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "color"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/shealth/home/data/IconData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/shealth/home/data/IconData;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mContext:Landroid/content/Context;

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    .line 55
    iput p3, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mColor:I

    .line 56
    return-void
.end method

.method private getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 144
    const/4 v0, 0x0

    .line 145
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 147
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.shealth.action.STEALTH_MODE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 148
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 149
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    :try_start_0
    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getActivityIcon(Landroid/content/Intent;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 158
    :cond_0
    :goto_0
    return-object v0

    .line 152
    :catch_0
    move-exception v1

    .line 153
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02020b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private getAppIconDrawable(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 126
    const/4 v0, 0x0

    .line 129
    .local v0, "appIcon":Landroid/graphics/drawable/Drawable;
    const-string v3, "home"

    const-string v4, "favorite"

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 132
    .local v2, "favIconString":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, p1}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/app/shealth/common/utils/ResourceUtil;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/shealth/common/utils/ResourceUtil;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    .line 133
    :catch_0
    move-exception v1

    .line 134
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    if-nez v0, :cond_0

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02020b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 137
    :cond_0
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 74
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "pos"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 82
    if-nez p2, :cond_0

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300b0

    invoke-virtual {v1, v2, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 84
    new-instance v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;-><init>()V

    .line 85
    .local v0, "viewHolder":Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;
    const v1, 0x7f08033b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->imageContainer:Landroid/widget/RelativeLayout;

    .line 86
    const v1, 0x7f08033c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconImage:Landroid/widget/ImageView;

    .line 87
    const v1, 0x7f08033f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconText:Landroid/widget/TextView;

    .line 88
    const v1, 0x7f08033d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->thirdIcon:Landroid/widget/ImageView;

    .line 89
    const v1, 0x7f08033e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->checkIcon:Landroid/widget/CheckBox;

    .line 90
    iget-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mColor:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 92
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 97
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppDisplayPlugInIcons()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppPluginId()I

    move-result v1

    if-lez v1, :cond_1

    .line 100
    iget-object v2, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppDisplayPlugInIcons()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v1}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->getAppIconDrawable(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 108
    :goto_1
    iget-object v2, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getAppType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    .line 111
    iget-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->thirdIcon:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getIsSelected()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 117
    iget-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->checkIcon:Landroid/widget/CheckBox;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 122
    :goto_3
    return-object p2

    .line 94
    .end local v0    # "viewHolder":Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;

    .restart local v0    # "viewHolder":Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;
    goto :goto_0

    .line 102
    :cond_1
    iget-object v2, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 105
    :cond_2
    iget-object v2, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->iconImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/home/data/IconData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/home/data/IconData;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter;->getAppIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 113
    :cond_3
    iget-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->thirdIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 119
    :cond_4
    iget-object v1, v0, Lcom/sec/android/app/shealth/home/favorite/EditFavoritesAdapter$ViewHolder;->checkIcon:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_3
.end method
