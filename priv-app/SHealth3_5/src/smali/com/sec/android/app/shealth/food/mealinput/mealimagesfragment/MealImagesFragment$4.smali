.class Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$4;
.super Ljava/lang/Object;
.source "MealImagesFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImageGridView$OnGridItemEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGridItemClick(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;)V
    .locals 6
    .param p1, "imagePagerHolder"    # Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    .prologue
    .line 172
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/sec/android/app/shealth/common/gallery/GalleryActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 173
    .local v2, "intent":Landroid/content/Intent;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v1, "imagePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    # getter for: Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->mMealImagesMap:Ljava/util/Map;
    invoke-static {v4}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->access$800(Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;

    .line 175
    .local v3, "pagerHolder":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    iget-object v4, v3, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 177
    .end local v3    # "pagerHolder":Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;
    :cond_0
    const-string v4, "gallery_image_paths"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 178
    const-string v4, "gallery_selected_image_path"

    iget-object v5, p1, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/ImagePagerHolder;->imagePath:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    iget-object v4, p0, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment$4;->this$0:Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/shealth/food/mealinput/mealimagesfragment/MealImagesFragment;->startActivity(Landroid/content/Intent;)V

    .line 180
    return-void
.end method
