.class Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;
.super Ljava/lang/Object;
.source "WeightSetGoalCaloriesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->initFooterButtons(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    # invokes: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->isInputValid()Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->access$000(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    # invokes: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->saveData()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->access$100(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth.weight"

    const-string v2, "3001"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->showWeightWithGoalFragment()V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment$2;->this$0:Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;

    # invokes: Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->showAlertDialog()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;->access$200(Lcom/sec/android/app/shealth/weight/goal/WeightSetGoalCaloriesFragment;)V

    goto :goto_0
.end method
