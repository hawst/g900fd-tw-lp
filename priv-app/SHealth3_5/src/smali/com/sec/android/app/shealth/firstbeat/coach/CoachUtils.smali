.class public Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;
.super Ljava/lang/Object;
.source "CoachUtils.java"


# static fields
.field public static final GOAL_IMPROVE:I = 0x2

.field public static final GOAL_MAINTAIN:I = 0x1

.field public static final GOAL_NOT_SET:I = 0x0

.field private static final ONE_DAY:J = 0x5265c00L

.field private static final TAG:Ljava/lang/String; = "Coach"

.field private static mContext:Landroid/content/Context;

.field private static mListTrainingProgramWorkout:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/shealth/firstbeat/coach/TrainingProgramWorkoutEx;",
            ">;"
        }
    .end annotation
.end field

.field private static mRealtimeCoachUtils:Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;

.field private static mTrainingProgramWorkout:Lfi/firstbeat/coach/eteintegration/TrainingProgramWorkout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;->mRealtimeCoachUtils:Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;

    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGregorianCalendarString(Ljava/util/GregorianCalendar;)Ljava/lang/String;
    .locals 4
    .param p0, "gc"    # Ljava/util/GregorianCalendar;

    .prologue
    .line 671
    if-nez p0, :cond_0

    .line 672
    const-string/jumbo v0, "null"

    .line 673
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy.MM.dd HH:mm:ss.SSS"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/sql/Date;

    invoke-virtual {p0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    sget-object v1, Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;->mRealtimeCoachUtils:Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;

    monitor-enter v1

    .line 67
    :try_start_0
    sput-object p0, Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;->mContext:Landroid/content/Context;

    .line 68
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    sget-object v0, Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;->mRealtimeCoachUtils:Lcom/sec/android/app/shealth/firstbeat/coach/CoachUtils;

    return-object v0

    .line 68
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
