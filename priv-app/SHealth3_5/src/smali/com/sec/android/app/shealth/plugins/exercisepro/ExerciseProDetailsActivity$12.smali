.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;
.super Ljava/lang/Object;
.source "ExerciseProDetailsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V
    .locals 0

    .prologue
    .line 670
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 674
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEndMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$1500(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/utils/ExerciseUtilsEx;->clearTempData(Landroid/content/Context;)V

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->setResult(I)V

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->finish()V

    .line 681
    :cond_0
    :goto_0
    return-void

    .line 678
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->mEditMode:Z
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$1300(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity$12;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->checkChangesAndExit()V
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;->access$1700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProDetailsActivity;)V

    goto :goto_0
.end method
