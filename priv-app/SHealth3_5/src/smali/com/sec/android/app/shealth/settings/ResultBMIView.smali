.class public Lcom/sec/android/app/shealth/settings/ResultBMIView;
.super Landroid/widget/RelativeLayout;
.source "ResultBMIView.java"


# instance fields
.field private mBmiLine:Landroid/view/View;

.field private mBmiValue:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/shealth/settings/ResultBMIView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mContext:Landroid/content/Context;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiValue:Landroid/widget/TextView;

    .line 47
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mContext:Landroid/content/Context;

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/shealth/settings/ResultBMIView;->initLayout()V

    .line 50
    return-void
.end method

.method private initLayout()V
    .locals 3

    .prologue
    .line 53
    iget-object v1, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mContext:Landroid/content/Context;

    const v2, 0x7f03001a

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 54
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f080097

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiValue:Landroid/widget/TextView;

    .line 55
    const v1, 0x7f080096

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiLine:Landroid/view/View;

    .line 56
    return-void
.end method


# virtual methods
.method public setBmiLine(Ljava/lang/String;)V
    .locals 14
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const-wide v12, 0x4032800000000000L    # 18.5

    const/high16 v11, 0x41f00000    # 30.0f

    const/high16 v10, 0x41c80000    # 25.0f

    .line 74
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    .line 76
    .local v0, "bmi":F
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResultBMIView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0597

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 77
    .local v6, "width":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResultBMIView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0598

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 79
    .local v5, "maxWidth":I
    const/4 v2, 0x0

    .line 80
    .local v2, "index":I
    const/4 v4, 0x0

    .line 81
    .local v4, "max":F
    const/4 v1, 0x0

    .line 83
    .local v1, "currentWidth":F
    float-to-double v8, v0

    cmpg-double v8, v8, v12

    if-gtz v8, :cond_1

    .line 84
    const/4 v2, 0x0

    .line 85
    const/high16 v4, 0x41940000    # 18.5f

    .line 100
    :cond_0
    :goto_0
    invoke-static {v0, v4, v6}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->getPercentageWidth(FFI)F

    move-result v7

    .line 101
    .local v7, "x":F
    mul-int v8, v6, v2

    int-to-float v8, v8

    add-float v1, v7, v8

    .line 103
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v3, v8, Landroid/util/DisplayMetrics;->density:F

    .line 105
    .local v3, "mDensity":F
    int-to-float v8, v5

    cmpl-float v8, v1, v8

    if-ltz v8, :cond_4

    .line 106
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiLine:Landroid/view/View;

    int-to-float v9, v5

    const/high16 v10, 0x3f800000    # 1.0f

    mul-float/2addr v10, v3

    sub-float/2addr v9, v10

    invoke-virtual {v8, v9}, Landroid/view/View;->setX(F)V

    .line 111
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiValue:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResultBMIView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v1, p1, v8, v9}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->setBmiValueLocate(FLjava/lang/String;Landroid/widget/TextView;Landroid/content/res/Resources;)Landroid/widget/TextView;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiValue:Landroid/widget/TextView;

    .line 112
    return-void

    .line 86
    .end local v3    # "mDensity":F
    .end local v7    # "x":F
    :cond_1
    float-to-double v8, v0

    cmpl-double v8, v8, v12

    if-lez v8, :cond_2

    cmpg-float v8, v0, v10

    if-gtz v8, :cond_2

    .line 87
    const/4 v2, 0x1

    .line 88
    const/high16 v4, 0x40d00000    # 6.5f

    .line 89
    const/high16 v8, 0x41940000    # 18.5f

    sub-float/2addr v0, v8

    goto :goto_0

    .line 90
    :cond_2
    cmpl-float v8, v0, v10

    if-lez v8, :cond_3

    cmpg-float v8, v0, v11

    if-gtz v8, :cond_3

    .line 91
    const/4 v2, 0x2

    .line 92
    const/high16 v4, 0x40a00000    # 5.0f

    .line 93
    sub-float/2addr v0, v10

    goto :goto_0

    .line 94
    :cond_3
    cmpl-float v8, v0, v11

    if-lez v8, :cond_0

    .line 95
    const/4 v2, 0x3

    .line 96
    const/high16 v4, 0x41200000    # 10.0f

    .line 97
    sub-float/2addr v0, v11

    goto :goto_0

    .line 108
    .restart local v3    # "mDensity":F
    .restart local v7    # "x":F
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiLine:Landroid/view/View;

    mul-int v9, v6, v2

    int-to-float v9, v9

    add-float/2addr v9, v7

    invoke-virtual {v8, v9}, Landroid/view/View;->setX(F)V

    goto :goto_1
.end method

.method public setBmiValue(FF)V
    .locals 9
    .param p1, "weight"    # F
    .param p2, "height"    # F

    .prologue
    .line 59
    float-to-double v3, p1

    float-to-double v5, p2

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    div-double/2addr v3, v5

    const-wide v5, 0x40c3880000000000L    # 10000.0

    mul-double v0, v3, v5

    .line 60
    .local v0, "result":D
    double-to-float v3, v0

    invoke-static {v3}, Lcom/sec/android/app/shealth/settings/userprofile/ProfileUtils;->convertDecimalFormat(F)Ljava/lang/String;

    move-result-object v2

    .line 62
    .local v2, "value":Ljava/lang/String;
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 63
    const-string v2, "0"

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiValue:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/settings/ResultBMIView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090201

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/settings/ResultBMIView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 70
    return-void

    .line 66
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/ResultBMIView;->mBmiValue:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/settings/ResultBMIView;->setBmiLine(Ljava/lang/String;)V

    goto :goto_0
.end method
