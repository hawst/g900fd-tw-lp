.class public Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;
.super Landroid/content/BroadcastReceiver;
.source "RealtimeNotification.java"


# static fields
.field public static final FLAG_PROCESSED:I = 0x2

.field public static final FLAG_PROCESSING:I = 0x1

.field public static final MASK_FLAG:I = 0xf

.field public static final MAX_PROCESSED_NOTIFICATON_COUNT:I = 0x32

.field public static final PROCESSING_NOTIFICATON_ID:I = 0xc8

.field public static final RECORDED_INDOOR:I = 0x42

.field public static final RECORDED_OUTDOOR:I = 0x102

.field public static final RECORDING_INDOOR:I = 0x21

.field public static final RECORDING_OUTDOOR:I = 0x81

.field public static final SEARCHING_INDOOR:I = 0x11

.field private static builder:Landroid/support/v4/app/NotificationCompat$Builder;

.field private static mIntent:Landroid/content/Intent;

.field private static mLastNotiID:I

.field private static mProcessedNotiMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mode:I

.field private static notificationId:I

.field private static pendingIntentMain:Landroid/app/PendingIntent;

.field private static result:Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

.field private static tickerMsg:Ljava/lang/String;

.field private static titleMsg:Ljava/lang/String;

.field private static txtMsg:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mLastNotiID:I

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mProcessedNotiMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static closeNotification(I)V
    .locals 3
    .param p0, "id"    # I

    .prologue
    .line 310
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 311
    .local v0, "localContext":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 316
    :goto_0
    return-void

    .line 313
    :cond_0
    const-string/jumbo v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 315
    .local v1, "mNotificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v1, p0}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method private static getActivityTypeTitle(ILandroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "activity_type"    # I
    .param p1, "localContext"    # Landroid/content/Context;

    .prologue
    .line 263
    packed-switch p0, :pswitch_data_0

    .line 277
    :goto_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    return-object v0

    .line 265
    :pswitch_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    goto :goto_0

    .line 268
    :pswitch_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    goto :goto_0

    .line 271
    :pswitch_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    goto :goto_0

    .line 274
    :pswitch_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0909d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    goto :goto_0

    .line 263
    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getIconAsPerActivityType(ILandroid/content/Context;)I
    .locals 1
    .param p0, "activity_type"    # I
    .param p1, "localContext"    # Landroid/content/Context;

    .prologue
    .line 244
    const v0, 0x7f02060b

    .line 245
    .local v0, "icon":I
    packed-switch p0, :pswitch_data_0

    .line 259
    :goto_0
    return v0

    .line 247
    :pswitch_0
    const v0, 0x7f02060b

    .line 248
    goto :goto_0

    .line 250
    :pswitch_1
    const v0, 0x7f020610

    .line 251
    goto :goto_0

    .line 253
    :pswitch_2
    const v0, 0x7f02060a

    .line 254
    goto :goto_0

    .line 256
    :pswitch_3
    const v0, 0x7f02060c

    goto :goto_0

    .line 245
    nop

    :pswitch_data_0
    .packed-switch 0x4652
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getNotification(IJLandroid/app/PendingIntent;Landroid/net/Uri;Landroid/content/Context;ZLcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/Notification;
    .locals 10
    .param p0, "mode"    # I
    .param p1, "when"    # J
    .param p3, "pendingIntent"    # Landroid/app/PendingIntent;
    .param p4, "soundUri"    # Landroid/net/Uri;
    .param p5, "localContext"    # Landroid/content/Context;
    .param p6, "isNewPendingintent"    # Z
    .param p7, "result"    # Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    .prologue
    .line 205
    and-int/lit8 v8, p0, 0xf

    const/4 v9, 0x1

    if-ne v8, v9, :cond_3

    .line 206
    if-eqz p6, :cond_0

    .line 207
    invoke-static {p5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getPendingForProcessing(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object p3

    .line 208
    :cond_0
    const v3, 0x20001002

    .line 210
    .local v3, "flags":I
    const/4 v1, 0x0

    .line 211
    .local v1, "autoCancel":Z
    const/16 v8, 0xc8

    sput v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->notificationId:I

    .line 212
    sget v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->notificationId:I

    invoke-static {v8}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->closeNotification(I)V

    .line 221
    :goto_0
    sput-object p3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->pendingIntentMain:Landroid/app/PendingIntent;

    .line 222
    const v4, 0x7f020560

    .line 224
    .local v4, "icon":I
    if-eqz p7, :cond_5

    move-object/from16 v0, p7

    iget v8, v0, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    invoke-static {v8, p5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getIconAsPerActivityType(ILandroid/content/Context;)I

    move-result v2

    .line 225
    .local v2, "bigIcon":I
    :goto_1
    invoke-virtual {p5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v8, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 226
    .local v5, "largeIcon":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v8, p5}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->tickerMsg:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8, p3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->txtMsg:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    sput-object v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->builder:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 229
    if-eqz p4, :cond_1

    .line 230
    sget-object v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->builder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v8, p4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSound(Landroid/net/Uri;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 233
    :cond_1
    sget-object v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->builder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    .line 234
    .local v7, "notification":Landroid/app/Notification;
    iget v8, v7, Landroid/app/Notification;->flags:I

    or-int/2addr v8, v3

    iput v8, v7, Landroid/app/Notification;->flags:I

    .line 235
    const-string/jumbo v8, "notification"

    invoke-virtual {p5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    .line 237
    .local v6, "mNotificationManager":Landroid/app/NotificationManager;
    sget v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->notificationId:I

    invoke-virtual {v6, v8, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 238
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x15

    if-lt v8, v9, :cond_2

    .line 239
    const/4 v8, 0x1

    iput v8, v7, Landroid/app/Notification;->visibility:I

    .line 240
    :cond_2
    return-object v7

    .line 214
    .end local v1    # "autoCancel":Z
    .end local v2    # "bigIcon":I
    .end local v3    # "flags":I
    .end local v4    # "icon":I
    .end local v5    # "largeIcon":Landroid/graphics/Bitmap;
    .end local v6    # "mNotificationManager":Landroid/app/NotificationManager;
    .end local v7    # "notification":Landroid/app/Notification;
    :cond_3
    if-eqz p6, :cond_4

    .line 215
    move-object/from16 v0, p7

    invoke-static {p5, p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getPendingForRecorded(Landroid/content/Context;ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/PendingIntent;

    move-result-object p3

    .line 216
    :cond_4
    const v3, 0x24001010

    .line 218
    .restart local v3    # "flags":I
    const/4 v1, 0x1

    .line 219
    .restart local v1    # "autoCancel":Z
    invoke-static {p1, p2}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getNotificationId(J)I

    move-result v8

    sput v8, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->notificationId:I

    goto :goto_0

    .line 224
    .restart local v4    # "icon":I
    :cond_5
    invoke-static {}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getRealtimeActivityType()I

    move-result v8

    invoke-static {v8, p5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getIconAsPerActivityType(ILandroid/content/Context;)I

    move-result v2

    goto :goto_1
.end method

.method public static getNotificationId(J)I
    .locals 4
    .param p0, "recordingTime"    # J

    .prologue
    .line 291
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mProcessedNotiMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 292
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mProcessedNotiMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 300
    .local v0, "notificationId":I
    :cond_0
    :goto_0
    return v0

    .line 294
    .end local v0    # "notificationId":I
    :cond_1
    sget v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mLastNotiID:I

    .line 295
    .restart local v0    # "notificationId":I
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mProcessedNotiMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget v3, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mLastNotiID:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mLastNotiID:I

    add-int/lit8 v2, v1, 0x1

    sput v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mLastNotiID:I

    const/16 v2, 0x32

    if-le v1, v2, :cond_0

    .line 297
    const/4 v1, 0x0

    sput v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mLastNotiID:I

    goto :goto_0
.end method

.method private static getPendingForProcessing(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 73
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "widgetActivityAction"

    const-string v3, "com.sec.shealth.action.EXERCISE"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const-string/jumbo v2, "widgetActivityPackage"

    const-string v3, "com.sec.android.app.shealth"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 76
    invoke-static {p0}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->isHomeActivityRunning(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    const-string v2, "RealtimeNotification"

    const-string v3, "getPendingForProcessing"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const-string v2, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    const/16 v2, 0xf61

    const/high16 v3, 0x8000000

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 83
    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->launchExistingHomeActivity(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    goto :goto_0
.end method

.method private static getPendingForRecorded(Landroid/content/Context;IJ)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mode"    # I
    .param p2, "exerciseId"    # J

    .prologue
    .line 348
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.shealth.plugins.exercisepro.service.RealtimeNotification"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    .line 350
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string v2, "activity_to_start"

    const-string v3, "com.sec.android.app.shealth.plugins.exercisepro.ExerciseProWorkoutInfoActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_workout_end_mode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 354
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->putExtraMode(I)V

    .line 355
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "pick_result"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 357
    sget v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mLastNotiID:I

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {p0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 358
    .local v0, "pendingIntent":Landroid/app/PendingIntent;
    return-object v0
.end method

.method private static getPendingForRecorded(Landroid/content/Context;ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/PendingIntent;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mode"    # I
    .param p2, "result"    # Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    .prologue
    .line 88
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.shealth.exercisepro.command.workoutinfo"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    .line 90
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_workout_end_mode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 92
    invoke-static {p1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->putExtraMode(I)V

    .line 94
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_sync_time"

    iget-wide v3, p2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->start_time:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 96
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_sync_exercise_info_db_id"

    iget-wide v3, p2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_exID:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 98
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_sync_duration"

    iget v3, p2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->duration:F

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 100
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_sync_distance"

    iget v3, p2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_distance_sum:F

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 102
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v2, "realtime_sync_calories"

    iget v3, p2, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->last_calories:F

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 103
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const/high16 v2, 0x24000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 105
    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const/high16 v3, 0x8000000

    invoke-static {p0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 106
    .local v0, "pendingIntent":Landroid/app/PendingIntent;
    return-object v0
.end method

.method private static getSoundURI(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p0, "localContext"    # Landroid/content/Context;

    .prologue
    .line 282
    const v1, 0x7f090a7f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    .line 283
    sget-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    sput-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->tickerMsg:Ljava/lang/String;

    .line 284
    const v1, 0x7f090a80

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->txtMsg:Ljava/lang/String;

    .line 285
    const/4 v1, 0x2

    invoke-static {v1}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v0

    .line 286
    .local v0, "soundUri":Landroid/net/Uri;
    return-object v0
.end method

.method private static launchExistingHomeActivity(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "launcherintent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 57
    const-string v1, "RealtimeNotification"

    const-string v2, "launchExistingHomeActivity - getPendingForProcessing"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    const-string v1, "com.sec.shealth.HomeActivity"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v1, "com.sec.android.app.shealth"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v1, "launchWidget"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    const-string v1, "exercisenotify"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 62
    const-class v1, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {p1, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 63
    const/high16 v1, 0x10010000

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 64
    const/16 v1, 0xf61

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, p1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 66
    .local v0, "pendingIntent":Landroid/app/PendingIntent;
    return-object v0
.end method

.method private static putExtraMode(I)V
    .locals 3
    .param p0, "mode"    # I

    .prologue
    .line 110
    const/16 v0, 0x42

    if-ne p0, v0, :cond_0

    .line 111
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_mode"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->mIntent:Landroid/content/Intent;

    const-string/jumbo v1, "realtime_sync_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static showNotification(IJJ)Landroid/app/Notification;
    .locals 4
    .param p0, "mode"    # I
    .param p1, "time"    # J
    .param p3, "exerciseId"    # J

    .prologue
    const/4 v2, 0x0

    .line 118
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getNotificationValue(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-object v2

    .line 122
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 123
    .local v0, "localContext":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 126
    and-int/lit8 v2, p0, 0xf

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 127
    invoke-static {v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getPendingForProcessing(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 131
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    :goto_1
    invoke-static {p0, p1, p2, v1}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->showNotification(IJLandroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v2

    goto :goto_0

    .line 129
    .end local v1    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_2
    invoke-static {v0, p0, p3, p4}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getPendingForRecorded(Landroid/content/Context;IJ)Landroid/app/PendingIntent;

    move-result-object v1

    .restart local v1    # "pendingIntent":Landroid/app/PendingIntent;
    goto :goto_1
.end method

.method private static showNotification(IJLandroid/app/PendingIntent;)Landroid/app/Notification;
    .locals 8
    .param p0, "mode"    # I
    .param p1, "when"    # J
    .param p3, "pendingIntent"    # Landroid/app/PendingIntent;

    .prologue
    const/4 v7, 0x0

    .line 171
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getNotificationValue(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-object v7

    .line 175
    :cond_1
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->txtMsg:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->tickerMsg:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    .line 176
    const/4 v4, 0x0

    .line 177
    .local v4, "soundUri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 178
    .local v5, "localContext":Landroid/content/Context;
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_2

    .line 179
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/calendar/CalendarFactory;->getInstance()Ljava/util/GregorianCalendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide p1

    .line 180
    :cond_2
    if-eqz v5, :cond_0

    .line 182
    sparse-switch p0, :sswitch_data_0

    goto :goto_0

    .line 185
    :sswitch_0
    const v0, 0x7f090a7d

    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    .line 186
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->tickerMsg:Ljava/lang/String;

    .line 187
    const v0, 0x7f090a7e

    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->txtMsg:Ljava/lang/String;

    .line 198
    :goto_1
    const/4 v6, 0x0

    move v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getNotification(IJLandroid/app/PendingIntent;Landroid/net/Uri;Landroid/content/Context;ZLcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/Notification;

    move-result-object v7

    goto :goto_0

    .line 190
    :sswitch_1
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getSoundURI(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v4

    .line 191
    goto :goto_1

    .line 193
    :sswitch_2
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getSoundURI(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v4

    .line 194
    goto :goto_1

    .line 182
    :sswitch_data_0
    .sparse-switch
        0x21 -> :sswitch_0
        0x42 -> :sswitch_1
        0x81 -> :sswitch_0
        0x102 -> :sswitch_2
    .end sparse-switch
.end method

.method public static showNotification(ILcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/Notification;
    .locals 8
    .param p0, "mode"    # I
    .param p1, "result"    # Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;

    .prologue
    const/4 v3, 0x0

    .line 135
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->getNotificationValue(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-object v3

    .line 138
    :cond_1
    if-eqz p1, :cond_0

    .line 140
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->txtMsg:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->tickerMsg:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    .line 141
    const/4 v4, 0x0

    .line 142
    .local v4, "soundUri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 143
    .local v5, "localContext":Landroid/content/Context;
    if-eqz v5, :cond_0

    .line 145
    sparse-switch p0, :sswitch_data_0

    goto :goto_0

    .line 147
    :sswitch_0
    const-string v0, "Searching ANT+ Fitness Equipment"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    .line 148
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->tickerMsg:Ljava/lang/String;

    .line 149
    const-string v0, "Move your device near the ANT+ Link Here logo"

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->txtMsg:Ljava/lang/String;

    .line 167
    :goto_1
    iget-wide v1, p1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->start_time:J

    const/4 v6, 0x1

    move v0, p0

    move-object v7, p1

    invoke-static/range {v0 .. v7}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getNotification(IJLandroid/app/PendingIntent;Landroid/net/Uri;Landroid/content/Context;ZLcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;)Landroid/app/Notification;

    move-result-object v3

    goto :goto_0

    .line 153
    :sswitch_1
    iget v0, p1, Lcom/sec/android/app/shealth/plugins/exercisepro/datas/WorkoutResult;->activity_type:I

    invoke-static {v0, v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getActivityTypeTitle(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    .line 154
    sget-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->titleMsg:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->tickerMsg:Ljava/lang/String;

    .line 155
    const v0, 0x7f090a7e

    invoke-virtual {v5, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->txtMsg:Ljava/lang/String;

    goto :goto_1

    .line 158
    :sswitch_2
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getSoundURI(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v4

    .line 159
    goto :goto_1

    .line 161
    :sswitch_3
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->getSoundURI(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v4

    .line 162
    goto :goto_1

    .line 145
    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x21 -> :sswitch_1
        0x42 -> :sswitch_2
        0x81 -> :sswitch_1
        0x102 -> :sswitch_3
    .end sparse-switch
.end method

.method public static updateNotification(Ljava/lang/String;)V
    .locals 9
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x15

    const/4 v7, 0x1

    .line 362
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 363
    .local v1, "localContext":Landroid/content/Context;
    const-string/jumbo v5, "notification"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 365
    .local v2, "mNotificationManager":Landroid/app/NotificationManager;
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->builder:Landroid/support/v4/app/NotificationCompat$Builder;

    if-eqz v5, :cond_1

    .line 366
    const/4 v3, 0x0

    .line 367
    .local v3, "notification":Landroid/app/Notification;
    invoke-static {v1}, Lcom/sec/android/app/shealth/receiver/InstalledReceiver;->isHomeActivityRunning(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 368
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 369
    .local v0, "intent":Landroid/content/Intent;
    const-string/jumbo v5, "widgetActivityAction"

    const-string v6, "com.sec.shealth.action.EXERCISE"

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    const-string/jumbo v5, "widgetActivityPackage"

    const-string v6, "com.sec.android.app.shealth"

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    const-string v5, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    const/16 v5, 0xf61

    const/high16 v6, 0x8000000

    invoke-static {v1, v5, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 374
    .local v4, "pendingIntent":Landroid/app/PendingIntent;
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->builder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v5, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 375
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->builder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v5}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 376
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v8, :cond_0

    .line 377
    iput v7, v3, Landroid/app/Notification;->visibility:I

    .line 384
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v4    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    :goto_0
    sget v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->notificationId:I

    invoke-virtual {v2, v5, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 386
    .end local v3    # "notification":Landroid/app/Notification;
    :cond_1
    return-void

    .line 379
    .restart local v3    # "notification":Landroid/app/Notification;
    :cond_2
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->builder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v5, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->pendingIntentMain:Landroid/app/PendingIntent;

    invoke-virtual {v5, v6}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 380
    sget-object v5, Lcom/sec/android/app/shealth/plugins/exercisepro/service/RealtimeNotification;->builder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v5}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 381
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v5, v8, :cond_0

    .line 382
    iput v7, v3, Landroid/app/Notification;->visibility:I

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    .line 329
    const-string v3, "RealtimeNotification"

    const-string v4, "RealtimeNotification.onReceive"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 332
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    const-string/jumbo v3, "widgetActivityAction"

    const-string v4, "com.sec.shealth.action.EXERCISE"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 334
    const-string/jumbo v3, "widgetActivityPackage"

    const-string v4, "com.sec.android.app.shealth"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    const/16 v3, 0xf61

    const/high16 v4, 0x8000000

    invoke-static {p1, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 340
    .local v2, "pi":Landroid/app/PendingIntent;
    :try_start_0
    invoke-virtual {v2}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    :goto_0
    return-void

    .line 341
    :catch_0
    move-exception v0

    .line 342
    .local v0, "e":Landroid/app/PendingIntent$CanceledException;
    invoke-virtual {v0}, Landroid/app/PendingIntent$CanceledException;->printStackTrace()V

    goto :goto_0
.end method
