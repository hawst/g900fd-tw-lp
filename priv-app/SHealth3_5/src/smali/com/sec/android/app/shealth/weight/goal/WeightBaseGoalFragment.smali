.class public abstract Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
.source "WeightBaseGoalFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;
    }
.end annotation


# static fields
.field private static final DAYS_IN_WEEK:I = 0x7


# instance fields
.field private mGoalActivity:Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

.field private mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

.field private mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;-><init>()V

    .line 49
    return-void
.end method

.method private initDaoAndUnitHelper()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDaoImplDb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    .line 64
    new-instance v0, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    .line 65
    return-void
.end method


# virtual methods
.method protected customizeActionBar(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V
    .locals 0
    .param p1, "actionBarHelper"    # Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    .prologue
    .line 107
    invoke-virtual {p1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->removeAllActionBarButtons()V

    .line 108
    return-void
.end method

.method protected getActiveGoal()Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;
    .locals 3

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->getWeightDao()Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;->getActiveGoal(J)Lcom/sec/android/app/shealth/common/utils/hcp/data/GoalData;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->mUnitHelper:Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper;->getWeightUnit()Lcom/sec/android/app/shealth/weight/utils/WeightUnitHelper$WeightType;

    move-result-object v0

    return-object v0
.end method

.method protected getDateFromTime(J)Ljava/util/Date;
    .locals 2
    .param p1, "time"    # J

    .prologue
    .line 132
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 133
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 135
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    return-object v1
.end method

.method protected abstract getFragmentType()Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment$GoalFragment;
.end method

.method protected getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->mGoalActivity:Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    return-object v0
.end method

.method public getWeightDao()Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->mWeightDao:Lcom/sec/android/app/shealth/weight/weightdao/WeightDao;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->getGoalActivity()Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;->getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->customizeActionBar(Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;)V

    .line 100
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 90
    :try_start_0
    check-cast p1, Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->mGoalActivity:Lcom/sec/android/app/shealth/weight/goal/WeightGoalActivity;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/ClassCastException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Activity must be instance of WeightGoalActivity."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/weight/goal/WeightBaseGoalFragment;->initDaoAndUnitHelper()V

    .line 60
    return-void
.end method
