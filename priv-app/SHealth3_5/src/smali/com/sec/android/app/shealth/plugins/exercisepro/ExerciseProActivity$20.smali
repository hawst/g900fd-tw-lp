.class Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;
.super Landroid/os/Handler;
.source "ExerciseProActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)V
    .locals 0

    .prologue
    .line 1758
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 1760
    if-eqz p1, :cond_0

    .line 1761
    const/4 v0, 0x0

    .line 1762
    .local v0, "hrmValue":I
    iget v5, p1, Landroid/os/Message;->arg1:I

    packed-switch v5, :pswitch_data_0

    .line 1802
    .end local v0    # "hrmValue":I
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1764
    .restart local v0    # "hrmValue":I
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1765
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;->createMeasurementDialog()V

    goto :goto_0

    .line 1769
    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg2:I

    .line 1770
    sget-boolean v5, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v5, :cond_3

    .line 1771
    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v5, :cond_1

    .line 1772
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->insertHRMdata(I)V

    .line 1775
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 1776
    .local v1, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 1777
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    if-eqz v3, :cond_2

    .line 1778
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1779
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1781
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .line 1782
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0

    .line 1773
    .end local v1    # "manager":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "transaction":Landroid/support/v4/app/FragmentTransaction;
    :cond_3
    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v5, :cond_1

    .line 1774
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->insertHRMdata(I)V

    goto :goto_1

    .line 1785
    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg2:I

    .line 1786
    sget-boolean v5, Lcom/sec/android/app/shealth/plugins/exercisepro/constants/ConfigurationConstants;->CHINA_AMAP_LOCATION_ON:Z

    if-eqz v5, :cond_6

    .line 1787
    if-eqz v0, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    if-eqz v5, :cond_4

    .line 1788
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mAmapStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/amap/AMapExerciseProStatusFragment;->insertHRMdata(I)V

    .line 1791
    :cond_4
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 1792
    .local v2, "manager1":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 1793
    .local v4, "transaction1":Landroid/support/v4/app/FragmentTransaction;
    if-eqz v4, :cond_5

    .line 1794
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v5}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$700(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1795
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1797
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mExerciseProHRMFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;
    invoke-static {v5, v6}, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->access$702(Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;)Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProHRMFragment;

    .line 1798
    invoke-static {}, Ljava/lang/System;->gc()V

    goto/16 :goto_0

    .line 1789
    .end local v2    # "manager1":Landroid/support/v4/app/FragmentManager;
    .end local v4    # "transaction1":Landroid/support/v4/app/FragmentTransaction;
    :cond_6
    if-eqz v0, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    if-eqz v5, :cond_4

    .line 1790
    iget-object v5, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity$20;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;

    iget-object v5, v5, Lcom/sec/android/app/shealth/plugins/exercisepro/ExerciseProActivity;->mStatusFragment:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v5, v0}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->insertHRMdata(I)V

    goto :goto_2

    .line 1762
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
