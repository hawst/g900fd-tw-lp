.class Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1$1;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;->onContentInitialization(Landroid/view/View;Landroid/app/Activity;Landroid/app/Dialog;Landroid/os/Bundle;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OKButtonHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;

.field final synthetic val$timePicker:Landroid/widget/TimePicker;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;Landroid/widget/TimePicker;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;

    iput-object p2, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1$1;->val$timePicker:Landroid/widget/TimePicker;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1$1;->val$timePicker:Landroid/widget/TimePicker;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->calendar:Ljava/util/Calendar;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1$1;->val$timePicker:Landroid/widget/TimePicker;

    iget-object v1, p0, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1$1;->this$1:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker$1;->this$0:Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;

    iget-object v1, v1, Lcom/sec/android/app/shealth/framework/ui/common/TimePicker;->calendar:Ljava/util/Calendar;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    return-void
.end method
