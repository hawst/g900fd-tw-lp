.class public Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "CignaLogViewAdapter.java"


# instance fields
.field private mChildList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDate:Landroid/widget/TextView;

.field private mGroup:Landroid/widget/TextView;

.field private mGroupList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPointValues:Landroid/widget/TextView;

.field private mPoints:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p2, "logDataList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;>;"
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mContext:Landroid/content/Context;

    .line 33
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mGroupList:Ljava/util/ArrayList;

    .line 34
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mChildList:Ljava/util/ArrayList;

    .line 36
    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 38
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 40
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mGroupList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mChildList:Ljava/util/ArrayList;

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 44
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mChildList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 53
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 59
    if-nez p4, :cond_0

    .line 60
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 61
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f03004a

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 65
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v6, 0x7f08016d

    invoke-virtual {p4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mPointValues:Landroid/widget/TextView;

    .line 66
    const v6, 0x7f08016e

    invoke-virtual {p4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mPoints:Landroid/widget/TextView;

    .line 67
    const v6, 0x7f080171

    invoke-virtual {p4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mDate:Landroid/widget/TextView;

    .line 69
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mChildList:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    invoke-virtual {v6, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 71
    .local v1, "childDetail":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 72
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v0

    .line 73
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-direct {v2, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 75
    .local v2, "d":Ljava/util/Date;
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v6, "yyyy.MM.dd"

    invoke-direct {v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 76
    .local v5, "logViewDateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {v5, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, "date":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mPointValues:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mPoints:Landroid/widget/TextView;

    const-string/jumbo v7, "points"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mDate:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    return-object p4
.end method

.method public getChildrenCount(I)I
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mChildList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mGroupList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mGroupList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 102
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 108
    if-nez p3, :cond_0

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 110
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030049

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 113
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mGroupList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 115
    .local v0, "header":Ljava/lang/String;
    const v2, 0x7f080167

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mGroup:Landroid/widget/TextView;

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/logview/CignaLogViewAdapter;->mGroup:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    return-object p3
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method
