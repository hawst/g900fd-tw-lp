.class Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;
.super Ljava/lang/Object;
.source "WalkingSContextListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->setHealthyStepToDB(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

.field final synthetic val$save_steps:I

.field final synthetic val$save_time:J


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;JI)V
    .locals 0

    .prologue
    .line 761
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    iput-wide p2, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_time:J

    iput p4, p0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_steps:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private stepValidationCheck(IJ)J
    .locals 8
    .param p1, "steps"    # I
    .param p2, "startTime"    # J

    .prologue
    const/4 v2, 0x0

    .line 860
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 861
    .local v7, "query":Ljava/lang/StringBuilder;
    const-string v0, "SELECT * FROM walk_info"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 862
    const-string v0, " WHERE "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    const-string/jumbo v0, "user_device__id LIKE \'10009%\' "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 864
    const-string v0, " AND "

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 865
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "start_time = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 866
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 868
    .local v6, "cursor":Landroid/database/Cursor;
    const-string v0, "WalkingSContextListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stepValidationCheck() cursor = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    if-eqz v6, :cond_1

    .line 873
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 875
    const-string v0, "WalkingSContextListener"

    const-string/jumbo v1, "stepValidationCheck() row exists!!"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    const-string/jumbo v0, "total_step"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 883
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 890
    :goto_0
    return-wide v0

    .line 883
    :cond_0
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 890
    :cond_1
    :goto_1
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 881
    :catchall_0
    move-exception v0

    .line 883
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 887
    :goto_2
    throw v0

    .line 885
    :catch_0
    move-exception v2

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_2
.end method


# virtual methods
.method public run()V
    .locals 24

    .prologue
    .line 764
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v23

    monitor-enter v23

    .line 765
    :try_start_0
    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    .line 766
    .local v12, "date":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_time:J

    invoke-virtual {v12, v3, v4}, Ljava/util/Date;->setTime(J)V

    .line 767
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "save time = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "healthy step count = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_steps:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_time:J

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(J)J

    move-result-wide v18

    .line 771
    .local v18, "startTime":J
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 772
    .local v17, "query":Ljava/lang/StringBuilder;
    const-string v3, "SELECT * FROM walk_info_extended"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 773
    const-string v3, " WHERE "

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 774
    const-string/jumbo v3, "user_device__id LIKE \'10009%\' "

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 775
    const-string v3, " AND "

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 776
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "start_time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v18

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 778
    const/4 v11, 0x0

    .line 780
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "setHealthyStepToDB sBufferForSteps.lastupdateTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v5

    iget-wide v5, v5, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastStepsUpdateTime:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Long.parseLong(startTime) = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    iget-wide v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastStepsUpdateTime:J

    cmp-long v3, v3, v18

    if-eqz v3, :cond_0

    .line 782
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 786
    :cond_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_steps:I

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v3, v1, v2}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->stepValidationCheck(IJ)J

    move-result-wide v20

    .line 787
    .local v20, "totalSteps":J
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "stepValidationCheck() returned total steps = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for time = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 788
    const-wide/16 v3, 0x0

    cmp-long v3, v20, v3

    if-gtz v3, :cond_2

    .line 851
    if-eqz v11, :cond_1

    .line 852
    :try_start_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_1
    monitor-exit v23
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 856
    .end local v20    # "totalSteps":J
    :goto_0
    return-void

    .line 790
    .restart local v20    # "totalSteps":J
    :cond_2
    if-eqz v11, :cond_8

    .line 791
    :try_start_3
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_5

    .line 792
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 793
    const-wide/16 v9, 0x0

    .line 794
    .local v9, "accmulation":J
    const-string/jumbo v3, "power_step"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 795
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 796
    .local v22, "values":Landroid/content/ContentValues;
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "db startTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "db healthy step : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_steps:I

    int-to-long v3, v3

    add-long/2addr v9, v3

    .line 799
    cmp-long v3, v9, v20

    if-lez v3, :cond_3

    .line 800
    move-wide/from16 v9, v20

    .line 801
    :cond_3
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    iput-wide v9, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    .line 802
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    const-string v4, "_id"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->stepsDBid:J

    .line 803
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    move-wide/from16 v0, v18

    iput-wide v0, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->lastStepsUpdateTime:J

    .line 804
    const-string/jumbo v3, "power_step"

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v4

    iget-wide v4, v4, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 806
    :try_start_4
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v6

    iget-wide v6, v6, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->stepsDBid:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 851
    .end local v9    # "accmulation":J
    .end local v22    # "values":Landroid/content/ContentValues;
    :goto_1
    if-eqz v11, :cond_4

    .line 852
    :try_start_5
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 855
    .end local v20    # "totalSteps":J
    :cond_4
    :goto_2
    monitor-exit v23

    goto/16 :goto_0

    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v12    # "date":Ljava/util/Date;
    .end local v17    # "query":Ljava/lang/StringBuilder;
    .end local v18    # "startTime":J
    :catchall_0
    move-exception v3

    monitor-exit v23
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v3

    .line 808
    .restart local v9    # "accmulation":J
    .restart local v11    # "cursor":Landroid/database/Cursor;
    .restart local v12    # "date":Ljava/util/Date;
    .restart local v17    # "query":Ljava/lang/StringBuilder;
    .restart local v18    # "startTime":J
    .restart local v20    # "totalSteps":J
    .restart local v22    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v14

    .line 809
    .local v14, "ie":Ljava/lang/IllegalArgumentException;
    :try_start_6
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert exception -"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 848
    .end local v9    # "accmulation":J
    .end local v14    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v20    # "totalSteps":J
    .end local v22    # "values":Landroid/content/ContentValues;
    :catch_1
    move-exception v13

    .line 849
    .local v13, "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 851
    if-eqz v11, :cond_4

    .line 852
    :try_start_8
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    .line 812
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v20    # "totalSteps":J
    :cond_5
    :try_start_9
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "db creation : start_time = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(J)J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_steps:I

    int-to-long v15, v3

    .line 814
    .local v15, "insertSteps":J
    cmp-long v3, v15, v20

    if-lez v3, :cond_6

    .line 815
    move-wide/from16 v15, v20

    .line 816
    :cond_6
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 817
    .restart local v22    # "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "start_time"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_time:J

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 818
    const-string v3, "end_time"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->this$0:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_time:J

    const/16 v7, 0xa

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->convertUnitTime(JI)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 819
    const-string/jumbo v3, "power_step"

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 820
    const-string/jumbo v3, "user_device__id"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "10009_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mDeviceID:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$200()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 822
    :try_start_a
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v22

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    .line 823
    :catch_2
    move-exception v14

    .line 824
    .restart local v14    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_b
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert exception -"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_1

    .line 851
    .end local v14    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v15    # "insertSteps":J
    .end local v20    # "totalSteps":J
    .end local v22    # "values":Landroid/content/ContentValues;
    :catchall_1
    move-exception v3

    if-eqz v11, :cond_7

    .line 852
    :try_start_c
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 828
    .restart local v20    # "totalSteps":J
    :cond_8
    :try_start_d
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    iget-wide v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->stepsDBid:J

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_a

    .line 830
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    iget-wide v4, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$2;->val$save_steps:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    iput-wide v4, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    .line 831
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    iget-wide v3, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    cmp-long v3, v3, v20

    if-lez v3, :cond_9

    .line 832
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v3

    move-wide/from16 v0, v20

    iput-wide v0, v3, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    .line 833
    :cond_9
    const-string v3, "WalkingSContextListener"

    const-string/jumbo v4, "update using buffer"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "healthyStep = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v5

    iget-wide v5, v5, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DBid = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v5

    iget-wide v5, v5, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->stepsDBid:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 837
    .restart local v22    # "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "power_step"

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v4

    iget-wide v4, v4, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->accumlatedHealthyStep:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 839
    :try_start_e
    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$100()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfoExtended;->CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->sBufferForSteps:Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener;->access$000()Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;

    move-result-object v6

    iget-wide v6, v6, Lcom/sec/android/app/shealth/walkingmate/service/walkingservice/WalkingSContextListener$bufferDataStructure;->stepsDBid:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto/16 :goto_1

    .line 841
    :catch_3
    move-exception v14

    .line 842
    .restart local v14    # "ie":Ljava/lang/IllegalArgumentException;
    :try_start_f
    const-string v3, "WalkingSContextListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert exception -"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 845
    .end local v14    # "ie":Ljava/lang/IllegalArgumentException;
    .end local v22    # "values":Landroid/content/ContentValues;
    :cond_a
    const-string v3, "WalkingSContextListener"

    const-string v4, "data is not saved"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto/16 :goto_1
.end method
