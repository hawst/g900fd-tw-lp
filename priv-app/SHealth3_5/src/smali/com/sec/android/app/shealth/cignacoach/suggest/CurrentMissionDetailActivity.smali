.class public Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;
.super Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;
.source "CurrentMissionDetailActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$6;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

.field private mCurrentMissionScrollView:Landroid/widget/ScrollView;

.field mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

.field private mDetailsDescription1:Landroid/widget/TextView;

.field private mDetailsDescription2:Landroid/widget/TextView;

.field private mFromHistory:Z

.field private mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

.field private mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

.field private mNewMissionDetailList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedMissionId:I

.field private mToast1:Landroid/widget/Toast;

.field private mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mToast1:Landroid/widget/Toast;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mNewMissionDetailList:Ljava/util/ArrayList;

    .line 597
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$5;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;Lcom/cigna/coach/apiobjects/MissionDetails;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;
    .param p1, "x1"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->addMissionDetails(Lcom/cigna/coach/apiobjects/MissionDetails;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;Lcom/cigna/coach/apiobjects/MissionDetails;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;
    .param p1, "x1"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->removeMissionDetails(Lcom/cigna/coach/apiobjects/MissionDetails;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mToast1:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->onCancel()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->onSave()V

    return-void
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->updateTrackingView()V

    return-void
.end method

.method private addMissionDetails(Lcom/cigna/coach/apiobjects/MissionDetails;)V
    .locals 7
    .param p1, "missionDetails"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 311
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mNewMissionDetailList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 313
    .local v0, "data":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isSameDay(JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 319
    .end local v0    # "data":Lcom/cigna/coach/apiobjects/MissionDetails;
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mNewMissionDetailList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getCurrentMissionTipTitleData()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 226
    .local v1, "listData":Ljava/util/List;, "Ljava/util/List<[Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getMissionTipList()Ljava/util/List;

    move-result-object v3

    .line 228
    .local v3, "missionTipList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;>;"
    if-eqz v3, :cond_0

    .line 229
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;

    .line 230
    .local v2, "missionTip":Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;->getTip()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "missionTip":Lcom/sec/android/app/shealth/cignacoach/data/MissionTipData;
    :cond_0
    return-object v1
.end method

.method private getMissionStatusAndCompleteDayStr(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)Ljava/lang/String;
    .locals 4
    .param p1, "status"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .prologue
    .line 549
    const/4 v0, 0x0

    .line 550
    .local v0, "result":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$6;->$SwitchMap$com$cigna$coach$interfaces$IGoalsMissions$MissionStatus:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 564
    const-string v0, ""

    .line 567
    :goto_0
    return-object v0

    .line 552
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090309

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCompleteDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 553
    goto :goto_0

    .line 555
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0902f3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCompleteDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 556
    goto :goto_0

    .line 558
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090329

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCompleteDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 559
    goto/16 :goto_0

    .line 561
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090306

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCompleteDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/DateFormatUtil;->getMonthLen3DateFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 562
    goto/16 :goto_0

    .line 550
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getTrackerStr(Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;)Ljava/lang/String;
    .locals 3
    .param p1, "trackerType"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .prologue
    .line 571
    const/4 v0, 0x0

    .line 572
    .local v0, "trackerStr":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$6;->$SwitchMap$com$cigna$coach$apiobjects$GoalMissionInfo$TrackerType:[I

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 594
    :goto_0
    return-object v0

    .line 574
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 575
    goto :goto_0

    .line 577
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c85

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 578
    goto :goto_0

    .line 580
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c86

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 581
    goto :goto_0

    .line 583
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c5f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 584
    goto :goto_0

    .line 586
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09017e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 587
    goto :goto_0

    .line 589
    :pswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090c87

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 590
    goto :goto_0

    .line 572
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private initView()V
    .locals 6

    .prologue
    .line 135
    const v3, 0x7f080134

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionScrollView:Landroid/widget/ScrollView;

    .line 136
    const v3, 0x7f080135

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    .line 138
    const v3, 0x7f080146

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setOnDisableInceptTouchEventListener(Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager$OnDisableInterceptTouchEventListener;)V

    .line 141
    const/4 v3, 0x7

    new-array v3, v3, [Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    .line 142
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    const/4 v5, 0x0

    const v3, 0x7f08014f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aput-object v3, v4, v5

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    const/4 v5, 0x1

    const v3, 0x7f080150

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aput-object v3, v4, v5

    .line 144
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    const/4 v5, 0x2

    const v3, 0x7f080151

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aput-object v3, v4, v5

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    const/4 v5, 0x3

    const v3, 0x7f080152

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aput-object v3, v4, v5

    .line 146
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    const/4 v5, 0x4

    const v3, 0x7f080153

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aput-object v3, v4, v5

    .line 147
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    const/4 v5, 0x5

    const v3, 0x7f080154

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aput-object v3, v4, v5

    .line 148
    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    const/4 v5, 0x6

    const v3, 0x7f080155

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aput-object v3, v4, v5

    .line 150
    const v3, 0x7f080137

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mDetailsDescription1:Landroid/widget/TextView;

    .line 151
    const v3, 0x7f080138

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mDetailsDescription2:Landroid/widget/TextView;

    .line 153
    const v3, 0x7f08014e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 154
    .local v2, "trackingViewCategoryTxt":Landroid/widget/TextView;
    const v3, 0x7f080145

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 155
    .local v1, "tipsCategoryTxt":Landroid/widget/TextView;
    const v3, 0x7f080136

    invoke-virtual {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 156
    .local v0, "detailCategoryTxt":Landroid/widget/TextView;
    const v3, 0x7f090304

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 157
    const v3, 0x7f090301

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 158
    const v3, 0x7f090300

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 159
    return-void
.end method

.method private isMissionCheck(Landroid/view/View;J)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "timeMill"    # J

    .prologue
    .line 327
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mNewMissionDetailList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 328
    .local v1, "missionDetail":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v2

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v2, p2, p3, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isSameDay(JJ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 329
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 330
    const/4 v2, 0x1

    .line 333
    .end local v1    # "missionDetail":Lcom/cigna/coach/apiobjects/MissionDetails;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onCancel()V
    .locals 0

    .prologue
    .line 502
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->finish()V

    .line 503
    return-void
.end method

.method private onSave()V
    .locals 13

    .prologue
    .line 391
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mNewMissionDetailList:Ljava/util/ArrayList;

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->setMissionDetails(Ljava/util/ArrayList;)V

    .line 392
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/CurrentGoalDataManager;->getUserGoalDataByGoalID(I)Lcom/sec/android/app/shealth/cignacoach/data/GoalData;

    move-result-object v5

    .line 393
    .local v5, "goalData":Lcom/sec/android/app/shealth/cignacoach/data/GoalData;
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->setUserMissionDetail(Lcom/sec/android/app/shealth/cignacoach/data/MissionData;)Lcom/cigna/coach/dataobjects/GoalMissionStatusData;

    move-result-object v3

    .line 395
    .local v3, "gmsd":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    if-nez v3, :cond_1

    .line 396
    sget-object v10, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    const-string v11, "Becuase GoalMissionStatusData is null, finish~"

    invoke-static {v10, v11}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->finish()V

    .line 499
    :cond_0
    :goto_0
    return-void

    .line 401
    :cond_1
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-result-object v6

    .line 402
    .local v6, "goalProgressStatus":Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v9

    .line 404
    .local v9, "missionProgressStatus":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    if-nez v9, :cond_2

    .line 405
    sget-object v10, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    const-string v11, "Becuase MissionProgressStatus is null, finish~"

    invoke-static {v10, v11}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->finish()V

    goto :goto_0

    .line 410
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-static {v10}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->valueOf(Lcom/sec/android/app/shealth/cignacoach/data/MissionData;)Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;

    move-result-object v1

    .line 411
    .local v1, "completeInfoData":Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mNewMissionDetailList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v1, v10}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setMissionProgressFrequency(I)V

    .line 412
    invoke-virtual {v1, v6}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setGoalProgressStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;)V

    .line 413
    invoke-virtual {v1, v9}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setMissionProgressStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;)V

    .line 414
    if-eqz v5, :cond_3

    .line 415
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getTitleString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setGoalTitle(Ljava/lang/String;)V

    .line 416
    invoke-virtual {v5}, Lcom/sec/android/app/shealth/cignacoach/data/GoalData;->getExtraString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setGoalFrequency(Ljava/lang/String;)V

    .line 418
    :cond_3
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->isMissionRepeatable()Z

    move-result v10

    invoke-virtual {v1, v10}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setMissionRepeatable(Z)V

    .line 420
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 421
    .local v0, "badgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 422
    sget-object v10, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Badge size : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_4

    .line 428
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v1, v10}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->setBadgeSize(I)V

    .line 430
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v10

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getBadgeIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 434
    :cond_4
    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    if-ne v6, v10, :cond_8

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "com.sec.android.app.shealth.cignacoach"

    const-string v12, "0017"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v10

    if-eqz v10, :cond_7

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_7

    .line 437
    if-eqz v0, :cond_5

    .line 438
    sget-object v10, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    const-string v11, "Goal Badge"

    invoke-static {v10, v11}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-direct {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->processBadgeDataForGoal(Ljava/util/ArrayList;)V

    .line 451
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->finish()V

    .line 454
    new-instance v7, Landroid/content/Intent;

    const-string v10, "com.sec.android.app.shealth.cignacoach.GOAL_COMPLETE"

    invoke-direct {v7, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 455
    .local v7, "intent":Landroid/content/Intent;
    const-string v10, "intent_category_type"

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 456
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 494
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_6
    :goto_2
    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    if-eq v9, v10, :cond_0

    .line 495
    new-instance v7, Landroid/content/Intent;

    const-string v10, "com.sec.android.app.shealth.cignacoach.MISSION_PROGRESSED"

    invoke-direct {v7, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 496
    .restart local v7    # "intent":Landroid/content/Intent;
    const-string v10, "EXTRA_NAME_EXPAND_GOAL_ID"

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v11

    invoke-virtual {v7, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 497
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 443
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_7
    :try_start_0
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 444
    .local v4, "goalCompleteIntent":Landroid/content/Intent;
    new-instance v10, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const-class v12, Lcom/sec/android/app/shealth/cignacoach/GoalCompleteActivity;

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 445
    const-string v10, "intent_complete_info_data"

    invoke-virtual {v4, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 446
    invoke-virtual {p0, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 447
    .end local v4    # "goalCompleteIntent":Landroid/content/Intent;
    :catch_0
    move-exception v2

    .line 448
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 459
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_8
    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    if-eq v9, v10, :cond_9

    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    if-ne v9, v10, :cond_d

    .line 461
    :cond_9
    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    if-ne v9, v10, :cond_a

    .line 462
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "com.sec.android.app.shealth.cignacoach"

    const-string v12, "0016"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    :cond_a
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v10

    if-eqz v10, :cond_c

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_c

    .line 466
    if-eqz v0, :cond_b

    .line 467
    iget-object v10, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v10}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v10

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/CompleteInfoData;->getMissionId()I

    move-result v11

    invoke-direct {p0, v0, v10, v11}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->processBadgeDataForMission(Ljava/util/ArrayList;II)V

    .line 479
    :cond_b
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->finish()V

    .line 482
    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    if-ne v9, v10, :cond_6

    .line 483
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 484
    .restart local v7    # "intent":Landroid/content/Intent;
    new-instance v7, Landroid/content/Intent;

    .end local v7    # "intent":Landroid/content/Intent;
    const-string v10, "com.sec.android.app.shealth.cignacoach.MISSION_COMPLETE"

    invoke-direct {v7, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 485
    .restart local v7    # "intent":Landroid/content/Intent;
    const-string v10, "EXTRA_NAME_EXPAND_GOAL_ID"

    iget-object v11, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v11}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v11

    invoke-virtual {v7, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 486
    invoke-virtual {p0, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 471
    .end local v7    # "intent":Landroid/content/Intent;
    :cond_c
    :try_start_1
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 472
    .local v8, "missionCompleteIntent":Landroid/content/Intent;
    new-instance v10, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const-class v12, Lcom/sec/android/app/shealth/cignacoach/MissionCompleteActivity;

    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v10}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 473
    const-string v10, "intent_complete_info_data"

    invoke-virtual {v8, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 474
    invoke-virtual {p0, v8}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 475
    .end local v8    # "missionCompleteIntent":Landroid/content/Intent;
    :catch_1
    move-exception v2

    .line 476
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 489
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_d
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090835

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 490
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->finish()V

    goto/16 :goto_2
.end method

.method private processBadgeDataForGoal(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 506
    .local p1, "earnedBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 508
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 510
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->startBadgeActivityForGoalComplete(Ljava/util/ArrayList;)V

    .line 513
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 515
    :cond_1
    return-void
.end method

.method private processBadgeDataForMission(Ljava/util/ArrayList;II)V
    .locals 2
    .param p2, "goalId"    # I
    .param p3, "missionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 526
    .local p1, "earnedBadgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 528
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 530
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->startBadgeActivityForMissionComplete(Ljava/util/ArrayList;II)V

    .line 533
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 536
    :cond_1
    return-void
.end method

.method private removeMissionDetails(Lcom/cigna/coach/apiobjects/MissionDetails;)V
    .locals 1
    .param p1, "missionDetails"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mNewMissionDetailList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 323
    return-void
.end method

.method private setListener()V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method private startBadgeActivityForGoalComplete(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 518
    .local p1, "badgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 519
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v2, 0x8ae

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 520
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 522
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 523
    return-void
.end method

.method private startBadgeActivityForMissionComplete(Ljava/util/ArrayList;II)V
    .locals 3
    .param p2, "goalId"    # I
    .param p3, "missionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 539
    .local p1, "badgeIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/cignacoach/BadgeAchieveActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 540
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 541
    const-string v1, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 542
    const-string v1, "EXTRA_NAME_BADGE_VIEW_MODE"

    const/16 v2, 0x457

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 543
    const-string v1, "EXTRA_NAME_BADGE_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 545
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 546
    return-void
.end method

.method private updateDetailView()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mDetailsDescription1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getWhatToDo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mDetailsDescription2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getWhyDoIt()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    return-void
.end method

.method private updateHeaderView()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v4}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getGoalID()I

    move-result v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaImageResource;->getCignaGoalIconIdByCategory(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setIconImage(Landroid/graphics/drawable/Drawable;)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getTitleString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setTitleText(Ljava/lang/String;)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getFrequencyDef()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setExtraText(Ljava/lang/String;)V

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setOnDoItAgainClickListener(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView$OnDoItAgainClickListener;)V

    .line 175
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mFromHistory:Z

    if-eqz v2, :cond_2

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityXDayLeft(Z)V

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityCompleteDate(Z)V

    .line 178
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachDataManager;->isCurrentUserMission(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isMissionRepeatable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 179
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityDoItAgain(Z)V

    .line 183
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityTrackerMissionInfoTxt(Z)V

    .line 185
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getMissionStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getMissionStatusAndCompleteDayStr(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setCompleteDateText(Ljava/lang/String;)V

    .line 211
    :goto_1
    return-void

    .line 181
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityDoItAgain(Z)V

    goto :goto_0

    .line 188
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityXDayLeft(Z)V

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityCompleteDate(Z)V

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityDoItAgain(Z)V

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getSelectFrequencyDays()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getMissionDetailList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int v0, v2, v3

    .line 193
    .local v0, "leftDays":I
    if-ne v0, v7, :cond_3

    .line 194
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    const v3, 0x7f090302

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setXDayLeftText(Ljava/lang/String;)V

    .line 199
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->isTrackerMission()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityTrackerMissionInfoTxt(Z)V

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getTrackerStr(Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;)Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "trackerStr":Ljava/lang/String;
    if-nez v1, :cond_4

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityTrackerMissionInfoTxt(Z)V

    goto :goto_1

    .line 196
    .end local v1    # "trackerStr":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    const v3, 0x7f090303

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setXDayLeftText(Ljava/lang/String;)V

    goto :goto_2

    .line 205
    .restart local v1    # "trackerStr":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setTrackerMissionInfoText(Ljava/lang/String;)V

    goto :goto_1

    .line 208
    .end local v1    # "trackerStr":Ljava/lang/String;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionHeaderView:Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailHeaderView;->setVisibilityTrackerMissionInfoTxt(Z)V

    goto :goto_1
.end method

.method private updateTipTitleView()V
    .locals 3

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getCurrentMissionTipTitleData()Ljava/util/List;

    move-result-object v0

    .line 215
    .local v0, "tipsData":Ljava/util/List;, "Ljava/util/List<[Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 216
    :cond_0
    const v1, 0x7f080144

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 220
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mIndicatorViewPager:Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;

    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailPageAdapter;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailPageAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/widget/IndicatorViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    goto :goto_0
.end method

.method private updateTrackingView()V
    .locals 9

    .prologue
    .line 239
    iget-boolean v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mFromHistory:Z

    if-eqz v6, :cond_1

    .line 241
    const v6, 0x7f08014a

    invoke-virtual {p0, v6}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 307
    :cond_0
    return-void

    .line 245
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getStartDateTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getWeek(J)Ljava/util/List;

    move-result-object v5

    .line 246
    .local v5, "timeMillis":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    .line 247
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 248
    move v1, v0

    .line 249
    .local v1, "index":I
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 251
    .local v3, "timeMill":J
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v6, v6, v1

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->setTrackingMode(Z)V

    .line 253
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->updateTrackingLayoutVisibility()V

    .line 254
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v6, v6, v1

    invoke-virtual {v6, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->updateView(J)V

    .line 255
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v6, v6, v1

    iget-object v7, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v7, v7, v1

    invoke-direct {p0, v7, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->isMissionCheck(Landroid/view/View;J)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->setSelection(Z)V

    .line 259
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->getInstance()Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/util/CignaCoachUtils;->isFuture(J)Z

    move-result v6

    if-nez v6, :cond_2

    .line 261
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v6, v6, v1

    new-instance v7, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;

    invoke-direct {v7, p0, v1, v3, v4}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$2;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;IJ)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 285
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->isSelection()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 286
    sget-object v6, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    const-string v7, "future time, unselect selected ones"

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v6, v6, v1

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->setSelection(Z)V

    .line 289
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mTrakingDayOfWeekView:[Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;

    aget-object v6, v6, v1

    new-instance v7, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$3;

    invoke-direct {v7, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$3;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/shealth/cignacoach/widget/CurrentMissionDetailTrakingDayOfWeekView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 347
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->customizeActionBar()V

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    const v4, 0x7f090c78

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitleIconVisibility(Z)V

    .line 351
    iget-boolean v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mFromHistory:Z

    if-nez v3, :cond_0

    .line 352
    new-instance v0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity$4;-><init>(Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;)V

    .line 374
    .local v0, "actionBarButtonListener":Landroid/view/View$OnClickListener;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setCancelDoneVisibility(Z)V

    .line 375
    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f090048

    invoke-direct {v1, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 376
    .local v1, "cancelButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButtonToSupportLayout([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 378
    new-instance v2, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v3, 0x7f09004f

    invoke-direct {v2, v5, v3, v0, v6}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IILandroid/view/View$OnClickListener;Z)V

    .line 379
    .local v2, "saveButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v3

    new-array v4, v6, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButtonToSupportLayout([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 382
    .end local v0    # "actionBarButtonListener":Landroid/view/View$OnClickListener;
    .end local v1    # "cancelButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    .end local v2    # "saveButton":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;
    :cond_0
    return-void
.end method

.method public disableInterceptTouchEvent(Z)V
    .locals 1
    .param p1, "disable"    # Z

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mCurrentMissionScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, p1}, Landroid/widget/ScrollView;->requestDisallowInterceptTouchEvent(Z)V

    .line 387
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 74
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 75
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    const-string v2, "CurrentMissionDetailActivity no receive mission object"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const-string v1, "EXTRA_NAME_SELECTED_MISSION_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 77
    const-string v1, "EXTRA_NAME_FROM_HISTORY"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mFromHistory:Z

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    if-nez v1, :cond_0

    .line 80
    const-string v1, "EXTRA_NAME_MISSION_ID"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mSelectedMissionId:I

    .line 81
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CurrentMissionDetailActivity selectedMissionId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mSelectedMissionId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mSelectedMissionId:I

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/util/SuggestedDataFactory;->createUserMissionDetailData(I)Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    .line 86
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    const v1, 0x7f030038

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->setContentView(I)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    if-nez v1, :cond_1

    .line 90
    sget-object v1, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    const-string v2, "MissionData is null."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->finish()V

    .line 105
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mMissionData:Lcom/sec/android/app/shealth/cignacoach/data/MissionData;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/cignacoach/data/MissionData;->getMissionDetailList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mNewMissionDetailList:Ljava/util/ArrayList;

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->initView()V

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->updateHeaderView()V

    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->updateTipTitleView()V

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->updateTrackingView()V

    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->updateDetailView()V

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->setListener()V

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->registerDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onDestroy()V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mDateChangeCallback:Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;

    invoke-static {v0}, Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService;->unregisterDayChangedListener(Lcom/sec/android/app/shealth/cignacoach/receiver/CoachIntentService$DateChageCallback;)V

    .line 119
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 110
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->setIntent(Landroid/content/Intent;)V

    .line 112
    sget-object v0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->TAG:Ljava/lang/String;

    const-string v1, "CurrentMissionDetailActivity onNewIntent"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mToast1:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mToast1:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/cignacoach/suggest/CurrentMissionDetailActivity;->mToast1:Landroid/widget/Toast;

    .line 130
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/cignacoach/language/CignaLoadGeneralActivity;->onStop()V

    .line 131
    return-void
.end method
