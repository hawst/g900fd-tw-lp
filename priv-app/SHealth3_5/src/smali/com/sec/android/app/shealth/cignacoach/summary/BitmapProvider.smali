.class public Lcom/sec/android/app/shealth/cignacoach/summary/BitmapProvider;
.super Ljava/lang/Object;
.source "BitmapProvider.java"


# instance fields
.field private mCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    const-wide/16 v4, 0x8

    div-long v0, v2, v4

    .line 21
    .local v0, "memory":J
    new-instance v2, Lcom/sec/android/app/shealth/cignacoach/summary/BitmapProvider$1;

    long-to-int v3, v0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/shealth/cignacoach/summary/BitmapProvider$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/summary/BitmapProvider;I)V

    iput-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/summary/BitmapProvider;->mCache:Landroid/support/v4/util/LruCache;

    .line 28
    return-void
.end method


# virtual methods
.method public getImage(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "requiredWidth"    # I
    .param p3, "requiredHeight"    # I

    .prologue
    .line 40
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/summary/BitmapProvider;->mCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 41
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-gt p2, v3, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-gt p3, v3, :cond_0

    .line 49
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    .line 46
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->getBitmapByPathWithCorrectingOrientationToNormal(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 47
    .local v1, "bitmapByPath":Landroid/graphics/Bitmap;
    invoke-static {v1, p2, p3}, Lcom/sec/android/app/shealth/common/utils/BitmapUtil;->cropAndScaleBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 48
    .local v2, "result":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/app/shealth/cignacoach/summary/BitmapProvider;->mCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v3, p1, v1}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v2

    .line 49
    goto :goto_0
.end method
