.class public Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
.super Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;
.source "SleepMonitorSummaryFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/sleepmonitor/IViewController;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;,
        Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;
    }
.end annotation


# static fields
.field private static final QUERY_INDIVIUAL_PART:Ljava/lang/String; = "SELECT rise_time as RISE_TIME, bed_time as BED_TIME, user_device__id as DEVICE_ID, quality AS QUALITY FROM sleep WHERE sync_status != 170004 AND bed_time >="

.field private static final QUERY_PART:Ljava/lang/String; = "SELECT count(*) as count, a.rise_time as RISE_TIME, a.user_device__id as DEVICE_ID, a.bed_time as BED_TIME, a.quality AS QUALITY, sum(a.rise_time-a.bed_time) AS RANGE,strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day, abs(a.bed_time-b.q) AS dayDataLong , sum(abs(a.rise_time-a.bed_time) * (a.efficiency/100)) as motionless_time_old, sum(abs(a.rise_time-a.bed_time))*ROUND((SUM(abs(a.rise_time-a.bed_time)*a.efficiency/100)*100)/sum(abs(a.rise_time-a.bed_time)),0)/100 AS motionless_time, AVG(a.efficiency) AS efficiency FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id WHERE a.sync_status != 170004 AND a.bed_time >="

.field private static final SETTING_WEARABLE_ID:Ljava/lang/String; = "content://settings/system/connected_wearable_id"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private HID:Ljava/text/SimpleDateFormat;

.field private HID_LAST_DAY:Ljava/text/SimpleDateFormat;

.field private averageSleepData:Landroid/widget/LinearLayout;

.field private averageSleepNoData:Landroid/widget/LinearLayout;

.field private connectionStatus:Landroid/view/View;

.field contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;

.field private date:J

.field dayEfficiency:F

.field private dayMotionLess:Landroid/widget/TextView;

.field private dayMotionLessBarPortion:Landroid/widget/LinearLayout;

.field private dayProgress:Landroid/widget/LinearLayout;

.field private dayTotalSleepBarPortion:Landroid/widget/LinearLayout;

.field public isSync:Z

.field private lastSync:Landroid/widget/TextView;

.field private mAlertDialog_GMLanch:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mAlertDialog_NotDevice:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mCreate:Z

.field private mainView:Landroid/view/View;

.field private monthMotionlessBarPortion:Landroid/widget/LinearLayout;

.field private monthProgress:Landroid/widget/LinearLayout;

.field private monthTotalSleepBarPortion:Landroid/widget/LinearLayout;

.field private motionLessSleepHour:Landroid/widget/TextView;

.field private motionLessSleepLabel:Landroid/widget/TextView;

.field private motionmonthAverage:Landroid/widget/TextView;

.field private sleepRatingBarInstanceOne:Landroid/widget/RatingBar;

.field private sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;

.field private statusObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;

.field private final tempCal:Ljava/util/Calendar;

.field private timeOfSleepInstanceOne:Landroid/widget/TextView;

.field private timeOfSleepInstanceTwo:Landroid/widget/TextView;

.field private timeOfSleepLayoutInstanceOne:Landroid/widget/RelativeLayout;

.field private timeOfSleepLayoutInstanceTwo:Landroid/widget/RelativeLayout;

.field private totalDaySleepTime:J

.field private totalSleepData:Landroid/widget/LinearLayout;

.field private totalSleepLabel:Landroid/widget/TextView;

.field private totalSleepNoData:Landroid/widget/LinearLayout;

.field private totalSleepTimeHour:Landroid/widget/TextView;

.field private updateTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->isSync:Z

    .line 93
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->tempCal:Ljava/util/Calendar;

    .line 1053
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mCreate:Z

    .line 1097
    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mAlertDialog_NotDevice:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1120
    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mAlertDialog_GMLanch:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 101
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 102
    return-void
.end method

.method private AlertDialog_NotDevoce()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1100
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const-string v1, "Sync Failed"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090d79

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$4;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mAlertDialog_NotDevice:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1118
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->isActivityTrackerConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RatingBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RatingBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceOne:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceTwo:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayProgress:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayTotalSleepBarPortion:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLessBarPortion:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLess:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->isGear2Connected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepData:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepNoData:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    .param p1, "x1"    # J

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalTimeForDay(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalDaySleepTime:J

    return-wide v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;J)J
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    .param p1, "x1"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalDaySleepTime:J

    return-wide p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;JI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    .param p1, "x1"    # J
    .param p3, "x2"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setTimeOfSleep(JI)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->isGear3Connected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->isGearFitConnected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->connectionStatus:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->updateConnectionStatus()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->updateLastSyncTime()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)Ljava/util/Date;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;J)Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;
    .param p1, "x1"    # J

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getSelectData(J)Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    move-result-object v0

    return-object v0
.end method

.method private getSelectData(J)Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;
    .locals 6
    .param p1, "time"    # J

    .prologue
    .line 998
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getSleepDataSelect(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v1

    .line 1000
    .local v1, "mCursor":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1002
    sget-object v2, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mCursor.getCount()="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "bed_time"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 1004
    :cond_0
    sget-object v2, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "null="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v4

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1005
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1007
    const/4 v0, 0x0

    .line 1015
    :goto_0
    return-object v0

    .line 1010
    :cond_1
    sget-object v2, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mCursor Main = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    new-instance v0, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;-><init>(Landroid/database/Cursor;)V

    .line 1013
    .local v0, "data":Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private isActivityTrackerConnected()Z
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 1156
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    if-nez v6, :cond_1

    .line 1186
    :cond_0
    :goto_0
    return v5

    .line 1159
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v7, "bluetooth"

    invoke-virtual {v6, v7}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    .line 1161
    .local v1, "btManager":Landroid/bluetooth/BluetoothManager;
    if-eqz v1, :cond_0

    .line 1164
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x12

    if-lt v6, v7, :cond_0

    .line 1165
    const/4 v6, 0x7

    invoke-virtual {v1, v6}, Landroid/bluetooth/BluetoothManager;->getConnectedDevices(I)Ljava/util/List;

    move-result-object v2

    .line 1167
    .local v2, "connectedList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v2, :cond_0

    .line 1168
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 1170
    .local v0, "btDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v0, :cond_2

    .line 1172
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1173
    .local v3, "deviceName":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 1175
    const-string v6, " "

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1176
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Samsung EI-AN900A"

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, " "

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1178
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private isGear2Connected()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1254
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1255
    .local v0, "appContext":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 1256
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable_id"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1257
    .local v1, "btAddress":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1259
    .local v2, "str":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 1261
    sget-object v4, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isGear2Connected = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " btAddress="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "GEAR2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1263
    sget-object v3, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v4, " Gear2 is connected"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1264
    const/4 v3, 0x1

    .line 1275
    .end local v1    # "btAddress":Ljava/lang/String;
    .end local v2    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 1266
    .restart local v1    # "btAddress":Ljava/lang/String;
    .restart local v2    # "str":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v5, " Gear2 is disconnected"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1270
    :cond_2
    sget-object v4, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v5, " connected_wearable is null OR wmanager_connected is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isGear3Connected()Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1215
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1216
    .local v0, "appContext":Landroid/content/Context;
    if-eqz v0, :cond_1

    .line 1217
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "connected_wearable_id"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1218
    .local v1, "btAddress":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "connected_wearable"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1220
    .local v3, "str":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    .line 1221
    sget-object v7, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isGear3Connected = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " btAddress="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "GEAR 3"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "GEAR S"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1223
    :cond_0
    sget-object v7, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v8, " Gear3 is connected"

    invoke-static {v7, v8}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string/jumbo v8, "wearable_connect_type"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1225
    .local v4, "type":Ljava/lang/String;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    .line 1226
    const-string v7, "#"

    invoke-virtual {v4, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1227
    .local v2, "lastToken":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 1230
    const-string v7, "2"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1249
    .end local v1    # "btAddress":Ljava/lang/String;
    .end local v2    # "lastToken":Ljava/lang/String;
    .end local v3    # "str":Ljava/lang/String;
    .end local v4    # "type":Ljava/lang/String;
    :cond_1
    :goto_0
    return v5

    .restart local v1    # "btAddress":Ljava/lang/String;
    .restart local v2    # "lastToken":Ljava/lang/String;
    .restart local v3    # "str":Ljava/lang/String;
    .restart local v4    # "type":Ljava/lang/String;
    :cond_2
    move v5, v6

    .line 1234
    goto :goto_0

    .end local v2    # "lastToken":Ljava/lang/String;
    :cond_3
    move v5, v6

    .line 1237
    goto :goto_0

    .line 1240
    .end local v4    # "type":Ljava/lang/String;
    :cond_4
    sget-object v6, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v7, " Gear3 is disconnected"

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1244
    :cond_5
    sget-object v6, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v7, " connected_wearable is null OR wmanager_connected is null"

    invoke-static {v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isGearFitConnected()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1190
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1191
    .local v0, "appContext":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 1192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable_id"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1193
    .local v1, "btAddress":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "connected_wearable"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1195
    .local v2, "str":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 1197
    sget-object v4, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isGearFitConnected = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " btAddress="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1198
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "WINGTIP"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1199
    sget-object v3, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v4, " Wingtip is connected"

    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    const/4 v3, 0x1

    .line 1210
    .end local v1    # "btAddress":Ljava/lang/String;
    .end local v2    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return v3

    .line 1202
    .restart local v1    # "btAddress":Ljava/lang/String;
    .restart local v2    # "str":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v5, " Wingtip is disconnected"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingI(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1206
    :cond_2
    sget-object v4, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v5, " connected_wearable is null OR wmanager_connected is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private mAlertDialog_GMLanch()V
    .locals 3

    .prologue
    .line 1123
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const-string v1, "Information"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090d7d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090048

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$5;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mAlertDialog_GMLanch:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 1152
    return-void
.end method

.method private setBars(Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;JJFF)V
    .locals 6
    .param p1, "data"    # Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;
    .param p2, "monthlyTime"    # J
    .param p4, "dayTime"    # J
    .param p6, "efficiency"    # F
    .param p7, "monthEfficiency"    # F

    .prologue
    .line 960
    cmp-long v4, p2, p4

    if-ltz v4, :cond_0

    .line 961
    long-to-float v4, p4

    long-to-float v5, p2

    div-float v3, v4, v5

    .line 962
    .local v3, "totalTimePortion":F
    long-to-float v4, p4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v5, p6, v5

    mul-float/2addr v4, v5

    long-to-float v5, p2

    div-float v0, v4, v5

    .line 964
    .local v0, "motionLessPortion":F
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x437a0000    # 250.0f

    mul-float/2addr v4, v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    const/4 v5, -0x1

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 965
    .local v1, "param":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayTotalSleepBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 966
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x437a0000    # 250.0f

    mul-float/2addr v4, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    const/4 v5, -0x1

    invoke-direct {v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 967
    .local v2, "param1":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLessBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 968
    invoke-direct {p0, p7}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setMonthProgress(F)V

    .line 978
    :goto_0
    return-void

    .line 970
    .end local v0    # "motionLessPortion":F
    .end local v1    # "param":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "param1":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "totalTimePortion":F
    :cond_0
    long-to-float v4, p2

    long-to-float v5, p4

    div-float v3, v4, v5

    .line 971
    .restart local v3    # "totalTimePortion":F
    long-to-float v4, p2

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v5, p7, v5

    mul-float/2addr v4, v5

    long-to-float v5, p4

    div-float v0, v4, v5

    .line 972
    .restart local v0    # "motionLessPortion":F
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x437a0000    # 250.0f

    mul-float/2addr v4, v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    const/4 v5, -0x1

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 973
    .restart local v1    # "param":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthTotalSleepBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 974
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v4, 0x437a0000    # 250.0f

    mul-float/2addr v4, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    const/4 v5, -0x1

    invoke-direct {v2, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 975
    .restart local v2    # "param1":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthMotionlessBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 976
    invoke-direct {p0, p6}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setDayProgress(F)V

    goto :goto_0
.end method

.method private setDayProgress(F)V
    .locals 6
    .param p1, "dayEfficiency"    # F

    .prologue
    const/4 v5, -0x1

    const/high16 v4, 0x437a0000    # 250.0f

    .line 989
    const/high16 v3, 0x42c80000    # 100.0f

    div-float v0, p1, v3

    .line 990
    .local v0, "motionDayLessPortion":F
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v1, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 991
    .local v1, "param2":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayTotalSleepBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 992
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    mul-float v3, v4, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 993
    .local v2, "param3":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLessBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 994
    return-void
.end method

.method private setMonthProgress(F)V
    .locals 6
    .param p1, "monthEfficiency"    # F

    .prologue
    const/4 v5, -0x1

    const/high16 v4, 0x437a0000    # 250.0f

    .line 981
    const/high16 v3, 0x42c80000    # 100.0f

    div-float v0, p1, v3

    .line 982
    .local v0, "motionMonthLessPortion":F
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v1, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 983
    .local v1, "param2":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthTotalSleepBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 984
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    mul-float v3, v4, v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 985
    .local v2, "param3":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthMotionlessBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 986
    return-void
.end method

.method private setTimeOfSleep(JI)V
    .locals 25
    .param p1, "dayTime"    # J
    .param p3, "count"    # I

    .prologue
    .line 499
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceOne:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 500
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceOne:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 501
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceTwo:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 503
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceTwo:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 504
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 516
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v15

    .line 518
    .local v15, "from":J
    invoke-static/range {p1 .. p2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getEndOfDay(J)J

    move-result-wide v23

    .line 520
    .local v23, "to":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT rise_time as RISE_TIME, bed_time as BED_TIME, user_device__id as DEVICE_ID, quality AS QUALITY FROM sleep WHERE sync_status != 170004 AND bed_time >="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND bed_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v23

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ORDER BY bed_time ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 521
    .local v5, "query":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 523
    .local v11, "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatDateMonth(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->HID:Ljava/text/SimpleDateFormat;

    .line 524
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->HID_LAST_DAY:Ljava/text/SimpleDateFormat;

    .line 526
    if-eqz v11, :cond_6

    .line 527
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 531
    const/4 v2, 0x1

    move/from16 v0, p3

    if-eq v0, v2, :cond_0

    const/4 v2, 0x2

    move/from16 v0, p3

    if-ne v0, v2, :cond_3

    .line 532
    :cond_0
    const-string v2, "BED_TIME"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 533
    .local v9, "bedTime":J
    const-string v2, "RISE_TIME"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v19

    .line 534
    .local v19, "riseTime":J
    const-string v2, "QUALITY"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 535
    .local v17, "quality":I
    move/from16 v0, v17

    int-to-float v2, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float v18, v2, v3

    .line 536
    .local v18, "rating":F
    const-string v2, "DEVICE_ID"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 537
    .local v14, "device":Ljava/lang/String;
    if-nez v14, :cond_1

    .line 538
    const-string v14, "10019"

    .line 539
    :cond_1
    const-string v2, "_"

    invoke-virtual {v14, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 541
    .local v8, "accessory":[Ljava/lang/String;
    if-eqz v8, :cond_7

    const-string v2, ""

    const/4 v3, 0x0

    aget-object v3, v8, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x0

    aget-object v2, v8, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x272e

    if-lt v2, v3, :cond_7

    const/4 v2, 0x0

    cmpl-float v2, v18, v2

    if-eqz v2, :cond_7

    .line 542
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 546
    :goto_0
    cmp-long v2, v9, v19

    if-lez v2, :cond_2

    .line 549
    move-wide/from16 v21, v9

    .line 550
    .local v21, "swapper":J
    move-wide/from16 v19, v9

    .line 551
    move-wide/from16 v9, v21

    .line 553
    .end local v21    # "swapper":J
    :cond_2
    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    .line 554
    .local v12, "date":Ljava/util/Date;
    invoke-virtual {v12, v9, v10}, Ljava/util/Date;->setTime(J)V

    .line 556
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->HID:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 557
    .local v13, "dayText":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ~ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 558
    move-wide/from16 v0, v19

    invoke-virtual {v12, v0, v1}, Ljava/util/Date;->setTime(J)V

    .line 561
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->HID:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 565
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceOne:Landroid/widget/TextView;

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 566
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/widget/RatingBar;->setRating(F)V

    .line 567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceTwo:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 570
    .end local v8    # "accessory":[Ljava/lang/String;
    .end local v9    # "bedTime":J
    .end local v12    # "date":Ljava/util/Date;
    .end local v13    # "dayText":Ljava/lang/String;
    .end local v14    # "device":Ljava/lang/String;
    .end local v17    # "quality":I
    .end local v18    # "rating":F
    .end local v19    # "riseTime":J
    :cond_3
    const/4 v2, 0x2

    move/from16 v0, p3

    if-ne v0, v2, :cond_a

    .line 571
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceTwo:Landroid/widget/RelativeLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 573
    invoke-interface {v11}, Landroid/database/Cursor;->moveToLast()Z

    .line 574
    const-string v2, "BED_TIME"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 575
    .restart local v9    # "bedTime":J
    const-string v2, "RISE_TIME"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v19

    .line 576
    .restart local v19    # "riseTime":J
    const-string v2, "QUALITY"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 577
    .restart local v17    # "quality":I
    move/from16 v0, v17

    int-to-float v2, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float v18, v2, v3

    .line 578
    .restart local v18    # "rating":F
    const-string v2, "DEVICE_ID"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 579
    .restart local v14    # "device":Ljava/lang/String;
    if-nez v14, :cond_4

    .line 580
    const-string v14, "10019"

    .line 581
    :cond_4
    const-string v2, "_"

    invoke-virtual {v14, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 582
    .restart local v8    # "accessory":[Ljava/lang/String;
    if-eqz v8, :cond_8

    const-string v2, ""

    const/4 v3, 0x0

    aget-object v3, v8, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x0

    aget-object v2, v8, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x272e

    if-lt v2, v3, :cond_8

    const/4 v2, 0x0

    cmpl-float v2, v18, v2

    if-eqz v2, :cond_8

    .line 583
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 587
    :goto_1
    cmp-long v2, v9, v19

    if-lez v2, :cond_5

    .line 590
    move-wide/from16 v21, v9

    .line 591
    .restart local v21    # "swapper":J
    move-wide/from16 v19, v9

    .line 592
    move-wide/from16 v9, v21

    .line 594
    .end local v21    # "swapper":J
    :cond_5
    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    .line 595
    .restart local v12    # "date":Ljava/util/Date;
    invoke-virtual {v12, v9, v10}, Ljava/util/Date;->setTime(J)V

    .line 597
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->HID:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 598
    .restart local v13    # "dayText":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ~ "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 599
    move-wide/from16 v0, v19

    invoke-virtual {v12, v0, v1}, Ljava/util/Date;->setTime(J)V

    .line 601
    move-wide/from16 v0, v19

    invoke-static {v0, v1, v9, v10}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isSameYear(JJ)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 602
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->HID:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 606
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceTwo:Landroid/widget/TextView;

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 607
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/widget/RatingBar;->setRating(F)V

    .line 618
    .end local v8    # "accessory":[Ljava/lang/String;
    .end local v9    # "bedTime":J
    .end local v12    # "date":Ljava/util/Date;
    .end local v13    # "dayText":Ljava/lang/String;
    .end local v14    # "device":Ljava/lang/String;
    .end local v17    # "quality":I
    .end local v18    # "rating":F
    .end local v19    # "riseTime":J
    :cond_6
    :goto_3
    return-void

    .line 544
    .restart local v8    # "accessory":[Ljava/lang/String;
    .restart local v9    # "bedTime":J
    .restart local v14    # "device":Ljava/lang/String;
    .restart local v17    # "quality":I
    .restart local v18    # "rating":F
    .restart local v19    # "riseTime":J
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 585
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    goto/16 :goto_1

    .line 604
    .restart local v12    # "date":Ljava/util/Date;
    .restart local v13    # "dayText":Ljava/lang/String;
    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->HID_LAST_DAY:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_2

    .line 611
    .end local v8    # "accessory":[Ljava/lang/String;
    .end local v9    # "bedTime":J
    .end local v12    # "date":Ljava/util/Date;
    .end local v13    # "dayText":Ljava/lang/String;
    .end local v14    # "device":Ljava/lang/String;
    .end local v17    # "quality":I
    .end local v18    # "rating":F
    .end local v19    # "riseTime":J
    :cond_a
    const/4 v2, 0x2

    move/from16 v0, p3

    if-le v0, v2, :cond_6

    .line 612
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceOne:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090d6d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 613
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 614
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceTwo:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_3
.end method

.method private totalTimeForDay(J)Ljava/lang/String;
    .locals 19
    .param p1, "dayTime"    # J

    .prologue
    .line 621
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 622
    .local v8, "cal":Ljava/util/Calendar;
    move-wide/from16 v0, p1

    invoke-virtual {v8, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 623
    const/16 v2, 0xb

    invoke-virtual {v8, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0xc

    if-ge v2, v3, :cond_0

    .line 624
    const/4 v2, 0x5

    const/4 v3, -0x1

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 626
    :cond_0
    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 627
    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 628
    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 629
    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 630
    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 631
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    .line 632
    .local v12, "from":J
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 633
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v15

    .line 634
    .local v15, "to":J
    const-wide/16 v17, 0x0

    .line 635
    .local v17, "totalSleepDuration":J
    const/4 v11, 0x0

    .line 636
    .local v11, "efficiency":F
    const/4 v14, 0x0

    .line 638
    .local v14, "motionLessTime":F
    const/4 v9, 0x0

    .line 640
    .local v9, "count":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT count(*) as count, a.rise_time as RISE_TIME, a.user_device__id as DEVICE_ID, a.bed_time as BED_TIME, a.quality AS QUALITY, sum(a.rise_time-a.bed_time) AS RANGE,strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day, abs(a.bed_time-b.q) AS dayDataLong , sum(abs(a.rise_time-a.bed_time) * (a.efficiency/100)) as motionless_time_old, sum(abs(a.rise_time-a.bed_time))*ROUND((SUM(abs(a.rise_time-a.bed_time)*a.efficiency/100)*100)/sum(abs(a.rise_time-a.bed_time)),0)/100 AS motionless_time, AVG(a.efficiency) AS efficiency FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id WHERE a.sync_status != 170004 AND a.bed_time >="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND a.bed_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GROUP BY day ORDER BY a.bed_time ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 641
    .local v5, "query":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 643
    .local v10, "cursor":Landroid/database/Cursor;
    if-eqz v10, :cond_2

    .line 644
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 645
    const-string v2, "count"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 646
    const-string v2, "count"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 647
    const-string v2, "RANGE"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v17

    .line 648
    const-string v2, "efficiency"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v11

    .line 650
    move-wide/from16 v0, v17

    long-to-float v2, v0

    mul-float/2addr v2, v11

    const/high16 v3, 0x42c80000    # 100.0f

    div-float v14, v2, v3

    .line 663
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 668
    :cond_2
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v11}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 653
    :cond_3
    const-string v2, "RANGE"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    add-long v17, v17, v2

    .line 654
    const-string/jumbo v2, "motionless_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v14

    .line 656
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 657
    move-wide/from16 v0, v17

    long-to-float v2, v0

    div-float v2, v14, v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v2, v2

    int-to-float v11, v2

    goto :goto_0

    .line 664
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private totalTimeForLast30Days(J)Ljava/lang/String;
    .locals 19
    .param p1, "dayTime"    # J

    .prologue
    .line 672
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 673
    .local v8, "cal":Ljava/util/Calendar;
    move-wide/from16 v0, p1

    invoke-virtual {v8, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 674
    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 675
    const/16 v2, 0xe

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 676
    const/16 v2, 0xa

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 677
    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 678
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v15

    .line 679
    .local v15, "to":J
    const/4 v2, 0x5

    const/16 v3, -0x1e

    invoke-virtual {v8, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 680
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    .line 681
    .local v12, "from":J
    const-wide/16 v17, 0x0

    .line 682
    .local v17, "totalSleepDuration":J
    const/4 v11, 0x0

    .line 683
    .local v11, "efficiency":F
    const/4 v9, 0x0

    .line 684
    .local v9, "count":I
    const/4 v14, 0x0

    .line 686
    .local v14, "motionLessTime":F
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT count(*) as count, a.rise_time as RISE_TIME, a.user_device__id as DEVICE_ID, a.bed_time as BED_TIME, a.quality AS QUALITY, sum(a.rise_time-a.bed_time) AS RANGE,strftime(\'%Y-%m-%d\', (a.bed_time-b.q)/1000,\'unixepoch\',\'localtime\') AS day, abs(a.bed_time-b.q) AS dayDataLong , sum(abs(a.rise_time-a.bed_time) * (a.efficiency/100)) as motionless_time_old, sum(abs(a.rise_time-a.bed_time))*ROUND((SUM(abs(a.rise_time-a.bed_time)*a.efficiency/100)*100)/sum(abs(a.rise_time-a.bed_time)),0)/100 AS motionless_time, AVG(a.efficiency) AS efficiency FROM sleep AS a LEFT OUTER JOIN(SELECT c._id, abs(RISE_Time - BED_TIME)/3600000 AS range, CASE WHEN cast(strftime(\'%H\', strftime(\'%Y-%m-%d %H:%m\', bed_time/1000,\'unixepoch\',\'localtime\')) as integer) BETWEEN 0 AND 12 THEN 43200000 ELSE 0 END as q FROM sleep AS c) AS b ON b._id = a._id WHERE a.sync_status != 170004 AND a.bed_time >="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND bed_time < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide v0, v15

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GROUP BY day ORDER BY bed_time ASC;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 687
    .local v5, "query":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 689
    .local v10, "cursor":Landroid/database/Cursor;
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 690
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 691
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_0

    .line 692
    add-int/lit8 v9, v9, 0x1

    .line 693
    const-string v2, "RANGE"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    add-long v17, v17, v2

    .line 694
    const-string/jumbo v2, "motionless_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v14, v2

    .line 695
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 698
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 700
    if-lez v9, :cond_1

    .line 701
    move-wide/from16 v0, v17

    long-to-float v2, v0

    int-to-float v3, v9

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v17, v0

    .line 702
    int-to-float v2, v9

    div-float v2, v14, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v14, v2

    .line 704
    :cond_1
    move-wide/from16 v0, v17

    long-to-float v2, v0

    div-float v2, v14, v2

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-int v2, v2

    int-to-float v11, v2

    .line 706
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v11}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private updateConnectionStatus()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->connectionStatus:Landroid/view/View;

    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$2;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 270
    return-void
.end method

.method private updateLastSyncTime()V
    .locals 6

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getLastUpdateTime(Landroid/content/Context;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->updateTime:J

    .line 256
    iget-wide v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->updateTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 258
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getDateFormatFull(Landroid/content/Context;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    iget-wide v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->updateTime:J

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-wide v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->updateTime:J

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->getTimeFormat(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "lastUpdate":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->lastSync:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09081a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    .end local v0    # "lastUpdate":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method protected getCalendarActivityClass()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1293
    const-class v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepCalendarActivity;

    return-object v0
.end method

.method protected getColumnNameForTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1288
    const-string/jumbo v0, "rise_time"

    return-object v0
.end method

.method protected getContentURI()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1028
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method protected getContentView(Landroid/content/Context;)Landroid/view/View;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 110
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 111
    const-string v3, "Date"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 112
    const-string v3, "Date"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->date:J

    .line 116
    :cond_0
    iget-wide v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->date:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    .line 117
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->tempCal:Ljava/util/Calendar;

    iget-wide v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->date:J

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->tempCal:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setText(Ljava/util/Date;)V

    .line 122
    :cond_1
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 124
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030217

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->getDateSelectorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x106000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 127
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f08096e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepLabel:Landroid/widget/TextView;

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080971

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepTimeHour:Landroid/widget/TextView;

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080972

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLess:Landroid/widget/TextView;

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080973

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayProgress:Landroid/widget/LinearLayout;

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080974

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayTotalSleepBarPortion:Landroid/widget/LinearLayout;

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080975

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLessBarPortion:Landroid/widget/LinearLayout;

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080976

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceOne:Landroid/widget/RelativeLayout;

    .line 141
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080977

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceOne:Landroid/widget/TextView;

    .line 142
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080978

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RatingBar;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceOne:Landroid/widget/RatingBar;

    .line 143
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080979

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepLayoutInstanceTwo:Landroid/widget/RelativeLayout;

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f08097a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceTwo:Landroid/widget/TextView;

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f08097b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RatingBar;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->sleepRatingBarInstanceTwo:Landroid/widget/RatingBar;

    .line 147
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceOne:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->bringToFront()V

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->timeOfSleepInstanceTwo:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->bringToFront()V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080970

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepData:Landroid/widget/LinearLayout;

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f08096f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalSleepNoData:Landroid/widget/LinearLayout;

    .line 152
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f08097f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->averageSleepData:Landroid/widget/LinearLayout;

    .line 153
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f08097e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->averageSleepNoData:Landroid/widget/LinearLayout;

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080982

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthProgress:Landroid/widget/LinearLayout;

    .line 156
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080983

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthTotalSleepBarPortion:Landroid/widget/LinearLayout;

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080984

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthMotionlessBarPortion:Landroid/widget/LinearLayout;

    .line 159
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f08097d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepLabel:Landroid/widget/TextView;

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090d65

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x1e

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080980

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    .line 166
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080981

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionmonthAverage:Landroid/widget/TextView;

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080986

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->connectionStatus:Landroid/view/View;

    .line 170
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080985

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->lastSync:Landroid/widget/TextView;

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    const v4, 0x7f080987

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 175
    .local v1, "btnExerciseMateGraph":Landroid/widget/ImageButton;
    new-instance v3, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$1;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->AlertDialog_NotDevoce()V

    .line 184
    invoke-direct {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mAlertDialog_GMLanch()V

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mainView:Landroid/view/View;

    return-object v3
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 1034
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    if-eqz v1, :cond_0

    .line 1035
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;

    .line 1036
    .local v0, "activity":Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->performPopStackAnimation()V

    .line 1037
    sget-object v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v2, "SleepMonitorSummaryFragment backpressed"

    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    .end local v0    # "activity":Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onBackPressed()V

    .line 1041
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 249
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 250
    .local v0, "view":Landroid/view/View;
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 251
    return-object v0
.end method

.method public onDateChanged(Ljava/util/Date;)V
    .locals 1
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 1045
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDateChanged(Ljava/util/Date;)V

    .line 1047
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1048
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setSelecteddate(Ljava/util/Date;)V

    .line 1049
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->update()V

    .line 1051
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 773
    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;

    .line 774
    iput-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->statusObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;

    .line 775
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onDestroy()V

    .line 776
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 760
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onPause()V

    .line 762
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;

    if-eqz v0, :cond_0

    .line 763
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 765
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->statusObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;

    if-eqz v0, :cond_1

    .line 766
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->statusObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 769
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1058
    new-instance v4, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->statusObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;

    .line 1059
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://settings/system/connected_wearable_id"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->statusObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$WearableStatusObserver;

    invoke-virtual {v4, v5, v7, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1061
    new-instance v4, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;

    .line 1062
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->contentObserver:Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$SleepDBContentObserver;

    invoke-virtual {v4, v5, v7, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1064
    :try_start_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/SummaryFragment;->onResume()V

    .line 1066
    iget-boolean v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mCreate:Z

    if-eqz v4, :cond_2

    .line 1069
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 1070
    .local v3, "tempCal":Ljava/util/Calendar;
    iget-wide v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->date:J

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1071
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    iput-object v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    .line 1073
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getSleepDataSelect(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v0

    .line 1074
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1075
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-static {v4, v5}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getPrevDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v2

    .line 1076
    .local v2, "temp":Ljava/util/Date;
    if-eqz v2, :cond_0

    .line 1078
    iput-object v2, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1081
    .end local v2    # "temp":Ljava/util/Date;
    :cond_0
    if-eqz v0, :cond_1

    .line 1083
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1088
    :cond_1
    :goto_0
    const/4 v4, 0x0

    :try_start_2
    iput-boolean v4, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mCreate:Z

    .line 1090
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "tempCal":Ljava/util/Calendar;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->update()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1095
    :goto_1
    return-void

    .line 1091
    :catch_0
    move-exception v1

    .line 1092
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1084
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v3    # "tempCal":Ljava/util/Calendar;
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method protected onSytemDateChanged()V
    .locals 1

    .prologue
    .line 1023
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->mCreate:Z

    .line 1024
    return-void
.end method

.method public resetDataConfirm(Z)V
    .locals 3
    .param p1, "daily"    # Z

    .prologue
    .line 1280
    sget-object v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->TAG:Ljava/lang/String;

    const-string v1, "Resetting daily data"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->loggingD(Ljava/lang/String;Ljava/lang/String;)V

    .line 1281
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-static {v0, v1, v2, p1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->sleepDeleteAll(Landroid/content/Context;JZ)I

    .line 1282
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->update()V

    .line 1283
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setNextAndPrevDates()V

    .line 1284
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 105
    return-void
.end method

.method public setMonthData(Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;)V
    .locals 28
    .param p1, "data"    # Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;

    .prologue
    .line 779
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalTimeForLast30Days(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 780
    .local v10, "args":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v10, v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 781
    .local v4, "averageMonthlySleepTime":J
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-nez v2, :cond_1

    .line 783
    if-eqz p1, :cond_0

    .line 784
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalDaySleepTime:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayEfficiency:F

    const/4 v9, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setBars(Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;JJFF)V

    .line 785
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthProgress:Landroid/widget/LinearLayout;

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 786
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthProgress:Landroid/widget/LinearLayout;

    const/4 v3, 0x2

    const/4 v6, 0x2

    const/4 v7, 0x2

    const/4 v8, 0x2

    invoke-virtual {v2, v3, v6, v7, v8}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 787
    new-instance v21, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x1

    move-object/from16 v0, v21

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 788
    .local v21, "param":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthTotalSleepBarPortion:Landroid/widget/LinearLayout;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 789
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthTotalSleepBarPortion:Landroid/widget/LinearLayout;

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 790
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthMotionlessBarPortion:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 791
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionmonthAverage:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f090d64

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v27, "--"

    aput-object v27, v7, v8

    const/4 v8, 0x1

    const-string v27, "--"

    aput-object v27, v7, v8

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 792
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 793
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f090d88

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 795
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->averageSleepData:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 796
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->averageSleepNoData:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 957
    .end local v21    # "param":Landroid/widget/LinearLayout$LayoutParams;
    :goto_0
    return-void

    .line 800
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->averageSleepData:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 801
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->averageSleepNoData:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 802
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 807
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthProgress:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 808
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthProgress:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2, v3, v6, v7, v8}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 809
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthTotalSleepBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f070224

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 810
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->monthMotionlessBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f070223

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 811
    const-wide/32 v2, 0xea60

    div-long v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v15

    .line 812
    .local v15, "monthAverageTotalMinute":J
    long-to-int v2, v15

    div-int/lit8 v13, v2, 0x3c

    .line 813
    .local v13, "monthAverageHour":I
    long-to-int v2, v15

    rem-int/lit8 v14, v2, 0x3c

    .line 814
    .local v14, "monthAverageMinute":I
    const-string v23, ""

    .line 815
    .local v23, "talkBackAvgHour":Ljava/lang/String;
    const-string v24, ""

    .line 816
    .local v24, "talkBackAvgMin":Ljava/lang/String;
    const-string v25, ""

    .line 817
    .local v25, "talkbackAvgMotionlessHour":Ljava/lang/String;
    const-string v26, ""

    .line 818
    .local v26, "talkbackAvgMotionlessMin":Ljava/lang/String;
    if-nez v13, :cond_4

    .line 820
    const-string v23, ""

    .line 821
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 822
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090f6d

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 825
    .local v22, "str":Ljava/lang/String;
    :goto_1
    new-instance v11, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v22

    invoke-direct {v11, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 826
    .local v11, "builder":Landroid/text/SpannableStringBuilder;
    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v3, 0x1e

    const/4 v6, 0x1

    invoke-direct {v2, v3, v6}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/4 v3, 0x0

    const/4 v6, 0x2

    const/16 v7, 0x21

    invoke-virtual {v11, v2, v3, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 828
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 829
    const/4 v2, 0x1

    if-ne v14, v2, :cond_3

    .line 830
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 891
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v13, :cond_b

    const-string v2, ""

    :goto_3
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v14, :cond_c

    const-string v2, ""

    :goto_4
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 892
    const/4 v2, 0x2

    aget-object v2, v10, v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const v3, 0xea60

    div-int/2addr v2, v3

    int-to-long v0, v2

    move-wide/from16 v17, v0

    .line 893
    .local v17, "monthlyAverageMotionlessMinute":J
    const/4 v2, 0x1

    aget-object v2, v10, v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v9

    .line 894
    .local v9, "monthlyEfficiency":F
    move-wide/from16 v0, v17

    long-to-int v2, v0

    div-int/lit8 v19, v2, 0x3c

    .line 895
    .local v19, "motionlessHour":I
    move-wide/from16 v0, v17

    long-to-int v2, v0

    rem-int/lit8 v20, v2, 0x3c

    .line 898
    .local v20, "motionlessMinute":I
    const/4 v2, 0x1

    move/from16 v0, v19

    if-ne v0, v2, :cond_10

    .line 899
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 900
    const/4 v2, 0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_e

    .line 901
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 902
    const v2, 0x7f090bfc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 906
    .local v12, "label":Ljava/lang/String;
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 941
    :goto_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "(%d"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 942
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 943
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionmonthAverage:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    float-to-int v0, v9

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v7, v8

    invoke-static {v6, v12, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "%)"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 946
    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionmonthAverage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090d8e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v19, :cond_18

    const-string v2, ""

    :goto_8
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-nez v20, :cond_19

    const-string v2, ""

    :goto_9
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    float-to-int v6, v9

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "%"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 947
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionmonthAverage:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 949
    if-eqz p1, :cond_1a

    .line 950
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayTotalSleepBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f070221

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 951
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayMotionLessBarPortion:Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f070222

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 956
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->totalDaySleepTime:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->dayEfficiency:F

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v9}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setBars(Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataItem;JJFF)V

    goto/16 :goto_0

    .line 824
    .end local v9    # "monthlyEfficiency":F
    .end local v11    # "builder":Landroid/text/SpannableStringBuilder;
    .end local v12    # "label":Ljava/lang/String;
    .end local v17    # "monthlyAverageMotionlessMinute":J
    .end local v19    # "motionlessHour":I
    .end local v20    # "motionlessMinute":I
    .end local v22    # "str":Ljava/lang/String;
    :cond_2
    const v2, 0x7f090f6d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "str":Ljava/lang/String;
    goto/16 :goto_1

    .line 832
    .restart local v11    # "builder":Landroid/text/SpannableStringBuilder;
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_2

    .line 837
    .end local v11    # "builder":Landroid/text/SpannableStringBuilder;
    .end local v22    # "str":Ljava/lang/String;
    :cond_4
    const/4 v2, 0x1

    if-ne v13, v2, :cond_6

    .line 838
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 839
    const v2, 0x7f090bff

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 843
    .restart local v22    # "str":Ljava/lang/String;
    :goto_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 854
    :goto_b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 856
    new-instance v11, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v22

    invoke-direct {v11, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 862
    .restart local v11    # "builder":Landroid/text/SpannableStringBuilder;
    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v3, 0x1e

    const/4 v6, 0x1

    invoke-direct {v2, v3, v6}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/4 v3, 0x0

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {v11, v2, v3, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 863
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 864
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 866
    const/4 v2, 0x1

    if-ne v14, v2, :cond_9

    .line 867
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 868
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090c00

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 872
    :goto_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 882
    :goto_d
    new-instance v11, Landroid/text/SpannableStringBuilder;

    .end local v11    # "builder":Landroid/text/SpannableStringBuilder;
    move-object/from16 v0, v22

    invoke-direct {v11, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 888
    .restart local v11    # "builder":Landroid/text/SpannableStringBuilder;
    new-instance v2, Landroid/text/style/AbsoluteSizeSpan;

    const/16 v3, 0x1e

    const/4 v6, 0x1

    invoke-direct {v2, v3, v6}, Landroid/text/style/AbsoluteSizeSpan;-><init>(IZ)V

    const/4 v3, 0x0

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {v11, v2, v3, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 889
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionLessSleepHour:Landroid/widget/TextView;

    invoke-virtual {v2, v11}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 841
    .end local v11    # "builder":Landroid/text/SpannableStringBuilder;
    .end local v22    # "str":Ljava/lang/String;
    :cond_5
    const v2, 0x7f090d71

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "str":Ljava/lang/String;
    goto/16 :goto_a

    .line 846
    .end local v22    # "str":Ljava/lang/String;
    :cond_6
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 847
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090d72

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 851
    .restart local v22    # "str":Ljava/lang/String;
    :goto_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    goto/16 :goto_b

    .line 849
    .end local v22    # "str":Ljava/lang/String;
    :cond_7
    const v2, 0x7f090d72

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .restart local v22    # "str":Ljava/lang/String;
    goto :goto_e

    .line 870
    .restart local v11    # "builder":Landroid/text/SpannableStringBuilder;
    :cond_8
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090d73

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto/16 :goto_c

    .line 875
    :cond_9
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 876
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090f6d

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 880
    :goto_f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_d

    .line 878
    :cond_a
    const v2, 0x7f090f6d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    goto :goto_f

    .line 891
    :cond_b
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_3

    :cond_c
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_4

    .line 904
    .restart local v9    # "monthlyEfficiency":F
    .restart local v17    # "monthlyAverageMotionlessMinute":J
    .restart local v19    # "motionlessHour":I
    .restart local v20    # "motionlessMinute":I
    :cond_d
    const v2, 0x7f090d62

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "label":Ljava/lang/String;
    goto/16 :goto_5

    .line 908
    .end local v12    # "label":Ljava/lang/String;
    :cond_e
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 909
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090bfb

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 913
    .restart local v12    # "label":Ljava/lang/String;
    :goto_10
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_6

    .line 911
    .end local v12    # "label":Ljava/lang/String;
    :cond_f
    const v2, 0x7f090d61

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "label":Ljava/lang/String;
    goto :goto_10

    .line 916
    .end local v12    # "label":Ljava/lang/String;
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 917
    const/4 v2, 0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_12

    .line 918
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 919
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090bfd

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 923
    .restart local v12    # "label":Ljava/lang/String;
    :goto_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_6

    .line 921
    .end local v12    # "label":Ljava/lang/String;
    :cond_11
    const v2, 0x7f090d63

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "label":Ljava/lang/String;
    goto :goto_11

    .line 924
    .end local v12    # "label":Ljava/lang/String;
    :cond_12
    if-nez v19, :cond_15

    .line 925
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 926
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090d6f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 929
    .restart local v12    # "label":Ljava/lang/String;
    :goto_12
    const-string v25, ""

    .line 930
    const/4 v2, 0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_14

    .line 931
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_6

    .line 928
    .end local v12    # "label":Ljava/lang/String;
    :cond_13
    const v2, 0x7f090d6f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "label":Ljava/lang/String;
    goto :goto_12

    .line 933
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_6

    .line 935
    .end local v12    # "label":Ljava/lang/String;
    :cond_15
    invoke-static {}, Lcom/sec/android/app/shealth/sleepmonitor/utils/Utils;->isArabLocale()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 936
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const v3, 0x7f090d60

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v2, v3, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 939
    .restart local v12    # "label":Ljava/lang/String;
    :goto_13
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090dc4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_6

    .line 938
    .end local v12    # "label":Ljava/lang/String;
    :cond_16
    const v2, 0x7f090d60

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "label":Ljava/lang/String;
    goto :goto_13

    .line 945
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->motionmonthAverage:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    float-to-int v8, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v12, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "%)"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 946
    :cond_18
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_8

    :cond_19
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_9

    .line 953
    :cond_1a
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->setMonthProgress(F)V

    goto/16 :goto_0
.end method

.method protected setNextAndPrevDates()V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 199
    const/4 v6, 0x0

    .line 202
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "bed_time DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 203
    if-eqz v6, :cond_0

    .line 205
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :cond_0
    if-eqz v6, :cond_1

    .line 213
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 225
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getPrevDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v8

    .line 226
    .local v8, "prevdate":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getNextDateOfData(Landroid/content/Context;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v7

    .line 228
    .local v7, "nextdate":Ljava/util/Date;
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 230
    const/4 v7, 0x0

    .line 233
    :cond_2
    if-nez v7, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->selectedDate:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/sleepmonitor/core/data/SleepDataUtil;->getStartOfDay(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 235
    new-instance v7, Ljava/util/Date;

    .end local v7    # "nextdate":Ljava/util/Date;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 239
    .restart local v7    # "nextdate":Ljava/util/Date;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setPrevDate(Ljava/util/Date;)V

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setNextDate(Ljava/util/Date;)V

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v1

    if-eqz v8, :cond_5

    move v0, v9

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setLeftArrowEnabled(Z)V

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getDateSelector()Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;

    move-result-object v0

    if-eqz v7, :cond_6

    :goto_1
    invoke-virtual {v0, v9}, Lcom/sec/android/app/shealth/framework/ui/common/DateSelector;->setRightArrowEnabled(Z)V

    .line 245
    return-void

    .line 212
    .end local v7    # "nextdate":Ljava/util/Date;
    .end local v8    # "prevdate":Ljava/util/Date;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 213
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .restart local v7    # "nextdate":Ljava/util/Date;
    .restart local v8    # "prevdate":Ljava/util/Date;
    :cond_5
    move v0, v10

    .line 242
    goto :goto_0

    :cond_6
    move v9, v10

    .line 243
    goto :goto_1
.end method

.method public update()V
    .locals 2

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 478
    :goto_0
    return-void

    .line 276
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment$3;-><init>(Lcom/sec/android/app/shealth/sleepmonitor/SleepMonitorSummaryFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
