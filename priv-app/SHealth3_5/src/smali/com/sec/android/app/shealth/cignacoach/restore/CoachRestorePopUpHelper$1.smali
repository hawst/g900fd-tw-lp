.class Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;
.super Ljava/lang/Object;
.source "CoachRestorePopUpHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->showRestoreTriggerPopup(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

.field final synthetic val$isLocalDataPresent:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Z)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    iput-boolean p2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->val$isLocalDataPresent:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 137
    new-instance v1, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->activity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->dialogButtonType:I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$500()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->popupLayoutRID:I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$000()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->titleId:I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$300()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090139

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v1

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->popupLayoutRID:I
    invoke-static {}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$000()I

    move-result v2

    new-instance v3, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1$1;-><init>(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    move-result-object v0

    .line 185
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    # setter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1, v2}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$602(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->mConfirmDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$600(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper$1;->this$0:Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;

    # getter for: Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;->access$400(Lcom/sec/android/app/shealth/cignacoach/restore/CoachRestorePopUpHelper;)Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "COACH_PROMPT_RESTORE"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 187
    return-void
.end method
