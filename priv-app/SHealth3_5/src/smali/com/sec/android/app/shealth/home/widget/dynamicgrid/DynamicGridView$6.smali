.class Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;
.super Ljava/lang/Object;
.source "DynamicGridView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->handleCellSwitch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

.field final synthetic val$deltaX:I

.field final synthetic val$deltaY:I

.field final synthetic val$finalTargetPosition:I

.field final synthetic val$observer:Landroid/view/ViewTreeObserver;

.field final synthetic val$originalPosition:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;Landroid/view/ViewTreeObserver;IIII)V
    .locals 0

    .prologue
    .line 571
    iput-object p1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iput-object p2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$observer:Landroid/view/ViewTreeObserver;

    iput p3, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$deltaY:I

    iput p4, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$deltaX:I

    iput p5, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$originalPosition:I

    iput p6, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$finalTargetPosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 3

    .prologue
    .line 574
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$observer:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$deltaY:I

    # += operator for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetY:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$012(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)I

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$deltaX:I

    # += operator for: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->mTotalOffsetX:I
    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$112(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;I)I

    .line 577
    iget-object v0, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->this$0:Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;

    iget v1, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$originalPosition:I

    iget v2, p0, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView$6;->val$finalTargetPosition:I

    # invokes: Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->animateReorder(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;->access$1600(Lcom/sec/android/app/shealth/home/widget/dynamicgrid/DynamicGridView;II)V

    .line 578
    const/4 v0, 0x1

    return v0
.end method
