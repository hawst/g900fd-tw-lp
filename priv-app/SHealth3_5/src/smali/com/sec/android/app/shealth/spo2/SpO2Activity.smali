.class public Lcom/sec/android/app/shealth/spo2/SpO2Activity;
.super Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;
.source "SpO2Activity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/logutils/graph/FragmentSwitchable;


# static fields
.field public static final ACTIONBAR_LOG:I = 0x0

.field public static final ACTIONBAR_SHARE_VIA:I = 0x1

.field private static final SHOW_INFORMATION_DIALOG:I = 0x1

.field private static final SHOW_INFORMATION_DIALOG_TIME:J = 0xfaL

.field private static final SHOW_SUMMARY_FRAGMENT:I = 0x2

.field private static final SHOW_SUMMARY_FRAGMENT_TIME:J = 0x96L

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private IMAGE_IDS:[I

.field private actionbarClickListener:Landroid/view/View$OnClickListener;

.field againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field backFinish:Z

.field private callFromHome:Z

.field public currentimageindex:I

.field private infoAnimation:Landroid/view/animation/AnimationSet;

.field public isAutoStartup:Z

.field public isMeasureCompleted:Z

.field mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

.field private mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

.field private final mHandler:Landroid/os/Handler;

.field private mInfoDialog:Landroid/app/Dialog;

.field private mInfoMovie:Landroid/widget/VideoView;

.field private mInfoView:Landroid/view/View;

.field public mIsActivityOnConfigChanged:Z

.field private mShowAgain:Landroid/widget/CheckBox;

.field private mShowAgainCheckLayout:Landroid/widget/LinearLayout;

.field public mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

.field private sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

.field private slidingimage:Landroid/widget/ImageView;

.field private timer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;-><init>()V

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoMovie:Landroid/widget/VideoView;

    .line 79
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->callFromHome:Z

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isAutoStartup:Z

    .line 82
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mIsActivityOnConfigChanged:Z

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 87
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->IMAGE_IDS:[I

    .line 93
    iput v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->slidingimage:Landroid/widget/ImageView;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->timer:Ljava/util/Timer;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 145
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$1;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mHandler:Landroid/os/Handler;

    .line 440
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$11;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    .line 497
    iput-boolean v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->backFinish:Z

    .line 546
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$13;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mCheckboxLayoutClickListener:Landroid/view/View$OnClickListener;

    .line 682
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    .line 786
    new-instance v0, Lcom/sec/android/app/shealth/spo2/SpO2Activity$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$19;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->againOnCheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void

    .line 87
    :array_0
    .array-data 4
        0x7f0201fa
        0x7f0201fb
        0x7f0201fc
        0x7f0201fd
        0x7f020758
        0x7f020759
        0x7f020758
        0x7f020759
    .end array-data
.end method

.method private AnimateandSlideShow()V
    .locals 12

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 685
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    if-ne v5, v11, :cond_0

    .line 686
    iput v10, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    .line 687
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->slidingimage:Landroid/widget/ImageView;

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 688
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->slidingimage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->IMAGE_IDS:[I

    iget v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 689
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->IMAGE_IDS:[I

    array-length v6, v6

    if-ge v5, v6, :cond_1

    .line 690
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    .line 694
    :goto_0
    const/16 v1, 0x1f4

    .line 695
    .local v1, "fadeInDuration":I
    const/16 v4, 0x3e8

    .line 696
    .local v4, "timeBetween":I
    const/16 v3, 0x1f4

    .line 701
    .local v3, "fadeOutDuration":I
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    .line 702
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v9, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 703
    .local v0, "fadeIn":Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v8, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 716
    .local v2, "fadeOut":Landroid/view/animation/Animation;
    :goto_1
    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 717
    int-to-long v5, v1

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 719
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 720
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    const/4 v6, 0x4

    if-le v5, v6, :cond_6

    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    if-gt v5, v11, :cond_6

    .line 721
    int-to-long v5, v1

    invoke-virtual {v2, v5, v6}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 726
    :goto_2
    int-to-long v5, v3

    invoke-virtual {v2, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 728
    new-instance v5, Landroid/view/animation/AnimationSet;

    invoke-direct {v5, v10}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    .line 729
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 730
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 731
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->setRepeatCount(I)V

    .line 732
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->slidingimage:Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 733
    iget-object v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    new-instance v6, Lcom/sec/android/app/shealth/spo2/SpO2Activity$18;

    invoke-direct {v6, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$18;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v5, v6}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 745
    return-void

    .line 692
    .end local v0    # "fadeIn":Landroid/view/animation/Animation;
    .end local v1    # "fadeInDuration":I
    .end local v2    # "fadeOut":Landroid/view/animation/Animation;
    .end local v3    # "fadeOutDuration":I
    .end local v4    # "timeBetween":I
    :cond_1
    iput v10, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    goto :goto_0

    .line 704
    .restart local v1    # "fadeInDuration":I
    .restart local v3    # "fadeOutDuration":I
    .restart local v4    # "timeBetween":I
    :cond_2
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    const/4 v6, 0x6

    if-eq v5, v6, :cond_3

    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    const/4 v6, 0x7

    if-ne v5, v6, :cond_4

    .line 705
    :cond_3
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v8, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 706
    .restart local v0    # "fadeIn":Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v8, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .restart local v2    # "fadeOut":Landroid/view/animation/Animation;
    goto :goto_1

    .line 707
    .end local v0    # "fadeIn":Landroid/view/animation/Animation;
    .end local v2    # "fadeOut":Landroid/view/animation/Animation;
    :cond_4
    iget v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->currentimageindex:I

    if-ne v5, v11, :cond_5

    .line 708
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v8, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 709
    .restart local v0    # "fadeIn":Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .restart local v2    # "fadeOut":Landroid/view/animation/Animation;
    goto :goto_1

    .line 711
    .end local v0    # "fadeIn":Landroid/view/animation/Animation;
    .end local v2    # "fadeOut":Landroid/view/animation/Animation;
    :cond_5
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v9, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 712
    .restart local v0    # "fadeIn":Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .restart local v2    # "fadeOut":Landroid/view/animation/Animation;
    goto :goto_1

    .line 724
    :cond_6
    const/16 v5, 0x5dc

    int-to-long v5, v5

    invoke-virtual {v2, v5, v6}, Landroid/view/animation/Animation;->setStartOffset(J)V

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showInfomationDialog()V

    return-void
.end method

.method static synthetic access$1002(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->slidingimage:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgainCheckLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    .param p1, "x1"    # Landroid/widget/LinearLayout;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgainCheckLayout:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->initInfoSlideShow()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showSummarayFragment()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/view/animation/AnimationSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Landroid/view/animation/AnimationSet;)Landroid/view/animation/AnimationSet;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    .param p1, "x1"    # Landroid/view/animation/AnimationSet;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->AnimateandSlideShow()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showDeleteDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isDrawerMenuShown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mDrawerLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->drawerMenu:Lcom/sec/android/app/shealth/framework/ui/view/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    .param p1, "x1"    # Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->prepareShareView()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgain:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/spo2/SpO2Activity;Landroid/widget/CheckBox;)Landroid/widget/CheckBox;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/shealth/spo2/SpO2Activity;
    .param p1, "x1"    # Landroid/widget/CheckBox;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgain:Landroid/widget/CheckBox;

    return-object p1
.end method

.method private initInfoSlideShow()V
    .locals 0

    .prologue
    .line 652
    invoke-direct {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->AnimateandSlideShow()V

    .line 680
    return-void
.end method

.method private showDeleteDialog()V
    .locals 4

    .prologue
    .line 252
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 253
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 254
    const v1, 0x7f090c11

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setAlertText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 256
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 257
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$5;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnBackPressListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnBackPressListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 268
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$6;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelButtonListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnCancelButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 278
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$7;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 290
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$8;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 297
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 299
    return-void
.end method

.method private showInfomationDialog()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 579
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 580
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 581
    const v1, 0x7f090240

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 582
    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 583
    const v1, 0x7f03021f

    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$14;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 604
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$15;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonClickListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnOKButtonClickListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 614
    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$16;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 635
    if-eqz p0, :cond_1

    .line 637
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v1, :cond_0

    .line 638
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 640
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity$17;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$17;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    const-wide/16 v3, 0xfa

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 648
    :cond_1
    return-void
.end method

.method private showRecommendedDialog()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 392
    const/4 v1, 0x0

    .line 393
    .local v1, "title":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    invoke-direct {v0, p0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 394
    .local v0, "builder":Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;
    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setCancelable(Z)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 395
    const v2, 0x7f090047

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOkButtonText(I)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090fad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 398
    const v2, 0x7f030226

    new-instance v3, Lcom/sec/android/app/shealth/spo2/SpO2Activity$9;

    invoke-direct {v3, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$9;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setContent(ILcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 407
    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 408
    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2Activity$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$10;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;

    .line 415
    if-eqz p0, :cond_1

    .line 416
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-nez v2, :cond_0

    .line 417
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$Builder;->build()Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 419
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSpo2RecommendedDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string/jumbo v4, "recommendedDialog"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 422
    :cond_1
    return-void
.end method

.method private showSummarayFragment()V
    .locals 3

    .prologue
    .line 778
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 779
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 780
    const-string/jumbo v1, "warning_checked_spo2"

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 781
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 784
    :cond_0
    return-void
.end method

.method private startHeartUserMenual()V
    .locals 3

    .prologue
    .line 425
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/settings/ManualActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_manual_url"

    const-string v2, "http://www.samsung.com/m-manual/common/S_HEALTH_K_HEARTRA"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->startActivity(Landroid/content/Intent;)V

    .line 426
    return-void
.end method

.method private updateDialogContent()V
    .locals 3

    .prologue
    .line 837
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 838
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->getView()Landroid/view/View;

    move-result-object v0

    .line 839
    .local v0, "content":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 840
    const v1, 0x7f0800b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090047

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 841
    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090240

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 843
    const v1, 0x7f08099a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090f96

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 844
    const v1, 0x7f08099b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090c15

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 845
    const v1, 0x7f08099c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090c16

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 846
    const v1, 0x7f080554

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090c2d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 847
    const v1, 0x7f08099d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090c26

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 848
    const v1, 0x7f080565

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090c08

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 849
    const v1, 0x7f0804ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    const v2, 0x7f09078c

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setText(I)V

    .line 850
    const v1, 0x7f080999

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090fa2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 853
    .end local v0    # "content":Landroid/view/View;
    :cond_0
    return-void
.end method


# virtual methods
.method public callShowInformationDialog()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 139
    return-void
.end method

.method protected customizeActionBar()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 431
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->customizeActionBar()V

    .line 432
    const/4 v0, 0x0

    .line 433
    .local v0, "emptyActionItemTextId":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    const v2, 0x7f090f95

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 434
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    new-array v2, v8, [Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;

    const v5, 0x7f0207c0

    const v6, 0x7f09005a

    iget-object v7, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;-><init>(IIILandroid/view/View$OnClickListener;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->addActionButton([Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButton;)V

    .line 437
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setMenuDividervisibility(Z)V

    .line 438
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 822
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 824
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 825
    .local v0, "currentView":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f080304

    if-ne v1, v2, :cond_0

    .line 827
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->getActionBarLayout()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08030e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    check-cast v1, Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 828
    const/4 v1, 0x1

    .line 833
    .end local v0    # "currentView":Landroid/view/View;
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method getActionBarHelperPackageAccess()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;
    .locals 1

    .prologue
    .line 806
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    return-object v0
.end method

.method getActionBarListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->actionbarClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getCallFromHome()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->callFromHome:Z

    return v0
.end method

.method public getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    return-object v0
.end method

.method protected getHelpItem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 815
    const-string v0, "com.sec.shealth.help.action.SPO2"

    return-object v0
.end method

.method public getShowDeleteDialog()Z
    .locals 2

    .prologue
    .line 533
    iget-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v1, :cond_0

    .line 534
    const/4 v0, 0x1

    .line 539
    .local v0, "isShow":Z
    :goto_0
    return v0

    .line 536
    .end local v0    # "isShow":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "isShow":Z
    goto :goto_0
.end method

.method public isDrawerVisible()Z
    .locals 1

    .prologue
    .line 774
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isDrawerMenuShown()Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 303
    packed-switch p1, :pswitch_data_0

    .line 313
    :goto_0
    return-void

    .line 305
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isAutoStartup:Z

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 309
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->finish()V

    goto :goto_0

    .line 303
    :pswitch_data_0
    .packed-switch 0x45a
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 502
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->callFromHome:Z

    if-eqz v0, :cond_0

    .line 503
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->finish()V

    .line 525
    :goto_0
    return-void

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_0

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 508
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->backFinish:Z

    if-eqz v0, :cond_2

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndUpdateUI(IF)V

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->finish()V

    .line 515
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$12;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 512
    :cond_2
    const v0, 0x7f090f97

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 513
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->backFinish:Z

    goto :goto_1

    .line 524
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 231
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mIsActivityOnConfigChanged:Z

    .line 233
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->clearAndRefreshDrawerMenu()V

    .line 234
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$4;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    const v1, 0x7f090240

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(I)V

    .line 248
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 249
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 100
    iput-boolean v5, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isAutoStartup:Z

    .line 101
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 103
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 104
    .local v1, "model":Ljava/lang/String;
    const-string v2, "SM-G850F"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-G850S"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SM-G850A"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->IMAGE_IDS:[I

    const v3, 0x7f0201f6

    aput v3, v2, v5

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->IMAGE_IDS:[I

    const v3, 0x7f0201f7

    aput v3, v2, v6

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->IMAGE_IDS:[I

    const/4 v3, 0x2

    const v4, 0x7f0201f8

    aput v4, v2, v3

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->IMAGE_IDS:[I

    const/4 v3, 0x3

    const v4, 0x7f0201f9

    aput v4, v2, v3

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "show_graph_fragment"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 118
    .local v0, "isShowGraphFrag":Z
    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .line 119
    new-instance v2, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-direct {v2}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .line 121
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "warning_checked_spo2"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    if-nez v0, :cond_2

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->callShowInformationDialog()V

    .line 129
    :goto_0
    if-eqz v0, :cond_3

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    .line 131
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->callFromHome:Z

    .line 135
    :goto_1
    return-void

    .line 127
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isAutoStartup:Z

    goto :goto_0

    .line 133
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;)V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100024

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isDrawerMenuShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    const/4 v0, 0x0

    .line 323
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->cancel()V

    .line 219
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->infoAnimation:Landroid/view/animation/AnimationSet;

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    if-eqz v0, :cond_2

    .line 222
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    if-eqz v0, :cond_3

    .line 224
    iput-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .line 226
    :cond_3
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onDestroy()V

    .line 227
    return-void
.end method

.method protected onLogSelected()V
    .locals 2

    .prologue
    .line 467
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/shealth/spo2/log/SpO2LogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->startActivity(Landroid/content/Intent;)V

    .line 468
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v5, 0x0

    .line 361
    iget-boolean v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->isMeasureCompleted:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    if-eqz v2, :cond_0

    .line 362
    iget-object v2, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    const/4 v3, 0x0

    invoke-virtual {v2, v5, v3}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->stopSensorAndUpdateUI(IF)V

    .line 364
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 388
    :goto_0
    return v5

    .line 366
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->prepareShareView()V

    .line 367
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.spo2"

    const-string v4, "SP07"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 371
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/shealth/spo2/common/Spo2InformationActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    .local v0, "inf_intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 376
    .end local v0    # "inf_intent":Landroid/content/Intent;
    :sswitch_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 377
    .local v1, "intentSetting":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    const-string v2, "android.shealth.action.LAUNCH_SETTINGS"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    invoke-virtual {p0, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 382
    .end local v1    # "intentSetting":Landroid/content/Intent;
    :sswitch_3
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.sec.android.app.shealth.spo2"

    const-string v4, "SP06"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/utils/logging/LogUtils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 364
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f080c8a -> :sswitch_3
        0x7f080c90 -> :sswitch_0
        0x7f080ca4 -> :sswitch_2
        0x7f080cad -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoMovie:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoMovie:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoMovie:Landroid/widget/VideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoMovie:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoMovie:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoMovie:Landroid/widget/VideoView;

    .line 207
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPause()V

    .line 208
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 329
    if-eqz p1, :cond_0

    .line 330
    :try_start_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f100024

    invoke-virtual {v4, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->getGraphFragment()Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    .line 333
    .local v1, "graphFragment":Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 334
    const v2, 0x7f080ca4

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 335
    const v2, 0x7f080cad

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 336
    const v2, 0x7f080c90

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 337
    const v2, 0x7f080c8a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 338
    const v2, 0x7f080c8a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 354
    invoke-super {p0, p1}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 356
    :cond_1
    return v3

    .line 341
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
    :cond_2
    const v4, 0x7f080ca4

    :try_start_1
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 342
    const v4, 0x7f080cad

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 343
    const v4, 0x7f080c90

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->getInstance()Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/shealth/spo2/common/SpO2DeviceConnector;->isStarted()Z

    move-result v5

    if-ne v5, v3, :cond_3

    :goto_1
    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 344
    const v2, 0x7f080c8a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 345
    const v2, 0x7f080c8a

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 349
    .end local v1    # "graphFragment":Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "graphFragment":Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;
    :cond_3
    move v2, v3

    .line 343
    goto :goto_1
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 161
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BaseActivity;->onResume()V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mInfoMovie:Landroid/widget/VideoView;

    if-nez v0, :cond_0

    .line 166
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mIsActivityOnConfigChanged:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 170
    iput-object v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->sHealthAlertDialogbuild:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$2;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 179
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mIsActivityOnConfigChanged:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    if-eqz v0, :cond_3

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    if-eqz v0, :cond_2

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;->setMeasuringEndUI()V

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->dismiss()V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mDeleteDialog:Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;

    new-instance v1, Lcom/sec/android/app/shealth/spo2/SpO2Activity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/shealth/spo2/SpO2Activity$3;-><init>(Lcom/sec/android/app/shealth/spo2/SpO2Activity;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog;->setOnDismissListener(Lcom/sec/android/app/shealth/common/commonui/dialog/SHealthAlertDialog$OnDialogDismissListener;)V

    .line 192
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mIsActivityOnConfigChanged:Z

    .line 193
    return-void
.end method

.method public switchFragmentToGraph()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 493
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->callFromHome:Z

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mGraphFragment:Lcom/sec/android/app/shealth/spo2/SpO2GraphFragmentSIC;

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 495
    return-void
.end method

.method public switchFragmentToSummary()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->mSummarayFragment:Lcom/sec/android/app/shealth/spo2/SpO2SummaryFragmentNew;

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/app/shealth/spo2/SpO2Activity;->showFragment(Lcom/sec/android/app/shealth/framework/ui/base/BaseFragment;ZZ)V

    .line 489
    return-void
.end method
