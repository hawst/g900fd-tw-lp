.class public Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;
.super Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IContentInitializatorGetter;
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonControllerProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;
    }
.end annotation


# static fields
.field private static HOME_KEY:Ljava/lang/String;

.field private static RECENT_KEY:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;


# instance fields
.field public SETTINGS_ConnectivityType_TAG:Ljava/lang/String;

.field public SETTINGS_DataType_TAG:Ljava/lang/String;

.field public SETTINGS_DeviceID_TAG:Ljava/lang/String;

.field public SETTINGS_DeviceName_TAG:Ljava/lang/String;

.field public SETTINGS_DeviceType_TAG:Ljava/lang/String;

.field public SETTINGS_RENAME_TAG:Ljava/lang/String;

.field private accessary_name:Ljava/lang/String;

.field private connectivity_type:I

.field private data_type:I

.field private device_type:I

.field private mActivity:Landroid/app/Activity;

.field private mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;

.field private mDeviceID:Ljava/lang/String;

.field private mImm:Landroid/view/inputmethod/InputMethodManager;

.field private mInput:Landroid/widget/EditText;

.field private mNameView:Landroid/widget/TextView;

.field private mRename:Ljava/lang/String;

.field mRenameInputController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

.field private mRenameLayout:Landroid/widget/LinearLayout;

.field private mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

.field mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

.field private mUnpairLayout:Landroid/widget/LinearLayout;

.field private settingsSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;


# direct methods
.method static constructor <clinit>()V
    .locals 32

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, -0x5a

    aput v11, v1, v10

    const/16 v10, -0x4e

    aput v10, v1, v9

    const/16 v9, -0x25ae

    aput v9, v1, v8

    const/16 v8, -0x45

    aput v8, v1, v7

    const/16 v7, 0x4210

    aput v7, v1, v6

    const/16 v6, -0x14d4

    aput v6, v1, v5

    const/16 v5, -0x72

    aput v5, v1, v4

    const/16 v4, 0x6359

    aput v4, v1, v3

    const/16 v3, -0x75fa

    aput v3, v1, v2

    const/4 v2, -0x8

    aput v2, v1, v0

    const/16 v0, 0xa

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x2b

    aput v12, v0, v11

    const/16 v11, -0x3e

    aput v11, v0, v10

    const/16 v10, -0x25de

    aput v10, v0, v9

    const/16 v9, -0x26

    aput v9, v0, v8

    const/16 v8, 0x4264

    aput v8, v0, v7

    const/16 v7, -0x14be

    aput v7, v0, v6

    const/16 v6, -0x15

    aput v6, v0, v5

    const/16 v5, 0x633a

    aput v5, v0, v4

    const/16 v4, -0x759d

    aput v4, v0, v3

    const/16 v3, -0x76

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-lt v2, v3, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->RECENT_KEY:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/16 v8, 0xc52

    aput v8, v1, v7

    const/16 v7, -0x1697

    aput v7, v1, v6

    const/16 v6, -0x7e

    aput v6, v1, v5

    const/16 v5, 0x211f

    aput v5, v1, v4

    const/16 v4, -0x2cb4

    aput v4, v1, v3

    const/16 v3, -0x44

    aput v3, v1, v2

    const/16 v2, -0x23f1

    aput v2, v1, v0

    const/4 v0, 0x7

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/16 v9, 0xc2b

    aput v9, v0, v8

    const/16 v8, -0x16f4

    aput v8, v0, v7

    const/16 v7, -0x17

    aput v7, v0, v6

    const/16 v6, 0x217a

    aput v6, v0, v5

    const/16 v5, -0x2cdf

    aput v5, v0, v4

    const/16 v4, -0x2d

    aput v4, v0, v3

    const/16 v3, -0x2399

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_2
    array-length v3, v0

    if-lt v2, v3, :cond_2

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_3
    array-length v3, v0

    if-lt v2, v3, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->HOME_KEY:Ljava/lang/String;

    const/16 v0, 0x1d

    new-array v1, v0, [I

    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    const/4 v6, 0x5

    const/4 v7, 0x6

    const/4 v8, 0x7

    const/16 v9, 0x8

    const/16 v10, 0x9

    const/16 v11, 0xa

    const/16 v12, 0xb

    const/16 v13, 0xc

    const/16 v14, 0xd

    const/16 v15, 0xe

    const/16 v16, 0xf

    const/16 v17, 0x10

    const/16 v18, 0x11

    const/16 v19, 0x12

    const/16 v20, 0x13

    const/16 v21, 0x14

    const/16 v22, 0x15

    const/16 v23, 0x16

    const/16 v24, 0x17

    const/16 v25, 0x18

    const/16 v26, 0x19

    const/16 v27, 0x1a

    const/16 v28, 0x1b

    const/16 v29, 0x1c

    const/16 v30, -0x33

    aput v30, v1, v29

    const/16 v29, 0x6a40

    aput v29, v1, v28

    const/16 v28, 0x6003

    aput v28, v1, v27

    const/16 v27, -0x62ea

    aput v27, v1, v26

    const/16 v26, -0xc

    aput v26, v1, v25

    const/16 v25, -0x36

    aput v25, v1, v24

    const/16 v24, -0x57ca

    aput v24, v1, v23

    const/16 v23, -0x17

    aput v23, v1, v22

    const/16 v22, -0x1a

    aput v22, v1, v21

    const/16 v21, 0x1367

    aput v21, v1, v20

    const/16 v20, -0x6683

    aput v20, v1, v19

    const/16 v19, -0x10

    aput v19, v1, v18

    const/16 v18, 0x566f

    aput v18, v1, v17

    const/16 v17, -0x32de

    aput v17, v1, v16

    const/16 v16, -0x58

    aput v16, v1, v15

    const/16 v15, -0x12

    aput v15, v1, v14

    const/16 v14, -0x1b

    aput v14, v1, v13

    const/16 v13, 0x6659

    aput v13, v1, v12

    const/16 v12, 0x7005

    aput v12, v1, v11

    const/16 v11, 0x6719

    aput v11, v1, v10

    const/16 v10, -0x12ef

    aput v10, v1, v9

    const/16 v9, -0x78

    aput v9, v1, v8

    const/16 v8, -0x51f6

    aput v8, v1, v7

    const/16 v7, -0x36

    aput v7, v1, v6

    const/16 v6, -0x50c2

    aput v6, v1, v5

    const/16 v5, -0x23

    aput v5, v1, v4

    const/16 v4, 0x3705

    aput v4, v1, v3

    const/16 v3, -0x6faa

    aput v3, v1, v2

    const/16 v2, -0x40

    aput v2, v1, v0

    const/16 v0, 0x1d

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, -0x4c

    aput v31, v0, v30

    const/16 v30, 0x6a34

    aput v30, v0, v29

    const/16 v29, 0x606a

    aput v29, v0, v28

    const/16 v28, -0x62a0

    aput v28, v0, v27

    const/16 v27, -0x63

    aput v27, v0, v26

    const/16 v26, -0x42

    aput v26, v0, v25

    const/16 v25, -0x57ab

    aput v25, v0, v24

    const/16 v24, -0x58

    aput v24, v0, v23

    const/16 v23, -0x6b

    aput v23, v0, v22

    const/16 v22, 0x1300

    aput v22, v0, v21

    const/16 v21, -0x66ed

    aput v21, v0, v20

    const/16 v20, -0x67

    aput v20, v0, v19

    const/16 v19, 0x561b

    aput v19, v0, v18

    const/16 v18, -0x32aa

    aput v18, v0, v17

    const/16 v17, -0x33

    aput v17, v0, v16

    const/16 v16, -0x43

    aput v16, v0, v15

    const/16 v15, -0x6a

    aput v15, v0, v14

    const/16 v14, 0x663c

    aput v14, v0, v13

    const/16 v13, 0x7066

    aput v13, v0, v12

    const/16 v12, 0x6770

    aput v12, v0, v11

    const/16 v11, -0x1299

    aput v11, v0, v10

    const/16 v10, -0x13

    aput v10, v0, v9

    const/16 v9, -0x51b2

    aput v9, v0, v8

    const/16 v8, -0x52

    aput v8, v0, v7

    const/16 v7, -0x50a5

    aput v7, v0, v6

    const/16 v6, -0x51

    aput v6, v0, v5

    const/16 v5, 0x376c

    aput v5, v0, v4

    const/16 v4, -0x6fc9

    aput v4, v0, v3

    const/16 v3, -0x70

    aput v3, v0, v2

    const/4 v2, 0x0

    :goto_4
    array-length v3, v0

    if-lt v2, v3, :cond_4

    array-length v0, v1

    new-array v0, v0, [C

    const/4 v2, 0x0

    :goto_5
    array-length v3, v0

    if-lt v2, v3, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->TAG:Ljava/lang/String;

    return-void

    :cond_0
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_1
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_3
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_4
    aget v3, v0, v2

    aget v4, v1, v2

    xor-int/2addr v3, v4

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    aget v3, v1, v2

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5
.end method

.method public constructor <init>()V
    .locals 20

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;-><init>()V

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x42

    aput v10, v2, v9

    const/16 v9, 0x1f36

    aput v9, v2, v8

    const/16 v8, 0x3f7a

    aput v8, v2, v7

    const/16 v7, 0x5b5c

    aput v7, v2, v6

    const/16 v6, 0x5932

    aput v6, v2, v5

    const/16 v5, -0x66d1

    aput v5, v2, v4

    const/4 v4, -0x4

    aput v4, v2, v3

    const/4 v3, -0x6

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/4 v11, -0x6

    aput v11, v1, v10

    const/16 v10, 0x1f7f

    aput v10, v1, v9

    const/16 v9, 0x3f1f

    aput v9, v1, v8

    const/16 v8, 0x5b3f

    aput v8, v1, v7

    const/16 v7, 0x595b

    aput v7, v1, v6

    const/16 v6, -0x66a7

    aput v6, v1, v5

    const/16 v5, -0x67

    aput v5, v1, v4

    const/16 v4, -0x42

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->SETTINGS_DeviceID_TAG:Ljava/lang/String;

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0xf

    aput v12, v2, v11

    const/16 v11, -0x64

    aput v11, v2, v10

    const/16 v10, -0x22ee

    aput v10, v2, v9

    const/16 v9, -0x6d

    aput v9, v2, v8

    const/16 v8, 0x7d12

    aput v8, v2, v7

    const/16 v7, -0x1ae2

    aput v7, v2, v6

    const/16 v6, -0x74

    aput v6, v2, v5

    const/16 v5, -0x6f

    aput v5, v2, v4

    const/16 v4, -0x73

    aput v4, v2, v3

    const/16 v3, -0x1d

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6c

    aput v13, v1, v12

    const/16 v12, -0xf

    aput v12, v1, v11

    const/16 v11, -0x228d

    aput v11, v1, v10

    const/16 v10, -0x23

    aput v10, v1, v9

    const/16 v9, 0x7d77

    aput v9, v1, v8

    const/16 v8, -0x1a83

    aput v8, v1, v7

    const/16 v7, -0x1b

    aput v7, v1, v6

    const/16 v6, -0x19

    aput v6, v1, v5

    const/16 v5, -0x18

    aput v5, v1, v4

    const/16 v4, -0x59

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->SETTINGS_DeviceName_TAG:Ljava/lang/String;

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x2ecc

    aput v10, v2, v9

    const/16 v9, -0x5f

    aput v9, v2, v8

    const/16 v8, 0x3a5d

    aput v8, v2, v7

    const/16 v7, -0x6392

    aput v7, v2, v6

    const/4 v6, -0x3

    aput v6, v2, v5

    const/16 v5, -0x2d

    aput v5, v2, v4

    const/16 v4, -0x25e9

    aput v4, v2, v3

    const/16 v3, -0x62

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x2eaf

    aput v11, v1, v10

    const/16 v10, -0x2f

    aput v10, v1, v9

    const/16 v9, 0x3a24

    aput v9, v1, v8

    const/16 v8, -0x63c6

    aput v8, v1, v7

    const/16 v7, -0x64

    aput v7, v1, v6

    const/16 v6, -0x59

    aput v6, v1, v5

    const/16 v5, -0x258a

    aput v5, v1, v4

    const/16 v4, -0x26

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->SETTINGS_DataType_TAG:Ljava/lang/String;

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x63

    aput v12, v2, v11

    const/16 v11, -0x41

    aput v11, v2, v10

    const/16 v10, 0x756a

    aput v10, v2, v9

    const/16 v9, -0x1cdf

    aput v9, v2, v8

    const/16 v8, -0x7a

    aput v8, v2, v7

    const/16 v7, -0x56

    aput v7, v2, v6

    const/16 v6, -0x2f

    aput v6, v2, v5

    const/16 v5, -0x4a

    aput v5, v2, v4

    const/16 v4, -0x2296

    aput v4, v2, v3

    const/16 v3, -0x67

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/4 v13, -0x8

    aput v13, v1, v12

    const/16 v12, -0x31

    aput v12, v1, v11

    const/16 v11, 0x7513

    aput v11, v1, v10

    const/16 v10, -0x1c8b

    aput v10, v1, v9

    const/16 v9, -0x1d

    aput v9, v1, v8

    const/16 v8, -0x37

    aput v8, v1, v7

    const/16 v7, -0x48

    aput v7, v1, v6

    const/16 v6, -0x40

    aput v6, v1, v5

    const/16 v5, -0x22f1

    aput v5, v1, v4

    const/16 v4, -0x23

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->SETTINGS_DeviceType_TAG:Ljava/lang/String;

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x5c21

    aput v18, v2, v17

    const/16 v17, -0x42d4

    aput v17, v2, v16

    const/16 v16, -0x3c

    aput v16, v2, v15

    const/16 v15, -0x4f

    aput v15, v2, v14

    const/16 v14, 0x561a

    aput v14, v2, v13

    const/16 v13, -0x4bde

    aput v13, v2, v12

    const/16 v12, -0x23

    aput v12, v2, v11

    const/16 v11, -0x54

    aput v11, v2, v10

    const/16 v10, 0x335b

    aput v10, v2, v9

    const/16 v9, 0x4847

    aput v9, v2, v8

    const/16 v8, -0x2d5

    aput v8, v2, v7

    const/16 v7, -0x68

    aput v7, v2, v6

    const/16 v6, -0x12c4

    aput v6, v2, v5

    const/16 v5, -0x7d

    aput v5, v2, v4

    const/16 v4, 0x5605

    aput v4, v2, v3

    const/16 v3, -0x3aeb

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x5c44

    aput v19, v1, v18

    const/16 v18, -0x42a4

    aput v18, v1, v17

    const/16 v17, -0x43

    aput v17, v1, v16

    const/16 v16, -0x1b

    aput v16, v1, v15

    const/16 v15, 0x5663

    aput v15, v1, v14

    const/16 v14, -0x4baa

    aput v14, v1, v13

    const/16 v13, -0x4c

    aput v13, v1, v12

    const/16 v12, -0x26

    aput v12, v1, v11

    const/16 v11, 0x3332

    aput v11, v1, v10

    const/16 v10, 0x4833

    aput v10, v1, v9

    const/16 v9, -0x2b8

    aput v9, v1, v8

    const/4 v8, -0x3

    aput v8, v1, v7

    const/16 v7, -0x12ae

    aput v7, v1, v6

    const/16 v6, -0x13

    aput v6, v1, v5

    const/16 v5, 0x566a    # 3.1E-41f

    aput v5, v1, v4

    const/16 v4, -0x3aaa

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v4, v1

    if-lt v3, v4, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v4, v1

    if-lt v3, v4, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->SETTINGS_ConnectivityType_TAG:Ljava/lang/String;

    const/4 v1, 0x6

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/16 v8, -0x66

    aput v8, v2, v7

    const/16 v7, -0x20b2

    aput v7, v2, v6

    const/16 v6, -0x42

    aput v6, v2, v5

    const/16 v5, -0x2a

    aput v5, v2, v4

    const/16 v4, -0x2a

    aput v4, v2, v3

    const/16 v3, 0x4858

    aput v3, v2, v1

    const/4 v1, 0x6

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, -0x1

    aput v9, v1, v8

    const/16 v8, -0x20dd

    aput v8, v1, v7

    const/16 v7, -0x21

    aput v7, v1, v6

    const/16 v6, -0x48

    aput v6, v1, v5

    const/16 v5, -0x4d

    aput v5, v1, v4

    const/16 v4, 0x480a

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_a
    array-length v4, v1

    if-lt v3, v4, :cond_a

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_b
    array-length v4, v1

    if-lt v3, v4, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->SETTINGS_RENAME_TAG:Ljava/lang/String;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$3;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$4;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRenameInputController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_8
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_9
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_a
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :cond_b
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_b
.end method

.method static synthetic access$000(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->hideSoftKey()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;)Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->settingsSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mNameView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->hideSIP()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mInput:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mImm:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Landroid/view/inputmethod/InputMethodManager;)Landroid/view/inputmethod/InputMethodManager;
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mImm:Landroid/view/inputmethod/InputMethodManager;

    return-object p1
.end method

.method private getFragmentController()V
    .locals 44

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, -0x3b

    aput v31, v2, v30

    const/16 v30, 0x24c

    aput v30, v2, v29

    const/16 v29, -0x2e95

    aput v29, v2, v28

    const/16 v28, -0x59

    aput v28, v2, v27

    const/16 v27, 0x3a5b

    aput v27, v2, v26

    const/16 v26, 0x344e

    aput v26, v2, v25

    const/16 v25, -0x59a9

    aput v25, v2, v24

    const/16 v24, -0x19

    aput v24, v2, v23

    const/16 v23, -0x26

    aput v23, v2, v22

    const/16 v22, -0x55

    aput v22, v2, v21

    const/16 v21, -0x6c

    aput v21, v2, v20

    const/16 v20, 0xf23

    aput v20, v2, v19

    const/16 v19, 0x6d7b

    aput v19, v2, v18

    const/16 v18, 0x1419

    aput v18, v2, v17

    const/16 v17, -0x478f

    aput v17, v2, v16

    const/16 v16, -0x15

    aput v16, v2, v15

    const/16 v15, -0x17

    aput v15, v2, v14

    const/16 v14, -0x7f9

    aput v14, v2, v13

    const/16 v13, -0x65

    aput v13, v2, v12

    const/16 v12, -0x65

    aput v12, v2, v11

    const/16 v11, -0x2fc6

    aput v11, v2, v10

    const/16 v10, -0x4b

    aput v10, v2, v9

    const/16 v9, 0x6b6e

    aput v9, v2, v8

    const/16 v8, -0x43f1

    aput v8, v2, v7

    const/16 v7, -0x27

    aput v7, v2, v6

    const/16 v6, 0x4f1a

    aput v6, v2, v5

    const/16 v5, 0x7826

    aput v5, v2, v4

    const/16 v4, -0x37e7

    aput v4, v2, v3

    const/16 v3, -0x68

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, -0x44

    aput v32, v1, v31

    const/16 v31, 0x238

    aput v31, v1, v30

    const/16 v30, -0x2efe

    aput v30, v1, v29

    const/16 v29, -0x2f

    aput v29, v1, v28

    const/16 v28, 0x3a32

    aput v28, v1, v27

    const/16 v27, 0x343a

    aput v27, v1, v26

    const/16 v26, -0x59cc

    aput v26, v1, v25

    const/16 v25, -0x5a

    aput v25, v1, v24

    const/16 v24, -0x57

    aput v24, v1, v23

    const/16 v23, -0x34

    aput v23, v1, v22

    const/16 v22, -0x6

    aput v22, v1, v21

    const/16 v21, 0xf4a

    aput v21, v1, v20

    const/16 v20, 0x6d0f

    aput v20, v1, v19

    const/16 v19, 0x146d

    aput v19, v1, v18

    const/16 v18, -0x47ec

    aput v18, v1, v17

    const/16 v17, -0x48

    aput v17, v1, v16

    const/16 v16, -0x66

    aput v16, v1, v15

    const/16 v15, -0x79e

    aput v15, v1, v14

    const/4 v14, -0x8

    aput v14, v1, v13

    const/16 v13, -0xe

    aput v13, v1, v12

    const/16 v12, -0x2fb4

    aput v12, v1, v11

    const/16 v11, -0x30

    aput v11, v1, v10

    const/16 v10, 0x6b2a

    aput v10, v1, v9

    const/16 v9, -0x4395

    aput v9, v1, v8

    const/16 v8, -0x44

    aput v8, v1, v7

    const/16 v7, 0x4f68

    aput v7, v1, v6

    const/16 v6, 0x784f

    aput v6, v1, v5

    const/16 v5, -0x3788

    aput v5, v1, v4

    const/16 v4, -0x38

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x27

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x533

    aput v42, v2, v41

    const/16 v41, 0x5e69

    aput v41, v2, v40

    const/16 v40, -0x21d5

    aput v40, v2, v39

    const/16 v39, -0x50

    aput v39, v2, v38

    const/16 v38, 0x304f

    aput v38, v2, v37

    const/16 v37, -0x59bc

    aput v37, v2, v36

    const/16 v36, -0x37

    aput v36, v2, v35

    const/16 v35, -0x379d

    aput v35, v2, v34

    const/16 v34, -0x18

    aput v34, v2, v33

    const/16 v33, 0x546

    aput v33, v2, v32

    const/16 v32, 0x2b6c

    aput v32, v2, v31

    const/16 v31, 0x310b

    aput v31, v2, v30

    const/16 v30, 0x7943

    aput v30, v2, v29

    const/16 v29, 0xe1c

    aput v29, v2, v28

    const/16 v28, -0x4a9e

    aput v28, v2, v27

    const/16 v27, -0x27

    aput v27, v2, v26

    const/16 v26, -0x6e

    aput v26, v2, v25

    const/16 v25, -0x38

    aput v25, v2, v24

    const/16 v24, -0x3f

    aput v24, v2, v23

    const/16 v23, -0x21c2

    aput v23, v2, v22

    const/16 v22, -0x4f

    aput v22, v2, v21

    const/16 v21, 0x7367

    aput v21, v2, v20

    const/16 v20, 0xb07

    aput v20, v2, v19

    const/16 v19, 0x3e65

    aput v19, v2, v18

    const/16 v18, -0x19a5

    aput v18, v2, v17

    const/16 v17, -0x75

    aput v17, v2, v16

    const/16 v16, -0x1d

    aput v16, v2, v15

    const/16 v15, -0x59

    aput v15, v2, v14

    const/16 v14, -0x45d2

    aput v14, v2, v13

    const/4 v13, -0x4

    aput v13, v2, v12

    const/16 v12, -0x7797

    aput v12, v2, v11

    const/16 v11, -0x1a

    aput v11, v2, v10

    const/16 v10, -0x15

    aput v10, v2, v9

    const/16 v9, 0x4e6d

    aput v9, v2, v8

    const/16 v8, -0x13e0

    aput v8, v2, v7

    const/16 v7, -0x73

    aput v7, v2, v6

    const/16 v6, -0x45

    aput v6, v2, v5

    const/16 v5, -0x17f8

    aput v5, v2, v3

    const/16 v3, -0x7b

    aput v3, v2, v1

    const/16 v1, 0x27

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x55f

    aput v43, v1, v42

    const/16 v42, 0x5e05

    aput v42, v1, v41

    const/16 v41, -0x21a2

    aput v41, v1, v40

    const/16 v40, -0x22

    aput v40, v1, v39

    const/16 v39, 0x306f

    aput v39, v1, v38

    const/16 v38, -0x59d0

    aput v38, v1, v37

    const/16 v37, -0x5a

    aput v37, v1, v36

    const/16 v36, -0x37f3

    aput v36, v1, v35

    const/16 v35, -0x38

    aput v35, v1, v34

    const/16 v34, 0x535

    aput v34, v1, v33

    const/16 v33, 0x2b05

    aput v33, v1, v32

    const/16 v32, 0x312b

    aput v32, v1, v31

    const/16 v31, 0x7931

    aput v31, v1, v30

    const/16 v30, 0xe79

    aput v30, v1, v29

    const/16 v29, -0x4af2

    aput v29, v1, v28

    const/16 v28, -0x4b

    aput v28, v1, v27

    const/16 v27, -0x3

    aput v27, v1, v26

    const/16 v26, -0x46

    aput v26, v1, v25

    const/16 v25, -0x4b

    aput v25, v1, v24

    const/16 v24, -0x21b0

    aput v24, v1, v23

    const/16 v23, -0x22

    aput v23, v1, v22

    const/16 v22, 0x7324

    aput v22, v1, v21

    const/16 v21, 0xb73

    aput v21, v1, v20

    const/16 v20, 0x3e0b

    aput v20, v1, v19

    const/16 v19, -0x19c2

    aput v19, v1, v18

    const/16 v18, -0x1a

    aput v18, v1, v17

    const/16 v17, -0x7c

    aput v17, v1, v16

    const/16 v16, -0x3a

    aput v16, v1, v15

    const/16 v15, -0x45a4

    aput v15, v1, v14

    const/16 v14, -0x46

    aput v14, v1, v13

    const/16 v13, -0x77f2

    aput v13, v1, v12

    const/16 v12, -0x78

    aput v12, v1, v11

    const/16 v11, -0x7e

    aput v11, v1, v10

    const/16 v10, 0x4e03

    aput v10, v1, v9

    const/16 v9, -0x13b2

    aput v9, v1, v8

    const/16 v8, -0x14

    aput v8, v1, v7

    const/16 v7, -0x28

    aput v7, v1, v6

    const/16 v6, -0x17a5

    aput v6, v1, v5

    const/16 v5, -0x18

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    return-void

    :catch_0
    move-exception v1

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    goto :goto_4

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method private hideSIP()V
    .locals 15

    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xc

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/4 v2, 0x6

    const/4 v4, 0x7

    const/16 v5, 0x8

    const/16 v6, 0x9

    const/16 v7, 0xa

    const/16 v8, 0xb

    const/16 v9, 0x445c

    aput v9, v1, v8

    const/16 v8, 0x442b

    aput v8, v1, v7

    const/16 v7, -0x4dd4

    aput v7, v1, v6

    const/16 v6, -0x3a

    aput v6, v1, v5

    const/16 v5, 0x2275

    aput v5, v1, v4

    const/16 v4, -0x20b1

    aput v4, v1, v2

    const/16 v2, -0x80

    aput v2, v1, v0

    const/16 v0, -0x32b4

    aput v0, v1, v14

    const/16 v0, -0x48

    aput v0, v1, v13

    const/16 v0, 0x7f18

    aput v0, v1, v12

    const/16 v0, 0x1511

    aput v0, v1, v11

    const/16 v0, -0x6d84

    aput v0, v1, v3

    const/16 v0, 0xc

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/4 v4, 0x6

    const/4 v5, 0x7

    const/16 v6, 0x8

    const/16 v7, 0x9

    const/16 v8, 0xa

    const/16 v9, 0xb

    const/16 v10, 0x4438

    aput v10, v0, v9

    const/16 v9, 0x4444

    aput v9, v0, v8

    const/16 v8, -0x4dbc

    aput v8, v0, v7

    const/16 v7, -0x4e

    aput v7, v0, v6

    const/16 v6, 0x2210

    aput v6, v0, v5

    const/16 v5, -0x20de

    aput v5, v0, v4

    const/16 v4, -0x21

    aput v4, v0, v2

    const/16 v2, -0x32c8

    aput v2, v0, v14

    const/16 v2, -0x33

    aput v2, v0, v13

    const/16 v2, 0x7f68

    aput v2, v0, v12

    const/16 v2, 0x157f

    aput v2, v0, v11

    const/16 v2, -0x6deb

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    return-void

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private hideSoftKey()V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->hideSIP()V

    return-void
.end method


# virtual methods
.method protected customizeActionBar()V
    .locals 3

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->customizeActionBar()V

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getActionBarHelper()Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->paired_accessories:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/shealth/framework/ui/base/ActionBarHelper;->setActionBarTitle(Ljava/lang/String;)V

    return-void
.end method

.method public getContentInitializationListener(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/ContentInitializationListener;
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/16 v2, -0x34

    aput v2, v1, v0

    const/4 v0, -0x2

    aput v0, v1, v8

    const/16 v0, -0x27a3

    aput v0, v1, v7

    const/16 v0, -0x4a

    aput v0, v1, v6

    const/16 v0, -0x2eac

    aput v0, v1, v5

    const/16 v0, -0x7d

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/16 v4, -0x57

    aput v4, v0, v2

    const/16 v2, -0x6d

    aput v2, v0, v8

    const/16 v2, -0x27c4

    aput v2, v0, v7

    const/16 v2, -0x28

    aput v2, v0, v6

    const/16 v2, -0x2ecf

    aput v2, v0, v5

    const/16 v2, -0x2f

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$5;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    :goto_2
    return-object v0

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x6

    new-array v1, v0, [I

    const/4 v0, 0x5

    const/16 v2, -0x30

    aput v2, v1, v0

    const/16 v0, -0x7c

    aput v0, v1, v8

    const/16 v0, -0x24

    aput v0, v1, v7

    const/16 v0, 0x1e02

    aput v0, v1, v6

    const/16 v0, 0x7e7b

    aput v0, v1, v5

    const/16 v0, 0x92c

    aput v0, v1, v3

    const/4 v0, 0x6

    new-array v0, v0, [I

    const/4 v2, 0x5

    const/16 v4, -0x4b

    aput v4, v0, v2

    const/16 v2, -0x17

    aput v2, v0, v8

    const/16 v2, -0x43

    aput v2, v0, v7

    const/16 v2, 0x1e6c

    aput v2, v0, v6

    const/16 v2, 0x7e1e

    aput v2, v0, v5

    const/16 v2, 0x97e

    aput v2, v0, v3

    move v2, v3

    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_0

    array-length v0, v1

    new-array v0, v0, [C

    :goto_1
    array-length v2, v0

    if-lt v3, v2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRenameInputController:Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;

    :goto_2
    return-object v0

    :cond_0
    aget v4, v0, v2

    aget v5, v1, v2

    xor-int/2addr v4, v5

    aput v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget v2, v1, v3

    int-to-char v2, v2

    aput-char v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 22

    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onCreate(Landroid/os/Bundle;)V

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$layout;->paired_devices_settings_layout:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->setContentView(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0xc

    aput v12, v3, v11

    const/16 v11, -0x9

    aput v11, v3, v10

    const/16 v10, -0x1f

    aput v10, v3, v9

    const/4 v9, -0x1

    aput v9, v3, v8

    const/16 v8, -0x75

    aput v8, v3, v7

    const/16 v7, -0x56

    aput v7, v3, v6

    const/16 v6, 0x7c6b

    aput v6, v3, v4

    const/16 v4, -0x31c8

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x50

    aput v13, v2, v12

    const/16 v12, -0x42

    aput v12, v2, v11

    const/16 v11, -0x7c

    aput v11, v2, v10

    const/16 v10, -0x64

    aput v10, v2, v9

    const/16 v9, -0x1e

    aput v9, v2, v8

    const/16 v8, -0x24

    aput v8, v2, v7

    const/16 v7, 0x7c0e

    aput v7, v2, v6

    const/16 v6, -0x3184

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_0
    array-length v6, v2

    if-lt v4, v6, :cond_6

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1
    array-length v6, v2

    if-lt v4, v6, :cond_7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x7a16

    aput v12, v3, v11

    const/16 v11, 0x6c33

    aput v11, v3, v10

    const/16 v10, -0x64f7

    aput v10, v3, v9

    const/4 v9, -0x8

    aput v9, v3, v8

    const/16 v8, -0x3fc5

    aput v8, v3, v7

    const/16 v7, -0x4a

    aput v7, v3, v6

    const/16 v6, -0x6a

    aput v6, v3, v4

    const/16 v4, -0x9b4

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x7a52

    aput v13, v2, v12

    const/16 v12, 0x6c7a

    aput v12, v2, v11

    const/16 v11, -0x6494

    aput v11, v2, v10

    const/16 v10, -0x65

    aput v10, v2, v9

    const/16 v9, -0x3fae

    aput v9, v2, v8

    const/16 v8, -0x40

    aput v8, v2, v7

    const/16 v7, -0xd

    aput v7, v2, v6

    const/16 v6, -0x9f8

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_2
    array-length v6, v2

    if-lt v4, v6, :cond_8

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_3
    array-length v6, v2

    if-lt v4, v6, :cond_9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mDeviceID:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0xa

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x7d

    aput v14, v3, v13

    const/16 v13, -0x28b3

    aput v13, v3, v12

    const/16 v12, -0x4a

    aput v12, v3, v11

    const/16 v11, -0x73c8

    aput v11, v3, v10

    const/16 v10, -0x17

    aput v10, v3, v9

    const/16 v9, 0x5636

    aput v9, v3, v8

    const/16 v8, 0x263f

    aput v8, v3, v7

    const/16 v7, 0x5650

    aput v7, v3, v6

    const/16 v6, 0x6933

    aput v6, v3, v4

    const/16 v4, 0x752d

    aput v4, v3, v2

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, -0x1a

    aput v15, v2, v14

    const/16 v14, -0x28e0

    aput v14, v2, v13

    const/16 v13, -0x29

    aput v13, v2, v12

    const/16 v12, -0x738a

    aput v12, v2, v11

    const/16 v11, -0x74

    aput v11, v2, v10

    const/16 v10, 0x5655

    aput v10, v2, v9

    const/16 v9, 0x2656

    aput v9, v2, v8

    const/16 v8, 0x5626

    aput v8, v2, v7

    const/16 v7, 0x6956

    aput v7, v2, v6

    const/16 v6, 0x7569

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_4
    array-length v6, v2

    if-lt v4, v6, :cond_a

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_5
    array-length v6, v2

    if-lt v4, v6, :cond_b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x56

    aput v12, v3, v11

    const/16 v11, -0x53

    aput v11, v3, v10

    const/16 v10, -0x49

    aput v10, v3, v9

    const/16 v9, 0x71

    aput v9, v3, v8

    const/16 v8, -0x89f

    aput v8, v3, v7

    const/16 v7, -0x7d

    aput v7, v3, v6

    const/16 v6, -0x75

    aput v6, v3, v4

    const/16 v4, 0x6c55

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, -0x31

    aput v13, v2, v12

    const/16 v12, -0x23

    aput v12, v2, v11

    const/16 v11, -0x32

    aput v11, v2, v10

    const/16 v10, 0x25

    aput v10, v2, v9

    const/16 v9, -0x900

    aput v9, v2, v8

    const/16 v8, -0x9

    aput v8, v2, v7

    const/16 v7, -0x16

    aput v7, v2, v6

    const/16 v6, 0x6c11

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_6
    array-length v6, v2

    if-lt v4, v6, :cond_c

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_7
    array-length v6, v2

    if-lt v4, v6, :cond_d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->data_type:I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0xa

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x3d91

    aput v14, v3, v13

    const/16 v13, -0x4e

    aput v13, v3, v12

    const/16 v12, -0x1bb2

    aput v12, v3, v11

    const/16 v11, -0x50

    aput v11, v3, v10

    const/16 v10, -0x5f

    aput v10, v3, v9

    const/16 v9, -0x769d

    aput v9, v3, v8

    const/16 v8, -0x20

    aput v8, v3, v7

    const/16 v7, -0x62

    aput v7, v3, v6

    const/16 v6, -0x32

    aput v6, v3, v4

    const/16 v4, -0x4c

    aput v4, v3, v2

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, -0x3df6

    aput v15, v2, v14

    const/16 v14, -0x3e

    aput v14, v2, v13

    const/16 v13, -0x1bc9

    aput v13, v2, v12

    const/16 v12, -0x1c

    aput v12, v2, v11

    const/16 v11, -0x3c

    aput v11, v2, v10

    const/16 v10, -0x7700

    aput v10, v2, v9

    const/16 v9, -0x77

    aput v9, v2, v8

    const/16 v8, -0x18

    aput v8, v2, v7

    const/16 v7, -0x55

    aput v7, v2, v6

    const/16 v6, -0x10

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_8
    array-length v6, v2

    if-lt v4, v6, :cond_e

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_9
    array-length v6, v2

    if-lt v4, v6, :cond_f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->device_type:I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x7d

    aput v20, v3, v19

    const/16 v19, -0x76

    aput v19, v3, v18

    const/16 v18, -0x41

    aput v18, v3, v17

    const/16 v17, -0x53

    aput v17, v3, v16

    const/16 v16, -0x5f

    aput v16, v3, v15

    const/16 v15, -0x1a

    aput v15, v3, v14

    const/16 v14, 0x236d

    aput v14, v3, v13

    const/16 v13, -0x4dab

    aput v13, v3, v12

    const/16 v12, -0x25

    aput v12, v3, v11

    const/16 v11, -0x5c

    aput v11, v3, v10

    const/16 v10, -0x76

    aput v10, v3, v9

    const/16 v9, -0x64

    aput v9, v3, v8

    const/16 v8, 0x5971

    aput v8, v3, v7

    const/16 v7, -0x75c9

    aput v7, v3, v6

    const/16 v6, -0x1b

    aput v6, v3, v4

    const/16 v4, -0x47af

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, -0x1a

    aput v21, v2, v20

    const/16 v20, -0x6

    aput v20, v2, v19

    const/16 v19, -0x3a

    aput v19, v2, v18

    const/16 v18, -0x7

    aput v18, v2, v17

    const/16 v17, -0x28

    aput v17, v2, v16

    const/16 v16, -0x6e

    aput v16, v2, v15

    const/16 v15, 0x2304

    aput v15, v2, v14

    const/16 v14, -0x4ddd

    aput v14, v2, v13

    const/16 v13, -0x4e

    aput v13, v2, v12

    const/16 v12, -0x30

    aput v12, v2, v11

    const/16 v11, -0x17

    aput v11, v2, v10

    const/4 v10, -0x7

    aput v10, v2, v9

    const/16 v9, 0x591f

    aput v9, v2, v8

    const/16 v8, -0x75a7

    aput v8, v2, v7

    const/16 v7, -0x76

    aput v7, v2, v6

    const/16 v6, -0x47ee

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_a
    array-length v6, v2

    if-lt v4, v6, :cond_10

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_b
    array-length v6, v2

    if-lt v4, v6, :cond_11

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->connectivity_type:I

    :cond_0
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x1e

    aput v11, v3, v10

    const/16 v10, -0x77c1

    aput v10, v3, v9

    const/16 v9, -0x13

    aput v9, v3, v8

    const/16 v8, -0x55

    aput v8, v3, v7

    const/16 v7, -0x65

    aput v7, v3, v6

    const/16 v6, -0xafb

    aput v6, v3, v5

    const/16 v5, -0x70

    aput v5, v3, v4

    const/16 v4, -0x3682

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x5a

    aput v12, v2, v11

    const/16 v11, -0x778a

    aput v11, v2, v10

    const/16 v10, -0x78

    aput v10, v2, v9

    const/16 v9, -0x38

    aput v9, v2, v8

    const/16 v8, -0xe

    aput v8, v2, v7

    const/16 v7, -0xa8d

    aput v7, v2, v6

    const/16 v6, -0xb

    aput v6, v2, v5

    const/16 v5, -0x36c6

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_c
    array-length v5, v2

    if-lt v4, v5, :cond_12

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_d
    array-length v5, v2

    if-lt v4, v5, :cond_13

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x46

    aput v11, v3, v10

    const/16 v10, -0x38

    aput v10, v3, v9

    const/16 v9, -0x67bc

    aput v9, v3, v8

    const/4 v8, -0x5

    aput v8, v3, v7

    const/16 v7, -0x1bce

    aput v7, v3, v6

    const/16 v6, -0x6e

    aput v6, v3, v5

    const/16 v5, -0xf9

    aput v5, v3, v4

    const/16 v4, -0x45

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/4 v12, -0x2

    aput v12, v2, v11

    const/16 v11, -0x7f

    aput v11, v2, v10

    const/16 v10, -0x67df

    aput v10, v2, v9

    const/16 v9, -0x68

    aput v9, v2, v8

    const/16 v8, -0x1ba5

    aput v8, v2, v7

    const/16 v7, -0x1c

    aput v7, v2, v6

    const/16 v6, -0x9e

    aput v6, v2, v5

    const/4 v5, -0x1

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_e
    array-length v5, v2

    if-lt v4, v5, :cond_14

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_f
    array-length v5, v2

    if-lt v4, v5, :cond_15

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mDeviceID:Ljava/lang/String;

    :cond_1
    const/16 v2, 0xa

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x5f

    aput v13, v3, v12

    const/16 v12, -0x6a

    aput v12, v3, v11

    const/16 v11, -0x22

    aput v11, v3, v10

    const/16 v10, -0x25

    aput v10, v3, v9

    const/16 v9, 0x2c04

    aput v9, v3, v8

    const/16 v8, -0x43b1

    aput v8, v3, v7

    const/16 v7, -0x2b

    aput v7, v3, v6

    const/16 v6, -0x2d

    aput v6, v3, v5

    const/16 v5, -0x79

    aput v5, v3, v4

    const/16 v4, -0x80

    aput v4, v3, v2

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x3c

    aput v14, v2, v13

    const/4 v13, -0x5

    aput v13, v2, v12

    const/16 v12, -0x41

    aput v12, v2, v11

    const/16 v11, -0x6b

    aput v11, v2, v10

    const/16 v10, 0x2c61

    aput v10, v2, v9

    const/16 v9, -0x43d4

    aput v9, v2, v8

    const/16 v8, -0x44

    aput v8, v2, v7

    const/16 v7, -0x5b

    aput v7, v2, v6

    const/16 v6, -0x1e

    aput v6, v2, v5

    const/16 v5, -0x3c

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_10
    array-length v5, v2

    if-lt v4, v5, :cond_16

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_11
    array-length v5, v2

    if-lt v4, v5, :cond_17

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0xa

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x5601

    aput v13, v3, v12

    const/16 v12, -0x38c5

    aput v12, v3, v11

    const/16 v11, -0x5a

    aput v11, v3, v10

    const/16 v10, -0x45a9

    aput v10, v3, v9

    const/16 v9, -0x21

    aput v9, v3, v8

    const/16 v8, 0x5829

    aput v8, v3, v7

    const/16 v7, 0x2731

    aput v7, v3, v6

    const/16 v6, 0x1251

    aput v6, v3, v5

    const/16 v5, -0x3089

    aput v5, v3, v4

    const/16 v4, -0x75

    aput v4, v3, v2

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x5664

    aput v14, v2, v13

    const/16 v13, -0x38aa

    aput v13, v2, v12

    const/16 v12, -0x39

    aput v12, v2, v11

    const/16 v11, -0x45e7

    aput v11, v2, v10

    const/16 v10, -0x46

    aput v10, v2, v9

    const/16 v9, 0x584a

    aput v9, v2, v8

    const/16 v8, 0x2758

    aput v8, v2, v7

    const/16 v7, 0x1227

    aput v7, v2, v6

    const/16 v6, -0x30ee

    aput v6, v2, v5

    const/16 v5, -0x31

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_12
    array-length v5, v2

    if-lt v4, v5, :cond_18

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_13
    array-length v5, v2

    if-lt v4, v5, :cond_19

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;

    :cond_2
    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x940

    aput v11, v3, v10

    const/16 v10, 0x7779

    aput v10, v3, v9

    const/16 v9, -0x1df2

    aput v9, v3, v8

    const/16 v8, -0x4a

    aput v8, v3, v7

    const/16 v7, -0x67a7

    aput v7, v3, v6

    const/16 v6, -0x14

    aput v6, v3, v5

    const/16 v5, -0x48f1

    aput v5, v3, v4

    const/16 v4, -0xd

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x925

    aput v12, v2, v11

    const/16 v11, 0x7709

    aput v11, v2, v10

    const/16 v10, -0x1d89

    aput v10, v2, v9

    const/16 v9, -0x1e

    aput v9, v2, v8

    const/16 v8, -0x67c8

    aput v8, v2, v7

    const/16 v7, -0x68

    aput v7, v2, v6

    const/16 v6, -0x4892

    aput v6, v2, v5

    const/16 v5, -0x49

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_14
    array-length v5, v2

    if-lt v4, v5, :cond_1a

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_15
    array-length v5, v2

    if-lt v4, v5, :cond_1b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0x8

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x3c

    aput v11, v3, v10

    const/16 v10, -0x3f

    aput v10, v3, v9

    const/16 v9, 0x7411

    aput v9, v3, v8

    const/16 v8, 0x5b20

    aput v8, v3, v7

    const/16 v7, 0x593a

    aput v7, v3, v6

    const/16 v6, 0x592d

    aput v6, v3, v5

    const/16 v5, 0x2338

    aput v5, v3, v4

    const/16 v4, -0x899

    aput v4, v3, v2

    const/16 v2, 0x8

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, -0x5f

    aput v12, v2, v11

    const/16 v11, -0x4f

    aput v11, v2, v10

    const/16 v10, 0x7468

    aput v10, v2, v9

    const/16 v9, 0x5b74

    aput v9, v2, v8

    const/16 v8, 0x595b

    aput v8, v2, v7

    const/16 v7, 0x5959

    aput v7, v2, v6

    const/16 v6, 0x2359

    aput v6, v2, v5

    const/16 v5, -0x8dd

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_16
    array-length v5, v2

    if-lt v4, v5, :cond_1c

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_17
    array-length v5, v2

    if-lt v4, v5, :cond_1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->data_type:I

    :cond_3
    const/16 v2, 0xa

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0x4660

    aput v13, v3, v12

    const/16 v12, 0x3d36

    aput v12, v3, v11

    const/16 v11, -0x65bc

    aput v11, v3, v10

    const/16 v10, -0x32

    aput v10, v3, v9

    const/16 v9, 0x663c

    aput v9, v3, v8

    const/16 v8, -0x77fb

    aput v8, v3, v7

    const/16 v7, -0x1f

    aput v7, v3, v6

    const/16 v6, -0x2e

    aput v6, v3, v5

    const/16 v5, -0x70

    aput v5, v3, v4

    const/16 v4, 0x6240

    aput v4, v3, v2

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0x4605

    aput v14, v2, v13

    const/16 v13, 0x3d46

    aput v13, v2, v12

    const/16 v12, -0x65c3

    aput v12, v2, v11

    const/16 v11, -0x66

    aput v11, v2, v10

    const/16 v10, 0x6659

    aput v10, v2, v9

    const/16 v9, -0x779a

    aput v9, v2, v8

    const/16 v8, -0x78

    aput v8, v2, v7

    const/16 v7, -0x5c

    aput v7, v2, v6

    const/16 v6, -0xb

    aput v6, v2, v5

    const/16 v5, 0x6204

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_18
    array-length v5, v2

    if-lt v4, v5, :cond_1e

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_19
    array-length v5, v2

    if-lt v4, v5, :cond_1f

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0xa

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x3aea

    aput v13, v3, v12

    const/16 v12, -0x4b

    aput v12, v3, v11

    const/16 v11, -0x32a1

    aput v11, v3, v10

    const/16 v10, -0x67

    aput v10, v3, v9

    const/16 v9, -0x6d0

    aput v9, v3, v8

    const/16 v8, -0x66

    aput v8, v3, v7

    const/16 v7, 0x6a56

    aput v7, v3, v6

    const/16 v6, -0x13e4

    aput v6, v3, v5

    const/16 v5, -0x77

    aput v5, v3, v4

    const/16 v4, 0x126d

    aput v4, v3, v2

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x3a8d

    aput v14, v2, v13

    const/16 v13, -0x3b

    aput v13, v2, v12

    const/16 v12, -0x32da

    aput v12, v2, v11

    const/16 v11, -0x33

    aput v11, v2, v10

    const/16 v10, -0x6ab

    aput v10, v2, v9

    const/4 v9, -0x7

    aput v9, v2, v8

    const/16 v8, 0x6a3f

    aput v8, v2, v7

    const/16 v7, -0x1396

    aput v7, v2, v6

    const/16 v6, -0x14

    aput v6, v2, v5

    const/16 v5, 0x1229

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_1a
    array-length v5, v2

    if-lt v4, v5, :cond_20

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1b
    array-length v5, v2

    if-lt v4, v5, :cond_21

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->device_type:I

    :cond_4
    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x72b

    aput v19, v3, v18

    const/16 v18, -0x3c89

    aput v18, v3, v17

    const/16 v17, -0x46

    aput v17, v3, v16

    const/16 v16, -0x4d

    aput v16, v3, v15

    const/16 v15, 0x5d2f

    aput v15, v3, v14

    const/16 v14, 0x429

    aput v14, v3, v13

    const/16 v13, -0x7393

    aput v13, v3, v12

    const/4 v12, -0x6

    aput v12, v3, v11

    const/16 v11, -0x65ca

    aput v11, v3, v10

    const/16 v10, -0x12

    aput v10, v3, v9

    const/16 v9, -0x1dac

    aput v9, v3, v8

    const/16 v8, -0x79

    aput v8, v3, v7

    const/16 v7, -0x39

    aput v7, v3, v6

    const/16 v6, -0x46

    aput v6, v3, v5

    const/16 v5, -0x67

    aput v5, v3, v4

    const/16 v4, -0x2591

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x74e

    aput v20, v2, v19

    const/16 v19, -0x3cf9

    aput v19, v2, v18

    const/16 v18, -0x3d

    aput v18, v2, v17

    const/16 v17, -0x19

    aput v17, v2, v16

    const/16 v16, 0x5d56

    aput v16, v2, v15

    const/16 v15, 0x45d

    aput v15, v2, v14

    const/16 v14, -0x73fc

    aput v14, v2, v13

    const/16 v13, -0x74

    aput v13, v2, v12

    const/16 v12, -0x65a1

    aput v12, v2, v11

    const/16 v11, -0x66

    aput v11, v2, v10

    const/16 v10, -0x1dc9

    aput v10, v2, v9

    const/16 v9, -0x1e

    aput v9, v2, v8

    const/16 v8, -0x57

    aput v8, v2, v7

    const/16 v7, -0x2c

    aput v7, v2, v6

    const/16 v6, -0xa

    aput v6, v2, v5

    const/16 v5, -0x25d4

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_1c
    array-length v5, v2

    if-lt v4, v5, :cond_22

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1d
    array-length v5, v2

    if-lt v4, v5, :cond_23

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x10

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0xe

    aput v19, v3, v18

    const/16 v18, -0x58a8

    aput v18, v3, v17

    const/16 v17, -0x22

    aput v17, v3, v16

    const/16 v16, -0x52

    aput v16, v3, v15

    const/16 v15, -0x1ab2

    aput v15, v3, v14

    const/16 v14, -0x6f

    aput v14, v3, v13

    const/16 v13, -0x62ac

    aput v13, v3, v12

    const/16 v12, -0x15

    aput v12, v3, v11

    const/16 v11, -0x9

    aput v11, v3, v10

    const/16 v10, -0x1b

    aput v10, v3, v9

    const/16 v9, -0x3d

    aput v9, v3, v8

    const/16 v8, -0x71ba

    aput v8, v3, v7

    const/16 v7, -0x20

    aput v7, v3, v6

    const/16 v6, -0xe8e

    aput v6, v3, v5

    const/16 v5, -0x62

    aput v5, v3, v4

    const/16 v4, -0x5b

    aput v4, v3, v2

    const/16 v2, 0x10

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, -0x69

    aput v20, v2, v19

    const/16 v19, -0x58d8

    aput v19, v2, v18

    const/16 v18, -0x59

    aput v18, v2, v17

    const/16 v17, -0x6

    aput v17, v2, v16

    const/16 v16, -0x1ac9

    aput v16, v2, v15

    const/16 v15, -0x1b

    aput v15, v2, v14

    const/16 v14, -0x62c3

    aput v14, v2, v13

    const/16 v13, -0x63

    aput v13, v2, v12

    const/16 v12, -0x62

    aput v12, v2, v11

    const/16 v11, -0x6f

    aput v11, v2, v10

    const/16 v10, -0x60

    aput v10, v2, v9

    const/16 v9, -0x71dd

    aput v9, v2, v8

    const/16 v8, -0x72

    aput v8, v2, v7

    const/16 v7, -0xee4

    aput v7, v2, v6

    const/16 v6, -0xf

    aput v6, v2, v5

    const/16 v5, -0x1a

    aput v5, v2, v4

    const/4 v4, 0x0

    :goto_1e
    array-length v5, v2

    if-lt v4, v5, :cond_24

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_1f
    array-length v5, v2

    if-lt v4, v5, :cond_25

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->connectivity_type:I

    :cond_5
    :goto_20
    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$string;->paired_accessories:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->setTitle(I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mActivity:Landroid/app/Activity;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->rename_layout:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRenameLayout:Landroid/widget/LinearLayout;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->unpair_layout:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mUnpairLayout:Landroid/widget/LinearLayout;

    sget v2, Lcom/sec/android/app/shealth/framework/ui/R$id;->accessary_name:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mNameView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mNameView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mNameView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRenameLayout:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mUnpairLayout:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$2;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const/4 v2, 0x6

    new-array v3, v2, [I

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/16 v10, -0x3e99

    aput v10, v3, v9

    const/16 v9, -0x54

    aput v9, v3, v8

    const/16 v8, 0x3628

    aput v8, v3, v7

    const/16 v7, -0x62a8

    aput v7, v3, v6

    const/4 v6, -0x8

    aput v6, v3, v4

    const/16 v4, -0x76

    aput v4, v3, v2

    const/4 v2, 0x6

    new-array v2, v2, [I

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/16 v11, -0x3efe

    aput v11, v2, v10

    const/16 v10, -0x3f

    aput v10, v2, v9

    const/16 v9, 0x3649

    aput v9, v2, v8

    const/16 v8, -0x62ca

    aput v8, v2, v7

    const/16 v7, -0x63

    aput v7, v2, v6

    const/16 v6, -0x28

    aput v6, v2, v4

    const/4 v4, 0x0

    :goto_22
    array-length v6, v2

    if-lt v4, v6, :cond_27

    array-length v2, v3

    new-array v2, v2, [C

    const/4 v4, 0x0

    :goto_23
    array-length v6, v2

    if-lt v4, v6, :cond_28

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/DialogFragment;

    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->hideSIP()V

    :goto_24
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->registerHomeKeyPress()V

    return-void

    :cond_6
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_7
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_8
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_9
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_3

    :cond_a
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_4

    :cond_b
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_5

    :cond_c
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_6

    :cond_d
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_7

    :cond_e
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_8

    :cond_f
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_9

    :cond_10
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_a

    :cond_11
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_b

    :cond_12
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_c

    :cond_13
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_d

    :cond_14
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_e

    :cond_15
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_f

    :cond_16
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_10

    :cond_17
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_11

    :cond_18
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_12

    :cond_19
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_13

    :cond_1a
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_14

    :cond_1b
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_15

    :cond_1c
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_16

    :cond_1d
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_17

    :cond_1e
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_18

    :cond_1f
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_19

    :cond_20
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1a

    :cond_21
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1b

    :cond_22
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1c

    :cond_23
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1d

    :cond_24
    aget v5, v2, v4

    aget v6, v3, v4

    xor-int/2addr v5, v6

    aput v5, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1e

    :cond_25
    aget v5, v3, v4

    int-to-char v5, v5

    aput-char v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1f

    :cond_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mNameView:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_21

    :cond_27
    aget v6, v2, v4

    aget v7, v3, v4

    xor-int/2addr v6, v7

    aput v6, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_22

    :cond_28
    aget v6, v3, v4

    int-to-char v6, v6

    aput-char v6, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_23

    :catch_0
    move-exception v2

    goto/16 :goto_20

    :catch_1
    move-exception v2

    goto/16 :goto_24
.end method

.method protected onDestroy()V
    .locals 1

    invoke-virtual {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->unRegisterHomeKeyPress()V

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->clearController()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onPause()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->hideSIP()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onResume()V

    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->getFragmentController()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 20

    invoke-virtual/range {p1 .. p1}, Landroid/os/Bundle;->clear()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mDeviceID:Ljava/lang/String;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x7c

    aput v10, v2, v9

    const/16 v9, -0x68

    aput v9, v2, v8

    const/16 v8, -0x9

    aput v8, v2, v7

    const/16 v7, -0xb

    aput v7, v2, v6

    const/4 v6, -0x7

    aput v6, v2, v5

    const/16 v5, -0x7e84

    aput v5, v2, v4

    const/16 v4, -0x1c

    aput v4, v2, v3

    const/16 v3, -0xa

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x40

    aput v11, v1, v10

    const/16 v10, -0x2f

    aput v10, v1, v9

    const/16 v9, -0x6e

    aput v9, v1, v8

    const/16 v8, -0x6a

    aput v8, v1, v7

    const/16 v7, -0x70

    aput v7, v1, v6

    const/16 v6, -0x7ef6

    aput v6, v1, v5

    const/16 v5, -0x7f

    aput v5, v1, v4

    const/16 v4, -0x4e

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mDeviceID:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0x6ece

    aput v12, v2, v11

    const/4 v11, -0x4

    aput v11, v2, v10

    const/16 v10, -0x68

    aput v10, v2, v9

    const/16 v9, -0x1588

    aput v9, v2, v8

    const/16 v8, -0x71

    aput v8, v2, v7

    const/16 v7, 0x21d

    aput v7, v2, v6

    const/16 v6, -0x4d95

    aput v6, v2, v5

    const/16 v5, -0x3c

    aput v5, v2, v4

    const/16 v4, -0x35

    aput v4, v2, v3

    const/16 v3, 0x5b12

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0x6ea9

    aput v13, v1, v12

    const/16 v12, -0x6f

    aput v12, v1, v11

    const/4 v11, -0x7

    aput v11, v1, v10

    const/16 v10, -0x15ca

    aput v10, v1, v9

    const/16 v9, -0x16

    aput v9, v1, v8

    const/16 v8, 0x27e

    aput v8, v1, v7

    const/16 v7, -0x4dfe

    aput v7, v1, v6

    const/16 v6, -0x4e

    aput v6, v1, v5

    const/16 v5, -0x52

    aput v5, v1, v4

    const/16 v4, 0x5b56

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v4, v1

    if-lt v3, v4, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v4, v1

    if-lt v3, v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->accessary_name:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x8

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, -0x43

    aput v10, v2, v9

    const/16 v9, -0x4c

    aput v9, v2, v8

    const/16 v8, -0x3b

    aput v8, v2, v7

    const/16 v7, -0x1e

    aput v7, v2, v6

    const/16 v6, -0x37

    aput v6, v2, v5

    const/16 v5, -0x51

    aput v5, v2, v4

    const/16 v4, -0x60

    aput v4, v2, v3

    const/16 v3, -0x50f0

    aput v3, v2, v1

    const/16 v1, 0x8

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, -0x28

    aput v11, v1, v10

    const/16 v10, -0x3c

    aput v10, v1, v9

    const/16 v9, -0x44

    aput v9, v1, v8

    const/16 v8, -0x4a

    aput v8, v1, v7

    const/16 v7, -0x58

    aput v7, v1, v6

    const/16 v6, -0x25

    aput v6, v1, v5

    const/16 v5, -0x3f

    aput v5, v1, v4

    const/16 v4, -0x50ac

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->data_type:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, -0xb3

    aput v12, v2, v11

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, 0x2072

    aput v10, v2, v9

    const/16 v9, 0x1474

    aput v9, v2, v8

    const/16 v8, -0x148f

    aput v8, v2, v7

    const/16 v7, -0x78

    aput v7, v2, v6

    const/16 v6, 0x864

    aput v6, v2, v5

    const/16 v5, 0x677e

    aput v5, v2, v4

    const/16 v4, -0x6efe

    aput v4, v2, v3

    const/16 v3, -0x2b

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, -0xd8

    aput v13, v1, v12

    const/4 v12, -0x1

    aput v12, v1, v11

    const/16 v11, 0x200b

    aput v11, v1, v10

    const/16 v10, 0x1420

    aput v10, v1, v9

    const/16 v9, -0x14ec

    aput v9, v1, v8

    const/16 v8, -0x15

    aput v8, v1, v7

    const/16 v7, 0x80d

    aput v7, v1, v6

    const/16 v6, 0x6708

    aput v6, v1, v5

    const/16 v5, -0x6e99

    aput v5, v1, v4

    const/16 v4, -0x6f

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v4, v1

    if-lt v3, v4, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v4, v1

    if-lt v3, v4, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->device_type:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/16 v1, 0x10

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, -0x3c

    aput v18, v2, v17

    const/16 v17, 0x2f67

    aput v17, v2, v16

    const/16 v16, 0x6256

    aput v16, v2, v15

    const/16 v15, -0x9ca

    aput v15, v2, v14

    const/16 v14, -0x71

    aput v14, v2, v13

    const/16 v13, -0x14

    aput v13, v2, v12

    const/16 v12, 0x3a1d

    aput v12, v2, v11

    const/16 v11, -0xfb4

    aput v11, v2, v10

    const/16 v10, -0x67

    aput v10, v2, v9

    const/16 v9, -0x4d

    aput v9, v2, v8

    const/16 v8, -0x52c4

    aput v8, v2, v7

    const/16 v7, -0x38

    aput v7, v2, v6

    const/16 v6, 0xe63

    aput v6, v2, v5

    const/16 v5, -0x4a0

    aput v5, v2, v4

    const/16 v4, -0x6c

    aput v4, v2, v3

    const/16 v3, -0x32

    aput v3, v2, v1

    const/16 v1, 0x10

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, -0x5f

    aput v19, v1, v18

    const/16 v18, 0x2f17

    aput v18, v1, v17

    const/16 v17, 0x622f

    aput v17, v1, v16

    const/16 v16, -0x99e

    aput v16, v1, v15

    const/16 v15, -0xa

    aput v15, v1, v14

    const/16 v14, -0x68

    aput v14, v1, v13

    const/16 v13, 0x3a74

    aput v13, v1, v12

    const/16 v12, -0xfc6

    aput v12, v1, v11

    const/16 v11, -0x10

    aput v11, v1, v10

    const/16 v10, -0x39

    aput v10, v1, v9

    const/16 v9, -0x52a1

    aput v9, v1, v8

    const/16 v8, -0x53

    aput v8, v1, v7

    const/16 v7, 0xe0d

    aput v7, v1, v6

    const/16 v6, -0x4f2

    aput v6, v1, v5

    const/4 v5, -0x5

    aput v5, v1, v4

    const/16 v4, -0x73

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_8
    array-length v4, v1

    if-lt v3, v4, :cond_8

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_9
    array-length v4, v1

    if-lt v3, v4, :cond_9

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->connectivity_type:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_a
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/shealth/framework/ui/base/GeneralActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_8
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_8

    :cond_9
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :catch_0
    move-exception v1

    goto :goto_a

    :catch_1
    move-exception v1

    goto :goto_a
.end method

.method public registerHomeKeyPress()V
    .locals 47

    new-instance v1, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;-><init>(Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$1;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const/16 v1, 0x2a

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x6b52

    aput v45, v2, v44

    const/16 v44, -0x41d4

    aput v44, v2, v43

    const/16 v43, -0xf

    aput v43, v2, v42

    const/16 v42, -0x74

    aput v42, v2, v41

    const/16 v41, -0x75

    aput v41, v2, v40

    const/16 v40, -0x4c

    aput v40, v2, v39

    const/16 v39, -0x34

    aput v39, v2, v38

    const/16 v38, -0x49

    aput v38, v2, v37

    const/16 v37, 0x3b5d

    aput v37, v2, v36

    const/16 v36, -0x5382

    aput v36, v2, v35

    const/16 v35, -0x8

    aput v35, v2, v34

    const/16 v34, 0x733f

    aput v34, v2, v33

    const/16 v33, -0x6ed6

    aput v33, v2, v32

    const/16 v32, -0x3e

    aput v32, v2, v31

    const/16 v31, -0x61

    aput v31, v2, v30

    const/16 v30, -0x59

    aput v30, v2, v29

    const/16 v29, -0x7c

    aput v29, v2, v28

    const/16 v28, 0x537f

    aput v28, v2, v27

    const/16 v27, -0x20e1

    aput v27, v2, v26

    const/16 v26, -0x64

    aput v26, v2, v25

    const/16 v25, 0x4614

    aput v25, v2, v24

    const/16 v24, -0x1ad8

    aput v24, v2, v23

    const/16 v23, -0x76

    aput v23, v2, v22

    const/16 v22, -0x23f7

    aput v22, v2, v21

    const/16 v21, -0x58

    aput v21, v2, v20

    const/16 v20, -0x59

    aput v20, v2, v19

    const/16 v19, -0x7efb

    aput v19, v2, v18

    const/16 v18, -0x51

    aput v18, v2, v17

    const/16 v17, 0x5c7b

    aput v17, v2, v16

    const/16 v16, -0x62ce

    aput v16, v2, v15

    const/4 v15, -0x8

    aput v15, v2, v14

    const/16 v14, -0x2d

    aput v14, v2, v13

    const/16 v13, 0x5e59

    aput v13, v2, v12

    const/16 v12, -0x3fc9

    aput v12, v2, v11

    const/16 v11, -0x12

    aput v11, v2, v10

    const/16 v10, 0x2c54

    aput v10, v2, v9

    const/16 v9, 0x4e45

    aput v9, v2, v8

    const/16 v8, -0x61df

    aput v8, v2, v7

    const/16 v7, -0x14

    aput v7, v2, v6

    const/16 v6, 0x2358

    aput v6, v2, v5

    const/16 v5, 0x4d4d

    aput v5, v2, v3

    const/16 v3, 0x6a2c

    aput v3, v2, v1

    const/16 v1, 0x2a

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x6b01

    aput v46, v1, v45

    const/16 v45, -0x4195

    aput v45, v1, v44

    const/16 v44, -0x42

    aput v44, v1, v43

    const/16 v43, -0x40

    aput v43, v1, v42

    const/16 v42, -0x36

    aput v42, v1, v41

    const/16 v41, -0x3

    aput v41, v1, v40

    const/16 v40, -0x78

    aput v40, v1, v39

    const/16 v39, -0x18

    aput v39, v1, v38

    const/16 v38, 0x3b10

    aput v38, v1, v37

    const/16 v37, -0x53c5

    aput v37, v1, v36

    const/16 v36, -0x54

    aput v36, v1, v35

    const/16 v35, 0x736c

    aput v35, v1, v34

    const/16 v34, -0x6e8d

    aput v34, v1, v33

    const/16 v33, -0x6f

    aput v33, v1, v32

    const/16 v32, -0x40

    aput v32, v1, v31

    const/16 v31, -0x1e

    aput v31, v1, v30

    const/16 v30, -0x29

    aput v30, v1, v29

    const/16 v29, 0x5330

    aput v29, v1, v28

    const/16 v28, -0x20ad

    aput v28, v1, v27

    const/16 v27, -0x21

    aput v27, v1, v26

    const/16 v26, 0x463a

    aput v26, v1, v25

    const/16 v25, -0x1aba

    aput v25, v1, v24

    const/16 v24, -0x1b

    aput v24, v1, v23

    const/16 v23, -0x23a0

    aput v23, v1, v22

    const/16 v22, -0x24

    aput v22, v1, v21

    const/16 v21, -0x3c

    aput v21, v1, v20

    const/16 v20, -0x7e9c

    aput v20, v1, v19

    const/16 v19, -0x7f

    aput v19, v1, v18

    const/16 v18, 0x5c0f

    aput v18, v1, v17

    const/16 v17, -0x62a4

    aput v17, v1, v16

    const/16 v16, -0x63

    aput v16, v1, v15

    const/16 v15, -0x59

    aput v15, v1, v14

    const/16 v14, 0x5e37

    aput v14, v1, v13

    const/16 v13, -0x3fa2

    aput v13, v1, v12

    const/16 v12, -0x40

    aput v12, v1, v11

    const/16 v11, 0x2c30

    aput v11, v1, v10

    const/16 v10, 0x4e2c

    aput v10, v1, v9

    const/16 v9, -0x61b2

    aput v9, v1, v8

    const/16 v8, -0x62

    aput v8, v1, v7

    const/16 v7, 0x233c

    aput v7, v1, v6

    const/16 v6, 0x4d23

    aput v6, v1, v5

    const/16 v5, 0x6a4d

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v5, v1

    if-lt v3, v5, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v5, v1

    if-lt v3, v5, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_0
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method protected rename()V
    .locals 40

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, -0x39

    aput v31, v2, v30

    const/16 v30, 0x2b56

    aput v30, v2, v29

    const/16 v29, 0x6d42

    aput v29, v2, v28

    const/16 v28, -0x4e5

    aput v28, v2, v27

    const/16 v27, -0x6e

    aput v27, v2, v26

    const/16 v26, -0x27cc

    aput v26, v2, v25

    const/16 v25, -0x45

    aput v25, v2, v24

    const/16 v24, -0x7a2

    aput v24, v2, v23

    const/16 v23, -0x75

    aput v23, v2, v22

    const/16 v22, 0x3b1a

    aput v22, v2, v21

    const/16 v21, 0x1055

    aput v21, v2, v20

    const/16 v20, 0x6679

    aput v20, v2, v19

    const/16 v19, -0x78ee

    aput v19, v2, v18

    const/16 v18, -0xd

    aput v18, v2, v17

    const/16 v17, -0x8

    aput v17, v2, v16

    const/16 v16, -0x76

    aput v16, v2, v15

    const/16 v15, -0x48

    aput v15, v2, v14

    const/16 v14, -0x59c8

    aput v14, v2, v13

    const/16 v13, -0x3b

    aput v13, v2, v12

    const/16 v12, -0x1ea2

    aput v12, v2, v11

    const/16 v11, -0x69

    aput v11, v2, v10

    const/16 v10, -0x75

    aput v10, v2, v9

    const/16 v9, -0x4a

    aput v9, v2, v8

    const/16 v8, 0xd12

    aput v8, v2, v7

    const/16 v7, 0x68

    aput v7, v2, v6

    const/16 v6, -0x528e

    aput v6, v2, v5

    const/16 v5, -0x3c

    aput v5, v2, v4

    const/16 v4, 0x1562

    aput v4, v2, v3

    const/16 v3, 0x2b45

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, -0x42

    aput v32, v1, v31

    const/16 v31, 0x2b22

    aput v31, v1, v30

    const/16 v30, 0x6d2b

    aput v30, v1, v29

    const/16 v29, -0x493

    aput v29, v1, v28

    const/16 v28, -0x5

    aput v28, v1, v27

    const/16 v27, -0x27c0

    aput v27, v1, v26

    const/16 v26, -0x28

    aput v26, v1, v25

    const/16 v25, -0x7e1

    aput v25, v1, v24

    const/16 v24, -0x8

    aput v24, v1, v23

    const/16 v23, 0x3b7d

    aput v23, v1, v22

    const/16 v22, 0x103b

    aput v22, v1, v21

    const/16 v21, 0x6610

    aput v21, v1, v20

    const/16 v20, -0x789a

    aput v20, v1, v19

    const/16 v19, -0x79

    aput v19, v1, v18

    const/16 v18, -0x63

    aput v18, v1, v17

    const/16 v17, -0x27

    aput v17, v1, v16

    const/16 v16, -0x35

    aput v16, v1, v15

    const/16 v15, -0x59a3

    aput v15, v1, v14

    const/16 v14, -0x5a

    aput v14, v1, v13

    const/16 v13, -0x1ec9

    aput v13, v1, v12

    const/16 v12, -0x1f

    aput v12, v1, v11

    const/16 v11, -0x12

    aput v11, v1, v10

    const/16 v10, -0xe

    aput v10, v1, v9

    const/16 v9, 0xd76

    aput v9, v1, v8

    const/16 v8, 0xd

    aput v8, v1, v7

    const/16 v7, -0x5300

    aput v7, v1, v6

    const/16 v6, -0x53

    aput v6, v1, v5

    const/16 v5, 0x1503

    aput v5, v1, v4

    const/16 v4, 0x2b15

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x1c

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, -0x39f9

    aput v32, v2, v31

    const/16 v31, -0x4

    aput v31, v2, v30

    const/16 v30, -0x63

    aput v30, v2, v29

    const/16 v29, -0x62

    aput v29, v2, v28

    const/16 v28, -0x3d

    aput v28, v2, v27

    const/16 v27, 0x454e

    aput v27, v2, v26

    const/16 v26, 0x6e33

    aput v26, v2, v25

    const/16 v25, 0x20b

    aput v25, v2, v24

    const/16 v24, 0x5546

    aput v24, v2, v23

    const/16 v23, -0x158b

    aput v23, v2, v22

    const/16 v22, -0x67

    aput v22, v2, v21

    const/16 v21, -0x3e

    aput v21, v2, v20

    const/16 v20, -0x51

    aput v20, v2, v19

    const/16 v19, 0x1a4d

    aput v19, v2, v18

    const/16 v18, -0x2192

    aput v18, v2, v17

    const/16 v17, -0x56

    aput v17, v2, v16

    const/16 v16, -0x1e

    aput v16, v2, v15

    const/16 v15, 0x2943

    aput v15, v2, v14

    const/16 v14, 0x3209

    aput v14, v2, v13

    const/16 v13, -0x7fac

    aput v13, v2, v12

    const/16 v12, -0x11

    aput v12, v2, v11

    const/16 v11, -0x52

    aput v11, v2, v10

    const/16 v10, -0x3a

    aput v10, v2, v9

    const/16 v9, -0x36

    aput v9, v2, v8

    const/16 v8, 0x571e

    aput v8, v2, v7

    const/16 v7, -0x11ca

    aput v7, v2, v6

    const/16 v6, -0x68

    aput v6, v2, v3

    const/16 v3, -0x3f

    aput v3, v2, v1

    const/16 v1, 0x1c

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, 0x15

    const/16 v27, 0x16

    const/16 v28, 0x17

    const/16 v29, 0x18

    const/16 v30, 0x19

    const/16 v31, 0x1a

    const/16 v32, 0x1b

    const/16 v33, -0x39d9

    aput v33, v1, v32

    const/16 v32, -0x3a

    aput v32, v1, v31

    const/16 v31, -0x43

    aput v31, v1, v30

    const/16 v30, -0x5

    aput v30, v1, v29

    const/16 v29, -0x60

    aput v29, v1, v28

    const/16 v28, 0x4527

    aput v28, v1, v27

    const/16 v27, 0x6e45

    aput v27, v1, v26

    const/16 v26, 0x26e

    aput v26, v1, v25

    const/16 v25, 0x5502

    aput v25, v1, v24

    const/16 v24, -0x15ab

    aput v24, v1, v23

    const/16 v23, -0x16

    aput v23, v1, v22

    const/16 v22, -0x5b

    aput v22, v1, v21

    const/16 v21, -0x3f

    aput v21, v1, v20

    const/16 v20, 0x1a24

    aput v20, v1, v19

    const/16 v19, -0x21e6

    aput v19, v1, v18

    const/16 v18, -0x22

    aput v18, v1, v17

    const/16 v17, -0x79

    aput v17, v1, v16

    const/16 v16, 0x2930

    aput v16, v1, v15

    const/16 v15, 0x3229

    aput v15, v1, v14

    const/16 v14, -0x7fce

    aput v14, v1, v13

    const/16 v13, -0x80

    aput v13, v1, v12

    const/16 v12, -0x72

    aput v12, v1, v11

    const/16 v11, -0x5d

    aput v11, v1, v10

    const/16 v10, -0x41

    aput v10, v1, v9

    const/16 v9, 0x5772

    aput v9, v1, v8

    const/16 v8, -0x11a9

    aput v8, v1, v7

    const/16 v7, -0x12

    aput v7, v1, v6

    const/16 v6, -0x1f

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v6, v1

    if-lt v3, v6, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v6, v1

    if-lt v3, v6, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->settingsSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->settingsSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mRename:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->renameDevice(Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;Ljava/lang/String;)Z

    move-result v4

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, -0x1ab7

    aput v32, v2, v31

    const/16 v31, -0x6f

    aput v31, v2, v30

    const/16 v30, -0x42

    aput v30, v2, v29

    const/16 v29, -0x35

    aput v29, v2, v28

    const/16 v28, 0x776a

    aput v28, v2, v27

    const/16 v27, -0x7dfd

    aput v27, v2, v26

    const/16 v26, -0x1f

    aput v26, v2, v25

    const/16 v25, -0x2b84

    aput v25, v2, v24

    const/16 v24, -0x59

    aput v24, v2, v23

    const/16 v23, 0x5375

    aput v23, v2, v22

    const/16 v22, -0x6dc3

    aput v22, v2, v21

    const/16 v21, -0x5

    aput v21, v2, v20

    const/16 v20, -0x5300

    aput v20, v2, v19

    const/16 v19, -0x27

    aput v19, v2, v18

    const/16 v18, -0xa

    aput v18, v2, v17

    const/16 v17, 0x2717

    aput v17, v2, v16

    const/16 v16, -0x26ac

    aput v16, v2, v15

    const/16 v15, -0x44

    aput v15, v2, v14

    const/16 v14, 0x4a38

    aput v14, v2, v13

    const/16 v13, 0x6723

    aput v13, v2, v12

    const/16 v12, -0x59ef

    aput v12, v2, v11

    const/16 v11, -0x3d

    aput v11, v2, v10

    const/16 v10, -0x76

    aput v10, v2, v9

    const/16 v9, 0x5373

    aput v9, v2, v8

    const/16 v8, -0x1bca

    aput v8, v2, v7

    const/16 v7, -0x6a

    aput v7, v2, v6

    const/16 v6, -0x64

    aput v6, v2, v5

    const/16 v5, -0x3fde

    aput v5, v2, v3

    const/16 v3, -0x70

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, -0x1ad0

    aput v33, v1, v32

    const/16 v32, -0x1b

    aput v32, v1, v31

    const/16 v31, -0x29

    aput v31, v1, v30

    const/16 v30, -0x43

    aput v30, v1, v29

    const/16 v29, 0x7703

    aput v29, v1, v28

    const/16 v28, -0x7d89

    aput v28, v1, v27

    const/16 v27, -0x7e

    aput v27, v1, v26

    const/16 v26, -0x2bc3

    aput v26, v1, v25

    const/16 v25, -0x2c

    aput v25, v1, v24

    const/16 v24, 0x5312

    aput v24, v1, v23

    const/16 v23, -0x6dad

    aput v23, v1, v22

    const/16 v22, -0x6e

    aput v22, v1, v21

    const/16 v21, -0x528c

    aput v21, v1, v20

    const/16 v20, -0x53

    aput v20, v1, v19

    const/16 v19, -0x6d

    aput v19, v1, v18

    const/16 v18, 0x2744

    aput v18, v1, v17

    const/16 v17, -0x26d9

    aput v17, v1, v16

    const/16 v16, -0x27

    aput v16, v1, v15

    const/16 v15, 0x4a5b

    aput v15, v1, v14

    const/16 v14, 0x674a

    aput v14, v1, v13

    const/16 v13, -0x5999

    aput v13, v1, v12

    const/16 v12, -0x5a

    aput v12, v1, v11

    const/16 v11, -0x32

    aput v11, v1, v10

    const/16 v10, 0x5317

    aput v10, v1, v9

    const/16 v9, -0x1bad

    aput v9, v1, v8

    const/16 v8, -0x1c

    aput v8, v1, v7

    const/16 v7, -0xb

    aput v7, v1, v6

    const/16 v6, -0x3fbd

    aput v6, v1, v5

    const/16 v5, -0x40

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v5, v1

    if-lt v3, v5, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v5, v1

    if-lt v3, v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x15

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, 0xa

    const/16 v16, 0xb

    const/16 v17, 0xc

    const/16 v18, 0xd

    const/16 v19, 0xe

    const/16 v20, 0xf

    const/16 v21, 0x10

    const/16 v22, 0x11

    const/16 v23, 0x12

    const/16 v24, 0x13

    const/16 v25, 0x14

    const/16 v26, -0x70

    aput v26, v2, v25

    const/16 v25, -0x26b4

    aput v25, v2, v24

    const/16 v24, -0x48

    aput v24, v2, v23

    const/16 v23, -0x46

    aput v23, v2, v22

    const/16 v22, -0x7f

    aput v22, v2, v21

    const/16 v21, -0x6dee

    aput v21, v2, v20

    const/16 v20, -0x3

    aput v20, v2, v19

    const/16 v19, -0x28

    aput v19, v2, v18

    const/16 v18, -0x68

    aput v18, v2, v17

    const/16 v17, -0x60cc

    aput v17, v2, v16

    const/16 v16, -0x13

    aput v16, v2, v15

    const/16 v15, -0x1a87

    aput v15, v2, v14

    const/16 v14, -0x6b

    aput v14, v2, v13

    const/16 v13, 0x7c24

    aput v13, v2, v12

    const/16 v12, 0x5c5c

    aput v12, v2, v11

    const/16 v11, 0x5039

    aput v11, v2, v10

    const/16 v10, -0x3cc3

    aput v10, v2, v9

    const/16 v9, -0x5e

    aput v9, v2, v8

    const/16 v8, 0x1348

    aput v8, v2, v7

    const/16 v7, 0x776

    aput v7, v2, v3

    const/16 v3, 0x5a55

    aput v3, v2, v1

    const/16 v1, 0x15

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x4

    const/4 v11, 0x5

    const/4 v12, 0x6

    const/4 v13, 0x7

    const/16 v14, 0x8

    const/16 v15, 0x9

    const/16 v16, 0xa

    const/16 v17, 0xb

    const/16 v18, 0xc

    const/16 v19, 0xd

    const/16 v20, 0xe

    const/16 v21, 0xf

    const/16 v22, 0x10

    const/16 v23, 0x11

    const/16 v24, 0x12

    const/16 v25, 0x13

    const/16 v26, 0x14

    const/16 v27, -0x50

    aput v27, v1, v26

    const/16 v26, -0x26c1

    aput v26, v1, v25

    const/16 v25, -0x27

    aput v25, v1, v24

    const/16 v24, -0x33

    aput v24, v1, v23

    const/16 v23, -0x5f

    aput v23, v1, v22

    const/16 v22, -0x6d84

    aput v22, v1, v21

    const/16 v21, -0x6e

    aput v21, v1, v20

    const/16 v20, -0x4f

    aput v20, v1, v19

    const/16 v19, -0x14

    aput v19, v1, v18

    const/16 v18, -0x60ab

    aput v18, v1, v17

    const/16 v17, -0x61

    aput v17, v1, v16

    const/16 v16, -0x1ae4

    aput v16, v1, v15

    const/16 v15, -0x1b

    aput v15, v1, v14

    const/16 v14, 0x7c4b

    aput v14, v1, v13

    const/16 v13, 0x5c7c

    aput v13, v1, v12

    const/16 v12, 0x505c

    aput v12, v1, v11

    const/16 v11, -0x3cb0

    aput v11, v1, v10

    const/16 v10, -0x3d

    aput v10, v1, v9

    const/16 v9, 0x1326

    aput v9, v1, v8

    const/16 v8, 0x713

    aput v8, v1, v7

    const/16 v7, 0x5a07

    aput v7, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v7, v1

    if-lt v3, v7, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v7, v1

    if-lt v3, v7, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v4, :cond_8

    const-string v1, ""

    :goto_8
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v1, 0xa

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, -0x29

    aput v14, v2, v13

    const/16 v13, -0x4d

    aput v13, v2, v12

    const/16 v12, -0x5a

    aput v12, v2, v11

    const/16 v11, -0x70d9

    aput v11, v2, v10

    const/4 v10, -0x4

    aput v10, v2, v9

    const/16 v9, -0x53b7

    aput v9, v2, v8

    const/16 v8, -0x31

    aput v8, v2, v7

    const/16 v7, -0x6586

    aput v7, v2, v6

    const/16 v6, -0x11

    aput v6, v2, v3

    const/16 v3, -0x10dd

    aput v3, v2, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x4

    const/4 v10, 0x5

    const/4 v11, 0x6

    const/4 v12, 0x7

    const/16 v13, 0x8

    const/16 v14, 0x9

    const/16 v15, -0x45

    aput v15, v1, v14

    const/16 v14, -0x3a

    aput v14, v1, v13

    const/16 v13, -0x40

    aput v13, v1, v12

    const/16 v12, -0x70ac

    aput v12, v1, v11

    const/16 v11, -0x71

    aput v11, v1, v10

    const/16 v10, -0x53d4

    aput v10, v1, v9

    const/16 v9, -0x54

    aput v9, v1, v8

    const/16 v8, -0x65e7

    aput v8, v1, v7

    const/16 v7, -0x66

    aput v7, v1, v6

    const/16 v6, -0x10b0

    aput v6, v1, v3

    const/4 v3, 0x0

    :goto_9
    array-length v6, v1

    if-lt v3, v6, :cond_b

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_a
    array-length v6, v1

    if-lt v3, v6, :cond_c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->requestPairedDevice()V

    :goto_b
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v7, v1, v3

    aget v8, v2, v3

    xor-int/2addr v7, v8

    aput v7, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v7, v2, v3

    int-to-char v7, v7

    aput-char v7, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :cond_8
    const/4 v1, 0x4

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v7, 0x3

    const/16 v8, 0x716

    aput v8, v2, v7

    const/16 v7, 0x6373

    aput v7, v2, v4

    const/16 v4, 0x10c

    aput v4, v2, v3

    const/16 v3, -0x4a91

    aput v3, v2, v1

    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/16 v9, 0x736

    aput v9, v1, v8

    const/16 v8, 0x6307

    aput v8, v1, v7

    const/16 v7, 0x163

    aput v7, v1, v4

    const/16 v4, -0x4aff

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_c
    array-length v4, v1

    if-lt v3, v4, :cond_9

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_d
    array-length v4, v1

    if-lt v3, v4, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_8

    :cond_9
    aget v4, v1, v3

    aget v7, v2, v3

    xor-int/2addr v4, v7

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    :cond_a
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    :cond_b
    aget v6, v1, v3

    aget v7, v2, v3

    xor-int/2addr v6, v7

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_9

    :cond_c
    aget v6, v2, v3

    int-to-char v6, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_a

    :catch_0
    move-exception v1

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, -0x19a

    aput v31, v2, v30

    const/16 v30, -0x76

    aput v30, v2, v29

    const/16 v29, -0x2e

    aput v29, v2, v28

    const/16 v28, -0x24e2

    aput v28, v2, v27

    const/16 v27, -0x4e

    aput v27, v2, v26

    const/16 v26, -0xffc

    aput v26, v2, v25

    const/16 v25, -0x6d

    aput v25, v2, v24

    const/16 v24, -0xe

    aput v24, v2, v23

    const/16 v23, -0x31

    aput v23, v2, v22

    const/16 v22, -0x78

    aput v22, v2, v21

    const/16 v21, -0x4f

    aput v21, v2, v20

    const/16 v20, 0x2353

    aput v20, v2, v19

    const/16 v19, 0x2557

    aput v19, v2, v18

    const/16 v18, -0x20af

    aput v18, v2, v17

    const/16 v17, -0x46

    aput v17, v2, v16

    const/16 v16, -0x6b

    aput v16, v2, v15

    const/16 v15, -0x28

    aput v15, v2, v14

    const/16 v14, -0x4ddc

    aput v14, v2, v13

    const/16 v13, -0x2f

    aput v13, v2, v12

    const/4 v12, -0x1

    aput v12, v2, v11

    const/16 v11, -0x70d3

    aput v11, v2, v10

    const/16 v10, -0x16

    aput v10, v2, v9

    const/16 v9, -0x43

    aput v9, v2, v8

    const/16 v8, 0x335e

    aput v8, v2, v7

    const/16 v7, 0x6856

    aput v7, v2, v6

    const/16 v6, 0xf1a

    aput v6, v2, v5

    const/16 v5, -0x349a

    aput v5, v2, v4

    const/16 v4, -0x56

    aput v4, v2, v3

    const/16 v3, 0x3779

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, -0x1e1

    aput v32, v1, v31

    const/16 v31, -0x2

    aput v31, v1, v30

    const/16 v30, -0x45

    aput v30, v1, v29

    const/16 v29, -0x2498

    aput v29, v1, v28

    const/16 v28, -0x25

    aput v28, v1, v27

    const/16 v27, -0xf90

    aput v27, v1, v26

    const/16 v26, -0x10

    aput v26, v1, v25

    const/16 v25, -0x4d

    aput v25, v1, v24

    const/16 v24, -0x44

    aput v24, v1, v23

    const/16 v23, -0x11

    aput v23, v1, v22

    const/16 v22, -0x21

    aput v22, v1, v21

    const/16 v21, 0x233a

    aput v21, v1, v20

    const/16 v20, 0x2523

    aput v20, v1, v19

    const/16 v19, -0x20db

    aput v19, v1, v18

    const/16 v18, -0x21

    aput v18, v1, v17

    const/16 v17, -0x3a

    aput v17, v1, v16

    const/16 v16, -0x55

    aput v16, v1, v15

    const/16 v15, -0x4dbf

    aput v15, v1, v14

    const/16 v14, -0x4e

    aput v14, v1, v13

    const/16 v13, -0x6a

    aput v13, v1, v12

    const/16 v12, -0x70a5

    aput v12, v1, v11

    const/16 v11, -0x71

    aput v11, v1, v10

    const/4 v10, -0x7

    aput v10, v1, v9

    const/16 v9, 0x333a

    aput v9, v1, v8

    const/16 v8, 0x6833

    aput v8, v1, v7

    const/16 v7, 0xf68

    aput v7, v1, v6

    const/16 v6, -0x34f1

    aput v6, v1, v5

    const/16 v5, -0x35

    aput v5, v1, v4

    const/16 v4, 0x3729

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_e
    array-length v4, v1

    if-lt v3, v4, :cond_d

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_f
    array-length v4, v1

    if-lt v3, v4, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x23

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, -0xd

    aput v38, v2, v37

    const/16 v37, -0xe

    aput v37, v2, v36

    const/16 v36, -0x9

    aput v36, v2, v35

    const/16 v35, -0x7f

    aput v35, v2, v34

    const/16 v34, -0x14db

    aput v34, v2, v33

    const/16 v33, -0x68

    aput v33, v2, v32

    const/16 v32, -0x5094

    aput v32, v2, v31

    const/16 v31, -0x71

    aput v31, v2, v30

    const/16 v30, -0x2c

    aput v30, v2, v29

    const/16 v29, 0x1675

    aput v29, v2, v28

    const/16 v28, 0x2e7a

    aput v28, v2, v27

    const/16 v27, 0x2542

    aput v27, v2, v26

    const/16 v26, 0x554a

    aput v26, v2, v25

    const/16 v25, -0x63d9

    aput v25, v2, v24

    const/16 v24, -0x18

    aput v24, v2, v23

    const/16 v23, -0x55d2

    aput v23, v2, v22

    const/16 v22, -0x3b

    aput v22, v2, v21

    const/16 v21, -0x48

    aput v21, v2, v20

    const/16 v20, -0x3c96

    aput v20, v2, v19

    const/16 v19, -0x53

    aput v19, v2, v18

    const/16 v18, -0x1d

    aput v18, v2, v17

    const/16 v17, 0x7d68

    aput v17, v2, v16

    const/16 v16, 0x5a1a

    aput v16, v2, v15

    const/16 v15, 0x3e3b

    aput v15, v2, v14

    const/16 v14, 0x4c4c

    aput v14, v2, v13

    const/16 v13, 0x6d0a

    aput v13, v2, v12

    const/16 v12, 0x640a

    aput v12, v2, v11

    const/16 v11, 0x740a

    aput v11, v2, v10

    const/16 v10, 0xc1d

    aput v10, v2, v9

    const/16 v9, -0x6f9e

    aput v9, v2, v8

    const/4 v8, -0x2

    aput v8, v2, v7

    const/16 v7, -0xc

    aput v7, v2, v6

    const/16 v6, -0x66

    aput v6, v2, v5

    const/16 v5, 0x5b38

    aput v5, v2, v3

    const/16 v3, 0x5436

    aput v3, v2, v1

    const/16 v1, 0x23

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, -0x61

    aput v39, v1, v38

    const/16 v38, -0x62

    aput v38, v1, v37

    const/16 v37, -0x7e

    aput v37, v1, v36

    const/16 v36, -0x11

    aput v36, v1, v35

    const/16 v35, -0x14fb

    aput v35, v1, v34

    const/16 v34, -0x15

    aput v34, v1, v33

    const/16 v33, -0x50fb

    aput v33, v1, v32

    const/16 v32, -0x51

    aput v32, v1, v31

    const/16 v31, -0x5a

    aput v31, v1, v30

    const/16 v30, 0x1610

    aput v30, v1, v29

    const/16 v29, 0x2e16

    aput v29, v1, v28

    const/16 v28, 0x252e

    aput v28, v1, v27

    const/16 v27, 0x5525

    aput v27, v1, v26

    const/16 v26, -0x63ab

    aput v26, v1, v25

    const/16 v25, -0x64

    aput v25, v1, v24

    const/16 v24, -0x55c0

    aput v24, v1, v23

    const/16 v23, -0x56

    aput v23, v1, v22

    const/16 v22, -0x5

    aput v22, v1, v21

    const/16 v21, -0x3ce2

    aput v21, v1, v20

    const/16 v20, -0x3d

    aput v20, v1, v19

    const/16 v19, -0x7a

    aput v19, v1, v18

    const/16 v18, 0x7d05

    aput v18, v1, v17

    const/16 v17, 0x5a7d

    aput v17, v1, v16

    const/16 v16, 0x3e5a

    aput v16, v1, v15

    const/16 v15, 0x4c3e

    aput v15, v1, v14

    const/16 v14, 0x6d4c

    aput v14, v1, v13

    const/16 v13, 0x646d

    aput v13, v1, v12

    const/16 v12, 0x7464

    aput v12, v1, v11

    const/16 v11, 0xc74

    aput v11, v1, v10

    const/16 v10, -0x6ff4

    aput v10, v1, v9

    const/16 v9, -0x70

    aput v9, v1, v8

    const/16 v8, -0x6b

    aput v8, v1, v7

    const/4 v7, -0x7

    aput v7, v1, v6

    const/16 v6, 0x5b6b

    aput v6, v1, v5

    const/16 v5, 0x545b

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_10
    array-length v5, v1

    if-lt v3, v5, :cond_f

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_11
    array-length v5, v1

    if-lt v3, v5, :cond_10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b

    :cond_d
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_e
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    :cond_f
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    :cond_10
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_11
.end method

.method public requestPairedDevice()V
    .locals 51

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, -0x7c

    aput v31, v2, v30

    const/16 v30, -0x7d

    aput v30, v2, v29

    const/16 v29, -0x3a

    aput v29, v2, v28

    const/16 v28, -0x1a

    aput v28, v2, v27

    const/16 v27, -0x62

    aput v27, v2, v26

    const/16 v26, 0x5e24

    aput v26, v2, v25

    const/16 v25, 0x6b3d

    aput v25, v2, v24

    const/16 v24, -0x18d6

    aput v24, v2, v23

    const/16 v23, -0x6c

    aput v23, v2, v22

    const/16 v22, -0x788

    aput v22, v2, v21

    const/16 v21, -0x6a

    aput v21, v2, v20

    const/16 v20, -0x16

    aput v20, v2, v19

    const/16 v19, -0x75f1

    aput v19, v2, v18

    const/16 v18, -0x2

    aput v18, v2, v17

    const/16 v17, 0x203a

    aput v17, v2, v16

    const/16 v16, -0x528d

    aput v16, v2, v15

    const/16 v15, -0x22

    aput v15, v2, v14

    const/16 v14, -0x72b3

    aput v14, v2, v13

    const/16 v13, -0x12

    aput v13, v2, v12

    const/16 v12, -0x6bc

    aput v12, v2, v11

    const/16 v11, -0x71

    aput v11, v2, v10

    const/16 v10, -0x33

    aput v10, v2, v9

    const/16 v9, -0x309d

    aput v9, v2, v8

    const/16 v8, -0x55

    aput v8, v2, v7

    const/16 v7, -0x2ca7

    aput v7, v2, v6

    const/16 v6, -0x5f

    aput v6, v2, v5

    const/16 v5, -0x6a9a

    aput v5, v2, v4

    const/16 v4, -0xc

    aput v4, v2, v3

    const/16 v3, -0x728d

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, -0x3

    aput v32, v1, v31

    const/16 v31, -0x9

    aput v31, v1, v30

    const/16 v30, -0x51

    aput v30, v1, v29

    const/16 v29, -0x70

    aput v29, v1, v28

    const/16 v28, -0x9

    aput v28, v1, v27

    const/16 v27, 0x5e50

    aput v27, v1, v26

    const/16 v26, 0x6b5e

    aput v26, v1, v25

    const/16 v25, -0x1895

    aput v25, v1, v24

    const/16 v24, -0x19

    aput v24, v1, v23

    const/16 v23, -0x7e1

    aput v23, v1, v22

    const/16 v22, -0x8

    aput v22, v1, v21

    const/16 v21, -0x7d

    aput v21, v1, v20

    const/16 v20, -0x7585

    aput v20, v1, v19

    const/16 v19, -0x76

    aput v19, v1, v18

    const/16 v18, 0x205f

    aput v18, v1, v17

    const/16 v17, -0x52e0

    aput v17, v1, v16

    const/16 v16, -0x53

    aput v16, v1, v15

    const/16 v15, -0x72d8

    aput v15, v1, v14

    const/16 v14, -0x73

    aput v14, v1, v13

    const/16 v13, -0x6d3

    aput v13, v1, v12

    const/4 v12, -0x7

    aput v12, v1, v11

    const/16 v11, -0x58

    aput v11, v1, v10

    const/16 v10, -0x30d9

    aput v10, v1, v9

    const/16 v9, -0x31

    aput v9, v1, v8

    const/16 v8, -0x2cc4

    aput v8, v1, v7

    const/16 v7, -0x2d

    aput v7, v1, v6

    const/16 v6, -0x6af1

    aput v6, v1, v5

    const/16 v5, -0x6b

    aput v5, v1, v4

    const/16 v4, -0x72dd

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_0
    array-length v4, v1

    if-lt v3, v4, :cond_0

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_1
    array-length v4, v1

    if-lt v3, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x1b

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, -0x34

    aput v30, v2, v29

    const/16 v29, -0x28

    aput v29, v2, v28

    const/16 v28, 0x6001

    aput v28, v2, v27

    const/16 v27, 0x6c0c

    aput v27, v2, v26

    const/16 v26, -0x45f3

    aput v26, v2, v25

    const/16 v25, -0x27

    aput v25, v2, v24

    const/16 v24, -0x6fa2

    aput v24, v2, v23

    const/16 v23, -0x1d

    aput v23, v2, v22

    const/16 v22, -0x1d

    aput v22, v2, v21

    const/16 v21, -0x4ed1

    aput v21, v2, v20

    const/16 v20, -0x28

    aput v20, v2, v19

    const/16 v19, -0x7da3

    aput v19, v2, v18

    const/16 v18, -0x19

    aput v18, v2, v17

    const/16 v17, -0x3c

    aput v17, v2, v16

    const/16 v16, -0x32

    aput v16, v2, v15

    const/16 v15, -0x1efc

    aput v15, v2, v14

    const/16 v14, -0x6d

    aput v14, v2, v13

    const/16 v13, -0x2ee2

    aput v13, v2, v12

    const/16 v12, -0x50

    aput v12, v2, v11

    const/16 v11, 0x731f

    aput v11, v2, v10

    const/16 v10, 0xf07

    aput v10, v2, v9

    const/16 v9, -0x2184

    aput v9, v2, v8

    const/16 v8, -0x45

    aput v8, v2, v7

    const/16 v7, 0x583d

    aput v7, v2, v6

    const/16 v6, 0x5729

    aput v6, v2, v5

    const/16 v5, 0x1832

    aput v5, v2, v3

    const/16 v3, -0x5796

    aput v3, v2, v1

    const/16 v1, 0x1b

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, -0x58

    aput v31, v1, v30

    const/16 v30, -0x43

    aput v30, v1, v29

    const/16 v29, 0x606d

    aput v29, v1, v28

    const/16 v28, 0x6c60

    aput v28, v1, v27

    const/16 v27, -0x4594

    aput v27, v1, v26

    const/16 v26, -0x46

    aput v26, v1, v25

    const/16 v25, -0x6f82

    aput v25, v1, v24

    const/16 v24, -0x70

    aput v24, v1, v23

    const/16 v23, -0x7a

    aput v23, v1, v22

    const/16 v22, -0x4eb4

    aput v22, v1, v21

    const/16 v21, -0x4f

    aput v21, v1, v20

    const/16 v20, -0x7dd5

    aput v20, v1, v19

    const/16 v19, -0x7e

    aput v19, v1, v18

    const/16 v18, -0x80

    aput v18, v1, v17

    const/16 v17, -0x56

    aput v17, v1, v16

    const/16 v16, -0x1e9f

    aput v16, v1, v15

    const/16 v15, -0x1f

    aput v15, v1, v14

    const/16 v14, -0x2e89

    aput v14, v1, v13

    const/16 v13, -0x2f

    aput v13, v1, v12

    const/16 v12, 0x734f

    aput v12, v1, v11

    const/16 v11, 0xf73

    aput v11, v1, v10

    const/16 v10, -0x21f1

    aput v10, v1, v9

    const/16 v9, -0x22

    aput v9, v1, v8

    const/16 v8, 0x5848

    aput v8, v1, v7

    const/16 v7, 0x5758

    aput v7, v1, v6

    const/16 v6, 0x1857

    aput v6, v1, v5

    const/16 v5, -0x57e8

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_2
    array-length v5, v1

    if-lt v3, v5, :cond_2

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_3
    array-length v5, v1

    if-lt v3, v5, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x1d

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x2

    const/4 v5, 0x3

    const/4 v6, 0x4

    const/4 v7, 0x5

    const/4 v8, 0x6

    const/4 v9, 0x7

    const/16 v10, 0x8

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0xb

    const/16 v14, 0xc

    const/16 v15, 0xd

    const/16 v16, 0xe

    const/16 v17, 0xf

    const/16 v18, 0x10

    const/16 v19, 0x11

    const/16 v20, 0x12

    const/16 v21, 0x13

    const/16 v22, 0x14

    const/16 v23, 0x15

    const/16 v24, 0x16

    const/16 v25, 0x17

    const/16 v26, 0x18

    const/16 v27, 0x19

    const/16 v28, 0x1a

    const/16 v29, 0x1b

    const/16 v30, 0x1c

    const/16 v31, 0x2f39

    aput v31, v2, v30

    const/16 v30, 0x755b

    aput v30, v2, v29

    const/16 v29, 0x441c

    aput v29, v2, v28

    const/16 v28, -0x31ce

    aput v28, v2, v27

    const/16 v27, -0x59

    aput v27, v2, v26

    const/16 v26, -0x32

    aput v26, v2, v25

    const/16 v25, -0x52

    aput v25, v2, v24

    const/16 v24, 0xa1d

    aput v24, v2, v23

    const/16 v23, 0x5e79

    aput v23, v2, v22

    const/16 v22, 0x2839

    aput v22, v2, v21

    const/16 v21, -0x40ba

    aput v21, v2, v20

    const/16 v20, -0x2a

    aput v20, v2, v19

    const/16 v19, -0x8d3

    aput v19, v2, v18

    const/16 v18, -0x7d

    aput v18, v2, v17

    const/16 v17, -0x5887

    aput v17, v2, v16

    const/16 v16, -0xc

    aput v16, v2, v15

    const/16 v15, -0x37c0

    aput v15, v2, v14

    const/16 v14, -0x53

    aput v14, v2, v13

    const/16 v13, 0x6c50

    aput v13, v2, v12

    const/16 v12, 0x4b05

    aput v12, v2, v11

    const/16 v11, 0x1b3d

    aput v11, v2, v10

    const/16 v10, 0x87e

    aput v10, v2, v9

    const/16 v9, 0x494c

    aput v9, v2, v8

    const/16 v8, -0x61d3

    aput v8, v2, v7

    const/4 v7, -0x5

    aput v7, v2, v6

    const/16 v6, -0x2c91

    aput v6, v2, v5

    const/16 v5, -0x46

    aput v5, v2, v4

    const/16 v4, -0x53e8

    aput v4, v2, v3

    const/4 v3, -0x4

    aput v3, v2, v1

    const/16 v1, 0x1d

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x2f40

    aput v32, v1, v31

    const/16 v31, 0x752f

    aput v31, v1, v30

    const/16 v30, 0x4475

    aput v30, v1, v29

    const/16 v29, -0x31bc

    aput v29, v1, v28

    const/16 v28, -0x32

    aput v28, v1, v27

    const/16 v27, -0x46

    aput v27, v1, v26

    const/16 v26, -0x33

    aput v26, v1, v25

    const/16 v25, 0xa5c

    aput v25, v1, v24

    const/16 v24, 0x5e0a

    aput v24, v1, v23

    const/16 v23, 0x285e

    aput v23, v1, v22

    const/16 v22, -0x40d8

    aput v22, v1, v21

    const/16 v21, -0x41

    aput v21, v1, v20

    const/16 v20, -0x8a7

    aput v20, v1, v19

    const/16 v19, -0x9

    aput v19, v1, v18

    const/16 v18, -0x58e4

    aput v18, v1, v17

    const/16 v17, -0x59

    aput v17, v1, v16

    const/16 v16, -0x37cd

    aput v16, v1, v15

    const/16 v15, -0x38

    aput v15, v1, v14

    const/16 v14, 0x6c33

    aput v14, v1, v13

    const/16 v13, 0x4b6c

    aput v13, v1, v12

    const/16 v12, 0x1b4b

    aput v12, v1, v11

    const/16 v11, 0x81b

    aput v11, v1, v10

    const/16 v10, 0x4908

    aput v10, v1, v9

    const/16 v9, -0x61b7

    aput v9, v1, v8

    const/16 v8, -0x62

    aput v8, v1, v7

    const/16 v7, -0x2ce3

    aput v7, v1, v6

    const/16 v6, -0x2d

    aput v6, v1, v5

    const/16 v5, -0x5387

    aput v5, v1, v4

    const/16 v4, -0x54

    aput v4, v1, v3

    const/4 v3, 0x0

    :goto_4
    array-length v4, v1

    if-lt v3, v4, :cond_4

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_5
    array-length v4, v1

    if-lt v3, v4, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x2e

    new-array v2, v1, [I

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x3

    const/4 v7, 0x4

    const/4 v8, 0x5

    const/4 v9, 0x6

    const/4 v10, 0x7

    const/16 v11, 0x8

    const/16 v12, 0x9

    const/16 v13, 0xa

    const/16 v14, 0xb

    const/16 v15, 0xc

    const/16 v16, 0xd

    const/16 v17, 0xe

    const/16 v18, 0xf

    const/16 v19, 0x10

    const/16 v20, 0x11

    const/16 v21, 0x12

    const/16 v22, 0x13

    const/16 v23, 0x14

    const/16 v24, 0x15

    const/16 v25, 0x16

    const/16 v26, 0x17

    const/16 v27, 0x18

    const/16 v28, 0x19

    const/16 v29, 0x1a

    const/16 v30, 0x1b

    const/16 v31, 0x1c

    const/16 v32, 0x1d

    const/16 v33, 0x1e

    const/16 v34, 0x1f

    const/16 v35, 0x20

    const/16 v36, 0x21

    const/16 v37, 0x22

    const/16 v38, 0x23

    const/16 v39, 0x24

    const/16 v40, 0x25

    const/16 v41, 0x26

    const/16 v42, 0x27

    const/16 v43, 0x28

    const/16 v44, 0x29

    const/16 v45, 0x2a

    const/16 v46, 0x2b

    const/16 v47, 0x2c

    const/16 v48, 0x2d

    const/16 v49, 0x1f31

    aput v49, v2, v48

    const/16 v48, -0x295

    aput v48, v2, v47

    const/16 v47, -0x72

    aput v47, v2, v46

    const/16 v46, -0x7e

    aput v46, v2, v45

    const/16 v45, 0xb1f

    aput v45, v2, v44

    const/16 v44, 0x786e

    aput v44, v2, v43

    const/16 v43, -0x1ae5

    aput v43, v2, v42

    const/16 v42, -0x74

    aput v42, v2, v41

    const/16 v41, -0x69

    aput v41, v2, v40

    const/16 v40, 0x1046

    aput v40, v2, v39

    const/16 v39, -0xeac

    aput v39, v2, v38

    const/16 v38, -0x6b

    aput v38, v2, v37

    const/16 v37, -0x31

    aput v37, v2, v36

    const/16 v36, -0x27

    aput v36, v2, v35

    const/16 v35, -0x6cae

    aput v35, v2, v34

    const/16 v34, -0xe

    aput v34, v2, v33

    const/16 v33, -0x69f

    aput v33, v2, v32

    const/16 v32, -0x6c

    aput v32, v2, v31

    const/16 v31, -0x53

    aput v31, v2, v30

    const/16 v30, -0x1ff8

    aput v30, v2, v29

    const/16 v29, -0x40

    aput v29, v2, v28

    const/16 v28, 0x5408

    aput v28, v2, v27

    const/16 v27, -0x11cb

    aput v27, v2, v26

    const/16 v26, -0x5d

    aput v26, v2, v25

    const/16 v25, -0xc

    aput v25, v2, v24

    const/16 v24, 0x115a

    aput v24, v2, v23

    const/16 v23, -0x4588

    aput v23, v2, v22

    const/16 v22, -0x34

    aput v22, v2, v21

    const/16 v21, 0x5b18

    aput v21, v2, v20

    const/16 v20, 0x701f

    aput v20, v2, v19

    const/16 v19, 0xb14

    aput v19, v2, v18

    const/16 v18, -0x7492

    aput v18, v2, v17

    const/16 v17, -0x7

    aput v17, v2, v16

    const/16 v16, -0x69

    aput v16, v2, v15

    const/16 v15, -0x5cf8

    aput v15, v2, v14

    const/16 v14, -0xd

    aput v14, v2, v13

    const/16 v13, -0x62

    aput v13, v2, v12

    const/16 v12, -0x22

    aput v12, v2, v11

    const/16 v11, 0x3306

    aput v11, v2, v10

    const/16 v10, -0x55a3

    aput v10, v2, v9

    const/16 v9, -0x3d

    aput v9, v2, v8

    const/16 v8, -0x3c

    aput v8, v2, v7

    const/16 v7, 0x3b7c

    aput v7, v2, v6

    const/16 v6, -0x13a2

    aput v6, v2, v5

    const/16 v5, -0x80

    aput v5, v2, v3

    const/16 v3, 0x717b

    aput v3, v2, v1

    const/16 v1, 0x2e

    new-array v1, v1, [I

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x4

    const/4 v9, 0x5

    const/4 v10, 0x6

    const/4 v11, 0x7

    const/16 v12, 0x8

    const/16 v13, 0x9

    const/16 v14, 0xa

    const/16 v15, 0xb

    const/16 v16, 0xc

    const/16 v17, 0xd

    const/16 v18, 0xe

    const/16 v19, 0xf

    const/16 v20, 0x10

    const/16 v21, 0x11

    const/16 v22, 0x12

    const/16 v23, 0x13

    const/16 v24, 0x14

    const/16 v25, 0x15

    const/16 v26, 0x16

    const/16 v27, 0x17

    const/16 v28, 0x18

    const/16 v29, 0x19

    const/16 v30, 0x1a

    const/16 v31, 0x1b

    const/16 v32, 0x1c

    const/16 v33, 0x1d

    const/16 v34, 0x1e

    const/16 v35, 0x1f

    const/16 v36, 0x20

    const/16 v37, 0x21

    const/16 v38, 0x22

    const/16 v39, 0x23

    const/16 v40, 0x24

    const/16 v41, 0x25

    const/16 v42, 0x26

    const/16 v43, 0x27

    const/16 v44, 0x28

    const/16 v45, 0x29

    const/16 v46, 0x2a

    const/16 v47, 0x2b

    const/16 v48, 0x2c

    const/16 v49, 0x2d

    const/16 v50, 0x1f11

    aput v50, v1, v49

    const/16 v49, -0x2e1

    aput v49, v1, v48

    const/16 v48, -0x3

    aput v48, v1, v47

    const/16 v47, -0x15

    aput v47, v1, v46

    const/16 v46, 0xb53

    aput v46, v1, v45

    const/16 v45, 0x780b

    aput v45, v1, v44

    const/16 v44, -0x1a88

    aput v44, v1, v43

    const/16 v43, -0x1b

    aput v43, v1, v42

    const/16 v42, -0x1f

    aput v42, v1, v41

    const/16 v41, 0x1023

    aput v41, v1, v40

    const/16 v40, -0xef0

    aput v40, v1, v39

    const/16 v39, -0xf

    aput v39, v1, v38

    const/16 v38, -0x56

    aput v38, v1, v37

    const/16 v37, -0x55

    aput v37, v1, v36

    const/16 v36, -0x6cc5

    aput v36, v1, v35

    const/16 v35, -0x6d

    aput v35, v1, v34

    const/16 v34, -0x6cf

    aput v34, v1, v33

    const/16 v33, -0x7

    aput v33, v1, v32

    const/16 v32, -0x73

    aput v32, v1, v31

    const/16 v31, -0x1fd2

    aput v31, v1, v30

    const/16 v30, -0x20

    aput v30, v1, v29

    const/16 v29, 0x5478

    aput v29, v1, v28

    const/16 v28, -0x11ac

    aput v28, v1, v27

    const/16 v27, -0x12

    aput v27, v1, v26

    const/16 v26, -0x6f

    aput v26, v1, v25

    const/16 v25, 0x1139

    aput v25, v1, v24

    const/16 v24, -0x45ef

    aput v24, v1, v23

    const/16 v23, -0x46

    aput v23, v1, v22

    const/16 v22, 0x5b7d

    aput v22, v1, v21

    const/16 v21, 0x705b

    aput v21, v1, v20

    const/16 v20, 0xb70

    aput v20, v1, v19

    const/16 v19, -0x74f5

    aput v19, v1, v18

    const/16 v18, -0x75

    aput v18, v1, v17

    const/16 v17, -0x2

    aput v17, v1, v16

    const/16 v16, -0x5c97

    aput v16, v1, v15

    const/16 v15, -0x5d

    aput v15, v1, v14

    const/16 v14, -0xd

    aput v14, v1, v13

    const/4 v13, -0x2

    aput v13, v1, v12

    const/16 v12, 0x3361

    aput v12, v1, v11

    const/16 v11, -0x55cd

    aput v11, v1, v10

    const/16 v10, -0x56

    aput v10, v1, v9

    const/16 v9, -0x4a

    aput v9, v1, v8

    const/16 v8, 0x3b1d

    aput v8, v1, v7

    const/16 v7, -0x13c5

    aput v7, v1, v6

    const/16 v6, -0x14

    aput v6, v1, v5

    const/16 v5, 0x7118

    aput v5, v1, v3

    const/4 v3, 0x0

    :goto_6
    array-length v5, v1

    if-lt v3, v5, :cond_6

    array-length v1, v2

    new-array v1, v1, [C

    const/4 v3, 0x0

    :goto_7
    array-length v5, v1

    if-lt v3, v5, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mScanningFragmentController:Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->connectivity_type:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->device_type:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->data_type:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mDeviceID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/shealth/framework/ui/base/ScanningFragmentController;->requestPairedDeviceForRename(IIILjava/lang/String;)Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->settingsSensorDeviceDetails:Lcom/sec/android/app/shealth/framework/ui/utils/SensorDeviceDetails;

    :goto_8
    return-void

    :cond_0
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_2
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :cond_3
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_4
    aget v4, v1, v3

    aget v5, v2, v3

    xor-int/2addr v4, v5

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    :cond_5
    aget v4, v2, v3

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    :cond_6
    aget v5, v1, v3

    aget v6, v2, v3

    xor-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    :cond_7
    aget v5, v2, v3

    int-to-char v5, v5

    aput-char v5, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_7

    :catch_0
    move-exception v1

    goto :goto_8
.end method

.method public unRegisterHomeKeyPress()V
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity;->mCloseSystemDialogsReceiver:Lcom/sec/android/app/shealth/framework/ui/base/PairedDevicesSettingsActivity$CloseSystemDialogsIntentReceiver;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
