.class public interface abstract Lcom/sec/android/app/shealth/backup/server/IRestoreListener;
.super Ljava/lang/Object;
.source "IRestoreListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;
    }
.end annotation


# virtual methods
.method public abstract onAccountException(Ljava/lang/String;)V
.end method

.method public abstract onData()V
.end method

.method public abstract onException(Ljava/lang/Exception;)V
.end method

.method public abstract onNoData()V
.end method

.method public abstract onRestoreProgress(Lcom/sec/android/app/shealth/backup/server/IRestoreListener$RestoreStatus;J)V
.end method
