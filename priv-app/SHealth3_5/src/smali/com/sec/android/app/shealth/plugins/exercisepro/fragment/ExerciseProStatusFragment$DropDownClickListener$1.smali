.class Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;
.super Ljava/lang/Object;
.source "ExerciseProStatusFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/ListPopup$OnItemClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

.field final synthetic val$typesMap:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 2464
    iput-object p1, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iput-object p2, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->val$typesMap:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(ILjava/lang/String;Landroid/widget/PopupWindow;)V
    .locals 6
    .param p1, "itemIndex"    # I
    .param p2, "itemContent"    # Ljava/lang/String;
    .param p3, "popupWindow"    # Landroid/widget/PopupWindow;

    .prologue
    .line 2467
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$4700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->dismiss()V

    .line 2468
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->popup:Lcom/sec/android/app/shealth/common/commonui/ListPopup;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$4700(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Lcom/sec/android/app/shealth/common/commonui/ListPopup;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/shealth/common/commonui/ListPopup;->setCurrentItem(I)V

    .line 2469
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->val$typesMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2470
    .local v2, "type":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->isHRMConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->curDataType:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->access$5000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;)I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 2471
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->mCanHideHRMData:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$3102(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;Z)Z

    .line 2473
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->curDataType:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->access$5000(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;)I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 2487
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/shealth/sharedpreferences/SharedPreferencesHelper;->saveRealtimeDispDataStatus(Ljava/util/ArrayList;)V

    .line 2488
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # invokes: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->updateDataText()V
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$2500(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)V

    .line 2489
    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "type chosen: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2490
    return-void

    .line 2475
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2477
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->viewIdx:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->access$5200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2478
    .local v1, "orgType":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2479
    .local v0, "chgIdx":I
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2480
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->viewIdx:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->access$5200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;)I

    move-result v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2482
    .end local v0    # "chgIdx":I
    .end local v1    # "orgType":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2484
    iget-object v3, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    iget-object v3, v3, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->this$0:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->dispDataTypes:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;->access$5100(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener$1;->this$1:Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;

    # getter for: Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->viewIdx:I
    invoke-static {v4}, Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;->access$5200(Lcom/sec/android/app/shealth/plugins/exercisepro/fragment/ExerciseProStatusFragment$DropDownClickListener;)I

    move-result v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method
