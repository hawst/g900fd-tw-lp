.class Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$2;
.super Ljava/lang/Object;
.source "CompatibleAccessoryDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->customizeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 244
    instance-of v2, p1, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 246
    check-cast v0, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;

    .line 248
    .local v0, "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    invoke-virtual {v0}, Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;->getActionButtonIndex()I

    move-result v2

    if-nez v2, :cond_0

    .line 250
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    const-class v3, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleDeviceConnectActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 252
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "accessory_device_detail"

    iget-object v3, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    iget-object v3, v3, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->mAccessoryDetails:Lcom/sec/android/app/shealth/settings/compatibleaccessories/AccessoryDetails;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 254
    iget-object v2, p0, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity$2;->this$0:Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/shealth/settings/compatibleaccessories/CompatibleAccessoryDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 257
    .end local v0    # "ab":Lcom/sec/android/app/shealth/framework/ui/actionbar/ActionBarButtonView;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
