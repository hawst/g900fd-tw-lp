.class Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;
.super Ljava/lang/Object;
.source "SetQuickInputActivity.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getDialogButtonController(Ljava/lang/String;)Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 5
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    const/16 v3, 0x1388

    const/4 v2, 0x2

    .line 278
    sget-object v1, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    if-ne p1, v1, :cond_1

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$000(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$100(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 282
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # invokes: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getCalorieValue()I
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$400(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)I

    move-result v0

    .line 283
    .local v0, "calorieValue":I
    if-lt v0, v2, :cond_0

    if-le v0, v3, :cond_3

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # invokes: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->showRangeToast()V
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$500(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)V

    .line 286
    if-ge v0, v2, :cond_2

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 295
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->selectAll()V

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->requestFocus()Z

    .line 313
    .end local v0    # "calorieValue":I
    :cond_1
    :goto_1
    return-void

    .line 292
    .restart local v0    # "calorieValue":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mCalorieValueEditText:Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$300(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/shealth/food/utils/MinMaxInputEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 301
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedMealType:Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$000(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider;->setCalorieForMealType(Lcom/sec/android/app/shealth/food/quickinput/helper/QuickInputCalorieProvider$MealType;I)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mClickedView:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$100(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0803b1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    const v4, 0x7f0900b9

    invoke-virtual {v3, v4}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$200(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$200(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity$3;->this$0:Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;

    # getter for: Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->mSetQuickInputDialog:Landroid/support/v4/app/DialogFragment;
    invoke-static {v1}, Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;->access$200(Lcom/sec/android/app/shealth/food/quickinput/SetQuickInputActivity;)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    goto :goto_1
.end method
