.class public Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;
.super Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;
.source "TwoCategoriesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$1;,
        Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;,
        Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/ListItem;",
        ":",
        "Ljava/lang/Comparable;",
        ">",
        "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static final HEADER_COUNT:I = 0x2


# instance fields
.field protected final firstCategory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final mHeaders:[Ljava/lang/String;

.field private mIsSingleCategoryCaptionVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p4, "firstHeader"    # Ljava/lang/String;
    .param p5, "mainHeader"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<TE;>;",
            "Ljava/util/List",
            "<TE;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    .local p2, "firstCategoryItems":Ljava/util/List;, "Ljava/util/List<TE;>;"
    .local p3, "mainCategoryItems":Ljava/util/List;, "Ljava/util/List<TE;>;"
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 40
    iput-boolean v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->mIsSingleCategoryCaptionVisible:Z

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->mHeaders:[Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->mHeaders:[Ljava/lang/String;

    aput-object p4, v0, v1

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->mHeaders:[Ljava/lang/String;

    const/4 v1, 0x1

    aput-object p5, v0, v1

    .line 54
    iput-object p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    .line 55
    return-void
.end method

.method private getAdditionalItemsCount()I
    .locals 4

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->isFirstHeaderVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->isMainHeaderVisible()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private getItemTypeForPosition(I)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;
    .locals 5
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter",
            "<TE;>.ItemTypedPosition;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    const/4 v4, 0x0

    .line 154
    const/4 v0, 0x0

    .line 155
    .local v0, "previousItemsCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->isFirstHeaderVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    if-nez p1, :cond_0

    .line 157
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;->LIST_HEADER:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;ILcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$1;)V

    .line 167
    :goto_0
    return-object v1

    .line 159
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->isMainHeaderVisible()Z

    move-result v1

    if-eqz v1, :cond_3

    sub-int v1, p1, v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_3

    .line 162
    sub-int v1, p1, v0

    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 163
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;->LIST_HEADER:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;ILcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$1;)V

    goto :goto_0

    .line 165
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 167
    :cond_3
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;

    sget-object v2, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;->LIST_ITEM:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;

    sub-int v3, p1, v0

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;-><init>(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;ILcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$1;)V

    goto :goto_0
.end method

.method private inflateHeader(Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/ViewGroup;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "header"    # Ljava/lang/String;

    .prologue
    .line 176
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300de

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 177
    .local v0, "convertView":Landroid/view/ViewGroup;
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->updateHeaderTitle(Landroid/view/View;Ljava/lang/String;)V

    .line 178
    return-object v0
.end method

.method private isHeaderVisible()Z
    .locals 1

    .prologue
    .line 138
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    iget-boolean v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->mIsSingleCategoryCaptionVisible:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->isOnlyOneCategoryContainData()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOnlyOneCategoryContainData()Z
    .locals 3

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    const/4 v1, 0x1

    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "categoryCounter":I
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 144
    add-int/lit8 v0, v0, 0x1

    .line 146
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 147
    add-int/lit8 v0, v0, 0x1

    .line 149
    :cond_1
    if-ne v0, v1, :cond_2

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateHeaderTitle(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "header"    # Ljava/lang/String;

    .prologue
    .line 182
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    const v0, 0x7f0803a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0901fd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 185
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 59
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    invoke-super {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->getAdditionalItemsCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 116
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->getItemTypeForPosition(I)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;->itemType:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 77
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->getItemTypeForPosition(I)Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;

    move-result-object v4

    .line 78
    .local v4, "typedPosition":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>.ItemTypedPosition;"
    iget v2, v4, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;->realPosition:I

    .line 79
    .local v2, "realPosition":I
    sget-object v5, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$1;->$SwitchMap$com$sec$android$app$shealth$food$foodpick$search$frequent$TwoCategoriesAdapter$ItemType:[I

    iget-object v6, v4, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;->itemType:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;

    invoke-virtual {v6}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 108
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Undefined item type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemTypedPosition;->itemType:Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;

    invoke-virtual {v7}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 81
    :pswitch_0
    move-object v3, p2

    .line 82
    .local v3, "result":Landroid/view/View;
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->mHeaders:[Ljava/lang/String;

    aget-object v1, v5, v2

    .line 83
    .local v1, "header":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 84
    invoke-direct {p0, p3, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->inflateHeader(Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v3

    .line 111
    .end local v1    # "header":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v3

    .line 86
    .restart local v1    # "header":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, v3, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->updateHeaderTitle(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .end local v1    # "header":Ljava/lang/String;
    .end local v3    # "result":Landroid/view/View;
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_3

    .line 91
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->getViewFromListItem(Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoritableListItem;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 92
    .restart local v3    # "result":Landroid/view/View;
    const v5, 0x7f0803d2

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 94
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v2, v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_2

    .line 96
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 98
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 103
    .end local v0    # "divider":Landroid/view/View;
    .end local v3    # "result":Landroid/view/View;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v2, v5

    .line 104
    invoke-super {p0, v2, p2, p3}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/FavoriteAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 106
    .restart local v3    # "result":Landroid/view/View;
    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 172
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    invoke-static {}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;->values()[Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter$ItemType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method protected isFirstHeaderVisible()Z
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->firstCategory:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->isHeaderVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isMainHeaderVisible()Z
    .locals 1

    .prologue
    .line 71
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;, "Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter<TE;>;"
    iget-object v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->itemsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/frequent/TwoCategoriesAdapter;->isHeaderVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
