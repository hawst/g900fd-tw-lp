.class public final Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchFactory;
.super Ljava/lang/Object;
.source "FoodSearchFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static getSearchApi(Landroid/content/Context;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "locale":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->KoreaWW:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->KoreaKR:Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;

    invoke-virtual {v3}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils$Countries;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 48
    .local v0, "isKoreanLocale":Z
    :goto_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isKoreaModel()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 49
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;-><init>(Landroid/content/Context;)V

    .line 55
    .local v2, "searchApi":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    :goto_1
    return-object v2

    .line 45
    .end local v0    # "isKoreanLocale":Z
    .end local v2    # "searchApi":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 50
    .restart local v0    # "isKoreanLocale":Z
    :cond_2
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ConfigUtils;->isSupportsBoohee()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 51
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;-><init>(Landroid/content/Context;)V

    .restart local v2    # "searchApi":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    goto :goto_1

    .line 53
    .end local v2    # "searchApi":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    :cond_3
    new-instance v2, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;-><init>(Landroid/content/Context;)V

    .restart local v2    # "searchApi":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    goto :goto_1
.end method

.method public static getSearchApiBySourceType(Landroid/content/Context;I)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "serverSourceType"    # I

    .prologue
    .line 70
    packed-switch p1, :pswitch_data_0

    .line 80
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 72
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/fatsecret/FatSecretSearch;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 74
    :pswitch_1
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/boohee/BooheeSearch;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 78
    :pswitch_2
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/samsungosp/SamsungOspSearch;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x46cd2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
