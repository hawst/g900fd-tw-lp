.class public Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;
.super Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;
.source "SearchListResultBase.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Landroid/os/Parcelable;",
        "Req:",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;",
        ">",
        "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult",
        "<",
        "Ljava/util/List",
        "<TE;>;TReq;>;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mTotalCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase$1;

    invoke-direct {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .param p2, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TE;>;I)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<TE;TReq;>;"
    .local p1, "searchResult":Ljava/util/List;, "Ljava/util/List<TE;>;"
    invoke-direct {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchResult;-><init>(Ljava/lang/Object;)V

    .line 34
    iput p2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->mTotalCount:I

    .line 35
    return-void
.end method


# virtual methods
.method public createNextSearchListRequest(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    .locals 5
    .param p1, "searchApi"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .prologue
    .line 51
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<TE;TReq;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getRequest()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    .line 52
    .local v0, "prevRequest":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->isAllDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    const/4 v1, 0x0

    .line 55
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getStringRequest()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getPageNumber()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getPerPageNumber()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;-><init>(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<TE;TReq;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 41
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<TE;TReq;>;"
    iget v0, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->mTotalCount:I

    return v0
.end method

.method public isAllDataLoaded(Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;)Z
    .locals 5
    .param p1, "searchApi"    # Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;

    .prologue
    .line 68
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<TE;TReq;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getRequest()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;

    .line 69
    .local v1, "prevRequest":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;
    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getPerPageNumber()I

    move-result v2

    invoke-virtual {v1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListRequest;->getPageNumber()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {p1}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/FoodSearchApi;->getIndexOfFirstPage()I

    move-result v4

    sub-int/2addr v3, v4

    mul-int v0, v2, v3

    .line 71
    .local v0, "loadedCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getTotalCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .local p0, "this":Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;, "Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase<TE;TReq;>;"
    const/4 v3, 0x0

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getSearchResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 77
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<TE;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 78
    .local v0, "isListEmpty":Z
    if-nez v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    if-nez v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getSearchResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 82
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 83
    iget v2, p0, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->mTotalCount:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/shealth/food/foodpick/search/api/base/SearchListResultBase;->getRequest()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 85
    return-void

    :cond_1
    move v2, v3

    .line 78
    goto :goto_0
.end method
