.class Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;
.super Ljava/lang/Object;
.source "WalkingMateSummaryFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->setPercentValue()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V
    .locals 0

    .prologue
    .line 890
    iput-object p1, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 894
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mTotalStep:I
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$800(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I
    invoke-static {v10}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v10

    int-to-float v10, v10

    div-float v7, v9, v10

    .line 895
    .local v7, "progress":F
    const/high16 v9, 0x42c80000    # 100.0f

    mul-float/2addr v9, v7

    float-to-int v6, v9

    .line 896
    .local v6, "per":I
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 898
    .local v2, "goalText":Landroid/text/SpannableStringBuilder;
    invoke-static {}, Lcom/sec/android/app/shealth/walkingmate/utils/Utils;->isArabLocale()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 899
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090b68

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I
    invoke-static {v13}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 905
    .local v3, "goalTxt":Ljava/lang/String;
    :goto_0
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 906
    .local v1, "goal":Landroid/text/SpannableString;
    new-instance v9, Landroid/text/style/StyleSpan;

    const/4 v10, 0x2

    invoke-direct {v9, v10}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v10, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v11

    const/4 v12, 0x0

    invoke-virtual {v1, v9, v10, v11, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 907
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090b68

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 908
    .local v8, "temp":Ljava/lang/String;
    const-string v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 909
    .local v5, "indexP":I
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v4

    .line 910
    .local v4, "indexL":I
    new-instance v9, Landroid/text/style/StyleSpan;

    const/4 v10, 0x3

    invoke-direct {v9, v10}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int v10, v5, v4

    const/4 v11, 0x0

    invoke-virtual {v1, v9, v5, v10, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 911
    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 913
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mConnectAccessory:I
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1000(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v9

    const/16 v10, 0x2719

    if-ne v9, v10, :cond_2

    .line 914
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mSyncLayout:Landroid/view/View;
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1100(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 918
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalTxt:Landroid/widget/TextView;
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1200(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 920
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    move-result-object v10

    const/16 v9, 0xa

    if-ge v6, v9, :cond_3

    const/4 v9, 0x1

    :goto_2
    invoke-virtual {v10, v9}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->changeCircleWidth(Z)V

    .line 921
    move v0, v6

    .line 922
    .local v0, "aniPer":I
    const/16 v9, 0xa

    if-ge v6, v9, :cond_0

    .line 923
    const/4 v6, 0x0

    .line 925
    :cond_0
    const/4 v9, 0x1

    if-lt v6, v9, :cond_5

    .line 926
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mIsResumed:Z
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1400(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 927
    new-instance v9, Landroid/os/Handler;

    invoke-direct {v9}, Landroid/os/Handler;-><init>()V

    new-instance v10, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5$1;

    invoke-direct {v10, p0, v0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5$1;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;I)V

    const-wide/16 v11, 0x258

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 948
    :goto_3
    return-void

    .line 902
    .end local v0    # "aniPer":I
    .end local v1    # "goal":Landroid/text/SpannableString;
    .end local v3    # "goalTxt":Ljava/lang/String;
    .end local v4    # "indexL":I
    .end local v5    # "indexP":I
    .end local v8    # "temp":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090b68

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mGoalValue:I
    invoke-static {v12}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$900(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "goalTxt":Ljava/lang/String;
    goto/16 :goto_0

    .line 916
    .restart local v1    # "goal":Landroid/text/SpannableString;
    .restart local v4    # "indexL":I
    .restart local v5    # "indexP":I
    .restart local v8    # "temp":Ljava/lang/String;
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # invokes: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->displayGearState()V
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$700(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)V

    goto :goto_1

    .line 920
    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    .line 936
    .restart local v0    # "aniPer":I
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;->this$0:Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;

    # getter for: Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->mProgressImage:Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;
    invoke-static {v9}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;->access$1300(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment;)Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;

    move-result-object v9

    invoke-virtual {v9, v6}, Lcom/sec/android/app/shealth/common/widget/HoloCircleSeekBar;->setProgress(I)V

    goto :goto_3

    .line 939
    :cond_5
    const/4 v6, 0x0

    .line 940
    new-instance v9, Landroid/os/Handler;

    invoke-direct {v9}, Landroid/os/Handler;-><init>()V

    new-instance v10, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5$2;

    invoke-direct {v10, p0}, Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5$2;-><init>(Lcom/sec/android/app/shealth/walkingmate/WalkingMateSummaryFragment$5;)V

    const-wide/16 v11, 0x258

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3
.end method
