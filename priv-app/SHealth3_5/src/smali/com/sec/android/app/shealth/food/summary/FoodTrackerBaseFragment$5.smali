.class Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;
.super Ljava/lang/Object;
.source "FoodTrackerBaseFragment.java"

# interfaces
.implements Lcom/sec/android/app/shealth/common/commonui/dialog/IDialogButtonController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V
    .locals 0

    .prologue
    .line 810
    iput-object p1, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDialogButtonClick(Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;)V
    .locals 5
    .param p1, "dialogButtonType"    # Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    .prologue
    .line 815
    sget-object v2, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->POSITIVE:Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/shealth/common/commonui/dialog/DialogButtonType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 817
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->isDaoInitNeeded()Z
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$700(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 819
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initDao()V

    .line 821
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v3, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # getter for: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mMealType:I
    invoke-static {v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$800(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->getMealIdByMealTypeId(J)J

    move-result-wide v0

    .line 822
    .local v0, "mealId":J
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    iget-object v2, v2, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->mealDao:Lcom/sec/android/app/shealth/food/fooddao/MealDao;

    invoke-interface {v2, v0, v1}, Lcom/sec/android/app/shealth/food/fooddao/MealDao;->deleteDataById(J)Z

    .line 823
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->insertQuickInputMealDataToEmptyMeal(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$900(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;I)V

    .line 824
    iget-object v2, p0, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment$5;->this$0:Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;

    # invokes: Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->initData()V
    invoke-static {v2}, Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;->access$1000(Lcom/sec/android/app/shealth/food/summary/FoodTrackerBaseFragment;)V

    .line 827
    .end local v0    # "mealId":J
    :cond_1
    return-void
.end method
